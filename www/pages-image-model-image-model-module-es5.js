(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-image-model-image-model-module"], {
    /***/
    "+6nL":
    /*!*********************************************************!*\
      !*** ./src/app/pages/image-model/image-model.module.ts ***!
      \*********************************************************/

    /*! exports provided: ImageModelPageModule */

    /***/
    function nL(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImageModelPageModule", function () {
        return ImageModelPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _image_model_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./image-model-routing.module */
      "HjL3");
      /* harmony import */


      var _image_model_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./image-model.page */
      "0dez");

      var ImageModelPageModule = function ImageModelPageModule() {
        _classCallCheck(this, ImageModelPageModule);
      };

      ImageModelPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _image_model_routing_module__WEBPACK_IMPORTED_MODULE_5__["ImageModelPageRoutingModule"]],
        declarations: [_image_model_page__WEBPACK_IMPORTED_MODULE_6__["ImageModelPage"]]
      })], ImageModelPageModule);
      /***/
    },

    /***/
    "0dez":
    /*!*******************************************************!*\
      !*** ./src/app/pages/image-model/image-model.page.ts ***!
      \*******************************************************/

    /*! exports provided: ImageModelPage */

    /***/
    function dez(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImageModelPage", function () {
        return ImageModelPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_image_model_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./image-model.page.html */
      "rgys");
      /* harmony import */


      var _image_model_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./image-model.page.scss */
      "rbJe");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var ImageModelPage = /*#__PURE__*/function () {
        function ImageModelPage(navContrl, router, route) {
          var _this = this;

          _classCallCheck(this, ImageModelPage);

          this.navContrl = navContrl;
          this.router = router;
          this.route = route;
          this.sliderOpts = {
            zoom: {
              maxRatio: 5
            }
          };
          this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
              _this.image = _this.router.getCurrentNavigation().extras.state.imageArray;
              console.log("this.imageArr111111 : ", _this.image);
            }
          }); // this.image = navParams.get('imageArray'); 
        }

        _createClass(ImageModelPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "zoom",
          value: function zoom(zoomIn) {
            var zoom = this.slider.nativeElement.swiper.zoom;

            if (zoomIn) {
              zoom["in"]();
            } else {
              zoom.out();
            }
          }
        }, {
          key: "slideChanged",
          value: function slideChanged() {
            var currentIndex = this.slides.getSwiper();
            console.log('Current index is', currentIndex);
          }
        }, {
          key: "dismiss",
          value: function dismiss() {
            this.navContrl.back(); // this.router.navigateByUrl('/')
            // this.modalController.dismiss();
          }
        }]);

        return ImageModelPage;
      }();

      ImageModelPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }];
      };

      ImageModelPage.propDecorators = {
        slider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: ['slider', {
            read: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ElementRef"]
          }]
        }],
        slides: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonSlides"]]
        }]
      };
      ImageModelPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-image-model',
        template: _raw_loader_image_model_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_image_model_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], ImageModelPage);
      /***/
    },

    /***/
    "HjL3":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/image-model/image-model-routing.module.ts ***!
      \*****************************************************************/

    /*! exports provided: ImageModelPageRoutingModule */

    /***/
    function HjL3(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImageModelPageRoutingModule", function () {
        return ImageModelPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _image_model_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./image-model.page */
      "0dez");

      var routes = [{
        path: '',
        component: _image_model_page__WEBPACK_IMPORTED_MODULE_3__["ImageModelPage"]
      }];

      var ImageModelPageRoutingModule = function ImageModelPageRoutingModule() {
        _classCallCheck(this, ImageModelPageRoutingModule);
      };

      ImageModelPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ImageModelPageRoutingModule);
      /***/
    },

    /***/
    "rbJe":
    /*!*********************************************************!*\
      !*** ./src/app/pages/image-model/image-model.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function rbJe(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".close-fake {\n  --background: transparent;\n  margin-top: 20px;\n  /* Status bar height on iOS 10 */\n  margin-top: constant(safe-area-inset-top);\n  /* Status bar height on iOS 11.0 */\n  margin-top: env(safe-area-inset-top);\n}\n.close-fake ion-icon {\n  font-size: 22px;\n  color: #ffffff;\n}\nion-content {\n  --background: rgba(0, 0, 0, 0.84);\n}\nion-slides {\n  height: 70%;\n}\n.swiper-slide img {\n  width: 100%;\n  height: 100vw;\n  width: 100%;\n  -o-object-fit: contain;\n  object-fit: contain;\n  -o-object-fit: cover;\n  object-fit: cover;\n}\n.swiper-slide {\n  display: block;\n}\np {\n  color: #ffffff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2ltYWdlLW1vZGVsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQ0FBQTtFQUNBLHlDQUFBO0VBQ0Esa0NBQUE7RUFDQSxvQ0FBQTtBQUNGO0FBQ0U7RUFDRSxlQUFBO0VBQ0EsY0FBQTtBQUNKO0FBS0E7RUFDRSxpQ0FBQTtBQUZGO0FBTUE7RUFDRSxXQUFBO0FBSEY7QUFNQTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLGlCQUFBO0FBSEY7QUFNQTtFQUNFLGNBQUE7QUFIRjtBQU1BO0VBQ0UsY0FBQTtBQUhGIiwiZmlsZSI6ImltYWdlLW1vZGVsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jbG9zZS1mYWtlIHtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgLyogU3RhdHVzIGJhciBoZWlnaHQgb24gaU9TIDEwICovXG4gIG1hcmdpbi10b3A6IGNvbnN0YW50KHNhZmUtYXJlYS1pbnNldC10b3ApO1xuICAvKiBTdGF0dXMgYmFyIGhlaWdodCBvbiBpT1MgMTEuMCAqL1xuICBtYXJnaW4tdG9wOiBlbnYoc2FmZS1hcmVhLWluc2V0LXRvcCk7XG5cbiAgaW9uLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgfVxuXG4gIC8vIG1hcmdpbi10b3A6IDQwcHg7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuODQpO1xuICAvLyAtLWJhY2tncm91bmQ6IHJnYig1NiA1NiA1Nik7XG59XG5cbmlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDcwJTtcbn1cblxuLnN3aXBlci1zbGlkZSBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDB2dztcbiAgd2lkdGg6IDEwMCU7XG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XG4gIC1vLW9iamVjdC1maXQ6IGNvdmVyO1xuICBvYmplY3QtZml0OiBjb3Zlcjtcbn1cblxuLnN3aXBlci1zbGlkZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG5we1xuICBjb2xvcjogI2ZmZmZmZjtcbn0iXX0= */";
      /***/
    },

    /***/
    "rgys":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/image-model/image-model.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function rgys(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content fullscreen>\n  <ion-item class=\"close-fake\" lines=\"none\" text-center>\n    <ion-button (click)=\"dismiss()\" fill=\"clear\" color=\"light\">\n      <ion-icon name=\"close\" slot=\"start\"></ion-icon>\n      \n    </ion-button>\n\n    <!-- <ion-button (click)=\"zoom(true)\" fill=\"clear\" color=\"light\">\n      <ion-icon name=\"add\" slot=\"start\"></ion-icon>\n      in\n    </ion-button>\n\n    <ion-button (click)=\"zoom(false)\" fill=\"clear\" color=\"light\">\n      <ion-icon name=\"remove\" slot=\"start\"></ion-icon>\n      out\n    </ion-button> -->\n  </ion-item>\n\n  <ion-slides [options]=\"sliderOpts\" #slider>\n    <ion-slide *ngFor=\"let img of image\">\n      <p class=\"ion-padding-horizontal--\">{{img.title}}</p>\n      <div class=\"swiper-zoom-container slide\">\n        <img [src]=\"img?.imageUrl\">\n      </div>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-image-model-image-model-module-es5.js.map