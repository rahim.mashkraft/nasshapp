(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-twillio-model-twillio-model-module~tab4-tab4-module"], {
    /***/
    "0dez":
    /*!*******************************************************!*\
      !*** ./src/app/pages/image-model/image-model.page.ts ***!
      \*******************************************************/

    /*! exports provided: ImageModelPage */

    /***/
    function dez(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImageModelPage", function () {
        return ImageModelPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_image_model_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./image-model.page.html */
      "rgys");
      /* harmony import */


      var _image_model_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./image-model.page.scss */
      "rbJe");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var ImageModelPage = /*#__PURE__*/function () {
        function ImageModelPage(navContrl, router, route) {
          var _this = this;

          _classCallCheck(this, ImageModelPage);

          this.navContrl = navContrl;
          this.router = router;
          this.route = route;
          this.sliderOpts = {
            zoom: {
              maxRatio: 5
            }
          };
          this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
              _this.image = _this.router.getCurrentNavigation().extras.state.imageArray;
              console.log("this.imageArr111111 : ", _this.image);
            }
          }); // this.image = navParams.get('imageArray'); 
        }

        _createClass(ImageModelPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "zoom",
          value: function zoom(zoomIn) {
            var zoom = this.slider.nativeElement.swiper.zoom;

            if (zoomIn) {
              zoom["in"]();
            } else {
              zoom.out();
            }
          }
        }, {
          key: "slideChanged",
          value: function slideChanged() {
            var currentIndex = this.slides.getSwiper();
            console.log('Current index is', currentIndex);
          }
        }, {
          key: "dismiss",
          value: function dismiss() {
            this.navContrl.back(); // this.router.navigateByUrl('/')
            // this.modalController.dismiss();
          }
        }]);

        return ImageModelPage;
      }();

      ImageModelPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }];
      };

      ImageModelPage.propDecorators = {
        slider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: ['slider', {
            read: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ElementRef"]
          }]
        }],
        slides: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonSlides"]]
        }]
      };
      ImageModelPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-image-model',
        template: _raw_loader_image_model_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_image_model_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], ImageModelPage);
      /***/
    },

    /***/
    "EmyZ":
    /*!*************************************************************!*\
      !*** ./src/app/pages/twillio-model/twillio-model.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function EmyZ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-label {\n  font-size: 18px;\n}\n\nion-button {\n  color: #fff;\n  font-size: 18px;\n  height: 45px;\n}\n\n.pdf {\n  --background: #337ab7 !important;\n}\n\n.video {\n  --background: #5bc0de !important;\n}\n\n.label-floating.sc-ion-label-ios-h {\n  color: #5f5f5f;\n}\n\nh5 {\n  color: #000 !important;\n}\n\nion-item {\n  padding-right: 20px;\n}\n\nion-select {\n  width: 24%;\n}\n\n.padding_top {\n  padding-top: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3R3aWxsaW8tbW9kZWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFFQSxZQUFBO0FBQUo7O0FBR0E7RUFDSSxnQ0FBQTtBQUFKOztBQUdBO0VBQ0ksZ0NBQUE7QUFBSjs7QUFHQTtFQUNJLGNBQUE7QUFBSjs7QUFHQTtFQUNJLHNCQUFBO0FBQUo7O0FBR0E7RUFDSSxtQkFBQTtBQUFKOztBQUdBO0VBQ0ksVUFBQTtBQUFKOztBQUdBO0VBQ0ksaUJBQUE7QUFBSiIsImZpbGUiOiJ0d2lsbGlvLW1vZGVsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1sYWJlbCB7XG4gICAgZm9udC1zaXplOiAxOHB4O1xufVxuXG5pb24tYnV0dG9uIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgLy8gLS1iYWNrZ3JvdW5kOiAjMDc5MTk5O1xuICAgIGhlaWdodDogNDVweDtcbn1cblxuLnBkZiB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMzM3YWI3ICFpbXBvcnRhbnQ7XG59XG5cbi52aWRlbyB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjNWJjMGRlICFpbXBvcnRhbnQ7XG59XG5cbi5sYWJlbC1mbG9hdGluZy5zYy1pb24tbGFiZWwtaW9zLWgge1xuICAgIGNvbG9yOiAjNWY1ZjVmO1xufVxuXG5oNSB7XG4gICAgY29sb3I6ICMwMDAgIWltcG9ydGFudDtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG5cbmlvbi1zZWxlY3Qge1xuICAgIHdpZHRoOiAyNCU7XG59XG5cbi5wYWRkaW5nX3RvcCB7XG4gICAgcGFkZGluZy10b3A6IDI1cHg7XG59Il19 */";
      /***/
    },

    /***/
    "bHg0":
    /*!***********************************************************!*\
      !*** ./src/app/pages/twillio-model/twillio-model.page.ts ***!
      \***********************************************************/

    /*! exports provided: TwillioModelPage */

    /***/
    function bHg0(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TwillioModelPage", function () {
        return TwillioModelPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_twillio_model_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./twillio-model.page.html */
      "nr2P");
      /* harmony import */


      var _twillio_model_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./twillio-model.page.scss */
      "EmyZ");
      /* harmony import */


      var _image_model_image_model_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../image-model/image-model.page */
      "0dez");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var TwillioModelPage = /*#__PURE__*/function () {
        function TwillioModelPage(modalController, navParams, router) {
          _classCallCheck(this, TwillioModelPage);

          this.modalController = modalController;
          this.router = router;
          this.imageArray = navParams.get('imageArray');
          this.videoLink = navParams.get('videoLink');
          console.log("imageArray : ", this.imageArray);
          console.log("videoLink : ", this.videoLink);
        }

        _createClass(TwillioModelPage, [{
          key: "playVideo",
          value: function playVideo() {
            HKVideoPlayer.play(this.videoLink);
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "countLength",
          value: function countLength(value) {
            var tempArray = value.toString();
            var length = tempArray.length;
            return length;
          }
        }, {
          key: "validation",
          value: function validation() {
            if (this.account_SID && this.auth_Token && this.twilio_Number) {}
          }
        }, {
          key: "update",
          value: function update() {
            console.log(this.account_SID);
            console.log(this.auth_Token);
            console.log(this.twilio_Number);

            if (this.countLength(this.twilio_Number) < 10) {
              alert("plase add min or max 10 digits Twilio number");
            } else {
              var Twilio = +1 + this.twilio_Number;
              console.log(Twilio);
            }
          }
        }, {
          key: "onKeyUp",
          value: function onKeyUp(event) {
            var newValue = event.target.value; // console.log(event.target.value);

            var regExp = new RegExp('^([0-9_\\-]+)$');

            if (!regExp.test(newValue)) {
              event.target.value = newValue.slice(11, -1);
            }
          }
        }, {
          key: "openImage",
          value: function openImage() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var modal;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.modalController.create({
                        component: _image_model_image_model_page__WEBPACK_IMPORTED_MODULE_3__["ImageModelPage"],
                        componentProps: {
                          imageArray: this.imageArray
                        }
                      });

                    case 2:
                      modal = _context.sent;
                      modal.onDidDismiss().then(function (data) {
                        console.log(data);
                      });
                      _context.next = 6;
                      return modal.present();

                    case 6:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "back",
          value: function back() {
            this.modalController.dismiss();
          }
        }]);

        return TwillioModelPage;
      }();

      TwillioModelPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavParams"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
        }];
      };

      TwillioModelPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-twillio-model',
        template: _raw_loader_twillio_model_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_twillio_model_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], TwillioModelPage);
      /***/
    },

    /***/
    "nr2P":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/twillio-model/twillio-model.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function nr2P(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"close\" (click)=\"back()\" slot=\"end\"></ion-icon>\n    <ion-title color=\"secondary\">Setup Twilio</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-label>\n          Set up account at twilio.com and follow instruction document\n        </ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-card>\n      <ion-row>\n        <ion-col>\n          <h5>\n            Need help <ion-icon name=\"help-circle-outline\"></ion-icon>\n          </h5>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-button class=\"pdf\" expand=\"block\" (click)=\"openImage()\">\n            Text Instructions\n          </ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-button class=\"video\" expand=\"block\" (click)=\"playVideo()\">\n            Video Tutorial\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </ion-grid>\n  <ion-item>\n    <ion-label position=\"floating\">Account SID</ion-label>\n    <ion-input [(ngModel)]=\"account_SID\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"floating\">Auth Token</ion-label>\n    <ion-input [(ngModel)]=\"auth_Token\"></ion-input>\n  </ion-item>\n  <ion-item class=\"padding_top\">\n    <ion-label>+1</ion-label>\n    <ion-input [(ngModel)]=\"twilio_Number\" placeholder=\"10 digits Twilio number e.g 4124388691\" (keyup)=\"onKeyUp($event)\" type=\"tel\" minlength=\"10\" maxlength=\"10\"></ion-input>\n  </ion-item>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-button expand=\"block\" (click)=\"update()\">\n          Update\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "rbJe":
    /*!*********************************************************!*\
      !*** ./src/app/pages/image-model/image-model.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function rbJe(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".close-fake {\n  --background: transparent;\n  margin-top: 20px;\n  /* Status bar height on iOS 10 */\n  margin-top: constant(safe-area-inset-top);\n  /* Status bar height on iOS 11.0 */\n  margin-top: env(safe-area-inset-top);\n}\n.close-fake ion-icon {\n  font-size: 22px;\n  color: #ffffff;\n}\nion-content {\n  --background: rgba(0, 0, 0, 0.84);\n}\nion-slides {\n  height: 70%;\n}\n.swiper-slide img {\n  width: 100%;\n  height: 100vw;\n  width: 100%;\n  -o-object-fit: contain;\n  object-fit: contain;\n  -o-object-fit: cover;\n  object-fit: cover;\n}\n.swiper-slide {\n  display: block;\n}\np {\n  color: #ffffff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2ltYWdlLW1vZGVsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQ0FBQTtFQUNBLHlDQUFBO0VBQ0Esa0NBQUE7RUFDQSxvQ0FBQTtBQUNGO0FBQ0U7RUFDRSxlQUFBO0VBQ0EsY0FBQTtBQUNKO0FBS0E7RUFDRSxpQ0FBQTtBQUZGO0FBTUE7RUFDRSxXQUFBO0FBSEY7QUFNQTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLGlCQUFBO0FBSEY7QUFNQTtFQUNFLGNBQUE7QUFIRjtBQU1BO0VBQ0UsY0FBQTtBQUhGIiwiZmlsZSI6ImltYWdlLW1vZGVsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jbG9zZS1mYWtlIHtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgLyogU3RhdHVzIGJhciBoZWlnaHQgb24gaU9TIDEwICovXG4gIG1hcmdpbi10b3A6IGNvbnN0YW50KHNhZmUtYXJlYS1pbnNldC10b3ApO1xuICAvKiBTdGF0dXMgYmFyIGhlaWdodCBvbiBpT1MgMTEuMCAqL1xuICBtYXJnaW4tdG9wOiBlbnYoc2FmZS1hcmVhLWluc2V0LXRvcCk7XG5cbiAgaW9uLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMjJweDtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgfVxuXG4gIC8vIG1hcmdpbi10b3A6IDQwcHg7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuODQpO1xuICAvLyAtLWJhY2tncm91bmQ6IHJnYig1NiA1NiA1Nik7XG59XG5cbmlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDcwJTtcbn1cblxuLnN3aXBlci1zbGlkZSBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDB2dztcbiAgd2lkdGg6IDEwMCU7XG4gIC1vLW9iamVjdC1maXQ6IGNvbnRhaW47XG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XG4gIC1vLW9iamVjdC1maXQ6IGNvdmVyO1xuICBvYmplY3QtZml0OiBjb3Zlcjtcbn1cblxuLnN3aXBlci1zbGlkZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG5we1xuICBjb2xvcjogI2ZmZmZmZjtcbn0iXX0= */";
      /***/
    },

    /***/
    "rgys":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/image-model/image-model.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function rgys(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content fullscreen>\n  <ion-item class=\"close-fake\" lines=\"none\" text-center>\n    <ion-button (click)=\"dismiss()\" fill=\"clear\" color=\"light\">\n      <ion-icon name=\"close\" slot=\"start\"></ion-icon>\n      \n    </ion-button>\n\n    <!-- <ion-button (click)=\"zoom(true)\" fill=\"clear\" color=\"light\">\n      <ion-icon name=\"add\" slot=\"start\"></ion-icon>\n      in\n    </ion-button>\n\n    <ion-button (click)=\"zoom(false)\" fill=\"clear\" color=\"light\">\n      <ion-icon name=\"remove\" slot=\"start\"></ion-icon>\n      out\n    </ion-button> -->\n  </ion-item>\n\n  <ion-slides [options]=\"sliderOpts\" #slider>\n    <ion-slide *ngFor=\"let img of image\">\n      <p class=\"ion-padding-horizontal--\">{{img.title}}</p>\n      <div class=\"swiper-zoom-container slide\">\n        <img [src]=\"img?.imageUrl\">\n      </div>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=default~pages-twillio-model-twillio-model-module~tab4-tab4-module-es5.js.map