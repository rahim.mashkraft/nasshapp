(function () {
  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-import-contacts-import-contacts-module"], {
    /***/
    "3QfB":
    /*!*************************************************************************!*\
      !*** ./src/app/pages/import-contacts/import-contacts-routing.module.ts ***!
      \*************************************************************************/

    /*! exports provided: ImportContactsPageRoutingModule */

    /***/
    function QfB(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImportContactsPageRoutingModule", function () {
        return ImportContactsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _import_contacts_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./import-contacts.page */
      "Wva4");

      var routes = [{
        path: '',
        component: _import_contacts_page__WEBPACK_IMPORTED_MODULE_3__["ImportContactsPage"]
      }];

      var ImportContactsPageRoutingModule = function ImportContactsPageRoutingModule() {
        _classCallCheck(this, ImportContactsPageRoutingModule);
      };

      ImportContactsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ImportContactsPageRoutingModule);
      /***/
    },

    /***/
    "5dVO":
    /*!********************************************!*\
      !*** ./src/app/services/loader.service.ts ***!
      \********************************************/

    /*! exports provided: LoaderService */

    /***/
    function dVO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoaderService", function () {
        return LoaderService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var LoaderService = /*#__PURE__*/function () {
        function LoaderService(loadingCtrl) {
          _classCallCheck(this, LoaderService);

          this.loadingCtrl = loadingCtrl;
        }

        _createClass(LoaderService, [{
          key: "showLoader",
          value: function showLoader() {
            this.isBusy = true; // this.loaderToShow = this.loadingCtrl.create({
            //   message: 'Please Wait..'
            // }).then((res) => {
            //   res.present();
            //   // res.onDidDismiss().then((dis) => {
            //   //    console.log('Loading dismissed!',dis);
            //   // });
            // });
            // // this.hideLoader();
          }
        }, {
          key: "hideLoader",
          value: function hideLoader() {
            // setTimeout(()=>{
            //   this.loadingCtrl.dismiss();
            // },100)
            this.isBusy = false;
          }
        }]);

        return LoaderService;
      }();

      LoaderService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoaderService);
      /***/
    },

    /***/
    "7chK":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/import-contacts/import-contacts.module.ts ***!
      \*****************************************************************/

    /*! exports provided: ImportContactsPageModule */

    /***/
    function chK(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImportContactsPageModule", function () {
        return ImportContactsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _import_contacts_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./import-contacts-routing.module */
      "3QfB");
      /* harmony import */


      var _import_contacts_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./import-contacts.page */
      "Wva4");
      /* harmony import */


      var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/modules/shared/shared.module */
      "FpXt");

      var ImportContactsPageModule = function ImportContactsPageModule() {
        _classCallCheck(this, ImportContactsPageModule);
      };

      ImportContactsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _import_contacts_routing_module__WEBPACK_IMPORTED_MODULE_5__["ImportContactsPageRoutingModule"], src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_import_contacts_page__WEBPACK_IMPORTED_MODULE_6__["ImportContactsPage"]]
      })], ImportContactsPageModule);
      /***/
    },

    /***/
    "KQl+":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/import-contacts/import-contacts.page.scss ***!
      \*****************************************************************/

    /*! exports provided: default */

    /***/
    function KQl(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".icon_style {\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n  font-size: 50px;\n  padding: 12px;\n  border-radius: 50%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2ltcG9ydC1jb250YWN0cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw0Q0FBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QUFDSiIsImZpbGUiOiJpbXBvcnQtY29udGFjdHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmljb25fc3R5bGUge1xuICAgIGJveC1zaGFkb3c6IHJnYigwIDAgMCAvIDEyJSkgMHB4IDRweCAxNnB4O1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICBwYWRkaW5nOiAxMnB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbn0iXX0= */";
      /***/
    },

    /***/
    "VV+B":
    /*!*******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/import-contacts/import-contacts.page.html ***!
      \*******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function VVB(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">Import Contacts</ion-title>\n    <ion-icon slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" routerLink=\"/tabs/tab1\"></ion-icon>\n  </ion-toolbar>\n</ion-header> -->\n<header title=\"Import Contacts\"></header>\n<ion-content>\n  \n  <!-- <ion-button id=\"square\" *ngIf=\"!isCheck\" (click)=\"loadContacts()\">Load Contact</ion-button> -->\n  <div *ngIf=\"!isCheck\" class=\"vertically_align_center\">\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"12\" class=\"ion-text-center\" (click)=\"loadContacts()\">\n            <ion-icon class=\"icon_style\" name=\"code-download-outline\" #loadingIcon></ion-icon>\n        </ion-col>\n        <ion-col size=\"12\" class=\"ion-text-center\">\n          <ion-label class=\"heading_font\"><strong>Load Contacts</strong></ion-label>\n          <p>Click on above <ion-icon style=\"font-size: 22px;\" name=\"code-download-outline\"></ion-icon> to load contacts from your phone in a list and perform selection for importing.</p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <!-- <ion-list *ngIf=\"isCheck\">\n    <ion-item>\n      <ion-label><strong>Select All</strong></ion-label>\n      <ion-checkbox slot=\"start\" \n      [(ngModel)]=\"masterCheck\"\n      [indeterminate]=\"isIndeterminate\"\n        (click)=\"checkMaster($event)\"></ion-checkbox>\n    </ion-item>\n  </ion-list>\n  <ion-list *ngIf=\"isCheck\">\n    <ion-item *ngFor=\"let item of checkBoxList\">\n      <ion-row>\n        <ion-col size=\"12\"><strong>{{item.value}}</strong></ion-col>\n        <ion-col size=\"12\">234213412</ion-col>\n        <ion-col size=\"12\">sagar@gmail.com</ion-col>\n      </ion-row>\n      <ion-checkbox slot=\"start\" \n      [(ngModel)]=\"item.isChecked\" \n      (ionChange)=\"checkEvent()\"></ion-checkbox>\n    </ion-item>\n  </ion-list> -->\n\n  <ion-searchbar *ngIf=\"isCheck\" type=\"text\" debounce=\"500\" \n  placeholder=\"Search by Name\"\n  (ionClear)=\"claerSearcher()\"\n  (ionChange)=\"getItems($event)\"></ion-searchbar>\n  <!-- <ion-list *ngIf=\"isItemAvailable\">\n      <ion-item *ngFor=\"let item of items\">{{ item }}</ion-item>\n  </ion-list> -->\n\n  <ion-list *ngIf=\"isCheck\">\n    <ion-item>\n      <ion-label><strong>Select All</strong></ion-label>\n      <ion-checkbox slot=\"start\" [(ngModel)]=\"masterCheck\" [indeterminate]=\"isIndeterminate\"\n        (click)=\"checkMaster()\"></ion-checkbox>\n    </ion-item>\n  </ion-list>\n\n  <ion-virtual-scroll *ngIf=\"isCheck\" [items]=\"myContacts\">\n    <ion-item *virtualItem=\"let contact\">\n      <ion-row>\n        <ion-col *ngIf=\"contact._objectInstance?.displayName || contact._objectInstance?.name?.givenName || contact._objectInstance?.name?.familyName\" size=\"12\">\n          <strong>{{contact._objectInstance?.displayName || contact._objectInstance?.name?.givenName || contact._objectInstance?.name?.familyName}}</strong></ion-col>\n        <ion-col *ngIf=\"contact._objectInstance?.phoneNumbers\" size=\"12\">\n          {{contact._objectInstance?.phoneNumbers[0]?.value}}</ion-col>\n        <ion-col *ngIf=\"contact._objectInstance?.emails\" size=\"12\">{{contact._objectInstance?.emails[0]?.value}}</ion-col>\n        <ion-col *ngIf=\"contact._objectInstance?.ims\" size=\"12\">{{contact._objectInstance?.ims[0]?.value}}</ion-col>\n      </ion-row>\n      <ion-checkbox slot=\"start\" [(ngModel)]=\"contact.isChecked\" (ionChange)=\"checkEvent()\"></ion-checkbox>\n    </ion-item>\n  </ion-virtual-scroll>\n  <!-- <ion-list *ngIf=\"isCheck\">\n    <ion-item *ngFor=\"let contact of myContacts\">\n        <ion-row>\n          <ion-col *ngIf=\"contact._objectInstance?.displayName || contact._objectInstance?.name?.givenName || contact._objectInstance?.name?.familyName\" size=\"12\">\n            <strong>{{contact._objectInstance?.displayName || contact._objectInstance?.name?.givenName || contact._objectInstance?.name?.familyName}}</strong></ion-col>\n          <ion-col *ngIf=\"contact._objectInstance?.phoneNumbers\" size=\"12\">\n            {{contact._objectInstance?.phoneNumbers[0]?.value}}</ion-col>\n          <ion-col *ngIf=\"contact._objectInstance?.emails\" size=\"12\">{{contact._objectInstance?.emails[0]?.value}}</ion-col>\n          <ion-col *ngIf=\"contact._objectInstance?.ims\" size=\"12\">{{contact._objectInstance?.ims[0]?.value}}</ion-col>\n        </ion-row>\n        <ion-checkbox slot=\"start\" [(ngModel)]=\"contact.isChecked\" (ionChange)=\"checkEvent()\"></ion-checkbox>\n    </ion-item>\n  </ion-list> -->\n\n  <!-- INFINITE SCROLL -->\n  <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadMoreData(1, $event)\" [disabled]=\"result.length > array_index\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more items...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n<ion-footer>\n  <ion-row *ngIf=\"isCheck\" padding-vertical>\n    <ion-col margin-left margin-right no-padding>\n      <ion-button expand=\"full\" (click)=\"selectGroup()\">Next</ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>";
      /***/
    },

    /***/
    "Wva4":
    /*!***************************************************************!*\
      !*** ./src/app/pages/import-contacts/import-contacts.page.ts ***!
      \***************************************************************/

    /*! exports provided: ImportContactsPage */

    /***/
    function Wva4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImportContactsPage", function () {
        return ImportContactsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_import_contacts_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./import-contacts.page.html */
      "VV+B");
      /* harmony import */


      var _import_contacts_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./import-contacts.page.scss */
      "KQl+");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/contacts/ngx */
      "TzAO");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/loader.service */
      "5dVO");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic/storage */
      "e8h1");
      /* harmony import */


      var _model_model_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../model/model.page */
      "9AS+");

      var ImportContactsPage = /*#__PURE__*/function () {
        function ImportContactsPage(navCtrl, contacts, router, loaderService, alertController, animationCtrl, storage, modalController) {
          var _this = this;

          _classCallCheck(this, ImportContactsPage);

          this.navCtrl = navCtrl;
          this.contacts = contacts;
          this.router = router;
          this.loaderService = loaderService;
          this.alertController = alertController;
          this.animationCtrl = animationCtrl;
          this.storage = storage;
          this.modalController = modalController;
          this.isChecked = false; // myContacts: Contact[] = [];

          this.myContacts = [];
          this.taskListCustom = [];
          this.array_index = 0;
          this.result = [];
          this.final_array = [];
          this.isCheck = false; // Declare the variable (in this case and initialize it with false)

          this.isItemAvailable = false;
          this.items = [];
          this.isCheck = false;
          setTimeout(function () {
            _this.startLoad();
          }, 300);
        }

        _createClass(ImportContactsPage, [{
          key: "startLoad",
          value: function startLoad() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var loadingAnimation;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      loadingAnimation = this.animationCtrl.create('loading-animation').addElement(this.loadingIcon.nativeElement).duration(700).iterations(3).fromTo('transform', 'translateX(0px)', 'translateY(25px)').fromTo('transform', 'translateY(25px)', 'translateX(0px)'); // .fromTo('transform', 'rotate(0deg)', 'rotate(360deg)');
                      // Don't forget to start the animation!

                      loadingAnimation.play();

                    case 2:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "checkMaster",
          value: function checkMaster() {
            var _this2 = this;

            setTimeout(function () {
              _this2.myContacts.forEach(function (obj) {
                obj.isChecked = _this2.masterCheck;
              });
            });
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.isCheck = false;
          }
        }, {
          key: "getCheckedvalue",
          value: function getCheckedvalue() {
            this.checkedItems = this.myContacts.filter(function (value) {
              return value.isChecked;
            });
            console.log(this.checkedItems);
          }
        }, {
          key: "checkEvent",
          value: function checkEvent() {
            var totalItems = this.myContacts.length;
            var checked = 0;
            this.myContacts.map(function (obj) {
              if (obj.isChecked) checked++;
            });

            if (checked > 0 && checked < totalItems) {
              //If even one item is checked but not all
              this.isIndeterminate = true;
              this.masterCheck = false;
            } else if (checked == totalItems) {
              //If all are checked
              this.masterCheck = true;
              this.isIndeterminate = false;
            } else {
              //If none is checked
              this.isIndeterminate = false;
              this.masterCheck = false;
            }

            this.getCheckedvalue();
          }
        }, {
          key: "onSuccess",
          value: function onSuccess(contacts) {
            alert('Found ' + contacts.length + ' contacts.');
          }
        }, {
          key: "onError",
          value: function onError(contactError) {
            alert('onError!');
          }
        }, {
          key: "loadContacts",
          value: function loadContacts() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this3 = this;

              var modal;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      console.log("loadContacts click"); // this.alertService.contactList('Are you sure, you want to get you contacts?')

                      _context2.next = 3;
                      return this.modalController.create({
                        component: _model_model_page__WEBPACK_IMPORTED_MODULE_9__["ModelPage"],
                        cssClass: 'my-custom-modal-class'
                      });

                    case 3:
                      modal = _context2.sent;
                      modal.onDidDismiss().then(function (data) {
                        console.log(data);

                        if (data.data == true) {
                          _this3.loaderService.showLoader();

                          _this3.import_contact();
                        } else {
                          console.log("fdfubadfafdkas");
                        }
                      });
                      _context2.next = 7;
                      return modal.present();

                    case 7:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "import_contact",
          value: function import_contact() {
            var _this4 = this;

            this.isCheck = true;
            var options = {
              filter: '',
              multiple: true,
              hasPhoneNumber: true
            };
            this.contacts.find(['displayName', 'name', 'emails', 'phoneNumbers'], options).then(function (contacts) {
              // this.contacts.find(['displayName', 'name', 'phoneNumbers', 'emails'], options).then((contacts) => {
              console.log("Contacts : ", contacts);
              _this4.myContacts = contacts;
              _this4.myContacts_backup = _this4.myContacts; // this.myContacts.sort()
              // this.chunks(this.myContacts)
              // this.storage.set("contacts", contacts)

              setTimeout(function () {
                _this4.loaderService.hideLoader();
              }, 500); // alert(this.myContacts)
              // alert(JSON.stringify(this.myContacts))
              // console.log("this.myContacts : ", this.myContacts)
            });
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "initializeItems",
          value: function initializeItems() {
            this.items = ["Ram", "gopi", "dravid", "wravid", "eravid", "rravid", "travid", "gravid"];
          }
        }, {
          key: "getItems",
          value: function getItems(ev) {
            // Reset items back to all of the items
            // set val to the value of the searchbar
            var val = ev.target.value; // if the value is an empty string don't filter the items

            if (val && val.trim() !== '') {
              this.isItemAvailable = true;
              this.myContacts = this.myContacts_backup.filter(function (item) {
                console.log("item : ", item);
                return item._objectInstance.name.givenName.toLowerCase().indexOf(val.toLowerCase()) > -1;
              });
            } else {
              this.isItemAvailable = false;
            }
          }
        }, {
          key: "claerSearcher",
          value: function claerSearcher() {
            console.log("claerSearcher");
            this.myContacts = this.myContacts;
          }
        }, {
          key: "ConvertToInt",
          value: function ConvertToInt(currentPage) {
            return parseInt(currentPage);
          }
        }, {
          key: "loadMoreData",
          value: function loadMoreData(value, event) {
            console.log(this.result.length);
            this.array_index = this.ConvertToInt(this.array_index) + this.ConvertToInt(value);
            console.log(this.array_index);

            if (this.result.length > this.array_index) {
              this.final_array = [].concat(_toConsumableArray(this.final_array), _toConsumableArray(this.result[this.array_index]));
              console.log(this.final_array);

              if (event) {
                event.target.complete();
              }
            } else {
              console.log("no more data");

              if (event) {
                event.target.complete();
              }
            }
          }
        }, {
          key: "selectGroup",
          value: function selectGroup() {
            var navigationExtras = {
              state: {
                checkedItems: this.checkedItems
              }
            }; // this.router.navigate(['post-add-second', navigationExtras]);

            this.router.navigate(['tabs/select-group'], navigationExtras); // this.navCtrl.navigateForward(['select-group'])
          }
        }, {
          key: "back",
          value: function back() {
            this.navCtrl.back();
          }
        }]);

        return ImportContactsPage;
      }();

      ImportContactsPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"]
        }, {
          type: _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_5__["Contacts"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_7__["LoaderService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AnimationController"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"]
        }];
      };

      ImportContactsPage.propDecorators = {
        infiniteScroll: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonInfiniteScroll"]]
        }],
        loadingIcon: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: ['loadingIcon', {
            read: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ElementRef"]
          }]
        }]
      };
      ImportContactsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-import-contacts',
        template: _raw_loader_import_contacts_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_import_contacts_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], ImportContactsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-import-contacts-import-contacts-module-es5.js.map