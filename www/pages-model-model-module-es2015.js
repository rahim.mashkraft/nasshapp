(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-model-model-module"],{

/***/ "3EkQ":
/*!*****************************************************!*\
  !*** ./src/app/pages/model/model-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: ModelPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModelPageRoutingModule", function() { return ModelPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _model_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./model.page */ "9AS+");




const routes = [
    {
        path: '',
        component: _model_page__WEBPACK_IMPORTED_MODULE_3__["ModelPage"]
    }
];
let ModelPageRoutingModule = class ModelPageRoutingModule {
};
ModelPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ModelPageRoutingModule);



/***/ }),

/***/ "8q6H":
/*!*********************************************!*\
  !*** ./src/app/pages/model/model.module.ts ***!
  \*********************************************/
/*! exports provided: ModelPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModelPageModule", function() { return ModelPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _model_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./model-routing.module */ "3EkQ");
/* harmony import */ var _model_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./model.page */ "9AS+");







let ModelPageModule = class ModelPageModule {
};
ModelPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _model_routing_module__WEBPACK_IMPORTED_MODULE_5__["ModelPageRoutingModule"]
        ],
        declarations: [_model_page__WEBPACK_IMPORTED_MODULE_6__["ModelPage"]]
    })
], ModelPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-model-model-module-es2015.js.map