(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-select-group-select-group-module"],{

/***/ "2g2N":
/*!*******************************************!*\
  !*** ./src/app/services/toast.service.ts ***!
  \*******************************************/
/*! exports provided: ToastService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastService", function() { return ToastService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let ToastService = class ToastService {
    constructor(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    showToast(message = 'Successfully Logged in!') {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                cssClass: 'bg-toast',
                message: message,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }
    showToastUpdateProfile(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                cssClass: 'bg-toast',
                message: message,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }
};
ToastService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
ToastService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ToastService);



/***/ }),

/***/ "5Rhe":
/*!***********************************************************!*\
  !*** ./src/app/pages/select-group/select-group.module.ts ***!
  \***********************************************************/
/*! exports provided: SelectGroupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectGroupPageModule", function() { return SelectGroupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _select_group_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-group-routing.module */ "Uc+a");
/* harmony import */ var _select_group_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-group.page */ "KOIj");







let SelectGroupPageModule = class SelectGroupPageModule {
};
SelectGroupPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_group_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectGroupPageRoutingModule"]
        ],
        declarations: [_select_group_page__WEBPACK_IMPORTED_MODULE_6__["SelectGroupPage"]]
    })
], SelectGroupPageModule);



/***/ }),

/***/ "5dVO":
/*!********************************************!*\
  !*** ./src/app/services/loader.service.ts ***!
  \********************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let LoaderService = class LoaderService {
    constructor(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    showLoader() {
        this.isBusy = true;
        // this.loaderToShow = this.loadingCtrl.create({
        //   message: 'Please Wait..'
        // }).then((res) => {
        //   res.present();
        //   // res.onDidDismiss().then((dis) => {
        //   //    console.log('Loading dismissed!',dis);
        //   // });
        // });
        // // this.hideLoader();
    }
    hideLoader() {
        // setTimeout(()=>{
        //   this.loadingCtrl.dismiss();
        // },100)
        this.isBusy = false;
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ "KOIj":
/*!*********************************************************!*\
  !*** ./src/app/pages/select-group/select-group.page.ts ***!
  \*********************************************************/
/*! exports provided: SelectGroupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectGroupPage", function() { return SelectGroupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_select_group_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./select-group.page.html */ "rGps");
/* harmony import */ var _select_group_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./select-group.page.scss */ "OdVg");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/contact-groups.service */ "rtEV");
/* harmony import */ var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/loader.service */ "5dVO");
/* harmony import */ var src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/toast.service */ "2g2N");









let SelectGroupPage = class SelectGroupPage {
    constructor(router, route, navCtrl, contactGroupService, loaderService, toastService) {
        this.router = router;
        this.route = route;
        this.navCtrl = navCtrl;
        this.contactGroupService = contactGroupService;
        this.loaderService = loaderService;
        this.toastService = toastService;
        this.isChecked = false;
        this.postData = [];
        this.radio_check = 1;
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.checkedItems = this.router.getCurrentNavigation().extras.state.checkedItems;
                console.log("this.checkedItems : ", this.checkedItems);
                this.checkedItems.forEach(obj => {
                    console.log(obj);
                });
                for (let i = 0; i < this.checkedItems.length; i++) {
                    let obj;
                    obj = {
                        firstname: '',
                        lastname: '',
                        email: '',
                        phone: 0
                    };
                    obj.firstname = this.checkedItems[i]._objectInstance.name.givenName;
                    obj.lastname = this.checkedItems[i]._objectInstance.name.familyName;
                    if (this.checkedItems[i]._objectInstance.emails) {
                        obj.email = this.checkedItems[i]._objectInstance.emails[0].value;
                    }
                    if (this.checkedItems[i]._objectInstance.ims) {
                        obj.email = this.checkedItems[i]._objectInstance.ims[0].value;
                    }
                    if (this.checkedItems[i]._objectInstance.phoneNumbers) {
                        obj.phone = this.checkedItems[i]._objectInstance.phoneNumbers[0].value;
                    }
                    this.postData.push(obj);
                }
                console.log("this.postData : ", this.postData);
            }
        });
    }
    ngOnInit() {
        this.getContactGroups();
    }
    getContactGroups() {
        this.contactGroupService.getContactGroups()
            .then((res) => {
            console.log(res);
            if (res.status !== false) {
                this.contact_groups = res.contactgroup;
            }
        });
    }
    submit() {
        this.loaderService.showLoader();
        let jj = 0;
        let formData = new FormData();
        console.log("this.radio_check : ", this.radio_check);
        for (let data of this.postData) {
            this.responseData = data;
            console.log(this.responseData);
            if (this.responseData.firstname == undefined) {
                formData.append("postdata[" + jj + "][firstname]", null);
            }
            else {
                formData.append("postdata[" + jj + "][firstname]", this.responseData.firstname);
            }
            if (this.responseData.lastname == undefined) {
                formData.append("postdata[" + jj + "][lastname]", null);
            }
            else {
                formData.append("postdata[" + jj + "][lastname]", this.responseData.lastname);
            }
            if (this.responseData.email == undefined) {
                formData.append("postdata[" + jj + "][email]", null);
            }
            else {
                formData.append("postdata[" + jj + "][email]", this.responseData.email);
            }
            formData.append("postdata[" + jj + "][phone]", this.responseData.phone);
            jj++;
        }
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("group_id", this.radio_check);
        this.contactGroupService.contactsImport(formData).then((res) => {
            console.log(res);
            if (res.status !== false) {
                this.toastService.showToast('Contacts successfully imported form mobile to your account.');
                this.loaderService.hideLoader();
                // this.back();
                setTimeout(() => {
                    this.navCtrl.navigateRoot('/tabs');
                }, 500);
                // this.contact_groups = res.contactgroup
            }
            else {
                this.loaderService.hideLoader();
            }
        });
    }
    back() {
        this.navCtrl.back();
    }
};
SelectGroupPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_6__["ContactGroupsService"] },
    { type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_7__["LoaderService"] },
    { type: src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"] }
];
SelectGroupPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-group',
        template: _raw_loader_select_group_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_group_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SelectGroupPage);



/***/ }),

/***/ "OdVg":
/*!***********************************************************!*\
  !*** ./src/app/pages/select-group/select-group.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzZWxlY3QtZ3JvdXAucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "Uc+a":
/*!*******************************************************************!*\
  !*** ./src/app/pages/select-group/select-group-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: SelectGroupPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectGroupPageRoutingModule", function() { return SelectGroupPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _select_group_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-group.page */ "KOIj");




const routes = [
    {
        path: '',
        component: _select_group_page__WEBPACK_IMPORTED_MODULE_3__["SelectGroupPage"]
    }
];
let SelectGroupPageRoutingModule = class SelectGroupPageRoutingModule {
};
SelectGroupPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectGroupPageRoutingModule);



/***/ }),

/***/ "rGps":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/select-group/select-group.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">Select Group</ion-title>\n    <ion-icon slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" routerLink=\"/tabs/tab1\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list mode=\"md\">\n    <ion-radio-group value=\"biff\" [(ngModel)]=\"radio_check\">\n  \n      <ion-item *ngFor=\"let item of contact_groups\">\n        <ion-label>{{item.title}}</ion-label>\n        <ion-radio slot=\"start\" value=\"{{item.id}}\"></ion-radio>\n      </ion-item>\n  \n    </ion-radio-group>\n  </ion-list>\n</ion-content>\n<ion-footer>\n  <ion-row padding-vertical>\n    <ion-col margin-left margin-right no-padding>\n      <ion-button expand=\"full\" (click)=\"submit()\">Submit</ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>");

/***/ }),

/***/ "rtEV":
/*!****************************************************!*\
  !*** ./src/app/services/contact-groups.service.ts ***!
  \****************************************************/
/*! exports provided: ContactGroupsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactGroupsService", function() { return ContactGroupsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");




let ContactGroupsService = class ContactGroupsService {
    constructor(http) {
        this.http = http;
        this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    getContactGroups() {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'contactgroups.php');
            this.http.get(this.apiUrl + 'contactgroups.php')
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
    getContactGroupsList(formData) {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'contactgroupslist.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'contactgroupslist.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
    contactsImport(formData) {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'contactsimport.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'contactsimport.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
    getContacts(formData) {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'contacts.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'contacts.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
};
ContactGroupsService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
ContactGroupsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], ContactGroupsService);



/***/ })

}]);
//# sourceMappingURL=pages-select-group-select-group-module-es2015.js.map