(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-success-model-success-model-module"], {
    /***/
    "9jCh":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/success-model/success-model-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: SuccessModelPageRoutingModule */

    /***/
    function jCh(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SuccessModelPageRoutingModule", function () {
        return SuccessModelPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _success_model_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./success-model.page */
      "hnmG");

      var routes = [{
        path: '',
        component: _success_model_page__WEBPACK_IMPORTED_MODULE_3__["SuccessModelPage"]
      }];

      var SuccessModelPageRoutingModule = function SuccessModelPageRoutingModule() {
        _classCallCheck(this, SuccessModelPageRoutingModule);
      };

      SuccessModelPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SuccessModelPageRoutingModule);
      /***/
    },

    /***/
    "sHii":
    /*!*************************************************************!*\
      !*** ./src/app/pages/success-model/success-model.module.ts ***!
      \*************************************************************/

    /*! exports provided: SuccessModelPageModule */

    /***/
    function sHii(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SuccessModelPageModule", function () {
        return SuccessModelPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _success_model_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./success-model-routing.module */
      "9jCh");
      /* harmony import */


      var _success_model_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./success-model.page */
      "hnmG");

      var SuccessModelPageModule = function SuccessModelPageModule() {
        _classCallCheck(this, SuccessModelPageModule);
      };

      SuccessModelPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _success_model_routing_module__WEBPACK_IMPORTED_MODULE_5__["SuccessModelPageRoutingModule"]],
        declarations: [_success_model_page__WEBPACK_IMPORTED_MODULE_6__["SuccessModelPage"]]
      })], SuccessModelPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-success-model-success-model-module-es5.js.map