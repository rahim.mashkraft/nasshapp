// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: false,

  // imageUrl: 'http:',
  // apiUrl: 'http://umrtariq.prospectingmagic.com/admin/appapi/',
  // profileImageUrl: 'http://umrtariq.prospectingmagic.com/'

  imageUrl: 'https:',
  apiUrl: 'https://mindandheartuniversity.com/admin/appapi/',
  profileImageUrl: 'https://mindandheartuniversity.com/',
  auth_token: '4d409766604ad82ba5eeb96077c977c9',
  downloadurl: 'https://mindandheartuniversity.com/admin/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
