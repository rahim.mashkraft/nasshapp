import { Component, ViewChild } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController, IonInfiniteScroll, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CategoriesService } from '../services/categories.service';
import { LoaderService } from '../services/loader.service';
import { Storage } from '@ionic/storage';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  data = [];
  loading = false;
  currentPage = 1;
  lastPage = 1;
  categoriesSubscription: Subscription;
  environment;
  notificationsData = [];
  taskListCustom = [];

  array_index = 0;
  dataObj: any;
  @ViewChild('search') search : any;
  constructor(
    private router: Router,
    private categoriesService: CategoriesService,
    public loaderService: LoaderService,
    public plt: Platform,
    public storage: Storage,
    private diagnostic: Diagnostic,
    public alertController: AlertController,
  ) {


    // this.eventsService.subscribe('startContact', (data) => {
    //   console.log("eventsService.subscribe startContact : ")
    //   // let options = {
    //   //   filter: '',
    //   //   multiple: true,
    //   //   hasPhoneNumber: true
    //   // }
    //   // this.contacts.find(['*'], options).then((contacts) => {
    //   //   // this.contacts.find(['displayName', 'name', 'phoneNumbers', 'emails'], options).then((contacts) => {
    //   //   console.log("Contacts : ", contacts)
    //   //   // this.contactList = contacts
    //   //   // console.log("contactList : ", this.contactList)
    //   //   this.storage.set("contacts", contacts)
    //   //   // localStorage.setItem('contacts', this.contactList);
    //   // })
    // });
    // this.eventsService.publish("startContact");

    this.environment = environment;
    // setTimeout(() => {
    //   this.submit()
    // }, 500);
    
        // setTimeout(() => {
      
          // this.playAudio()
    // }, 1500);
    this.checkLocation();
    this.dailygoalscheck();
  }
  dailygoalscheck(){
    let formData = new FormData()
    formData.append("userid",'113')
    const obj = {
      userid: localStorage.getItem('user_id')
    };
    this.categoriesService.dailygoalscheck(formData)
    .then((res: any) => {
      console.log("res : ",res)
    });
  }
  checkLocation() {
    if (this.plt.is('cordova')) {
      this.diagnostic.getLocationAuthorizationStatus().then((status) => {
        setTimeout(() => {
          console.log("status.NOT_REQUESTED : ", this.diagnostic.permissionStatus.NOT_REQUESTED)
          console.log("status.DENIED_ALWAYS : ", this.diagnostic.permissionStatus.DENIED_ALWAYS)
          console.log("status.GRANTED : ", this.diagnostic.permissionStatus.GRANTED)
          console.log("status.GRANTED_WHEN_IN_USE : ", this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE)
        }, 100);
        // alert(status) ;

        if (status == this.diagnostic.permissionStatus.GRANTED || status == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
          this.diagnostic.isLocationEnabled().then((res) => {
            console.log("this.diagnostic.isLocationEnabled() : ", res)
            if (res == true) {
            }
          })
        } else if (status == this.diagnostic.permissionStatus.NOT_REQUESTED) {
          console.log("status == this.diagnostic.permissionStatus.NOT_REQUESTED")
        } else {
          // this.further();
        }
        console.log("status : ", status)
      })
    }
  }
  async further() {
    const alert = await this.alertController.create({
      header: 'Turn on Location Services',
      message: 'Please turn on location services',
      buttons: [
        {
          text: 'Not now',
          handler: () => {
            console.log('Cancel No');

          }
        },
        {
          text: 'Settings',
          handler: () => {
            //console.log('Cancel Yes');
            this.switchToSettings();
          }
        }
      ]
    })
    // this.currentAlert = alert;
    await alert.present();
  }
  switchToSettings() {
    this.diagnostic.switchToSettings()
      .then(() => {
        console.log("Successfully switched to Settings app");
      }).catch(e => console.error(e));
  }
  responseData
  submit() {
    // let jj = 0;
    // let formData = new FormData();
    // // console.log("this.radio_check : ", this.radio_check)
    // for (let data of this.postdata) {
    //   this.responseData = data;
    //   console.log(this.responseData)
    //   // var firstname = this.responseData.firstname;
    //   // var lastname = this.responseData.lastname;
    //   // var email = this.responseData.email;
    //   // var phone = this.responseData.phone;
    //   formData.append("postdata[" + jj + "][firstname]", this.responseData.firstname);
    //   formData.append("postdata[" + jj + "][lastname]", this.responseData.lastname);
    //   formData.append("postdata[" + jj + "][email]", this.responseData.email);
    //   formData.append("postdata[" + jj + "][phone]", this.responseData.phone);
    //   jj++;
    // }
    // formData.append("aid", localStorage.getItem('user_id'));
    // // formData.append("group_id", this.radio_check);
    // this.contactGroupService.contactsImport(formData).then(data => {

    // })
  }
  ionViewWillEnter() {
    this.currentPage = 1;
    this.fetchCategories(this.currentPage);
  }
  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.taskListCustom.length == 5) {
        event.target.disabled = true;
      }
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  fetchCategories(currentPage, event = null) {
    if (this.loading) { return; }
    if (this.categoriesSubscription) { this.categoriesSubscription.unsubscribe(); }
    this.loading = true;
    this.loaderService.showLoader()
    let formData = new FormData();
    formData.append("PageNumber", currentPage);
    this.categoriesService.getQuizCategories(formData).then(data => {
      console.log("data : ", data)
      this.dataObj = data
      this.loading = false;
      this.currentPage = this.dataObj.CurrentPage;
      this.lastPage = this.dataObj.total;
      if (this.currentPage == 1) {
        this.data = this.dataObj.categories;
        setTimeout(() => {
          this.loaderService.hideLoader()
        },500)
        // setTimeout(() => {
        //   this.eventsService.publish("startContact");

        // },1000)
      } else {
        this.data = [...this.data, ...this.dataObj.categories];
        setTimeout(() => {
          this.loaderService.hideLoader()
        },500)
      }
      if (event) {
        event.target.complete();
      }
    }).catch(err => {
          this.loaderService.hideLoader()
          this.loading = false;
      if (event) {
        event.target.complete();
      }
    })
    // this.categoriesSubscription = this.categoriesService.getQuizCategories({PageNumber: page})
    // .subscribe((res: any) => {
    //   this.loading = false;
    //   this.currentPage = res.CurrentPage;
    //   this.lastPage = res.lastPage;
    //   if (this.currentPage === 1) {
    //     this.data = res.categories;
    //   } else {
    //     this.data = [...this.data, ...res.categories];
    //   }
    //   if (event) {
    //     event.target.complete();
    //   }
    // },
    // err => {
    //   this.loading = false;
    //   if (event) {
    //     event.target.complete();
    //   }
    // });
  }
  goToSubCat(id, name) {
    let navigationExtras: NavigationExtras = {
      state: {
        name: name,
        id: id,
      }
    };
    // this.router.navigate(['post-add-second', navigationExtras]);
    this.router.navigate(['tabs/tab1/sub-categories'], navigationExtras);

    // this.navCtrl.navigateForward(['tabs/tab1/sub-categories'],navigationExtras);
  }
  ConvertToInt(currentPage) {
    return parseInt(currentPage);
  }
  loadMoreData(value, event) {
    this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value)
    this.fetchCategories(this.currentPage, event)
  }
  refreshData(event) {
    this.currentPage = 1;
    // this.data = [];
    this.fetchCategories(1, event);
  }
  notification() {
    this.router.navigate(['tabs/notification']);
  }
}
