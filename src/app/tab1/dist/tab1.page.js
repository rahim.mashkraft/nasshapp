"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.Tab1Page = void 0;
var core_1 = require("@angular/core");
var angular_1 = require("@ionic/angular");
var environment_1 = require("src/environments/environment");
var Tab1Page = /** @class */ (function () {
    function Tab1Page(router, categoriesService, loaderService, plt, storage, diagnostic, alertController) {
        // this.eventsService.subscribe('startContact', (data) => {
        //   console.log("eventsService.subscribe startContact : ")
        //   // let options = {
        //   //   filter: '',
        //   //   multiple: true,
        //   //   hasPhoneNumber: true
        //   // }
        //   // this.contacts.find(['*'], options).then((contacts) => {
        //   //   // this.contacts.find(['displayName', 'name', 'phoneNumbers', 'emails'], options).then((contacts) => {
        //   //   console.log("Contacts : ", contacts)
        //   //   // this.contactList = contacts
        //   //   // console.log("contactList : ", this.contactList)
        //   //   this.storage.set("contacts", contacts)
        //   //   // localStorage.setItem('contacts', this.contactList);
        //   // })
        // });
        // this.eventsService.publish("startContact");
        this.router = router;
        this.categoriesService = categoriesService;
        this.loaderService = loaderService;
        this.plt = plt;
        this.storage = storage;
        this.diagnostic = diagnostic;
        this.alertController = alertController;
        this.data = [];
        this.loading = false;
        this.currentPage = 1;
        this.lastPage = 1;
        this.notificationsData = [];
        this.taskListCustom = [];
        this.array_index = 0;
        this.environment = environment_1.environment;
        // setTimeout(() => {
        //   this.submit()
        // }, 500);
        // setTimeout(() => {
        // this.playAudio()
        // }, 1500);
        this.checkLocation();
        this.dailygoalscheck();
    }
    Tab1Page.prototype.dailygoalscheck = function () {
        var formData = new FormData();
        formData.append("userid", '113');
        var obj = {
            userid: localStorage.getItem('user_id')
        };
        this.categoriesService.dailygoalscheck(formData)
            .then(function (res) {
            console.log("res : ", res);
        });
    };
    Tab1Page.prototype.checkLocation = function () {
        var _this = this;
        if (this.plt.is('cordova')) {
            this.diagnostic.getLocationAuthorizationStatus().then(function (status) {
                setTimeout(function () {
                    console.log("status.NOT_REQUESTED : ", _this.diagnostic.permissionStatus.NOT_REQUESTED);
                    console.log("status.DENIED_ALWAYS : ", _this.diagnostic.permissionStatus.DENIED_ALWAYS);
                    console.log("status.GRANTED : ", _this.diagnostic.permissionStatus.GRANTED);
                    console.log("status.GRANTED_WHEN_IN_USE : ", _this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE);
                }, 100);
                // alert(status) ;
                if (status == _this.diagnostic.permissionStatus.GRANTED || status == _this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
                    _this.diagnostic.isLocationEnabled().then(function (res) {
                        console.log("this.diagnostic.isLocationEnabled() : ", res);
                        if (res == true) {
                        }
                    });
                }
                else if (status == _this.diagnostic.permissionStatus.NOT_REQUESTED) {
                    console.log("status == this.diagnostic.permissionStatus.NOT_REQUESTED");
                }
                else {
                    // this.further();
                }
                console.log("status : ", status);
            });
        }
    };
    Tab1Page.prototype.further = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Turn on Location Services',
                            message: 'Please turn on location services',
                            buttons: [
                                {
                                    text: 'Not now',
                                    handler: function () {
                                        console.log('Cancel No');
                                    }
                                },
                                {
                                    text: 'Settings',
                                    handler: function () {
                                        //console.log('Cancel Yes');
                                        _this.switchToSettings();
                                    }
                                }
                            ]
                        })
                        // this.currentAlert = alert;
                    ];
                    case 1:
                        alert = _a.sent();
                        // this.currentAlert = alert;
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        // this.currentAlert = alert;
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Tab1Page.prototype.switchToSettings = function () {
        this.diagnostic.switchToSettings()
            .then(function () {
            console.log("Successfully switched to Settings app");
        })["catch"](function (e) { return console.error(e); });
    };
    Tab1Page.prototype.submit = function () {
        // let jj = 0;
        // let formData = new FormData();
        // // console.log("this.radio_check : ", this.radio_check)
        // for (let data of this.postdata) {
        //   this.responseData = data;
        //   console.log(this.responseData)
        //   // var firstname = this.responseData.firstname;
        //   // var lastname = this.responseData.lastname;
        //   // var email = this.responseData.email;
        //   // var phone = this.responseData.phone;
        //   formData.append("postdata[" + jj + "][firstname]", this.responseData.firstname);
        //   formData.append("postdata[" + jj + "][lastname]", this.responseData.lastname);
        //   formData.append("postdata[" + jj + "][email]", this.responseData.email);
        //   formData.append("postdata[" + jj + "][phone]", this.responseData.phone);
        //   jj++;
        // }
        // formData.append("aid", localStorage.getItem('user_id'));
        // // formData.append("group_id", this.radio_check);
        // this.contactGroupService.contactsImport(formData).then(data => {
        // })
    };
    Tab1Page.prototype.ionViewWillEnter = function () {
        this.currentPage = 1;
        this.fetchCategories(this.currentPage);
    };
    Tab1Page.prototype.loadData = function (event) {
        var _this = this;
        setTimeout(function () {
            console.log('Done');
            event.target.complete();
            // App logic to determine if all data is loaded
            // and disable the infinite scroll
            if (_this.taskListCustom.length == 5) {
                event.target.disabled = true;
            }
        }, 500);
    };
    Tab1Page.prototype.toggleInfiniteScroll = function () {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    };
    Tab1Page.prototype.fetchCategories = function (currentPage, event) {
        var _this = this;
        if (event === void 0) { event = null; }
        if (this.loading) {
            return;
        }
        if (this.categoriesSubscription) {
            this.categoriesSubscription.unsubscribe();
        }
        this.loading = true;
        this.loaderService.showLoader();
        var formData = new FormData();
        formData.append("PageNumber", currentPage);
        this.categoriesService.getQuizCategories(formData).then(function (data) {
            console.log("data : ", data);
            _this.dataObj = data;
            _this.loading = false;
            _this.currentPage = _this.dataObj.CurrentPage;
            _this.lastPage = _this.dataObj.total;
            if (_this.currentPage == 1) {
                _this.data = _this.dataObj.categories;
                setTimeout(function () {
                    _this.loaderService.hideLoader();
                }, 500);
                // setTimeout(() => {
                //   this.eventsService.publish("startContact");
                // },1000)
            }
            else {
                _this.data = __spreadArrays(_this.data, _this.dataObj.categories);
                setTimeout(function () {
                    _this.loaderService.hideLoader();
                }, 500);
            }
            if (event) {
                event.target.complete();
            }
        })["catch"](function (err) {
            _this.loaderService.hideLoader();
            _this.loading = false;
            if (event) {
                event.target.complete();
            }
        });
        // this.categoriesSubscription = this.categoriesService.getQuizCategories({PageNumber: page})
        // .subscribe((res: any) => {
        //   this.loading = false;
        //   this.currentPage = res.CurrentPage;
        //   this.lastPage = res.lastPage;
        //   if (this.currentPage === 1) {
        //     this.data = res.categories;
        //   } else {
        //     this.data = [...this.data, ...res.categories];
        //   }
        //   if (event) {
        //     event.target.complete();
        //   }
        // },
        // err => {
        //   this.loading = false;
        //   if (event) {
        //     event.target.complete();
        //   }
        // });
    };
    Tab1Page.prototype.goToSubCat = function (id, name) {
        var navigationExtras = {
            state: {
                name: name,
                id: id
            }
        };
        // this.router.navigate(['post-add-second', navigationExtras]);
        this.router.navigate(['tabs/tab1/sub-categories'], navigationExtras);
        // this.navCtrl.navigateForward(['tabs/tab1/sub-categories'],navigationExtras);
    };
    Tab1Page.prototype.ConvertToInt = function (currentPage) {
        return parseInt(currentPage);
    };
    Tab1Page.prototype.loadMoreData = function (value, event) {
        this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value);
        this.fetchCategories(this.currentPage, event);
    };
    Tab1Page.prototype.refreshData = function (event) {
        this.currentPage = 1;
        // this.data = [];
        this.fetchCategories(1, event);
    };
    Tab1Page.prototype.notification = function () {
        this.router.navigate(['tabs/notification']);
    };
    __decorate([
        core_1.ViewChild(angular_1.IonInfiniteScroll)
    ], Tab1Page.prototype, "infiniteScroll");
    __decorate([
        core_1.ViewChild('search')
    ], Tab1Page.prototype, "search");
    Tab1Page = __decorate([
        core_1.Component({
            selector: 'app-tab1',
            templateUrl: 'tab1.page.html',
            styleUrls: ['tab1.page.scss']
        })
    ], Tab1Page);
    return Tab1Page;
}());
exports.Tab1Page = Tab1Page;
