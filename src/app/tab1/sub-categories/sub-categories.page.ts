import { LoaderService } from 'src/app/services/loader.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { SubCategoryService } from 'src/app/services/sub-category.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-sub-categories',
  templateUrl: './sub-categories.page.html',
  styleUrls: ['./sub-categories.page.scss'],
})
export class SubCategoriesPage implements OnInit {
  id: any;
  loading = false;
  currentPage = 1;
  lastPage = 1;
  data = [];
  videos_data = [];
  name = '';
  environment;
  constructor(
    public navCtrl: NavController,
    private router: Router,
    private route: ActivatedRoute,
    private subCategoryService: SubCategoryService,
    public loaderService: LoaderService
  ) {
    this.environment = environment;
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.name = this.router.getCurrentNavigation().extras.state.name;
        this.id = this.router.getCurrentNavigation().extras.state.id;
        this.currentPage = 1;
        this.lastPage = 1;
        this.data = [];
        this.videos_data = [];
        // const c_id = localStorage.getItem('c_id');
        // console.log("c_id : ",c_id)
        // if(c_id){
        //   this.getSubCategory(this.currentPage, c_id, this.name)
        // }else{
          this.getSubCategory(this.currentPage, this.id)
        // }
      }
    });
  }

  ngOnInit() {

  }
  ionViewWillEnter() {
    // this.environment = environment;
    // this.route.queryParams.subscribe(params => {
    //   if (this.router.getCurrentNavigation().extras.state) {
    //     this.name = this.router.getCurrentNavigation().extras.state.name;
    //     this.id = this.router.getCurrentNavigation().extras.state.id;
    //   }
    // });

  }
  getSubCategory(currentPage, c_id, event = null) {
    this.loaderService.showLoader()
    this.loading = true;
    let formData = new FormData();
    // localStorage.setItem('c_id', c_id);
    formData.append("PageNumber", currentPage);
    formData.append("categoryid", c_id);
    this.subCategoryService.getSubCategory(formData).then((res: any) => {
      console.log(res);
      if (res.status != false) {
        this.loading = false;
        this.currentPage = res.CurrentPage;
        this.lastPage = res.total;
        if (this.currentPage == 1) {
          this.data = res.categories;
          if(res.videocount > 0){
            this.videos_data = res.videos;
          }
          this.loaderService.hideLoader()
        } else {
          this.data = [...this.data, ...res.categories];
          if(res.videocount > 0){
            this.videos_data = [...this.videos_data, ...res.videos];
          }
          this.loaderService.hideLoader()
        }
        if (event) {
          event.target.complete();
        }
      } else {
          this.loaderService.hideLoader()
          this.loading = false;
      }
    }, err => {
          this.loaderService.hideLoader()
          this.loading = false;
      if (event) {
        event.target.complete();
      }
    });
  }
  subCategoryDetail(id, name){
    let navigationExtras: NavigationExtras = {
      state: {
        id: id,
        name: name,
      }
    };
    this.router.navigate(['tabs/tab1/sub-categories/sub-categories-detail'], navigationExtras);
  }
  refreshData(event) {
    this.currentPage = 1;
    this.getSubCategory(1, event);
  }
  ConvertToInt(currentPage) {
    return parseInt(currentPage);
  }
  loadMoreData(value, event) {
    this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value)
    this.getSubCategory(this.currentPage, event)
  }
  back() {
    this.navCtrl.back();
  }
}
