import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubCategoriesPage } from './sub-categories.page';

const routes: Routes = [
  {
    path: '',
    component: SubCategoriesPage
  },
  {
    path: 'video',
    loadChildren: () => import('..//../pages/video/video.module').then( m => m.VideoPageModule)
  },
  {
    path: 'video-player',
    loadChildren: () => import('..//../pages/video-player/video-player.module').then( m => m.VideoPlayerPageModule)
  },
  {
    path: 'sub-categories-detail',
    loadChildren: () => import('./sub-categories-detail/sub-categories-detail.module').then( m => m.SubCategoriesDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubCategoriesPageRoutingModule {}
