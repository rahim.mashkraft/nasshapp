import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubCategoriesDetailPageRoutingModule } from './sub-categories-detail-routing.module';

import { SubCategoriesDetailPage } from './sub-categories-detail.page';
import { SharedModule } from 'src/app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubCategoriesDetailPageRoutingModule,
    SharedModule
  ],
  declarations: [SubCategoriesDetailPage]
})
export class SubCategoriesDetailPageModule {}
