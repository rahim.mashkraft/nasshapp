"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.SubCategoriesDetailPage = void 0;
var core_1 = require("@angular/core");
var environment_prod_1 = require("src/environments/environment.prod");
var SubCategoriesDetailPage = /** @class */ (function () {
    function SubCategoriesDetailPage(navCtrl, router, route, subCategoryService, loaderService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.router = router;
        this.route = route;
        this.subCategoryService = subCategoryService;
        this.loaderService = loaderService;
        this.name = '';
        this.currentPage = 1;
        this.lastPage = 1;
        this.loading = false;
        this.environment = environment_prod_1.environment;
        this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
                _this.name = _this.router.getCurrentNavigation().extras.state.name;
                _this.id = _this.router.getCurrentNavigation().extras.state.id;
                _this.currentPage = 1;
                _this.lastPage = 1;
                _this.videos_data = [];
                _this.getSubCategory(_this.currentPage, _this.id);
            }
        });
    }
    SubCategoriesDetailPage.prototype.getSubCategory = function (currentPage, c_id, event) {
        var _this = this;
        if (event === void 0) { event = null; }
        this.loaderService.showLoader();
        this.loading = true;
        var formData = new FormData();
        formData.append("PageNumber", currentPage);
        formData.append("categoryid", c_id);
        this.subCategoryService.getSubCategory(formData).then(function (res) {
            console.log(res);
            if (res.status != false) {
                _this.loading = false;
                _this.currentPage = res.CurrentPage;
                _this.lastPage = res.total;
                if (_this.currentPage == 1) {
                    if (res.videocount > 0) {
                        _this.videos_data = res.videos;
                    }
                    _this.loaderService.hideLoader();
                }
                else {
                    if (res.videocount > 0) {
                        _this.videos_data = __spreadArrays(_this.videos_data, res.videos);
                    }
                    _this.loaderService.hideLoader();
                }
                if (event) {
                    event.target.complete();
                }
            }
            else {
                _this.loaderService.hideLoader();
                _this.loading = false;
            }
        }, function (err) {
            _this.loaderService.hideLoader();
            _this.loading = false;
            if (event) {
                event.target.complete();
            }
        });
    };
    SubCategoriesDetailPage.prototype.refreshData = function (event) {
        this.currentPage = 1;
        this.getSubCategory(1, event);
    };
    SubCategoriesDetailPage.prototype.ConvertToInt = function (currentPage) {
        return parseInt(currentPage);
    };
    SubCategoriesDetailPage.prototype.loadMoreData = function (value, event) {
        this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value);
        this.getSubCategory(this.currentPage, event);
    };
    SubCategoriesDetailPage.prototype.openVideo = function (videotitle, videoimage, videourl, videoid) {
        var navigationExtras = {
            state: {
                videotitle: videotitle,
                videoimage: videoimage,
                videourl: videourl,
                videoid: videoid
            }
        };
        // this.router.navigate(['post-add-second', navigationExtras]);
        this.router.navigate(['tabs/tab1/sub-categories/video'], navigationExtras);
        // this.router.navigate(['testing'], navigationExtras);
    };
    SubCategoriesDetailPage.prototype.ngOnInit = function () {
    };
    SubCategoriesDetailPage = __decorate([
        core_1.Component({
            selector: 'app-sub-categories-detail',
            templateUrl: './sub-categories-detail.page.html',
            styleUrls: ['./sub-categories-detail.page.scss']
        })
    ], SubCategoriesDetailPage);
    return SubCategoriesDetailPage;
}());
exports.SubCategoriesDetailPage = SubCategoriesDetailPage;
