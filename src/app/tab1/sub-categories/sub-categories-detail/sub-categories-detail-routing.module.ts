import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubCategoriesDetailPage } from './sub-categories-detail.page';

const routes: Routes = [
  {
    path: '',
    component: SubCategoriesDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubCategoriesDetailPageRoutingModule {}
