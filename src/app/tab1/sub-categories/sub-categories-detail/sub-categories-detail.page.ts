import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { LoaderService } from 'src/app/services/loader.service';
import { SubCategoryService } from 'src/app/services/sub-category.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-sub-categories-detail',
  templateUrl: './sub-categories-detail.page.html',
  styleUrls: ['./sub-categories-detail.page.scss'],
})
export class SubCategoriesDetailPage implements OnInit {

  public name: string = '';
  public environment: any;
  public id: number;
  public currentPage: number = 1;
  public lastPage: number = 1;
  public videos_data: any;
  public loading:boolean = false;

  constructor(
    public navCtrl: NavController,
    private router: Router,
    private route: ActivatedRoute,
    private subCategoryService: SubCategoryService,
    public loaderService: LoaderService
  ) {
    this.environment = environment;
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.name = this.router.getCurrentNavigation().extras.state.name;
        this.id = this.router.getCurrentNavigation().extras.state.id;
        this.currentPage = 1;
        this.lastPage = 1;
        this.videos_data = [];
        this.getSubCategory(this.currentPage, this.id)
      }
    });
  }
  getSubCategory(currentPage, c_id, event = null) {
    this.loaderService.showLoader()
    this.loading = true;
    let formData = new FormData();
    formData.append("PageNumber", currentPage);
    formData.append("categoryid", c_id);
    this.subCategoryService.getSubCategory(formData).then((res: any) => {
      console.log(res);
      if (res.status != false) {
        this.loading = false;
        this.currentPage = res.CurrentPage;
        this.lastPage = res.total;
        if (this.currentPage == 1) {
          if(res.videocount > 0){
            this.videos_data = res.videos;
          }
          this.loaderService.hideLoader()
        } else {
          if(res.videocount > 0){
            this.videos_data = [...this.videos_data, ...res.videos];
          }
          this.loaderService.hideLoader()
        }
        if (event) {
          event.target.complete();
        }
      } else {
          this.loaderService.hideLoader()
          this.loading = false;
      }
    }, err => {
          this.loaderService.hideLoader()
          this.loading = false;
      if (event) {
        event.target.complete();
      }
    });
  }
  refreshData(event) {
    this.currentPage = 1;
    this.getSubCategory(1, event);
  }
  ConvertToInt(currentPage) {
    return parseInt(currentPage);
  }
  loadMoreData(value, event) {
    this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value)
    this.getSubCategory(this.currentPage, event)
  }
  openVideo(videotitle, videoimage, videourl, videoid){
    let navigationExtras: NavigationExtras = {
      state: {
        videotitle: videotitle,
        videoimage: videoimage,
        videourl: videourl,
        videoid: videoid
      }
    };
    // this.router.navigate(['post-add-second', navigationExtras]);
    this.router.navigate(['tabs/tab1/sub-categories/video'], navigationExtras);
    // this.router.navigate(['testing'], navigationExtras);
  }
  ngOnInit() {
  }

}
