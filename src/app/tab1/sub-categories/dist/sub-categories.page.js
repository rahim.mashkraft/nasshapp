"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.SubCategoriesPage = void 0;
var core_1 = require("@angular/core");
var environment_1 = require("src/environments/environment");
var SubCategoriesPage = /** @class */ (function () {
    function SubCategoriesPage(navCtrl, router, route, subCategoryService, loaderService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.router = router;
        this.route = route;
        this.subCategoryService = subCategoryService;
        this.loaderService = loaderService;
        this.loading = false;
        this.currentPage = 1;
        this.lastPage = 1;
        this.data = [];
        this.videos_data = [];
        this.name = '';
        this.environment = environment_1.environment;
        this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
                _this.name = _this.router.getCurrentNavigation().extras.state.name;
                _this.id = _this.router.getCurrentNavigation().extras.state.id;
                _this.currentPage = 1;
                _this.lastPage = 1;
                _this.data = [];
                _this.videos_data = [];
                // const c_id = localStorage.getItem('c_id');
                // console.log("c_id : ",c_id)
                // if(c_id){
                //   this.getSubCategory(this.currentPage, c_id, this.name)
                // }else{
                _this.getSubCategory(_this.currentPage, _this.id);
                // }
            }
        });
    }
    SubCategoriesPage.prototype.ngOnInit = function () {
    };
    SubCategoriesPage.prototype.ionViewWillEnter = function () {
        // this.environment = environment;
        // this.route.queryParams.subscribe(params => {
        //   if (this.router.getCurrentNavigation().extras.state) {
        //     this.name = this.router.getCurrentNavigation().extras.state.name;
        //     this.id = this.router.getCurrentNavigation().extras.state.id;
        //   }
        // });
    };
    SubCategoriesPage.prototype.getSubCategory = function (currentPage, c_id, event) {
        var _this = this;
        if (event === void 0) { event = null; }
        this.loaderService.showLoader();
        this.loading = true;
        var formData = new FormData();
        // localStorage.setItem('c_id', c_id);
        formData.append("PageNumber", currentPage);
        formData.append("categoryid", c_id);
        this.subCategoryService.getSubCategory(formData).then(function (res) {
            console.log(res);
            if (res.status != false) {
                _this.loading = false;
                _this.currentPage = res.CurrentPage;
                _this.lastPage = res.total;
                if (_this.currentPage == 1) {
                    _this.data = res.categories;
                    if (res.videocount > 0) {
                        _this.videos_data = res.videos;
                    }
                    _this.loaderService.hideLoader();
                }
                else {
                    _this.data = __spreadArrays(_this.data, res.categories);
                    if (res.videocount > 0) {
                        _this.videos_data = __spreadArrays(_this.videos_data, res.videos);
                    }
                    _this.loaderService.hideLoader();
                }
                if (event) {
                    event.target.complete();
                }
            }
            else {
                _this.loaderService.hideLoader();
                _this.loading = false;
            }
        }, function (err) {
            _this.loaderService.hideLoader();
            _this.loading = false;
            if (event) {
                event.target.complete();
            }
        });
    };
    SubCategoriesPage.prototype.subCategoryDetail = function (id, name) {
        var navigationExtras = {
            state: {
                id: id,
                name: name
            }
        };
        this.router.navigate(['tabs/tab1/sub-categories/sub-categories-detail'], navigationExtras);
    };
    SubCategoriesPage.prototype.refreshData = function (event) {
        this.currentPage = 1;
        this.getSubCategory(1, event);
    };
    SubCategoriesPage.prototype.ConvertToInt = function (currentPage) {
        return parseInt(currentPage);
    };
    SubCategoriesPage.prototype.loadMoreData = function (value, event) {
        this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value);
        this.getSubCategory(this.currentPage, event);
    };
    SubCategoriesPage.prototype.back = function () {
        this.navCtrl.back();
    };
    SubCategoriesPage = __decorate([
        core_1.Component({
            selector: 'app-sub-categories',
            templateUrl: './sub-categories.page.html',
            styleUrls: ['./sub-categories.page.scss']
        })
    ], SubCategoriesPage);
    return SubCategoriesPage;
}());
exports.SubCategoriesPage = SubCategoriesPage;
