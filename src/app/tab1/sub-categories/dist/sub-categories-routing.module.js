"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SubCategoriesPageRoutingModule = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var sub_categories_page_1 = require("./sub-categories.page");
var routes = [
    {
        path: '',
        component: sub_categories_page_1.SubCategoriesPage
    },
    {
        path: 'video',
        loadChildren: function () { return Promise.resolve().then(function () { return require('..//../pages/video/video.module'); }).then(function (m) { return m.VideoPageModule; }); }
    },
    {
        path: 'video-player',
        loadChildren: function () { return Promise.resolve().then(function () { return require('..//../pages/video-player/video-player.module'); }).then(function (m) { return m.VideoPlayerPageModule; }); }
    },
    {
        path: 'sub-categories-detail',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./sub-categories-detail/sub-categories-detail.module'); }).then(function (m) { return m.SubCategoriesDetailPageModule; }); }
    }
];
var SubCategoriesPageRoutingModule = /** @class */ (function () {
    function SubCategoriesPageRoutingModule() {
    }
    SubCategoriesPageRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], SubCategoriesPageRoutingModule);
    return SubCategoriesPageRoutingModule;
}());
exports.SubCategoriesPageRoutingModule = SubCategoriesPageRoutingModule;
