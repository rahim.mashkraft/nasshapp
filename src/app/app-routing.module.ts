import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  // { path: '', redirectTo: 'Auth/login', pathMatch: 'full' },
  {
    path: '',
    loadChildren: () => import('./pages/Auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/Auth/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'tabs/edit-profile',
    loadChildren: () => import('./pages/edit-profile/edit-profile.module').then( m => m.EditProfilePageModule)
  },
  {
    path: 'tabs/select-group',
    loadChildren: () => import('./pages/select-group/select-group.module').then( m => m.SelectGroupPageModule)
  },
  {
    path: 'send-email-contact',
    loadChildren: () => import('./pages/send-email-contact/send-email-contact.module').then( m => m.SendEmailContactPageModule)
  },
  {
    path: 'send-sms-contact',
    loadChildren: () => import('./pages/send-sms-contact/send-sms-contact.module').then( m => m.SendSmsContactPageModule)
  },
  {
    path: 'model',
    loadChildren: () => import('./pages/model/model.module').then( m => m.ModelPageModule)
  },
  {
    path: 'pdf-viewer',
    loadChildren: () => import('./pages/pdf-viewer/pdf-viewer.module').then( m => m.PdfViewerPageModule)
  },
  {
    path: 'testing',
    loadChildren: () => import('./pages/testing/testing.module').then( m => m.TestingPageModule)
  },
  {
    path: 'success-model',
    loadChildren: () => import('./pages/success-model/success-model.module').then( m => m.SuccessModelPageModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./pages/chat/chat.module').then( m => m.ChatPageModule)
  },
  {
    path: 'video-player',
    loadChildren: () => import('./pages/video-player/video-player.module').then( m => m.VideoPlayerPageModule)
  },
  {
    path: 'twillio-model',
    loadChildren: () => import('./pages/twillio-model/twillio-model.module').then( m => m.TwillioModelPageModule)
  },
  {
    path: 'image-model',
    loadChildren: () => import('./pages/image-model/image-model.module').then( m => m.ImageModelPageModule)
  },
  {
    path: 'iframe',
    loadChildren: () => import('./pages/iframe/iframe.module').then( m => m.IframePageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./pages/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
