import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, MenuController, NavController, Platform } from '@ionic/angular';
import { AlertService } from '../services/alert.service';
// import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic";
import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic/ngx";
import { EventsService } from '../services/events.service';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts/ngx';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { INotificationPayload } from 'plugins/cordova-plugin-fcm-with-dependecy-updated/typings';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  disableTabs = false;

  public hasPermission: boolean;
  public token: string;
  public pushPayload: INotificationPayload;
  constructor(
    public menuCtrl: MenuController,
    private alertService: AlertService,
    private router: Router,
    public fcm: FCM,
    public platform: Platform,
    private contacts: Contacts,
    public storage: Storage,
    private eventsService: EventsService,
    private geolocation: Geolocation,
    private diagnostic: Diagnostic,
    public alertController: AlertController,
    public navCtrl: NavController,
  ) {
    if (this.platform.is('cordova')) {
      console.log("cordova : ");
      this.platform.ready().then(() => {
        // setTimeout(() => {
        //   this.eventsService.publish("startContact");
        // },2000)
        // console.log("this.platform.ready() : ");
        // console.log("this.fcm : ", this.fcm);
        // alert(JSON.stringify(this.fcm))
        let thisObj = this;
        thisObj.fcm.onNotification().subscribe(data => {
          console.log("data : ", data)
          // alert(JSON.stringify(this.fcm.onNotification()))

          console.log("this.fcm : ", this.fcm);

          if (data.wasTapped) {
            console.log("Received in background");
            console.log(JSON.stringify(data));

          } else {
            console.log("Received in foreground");
            console.log(JSON.stringify(data));
            // alert(data)
            var result = JSON.stringify(data)
            // alert(result)
            var res = JSON.parse(result);
            this.eventsService.publish("notification_receive")
            // alert(res)

          };
        })
        this.checkLocation()

      });

    }
  }
  checkLocation() {
    if (this.platform.is('cordova')) {
      this.diagnostic.getLocationAuthorizationStatus().then((status) => {
        setTimeout(() => {
          console.log("status.NOT_REQUESTED : ", this.diagnostic.permissionStatus.NOT_REQUESTED)
          console.log("status.DENIED_ALWAYS : ", this.diagnostic.permissionStatus.DENIED_ALWAYS)
          console.log("status.GRANTED : ", this.diagnostic.permissionStatus.GRANTED)
          console.log("status.GRANTED_WHEN_IN_USE : ", this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE)
        }, 100);
        // alert(status) ;

        if (status == this.diagnostic.permissionStatus.GRANTED || status == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
          this.diagnostic.isLocationEnabled().then((res) => {
            console.log("this.diagnostic.isLocationEnabled() : ", res)
            if (res == true) {
              this.getCurrentLocation();
              setInterval(() => {
                this.getCurrentLocation();
              }, 100000)
            }
          })
        } else if (status == this.diagnostic.permissionStatus.NOT_REQUESTED) {
          console.log("status == this.diagnostic.permissionStatus.NOT_REQUESTED")
        } else {
          // this.further();
        }
        console.log("status : ", status)
      })
    }
  }
  Diagnostic_push() {

    this.diagnostic.getLocationAuthorizationStatus()
      .then((status) => {
        alert(status);
        if (status) {

        } else {
          this.further();
        }
      }).catch(e => console.error(e));
  }
  async further() {
    const alert = await this.alertController.create({
      header: 'Turn on Location Services',
      message: 'Please turn on location services',
      buttons: [
        {
          text: 'Not now',
          handler: () => {
            console.log('Cancel No');

          }
        },
        {
          text: 'Settings',
          handler: () => {
            //console.log('Cancel Yes');
            this.switchToSettings();
          }
        }
      ]
    })
    // this.currentAlert = alert;
    await alert.present();
  }
  switchToSettings() {
    this.diagnostic.switchToSettings()
      .then(() => {
        console.log("Successfully switched to Settings app");
      }).catch(e => console.error(e));
  }

  getCurrentLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      resp.coords.latitude;
      resp.coords.longitude;
      console.log('latitude 1', resp.coords.latitude);
      console.log('longitude 1', resp.coords.longitude);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
    let watch = this.geolocation.watchPosition();
    console.log("watch : ", watch)
    watch.subscribe((data) => {
      console.log("data : ", data)
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
    });

  }

  toggleMenu() {
    if (this.disableTabs) {
      this.alertService.showDisableAlert();
    }
    else {
      this.menuCtrl.toggle();
    }
  }
  chat(){
    this.navCtrl.navigateRoot(['tabs/tab4']);
  }
  checkin(){

  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }
  ionViewDidLeave(): void {
    this.menuCtrl.enable(true);
  }
  goToContactTable() {
    // this.router.navigate(['tabs/tab3']);
  }
}
