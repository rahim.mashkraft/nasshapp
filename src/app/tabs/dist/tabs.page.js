"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.TabsPage = void 0;
var core_1 = require("@angular/core");
var TabsPage = /** @class */ (function () {
    function TabsPage(menuCtrl, alertService, router, fcm, platform, contacts, storage, eventsService, geolocation, diagnostic, alertController, navCtrl) {
        var _this = this;
        this.menuCtrl = menuCtrl;
        this.alertService = alertService;
        this.router = router;
        this.fcm = fcm;
        this.platform = platform;
        this.contacts = contacts;
        this.storage = storage;
        this.eventsService = eventsService;
        this.geolocation = geolocation;
        this.diagnostic = diagnostic;
        this.alertController = alertController;
        this.navCtrl = navCtrl;
        this.disableTabs = false;
        if (this.platform.is('cordova')) {
            console.log("cordova : ");
            this.platform.ready().then(function () {
                // setTimeout(() => {
                //   this.eventsService.publish("startContact");
                // },2000)
                // console.log("this.platform.ready() : ");
                // console.log("this.fcm : ", this.fcm);
                // alert(JSON.stringify(this.fcm))
                var thisObj = _this;
                thisObj.fcm.onNotification().subscribe(function (data) {
                    console.log("data : ", data);
                    // alert(JSON.stringify(this.fcm.onNotification()))
                    console.log("this.fcm : ", _this.fcm);
                    if (data.wasTapped) {
                        console.log("Received in background");
                        console.log(JSON.stringify(data));
                    }
                    else {
                        console.log("Received in foreground");
                        console.log(JSON.stringify(data));
                        // alert(data)
                        var result = JSON.stringify(data);
                        // alert(result)
                        var res = JSON.parse(result);
                        _this.eventsService.publish("notification_receive");
                        // alert(res)
                    }
                    ;
                });
                _this.checkLocation();
            });
        }
    }
    TabsPage.prototype.checkLocation = function () {
        var _this = this;
        if (this.platform.is('cordova')) {
            this.diagnostic.getLocationAuthorizationStatus().then(function (status) {
                setTimeout(function () {
                    console.log("status.NOT_REQUESTED : ", _this.diagnostic.permissionStatus.NOT_REQUESTED);
                    console.log("status.DENIED_ALWAYS : ", _this.diagnostic.permissionStatus.DENIED_ALWAYS);
                    console.log("status.GRANTED : ", _this.diagnostic.permissionStatus.GRANTED);
                    console.log("status.GRANTED_WHEN_IN_USE : ", _this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE);
                }, 100);
                // alert(status) ;
                if (status == _this.diagnostic.permissionStatus.GRANTED || status == _this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
                    _this.diagnostic.isLocationEnabled().then(function (res) {
                        console.log("this.diagnostic.isLocationEnabled() : ", res);
                        if (res == true) {
                            _this.getCurrentLocation();
                            setInterval(function () {
                                _this.getCurrentLocation();
                            }, 100000);
                        }
                    });
                }
                else if (status == _this.diagnostic.permissionStatus.NOT_REQUESTED) {
                    console.log("status == this.diagnostic.permissionStatus.NOT_REQUESTED");
                }
                else {
                    // this.further();
                }
                console.log("status : ", status);
            });
        }
    };
    TabsPage.prototype.Diagnostic_push = function () {
        var _this = this;
        this.diagnostic.getLocationAuthorizationStatus()
            .then(function (status) {
            alert(status);
            if (status) {
            }
            else {
                _this.further();
            }
        })["catch"](function (e) { return console.error(e); });
    };
    TabsPage.prototype.further = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Turn on Location Services',
                            message: 'Please turn on location services',
                            buttons: [
                                {
                                    text: 'Not now',
                                    handler: function () {
                                        console.log('Cancel No');
                                    }
                                },
                                {
                                    text: 'Settings',
                                    handler: function () {
                                        //console.log('Cancel Yes');
                                        _this.switchToSettings();
                                    }
                                }
                            ]
                        })
                        // this.currentAlert = alert;
                    ];
                    case 1:
                        alert = _a.sent();
                        // this.currentAlert = alert;
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        // this.currentAlert = alert;
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TabsPage.prototype.switchToSettings = function () {
        this.diagnostic.switchToSettings()
            .then(function () {
            console.log("Successfully switched to Settings app");
        })["catch"](function (e) { return console.error(e); });
    };
    TabsPage.prototype.getCurrentLocation = function () {
        this.geolocation.getCurrentPosition().then(function (resp) {
            resp.coords.latitude;
            resp.coords.longitude;
            console.log('latitude 1', resp.coords.latitude);
            console.log('longitude 1', resp.coords.longitude);
        })["catch"](function (error) {
            console.log('Error getting location', error);
        });
        var watch = this.geolocation.watchPosition();
        console.log("watch : ", watch);
        watch.subscribe(function (data) {
            console.log("data : ", data);
            // data can be a set of coordinates, or an error (if an error occurred).
            // data.coords.latitude
            // data.coords.longitude
        });
    };
    TabsPage.prototype.toggleMenu = function () {
        if (this.disableTabs) {
            this.alertService.showDisableAlert();
        }
        else {
            this.menuCtrl.toggle();
        }
    };
    TabsPage.prototype.chat = function () {
        this.navCtrl.navigateRoot(['tabs/tab4']);
    };
    TabsPage.prototype.checkin = function () {
    };
    TabsPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
    };
    TabsPage.prototype.ionViewDidLeave = function () {
        this.menuCtrl.enable(true);
    };
    TabsPage.prototype.goToContactTable = function () {
        // this.router.navigate(['tabs/tab3']);
    };
    TabsPage = __decorate([
        core_1.Component({
            selector: 'app-tabs',
            templateUrl: 'tabs.page.html',
            styleUrls: ['tabs.page.scss']
        })
    ], TabsPage);
    return TabsPage;
}());
exports.TabsPage = TabsPage;
