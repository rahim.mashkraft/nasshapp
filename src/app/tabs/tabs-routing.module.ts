import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        loadChildren: () => import('../tab1/tab1.module').then(m => m.Tab1PageModule)
      },
      {
        path: 'tab2',
        loadChildren: () => import('../tab2/tab2.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'tab3',
        loadChildren: () => import('../tab3/tab3.module').then(m => m.Tab3PageModule)
      },
      {
        path: 'tab4',
        loadChildren: () => import('../tab4/tab4.module').then( m => m.Tab4PageModule)
      },
      {
        path: 'import-contacts',
        loadChildren: () => import('../pages/import-contacts/import-contacts.module').then( m => m.ImportContactsPageModule)
      },
      {
        path: 'contact-table',
        loadChildren: () => import('../pages/contact-table/contact-table.module').then( m => m.ContactTablePageModule)
      },
      {
        path: 'send-broadcast',
        loadChildren: () => import('../pages/send-broadcast/send-broadcast.module').then( m => m.SendBroadcastPageModule)
      },
      {
        path: 'send-smsbroadcast',
        loadChildren: () => import('../pages/send-smsbroadcast/send-smsbroadcast.module').then( m => m.SendSMSBroadcastPageModule)
      },
      {
        path: 'notification',
        loadChildren: () => import('../pages/notification/notification.module').then( m => m.NotificationPageModule)
      },
      {
        path: 'member',
        loadChildren: () => import('../pages/member/member.module').then( m => m.MemberPageModule)
      },
      {
        path: 'contact-groups',
        loadChildren: () => import('../pages/contact-groups/contact-groups.module').then( m => m.ContactGroupsPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
