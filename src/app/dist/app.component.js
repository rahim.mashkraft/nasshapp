"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.AppComponent = void 0;
var core_1 = require("@angular/core");
var environment_1 = require("src/environments/environment");
var w = window;
var AppComponent = /** @class */ (function () {
    function AppComponent(platform, splashScreen, statusBar, navCtrl, menu, alertService, eventsService, cdr, router, storage, fcm) {
        var _this = this;
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.alertService = alertService;
        this.eventsService = eventsService;
        this.cdr = cdr;
        this.router = router;
        this.storage = storage;
        this.fcm = fcm;
        this.onlineOffline = navigator.onLine;
        this.userData = {
            aid: '',
            firstname: '',
            lastname: '',
            email: '',
            profileimage: '',
            address: '',
            city: '',
            country: '',
            phone: '',
            username: '',
            zipcode: '',
            state: ''
        };
        this.setupFCM();
        this.environment = environment_1.environment;
        this.initializeApp();
        window.addEventListener('offline', function () {
            _this.alertService.presentNetworkAlert();
        });
    }
    AppComponent.prototype.setupFCM = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c;
            var _this = this;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0: return [4 /*yield*/, this.platform.ready()];
                    case 1:
                        _d.sent();
                        console.log('FCM setup started');
                        if (!this.platform.is('cordova')) {
                            return [2 /*return*/];
                        }
                        console.log('In cordova platform');
                        console.log('Subscribing to token updates');
                        this.fcm.onTokenRefresh().subscribe(function (newToken) {
                            _this.token = newToken;
                            console.log('onTokenRefresh received event with: ', newToken);
                        });
                        console.log('Subscribing to new notifications');
                        this.fcm.onNotification().subscribe(function (payload) {
                            _this.pushPayload = payload;
                            console.log('onNotification received event with: ', payload);
                        });
                        _a = this;
                        return [4 /*yield*/, this.fcm.requestPushPermission()];
                    case 2:
                        _a.hasPermission = _d.sent();
                        console.log('requestPushPermission result: ', this.hasPermission);
                        _b = this;
                        return [4 /*yield*/, this.fcm.getToken()];
                    case 3:
                        _b.token = _d.sent();
                        console.log('getToken result: ', this.token);
                        _c = this;
                        return [4 /*yield*/, this.fcm.getInitialPushPayload()];
                    case 4:
                        _c.pushPayload = _d.sent();
                        console.log('getInitialPushPayload result: ', this.pushPayload);
                        return [2 /*return*/];
                }
            });
        });
    };
    Object.defineProperty(AppComponent.prototype, "pushPayloadString", {
        get: function () {
            return JSON.stringify(this.pushPayload, null, 4);
        },
        enumerable: false,
        configurable: true
    });
    AppComponent.prototype.initializeApp = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loginUser;
            var _this = this;
            return __generator(this, function (_a) {
                if (!navigator.onLine) {
                    this.alertService.presentNetworkAlert();
                }
                this.platform.ready().then(function () {
                    _this.statusBar.styleDefault();
                    setTimeout(function () {
                        _this.splashScreen.hide();
                    }, 2000);
                    // let thisObj = this;
                    // thisObj.fcm.requestPushPermission().then(() => {
                    //   console.log("requestPushPermission true: ")
                    //   // alert("suecce ")
                    // }).catch((e) => {
                    //   console.log("requestPushPermission : ",e)
                    //   // alert("errr "+e)
                    // })
                    _this.eventsService.subscribe('userLogged', function (data) {
                        console.log("userLogged : ", data);
                        _this.userData.firstname = data.firstname;
                        _this.userData.lastname = data.lastname;
                        _this.userData.email = data.email;
                        _this.userData.profileimage = data.profileimage;
                        _this.profileImage = environment_1.environment.profileImageUrl + _this.userData.profileimage;
                        console.log("this.userData.profileImage : ", _this.userData.profileImage);
                        console.log("this.profileImage 1 : ", _this.profileImage);
                        setTimeout(function () {
                            _this.cdr.detectChanges();
                        }, 1000);
                    });
                    _this.eventsService.subscribe('nameChanged', function (data) {
                        _this.userData.firstname = data.firstname;
                        _this.userData.lastname = data.lastname;
                    });
                    _this.eventsService.subscribe('pictureChanged', function (data) {
                        _this.userData.profileimage = data.profileimage;
                        _this.profileImage = environment_1.environment.profileImageUrl + _this.userData.profileimage;
                        console.log("this.profileImage 2 : ", _this.profileImage);
                        setTimeout(function () {
                            _this.cdr.detectChanges();
                        }, 1000);
                    });
                    setTimeout(function () {
                        _this.userData.firstname = localStorage.getItem('firstname');
                        _this.userData.lastname = localStorage.getItem('lastname');
                        _this.userData.email = localStorage.getItem('email');
                        _this.userData.profileimage = localStorage.getItem('profileimage');
                        if (_this.userData.profileimage) {
                            _this.profileImage = environment_1.environment.profileImageUrl + _this.userData.profileimage;
                            console.log("this.userData.profileImage : ", _this.userData.profileImage);
                            console.log("this.profileImage 3 : ", _this.profileImage);
                        }
                        setTimeout(function () {
                            _this.cdr.detectChanges();
                        }, 1000);
                    }, 1000);
                });
                loginUser = localStorage.getItem('user_id');
                console.log("loginUser : ", loginUser);
                if (loginUser) {
                    this.navCtrl.navigateRoot('/tabs');
                }
                else {
                    this.navCtrl.navigateRoot('');
                }
                return [2 /*return*/];
            });
        });
    };
    AppComponent.prototype.checkWelcomeScreen = function (loginUser) {
        if (loginUser === void 0) { loginUser = null; }
        var firstTimeAppOpened = localStorage.getItem('firstTime');
        if (firstTimeAppOpened) {
            this.navCtrl.navigateRoot('welcome-screen');
        }
        else {
            if (loginUser) {
                this.navCtrl.navigateRoot('/tabs');
            }
            else {
                this.navCtrl.navigateRoot('');
            }
        }
    };
    AppComponent.prototype.initDeepLinking = function () {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.platform.is('cordova')) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.initDeepLinkingBranchio()];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.initDeepLinkingWeb()];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AppComponent.prototype.initDeepLinkingWeb = function () {
        return __awaiter(this, void 0, Promise, function () {
            var userId;
            return __generator(this, function (_a) {
                userId = this.platform.getQueryParam('$userId') ||
                    this.platform.getQueryParam('userId') ||
                    this.platform.getQueryParam('%24userId');
                this.navCtrl.navigateRoot('');
                console.log('Parameter', userId);
                return [2 /*return*/];
            });
        });
    };
    AppComponent.prototype.initDeepLinkingBranchio = function () {
        return __awaiter(this, void 0, Promise, function () {
            var branchIo, data, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        branchIo = window['Branch'];
                        if (!branchIo) return [3 /*break*/, 2];
                        return [4 /*yield*/, branchIo.initSession()];
                    case 1:
                        data = _a.sent();
                        console.log(data);
                        if (data.userId !== undefined) {
                            console.log('Parameter', data.userId);
                            // this.userService.inviteId = data.userId;
                            this.navCtrl.navigateRoot('register');
                        }
                        else {
                            // this.checkWelcomeScreen();
                            this.navCtrl.navigateRoot('/login');
                        }
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        console.error(err_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AppComponent.prototype.importContact = function () {
        this.router.navigate(['tabs/import-contacts']);
        this.menu.close();
    };
    AppComponent.prototype.contactsTable = function () {
        this.router.navigate(['tabs/contact-table']);
        this.menu.close();
    };
    AppComponent.prototype.sendBroadcast = function () {
        this.router.navigate(['tabs/send-broadcast']);
        this.menu.close();
    };
    AppComponent.prototype.sendSmsBroadcast = function () {
        this.router.navigate(['tabs/send-smsbroadcast']);
        this.menu.close();
    };
    AppComponent.prototype.goToEditProfile = function () {
        this.router.navigate(['tabs/edit-profile']);
        this.menu.close();
    };
    AppComponent.prototype.member = function () {
        this.router.navigate(['tabs/member']);
        this.menu.close();
    };
    AppComponent.prototype.contactGroups = function () {
        this.router.navigate(['tabs/contact-groups']);
        this.menu.close();
    };
    AppComponent.prototype.video = function () {
        this.router.navigate(['testing']);
        this.menu.close();
    };
    AppComponent.prototype.successModel = function () {
        this.router.navigate(['success-model']);
        this.menu.close();
    };
    AppComponent.prototype.openFirst = function () {
        this.menu.enable(true, 'first');
        this.menu.open('first');
    };
    AppComponent.prototype.openEnd = function () {
        this.menu.open('end');
    };
    AppComponent.prototype.openCustom = function () {
        this.menu.enable(true, 'custom');
        this.menu.open('custom');
    };
    AppComponent.prototype.ionViewWillEnter = function () {
        this.menu.enable(true);
    };
    AppComponent.prototype.ionViewDidLeave = function () {
        this.menu.enable(true);
    };
    AppComponent.prototype.logout = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        localStorage.clear();
                        return [4 /*yield*/, this.menu.enable(true)];
                    case 1:
                        _a.sent();
                        this.navCtrl.navigateRoot('/');
                        return [2 /*return*/];
                }
            });
        });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: 'app.component.html',
            styleUrls: ['app.component.scss']
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
// ionic cordova plugin add cordova-plugin-facebook-connect --variable APP_ID="1636116319932991" --variable APP_NAME="Nash Consulting"
