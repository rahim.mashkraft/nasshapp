"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppModule = void 0;
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router");
var ngx_1 = require("@ionic-native/network/ngx");
var ngx_2 = require("@ionic-native/splash-screen/ngx");
var ngx_3 = require("@ionic-native/status-bar/ngx");
var angular_1 = require("@ionic/angular");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var login_service_1 = require("./services/login.service");
var ngx_4 = require("@ionic-native/file-transfer/ngx");
var ngx_5 = require("@ionic-native/camera/ngx");
var ngx_6 = require("@ionic-native/contacts/ngx");
var ngx_quill_1 = require("ngx-quill");
var ngx_7 = require("@ionic-native/facebook/ngx");
var ngx_8 = require("@ionic-native/google-plus/ngx");
var ngx_9 = require("@ionic-native/device/ngx");
var ngx_10 = require("@ionic-native/file/ngx");
var ngx_11 = require("@ionic-native/document-viewer/ngx");
var ng2_pdf_viewer_1 = require("ng2-pdf-viewer");
var ngx_12 = require("@ionic-native/diagnostic/ngx");
var storage_1 = require("@ionic/storage");
var ngx_13 = require("@ionic-native/geolocation/ngx");
// import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic";
var ngx_14 = require("cordova-plugin-fcm-with-dependecy-updated/ionic/ngx");
var shared_module_1 = require("./modules/shared/shared.module");
var ngx_15 = require("@ionic-native/in-app-browser/ngx");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
            ],
            entryComponents: [],
            imports: [
                platform_browser_1.BrowserModule,
                shared_module_1.SharedModule,
                angular_1.IonicModule.forRoot(),
                app_routing_module_1.AppRoutingModule,
                forms_1.FormsModule,
                http_1.HttpClientModule,
                forms_1.ReactiveFormsModule,
                storage_1.IonicStorageModule.forRoot(),
                ng2_pdf_viewer_1.PdfViewerModule,
                ngx_quill_1.QuillModule.forRoot({
                    customOptions: [{
                            "import": 'formats/font',
                            whitelist: ['mirza', 'roboto', 'aref', 'serif', 'sansserif', 'monospace']
                        }]
                }),
            ],
            providers: [
                ngx_3.StatusBar,
                ngx_2.SplashScreen,
                login_service_1.LoginService,
                ngx_1.Network,
                ngx_5.Camera,
                ngx_4.FileTransfer,
                ngx_6.Contacts,
                ngx_7.Facebook,
                ngx_8.GooglePlus,
                ngx_9.Device,
                ngx_14.FCM,
                ngx_10.File,
                ngx_11.DocumentViewer,
                ngx_12.Diagnostic,
                ngx_13.Geolocation,
                ngx_15.InAppBrowser,
                { provide: router_1.RouteReuseStrategy, useClass: angular_1.IonicRouteStrategy },
            ],
            bootstrap: [app_component_1.AppComponent],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
// cordova plugin add cordova-plugin-googleplus@8.5.1 --save --variable REVERSED_CLIENT_ID=com.googleusercontent.apps.584704282410-peo3u9r6to27b0r46imes0o1jo90idl2
