"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.HeaderComponent = void 0;
var core_1 = require("@angular/core");
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router, navCtrl, eventsService, notificationListService) {
        var _this = this;
        this.router = router;
        this.navCtrl = navCtrl;
        this.eventsService = eventsService;
        this.notificationListService = notificationListService;
        this.title = "";
        this.notification = false;
        this.count = 0;
        this.checkPage = false;
        this.eventsService.subscribe('notification_count', function (data) {
            _this.notificationsCount();
        });
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.notificationsCount();
    };
    HeaderComponent.prototype.notificationsCount = function () {
        var _this = this;
        // this.loaderService.showLoader()
        var formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        this.notificationListService.notificationsCount(formData).then(function (res) {
            console.log(res);
            if (res.status !== false) {
                _this.count = res.count;
                console.log("this.notification_count : ", _this.count);
                // this.loaderService.hideLoader()
            }
            else {
                // this.loaderService.hideLoader()
            }
        });
    };
    Object.defineProperty(HeaderComponent.prototype, "hasNotification", {
        get: function () {
            return (this.notification) ? true : false;
        },
        enumerable: false,
        configurable: true
    });
    HeaderComponent.prototype.notification_fun = function () {
        this.router.navigate(['tabs/notification']);
    };
    HeaderComponent.prototype.home = function () {
        this.navCtrl.navigateRoot('/tabs/tab1');
    };
    HeaderComponent.prototype.back = function () {
        this.navCtrl.back();
    };
    __decorate([
        core_1.Input()
    ], HeaderComponent.prototype, "title");
    __decorate([
        core_1.Input()
    ], HeaderComponent.prototype, "notification");
    __decorate([
        core_1.Input()
    ], HeaderComponent.prototype, "count");
    __decorate([
        core_1.Input()
    ], HeaderComponent.prototype, "checkPage");
    HeaderComponent = __decorate([
        core_1.Component({
            selector: 'header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.scss']
        })
    ], HeaderComponent);
    return HeaderComponent;
}());
exports.HeaderComponent = HeaderComponent;
