import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { EventsService } from 'src/app/services/events.service';
import { NotificationListService } from 'src/app/services/notification-list.service';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() title: string = "";
  @Input() notification: boolean = false;
  @Input() count: number = 0;
  @Input() checkPage:boolean = false;
  constructor(
    private router: Router,
    public navCtrl: NavController,
    private eventsService: EventsService,
    private notificationListService: NotificationListService
  ) {
    this.eventsService.subscribe('notification_count', (data) => {
      this.notificationsCount()
    });
   }

  ngOnInit() {
    this.notificationsCount()
  }
  notificationsCount() {
    // this.loaderService.showLoader()
    let formData = new FormData();
    formData.append("aid", localStorage.getItem('user_id'));
    this.notificationListService.notificationsCount(formData).then((res: any) => {
      console.log(res);
      if (res.status !== false) {
        this.count = res.count;
        console.log("this.notification_count : ",this.count)
        // this.loaderService.hideLoader()
      } else {
        // this.loaderService.hideLoader()
      }
    });
  }
  get hasNotification() {
    return (this.notification) ? true : false;
  }
  notification_fun() {
    this.router.navigate(['tabs/notification']);
  }
  home() {
    this.navCtrl.navigateRoot('/tabs/tab1');
  }
  back() {
    this.navCtrl.back();
  }
}
