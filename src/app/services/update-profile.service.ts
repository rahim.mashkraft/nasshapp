import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ConnectionStatus, NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class UpdateProfileService {
  apiUrl = environment.apiUrl;
  constructor(
    private http: HttpClient,
    private networkService: NetworkService
  ) { }
  updateprofile(formData) {
    return new Promise((resolve, reject) => {
      this.networkService.onNetworkChange().subscribe((status: ConnectionStatus) => {
        if (status == ConnectionStatus.Online) {
          console.log(this.apiUrl + 'updateprofile.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'updateprofile.php', formData)
            .subscribe(res => {
              resolve(res);
            }, (err) => {
              reject(err)
              console.log('something went wrong please try again');
            });
        }
      });
    });
  }
}
