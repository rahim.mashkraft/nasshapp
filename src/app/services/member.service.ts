import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getMembers(formData) {
    return new Promise((resolve, reject) => {
      // if (status == ConnectionStatus.Online) {
        console.log(this.apiUrl + 'members.php');
        formData.append("auth_token",environment.auth_token)
        this.http.post(this.apiUrl + 'members.php', formData)
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err)
            console.log('something went wrong please try again');
          });
      // }
    });
  }
}
