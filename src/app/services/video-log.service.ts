import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VideoLogService {
  apiUrl = environment.apiUrl;
  constructor(
    private http: HttpClient,
  ) { }
  videoLog(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'videolog.php');
      formData.append("auth_token", environment.auth_token)
      this.http.post(this.apiUrl + 'videolog.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
}
