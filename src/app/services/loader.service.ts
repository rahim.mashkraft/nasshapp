import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  loaderToShow: any;
  isBusy:boolean;
  constructor(
    public loadingCtrl: LoadingController,
  ) { }
  showLoader() {
    this.isBusy = true;
    // this.loaderToShow = this.loadingCtrl.create({
    //   message: 'Please Wait..'
    // }).then((res) => {
    //   res.present();
    //   // res.onDidDismiss().then((dis) => {
    //   //    console.log('Loading dismissed!',dis);
    //   // });
    // });
    // // this.hideLoader();
  }

  hideLoader() {
    // setTimeout(()=>{
    //   this.loadingCtrl.dismiss();
    // },100)
    this.isBusy = false;
  }
}
