import { TestBed } from '@angular/core/testing';

import { ContactGroupsService } from './contact-groups.service';

describe('ContactGroupsService', () => {
  let service: ContactGroupsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ContactGroupsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
