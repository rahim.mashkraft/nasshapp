import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactGroupsService {

  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient,) { }

  getContactGroups() {
    return new Promise((resolve, reject) => {
      // if (status == ConnectionStatus.Online) {
      console.log(this.apiUrl + 'contactgroups.php');
      this.http.get(this.apiUrl + 'contactgroups.php')
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
      // }
    });
  }
  getContactGroupsList(formData) {
    return new Promise((resolve, reject) => {
      // if (status == ConnectionStatus.Online) {
      console.log(this.apiUrl + 'contactgroupslist.php');
      formData.append("auth_token", environment.auth_token)
      this.http.post(this.apiUrl + 'contactgroupslist.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
      // }
    });
  }
  contactsImport(formData) {
    return new Promise((resolve, reject) => {
      // if (status == ConnectionStatus.Online) {
      console.log(this.apiUrl + 'contactsimport.php');
      formData.append("auth_token", environment.auth_token)
      this.http.post(this.apiUrl + 'contactsimport.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
      // }
    });
  }

  getContacts(formData) {
    return new Promise((resolve, reject) => {
      // if (status == ConnectionStatus.Online) {
      console.log(this.apiUrl + 'contacts.php');
      formData.append("auth_token", environment.auth_token)
      this.http.post(this.apiUrl + 'contacts.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
      // }
    });
  }
}