import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AlertController } from '@ionic/angular';
import { ConnectionStatus, NetworkService } from './network.service';
@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  apiUrl = environment.apiUrl;
  constructor(
    public alertController: AlertController,
    private http: HttpClient,
    private networkService: NetworkService
  ) { }
  getQuizCategories(formData) {
    return new Promise((resolve, reject) => {
      this.networkService.onNetworkChange().subscribe((status: ConnectionStatus) => {
        if (status == ConnectionStatus.Online) {
          console.log(this.apiUrl + 'home.php');
          formData.append("auth_token", environment.auth_token)
          this.http.post(this.apiUrl + 'home.php', formData)
            .subscribe(res => {
              resolve(res);
            }, (err) => {
              reject(err)
              console.log('something went wrong please try again');
            });
        }
      });
    });
  }
  dailygoalscheck(formData) {
    return new Promise((resolve, reject) => {
      // const httpOptions = {
      //     headers: new HttpHeaders({
      //        'Accept': 'application/json ',
      //        'Content-Type': 'application/json',
      //     })
      //   }
      console.log(this.apiUrl + 'dailygoalscheck.php');
      this.http.post(this.apiUrl + 'dailygoalscheck.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
}
