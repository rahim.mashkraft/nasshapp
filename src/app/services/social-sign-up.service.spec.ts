import { TestBed } from '@angular/core/testing';

import { SocialSignUPService } from './social-sign-up.service';

describe('SocialSignUPService', () => {
  let service: SocialSignUPService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SocialSignUPService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
