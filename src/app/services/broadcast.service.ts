import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BroadcastService {

  apiUrl = environment.apiUrl;
  constructor(
    private http: HttpClient,
  ) { }
  broadcast(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'broadcast.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'broadcast.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
  smsbroadcast(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'smsbroadcast.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'smsbroadcast.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
}
