import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ConnectionStatus, NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  apiUrl = environment.apiUrl;
  constructor(
    private http: HttpClient,
    private networkService: NetworkService
  ) { }
  checkemail(formData) {
    return new Promise((resolve, reject) => {
      this.networkService.onNetworkChange().subscribe((status: ConnectionStatus) => {
        if (status == ConnectionStatus.Online) {
          console.log(this.apiUrl + 'checkemail.php');
          formData.append("auth_token", environment.auth_token)
          this.http.post(this.apiUrl + 'checkemail.php', formData)
            .subscribe(res => {
              resolve(res);
            }, (err) => {
              reject(err)
              console.log('something went wrong please try again');
            });
        }
      });
    });
  }
  checkusername(formData) {
    return new Promise((resolve, reject) => {
      this.networkService.onNetworkChange().subscribe((status: ConnectionStatus) => {
        if (status == ConnectionStatus.Online) {
          console.log(this.apiUrl + 'checkusername.php');
          formData.append("auth_token", environment.auth_token)
          this.http.post(this.apiUrl + 'checkusername.php', formData)
            .subscribe(res => {
              resolve(res);
            }, (err) => {
              reject(err)
              console.log('something went wrong please try again');
            });
        }
      });
    });
  }
  checkphone(formData) {
    return new Promise((resolve, reject) => {
      this.networkService.onNetworkChange().subscribe((status: ConnectionStatus) => {
        if (status == ConnectionStatus.Online) {
          console.log(this.apiUrl + 'checkphone.php');
          formData.append("auth_token", environment.auth_token)
          this.http.post(this.apiUrl + 'checkphone.php', formData)
            .subscribe(res => {
              resolve(res);
            }, (err) => {
              reject(err)
              console.log('something went wrong please try again');
            });
        }
      });
    });
  }
  register(formData) {
    return new Promise((resolve, reject) => {
      this.networkService.onNetworkChange().subscribe((status: ConnectionStatus) => {
        if (status == ConnectionStatus.Online) {
          console.log(this.apiUrl + 'register.php');
          formData.append("auth_token", environment.auth_token)
          this.http.post(this.apiUrl + 'register.php', formData)
            .subscribe(res => {
              resolve(res);
            }, (err) => {
              reject(err)
              console.log('something went wrong please try again');
            });
        }
      });
    });
  }
}
