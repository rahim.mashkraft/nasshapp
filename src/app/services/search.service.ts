import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  apiUrl = environment.apiUrl;
  constructor(
    private http: HttpClient,
  ) { }
  searchContacts(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'searchcontacts.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'searchcontacts.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
}
