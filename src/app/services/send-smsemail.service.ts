import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SendSMSEmailService {
  apiUrl = environment.apiUrl;
  constructor(
    private http: HttpClient,
  ) { }
  sendemail(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'sendemail.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'sendemail.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
  sendsms(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'sendsms.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'sendsms.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
}
