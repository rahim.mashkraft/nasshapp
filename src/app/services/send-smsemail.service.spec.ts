import { TestBed } from '@angular/core/testing';

import { SendSMSEmailService } from './send-smsemail.service';

describe('SendSMSEmailService', () => {
  let service: SendSMSEmailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SendSMSEmailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
