import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  apiUrl = environment.apiUrl;
  constructor(
    private http: HttpClient,
  ) { }
  smschatcheck(formData) {
    return new Promise((resolve, reject) => {
      // console.log(this.apiUrl + 'smschatcheck.php');
      let api = "https://wvfitness.net/admin/appapi/"
      formData.append("auth_token", environment.auth_token)
      this.http.post(api + 'smschatcheck.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
  smschathistory(formData) {
    return new Promise((resolve, reject) => {
      // console.log(this.apiUrl + 'smschatcheck.php');
      let api = "https://wvfitness.net/admin/appapi/"
      formData.append("auth_token", environment.auth_token)
      this.http.post(api + 'smschathistory.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
  updatetwilio(formData) {
    return new Promise((resolve, reject) => {
      console.log(formData);
      console.log(this.apiUrl + 'updatetwilio.php');
      formData.append("auth_token", environment.auth_token)
      this.http.post(this.apiUrl + 'updatetwilio.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
}
