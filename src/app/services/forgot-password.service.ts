import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordService {
  apiUrl = environment.apiUrl;
  constructor(
    private http: HttpClient,
  ) { }

  forgotpassword(formData: any){
    return new Promise((resolve, reject) => {
      formData.append("auth_token", environment.auth_token)
      this.http.post(this.apiUrl + 'forgotpassword.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
}
