import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  apiUrl = environment.apiUrl;
  constructor(
    private http: HttpClient,
  ) { }
  getQuiz(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'video.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'video.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
  getAllQuizes(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'quizdetails.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'quizdetails.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
  saveQuizAttempt(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'video.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'video.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
  saveQuizAnswer(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'video.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'video.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
  quizdetails(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'quizdetails.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'quizdetails.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
  performquiz(formData){
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'performquiz.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'performquiz.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
}
