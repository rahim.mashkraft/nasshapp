import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(public toastCtrl: ToastController,) { }
  async showToast(message = 'Successfully Logged in!') {
    const toast = await this.toastCtrl.create({
      cssClass: 'bg-toast',
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  async showToastUpdateProfile(message: any) {
    const toast = await this.toastCtrl.create({
      cssClass: 'bg-toast',
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }
}
