import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders,  } from '@angular/common/http';
import { map , tap, catchError, } from 'rxjs/operators';
import { AlertController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { ConnectionStatus, NetworkService } from './network.service';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  apiUrl = environment.apiUrl;
  constructor(
    public alertController: AlertController,
    private http: HttpClient,
  ) { }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
    /** Log a HeroService message with the MessageService */
    private log(message: string) {
      this.presentAlertError(message);
    console.log(message);
  }
  async presentAlertError(error) {
    const alert = await this.alertController.create({
      cssClass: 'alertCustom',
      header: 'Error',
     // subHeader: 'Subtitle',
      message: error,
      buttons: ['OK']
    });

    await alert.present();
  }
  login(formData: any, ): Observable<any> {
    formData.append("auth_token",environment.auth_token)
    return this.http.post(this.apiUrl + 'login.php' , formData, )
    .pipe(
      map(res => res),
      catchError(this.handleError('login', []))
    );
  }
}
