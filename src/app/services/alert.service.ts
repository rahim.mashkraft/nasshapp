import { Injectable } from '@angular/core';
import { AlertController, LoadingController, NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  loaderObj: HTMLIonLoadingElement;

  isCheck:any = false;
  constructor(
    public alertController: AlertController,
    public loadingCtrl: LoadingController,
    private navCtrl: NavController,
    ) { 
    this.isCheck = false
  }
  async presentAlertError(error) {
    const alert = await this.alertController.create({
      cssClass: 'alertCustom',
      header: 'Warning',
      // subHeader: 'Subtitle',
      message: error,
      buttons: ['OK']
    });

    await alert.present();
  }
  async showPwdSuccessAlert(email) {
    const alert = await this.alertController.create({
      header: 'SUCCESS',
      backdropDismiss: false,
      message: `
        <p> Password reset email has been sent to ${email}.</p>
        <p> The email contains a URL that you will need to visit to confirm your email. Once you've confirmed
        your email you may reset your password.</p>
      `,
      buttons: [
        {
          text: 'Close',
          role: 'cancel'
        }
      ]
    });
    alert.present();
  }
  async presentAlertSuccess() {
    const alert = await this.alertController.create({
      header: 'Success',
      cssClass: 'alertCustom',
      // subHeader: 'You have Successfully ',
      message: 'You have successfully logged In ',
      buttons: ['OK']
    });

    await alert.present();
  }
  async forgotPass() {
    const alert = await this.alertController.create({
      header: 'Forgot Password?',
      message: 'Enter you email address to send a reset link password.',
      inputs: [
        {
          name: 'email',
          type: 'email',
          placeholder: 'Email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            // console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: async (alertData) => {
            this.showPwdLoader(alertData);
          }
        }
      ]
    });

    await alert.present();
  }
  async showPwdLoader(alertData) {
    await this.loadingCtrl.create({
      spinner: 'circles',
      animated: true,
      showBackdrop: true,
      translucent: true,
      mode: 'ios',
      message: 'Please wait...'
    }).then((load: HTMLIonLoadingElement) => {
      this.loaderObj = load;
      this.loaderObj.present().then(() => {

      });
    });
  }

  async showDisableAlert() {
    const alert = await this.alertController.create({
      backdropDismiss: false,
      message: 'Please finish workout to continue.',
      buttons: [
        {
          text: 'Okay',
          role: 'cancel'
        }
      ]
    });
    await alert.present();
  }
  async presentNetworkAlert() {
    const alert = await this.alertController.create({
      header: 'Warning',
      cssClass: 'alertCustom',
      // subHeader: 'You have Successfully ',
      message: ' No Internet Available ',
      buttons: ['OK']
    });

    await alert.present();
  }
  async generalAlert(message) {
    const alert = await this.alertController.create({
      header: 'Warning',
      cssClass: 'alertCustom',
      // subHeader: 'You have Successfully ',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }
  async contactList(message) {
    const alert = await this.alertController.create({
      // header: 'Warning',
      cssClass: 'alertCustom',
      // subHeader: 'You have Successfully ',
      message: message,
      buttons: [{
        text: 'OK',
        handler: async () => {
          this.isCheck = true
        }
      }]
    });

    await alert.present();
  }

    async showAlertForQuiz(quiz) {
    let firstTime = false;
    let attemptsRemaining = 0;
    if (quiz.is_attempt == 0) {
      firstTime = true;
    }
    if (quiz.redo && (quiz.is_attempt < quiz.redo_attempt + 1)) {
      attemptsRemaining = (quiz.redo_attempt + 1) - quiz.is_attempt;
    }

    const buttons = [];
    buttons.push(
      {
        text: 'Dismiss',
        role: 'cancel'
      }
    );

    if (firstTime || attemptsRemaining > 0) {
      buttons.push(
        {
          text: 'Continue',
          handler: () => {
            this.navCtrl.navigateForward(['home-results/quiz/start-quiz/' + quiz.id]);
          }
        }
      );
    }

    if (firstTime && attemptsRemaining == 0) {
      attemptsRemaining = 1;
    }

    const alert = await this.alertController.create({
      header: quiz.title,
      message: `
        <p>${quiz.description}</p>
        <p class="font-weight-600">Details</p>
        <ul class="text-align-left">
          <li>Time: ${this.transform(quiz.quiztime)}</li>
          <li>Total points: ${quiz.totalscore}</li>
          <li>Passing points: ${quiz.passcore}</li>
          <li>Attempt remaining: ${attemptsRemaining}</li>
        </ul>
      `,
      buttons: buttons
    });
    alert.present();
  }
  transform(time: number): string {

    if (time >= 60) {
      const timeInMinutes: number = Math.floor(time / 60);
      const hours: number = Math.floor(time / 3600);
      let minutes = timeInMinutes;
      if (minutes >= 60) {
        minutes = timeInMinutes % 60;
      }
      return (hours > 0 ? '0' + hours + 'hr ' : '')
      + (minutes < 10 ? '0' + minutes : minutes) +
      'mins ' + ((time - timeInMinutes * 60) < 10 ? '0' +
      (time - timeInMinutes * 60) : (time - timeInMinutes * 60)) + 'secs';
    } else {
      return (time < 10 ? '00: 0' + time : '00:' + time) + 'secs';
    }
  }
  async showResults(results, header = 'Time is up!') {
    const alert = await this.alertController.create({
      header: header,
      backdropDismiss: false,
      message: `
        <p> Results of your quiz are:</p>
        <ul class="text-align-left">
          <li>Points earned: ${results.totalpoints} out of ${results.totalscore}</li>
          <li>Passing score: ${results.passcore}</li>
          <li>Time elapsed: ${results.total_time}</li>
        </ul>
      `,
      buttons: [
        {
          text: 'Close',
          handler: () => {
            this.navCtrl.navigateBack(['home-results/quiz'])
          }
        }
      ]
    });
    alert.present();
  }
}
