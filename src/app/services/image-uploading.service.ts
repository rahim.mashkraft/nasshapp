import { Injectable } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { LoaderService } from './loader.service';
import { environment } from 'src/environments/environment';
import { ToastService } from './toast.service';
import { EventsService } from './events.service';

@Injectable({
  providedIn: 'root'
})
export class ImageUploadingService {
  imgData: any;
  member_name: any;
  apiUrl = environment.apiUrl + 'updateprofileimage.php';
  constructor(
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public transfer: FileTransfer,
    private loaderService: LoaderService,
    private toastService: ToastService,
    private eventsService: EventsService
  ) {
    this.member_name = localStorage.getItem('name');
  }
  async uploadPhoto() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'Choose image from',
      cssClass: 'alertCustom',
      buttons: [
        {
          text: 'Camera',
          role: 'destructive',
          handler: () => {
            // console.log('Destructive clicked');
            this.launchCamera('camera');
          }
        }, {
          text: 'Gallery',
          handler: () => {
            this.launchCamera('gallery');
            // console.log('Archive clicked');
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            // console.log('Cancel clicked');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  launchCamera(source) {
    const options: CameraOptions = {
      quality: 80,
      sourceType:
        source === 'camera'
          ? this.camera.PictureSourceType.CAMERA
          : this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetHeight: 400,
      targetWidth: 400,
      correctOrientation: true,
      allowEdit: false
    };

    this.camera.getPicture(options).then(
      async imageData => {

        const base64Image = 'data:image/png;base64,' + imageData;
        this.imgData = base64Image;
        console.log("imData : ",this.imgData);

        this.uploadImg();
      },
      err => {
        console.log(err);
      });
  }
  imagedata
  profileImage
  async uploadImg() {
    this.loaderService.showLoader();
    const fileTransfer: FileTransferObject = this.transfer.create();
    const imgData: FileUploadOptions = {
      fileKey: 'image',
      // fileName: 'name.png',
      fileName: Date.now()+localStorage.getItem('user_id')+'.jpg',
      chunkedMode: false,
      mimeType: 'image/jpeg',
      headers: {}
    };
    console.log("imgData 1 ",imgData);
    console.log(this.apiUrl + '?aid=' + localStorage.getItem('user_id'));
    return fileTransfer.upload(this.imgData, this.apiUrl + '?aid=' + localStorage.getItem('user_id'), imgData)
    .then((data) => {
     console.log("data : ",data);

      this.imagedata = JSON.parse(data.response);
      this.profileImage = this.imagedata.image;
      localStorage.removeItem('profileimage')
      localStorage.setItem('profileimage' , this.profileImage);
      console.log("profileImage : ",this.profileImage);
      this.loaderService.hideLoader();
      this.toastService.showToast('Picture Update Successfully');
      const userData = {
        profileimage : this.imagedata.image
      };
      this.eventsService.publish('pictureChanged', userData);
    //   // alert("success");
    //  // loading.dismiss();
    //  // this.GetProfilePhoto();
    //   // this.navCtrl.push(ElectronicPage);
    }, (err) => {
      this.loaderService.hideLoader();
       console.log(err);
     // loading.dismiss();
      // this.navCtrl.push(ElectronicPage);
      // alert("error" + JSON.stringify(err));
    });
  }
}
