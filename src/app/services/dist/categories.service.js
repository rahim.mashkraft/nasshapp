"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CategoriesService = void 0;
var core_1 = require("@angular/core");
var environment_1 = require("src/environments/environment");
var network_service_1 = require("./network.service");
var CategoriesService = /** @class */ (function () {
    function CategoriesService(alertController, http, networkService) {
        this.alertController = alertController;
        this.http = http;
        this.networkService = networkService;
        this.apiUrl = environment_1.environment.apiUrl;
    }
    CategoriesService.prototype.getQuizCategories = function (formData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.networkService.onNetworkChange().subscribe(function (status) {
                if (status == network_service_1.ConnectionStatus.Online) {
                    console.log(_this.apiUrl + 'home.php');
                    formData.append("auth_token", environment_1.environment.auth_token);
                    _this.http.post(_this.apiUrl + 'home.php', formData)
                        .subscribe(function (res) {
                        resolve(res);
                    }, function (err) {
                        reject(err);
                        console.log('something went wrong please try again');
                    });
                }
            });
        });
    };
    CategoriesService.prototype.dailygoalscheck = function (formData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // const httpOptions = {
            //     headers: new HttpHeaders({
            //        'Accept': 'application/json ',
            //        'Content-Type': 'application/json',
            //     })
            //   }
            console.log(_this.apiUrl + 'dailygoalscheck.php');
            _this.http.post(_this.apiUrl + 'dailygoalscheck.php', formData)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
            });
        });
    };
    CategoriesService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], CategoriesService);
    return CategoriesService;
}());
exports.CategoriesService = CategoriesService;
