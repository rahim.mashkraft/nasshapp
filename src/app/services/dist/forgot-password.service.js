"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ForgotPasswordService = void 0;
var core_1 = require("@angular/core");
var environment_1 = require("src/environments/environment");
var ForgotPasswordService = /** @class */ (function () {
    function ForgotPasswordService(http) {
        this.http = http;
        this.apiUrl = environment_1.environment.apiUrl;
    }
    ForgotPasswordService.prototype.forgotpassword = function (formData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            formData.append("auth_token", environment_1.environment.auth_token);
            _this.http.post(_this.apiUrl + 'forgotpassword.php', formData)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
            });
        });
    };
    ForgotPasswordService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], ForgotPasswordService);
    return ForgotPasswordService;
}());
exports.ForgotPasswordService = ForgotPasswordService;
