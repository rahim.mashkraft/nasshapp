"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.ImageUploadingService = void 0;
var core_1 = require("@angular/core");
var environment_1 = require("src/environments/environment");
var ImageUploadingService = /** @class */ (function () {
    function ImageUploadingService(camera, actionSheetCtrl, transfer, loaderService, toastService, eventsService) {
        this.camera = camera;
        this.actionSheetCtrl = actionSheetCtrl;
        this.transfer = transfer;
        this.loaderService = loaderService;
        this.toastService = toastService;
        this.eventsService = eventsService;
        this.apiUrl = environment_1.environment.apiUrl + 'updateprofileimage.php';
        this.member_name = localStorage.getItem('name');
    }
    ImageUploadingService.prototype.uploadPhoto = function () {
        return __awaiter(this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetCtrl.create({
                            header: 'Choose image from',
                            cssClass: 'alertCustom',
                            buttons: [
                                {
                                    text: 'Camera',
                                    role: 'destructive',
                                    handler: function () {
                                        // console.log('Destructive clicked');
                                        _this.launchCamera('camera');
                                    }
                                }, {
                                    text: 'Gallery',
                                    handler: function () {
                                        _this.launchCamera('gallery');
                                        // console.log('Archive clicked');
                                    }
                                }, {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    handler: function () {
                                        // console.log('Cancel clicked');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ImageUploadingService.prototype.launchCamera = function (source) {
        var _this = this;
        var options = {
            quality: 80,
            sourceType: source === 'camera'
                ? this.camera.PictureSourceType.CAMERA
                : this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            targetHeight: 400,
            targetWidth: 400,
            correctOrientation: true,
            allowEdit: false
        };
        this.camera.getPicture(options).then(function (imageData) { return __awaiter(_this, void 0, void 0, function () {
            var base64Image;
            return __generator(this, function (_a) {
                base64Image = 'data:image/png;base64,' + imageData;
                this.imgData = base64Image;
                console.log("imData : ", this.imgData);
                this.uploadImg();
                return [2 /*return*/];
            });
        }); }, function (err) {
            console.log(err);
        });
    };
    ImageUploadingService.prototype.uploadImg = function () {
        return __awaiter(this, void 0, void 0, function () {
            var fileTransfer, imgData;
            var _this = this;
            return __generator(this, function (_a) {
                this.loaderService.showLoader();
                fileTransfer = this.transfer.create();
                imgData = {
                    fileKey: 'image',
                    // fileName: 'name.png',
                    fileName: Date.now() + localStorage.getItem('user_id') + '.jpg',
                    chunkedMode: false,
                    mimeType: 'image/jpeg',
                    headers: {}
                };
                console.log("imgData 1 ", imgData);
                console.log(this.apiUrl + '?aid=' + localStorage.getItem('user_id'));
                return [2 /*return*/, fileTransfer.upload(this.imgData, this.apiUrl + '?aid=' + localStorage.getItem('user_id'), imgData)
                        .then(function (data) {
                        console.log("data : ", data);
                        _this.imagedata = JSON.parse(data.response);
                        _this.profileImage = _this.imagedata.image;
                        localStorage.removeItem('profileimage');
                        localStorage.setItem('profileimage', _this.profileImage);
                        console.log("profileImage : ", _this.profileImage);
                        _this.loaderService.hideLoader();
                        _this.toastService.showToast('Picture Update Successfully');
                        var userData = {
                            profileimage: _this.imagedata.image
                        };
                        _this.eventsService.publish('pictureChanged', userData);
                        //   // alert("success");
                        //  // loading.dismiss();
                        //  // this.GetProfilePhoto();
                        //   // this.navCtrl.push(ElectronicPage);
                    }, function (err) {
                        _this.loaderService.hideLoader();
                        console.log(err);
                        // loading.dismiss();
                        // this.navCtrl.push(ElectronicPage);
                        // alert("error" + JSON.stringify(err));
                    })];
            });
        });
    };
    ImageUploadingService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], ImageUploadingService);
    return ImageUploadingService;
}());
exports.ImageUploadingService = ImageUploadingService;
