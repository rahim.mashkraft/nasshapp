"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.LoaderService = void 0;
var core_1 = require("@angular/core");
var LoaderService = /** @class */ (function () {
    function LoaderService(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    LoaderService.prototype.showLoader = function () {
        this.isBusy = true;
        // this.loaderToShow = this.loadingCtrl.create({
        //   message: 'Please Wait..'
        // }).then((res) => {
        //   res.present();
        //   // res.onDidDismiss().then((dis) => {
        //   //    console.log('Loading dismissed!',dis);
        //   // });
        // });
        // // this.hideLoader();
    };
    LoaderService.prototype.hideLoader = function () {
        // setTimeout(()=>{
        //   this.loadingCtrl.dismiss();
        // },100)
        this.isBusy = false;
    };
    LoaderService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], LoaderService);
    return LoaderService;
}());
exports.LoaderService = LoaderService;
