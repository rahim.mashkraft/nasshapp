"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ChatService = void 0;
var core_1 = require("@angular/core");
var environment_1 = require("src/environments/environment");
var ChatService = /** @class */ (function () {
    function ChatService(http) {
        this.http = http;
        this.apiUrl = environment_1.environment.apiUrl;
    }
    ChatService.prototype.smschatcheck = function (formData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // console.log(this.apiUrl + 'smschatcheck.php');
            var api = "https://wvfitness.net/admin/appapi/";
            formData.append("auth_token", environment_1.environment.auth_token);
            _this.http.post(api + 'smschatcheck.php', formData)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
            });
        });
    };
    ChatService.prototype.smschathistory = function (formData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // console.log(this.apiUrl + 'smschatcheck.php');
            var api = "https://wvfitness.net/admin/appapi/";
            formData.append("auth_token", environment_1.environment.auth_token);
            _this.http.post(api + 'smschathistory.php', formData)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
            });
        });
    };
    ChatService.prototype.updatetwilio = function (formData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log(formData);
            console.log(_this.apiUrl + 'updatetwilio.php');
            formData.append("auth_token", environment_1.environment.auth_token);
            _this.http.post(_this.apiUrl + 'updatetwilio.php', formData)
                .subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
            });
        });
    };
    ChatService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], ChatService);
    return ChatService;
}());
exports.ChatService = ChatService;
