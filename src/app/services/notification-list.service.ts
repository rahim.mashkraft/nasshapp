import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NotificationListService {
  apiUrl = environment.apiUrl;
  constructor(
    private http: HttpClient,
  ) { }
  notificationList(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'pushnotifications.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'pushnotifications.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
  notificationsCount(formData) {
    return new Promise((resolve, reject) => {
      console.log(this.apiUrl + 'notificationscount.php');
      formData.append("auth_token",environment.auth_token)
      this.http.post(this.apiUrl + 'notificationscount.php', formData)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err)
          console.log('something went wrong please try again');
        });
    });
  }
}
