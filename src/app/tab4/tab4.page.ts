import { IframePage } from './../pages/iframe/iframe.page';
import { ChatService } from './../services/chat.service';
import { TwillioModelPage } from './../pages/twillio-model/twillio-model.page';
import { NavigationExtras, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LoaderService } from '../services/loader.service';
import { ImageModelPage } from '../pages/image-model/image-model.page';
import { EventsService } from '../services/events.service';

// Define On Top 
declare var HKVideoPlayer;
@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  public isCheck: boolean;
  public imageArray: any = [
    {
      imageUrl: '',
      title: ''
    },
    {
      imageUrl: '',
      title: ''
    },
    {
      imageUrl: '',
      title: ''
    },
    {
      imageUrl: '',
      title: ''
    },
  ];;
  public videoLink: any;

  public account_SID: any;
  public auth_Token: any;
  public twilio_Number: any;
  public chatList: any;
  public affiliatenumber: any;

  public sortBy: any;
  public search_value: any;
  public currentPage = 1;
  public avatar_url: any = "../assets/user-placeholder.png";
  constructor(
    public modalController: ModalController,
    private router: Router,
    private chatService: ChatService,
    public loaderService: LoaderService,
    private eventsService: EventsService
  ) {
    this.sortBy = 'n';
    this.eventsService.subscribe('notification_receive', (data) => {
      this.currentPage = 1;
      this.smschatcheckEvent(this.currentPage);
    });
  }
  smschatcheckEvent(currentPage) {
    let formData = new FormData();
    formData.append("aid", "531");
    formData.append("PageNumber", currentPage);
    this.chatService.smschatcheck(formData).then((data: any) => {
      console.log("smschatcheckEvent : ", data);
      if (data.status) {
        this.isCheck = true;
        this.chatList = data.contacts;
        this.affiliatenumber = data.affiliatenumber;
      }
    })
  }

  setFilteredItems(ev: any, currentPage) {
    console.log(currentPage)
    const val = ev.target.value;
    console.log(val)
    var str = new String(val);
    var len = str.length;
    // if the value is an empty string don't filter the items
    if (len >= 3 && val && val.trim() !== '') {
      if (this.sortBy == 'n') {
        this.smschatcheck(this.currentPage, this.search_value, '');
      } else {
        this.smschatcheck(this.currentPage, this.search_value, this.sortBy);
      }
    }
  }
  claerSearcher() {
    console.log("claerSearcher")
    // this.contactsData = [];
    this.currentPage = 1;
    this.smschatcheck(this.currentPage, '', this.sortBy);
  }
  sortFunction(sortBy) {
    console.log(sortBy)
    if (sortBy == 'n') {
      this.smschatcheck(this.currentPage, this.search_value, '');
    } else {
      this.smschatcheck(this.currentPage, this.search_value, this.sortBy);
    }
  }
  ionViewWillEnter() {
    this.imageArray = [
      {
        imageUrl: '',
        title: ''
      },
      {
        imageUrl: '',
        title: ''
      },
      {
        imageUrl: '',
        title: ''
      },
      {
        imageUrl: '',
        title: ''
      },
    ];
    this.smschatcheck(this.currentPage, '', '');
  }
  ngOnInit() {
  }

  smschatcheck(currentPage, search_value, sort_value, event = null) {
    this.loaderService.showLoader();
    let formData = new FormData();
    // formData.append("aid", localStorage.getItem('user_id'));
    formData.append("aid", "531");
    formData.append("PageNumber", currentPage);
    if (search_value) {
      formData.append("searchstring", search_value);
    }
    if (sort_value) {
      formData.append("sort", sort_value);
    }
    this.chatService.smschatcheck(formData).then((data: any) => {
      console.log("data : ", data);
      if (data.status) {
        this.isCheck = true;
        this.chatList = data.contacts;
        this.affiliatenumber = data.affiliatenumber;
        this.loaderService.hideLoader();
      } else {
        this.imageArray[0].imageUrl = data?.image1;
        this.imageArray[0].title = 'Login to twilio';
        this.imageArray[1].imageUrl = data?.image2;
        this.imageArray[1].title = 'Click on PhoneNumbers';
        this.imageArray[2].imageUrl = data.image3;
        this.imageArray[2].title = 'Click on your Twilio phone number';
        this.imageArray[3].imageUrl = data.image4;
        this.imageArray[3].title = 'Enter this url into field labeled "A MESSAGE COMES IN":';
        this.videoLink = data.videolink;
        this.isCheck = false;
        this.loaderService.hideLoader();
        console.log("imageArray : ", this.imageArray)
        // this.twillioModel(this.imageArray, this.videoLink);
      }
      if (event) {
        event.target.complete();
      }
    }, err => {
      this.isCheck = false;
      this.loaderService.hideLoader();
      if (event) {
        event.target.complete();
      }
    });
  }
  async twillioModel(imageArray, videoLink) {
    const modal = await this.modalController.create({
      component: TwillioModelPage,
      componentProps: {
        imageArray: imageArray,
        videoLink: videoLink
      }
    })
    modal.onDidDismiss().then((data) => {
      console.log(data);
    })
    await modal.present();
  }
  goToChat(contactnumber, firstname, lastname) {
    let navigationExtras: NavigationExtras = {
      state: {
        affiliatenumber: this.affiliatenumber,
        contactnumber: contactnumber,
        firstname: firstname,
        lastname: lastname
      }
    };
    this.router.navigate(['chat'], navigationExtras)
  }
  refreshDataList(event) {
    this.currentPage = 1
    this.smschatcheck(this.currentPage, '', '', event);
  }

  //////////////////////////// TWILIO  /////////////////////////
  async playVideo() {
    // HKVideoPlayer.play(this.videoLink);
    const modal = await this.modalController.create({
      component: IframePage,
      componentProps: {
        videoLink: this.videoLink,
      }
    })
    modal.onDidDismiss().then((data) => {
      console.log(data);
    })
    await modal.present();
  }
  countLength(value) {
    const tempArray = value.toString();
    const length = tempArray.length;
    return length;
  }
  validation() {
    if (this.account_SID && this.auth_Token && this.twilio_Number) {

    }
  }
  update() {
    console.log(this.account_SID)
    console.log(this.auth_Token)
    console.log(this.twilio_Number)
    if (this.account_SID && this.auth_Token && this.twilio_Number) {
      if (this.countLength(this.twilio_Number) < 10) {
        alert("plase add min or max 10 digits Twilio number")
      } else {
        this.updatetwilio();
      }
    } else {
      alert("Please fill all fields")
    }
  }
  updatetwilio() {
    this.loaderService.showLoader();
    let formData = new FormData();
    formData.append("aid", localStorage.getItem('user_id'));
    formData.append("accountsid", this.account_SID);
    formData.append("authtoken", this.auth_Token);
    // formData.append("fromnumber", "+1" + this.twilio_Number);
    formData.append("fromnumber", this.twilio_Number);
    this.chatService.updatetwilio(formData).then((data: any) => {
      console.log("data : ", data);
      if (data.status) {
        this.loaderService.hideLoader();
      } else {
        this.loaderService.hideLoader();
      }
    }, err => {
      this.loaderService.hideLoader();
    });
  }
  public onKeyUp(event: any) {
    let newValue = event.target.value;
    // console.log(event.target.value);
    let regExp = new RegExp('^([0-9_\\-]+)$');
    if (!regExp.test(newValue)) {
      event.target.value = newValue.slice(11, -1);
    }
  }
  async openImage() {
    // const modal = await this.modalController.create({
    //   component: ImageModelPage,
    //   componentProps: {
    //     imageArray: this.imageArray,
    //   }
    // })
    // modal.onDidDismiss().then((data) => {
    //   console.log(data);
    // })
    // await modal.present();
    let navigationExtras: NavigationExtras = {
      state: {
        imageArray: this.imageArray
      }
    };
    this.router.navigate(['image-model'], navigationExtras);
  }
}
