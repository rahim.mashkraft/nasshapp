"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.Tab4Page = void 0;
var iframe_page_1 = require("./../pages/iframe/iframe.page");
var twillio_model_page_1 = require("./../pages/twillio-model/twillio-model.page");
var core_1 = require("@angular/core");
var Tab4Page = /** @class */ (function () {
    function Tab4Page(modalController, router, chatService, loaderService, eventsService) {
        var _this = this;
        this.modalController = modalController;
        this.router = router;
        this.chatService = chatService;
        this.loaderService = loaderService;
        this.eventsService = eventsService;
        this.imageArray = [
            {
                imageUrl: '',
                title: ''
            },
            {
                imageUrl: '',
                title: ''
            },
            {
                imageUrl: '',
                title: ''
            },
            {
                imageUrl: '',
                title: ''
            },
        ];
        this.currentPage = 1;
        this.avatar_url = "../assets/user-placeholder.png";
        this.sortBy = 'n';
        this.eventsService.subscribe('notification_receive', function (data) {
            _this.currentPage = 1;
            _this.smschatcheckEvent(_this.currentPage);
        });
    }
    ;
    Tab4Page.prototype.smschatcheckEvent = function (currentPage) {
        var _this = this;
        var formData = new FormData();
        formData.append("aid", "531");
        formData.append("PageNumber", currentPage);
        this.chatService.smschatcheck(formData).then(function (data) {
            console.log("smschatcheckEvent : ", data);
            if (data.status) {
                _this.isCheck = true;
                _this.chatList = data.contacts;
                _this.affiliatenumber = data.affiliatenumber;
            }
        });
    };
    Tab4Page.prototype.setFilteredItems = function (ev, currentPage) {
        console.log(currentPage);
        var val = ev.target.value;
        console.log(val);
        var str = new String(val);
        var len = str.length;
        // if the value is an empty string don't filter the items
        if (len >= 3 && val && val.trim() !== '') {
            if (this.sortBy == 'n') {
                this.smschatcheck(this.currentPage, this.search_value, '');
            }
            else {
                this.smschatcheck(this.currentPage, this.search_value, this.sortBy);
            }
        }
    };
    Tab4Page.prototype.claerSearcher = function () {
        console.log("claerSearcher");
        // this.contactsData = [];
        this.currentPage = 1;
        this.smschatcheck(this.currentPage, '', this.sortBy);
    };
    Tab4Page.prototype.sortFunction = function (sortBy) {
        console.log(sortBy);
        if (sortBy == 'n') {
            this.smschatcheck(this.currentPage, this.search_value, '');
        }
        else {
            this.smschatcheck(this.currentPage, this.search_value, this.sortBy);
        }
    };
    Tab4Page.prototype.ionViewWillEnter = function () {
        this.imageArray = [
            {
                imageUrl: '',
                title: ''
            },
            {
                imageUrl: '',
                title: ''
            },
            {
                imageUrl: '',
                title: ''
            },
            {
                imageUrl: '',
                title: ''
            },
        ];
        this.smschatcheck(this.currentPage, '', '');
    };
    Tab4Page.prototype.ngOnInit = function () {
    };
    Tab4Page.prototype.smschatcheck = function (currentPage, search_value, sort_value, event) {
        var _this = this;
        if (event === void 0) { event = null; }
        this.loaderService.showLoader();
        var formData = new FormData();
        // formData.append("aid", localStorage.getItem('user_id'));
        formData.append("aid", "531");
        formData.append("PageNumber", currentPage);
        if (search_value) {
            formData.append("searchstring", search_value);
        }
        if (sort_value) {
            formData.append("sort", sort_value);
        }
        this.chatService.smschatcheck(formData).then(function (data) {
            console.log("data : ", data);
            if (data.status) {
                _this.isCheck = true;
                _this.chatList = data.contacts;
                _this.affiliatenumber = data.affiliatenumber;
                _this.loaderService.hideLoader();
            }
            else {
                _this.imageArray[0].imageUrl = data === null || data === void 0 ? void 0 : data.image1;
                _this.imageArray[0].title = 'Login to twilio';
                _this.imageArray[1].imageUrl = data === null || data === void 0 ? void 0 : data.image2;
                _this.imageArray[1].title = 'Click on PhoneNumbers';
                _this.imageArray[2].imageUrl = data.image3;
                _this.imageArray[2].title = 'Click on your Twilio phone number';
                _this.imageArray[3].imageUrl = data.image4;
                _this.imageArray[3].title = 'Enter this url into field labeled "A MESSAGE COMES IN":';
                _this.videoLink = data.videolink;
                _this.isCheck = false;
                _this.loaderService.hideLoader();
                console.log("imageArray : ", _this.imageArray);
                // this.twillioModel(this.imageArray, this.videoLink);
            }
            if (event) {
                event.target.complete();
            }
        }, function (err) {
            _this.isCheck = false;
            _this.loaderService.hideLoader();
            if (event) {
                event.target.complete();
            }
        });
    };
    Tab4Page.prototype.twillioModel = function (imageArray, videoLink) {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: twillio_model_page_1.TwillioModelPage,
                            componentProps: {
                                imageArray: imageArray,
                                videoLink: videoLink
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (data) {
                            console.log(data);
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Tab4Page.prototype.goToChat = function (contactnumber, firstname, lastname) {
        var navigationExtras = {
            state: {
                affiliatenumber: this.affiliatenumber,
                contactnumber: contactnumber,
                firstname: firstname,
                lastname: lastname
            }
        };
        this.router.navigate(['chat'], navigationExtras);
    };
    Tab4Page.prototype.refreshDataList = function (event) {
        this.currentPage = 1;
        this.smschatcheck(this.currentPage, '', '', event);
    };
    //////////////////////////// TWILIO  /////////////////////////
    Tab4Page.prototype.playVideo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: iframe_page_1.IframePage,
                            componentProps: {
                                videoLink: this.videoLink
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (data) {
                            console.log(data);
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Tab4Page.prototype.countLength = function (value) {
        var tempArray = value.toString();
        var length = tempArray.length;
        return length;
    };
    Tab4Page.prototype.validation = function () {
        if (this.account_SID && this.auth_Token && this.twilio_Number) {
        }
    };
    Tab4Page.prototype.update = function () {
        console.log(this.account_SID);
        console.log(this.auth_Token);
        console.log(this.twilio_Number);
        if (this.account_SID && this.auth_Token && this.twilio_Number) {
            if (this.countLength(this.twilio_Number) < 10) {
                alert("plase add min or max 10 digits Twilio number");
            }
            else {
                this.updatetwilio();
            }
        }
        else {
            alert("Please fill all fields");
        }
    };
    Tab4Page.prototype.updatetwilio = function () {
        var _this = this;
        this.loaderService.showLoader();
        var formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("accountsid", this.account_SID);
        formData.append("authtoken", this.auth_Token);
        // formData.append("fromnumber", "+1" + this.twilio_Number);
        formData.append("fromnumber", this.twilio_Number);
        this.chatService.updatetwilio(formData).then(function (data) {
            console.log("data : ", data);
            if (data.status) {
                _this.loaderService.hideLoader();
            }
            else {
                _this.loaderService.hideLoader();
            }
        }, function (err) {
            _this.loaderService.hideLoader();
        });
    };
    Tab4Page.prototype.onKeyUp = function (event) {
        var newValue = event.target.value;
        // console.log(event.target.value);
        var regExp = new RegExp('^([0-9_\\-]+)$');
        if (!regExp.test(newValue)) {
            event.target.value = newValue.slice(11, -1);
        }
    };
    Tab4Page.prototype.openImage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var navigationExtras;
            return __generator(this, function (_a) {
                navigationExtras = {
                    state: {
                        imageArray: this.imageArray
                    }
                };
                this.router.navigate(['image-model'], navigationExtras);
                return [2 /*return*/];
            });
        });
    };
    Tab4Page = __decorate([
        core_1.Component({
            selector: 'app-tab4',
            templateUrl: './tab4.page.html',
            styleUrls: ['./tab4.page.scss']
        })
    ], Tab4Page);
    return Tab4Page;
}());
exports.Tab4Page = Tab4Page;
