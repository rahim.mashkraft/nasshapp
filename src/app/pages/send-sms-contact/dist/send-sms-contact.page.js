"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SendSmsContactPage = void 0;
var core_1 = require("@angular/core");
var SendSmsContactPage = /** @class */ (function () {
    function SendSmsContactPage(router, route, loaderService, sendSmsemailService, toastService, navCtrl) {
        var _this = this;
        this.router = router;
        this.route = route;
        this.loaderService = loaderService;
        this.sendSmsemailService = sendSmsemailService;
        this.toastService = toastService;
        this.navCtrl = navCtrl;
        // error_variable
        this.Phone_error = '';
        this.name_error = '';
        this.message_error = '';
        this.check_validation = false;
        this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
                _this.item_obj = _this.router.getCurrentNavigation().extras.state.item_obj;
                _this.pageName = _this.router.getCurrentNavigation().extras.state.pageName;
                console.log("this.item_obj : ", _this.item_obj);
                _this.phone_number = _this.item_obj.phone;
                _this.name = _this.item_obj.firstname + ' ' + _this.item_obj.lastname;
                if (_this.pageName == 'member') {
                    _this.affaid = _this.item_obj.affaid;
                }
                else {
                    _this.cid = _this.item_obj.cid;
                }
            }
        });
    }
    SendSmsContactPage.prototype.ngOnInit = function () {
    };
    SendSmsContactPage.prototype.validation = function () {
        if (this.phone_number == '') {
            this.Phone_error = 'This field is required';
            return;
        }
        if (this.name == '') {
            this.name_error = 'This field is required';
            return;
        }
        if (this.message == '') {
            this.message_error = 'This field is required';
            return;
        }
        this.check_validation = true;
    };
    SendSmsContactPage.prototype.sendsms = function () {
        var _this = this;
        this.validation();
        if (this.check_validation == true) {
            this.loaderService.showLoader();
            var formData = new FormData();
            formData.append("aid", localStorage.getItem('user_id'));
            if (this.pageName == 'member') {
                formData.append("affaid", this.affaid);
            }
            else {
                formData.append("cid", this.cid);
            }
            formData.append("name", this.name);
            formData.append("phone", this.phone_number);
            formData.append("message", this.message);
            this.sendSmsemailService.sendsms(formData).then(function (res) {
                console.log(res);
                if (res.status !== false) {
                    _this.toastService.showToast(res.message);
                    _this.loaderService.hideLoader();
                    // this.navCtrl.navigateRoot('/tabs');
                    _this.back();
                    // this.contact_groups = res.contactgroup
                }
                else {
                    _this.toastService.showToast(res.message);
                    _this.loaderService.hideLoader();
                }
            });
        }
    };
    SendSmsContactPage.prototype.back = function () {
        this.navCtrl.back();
    };
    SendSmsContactPage = __decorate([
        core_1.Component({
            selector: 'app-send-sms-contact',
            templateUrl: './send-sms-contact.page.html',
            styleUrls: ['./send-sms-contact.page.scss']
        })
    ], SendSmsContactPage);
    return SendSmsContactPage;
}());
exports.SendSmsContactPage = SendSmsContactPage;
