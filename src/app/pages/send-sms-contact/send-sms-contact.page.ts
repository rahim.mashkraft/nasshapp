import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { LoaderService } from 'src/app/services/loader.service';
import { SendSMSEmailService } from 'src/app/services/send-smsemail.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-send-sms-contact',
  templateUrl: './send-sms-contact.page.html',
  styleUrls: ['./send-sms-contact.page.scss'],
})
export class SendSmsContactPage implements OnInit {
  // error_variable
  public Phone_error: any = '';
  public name_error: any = '';
  public message_error: any = '';
  public check_validation: any = false;

  public phone_number: any;
  public name: any;
  public message: any;
  public cid: any;
  public affaid: any;

  public item_obj: any;
  public pageName: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public loaderService: LoaderService,
    private sendSmsemailService: SendSMSEmailService,
    private toastService: ToastService,
    private navCtrl: NavController
  ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.item_obj = this.router.getCurrentNavigation().extras.state.item_obj;
        this.pageName = this.router.getCurrentNavigation().extras.state.pageName;
        console.log("this.item_obj : ", this.item_obj)
        this.phone_number = this.item_obj.phone;
        this.name = this.item_obj.firstname + ' ' + this.item_obj.lastname;
        if (this.pageName == 'member') {
          this.affaid = this.item_obj.affaid;
        } else {
          this.cid = this.item_obj.cid;
        }

      }
    });
  }

  ngOnInit() {
  }

  validation() {
    if (this.phone_number == '') {
      this.Phone_error = 'This field is required'
      return;
    }
    if (this.name == '') {
      this.name_error = 'This field is required'
      return;
    }
    if (this.message == '') {
      this.message_error = 'This field is required'
      return;
    }

    this.check_validation = true
  }
  sendsms() {
    this.validation();
    if (this.check_validation == true) {
      this.loaderService.showLoader()
      let formData = new FormData();
      formData.append("aid", localStorage.getItem('user_id'));
      if (this.pageName == 'member') {
        formData.append("affaid", this.affaid);
      } else {
        formData.append("cid", this.cid);
      }
      formData.append("name", this.name);
      formData.append("phone", this.phone_number);
      formData.append("message", this.message);
      this.sendSmsemailService.sendsms(formData).then((res: any) => {
        console.log(res);
        if (res.status !== false) {
          this.toastService.showToast(res.message)
          this.loaderService.hideLoader()
          // this.navCtrl.navigateRoot('/tabs');
          this.back()
          // this.contact_groups = res.contactgroup
        } else {
          this.toastService.showToast(res.message)
          this.loaderService.hideLoader()
        }
      });
    }
  }
  back() {
    this.navCtrl.back();
  }

}
