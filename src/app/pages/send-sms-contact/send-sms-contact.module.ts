import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SendSmsContactPageRoutingModule } from './send-sms-contact-routing.module';

import { SendSmsContactPage } from './send-sms-contact.page';
import { SharedModule } from 'src/app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SendSmsContactPageRoutingModule,
    SharedModule
  ],
  declarations: [SendSmsContactPage]
})
export class SendSmsContactPageModule {}
