import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SendSmsContactPage } from './send-sms-contact.page';

const routes: Routes = [
  {
    path: '',
    component: SendSmsContactPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SendSmsContactPageRoutingModule {}
