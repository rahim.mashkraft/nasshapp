import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContactGroupsPageRoutingModule } from './contact-groups-routing.module';

import { ContactGroupsPage } from './contact-groups.page';
import { SharedModule } from 'src/app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactGroupsPageRoutingModule,
    SharedModule
  ],
  declarations: [ContactGroupsPage]
})
export class ContactGroupsPageModule {}
