import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ContactGroupsService } from 'src/app/services/contact-groups.service';
import { LoaderService } from 'src/app/services/loader.service';
import { ModelPage } from '../model/model.page';

@Component({
  selector: 'app-contact-groups',
  templateUrl: './contact-groups.page.html',
  styleUrls: ['./contact-groups.page.scss'],
})
export class ContactGroupsPage implements OnInit {

  public dataObj:any;
  public currentPage:any = 1;
  public lastPage:any = 1;
  public contactsGroupsData:any;
  public isCheck: any =  false;

  public contactsData:any;
  constructor(
    public loaderService: LoaderService,
    private contactGroupsService: ContactGroupsService,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    this.getContactGroups(this.currentPage)
  }
  getContactGroups(currentPage, event = null) {
    this.loaderService.showLoader();
    let formData = new FormData();
    formData.append("PageNumber", currentPage);

    formData.append("aid", localStorage.getItem('user_id'));
    this.contactGroupsService.getContactGroupsList(formData).then((data: any) => {
      console.log("data : ", data)
      this.dataObj = data
      this.currentPage = this.dataObj.CurrentPage;
      this.lastPage = this.dataObj.total;
      if (this.currentPage == 1) {
        this.contactsGroupsData = this.dataObj.contactgroups;
        if(this.contactsGroupsData.length == 0){
          this.isCheck = true;
        }
      } else {
        this.contactsGroupsData = [...this.contactsGroupsData, ...this.dataObj.contactgroups];
      }
      this.loaderService.hideLoader();
      if (event) {
        event.target.complete();
      }
    },err => {
        this.loaderService.hideLoader();
        this.isCheck = true;
        if (event) {
          event.target.complete();
        }
      });
  }
  async addContact(){
    const modal = await this.modalController.create({
      component: ModelPage,
      cssClass: 'my-custom-modal-class'
    });
    return await modal.present();
  }
}
