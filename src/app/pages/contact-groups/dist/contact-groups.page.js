"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.ContactGroupsPage = void 0;
var core_1 = require("@angular/core");
var model_page_1 = require("../model/model.page");
var ContactGroupsPage = /** @class */ (function () {
    function ContactGroupsPage(loaderService, contactGroupsService, modalController) {
        this.loaderService = loaderService;
        this.contactGroupsService = contactGroupsService;
        this.modalController = modalController;
        this.currentPage = 1;
        this.lastPage = 1;
        this.isCheck = false;
    }
    ContactGroupsPage.prototype.ngOnInit = function () {
        this.getContactGroups(this.currentPage);
    };
    ContactGroupsPage.prototype.getContactGroups = function (currentPage, event) {
        var _this = this;
        if (event === void 0) { event = null; }
        this.loaderService.showLoader();
        var formData = new FormData();
        formData.append("PageNumber", currentPage);
        formData.append("aid", localStorage.getItem('user_id'));
        this.contactGroupsService.getContactGroupsList(formData).then(function (data) {
            console.log("data : ", data);
            _this.dataObj = data;
            _this.currentPage = _this.dataObj.CurrentPage;
            _this.lastPage = _this.dataObj.total;
            if (_this.currentPage == 1) {
                _this.contactsGroupsData = _this.dataObj.contactgroups;
                if (_this.contactsGroupsData.length == 0) {
                    _this.isCheck = true;
                }
            }
            else {
                _this.contactsGroupsData = __spreadArrays(_this.contactsGroupsData, _this.dataObj.contactgroups);
            }
            _this.loaderService.hideLoader();
            if (event) {
                event.target.complete();
            }
        }, function (err) {
            _this.loaderService.hideLoader();
            _this.isCheck = true;
            if (event) {
                event.target.complete();
            }
        });
    };
    ContactGroupsPage.prototype.addContact = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: model_page_1.ModelPage,
                            cssClass: 'my-custom-modal-class'
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ContactGroupsPage = __decorate([
        core_1.Component({
            selector: 'app-contact-groups',
            templateUrl: './contact-groups.page.html',
            styleUrls: ['./contact-groups.page.scss']
        })
    ], ContactGroupsPage);
    return ContactGroupsPage;
}());
exports.ContactGroupsPage = ContactGroupsPage;
