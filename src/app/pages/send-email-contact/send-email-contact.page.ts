import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { LoaderService } from 'src/app/services/loader.service';
import { SendSMSEmailService } from 'src/app/services/send-smsemail.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-send-email-contact',
  templateUrl: './send-email-contact.page.html',
  styleUrls: ['./send-email-contact.page.scss'],
})
export class SendEmailContactPage implements OnInit {

  // error_variable
  public to_error: any = '';
  public name_error: any = '';
  public subject_error: any = '';
  public message_error: any = '';
  public check_validation: any = false;

  public to: any;
  public name: any;
  public subject: any;
  public message: any;
  public cid: any;
  public affaid: any;

  public item_obj: any;
  public pageName: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public loaderService: LoaderService,
    private sendSmsemailService: SendSMSEmailService,
    private toastService: ToastService,
    public navCtrl: NavController,
  ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.item_obj = this.router.getCurrentNavigation().extras.state.item_obj;
        this.pageName = this.router.getCurrentNavigation().extras.state.pageName;
        console.log("this.item_obj : ", this.item_obj)
        this.to = this.item_obj.email;
        this.name = this.item_obj.firstname + ' ' + this.item_obj.lastname;
        console.log("this.pageName : ",this.pageName)
        if (this.pageName == 'member') {
          this.affaid = this.item_obj.affaid;
        } else {
          this.cid = this.item_obj.cid;
        }
      }
    });
  }

  ngOnInit() {
  }
  ionFocus(event) {
    this.subject_error = ''
    console.log(event)
  }
  validation() {
    if (this.to == '') {
      this.to_error = 'This field is required'
      return;
    }
    if (this.name == '') {
      this.name_error = 'This field is required'
      return;
    }
    if (this.subject == '') {
      this.subject_error = 'This field is required'
      return;
    }
    if (this.message == '') {
      this.message_error = 'This field is required'
      return;
    }

    this.check_validation = true
  }
  sendemail() {
    this.validation();
    if (this.check_validation == true) {
      this.loaderService.showLoader()
      let formData = new FormData();
      formData.append("aid", localStorage.getItem('user_id'));
      if (this.pageName == 'member') {
        formData.append("affaid", this.affaid);
      } else {
        formData.append("cid", this.cid);
      }
      formData.append("email", this.to);
      formData.append("name", this.name);
      formData.append("subject", this.subject);
      formData.append("message", this.message);
      this.sendSmsemailService.sendemail(formData).then((res: any) => {
        console.log(res);
        if (res.status !== false) {
          this.toastService.showToast(res.message)
          this.loaderService.hideLoader()
          // this.navCtrl.navigateRoot('/tabs');
          this.back()
          // this.contact_groups = res.contactgroup
        } else {
          this.toastService.showToast(res.message)
          this.loaderService.hideLoader()
        }
      });
    }
  }
  back() {
    this.navCtrl.back();
  }
}
