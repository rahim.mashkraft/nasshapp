import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SendEmailContactPage } from './send-email-contact.page';

const routes: Routes = [
  {
    path: '',
    component: SendEmailContactPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SendEmailContactPageRoutingModule {}
