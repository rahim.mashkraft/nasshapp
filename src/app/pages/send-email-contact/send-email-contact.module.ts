import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SendEmailContactPageRoutingModule } from './send-email-contact-routing.module';

import { SendEmailContactPage } from './send-email-contact.page';
import { QuillModule } from 'ngx-quill';
import { SharedModule } from 'src/app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SendEmailContactPageRoutingModule,
    QuillModule,
    SharedModule
  ],
  declarations: [SendEmailContactPage]
})
export class SendEmailContactPageModule {}
