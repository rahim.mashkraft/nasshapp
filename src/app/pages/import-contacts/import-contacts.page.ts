import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts/ngx';
import { AlertController, AnimationController, IonInfiniteScroll, ModalController, NavController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert.service';
import { LoaderService } from 'src/app/services/loader.service';
import { Storage } from '@ionic/storage';
import { EventsService } from 'src/app/services/events.service';
import { ModelPage } from '../model/model.page';

@Component({
  selector: 'app-import-contacts',
  templateUrl: './import-contacts.page.html',
  styleUrls: ['./import-contacts.page.scss'],
})
export class ImportContactsPage implements OnInit {
  isIndeterminate: boolean;
  masterCheck: boolean;
  checkBoxList: any;

  checkedItems: any;
  isChecked: any = false;
  // myContacts: Contact[] = [];
  myContacts: any = [];
  myContacts_backup: any;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  taskListCustom = [];
  array_index = 0
  result = [];
  final_array = []

  isCheck: any = false;
  @ViewChild('loadingIcon', { read: ElementRef }) loadingIcon: ElementRef;
  constructor(
    public navCtrl: NavController,
    private contacts: Contacts,
    private router: Router,
    public loaderService: LoaderService,
    public alertController: AlertController,
    private animationCtrl: AnimationController,
    public storage: Storage,
    public modalController: ModalController
  ) {
    this.isCheck = false
    setTimeout(() => {
      this.startLoad()
    }, 300)
  }
  async startLoad() {
    const loadingAnimation = this.animationCtrl.create('loading-animation')
      .addElement(this.loadingIcon.nativeElement)
      .duration(700)
      .iterations(3)
      .fromTo('transform', 'translateX(0px)', 'translateY(25px)')
      .fromTo('transform', 'translateY(25px)', 'translateX(0px)');
    // .fromTo('transform', 'rotate(0deg)', 'rotate(360deg)');

    // Don't forget to start the animation!
    loadingAnimation.play();
  }
  checkMaster() {
    setTimeout(() => {
      this.myContacts.forEach(obj => {
        obj.isChecked = this.masterCheck;
      });
    });
  }
  ionViewWillEnter() {
    this.isCheck = false

  }
  getCheckedvalue() {
    this.checkedItems = this.myContacts.filter(value => {
      return value.isChecked;
    });
    console.log(this.checkedItems);
  }
  checkEvent() {
    const totalItems = this.myContacts.length;
    let checked = 0;
    this.myContacts.map(obj => {
      if (obj.isChecked) checked++;
    });
    if (checked > 0 && checked < totalItems) {
      //If even one item is checked but not all
      this.isIndeterminate = true;
      this.masterCheck = false;
    } else if (checked == totalItems) {
      //If all are checked
      this.masterCheck = true;
      this.isIndeterminate = false;
    } else {
      //If none is checked
      this.isIndeterminate = false;
      this.masterCheck = false;
    }
    this.getCheckedvalue()
  }
  onSuccess(contacts) {
    alert('Found ' + contacts.length + ' contacts.');
  };

  onError(contactError) {
    alert('onError!');
  };
  async loadContacts() {
    console.log("loadContacts click")
    // this.alertService.contactList('Are you sure, you want to get you contacts?')
    const modal = await this.modalController.create({
      component: ModelPage,
      cssClass: 'my-custom-modal-class',

    });
    modal.onDidDismiss().then((data) => {
      console.log(data);
      if (data.data == true) {
        this.loaderService.showLoader()
        this.import_contact()
      } else {
        console.log("fdfubadfafdkas")
      }
    })
    await modal.present();
    // const alert = await this.alertController.create({
    //   // header: 'Error',
    //   cssClass: 'alertCustom',
    //   // subHeader: 'You have Successfully ',
    //   message: 'Are you sure, you want to get you contacts?',
    //   buttons: [{
    //     text: 'OK',
    //     handler: async () => {
    //       this.isCheck = true

    //       this.import_contact()

    //     }
    //   }]
    // });

    // await alert.present();
  }
  import_contact() {
    this.isCheck = true
    let options = {
      filter: '',
      multiple: true,
      hasPhoneNumber: true,
    }
    this.contacts.find(['displayName', 'name', 'emails', 'phoneNumbers'], options).then((contacts: Contact[]) => {
      // this.contacts.find(['displayName', 'name', 'phoneNumbers', 'emails'], options).then((contacts) => {
      console.log("Contacts : ", contacts)
      this.myContacts = contacts
      this.myContacts_backup = this.myContacts
      // this.myContacts.sort()
      // this.chunks(this.myContacts)
      // this.storage.set("contacts", contacts)
      setTimeout(() => {
        this.loaderService.hideLoader()
      }, 500)
      // alert(this.myContacts)
      // alert(JSON.stringify(this.myContacts))
      // console.log("this.myContacts : ", this.myContacts)
    })
  }
  ngOnInit() {
  }
  // Declare the variable (in this case and initialize it with false)
  isItemAvailable = false;
  items = [];

  initializeItems() {
    this.items = ["Ram", "gopi", "dravid", "wravid", "eravid", "rravid", "travid", "gravid"];
  }

  getItems(ev: any) {
    // Reset items back to all of the items

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() !== '') {
      this.isItemAvailable = true;

      this.myContacts = this.myContacts_backup.filter((item) => {
        console.log("item : ", item)
        return (item._objectInstance.name.givenName.toLowerCase().indexOf(val.toLowerCase()) > -1)
      })
    } else {
      this.isItemAvailable = false;
    }
  }
  claerSearcher() {
    console.log("claerSearcher")
    this.myContacts = this.myContacts;
  }
  ConvertToInt(currentPage) {
    return parseInt(currentPage);
  }
  loadMoreData(value, event) {
    console.log(this.result.length)
    this.array_index = this.ConvertToInt(this.array_index) + this.ConvertToInt(value)
    console.log(this.array_index)
    if (this.result.length > this.array_index) {
      this.final_array = [...this.final_array, ...this.result[this.array_index]];
      console.log(this.final_array)
      if (event) {
        event.target.complete();
      }
    } else {
      console.log("no more data")
      if (event) {
        event.target.complete();
      }
    }
  }
  selectGroup() {
    let navigationExtras: NavigationExtras = {
      state: {
        checkedItems: this.checkedItems,
      }
    };
    // this.router.navigate(['post-add-second', navigationExtras]);
    this.router.navigate(['tabs/select-group'], navigationExtras);
    // this.navCtrl.navigateForward(['select-group'])
  }
  back() {
    this.navCtrl.back();
  }
}
