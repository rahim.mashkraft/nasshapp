"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.ImportContactsPage = void 0;
var core_1 = require("@angular/core");
var angular_1 = require("@ionic/angular");
var model_page_1 = require("../model/model.page");
var ImportContactsPage = /** @class */ (function () {
    function ImportContactsPage(navCtrl, contacts, router, loaderService, alertController, animationCtrl, storage, modalController) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.contacts = contacts;
        this.router = router;
        this.loaderService = loaderService;
        this.alertController = alertController;
        this.animationCtrl = animationCtrl;
        this.storage = storage;
        this.modalController = modalController;
        this.isChecked = false;
        // myContacts: Contact[] = [];
        this.myContacts = [];
        this.taskListCustom = [];
        this.array_index = 0;
        this.result = [];
        this.final_array = [];
        this.isCheck = false;
        // Declare the variable (in this case and initialize it with false)
        this.isItemAvailable = false;
        this.items = [];
        this.isCheck = false;
        setTimeout(function () {
            _this.startLoad();
        }, 300);
    }
    ImportContactsPage.prototype.startLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loadingAnimation;
            return __generator(this, function (_a) {
                loadingAnimation = this.animationCtrl.create('loading-animation')
                    .addElement(this.loadingIcon.nativeElement)
                    .duration(700)
                    .iterations(3)
                    .fromTo('transform', 'translateX(0px)', 'translateY(25px)')
                    .fromTo('transform', 'translateY(25px)', 'translateX(0px)');
                // .fromTo('transform', 'rotate(0deg)', 'rotate(360deg)');
                // Don't forget to start the animation!
                loadingAnimation.play();
                return [2 /*return*/];
            });
        });
    };
    ImportContactsPage.prototype.checkMaster = function () {
        var _this = this;
        setTimeout(function () {
            _this.myContacts.forEach(function (obj) {
                obj.isChecked = _this.masterCheck;
            });
        });
    };
    ImportContactsPage.prototype.ionViewWillEnter = function () {
        this.isCheck = false;
    };
    ImportContactsPage.prototype.getCheckedvalue = function () {
        this.checkedItems = this.myContacts.filter(function (value) {
            return value.isChecked;
        });
        console.log(this.checkedItems);
    };
    ImportContactsPage.prototype.checkEvent = function () {
        var totalItems = this.myContacts.length;
        var checked = 0;
        this.myContacts.map(function (obj) {
            if (obj.isChecked)
                checked++;
        });
        if (checked > 0 && checked < totalItems) {
            //If even one item is checked but not all
            this.isIndeterminate = true;
            this.masterCheck = false;
        }
        else if (checked == totalItems) {
            //If all are checked
            this.masterCheck = true;
            this.isIndeterminate = false;
        }
        else {
            //If none is checked
            this.isIndeterminate = false;
            this.masterCheck = false;
        }
        this.getCheckedvalue();
    };
    ImportContactsPage.prototype.onSuccess = function (contacts) {
        alert('Found ' + contacts.length + ' contacts.');
    };
    ;
    ImportContactsPage.prototype.onError = function (contactError) {
        alert('onError!');
    };
    ;
    ImportContactsPage.prototype.loadContacts = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("loadContacts click");
                        return [4 /*yield*/, this.modalController.create({
                                component: model_page_1.ModelPage,
                                cssClass: 'my-custom-modal-class'
                            })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (data) {
                            console.log(data);
                            if (data.data == true) {
                                _this.loaderService.showLoader();
                                _this.import_contact();
                            }
                            else {
                                console.log("fdfubadfafdkas");
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ImportContactsPage.prototype.import_contact = function () {
        var _this = this;
        this.isCheck = true;
        var options = {
            filter: '',
            multiple: true,
            hasPhoneNumber: true
        };
        this.contacts.find(['displayName', 'name', 'emails', 'phoneNumbers'], options).then(function (contacts) {
            // this.contacts.find(['displayName', 'name', 'phoneNumbers', 'emails'], options).then((contacts) => {
            console.log("Contacts : ", contacts);
            _this.myContacts = contacts;
            _this.myContacts_backup = _this.myContacts;
            // this.myContacts.sort()
            // this.chunks(this.myContacts)
            // this.storage.set("contacts", contacts)
            setTimeout(function () {
                _this.loaderService.hideLoader();
            }, 500);
            // alert(this.myContacts)
            // alert(JSON.stringify(this.myContacts))
            // console.log("this.myContacts : ", this.myContacts)
        });
    };
    ImportContactsPage.prototype.ngOnInit = function () {
    };
    ImportContactsPage.prototype.initializeItems = function () {
        this.items = ["Ram", "gopi", "dravid", "wravid", "eravid", "rravid", "travid", "gravid"];
    };
    ImportContactsPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() !== '') {
            this.isItemAvailable = true;
            this.myContacts = this.myContacts_backup.filter(function (item) {
                console.log("item : ", item);
                return (item._objectInstance.name.givenName.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
        else {
            this.isItemAvailable = false;
        }
    };
    ImportContactsPage.prototype.claerSearcher = function () {
        console.log("claerSearcher");
        this.myContacts = this.myContacts;
    };
    ImportContactsPage.prototype.ConvertToInt = function (currentPage) {
        return parseInt(currentPage);
    };
    ImportContactsPage.prototype.loadMoreData = function (value, event) {
        console.log(this.result.length);
        this.array_index = this.ConvertToInt(this.array_index) + this.ConvertToInt(value);
        console.log(this.array_index);
        if (this.result.length > this.array_index) {
            this.final_array = __spreadArrays(this.final_array, this.result[this.array_index]);
            console.log(this.final_array);
            if (event) {
                event.target.complete();
            }
        }
        else {
            console.log("no more data");
            if (event) {
                event.target.complete();
            }
        }
    };
    ImportContactsPage.prototype.selectGroup = function () {
        var navigationExtras = {
            state: {
                checkedItems: this.checkedItems
            }
        };
        // this.router.navigate(['post-add-second', navigationExtras]);
        this.router.navigate(['tabs/select-group'], navigationExtras);
        // this.navCtrl.navigateForward(['select-group'])
    };
    ImportContactsPage.prototype.back = function () {
        this.navCtrl.back();
    };
    __decorate([
        core_1.ViewChild(angular_1.IonInfiniteScroll)
    ], ImportContactsPage.prototype, "infiniteScroll");
    __decorate([
        core_1.ViewChild('loadingIcon', { read: core_1.ElementRef })
    ], ImportContactsPage.prototype, "loadingIcon");
    ImportContactsPage = __decorate([
        core_1.Component({
            selector: 'app-import-contacts',
            templateUrl: './import-contacts.page.html',
            styleUrls: ['./import-contacts.page.scss']
        })
    ], ImportContactsPage);
    return ImportContactsPage;
}());
exports.ImportContactsPage = ImportContactsPage;
