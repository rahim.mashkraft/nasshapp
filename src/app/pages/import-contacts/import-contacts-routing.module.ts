import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImportContactsPage } from './import-contacts.page';

const routes: Routes = [
  {
    path: '',
    component: ImportContactsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImportContactsPageRoutingModule {}
