import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController, MenuController, ModalController, NavController } from '@ionic/angular';
import { LoaderService } from 'src/app/services/loader.service';
import { RegisterService } from 'src/app/services/register.service';
import { mustMatch } from '../../_helpers/must-match.validator';
import { TermsConditionsPage } from './terms-conditions/terms-conditions.page';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public weight = [] as any;
  public jsonData: any;
  public height: any;
  public termConditionsCheck = false;
  public communityData = [];
  public showOtherCommunityField = false;
  public data_check_any:any;
  public onRegisterForm: FormGroup;
  public formValue: any = [];

  public eye: boolean = false;
  public passwordType:any='password';
  public c_eye: boolean = false;
  public c_passwordType:any='password';
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    public alertController: AlertController,
    private modalCtrl: ModalController,
    private registerService: RegisterService,
    public loaderService: LoaderService
  ) {
    this.jsonData = {
      numbers: [
        { description: '4 Feet' },
        { description: '5 Feet' },
        { description: '6 Feet' },
        { description: '7 Feet' },
      ],
      inches: [
        { description: '0 inches' },
        { description: '1 inches' },
        { description: '2 inches' },
        { description: '3 inches' },
        { description: '4 inches' },
        { description: '5 inches' },
        { description: '6 inches' },
        { description: '7 inches' },
        { description: '8 inches' },
        { description: '9 inches' },
        { description: '10 inches' },
        { description: '11 inches' },
        { description: '12 inches' },
      ],
    };
  }
  showPassword(value){
    if(value == 0){
      this.eye = true;
      this.passwordType =  'text';
    }else{
      this.c_eye = true;
      this.c_passwordType =  'text';
    }
  }
  hidePassword(value){
    if(value == 0){
      this.eye = false;
      this.passwordType =  'password';
    }else{
      this.c_eye = false;
      this.c_passwordType =  'password';
    }
  }


  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  fetchCommunityData() {
    // this.userService.getCommunityData()
    // .subscribe((res: any) => {
    //   this.communityData = res.data;
    // });
  }

  async termConditions() {
    const modal = await this.modalCtrl.create({
      component: TermsConditionsPage
    });
    modal.present();
  }

  back() {
    this.navCtrl.back();
  }
  unCheckFocusEmail() {
    let formData = new FormData();
    formData.append("email", this.onRegisterForm.value['email']);
    if (this.onRegisterForm.value['email'] != '') {
      this.registerService.checkemail(formData).then(data => {
        console.log("data : ", data)
        this.data_check_any = data;
        if(this.data_check_any.status == false){
          this.presentAlertError(this.data_check_any.error_msg)
          this.onRegisterForm = this.formBuilder.group({
            fname: [{ value: this.onRegisterForm.value['fname'], disabled: false }, Validators.required],
            lname: [{ value: this.onRegisterForm.value['lname'], disabled: false }, Validators.required],
            email: [ '', Validators.compose([Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
            phone_number: [{ value: this.onRegisterForm.value['phone_number'], disabled: false }, Validators.required],
            Username: [{ value: this.onRegisterForm.value['Username'], disabled: false }, Validators.required],
            // password: [null, Validators.compose([Validators.required])],
            password: [this.onRegisterForm.value['password'], Validators.compose([Validators.required])],
            confirm_password: [this.onRegisterForm.value['confirm_password'], [Validators.required]],
          });
        }
      })
    }
  }
  unCheckFocusPhone() {
    let formData = new FormData();
    formData.append("phone", this.onRegisterForm.value['phone_number']);
    // formData.append("phone", value);
    if (this.onRegisterForm.value['phone_number'] != '') {
      this.registerService.checkphone(formData).then(data => {
        console.log("data : ", data)
        this.data_check_any = data;
        if(this.data_check_any.status == false){
          this.presentAlertError(this.data_check_any.error_msg)
          this.onRegisterForm = this.formBuilder.group({
            fname: [{ value: this.onRegisterForm.value['fname'], disabled: false }, Validators.required],
            lname: [{ value: this.onRegisterForm.value['lname'], disabled: false }, Validators.required],
            email: [ this.onRegisterForm.value['email'], Validators.compose([Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
            phone_number: [{ value: '', disabled: false }, Validators.required],
            Username: [{ value: this.onRegisterForm.value['Username'], disabled: false }, Validators.required],
            // password: [null, Validators.compose([Validators.required])],
            password: [this.onRegisterForm.value['password'], Validators.compose([Validators.required])],
            confirm_password: [this.onRegisterForm.value['confirm_password'], [Validators.required]],
          });
        }
      })
    }
  }
  unCheckFocusUsername() {
    let formData = new FormData();
    formData.append("username", this.onRegisterForm.value['Username']);
    if (this.onRegisterForm.value['Username'] != '') {
      this.registerService.checkusername(formData).then(data => {
        console.log("data : ", data)
        this.data_check_any = data;
        if(this.data_check_any.status == false){
          this.presentAlertError(this.data_check_any.error_msg)
          this.onRegisterForm = this.formBuilder.group({
            fname: [{ value: this.onRegisterForm.value['fname'], disabled: false }, Validators.required],
            lname: [{ value: this.onRegisterForm.value['lname'], disabled: false }, Validators.required],
            email: [ this.onRegisterForm.value['email'], Validators.compose([Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
            phone_number: [{ value: this.onRegisterForm.value['phone_number'], disabled: false }, Validators.required],
            Username: [{ value: '', disabled: false }, Validators.required],
            // password: [null, Validators.compose([Validators.required])],
            password: [this.onRegisterForm.value['password'], Validators.compose([Validators.required])],
            confirm_password: [this.onRegisterForm.value['confirm_password'], [Validators.required]],
          });
        }
      })
    }

  }
  public onKeyUp(event: any) {
    let newValue = event.target.value;
    // console.log(event.target.value);
    let regExp = new RegExp('^[A-Za-z0-9? ]+$');

    if (!regExp.test(newValue)) {
      event.target.value = newValue.slice(0, -1);
    }
  }
  ngOnInit() {
    this.onRegisterForm = this.formBuilder.group({
      fname: [{ value: '', disabled: false }, Validators.required],
      lname: [{ value: '', disabled: false }, Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
      phone_number: [{ value: '', disabled: false }, Validators.required],
      Username: [{ value: '', disabled: false }, Validators.required],
      // password: [null, Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      confirm_password: ['', [Validators.required]],
    },
    {
      validator: mustMatch('password', 'confirm_password')
    },
  );
    this.fetchCommunityData();
  }
  setTermsConditions(event) {
    console.log("this.detail : ",event)

    this.termConditionsCheck = event.detail.checked;
    console.log("this.termConditionsCheck : ",this.termConditionsCheck)
  }

  async presentAlertSuccess() {
    const alert = await this.alertController.create({
      header: 'Success',
      cssClass: 'alertCustom',
      subHeader: 'You have Successfully Registered',
      message: 'Please Log In to Continue',
      buttons:  [
        {
         text: 'Ok',
         handler: () => {
           console.log
           this.navCtrl.navigateBack('/');
         }
       }
     ]
    });

    await alert.present();
  }
  async presentAlertError(error) {
    const alert = await this.alertController.create({
      cssClass: 'alertCustom',
      header: 'Error',
      // subHeader: 'Subtitle',
      message: error,
      buttons: ['OK']
    });

    await alert.present();
  }

  async signUp() {
    // this.onRegisterForm.value();
    this.loaderService.showLoader()
    console.log(this.onRegisterForm.value);
    let formData = new FormData();
    formData.append("firstname", this.onRegisterForm.value['fname']);
    formData.append("lastname", this.onRegisterForm.value['lname']);
    formData.append("email", this.onRegisterForm.value['email']);
    formData.append("username", this.onRegisterForm.value['Username']);
    formData.append("phone", this.onRegisterForm.value['phone_number']);
    formData.append("password", this.onRegisterForm.value['password']);
    this.registerService.register(formData).then(data => {
      console.log("data : ", data)
      let dataObj;
      dataObj = data;
      if(dataObj.status){
        this.presentAlertSuccess();
        this.loaderService.hideLoader()
      }else{
        this.loaderService.hideLoader()
      }
    })
    // this.userService
    //   .register(this.onRegisterForm.value)
    //   .subscribe((res: any) => {
    //       console.log(res);
    //     if (res.status !== false) {
    //       this.presentAlertSuccess();
    //       this.navCtrl.navigateRoot('/');
    //     } else {
    //       this.presentAlertError(res.error_msg);
    //     }
    //   });
    // const loader = await this.loadingCtrl.create({
    //   duration: 2000
    // });

    // loader.present();
    // loader.onWillDismiss().then(() => {
    //   this.navCtrl.navigateRoot('/home-results');
    // });
  }

  // // //
  goToLogin() {
    this.navCtrl.navigateForward('login');
  }
}
