import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.page.html',
  styleUrls: ['./terms-conditions.page.scss'],
})
export class TermsConditionsPage implements OnInit {
  public termsConditions:any;
  public loading:any;
  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
  }
  async close() {
    await this.modalCtrl.dismiss();
  }
}
