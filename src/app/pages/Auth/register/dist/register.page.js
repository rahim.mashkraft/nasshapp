"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.RegisterPage = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var must_match_validator_1 = require("../../_helpers/must-match.validator");
var terms_conditions_page_1 = require("./terms-conditions/terms-conditions.page");
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, menuCtrl, loadingCtrl, formBuilder, alertController, modalCtrl, registerService, loaderService) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.alertController = alertController;
        this.modalCtrl = modalCtrl;
        this.registerService = registerService;
        this.loaderService = loaderService;
        this.weight = [];
        this.termConditionsCheck = false;
        this.communityData = [];
        this.showOtherCommunityField = false;
        this.formValue = [];
        this.eye = false;
        this.passwordType = 'password';
        this.c_eye = false;
        this.c_passwordType = 'password';
        this.jsonData = {
            numbers: [
                { description: '4 Feet' },
                { description: '5 Feet' },
                { description: '6 Feet' },
                { description: '7 Feet' },
            ],
            inches: [
                { description: '0 inches' },
                { description: '1 inches' },
                { description: '2 inches' },
                { description: '3 inches' },
                { description: '4 inches' },
                { description: '5 inches' },
                { description: '6 inches' },
                { description: '7 inches' },
                { description: '8 inches' },
                { description: '9 inches' },
                { description: '10 inches' },
                { description: '11 inches' },
                { description: '12 inches' },
            ]
        };
    }
    RegisterPage.prototype.showPassword = function (value) {
        if (value == 0) {
            this.eye = true;
            this.passwordType = 'text';
        }
        else {
            this.c_eye = true;
            this.c_passwordType = 'text';
        }
    };
    RegisterPage.prototype.hidePassword = function (value) {
        if (value == 0) {
            this.eye = false;
            this.passwordType = 'password';
        }
        else {
            this.c_eye = false;
            this.c_passwordType = 'password';
        }
    };
    RegisterPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    RegisterPage.prototype.fetchCommunityData = function () {
        // this.userService.getCommunityData()
        // .subscribe((res: any) => {
        //   this.communityData = res.data;
        // });
    };
    RegisterPage.prototype.termConditions = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: terms_conditions_page_1.TermsConditionsPage
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage.prototype.back = function () {
        this.navCtrl.back();
    };
    RegisterPage.prototype.unCheckFocusEmail = function () {
        var _this = this;
        var formData = new FormData();
        formData.append("email", this.onRegisterForm.value['email']);
        if (this.onRegisterForm.value['email'] != '') {
            this.registerService.checkemail(formData).then(function (data) {
                console.log("data : ", data);
                _this.data_check_any = data;
                if (_this.data_check_any.status == false) {
                    _this.presentAlertError(_this.data_check_any.error_msg);
                    _this.onRegisterForm = _this.formBuilder.group({
                        fname: [{ value: _this.onRegisterForm.value['fname'], disabled: false }, forms_1.Validators.required],
                        lname: [{ value: _this.onRegisterForm.value['lname'], disabled: false }, forms_1.Validators.required],
                        email: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
                        phone_number: [{ value: _this.onRegisterForm.value['phone_number'], disabled: false }, forms_1.Validators.required],
                        Username: [{ value: _this.onRegisterForm.value['Username'], disabled: false }, forms_1.Validators.required],
                        // password: [null, Validators.compose([Validators.required])],
                        password: [_this.onRegisterForm.value['password'], forms_1.Validators.compose([forms_1.Validators.required])],
                        confirm_password: [_this.onRegisterForm.value['confirm_password'], [forms_1.Validators.required]]
                    });
                }
            });
        }
    };
    RegisterPage.prototype.unCheckFocusPhone = function () {
        var _this = this;
        var formData = new FormData();
        formData.append("phone", this.onRegisterForm.value['phone_number']);
        // formData.append("phone", value);
        if (this.onRegisterForm.value['phone_number'] != '') {
            this.registerService.checkphone(formData).then(function (data) {
                console.log("data : ", data);
                _this.data_check_any = data;
                if (_this.data_check_any.status == false) {
                    _this.presentAlertError(_this.data_check_any.error_msg);
                    _this.onRegisterForm = _this.formBuilder.group({
                        fname: [{ value: _this.onRegisterForm.value['fname'], disabled: false }, forms_1.Validators.required],
                        lname: [{ value: _this.onRegisterForm.value['lname'], disabled: false }, forms_1.Validators.required],
                        email: [_this.onRegisterForm.value['email'], forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
                        phone_number: [{ value: '', disabled: false }, forms_1.Validators.required],
                        Username: [{ value: _this.onRegisterForm.value['Username'], disabled: false }, forms_1.Validators.required],
                        // password: [null, Validators.compose([Validators.required])],
                        password: [_this.onRegisterForm.value['password'], forms_1.Validators.compose([forms_1.Validators.required])],
                        confirm_password: [_this.onRegisterForm.value['confirm_password'], [forms_1.Validators.required]]
                    });
                }
            });
        }
    };
    RegisterPage.prototype.unCheckFocusUsername = function () {
        var _this = this;
        var formData = new FormData();
        formData.append("username", this.onRegisterForm.value['Username']);
        if (this.onRegisterForm.value['Username'] != '') {
            this.registerService.checkusername(formData).then(function (data) {
                console.log("data : ", data);
                _this.data_check_any = data;
                if (_this.data_check_any.status == false) {
                    _this.presentAlertError(_this.data_check_any.error_msg);
                    _this.onRegisterForm = _this.formBuilder.group({
                        fname: [{ value: _this.onRegisterForm.value['fname'], disabled: false }, forms_1.Validators.required],
                        lname: [{ value: _this.onRegisterForm.value['lname'], disabled: false }, forms_1.Validators.required],
                        email: [_this.onRegisterForm.value['email'], forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
                        phone_number: [{ value: _this.onRegisterForm.value['phone_number'], disabled: false }, forms_1.Validators.required],
                        Username: [{ value: '', disabled: false }, forms_1.Validators.required],
                        // password: [null, Validators.compose([Validators.required])],
                        password: [_this.onRegisterForm.value['password'], forms_1.Validators.compose([forms_1.Validators.required])],
                        confirm_password: [_this.onRegisterForm.value['confirm_password'], [forms_1.Validators.required]]
                    });
                }
            });
        }
    };
    RegisterPage.prototype.onKeyUp = function (event) {
        var newValue = event.target.value;
        // console.log(event.target.value);
        var regExp = new RegExp('^[A-Za-z0-9? ]+$');
        if (!regExp.test(newValue)) {
            event.target.value = newValue.slice(0, -1);
        }
    };
    RegisterPage.prototype.ngOnInit = function () {
        this.onRegisterForm = this.formBuilder.group({
            fname: [{ value: '', disabled: false }, forms_1.Validators.required],
            lname: [{ value: '', disabled: false }, forms_1.Validators.required],
            email: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
            phone_number: [{ value: '', disabled: false }, forms_1.Validators.required],
            Username: [{ value: '', disabled: false }, forms_1.Validators.required],
            // password: [null, Validators.compose([Validators.required])],
            password: ['', forms_1.Validators.compose([forms_1.Validators.required])],
            confirm_password: ['', [forms_1.Validators.required]]
        }, {
            validator: must_match_validator_1.mustMatch('password', 'confirm_password')
        });
        this.fetchCommunityData();
    };
    RegisterPage.prototype.setTermsConditions = function (event) {
        console.log("this.detail : ", event);
        this.termConditionsCheck = event.detail.checked;
        console.log("this.termConditionsCheck : ", this.termConditionsCheck);
    };
    RegisterPage.prototype.presentAlertSuccess = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Success',
                            cssClass: 'alertCustom',
                            subHeader: 'You have Successfully Registered',
                            message: 'Please Log In to Continue',
                            buttons: [
                                {
                                    text: 'Ok',
                                    handler: function () {
                                        console.log;
                                        _this.navCtrl.navigateBack('/');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage.prototype.presentAlertError = function (error) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            cssClass: 'alertCustom',
                            header: 'Error',
                            // subHeader: 'Subtitle',
                            message: error,
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    RegisterPage.prototype.signUp = function () {
        return __awaiter(this, void 0, void 0, function () {
            var formData;
            var _this = this;
            return __generator(this, function (_a) {
                // this.onRegisterForm.value();
                this.loaderService.showLoader();
                console.log(this.onRegisterForm.value);
                formData = new FormData();
                formData.append("firstname", this.onRegisterForm.value['fname']);
                formData.append("lastname", this.onRegisterForm.value['lname']);
                formData.append("email", this.onRegisterForm.value['email']);
                formData.append("username", this.onRegisterForm.value['Username']);
                formData.append("phone", this.onRegisterForm.value['phone_number']);
                formData.append("password", this.onRegisterForm.value['password']);
                this.registerService.register(formData).then(function (data) {
                    console.log("data : ", data);
                    var dataObj;
                    dataObj = data;
                    if (dataObj.status) {
                        _this.presentAlertSuccess();
                        _this.loaderService.hideLoader();
                    }
                    else {
                        _this.loaderService.hideLoader();
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    // // //
    RegisterPage.prototype.goToLogin = function () {
        this.navCtrl.navigateForward('login');
    };
    RegisterPage = __decorate([
        core_1.Component({
            selector: 'app-register',
            templateUrl: './register.page.html',
            styleUrls: ['./register.page.scss']
        })
    ], RegisterPage);
    return RegisterPage;
}());
exports.RegisterPage = RegisterPage;
