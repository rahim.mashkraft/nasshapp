"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.LoginPage = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var network_service_1 = require("src/app/services/network.service");
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, menuCtrl, formBuilder, loginService, networkService, loaderService, toastService, alertService, eventsService, socialSignUpService, fb, googlePlus, platform, fcm, device, deviceIdService) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.formBuilder = formBuilder;
        this.loginService = loginService;
        this.networkService = networkService;
        this.loaderService = loaderService;
        this.toastService = toastService;
        this.alertService = alertService;
        this.eventsService = eventsService;
        this.socialSignUpService = socialSignUpService;
        this.fb = fb;
        this.googlePlus = googlePlus;
        this.platform = platform;
        this.fcm = fcm;
        this.device = device;
        this.deviceIdService = deviceIdService;
        this.formData = new FormData();
        this.eye = false;
        this.passwordType = 'password';
        this.setupFCM();
    }
    LoginPage.prototype.showPassword = function () {
        this.eye = true;
        this.passwordType = 'text';
    };
    LoginPage.prototype.hidePassword = function () {
        this.eye = false;
        this.passwordType = 'password';
    };
    LoginPage.prototype.ionViewDidLoad = function () {
    };
    LoginPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    LoginPage.prototype.ngOnInit = function () {
        this.onLoginForm = this.formBuilder.group({
            email: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
            password: ['', forms_1.Validators.compose([forms_1.Validators.required])],
            toggle: ['false']
        });
    };
    // // //
    LoginPage.prototype.goToRegister = function () {
        this.navCtrl.navigateForward('/register');
    };
    LoginPage.prototype.goToHome = function () {
        var _this = this;
        // this.udId = localStorage.getItem('device_token')
        this.formData.append("email", this.onLoginForm.value['email']);
        this.formData.append("password", this.onLoginForm.value['password']);
        this.networkService.onNetworkChange().subscribe(function (status) {
            if (status == network_service_1.ConnectionStatus.Online) {
                _this.loaderService.showLoader();
                _this.loginService.login(_this.formData).subscribe(function (res) {
                    console.log(res);
                    if (res.status !== false) {
                        _this.get_device_id_token(res.aid);
                        localStorage.setItem('user_id', res.aid);
                        localStorage.setItem('firstname', res.firstname);
                        localStorage.setItem('lastname', res.lastname);
                        localStorage.setItem('email', res.email);
                        localStorage.setItem('profileimage', res.profileimage);
                        localStorage.setItem('address', res.address);
                        localStorage.setItem('city', res.city);
                        localStorage.setItem('country', res.country);
                        localStorage.setItem('phone', res.phone);
                        localStorage.setItem('username', res.username);
                        localStorage.setItem('zipcode', res.zipcode);
                        localStorage.setItem('state', res.state);
                        localStorage.setItem('toggle', _this.onLoginForm.value['toggle']);
                        _this.loaderService.hideLoader();
                        _this.toastService.showToast();
                        var userData = {
                            firstname: res.firstname,
                            lastname: res.lastname,
                            email: res.email,
                            profileimage: res.profileimage
                        };
                        _this.eventsService.publish('userLogged', userData);
                        _this.navCtrl.navigateRoot('/tabs');
                    }
                    else if (res.status !== true) {
                        _this.loaderService.hideLoader();
                        _this.alertService.presentAlertError(res.error_msg);
                    }
                    else {
                        _this.loaderService.hideLoader();
                        _this.alertService.presentAlertError('Error Connecting to Internet');
                    }
                });
            }
            else {
                console.log("network not available");
                _this.alertService.presentNetworkAlert();
            }
        });
    };
    LoginPage.prototype.facebookLogin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.fb.login(['public_profile', 'email']).then(function (res) {
                    if (res.status == "connected") {
                        var fb_id = res.authResponse.userID;
                        var fb_token = res.authResponse.accessToken;
                        _this.fb.api("/me?fields=name,email,first_name,last_name,picture", []).then(function (user) {
                            console.log('facebookLogin : ', user);
                            var name = user.name;
                            if (user.email) {
                                var email = user.email;
                            }
                            else {
                                email = '';
                            }
                            var first_name = user.first_name;
                            var last_name = user.last_name;
                            console.log("=== USER INFOS ===");
                            console.log("Name : " + name);
                            console.log("Email : " + email);
                            console.log("fb_id : " + fb_id);
                            var res = name.split();
                            var firstname = res[0];
                            var lastname = res[1];
                            var picture = user.picture.data.url;
                            if (picture == undefined || picture == null) {
                                picture = '';
                            }
                            var item = { 'first_name': first_name, 'last_name': last_name, 'image': picture, 'email': email, 'signuptype': 'FACEBOOK', 'signupid': fb_id };
                            _this.social_login(item);
                        });
                    }
                    else {
                        console.log("An error occurred...");
                    }
                })["catch"](function (e) {
                    console.log('Error logging into Facebook', e);
                });
                return [2 /*return*/];
            });
        });
    };
    LoginPage.prototype.login_google = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.googlePlus.login({}).then(function (res) {
                    console.log("googlePlus : ", res);
                    var name = res.displayName;
                    var email = res.email;
                    var userId = res.userId;
                    var firstname = res.givenName;
                    var lastname = res.familyName;
                    var picture = res.imageUrl;
                    if (picture == undefined || picture == null) {
                        picture = '';
                    }
                    var name3 = '';
                    if (lastname != '') {
                        name3 = firstname + " " + lastname;
                    }
                    else {
                        name3 = firstname;
                    }
                    console.log("Name3 : " + name3);
                    if (name3 != '') {
                        name = name3;
                        console.log("Name4 : " + name);
                    }
                    console.log("=== USER INFOS ===");
                    console.log("Name : " + name);
                    console.log("Email : " + email);
                    console.log("userId : " + userId);
                    console.log("picture : " + picture);
                    var item = { 'firstname': firstname, 'lastname': lastname, 'email': email, 'image': picture, 'signuptype': 'GOOGLE', 'signupid': userId };
                    _this.social_login(item);
                }, function (err) {
                    console.log(err);
                });
                return [2 /*return*/];
            });
        });
    };
    LoginPage.prototype.loginWithApple = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        self = this;
                        return [4 /*yield*/, cordova.plugins.SignInWithApple.signin({ requestedScopes: [0, 1] }, function (succ) {
                                console.log(succ);
                                console.log("loginWithApple : ", succ);
                                // console.error('error222')
                                var apple_id = 0;
                                var givenName = '';
                                var familyName = '';
                                var email = '';
                                var picture = '';
                                if (succ.user) {
                                    apple_id = succ.user;
                                }
                                if (succ.email) {
                                    email = succ.email;
                                }
                                if (succ.fullName.givenName) {
                                    givenName = succ.fullName.givenName;
                                }
                                if (succ.fullName.familyName) {
                                    familyName = succ.fullName.familyName;
                                }
                                var name = givenName + ' ' + familyName;
                                var name2 = givenName + '' + familyName;
                                var fname = '';
                                var lname = '';
                                fname = givenName;
                                lname = familyName;
                                console.log("Name : " + name);
                                console.log("Email : " + email);
                                console.log("userId : " + apple_id);
                                // alert(JSON.stringify(succ));
                                // alert(JSON.stringify("userId : " + apple_id));
                                // alert(JSON.stringify('email='+succ.email));
                                // alert(JSON.stringify('email='+email));
                                // if (email == '') {
                                //   email = self.profile.email;
                                // }
                                // if (name2 == '') {
                                //   name = self.profile.fname + ' ' + self.profile.lname;
                                //   fname = self.profile.fname;
                                //   lname = self.profile.lname;
                                // }
                                var item = { 'firstname': fname, 'lastname': lname, 'email': email, 'image': picture, 'signuptype': 'APPLE', 'signupid': apple_id };
                                self.social_login(item);
                                // setTimeout(() => {
                                //   self.social_login(email, apple_id, name, fname, lname);
                                // }, 500);
                            }, function (err) {
                                setTimeout(function () {
                                    self.loaderService.hideLoader();
                                }, 500);
                                console.error('errorAEE');
                                console.error(err);
                                console.log(JSON.stringify(err));
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.social_login = function (item) {
        var _this = this;
        this.loaderService.showLoader();
        var formData = new FormData();
        formData.append("firstname", item.firstname);
        formData.append("lastname", item.lastname);
        formData.append("email", item.email);
        formData.append("image", item.image);
        formData.append("signuptype", item.signuptype);
        formData.append("signupid", item.signupid);
        this.socialSignUpService.social_login(formData).then(function (res) {
            console.log("data : ", res);
            if (res.status != false) {
                _this.get_device_id_token(res.aid);
                localStorage.setItem('user_id', res.aid);
                localStorage.setItem('firstname', res.firstname);
                localStorage.setItem('lastname', res.lastname);
                localStorage.setItem('email', res.email);
                localStorage.setItem('profileimage', res.profileimage);
                localStorage.setItem('address', res.address);
                localStorage.setItem('city', res.city);
                localStorage.setItem('country', res.country);
                localStorage.setItem('phone', res.phone);
                localStorage.setItem('username', res.username);
                localStorage.setItem('zipcode', res.zipcode);
                localStorage.setItem('state', res.state);
                localStorage.setItem('toggle', _this.onLoginForm.value['toggle']);
                _this.loaderService.hideLoader();
                _this.toastService.showToast();
                var userData = {
                    firstname: res.firstname,
                    lastname: res.lastname,
                    email: res.email,
                    profileimage: res.profileimage
                };
                _this.eventsService.publish('userLogged', userData);
                _this.navCtrl.navigateRoot('/tabs');
            }
            else if (res.status != true) {
                _this.loaderService.hideLoader();
                _this.alertService.presentAlertError(res.error_msg);
            }
            else {
                _this.loaderService.hideLoader();
                _this.alertService.presentAlertError('Error Connecting to Internet');
            }
        }, function (err) {
            _this.loaderService.hideLoader();
        });
    };
    LoginPage.prototype.get_device_id_token = function (aid) {
        var _this = this;
        if (this.platform.is('cordova')) {
            var thisObj = this;
            console.log("device token 1 =");
            // setTimeout(() => {
            thisObj.fcm.onTokenRefresh().subscribe(function (token) {
                console.log("device token=" + token);
                _this.device_token = token;
                console.log('Device UUID is: ' + _this.device.uuid);
                _this.device_store(_this.device.uuid, token, aid);
            });
            // }, 1000);
            // setTimeout(() => {
            thisObj.fcm.getToken().then(function (token) {
                _this.device_token = token;
                console.log("device token2=" + token);
                console.log('Device UUID is2: ' + _this.device.uuid);
                _this.device_store(_this.device.uuid, token, aid);
            });
            // }, 1000);
        }
    };
    LoginPage.prototype.device_store = function (uuid, fcmtoken, aid) {
        var formData = new FormData();
        formData.append("udid", uuid);
        formData.append("device_token", fcmtoken);
        formData.append("aid", aid);
        this.deviceIdService.device_store(formData).then(function (data) {
            console.log(data);
        });
    };
    LoginPage.prototype.setupFCM = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _c;
            var _this = this;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0: return [4 /*yield*/, this.platform.ready()];
                    case 1:
                        _d.sent();
                        console.log('FCM setup started');
                        if (!this.platform.is('cordova')) {
                            return [2 /*return*/];
                        }
                        console.log('In cordova platform');
                        console.log('Subscribing to token updates');
                        this.fcm.onTokenRefresh().subscribe(function (newToken) {
                            _this.token = newToken;
                            console.log('onTokenRefresh received event with: ', newToken);
                        });
                        console.log('Subscribing to new notifications');
                        this.fcm.onNotification().subscribe(function (payload) {
                            _this.pushPayload = payload;
                            console.log('onNotification received event with: ', payload);
                        });
                        _a = this;
                        return [4 /*yield*/, this.fcm.requestPushPermission()];
                    case 2:
                        _a.hasPermission = _d.sent();
                        console.log('requestPushPermission result: ', this.hasPermission);
                        _b = this;
                        return [4 /*yield*/, this.fcm.getToken()];
                    case 3:
                        _b.token = _d.sent();
                        console.log('getToken result: ', this.token);
                        _c = this;
                        return [4 /*yield*/, this.fcm.getInitialPushPayload()];
                    case 4:
                        _c.pushPayload = _d.sent();
                        console.log('getInitialPushPayload result: ', this.pushPayload);
                        return [2 /*return*/];
                }
            });
        });
    };
    Object.defineProperty(LoginPage.prototype, "pushPayloadString", {
        get: function () {
            return JSON.stringify(this.pushPayload, null, 4);
        },
        enumerable: false,
        configurable: true
    });
    LoginPage.prototype.forgotPass = function () {
        this.navCtrl.navigateForward(['forgot-password']);
    };
    LoginPage = __decorate([
        core_1.Component({
            selector: 'app-login',
            templateUrl: './login.page.html',
            styleUrls: ['./login.page.scss']
        })
    ], LoginPage);
    return LoginPage;
}());
exports.LoginPage = LoginPage;
// cordova plugin add cordova-plugin-googleplus --save --variable REVERSED_CLIENT_ID=com.googleusercontent.apps.584704282410-peo3u9r6to27b0r46imes0o1jo90idl2 --variable WEB_APPLICATION_CLIENT_ID=584704282410-sf9ug9rupeug4oj39ecotqnoa9lvkf37.apps.googleusercontent.com
