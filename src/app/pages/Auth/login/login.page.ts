import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { NavController, MenuController, AlertController, LoadingController, Platform } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert.service';
import { EventsService } from 'src/app/services/events.service';
import { LoaderService } from 'src/app/services/loader.service';
import { LoginService } from 'src/app/services/login.service';
import { ConnectionStatus, NetworkService } from 'src/app/services/network.service';
import { SocialSignUPService } from 'src/app/services/social-sign-up.service';
import { ToastService } from 'src/app/services/toast.service';
// import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic";
import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic/ngx";
import { Device } from '@ionic-native/device/ngx';
import { DeviceIdService } from 'src/app/services/device-id.service';
import { INotificationPayload } from 'plugins/cordova-plugin-fcm-with-dependecy-updated/typings';

declare var cordova: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public onLoginForm: FormGroup;
  signed_in: any;
  public udId: any;
  formData = new FormData();

  device_token: any;

  public hasPermission: boolean;
  public token: string;
  public pushPayload: INotificationPayload;

  public eye: boolean = false;
  public passwordType:any='password';

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private networkService: NetworkService,
    public loaderService: LoaderService,
    private toastService: ToastService,
    private alertService: AlertService,
    private eventsService: EventsService,
    private socialSignUpService: SocialSignUPService,
    private fb: Facebook,
    private googlePlus: GooglePlus,
    public platform: Platform,
    private fcm: FCM,
    private device: Device,
    private deviceIdService: DeviceIdService
  ) {
    this.setupFCM();
  }
  showPassword(){
    this.eye = true;
    this.passwordType =  'text';
  }
  hidePassword(){
    this.eye = false;
    this.passwordType =  'password';
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {

    this.onLoginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
      password: ['', Validators.compose([Validators.required])],
      toggle: ['false'],
    });
  }

  // // //
  goToRegister() {
    this.navCtrl.navigateForward('/register');
  }

  goToHome() {
    // this.udId = localStorage.getItem('device_token')
    this.formData.append("email", this.onLoginForm.value['email']);
    this.formData.append("password", this.onLoginForm.value['password']);
    this.networkService.onNetworkChange().subscribe((status: ConnectionStatus) => {
      if (status == ConnectionStatus.Online) {
        this.loaderService.showLoader();
        this.loginService.login(this.formData).subscribe((res: any) => {

          console.log(res);
          if (res.status !== false) {
            this.get_device_id_token(res.aid)
            localStorage.setItem('user_id', res.aid);
            localStorage.setItem('firstname', res.firstname);
            localStorage.setItem('lastname', res.lastname);
            localStorage.setItem('email', res.email);
            localStorage.setItem('profileimage', res.profileimage);
            localStorage.setItem('address', res.address);
            localStorage.setItem('city', res.city);
            localStorage.setItem('country', res.country);
            localStorage.setItem('phone', res.phone);
            localStorage.setItem('username', res.username);
            localStorage.setItem('zipcode', res.zipcode);
            localStorage.setItem('state', res.state);
            localStorage.setItem('toggle', this.onLoginForm.value['toggle']);

            this.loaderService.hideLoader();
            this.toastService.showToast();
            const userData = {
              firstname: res.firstname,
              lastname: res.lastname,
              email: res.email,
              profileimage: res.profileimage
            };
            this.eventsService.publish('userLogged', userData);
            this.navCtrl.navigateRoot('/tabs');
          } else if (res.status !== true) {
            this.loaderService.hideLoader();
            this.alertService.presentAlertError(res.error_msg);
          } else {
            this.loaderService.hideLoader();
            this.alertService.presentAlertError('Error Connecting to Internet');

          }
        });
      } else {
        console.log("network not available")
        this.alertService.presentNetworkAlert()
      }
    });

  }
  async facebookLogin() {
    this.fb.login(['public_profile', 'email']).then((res: FacebookLoginResponse) => {
      if (res.status == "connected") {
        var fb_id = res.authResponse.userID;
        var fb_token = res.authResponse.accessToken;
        this.fb.api("/me?fields=name,email,first_name,last_name,picture", []).then((user) => {
          console.log('facebookLogin : ', user)
          var name = user.name;
          if (user.email) {
            var email = user.email;
          } else {
            email = ''
          }
          var first_name = user.first_name;
          var last_name = user.last_name;
          console.log("=== USER INFOS ===");
          console.log("Name : " + name);
          console.log("Email : " + email);
          console.log("fb_id : " + fb_id);
          var res = name.split();
          let firstname = res[0];
          let lastname = res[1];
          var picture = user.picture.data.url;
          if (picture == undefined || picture == null) {
            picture = '';
          }
          let item = { 'first_name': first_name, 'last_name': last_name, 'image': picture, 'email': email, 'signuptype': 'FACEBOOK', 'signupid': fb_id }
          this.social_login(item);
        });
      }
      else {
        console.log("An error occurred...");

      }

    })
      .catch((e) => {
        console.log('Error logging into Facebook', e);
      });
  }
  async login_google() {
    this.googlePlus.login({}).then(res => {
      console.log("googlePlus : ", res);
      var name = res.displayName;
      var email = res.email;
      var userId = res.userId;
      var firstname = res.givenName;
      var lastname = res.familyName;
      var picture = res.imageUrl;
      if (picture == undefined || picture == null) {
        picture = '';
      }
      var name3 = '';
      if (lastname != '') {
        name3 = firstname + " " + lastname;
      } else {
        name3 = firstname;
      }
      console.log("Name3 : " + name3);
      if (name3 != '') {
        name = name3;
        console.log("Name4 : " + name);
      }
      console.log("=== USER INFOS ===");
      console.log("Name : " + name);
      console.log("Email : " + email);
      console.log("userId : " + userId);
      console.log("picture : " + picture);
      let item = { 'firstname': firstname, 'lastname': lastname, 'email': email, 'image': picture, 'signuptype': 'GOOGLE', 'signupid': userId }
      this.social_login(item);
    }, err => {
      console.log(err);
    });

  }
  async loginWithApple() {
    // this.loaderService.showLoader();
    // let loading = this.loadingCtrl.create({
    //   content: ''
    // });

    let self = this;

    await cordova.plugins.SignInWithApple.signin(
      { requestedScopes: [0, 1] },
      function (succ) {
        console.log(succ);
        console.log("loginWithApple : ",succ);
        // console.error('error222')
        var apple_id = 0;
        var givenName = '';
        var familyName = '';
        var email = '';
        var picture = '';
        if (succ.user) {
          apple_id = succ.user;
        }
        if (succ.email) {
          email = succ.email;
        }
        if (succ.fullName.givenName) {
          givenName = succ.fullName.givenName
        }
        if (succ.fullName.familyName) {
          familyName = succ.fullName.familyName
        }
        var name = givenName + ' ' + familyName;
        var name2 = givenName + '' + familyName;

        var fname = '';
        var lname = '';

        fname = givenName;
        lname = familyName;

        console.log("Name : " + name);
        console.log("Email : " + email);
        console.log("userId : " + apple_id);
        // alert(JSON.stringify(succ));
        // alert(JSON.stringify("userId : " + apple_id));
        // alert(JSON.stringify('email='+succ.email));
        // alert(JSON.stringify('email='+email));

        // if (email == '') {
        //   email = self.profile.email;
        // }
        // if (name2 == '') {
        //   name = self.profile.fname + ' ' + self.profile.lname;
        //   fname = self.profile.fname;
        //   lname = self.profile.lname;
        // }
        let item = { 'firstname': fname, 'lastname': lname, 'email': email, 'image': picture, 'signuptype': 'APPLE', 'signupid': apple_id }
        self.social_login(item);
        // setTimeout(() => {
        //   self.social_login(email, apple_id, name, fname, lname);
        // }, 500);


      },
      function (err) {
        setTimeout(() => {
          self.loaderService.hideLoader();
        }, 500);

        console.error('errorAEE')
        console.error(err)
        console.log(JSON.stringify(err))
      }
    )
  }

  social_login(item) {
    this.loaderService.showLoader();
    let formData = new FormData();
    formData.append("firstname", item.firstname);
    formData.append("lastname", item.lastname);
    formData.append("email", item.email);
    formData.append("image", item.image);
    formData.append("signuptype", item.signuptype);
    formData.append("signupid", item.signupid);
    this.socialSignUpService.social_login(formData).then((res: any) => {
      console.log("data : ", res)
      if (res.status != false) {
        this.get_device_id_token(res.aid)
        localStorage.setItem('user_id', res.aid);
        localStorage.setItem('firstname', res.firstname);
        localStorage.setItem('lastname', res.lastname);
        localStorage.setItem('email', res.email);
        localStorage.setItem('profileimage', res.profileimage);
        localStorage.setItem('address', res.address);
        localStorage.setItem('city', res.city);
        localStorage.setItem('country', res.country);
        localStorage.setItem('phone', res.phone);
        localStorage.setItem('username', res.username);
        localStorage.setItem('zipcode', res.zipcode);
        localStorage.setItem('state', res.state);
        localStorage.setItem('toggle', this.onLoginForm.value['toggle']);

        this.loaderService.hideLoader();
        this.toastService.showToast();
        const userData = {
          firstname: res.firstname,
          lastname: res.lastname,
          email: res.email,
          profileimage: res.profileimage
        };
        this.eventsService.publish('userLogged', userData);
        this.navCtrl.navigateRoot('/tabs');
      } else if (res.status != true) {
        this.loaderService.hideLoader();
        this.alertService.presentAlertError(res.error_msg);
      } else {
        this.loaderService.hideLoader();
        this.alertService.presentAlertError('Error Connecting to Internet');

      }
    },
      err => {
        this.loaderService.hideLoader();
      });
  }
  get_device_id_token(aid) {
    if (this.platform.is('cordova')) {
      let thisObj = this;
      console.log("device token 1 =");

      // setTimeout(() => {
      thisObj.fcm.onTokenRefresh().subscribe(token => {
        console.log("device token=" + token);
        this.device_token = token;
        console.log('Device UUID is: ' + this.device.uuid);

        this.device_store(this.device.uuid, token, aid);
      });
      // }, 1000);
      // setTimeout(() => {
      thisObj.fcm.getToken().then(token => {
        this.device_token = token;
        console.log("device token2=" + token);
        console.log('Device UUID is2: ' + this.device.uuid);
        this.device_store(this.device.uuid, token, aid);
      });
      // }, 1000);
    }
  }
  device_store(uuid, fcmtoken, aid) {
    let formData = new FormData();
    formData.append("udid", uuid);
    formData.append("device_token", fcmtoken);
    formData.append("aid", aid);
    this.deviceIdService.device_store(formData).then(data => {
      console.log(data)
    });
  }
  private async setupFCM() {
    await this.platform.ready();
    console.log('FCM setup started');

    if (!this.platform.is('cordova')) {
      return;
    }
    console.log('In cordova platform');

    console.log('Subscribing to token updates');
    this.fcm.onTokenRefresh().subscribe((newToken) => {
      this.token = newToken;
      console.log('onTokenRefresh received event with: ', newToken);
    });

    console.log('Subscribing to new notifications');
    this.fcm.onNotification().subscribe((payload) => {
      this.pushPayload = payload;
      console.log('onNotification received event with: ', payload);
    });

    this.hasPermission = await this.fcm.requestPushPermission();
    console.log('requestPushPermission result: ', this.hasPermission);

    this.token = await this.fcm.getToken();
    console.log('getToken result: ', this.token);

    this.pushPayload = await this.fcm.getInitialPushPayload();
    console.log('getInitialPushPayload result: ', this.pushPayload);
  }

  public get pushPayloadString() {
    return JSON.stringify(this.pushPayload, null, 4);
  }
  forgotPass(){
    this.navCtrl.navigateForward(['forgot-password'])
  }
}
// cordova plugin add cordova-plugin-googleplus --save --variable REVERSED_CLIENT_ID=com.googleusercontent.apps.584704282410-peo3u9r6to27b0r46imes0o1jo90idl2 --variable WEB_APPLICATION_CLIENT_ID=584704282410-sf9ug9rupeug4oj39ecotqnoa9lvkf37.apps.googleusercontent.com