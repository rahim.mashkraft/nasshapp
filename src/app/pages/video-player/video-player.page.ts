import { LoaderService } from 'src/app/services/loader.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { get } from 'scriptjs';
import { QuizService } from 'src/app/services/quiz.service';


@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.page.html',
  styleUrls: ['./video-player.page.scss'],
})
export class VideoPlayerPage implements OnInit {

  public today = Date.now();
  public myValue: any = 0;
  public videotitle: any;
  public videourl: any;
  public videoid: any;
  public videoimage: any;
  public take_quiz: any = true;
  public pdf: any = true;
  public pdfFile: any;
  public quizid:any;
  public btnPlay: any;
  public btnPause: any;
  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private quizService: QuizService,
    public loaderService: LoaderService
  ) {
    this.loaderService.showLoader()
    this.btnPlay = true
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.videotitle = this.router.getCurrentNavigation().extras.state.videotitle;
        this.videourl = this.router.getCurrentNavigation().extras.state.videourl;
        this.videoid = this.router.getCurrentNavigation().extras.state.videoid;
        this.videoimage = this.router.getCurrentNavigation().extras.state.videoimage;
        console.log("videoimage : ", this.videoimage)
        this.videoimage = environment.imageUrl + this.videoimage
        console.log("videoimage after : ", this.videoimage)
        this.getQuiz();
        // this.loaderService.showLoader()
      }
    });
    get("assets/javaScript.js", (data) => {
      console.log(data)
    });
   }
   min_:any;
   setCurrentTime(data, video) {
     var sec = video.currentTime;
     var h = Math.floor(sec / 3600);
     sec = sec % 3600;
     var min = Math.floor(sec / 60);
     this.min_ = min;
     sec = Math.floor(sec % 60);
     if (sec.toString().length < 2) sec = "0" + sec;
     if (this.min_.toString().length < 2) this.min_ = "0" + this.min_;
     if (h == 0) {
       document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
     } else {
       document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
     }
   }
   onMetadata(e, video) {
    // this.seekbar.min = 0;
    var sec = video.duration;
    var h = Math.floor(sec / 3600);
    sec = sec % 3600;
    var min = Math.floor(sec / 60);
    this.min_ = min;
    sec = Math.floor(sec % 60);
    if (sec.toString().length < 2) sec = "0" + sec;
    if (this.min_.toString().length < 2) this.min_ = "0" + this.min_;
    if (h == 0) {
      document.getElementById('lblTime2').innerHTML = this.min_ + ":" + sec;
    } else {
      document.getElementById('lblTime2').innerHTML = h + ":" + this.min_ + ":" + sec;
    }
    setTimeout(() => {
      this.loaderService.hideLoader();
      video.play();

    }, 500)
  }
  PlayNow(video) {
    video.play();
    this.btnPause = true;
    this.btnPlay = false;
  }
  PauseNow(video) {
    video.pause();
    this.btnPause = false;
    this.btnPlay = true;
  }
  ngOnInit() {
  }

  getQuiz() {
    let formData = new FormData();
    formData.append("aid", localStorage.getItem('user_id'));
    formData.append("videoid", this.videoid);
    this.quizService.getQuiz(formData).then((res: any) => {
      console.log("getQuiz : ", res);
      if (res.quizcount > 0) {
        this.take_quiz = false
      }
      this.quizid = res.quizid;
      if (res.downloadcount > 0) {
        this.pdf = false
        this.pdfFile = environment.downloadurl + res.downloadlink;
        console.log("this.pdfFile : ", this.pdfFile)
      }
    });
  }
}
