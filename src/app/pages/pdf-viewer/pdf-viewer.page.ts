import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, NavParams, Platform } from '@ionic/angular';

@Component({
  selector: 'app-pdf-viewer',
  templateUrl: './pdf-viewer.page.html',
  styleUrls: ['./pdf-viewer.page.scss'],
})
export class PdfViewerPage implements OnInit {

  pdfSrc:any;
  check_platform:any;
  heading_name:any;
  constructor(
    private platform: Platform,
    public navCtrl: NavController,
    private router: Router,
    private route: ActivatedRoute,
    ) {
    if (this.platform.is('android')) {
      this.check_platform = 1;
    }else{
      this.check_platform = 0;
    }
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.heading_name = this.router.getCurrentNavigation().extras.state.name;
        this.pdfSrc = this.router.getCurrentNavigation().extras.state.url;
        console.log("this.pdfSrc : ",this.pdfSrc)
        console.log("this.heading_name : ",this.heading_name)
    
        let win: any = window; // hack compilator
        let safeURL = win.Ionic.WebView.convertFileSrc(this.pdfSrc);
        this.pdfSrc= safeURL;
        console.log("this.pdfSrc : ",this.pdfSrc)
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PdfViewerPage');
  }

  ngOnInit() {
  }
  back() {
    this.navCtrl.back();
  }
}
