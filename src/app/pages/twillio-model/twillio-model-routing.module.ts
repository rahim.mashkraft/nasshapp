import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TwillioModelPage } from './twillio-model.page';

const routes: Routes = [
  {
    path: '',
    component: TwillioModelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TwillioModelPageRoutingModule {}
