import { ImageModelPage } from './../image-model/image-model.page';
import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ModalController, NavParams } from '@ionic/angular';

// Define On Top 
declare var HKVideoPlayer;
@Component({
  selector: 'app-twillio-model',
  templateUrl: './twillio-model.page.html',
  styleUrls: ['./twillio-model.page.scss'],
})
export class TwillioModelPage implements OnInit {

  public imageArray: any;
  public videoLink: any;
  public account_SID: any;
  public auth_Token: any;
  public twilio_Number: any;
  constructor(
    public modalController: ModalController,
    navParams: NavParams,
    private router: Router,
  ) {
    this.imageArray = navParams.get('imageArray');
    this.videoLink = navParams.get('videoLink');
    console.log("imageArray : ", this.imageArray)
    console.log("videoLink : ", this.videoLink)
  }
  playVideo() {
    HKVideoPlayer.play(this.videoLink);
  }
  ngOnInit() {
  }
  countLength(value) {
    const tempArray = value.toString();
    const length = tempArray.length;
    return length;
  }
  validation(){
    if(this.account_SID && this.auth_Token && this.twilio_Number){

    }
  }
  update() {
    console.log(this.account_SID)
    console.log(this.auth_Token)
    console.log(this.twilio_Number)
    if (this.countLength(this.twilio_Number) < 10) {
      alert("plase add min or max 10 digits Twilio number")
    } else {
      let Twilio = +1 + this.twilio_Number;
      console.log(Twilio)
    }
  }
  public onKeyUp(event: any) {
    let newValue = event.target.value;
    // console.log(event.target.value);
    let regExp = new RegExp('^([0-9_\\-]+)$');
    if (!regExp.test(newValue)) {
      event.target.value = newValue.slice(11, -1);
    }
  }
  async openImage() {
    const modal = await this.modalController.create({
      component: ImageModelPage,
      componentProps: {
        imageArray: this.imageArray,
      }
    })
    modal.onDidDismiss().then((data) => {
      console.log(data);
    })
    await modal.present();
    // let navigationExtras: NavigationExtras = {
    //   state: {
    //     imageArray: this.imageArray
    //   }
    // };
    // this.router.navigate(['image-model'], navigationExtras);
  }

  back() {
    this.modalController.dismiss();
  }
}
