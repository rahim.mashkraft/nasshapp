import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TwillioModelPageRoutingModule } from './twillio-model-routing.module';

import { TwillioModelPage } from './twillio-model.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TwillioModelPageRoutingModule
  ],
  declarations: [TwillioModelPage]
})
export class TwillioModelPageModule {}
