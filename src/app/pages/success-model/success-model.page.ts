import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-success-model',
  templateUrl: './success-model.page.html',
  styleUrls: ['./success-model.page.scss'],
})
export class SuccessModelPage implements OnInit {
  data
  constructor(navParams: NavParams,public modalController: ModalController,) {
    this.data = navParams.get('value'); 
    console.log("this.data : ",this.data)
   }

  ngOnInit() {
  }
  dismiss(){
    this.modalController.dismiss();
  }
}
