import { SuccessModelPage } from './../success-model/success-model.page';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { NavController, LoadingController, AlertController, AnimationController, ModalController } from '@ionic/angular';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { QuizService } from 'src/app/services/quiz.service';
import { AlertService } from 'src/app/services/alert.service';
import { BehaviorSubject } from 'rxjs';
import { DataService } from 'src/app/services/data.service';

const circleR = 80;
const circleDasharray = 2 * Math.PI * circleR

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.page.html',
  styleUrls: ['./quiz.page.scss'],
})
export class QuizPage implements OnInit {


  // counter = 0;
  // // timer = 3;
  // // interval: any;
  // countDownInterval: any;
  // loaderObj: HTMLIonLoadingElement;
  // questionsForm: FormGroup;
  // questionsArray: FormArray;
  // selectedQuestion = 0;
  // // questions = [];
  alertObj: any;
  // id: number;
  // loading = true;
  // totalpoints: number;
  // attemptid: number;
  // savingAnswer = false;
  // quiz_question_ans: any = []
  // array: any = [
  //   {
  //     maxscore: 'maxscore',
  //     questiontitle: 'questiontitle',

  //   },
  //   {
  //     maxscore: 'maxscore1',
  //     questiontitle: 'questiontitle1',

  //   },
  //   {
  //     maxscore: 'maxscore2',
  //     questiontitle: 'questiontitle2',

  //   },
  //   {
  //     quiz_question_ans: [
  //       {
  //         id: '1',
  //         answertitle: "answertitle"
  //       },
  //       {
  //         id: '2',
  //         answertitle: "answertitle2"
  //       },
  //       {
  //         id: '3',
  //         answertitle: "answertitle3"
  //       }
  //     ]
  //   }
  // ]

  ///////////////////////////////////////////////////////////
  @ViewChild('slides') slides: any;

  hasAnswered: boolean = false;
  score: number = 0;

  slideOptions: any;
  questions: any;

  start_timer: any = false;
  circleR = circleR;
  circleDasharray = circleDasharray;
  startDuration: any = 5;
  time: BehaviorSubject<string> = new BehaviorSubject('00:00');
  percent: BehaviorSubject<number> = new BehaviorSubject(100)
  timer: number;
  interval;
  state: 'start' | 'stop' = 'stop';

  quizid: any;
  data: any;
  quiztime
  start_quiz: boolean = false;
  questionid:any = 0;
  answerid:any = 0;
  attemptid:any = 0;
  constructor(private navCtrl: NavController, private alertController: AlertController, private route: ActivatedRoute,
    private quizService: QuizService,
    private router: Router,
    public modalController: ModalController,
    public dataService: DataService,
    private changeDetector: ChangeDetectorRef) {

    // this.dataService.load().then((data:any) => {
    //   this.questions = data.questions;
    //   this.nextSlide()
    // });
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.quizid = this.router.getCurrentNavigation().extras.state.quizids;
        this.quiztime = this.router.getCurrentNavigation().extras.state.quiztimes;
        console.log("videoimage after : ", this.quizid)
        console.log("quiztime after : ", this.quiztime)
        this.performquiz()
      }
    });
    // this.startQuiz();
  }
  performquiz(){
    let formData = new FormData();
    formData.append("aid", localStorage.getItem('user_id'));
    formData.append("quizid", this.quizid);
    if(this.questionid != 0){
      formData.append("questionid", this.questionid);
      formData.append("answerid", this.answerid);
      formData.append("attemptid", this.attemptid);
    }


    this.quizService.performquiz(formData).then((data: any) => {
      if (data.status) {
        this.questions = data;
    // this.showAlert(this.data)
    // this.nextSlide();
    this.startTimer(this.quiztime);
      } else {
        this.openGifModel(data.message)
        // this.alertService.presentAlertError(data.message);
      }
    })
  }
  async openGifModel(data){
    const modal = await this.modalController.create({
      component: SuccessModelPage,
      componentProps: {
        value: data
      }
    })
    modal.onDidDismiss().then((data) => {
      console.log(data);
      this.navCtrl.back();
    })
    await modal.present();
  }
  startQuiz() {
    this.start_quiz = true;
    this.dataService.load().then((data: any) => {
      this.questions = data.questions;
      this.nextSlide();
      this.startTimer(this.startDuration);
    });

  }
  startTimer(duration: number) {
    this.state = 'start';
    clearInterval(this.interval);
    this.timer = duration * 60;
    // this.updateTimeValue();
    this.interval = setInterval(() => {
      this.updateTimeValue()
    }, 1000)
  }
  stopTimer() {
    clearInterval(this.interval);
    this.time.next('00:00');
    this.state = 'stop';
  }
  updateTimeValue() {
    let minutes: any = this.timer / 60;
    let seconds: any = this.timer % 60;

    minutes = String('0' + Math.floor(minutes)).slice(-2);
    seconds = String('0' + Math.floor(seconds)).slice(-2);

    const text = minutes + ':' + seconds;
    this.time.next(text);

    const totalTime = this.startDuration * 5;
    const percentage = ((totalTime - this.timer) / totalTime) * 100;
    this.percent.next(percentage);

    --this.timer;
    if (this.timer < 0) {
      // this.startTimer(this.startDuration);
      this.stopTimer()
    }
  }


  ionViewDidLoad() {
    this.slides.lockSwipes(true);
  }

  nextSlide() {
    this.start_timer = true
    this.performquiz()
    // this.startTimer(this.startDuration)
    // this.slides.lockSwipes(false);
    // this.slides.slideNext();
    // this.slides.lockSwipes(true);
  }

  selectAnswer(answer, question) {

    console.log("answer : ",answer)
    console.log("question : ",question)
    this.questionid = answer.id;
    this.answerid = question.questionid;
    this.attemptid = question.attemptid;
    // this.hasAnswered = true;
    // answer.selected = true;
    // question.flashCardFlipped = true;

    // if (answer.correct) {
    //   this.score++;
    // }

    // setTimeout(() => {
      // this.hasAnswered = false;
      // // this.nextSlide();
      // answer.selected = false;
      // question.flashCardFlipped = false;
    // }, 1000);
  }


  ngOnInit() {
    // this.route.queryParams.subscribe(params => {
    //   // if (params) {
    //   //   console.log(params)
    //   //   // let queryParams = JSON.parse(params);
    //   //   // console.log(queryParams)
    //   // }
    //   if (this.router.getCurrentNavigation().extras.state) {
    //     this.quizid = this.router.getCurrentNavigation().extras.state.quizid;
    //     console.log("quizid : ", this.quizid)
    //     if(this.quizid){
    //       this.quizdetails()
    //     }
    //   }
    // });
    // this.questionsForm = this.formBuilder.group({
    //   id: null,
    //   questionsArray: this.formBuilder.array([])
    // });
    // this.questionsArray = this.questionsForm.get('questionsArray') as FormArray;
  }

  // startCountDown() {
  //   this.countDownInterval = setInterval(() => {
  //     this.counter--;
  //     if (this.counter === 0) {
  //       clearInterval(this.countDownInterval);
  //       this.submit(true);
  //     }
  //     this.changeDetector.detectChanges();
  //   }, 1000);
  // }

  // transform(time: number): string {

  //   if (time >= 60) {
  //     const timeInMinutes: number = Math.floor(time / 60);
  //     const hours: number = Math.floor(time / 3600);
  //     let minutes = timeInMinutes;
  //     if (minutes >= 60) {
  //       minutes = timeInMinutes % 60;
  //     }
  //     return (hours > 0 ? '0' + hours + ': ' : '')
  //       + (minutes < 10 ? '0' + minutes : minutes) +
  //       ': ' + ((time - timeInMinutes * 60) < 10 ? '0' +
  //         (time - timeInMinutes * 60) : (time - timeInMinutes * 60));
  //   } else {
  //     return (time < 10 ? '00: 0' + time : '00:' + time) + '';
  //   }
  // }

  // initializeForm() {
  //   this.questions.forEach(element => {
  //     this.questionsArray.push(this.questionGroup());
  //   });
  // }

  // questionGroup(): FormGroup {
  //   return this.formBuilder.group({
  //     questionanswerid: [null, Validators.required],
  //     points: [null]
  //   });
  // }

  async ionViewWillEnter() {
    // this.counter = 0;
    // this.timer = 3;
    // this.selectedQuestion = 0;
    // this.questions = [];
    // this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    await this.showLoader();
  }
  // getAllQuizes() {
  //   let formData = new FormData();
  //   formData.append("aid", localStorage.getItem('user_id'));
  //   formData.append("quizid", '1');
  //   this.quizService.getAllQuizes(formData).then((res: any) => {
  //     console.log("getAllQuizes : ", res);
  //     // if (res.quizcount > 0) {
  //     //   this.take_quiz = false
  //     // }
  //     // if (res.downloadcount > 0) {
  //     //   this.pdf = false
  //     //   this.pdfFile = environment.downloadurl + res.downloadlink;
  //     //   console.log("this.pdfFile : ", this.pdfFile)
  //     // }
  //   });
  // }
  // fetchQuizQuestions() {
  //   let formData = new FormData();
  //   formData.append("aid", localStorage.getItem('user_id'));
  //   formData.append("quizid", '1');
  //   this.quizService.getAllQuizes(formData).then((res: any) => {
  //     this.counter = res.quiztime;
  //     this.questions = res.quiz_questions;
  //     this.totalpoints = res.totalscore;
  //     if (this.counter && this.questions.length) {
  //       this.saveQuizAttempt();
  //     } else {
  //       this.loading = false;
  //     }
  //   },
  //   err => {
  //     this.loading = false;
  //   });
  //   // this.quizService.getAllQuizes({
  //   //   quizid: this.id,
  //   //   memberid: localStorage.getItem('user_id'),
  //   // })
  //   //   .then((res: any) => {
  //   //     this.counter = res.data[0].quiztime;
  //   //     this.questions = res.data[0].quiz_questions;
  //   //     this.totalpoints = res.data[0].totalscore;
  //   //     if (this.counter && this.questions.length) {
  //   //       this.saveQuizAttempt();
  //   //     } else {
  //   //       this.loading = false;
  //   //     }
  //   //   },
  //   //     err => {
  //   //       this.loading = false;
  //   //     });
  // }

  ionViewWillLeave() {
    if (this.alertObj) { this.alertObj.dismiss(); }
    this.changeDetector.detectChanges();
  }

  // startTimer2() {
  //   this.timer = 3;
  //   this.interval = setInterval(() => {
  //     this.timer--;
  //     this.loaderObj.message = `Staring in ${this.timer} seconds`;
  //     if (this.timer === 0) {
  //       clearInterval(this.interval);
  //       this.hideLoader();
  //       this.fetchQuizQuestions();
  //     }
  //   }, 1000);
  // }

  // saveQuizAttempt() {
  //   const obj = {
  //     memberid: localStorage.getItem('user_id'),
  //     quizid: this.id,
  //     // totalpoints: this.totalpoints
  //   };
  //   this.quizService.saveQuizAttempt(obj)
  //     .then((res: any) => {
  //       this.loading = false;
  //       this.attemptid = res.attemptid;
  //       this.initializeForm();
  //       this.startCountDown();
  //       this.changeDetector.detectChanges();

  //     },
  //       err => {
  //         this.loading = false;
  //       });
  // }

  async showLoader() {
    // await this.loadingCtrl.create({
    //   spinner: 'circles',
    //   animated: true,
    //   showBackdrop: true,
    //   translucent: true,
    //   mode: 'ios',
    //   message: `Starting in ${this.timer} seconds`
    // }).then((load: HTMLIonLoadingElement) => {
    //   this.loaderObj = load;
    //   this.loaderObj.present().then(() => {
    //     this.hideLoader();
    //     // this.startTimer2();
    //   });
    // });
  }

  hideLoader() {
    // this.loaderObj.dismiss();
  }

  back(check = 1) {
    if (check) {
      // this.navCtrl.navigateBack(['home-results/quiz']);
      this.navCtrl.back();
    } else {
      this.backConfirm();
    }
  }

  async backConfirm(header = 'Confirmation') {
    this.alertObj = await this.alertController.create({
      header: header,
      message: `
        <p> Are you sure you want to leave?</p>
      `,
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.back();
          }
        }
      ]
    });
    this.alertObj.present();
  }

  // next(type = false, Timeup = false) {
  //   this.savingAnswer = true;
  //   const answerId = this.questionsForm.get('questionsArray.' + this.selectedQuestion + '.questionanswerid').value;
  //   let selectedAns;
  //   let points = 0;
  //   this.questions[this.selectedQuestion].quiz_question_ans.forEach((element) => {
  //     if (element.id == answerId) {
  //       selectedAns = element.answertitle;
  //       points = element.score;
  //     }
  //   });
  //   const obj = {
  //     attemptid: this.attemptid,
  //     memberid: Number(localStorage.getItem('user_id')),
  //     quizid: this.id,
  //     questionid: Timeup ? null : this.questions[this.selectedQuestion].id,
  //     questionanswerid: Timeup ? null : answerId ? Number(answerId) : null,
  //     answertext: Timeup ? null : selectedAns,
  //     points: Timeup ? null : points,
  //     totalpoints: this.totalpoints,
  //     endtime: type
  //   };
  //   console.log(obj);
  //   this.quizService.saveQuizAnswer(obj)
  //     .then((res: any) => {
  //       if (!type) {
  //         this.selectedQuestion++;
  //       } else {
  //         if (Timeup) {
  //           this.alertService.showResults(res.result);
  //         } else {
  //           this.alertService.showResults(res.result, 'Results');
  //         }
  //       }
  //       this.savingAnswer = false;
  //     },
  //       err => {
  //         this.savingAnswer = false;
  //       });
  // }

  // submit(type = false) {
  //   console.log(this.questionsForm.value);
  //   clearInterval(this.countDownInterval);
  //   this.next(true, type);
  // }

}
