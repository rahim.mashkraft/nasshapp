"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.QuizPage = void 0;
var success_model_page_1 = require("./../success-model/success-model.page");
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var circleR = 80;
var circleDasharray = 2 * Math.PI * circleR;
var QuizPage = /** @class */ (function () {
    function QuizPage(navCtrl, alertController, route, quizService, router, modalController, dataService, changeDetector) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertController = alertController;
        this.route = route;
        this.quizService = quizService;
        this.router = router;
        this.modalController = modalController;
        this.dataService = dataService;
        this.changeDetector = changeDetector;
        this.hasAnswered = false;
        this.score = 0;
        this.start_timer = false;
        this.circleR = circleR;
        this.circleDasharray = circleDasharray;
        this.startDuration = 5;
        this.time = new rxjs_1.BehaviorSubject('00:00');
        this.percent = new rxjs_1.BehaviorSubject(100);
        this.state = 'stop';
        this.start_quiz = false;
        this.questionid = 0;
        this.answerid = 0;
        this.attemptid = 0;
        // this.dataService.load().then((data:any) => {
        //   this.questions = data.questions;
        //   this.nextSlide()
        // });
        this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
                _this.quizid = _this.router.getCurrentNavigation().extras.state.quizids;
                _this.quiztime = _this.router.getCurrentNavigation().extras.state.quiztimes;
                console.log("videoimage after : ", _this.quizid);
                console.log("quiztime after : ", _this.quiztime);
                _this.performquiz();
            }
        });
        // this.startQuiz();
    }
    QuizPage.prototype.performquiz = function () {
        var _this = this;
        var formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("quizid", this.quizid);
        if (this.questionid != 0) {
            formData.append("questionid", this.questionid);
            formData.append("answerid", this.answerid);
            formData.append("attemptid", this.attemptid);
        }
        this.quizService.performquiz(formData).then(function (data) {
            if (data.status) {
                _this.questions = data;
                // this.showAlert(this.data)
                // this.nextSlide();
                _this.startTimer(_this.quiztime);
            }
            else {
                _this.openGifModel(data.message);
                // this.alertService.presentAlertError(data.message);
            }
        });
    };
    QuizPage.prototype.openGifModel = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: success_model_page_1.SuccessModelPage,
                            componentProps: {
                                value: data
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (data) {
                            console.log(data);
                            _this.navCtrl.back();
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    QuizPage.prototype.startQuiz = function () {
        var _this = this;
        this.start_quiz = true;
        this.dataService.load().then(function (data) {
            _this.questions = data.questions;
            _this.nextSlide();
            _this.startTimer(_this.startDuration);
        });
    };
    QuizPage.prototype.startTimer = function (duration) {
        var _this = this;
        this.state = 'start';
        clearInterval(this.interval);
        this.timer = duration * 60;
        // this.updateTimeValue();
        this.interval = setInterval(function () {
            _this.updateTimeValue();
        }, 1000);
    };
    QuizPage.prototype.stopTimer = function () {
        clearInterval(this.interval);
        this.time.next('00:00');
        this.state = 'stop';
    };
    QuizPage.prototype.updateTimeValue = function () {
        var minutes = this.timer / 60;
        var seconds = this.timer % 60;
        minutes = String('0' + Math.floor(minutes)).slice(-2);
        seconds = String('0' + Math.floor(seconds)).slice(-2);
        var text = minutes + ':' + seconds;
        this.time.next(text);
        var totalTime = this.startDuration * 5;
        var percentage = ((totalTime - this.timer) / totalTime) * 100;
        this.percent.next(percentage);
        --this.timer;
        if (this.timer < 0) {
            // this.startTimer(this.startDuration);
            this.stopTimer();
        }
    };
    QuizPage.prototype.ionViewDidLoad = function () {
        this.slides.lockSwipes(true);
    };
    QuizPage.prototype.nextSlide = function () {
        this.start_timer = true;
        this.performquiz();
        // this.startTimer(this.startDuration)
        // this.slides.lockSwipes(false);
        // this.slides.slideNext();
        // this.slides.lockSwipes(true);
    };
    QuizPage.prototype.selectAnswer = function (answer, question) {
        console.log("answer : ", answer);
        console.log("question : ", question);
        this.questionid = answer.id;
        this.answerid = question.questionid;
        this.attemptid = question.attemptid;
        // this.hasAnswered = true;
        // answer.selected = true;
        // question.flashCardFlipped = true;
        // if (answer.correct) {
        //   this.score++;
        // }
        // setTimeout(() => {
        // this.hasAnswered = false;
        // // this.nextSlide();
        // answer.selected = false;
        // question.flashCardFlipped = false;
        // }, 1000);
    };
    QuizPage.prototype.ngOnInit = function () {
        // this.route.queryParams.subscribe(params => {
        //   // if (params) {
        //   //   console.log(params)
        //   //   // let queryParams = JSON.parse(params);
        //   //   // console.log(queryParams)
        //   // }
        //   if (this.router.getCurrentNavigation().extras.state) {
        //     this.quizid = this.router.getCurrentNavigation().extras.state.quizid;
        //     console.log("quizid : ", this.quizid)
        //     if(this.quizid){
        //       this.quizdetails()
        //     }
        //   }
        // });
        // this.questionsForm = this.formBuilder.group({
        //   id: null,
        //   questionsArray: this.formBuilder.array([])
        // });
        // this.questionsArray = this.questionsForm.get('questionsArray') as FormArray;
    };
    // startCountDown() {
    //   this.countDownInterval = setInterval(() => {
    //     this.counter--;
    //     if (this.counter === 0) {
    //       clearInterval(this.countDownInterval);
    //       this.submit(true);
    //     }
    //     this.changeDetector.detectChanges();
    //   }, 1000);
    // }
    // transform(time: number): string {
    //   if (time >= 60) {
    //     const timeInMinutes: number = Math.floor(time / 60);
    //     const hours: number = Math.floor(time / 3600);
    //     let minutes = timeInMinutes;
    //     if (minutes >= 60) {
    //       minutes = timeInMinutes % 60;
    //     }
    //     return (hours > 0 ? '0' + hours + ': ' : '')
    //       + (minutes < 10 ? '0' + minutes : minutes) +
    //       ': ' + ((time - timeInMinutes * 60) < 10 ? '0' +
    //         (time - timeInMinutes * 60) : (time - timeInMinutes * 60));
    //   } else {
    //     return (time < 10 ? '00: 0' + time : '00:' + time) + '';
    //   }
    // }
    // initializeForm() {
    //   this.questions.forEach(element => {
    //     this.questionsArray.push(this.questionGroup());
    //   });
    // }
    // questionGroup(): FormGroup {
    //   return this.formBuilder.group({
    //     questionanswerid: [null, Validators.required],
    //     points: [null]
    //   });
    // }
    QuizPage.prototype.ionViewWillEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: 
                    // this.counter = 0;
                    // this.timer = 3;
                    // this.selectedQuestion = 0;
                    // this.questions = [];
                    // this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
                    return [4 /*yield*/, this.showLoader()];
                    case 1:
                        // this.counter = 0;
                        // this.timer = 3;
                        // this.selectedQuestion = 0;
                        // this.questions = [];
                        // this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // getAllQuizes() {
    //   let formData = new FormData();
    //   formData.append("aid", localStorage.getItem('user_id'));
    //   formData.append("quizid", '1');
    //   this.quizService.getAllQuizes(formData).then((res: any) => {
    //     console.log("getAllQuizes : ", res);
    //     // if (res.quizcount > 0) {
    //     //   this.take_quiz = false
    //     // }
    //     // if (res.downloadcount > 0) {
    //     //   this.pdf = false
    //     //   this.pdfFile = environment.downloadurl + res.downloadlink;
    //     //   console.log("this.pdfFile : ", this.pdfFile)
    //     // }
    //   });
    // }
    // fetchQuizQuestions() {
    //   let formData = new FormData();
    //   formData.append("aid", localStorage.getItem('user_id'));
    //   formData.append("quizid", '1');
    //   this.quizService.getAllQuizes(formData).then((res: any) => {
    //     this.counter = res.quiztime;
    //     this.questions = res.quiz_questions;
    //     this.totalpoints = res.totalscore;
    //     if (this.counter && this.questions.length) {
    //       this.saveQuizAttempt();
    //     } else {
    //       this.loading = false;
    //     }
    //   },
    //   err => {
    //     this.loading = false;
    //   });
    //   // this.quizService.getAllQuizes({
    //   //   quizid: this.id,
    //   //   memberid: localStorage.getItem('user_id'),
    //   // })
    //   //   .then((res: any) => {
    //   //     this.counter = res.data[0].quiztime;
    //   //     this.questions = res.data[0].quiz_questions;
    //   //     this.totalpoints = res.data[0].totalscore;
    //   //     if (this.counter && this.questions.length) {
    //   //       this.saveQuizAttempt();
    //   //     } else {
    //   //       this.loading = false;
    //   //     }
    //   //   },
    //   //     err => {
    //   //       this.loading = false;
    //   //     });
    // }
    QuizPage.prototype.ionViewWillLeave = function () {
        if (this.alertObj) {
            this.alertObj.dismiss();
        }
        this.changeDetector.detectChanges();
    };
    // startTimer2() {
    //   this.timer = 3;
    //   this.interval = setInterval(() => {
    //     this.timer--;
    //     this.loaderObj.message = `Staring in ${this.timer} seconds`;
    //     if (this.timer === 0) {
    //       clearInterval(this.interval);
    //       this.hideLoader();
    //       this.fetchQuizQuestions();
    //     }
    //   }, 1000);
    // }
    // saveQuizAttempt() {
    //   const obj = {
    //     memberid: localStorage.getItem('user_id'),
    //     quizid: this.id,
    //     // totalpoints: this.totalpoints
    //   };
    //   this.quizService.saveQuizAttempt(obj)
    //     .then((res: any) => {
    //       this.loading = false;
    //       this.attemptid = res.attemptid;
    //       this.initializeForm();
    //       this.startCountDown();
    //       this.changeDetector.detectChanges();
    //     },
    //       err => {
    //         this.loading = false;
    //       });
    // }
    QuizPage.prototype.showLoader = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    QuizPage.prototype.hideLoader = function () {
        // this.loaderObj.dismiss();
    };
    QuizPage.prototype.back = function (check) {
        if (check === void 0) { check = 1; }
        if (check) {
            // this.navCtrl.navigateBack(['home-results/quiz']);
            this.navCtrl.back();
        }
        else {
            this.backConfirm();
        }
    };
    QuizPage.prototype.backConfirm = function (header) {
        if (header === void 0) { header = 'Confirmation'; }
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.alertController.create({
                                header: header,
                                message: "\n        <p> Are you sure you want to leave?</p>\n      ",
                                buttons: [
                                    {
                                        text: 'No',
                                        role: 'cancel'
                                    },
                                    {
                                        text: 'Yes',
                                        handler: function () {
                                            _this.back();
                                        }
                                    }
                                ]
                            })];
                    case 1:
                        _a.alertObj = _b.sent();
                        this.alertObj.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        core_1.ViewChild('slides')
    ], QuizPage.prototype, "slides");
    QuizPage = __decorate([
        core_1.Component({
            selector: 'app-quiz',
            templateUrl: './quiz.page.html',
            styleUrls: ['./quiz.page.scss']
        })
    ], QuizPage);
    return QuizPage;
}());
exports.QuizPage = QuizPage;
