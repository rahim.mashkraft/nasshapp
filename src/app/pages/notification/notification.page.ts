import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoaderService } from 'src/app/services/loader.service';
import { NotificationListService } from 'src/app/services/notification-list.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {

  notification_list:any;
  constructor(
    public navCtrl: NavController,
    private notificationListService: NotificationListService,
    public loaderService: LoaderService
  ) { }

  ngOnInit() {
    this.notificationList(0)
  }
  notificationList(readstatus) {
    this.loaderService.showLoader()
    let formData = new FormData();
    formData.append("aid", localStorage.getItem('user_id'));
    formData.append("readstatus", readstatus);
    this.notificationListService.notificationList(formData).then((res: any) => {
      console.log(res);
      if (res.status !== false) {
        this.notification_list = res.notifications;
        this.loaderService.hideLoader()
      } else {
        this.loaderService.hideLoader()
      }
    });
  }
  readNotification(){
    this.notificationList(1)
  }
  back() {
    this.navCtrl.back();
  }
}
// ionic cordova plugin add cordova-plugin-googleplus --variable REVERSED_CLIENT_ID=com.googleusercontent.apps.584704282410-peo3u9r6to27b0r46imes0o1jo90idl2