"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.NotificationPage = void 0;
var core_1 = require("@angular/core");
var NotificationPage = /** @class */ (function () {
    function NotificationPage(navCtrl, notificationListService, loaderService) {
        this.navCtrl = navCtrl;
        this.notificationListService = notificationListService;
        this.loaderService = loaderService;
    }
    NotificationPage.prototype.ngOnInit = function () {
        this.notificationList(0);
    };
    NotificationPage.prototype.notificationList = function (readstatus) {
        var _this = this;
        this.loaderService.showLoader();
        var formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("readstatus", readstatus);
        this.notificationListService.notificationList(formData).then(function (res) {
            console.log(res);
            if (res.status !== false) {
                _this.notification_list = res.notifications;
                _this.loaderService.hideLoader();
            }
            else {
                _this.loaderService.hideLoader();
            }
        });
    };
    NotificationPage.prototype.readNotification = function () {
        this.notificationList(1);
    };
    NotificationPage.prototype.back = function () {
        this.navCtrl.back();
    };
    NotificationPage = __decorate([
        core_1.Component({
            selector: 'app-notification',
            templateUrl: './notification.page.html',
            styleUrls: ['./notification.page.scss']
        })
    ], NotificationPage);
    return NotificationPage;
}());
exports.NotificationPage = NotificationPage;
// ionic cordova plugin add cordova-plugin-googleplus --variable REVERSED_CLIENT_ID=com.googleusercontent.apps.584704282410-peo3u9r6to27b0r46imes0o1jo90idl2
