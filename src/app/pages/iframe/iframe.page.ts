import { ModalController, NavParams } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { SafeHtml, SafeResourceUrl, SafeStyle, SafeScript, SafeUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-iframe',
  templateUrl: './iframe.page.html',
  styleUrls: ['./iframe.page.scss'],
})
export class IframePage implements OnInit {
  video: any;
  trustedVideoUrl: SafeResourceUrl | SafeHtml | SafeStyle | SafeScript | SafeUrl  = null;

  constructor(
    private navParam: NavParams,
    private domSanitizer: DomSanitizer,
    public modalController: ModalController,
  ) { }
  ionViewWillEnter(): void {
    this.video = this.navParam.get('videoLink'); 
    this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.video);
  }

  ngOnInit() {
  }
  dismiss(){
    this.modalController.dismiss();
  }
}
