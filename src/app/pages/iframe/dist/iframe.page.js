"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.IframePage = void 0;
var core_1 = require("@angular/core");
var IframePage = /** @class */ (function () {
    function IframePage(navParam, domSanitizer, modalController) {
        this.navParam = navParam;
        this.domSanitizer = domSanitizer;
        this.modalController = modalController;
        this.trustedVideoUrl = null;
    }
    IframePage.prototype.ionViewWillEnter = function () {
        this.video = this.navParam.get('videoLink');
        this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.video);
    };
    IframePage.prototype.ngOnInit = function () {
    };
    IframePage.prototype.dismiss = function () {
        this.modalController.dismiss();
    };
    IframePage = __decorate([
        core_1.Component({
            selector: 'app-iframe',
            templateUrl: './iframe.page.html',
            styleUrls: ['./iframe.page.scss']
        })
    ], IframePage);
    return IframePage;
}());
exports.IframePage = IframePage;
