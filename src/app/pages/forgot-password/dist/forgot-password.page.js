"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ForgotPasswordPage = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var ForgotPasswordPage = /** @class */ (function () {
    function ForgotPasswordPage(formBuilder, NavController, loaderService, forgotPasswordService, AlertService) {
        this.formBuilder = formBuilder;
        this.NavController = NavController;
        this.loaderService = loaderService;
        this.forgotPasswordService = forgotPasswordService;
        this.AlertService = AlertService;
    }
    ForgotPasswordPage.prototype.ngOnInit = function () {
        this.forgotPasswordForm = this.formBuilder.group({
            email: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
            toggle: ['false']
        });
    };
    ForgotPasswordPage.prototype.submit = function () {
        this.forgotpassword();
    };
    ForgotPasswordPage.prototype.forgotpassword = function () {
        var _this = this;
        this.loaderService.showLoader();
        var formData = new FormData();
        formData.append("email", this.forgotPasswordForm.value['email']);
        this.forgotPasswordService.forgotpassword(formData).then(function (data) {
            console.log("data : ", data);
            if (data.status) {
                _this.loaderService.hideLoader();
                _this.AlertService.showPwdSuccessAlert(_this.forgotPasswordForm.value['email']);
                _this.dismiss();
            }
            else {
                _this.AlertService.presentAlertError(data.message);
                _this.loaderService.hideLoader();
            }
        }, function (err) {
            _this.loaderService.hideLoader();
        });
    };
    ForgotPasswordPage.prototype.dismiss = function () {
        this.NavController.back();
    };
    ForgotPasswordPage = __decorate([
        core_1.Component({
            selector: 'app-forgot-password',
            templateUrl: './forgot-password.page.html',
            styleUrls: ['./forgot-password.page.scss']
        })
    ], ForgotPasswordPage);
    return ForgotPasswordPage;
}());
exports.ForgotPasswordPage = ForgotPasswordPage;
