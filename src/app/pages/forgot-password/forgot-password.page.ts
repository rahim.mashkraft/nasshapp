import { AlertService } from 'src/app/services/alert.service';
import { LoaderService } from 'src/app/services/loader.service';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ForgotPasswordService } from 'src/app/services/forgot-password.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  public forgotPasswordForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private NavController: NavController,
    public loaderService: LoaderService,
    private forgotPasswordService: ForgotPasswordService, 
    private AlertService: AlertService
  ) { }

  ngOnInit() {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
      toggle: ['false'],
    });
  }
  submit(){
    this.forgotpassword();
  }
  forgotpassword(){
    this.loaderService.showLoader();
    let formData = new FormData();
    formData.append("email",  this.forgotPasswordForm.value['email']);
    this.forgotPasswordService.forgotpassword(formData).then((data: any) => {
      console.log("data : ", data);
      if (data.status) {
        this.loaderService.hideLoader();
        this.AlertService.showPwdSuccessAlert(this.forgotPasswordForm.value['email'])
        this.dismiss();
      }else{
        this.AlertService.presentAlertError(data.message)
        this.loaderService.hideLoader(); 
      }
    }, err => {
      this.loaderService.hideLoader();
    });
  }
  dismiss(){
    this.NavController.back();
  }
}
