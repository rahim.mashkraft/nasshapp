"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ChatPage = void 0;
var core_1 = require("@angular/core");
var ChatPage = /** @class */ (function () {
    function ChatPage(imageUploadingService, router, route, loaderService, chatService) {
        var _this = this;
        this.imageUploadingService = imageUploadingService;
        this.router = router;
        this.route = route;
        this.loaderService = loaderService;
        this.chatService = chatService;
        this.currentPage = 1;
        this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
                _this.affiliatenumber = _this.router.getCurrentNavigation().extras.state.affiliatenumber;
                _this.contactnumber = _this.router.getCurrentNavigation().extras.state.contactnumber;
                _this.firstname = _this.router.getCurrentNavigation().extras.state.firstname;
                _this.lastname = _this.router.getCurrentNavigation().extras.state.lastname;
                console.log("this.affiliatenumber : ", _this.affiliatenumber);
                console.log("this.contactnumber : ", _this.contactnumber);
                _this.smschathistory(_this.currentPage);
            }
        });
    }
    ChatPage.prototype.smschathistory = function (currentPage, event) {
        var _this = this;
        if (event === void 0) { event = null; }
        this.loaderService.showLoader();
        var formData = new FormData();
        // formData.append("aid", localStorage.getItem('user_id'));
        formData.append("affiliatenumber", this.affiliatenumber);
        formData.append("contactnumber", this.contactnumber);
        formData.append("PageNumber", currentPage);
        this.chatService.smschathistory(formData).then(function (data) {
            console.log("data : ", data);
            if (data.status) {
                _this.isCheck = true;
                _this.chatHistory = data.contacts;
                _this.loaderService.hideLoader();
                if (currentPage == 1) {
                    _this.scrollDown();
                }
            }
            else {
                _this.isCheck = false;
                _this.loaderService.hideLoader();
            }
            if (event) {
                event.target.complete();
                if (currentPage == 1) {
                    _this.scrollDown();
                }
            }
        }, function (err) {
            _this.isCheck = false;
            _this.loaderService.hideLoader();
            if (event) {
                event.target.complete();
            }
        });
    };
    ChatPage.prototype.ngOnInit = function () {
        this.scrollDown();
    };
    ChatPage.prototype.uploadPhoto = function () {
        var image;
        image = this.imageUploadingService.uploadPhoto().then(function (data) {
            console.log("data : ", data);
        });
        console.log("image : ", image);
    };
    ChatPage.prototype.sendMsg = function () {
        if (this.message) {
            console.log(this.message.match(/.{1,155}/g));
        }
    };
    ChatPage.prototype.scrollDown = function () {
        var _this = this;
        // document.querySelector('.chat_8').scrollIntoView();
        setTimeout(function () {
            // this.content.scrollToBottom(50)
            _this.content.scrollToBottom(50);
        }, 170);
    };
    ChatPage.prototype.userTyping = function (event) {
        console.log(event);
        this.start_typing = event.target.value;
        this.scrollDown();
    };
    ChatPage.prototype.refreshDataList = function (event) {
        this.currentPage = this.currentPage + 1;
        this.smschathistory(this.currentPage, event);
    };
    __decorate([
        core_1.ViewChild('IonContent')
    ], ChatPage.prototype, "content");
    ChatPage = __decorate([
        core_1.Component({
            selector: 'app-chat',
            templateUrl: './chat.page.html',
            styleUrls: ['./chat.page.scss']
        })
    ], ChatPage);
    return ChatPage;
}());
exports.ChatPage = ChatPage;
