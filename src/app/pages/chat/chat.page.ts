import { ChatService } from './../../services/chat.service';
import { LoaderService } from 'src/app/services/loader.service';
import { ImageUploadingService } from 'src/app/services/image-uploading.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  @ViewChild('IonContent') content: IonContent;
  public message: any;
  public start_typing: any;
  public affiliatenumber: any;
  public contactnumber: any;
  public isCheck:boolean;
  public chatHistory:any;
  public firstname:any;
  public lastname:any;
  public currentPage:number = 1;
  constructor(
    private imageUploadingService: ImageUploadingService,
    private router: Router,
    private route: ActivatedRoute,
    public loaderService: LoaderService,
    private chatService: ChatService
  ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.affiliatenumber = this.router.getCurrentNavigation().extras.state.affiliatenumber;
        this.contactnumber = this.router.getCurrentNavigation().extras.state.contactnumber;
        this.firstname = this.router.getCurrentNavigation().extras.state.firstname;
        this.lastname = this.router.getCurrentNavigation().extras.state.lastname;
        console.log("this.affiliatenumber : ", this.affiliatenumber);
        console.log("this.contactnumber : ", this.contactnumber);
        this.smschathistory(this.currentPage);
      }
    });
  }
  smschathistory(currentPage, event = null) {
    this.loaderService.showLoader();
    let formData = new FormData();
    // formData.append("aid", localStorage.getItem('user_id'));
    formData.append("affiliatenumber", this.affiliatenumber);
    formData.append("contactnumber", this.contactnumber);
    formData.append("PageNumber", currentPage);
    this.chatService.smschathistory(formData).then((data: any) => {
      console.log("data : ", data);
      if (data.status) {
        this.isCheck = true;
        this.chatHistory = data.contacts;
        this.loaderService.hideLoader();
        if(currentPage == 1){
          this.scrollDown();
        }
      } else {
        this.isCheck = false;
        this.loaderService.hideLoader();
      }
      if (event) {
        event.target.complete();
        if(currentPage == 1){
          this.scrollDown();
        }
      }
    }, err => {
      this.isCheck = false;
      this.loaderService.hideLoader();
      if (event) {
        event.target.complete();
      }
    });
  }
  ngOnInit() {
    this.scrollDown();
  }
  uploadPhoto() {
    let image;
    image = this.imageUploadingService.uploadPhoto().then((data: any) => {
      console.log("data : ", data)
    })
    console.log("image : ", image)
  }
  sendMsg() {
    if (this.message) {
      console.log(this.message.match(/.{1,155}/g));
    }
  }
  scrollDown() {
    // document.querySelector('.chat_8').scrollIntoView();
    setTimeout(() => {
      // this.content.scrollToBottom(50)
      this.content.scrollToBottom(50);
    }, 170);
  }

  userTyping(event: any) {
    console.log(event);
    this.start_typing = event.target.value;
    this.scrollDown();
  }

  refreshDataList(event) {
    this.currentPage = this.currentPage+1
    this.smschathistory(this.currentPage, event);
  }
}
