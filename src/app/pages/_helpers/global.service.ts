import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  disableTabsAndMenu: Subject<boolean>;

  constructor() { 
    this.disableTabsAndMenu = new Subject();
  }
}
