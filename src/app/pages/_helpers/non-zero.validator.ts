import { FormGroup } from '@angular/forms';

export function nonZero(controlName: string, ) {
    return (formGroup: FormGroup) => {

        if (Number(controlName) < 0) {
            return {nonZero: true};
          } else {
            return null;
          }
    };


}
