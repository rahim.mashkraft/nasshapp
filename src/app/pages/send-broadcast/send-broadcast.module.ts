import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SendBroadcastPageRoutingModule } from './send-broadcast-routing.module';

import { SendBroadcastPage } from './send-broadcast.page';
import { QuillModule } from 'ngx-quill';
import { SharedModule } from 'src/app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SendBroadcastPageRoutingModule,
    QuillModule,
    SharedModule
  ],
  declarations: [SendBroadcastPage]
})
export class SendBroadcastPageModule {}
