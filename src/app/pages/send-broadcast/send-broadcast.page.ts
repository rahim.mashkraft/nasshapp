import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ContactGroupsService } from 'src/app/services/contact-groups.service';
import * as moment from 'moment';
import { BroadcastService } from 'src/app/services/broadcast.service';
import { ToastService } from 'src/app/services/toast.service';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-send-broadcast',
  templateUrl: './send-broadcast.page.html',
  styleUrls: ['./send-broadcast.page.scss'],
})
export class SendBroadcastPage implements OnInit {
  public contact_groups: any;
  public radio_check: any = 0;
  public myDate: any = '';
  public myTime: any = '';
  public hours: any;
  public minutes: any;
  public date_format: any;
  public subject: any = '';
  public message: any = '';

  public radio_check_error: any = '';
  public myDate_error: any = '';
  public myTime_error: any = '';
  public subject_error: any = '';
  public message_error: any = '';

  public check_validation: any = false;
  constructor(
    public navCtrl: NavController,
    private contactGroupService: ContactGroupsService,
    private broadcastService: BroadcastService,
    private toastService: ToastService,
    public loaderService: LoaderService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.myDate = new Date().toISOString()
    this.myTime = new Date().toISOString()
    this.radio_check = 1;
    this.getContactGroups();
  }
  getContactGroups() {
    this.contactGroupService.getContactGroups().then((res: any) => {
      console.log(res);
      if (res.status !== false) {
        this.contact_groups = res.contactgroup
      }
    });
  }
  ionFocus(event) {
    this.subject_error = ''
    console.log(event)
  }
  validation() {
    if (this.subject == '') {
      this.subject_error = 'This field is required'
      return;
    }
    if (this.message == '') {
      this.message_error = 'This field is required'
      return;
    }
    if (this.radio_check == 0) {
      this.radio_check_error = 'This field is required'
      return;
    }
    if (this.myDate == '') {
      this.myDate_error = 'This field is required'
      return;
    }
    if (this.myTime == '') {
      this.myTime_error = 'This field is required'
      return;
    }
    this.check_validation = true
  }
  broadcast() {
    this.validation();
    if (this.check_validation == true) {
      this.loaderService.showLoader()
      this.date_format = moment(this.myDate).format("MM/D/YYYY")
      this.hours = moment(this.myTime).format('h A')
      this.minutes = moment(this.myTime).format('mm')
      let formData = new FormData();
      formData.append("aid", localStorage.getItem('user_id'));
      formData.append("subject", this.subject);
      formData.append("bdate", this.date_format);
      formData.append("bhour", this.hours);
      formData.append("bmin", this.minutes);
      formData.append("cid", this.radio_check);
      formData.append("message", this.message);
      this.broadcastService.broadcast(formData).then((res: any) => {
        console.log(res);
        if (res.status !== false) {
          this.toastService.showToast('Email Broadcast sent successfully.')
          this.radio_check = 0;
          this.myDate = '';
          this.myTime = '';
          this.subject = '';
          this.message = '';
          this.loaderService.hideLoader()
          this.back();
          // this.contact_groups = res.contactgroup
        }else{
          this.loaderService.hideLoader()
        }
      });
    }
  }
  back() {
    this.navCtrl.back();
  }
}
