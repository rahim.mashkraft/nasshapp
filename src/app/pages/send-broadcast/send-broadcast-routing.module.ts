import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SendBroadcastPage } from './send-broadcast.page';

const routes: Routes = [
  {
    path: '',
    component: SendBroadcastPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SendBroadcastPageRoutingModule {}
