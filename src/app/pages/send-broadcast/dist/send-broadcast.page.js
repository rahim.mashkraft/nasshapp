"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SendBroadcastPage = void 0;
var core_1 = require("@angular/core");
var moment = require("moment");
var SendBroadcastPage = /** @class */ (function () {
    function SendBroadcastPage(navCtrl, contactGroupService, broadcastService, toastService, loaderService) {
        this.navCtrl = navCtrl;
        this.contactGroupService = contactGroupService;
        this.broadcastService = broadcastService;
        this.toastService = toastService;
        this.loaderService = loaderService;
        this.radio_check = 0;
        this.myDate = '';
        this.myTime = '';
        this.subject = '';
        this.message = '';
        this.radio_check_error = '';
        this.myDate_error = '';
        this.myTime_error = '';
        this.subject_error = '';
        this.message_error = '';
        this.check_validation = false;
    }
    SendBroadcastPage.prototype.ngOnInit = function () {
    };
    SendBroadcastPage.prototype.ionViewWillEnter = function () {
        this.myDate = new Date().toISOString();
        this.myTime = new Date().toISOString();
        this.radio_check = 1;
        this.getContactGroups();
    };
    SendBroadcastPage.prototype.getContactGroups = function () {
        var _this = this;
        this.contactGroupService.getContactGroups().then(function (res) {
            console.log(res);
            if (res.status !== false) {
                _this.contact_groups = res.contactgroup;
            }
        });
    };
    SendBroadcastPage.prototype.ionFocus = function (event) {
        this.subject_error = '';
        console.log(event);
    };
    SendBroadcastPage.prototype.validation = function () {
        if (this.subject == '') {
            this.subject_error = 'This field is required';
            return;
        }
        if (this.message == '') {
            this.message_error = 'This field is required';
            return;
        }
        if (this.radio_check == 0) {
            this.radio_check_error = 'This field is required';
            return;
        }
        if (this.myDate == '') {
            this.myDate_error = 'This field is required';
            return;
        }
        if (this.myTime == '') {
            this.myTime_error = 'This field is required';
            return;
        }
        this.check_validation = true;
    };
    SendBroadcastPage.prototype.broadcast = function () {
        var _this = this;
        this.validation();
        if (this.check_validation == true) {
            this.loaderService.showLoader();
            this.date_format = moment(this.myDate).format("MM/D/YYYY");
            this.hours = moment(this.myTime).format('h A');
            this.minutes = moment(this.myTime).format('mm');
            var formData = new FormData();
            formData.append("aid", localStorage.getItem('user_id'));
            formData.append("subject", this.subject);
            formData.append("bdate", this.date_format);
            formData.append("bhour", this.hours);
            formData.append("bmin", this.minutes);
            formData.append("cid", this.radio_check);
            formData.append("message", this.message);
            this.broadcastService.broadcast(formData).then(function (res) {
                console.log(res);
                if (res.status !== false) {
                    _this.toastService.showToast('Email Broadcast sent successfully.');
                    _this.radio_check = 0;
                    _this.myDate = '';
                    _this.myTime = '';
                    _this.subject = '';
                    _this.message = '';
                    _this.loaderService.hideLoader();
                    _this.back();
                    // this.contact_groups = res.contactgroup
                }
                else {
                    _this.loaderService.hideLoader();
                }
            });
        }
    };
    SendBroadcastPage.prototype.back = function () {
        this.navCtrl.back();
    };
    SendBroadcastPage = __decorate([
        core_1.Component({
            selector: 'app-send-broadcast',
            templateUrl: './send-broadcast.page.html',
            styleUrls: ['./send-broadcast.page.scss']
        })
    ], SendBroadcastPage);
    return SendBroadcastPage;
}());
exports.SendBroadcastPage = SendBroadcastPage;
