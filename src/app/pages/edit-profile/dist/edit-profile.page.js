"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.EditProfilePage = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var environment_1 = require("src/environments/environment");
var must_match_validator_1 = require("../_helpers/must-match.validator");
var EditProfilePage = /** @class */ (function () {
    function EditProfilePage(navCtrl, imageUploadingService, formBuilder, updateProfileService, toastService, eventsService, loaderService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.imageUploadingService = imageUploadingService;
        this.formBuilder = formBuilder;
        this.updateProfileService = updateProfileService;
        this.toastService = toastService;
        this.eventsService = eventsService;
        this.loaderService = loaderService;
        this.userData = {
            aid: '',
            firstname: '',
            lastname: '',
            email: '',
            profileimage: '',
            address: '',
            city: '',
            country: '',
            phone: '',
            username: '',
            zipcode: '',
            state: ''
        };
        this.eye = false;
        this.passwordType = 'password';
        this.c_eye = false;
        this.c_passwordType = 'password';
        this.environment = environment_1.environment;
        this.eventsService.subscribe('pictureChanged', function (data) {
            _this.userData.profileimage = data.profileimage;
            // this.profileImage = environment.profileImageUrl+this.userData.profileimage
            // console.log("this.profileImage 2 : ",this.profileImage)
            // setTimeout(() => {
            //   this.cdr.detectChanges()
            // }, 1000);
        });
    }
    EditProfilePage.prototype.showPassword = function (value) {
        if (value == 0) {
            this.eye = true;
            this.passwordType = 'text';
        }
        else {
            this.c_eye = true;
            this.c_passwordType = 'text';
        }
    };
    EditProfilePage.prototype.hidePassword = function (value) {
        if (value == 0) {
            this.eye = false;
            this.passwordType = 'password';
        }
        else {
            this.c_eye = false;
            this.c_passwordType = 'password';
        }
    };
    EditProfilePage.prototype.ngOnInit = function () {
        this.userData.aid = localStorage.getItem('user_id');
        this.userData.firstname = localStorage.getItem('firstname');
        this.userData.lastname = localStorage.getItem('lastname');
        this.userData.email = localStorage.getItem('email');
        this.userData.profileimage = localStorage.getItem('profileimage');
        this.userData.address = localStorage.getItem('address');
        this.userData.city = localStorage.getItem('city');
        this.userData.country = localStorage.getItem('country');
        this.userData.phone = localStorage.getItem('phone');
        this.userData.username = localStorage.getItem('username');
        this.userData.zipcode = localStorage.getItem('zipcode');
        this.userData.state = localStorage.getItem('state');
        console.log(this.userData);
        this.onEditProfile = this.formBuilder.group({
            firstname: [this.userData.firstname, forms_1.Validators.compose([forms_1.Validators.required])],
            lastname: [this.userData.lastname, forms_1.Validators.compose([forms_1.Validators.required])],
            city: [this.userData.city, forms_1.Validators.compose([forms_1.Validators.required])],
            country: [this.userData.country, forms_1.Validators.compose([forms_1.Validators.required])],
            address: [this.userData.address, forms_1.Validators.compose([])],
            phone: [this.userData.phone, forms_1.Validators.compose([])],
            zipcode: [this.userData.zipcode, forms_1.Validators.compose([])],
            state: [this.userData.state, forms_1.Validators.compose([])]
        }, {
        // validator : nonZero ('age' )
        }),
            this.onChangePassword = this.formBuilder.group({
                password: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(6)])],
                confirm_password: ['', forms_1.Validators.compose([forms_1.Validators.required,])]
            }, {
                validator: must_match_validator_1.mustMatch('password', 'confirm_password')
            });
    };
    EditProfilePage.prototype.updateProfile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var formData;
            var _this = this;
            return __generator(this, function (_a) {
                this.loaderService.showLoader();
                formData = new FormData();
                formData.append("aid", this.userData.aid);
                formData.append("firstname", this.onEditProfile.value['firstname']);
                formData.append("lastname", this.onEditProfile.value['lastname']);
                formData.append("city", this.onEditProfile.value['city']);
                formData.append("country", this.onEditProfile.value['country']);
                formData.append("phone", this.onEditProfile.value['phone']);
                formData.append("address", this.onEditProfile.value['address']);
                formData.append("zipcode", this.onEditProfile.value['zipcode']);
                formData.append("state", this.onEditProfile.value['state']);
                this.updateProfileService.updateprofile(formData)
                    .then(function (res) {
                    // console.log(res);
                    if (res.status !== false) {
                        localStorage.setItem('firstname', res.firstname);
                        localStorage.setItem('lastname', res.lastname);
                        localStorage.setItem('address', res.address);
                        localStorage.setItem('city', res.city);
                        localStorage.setItem('country', res.country);
                        localStorage.setItem('phone', res.phone);
                        localStorage.setItem('zipcode', res.zipcode);
                        localStorage.setItem('state', res.state);
                        var userData = {
                            firstname: res.firstname,
                            lastname: res.lastname
                        };
                        _this.eventsService.publish('nameChanged', userData);
                        _this.loaderService.hideLoader();
                        _this.toastService.showToastUpdateProfile('Profile Update Successfully');
                    }
                    else {
                        _this.loaderService.hideLoader();
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    EditProfilePage.prototype.updatePassword = function () {
        // this.userService.updatePassword(data)
        //   .subscribe((res: any) => {
        //     // console.log(res);
        //     if (res.status !== false) {
        //       this.onChangePassword.reset('password');
        //       this.toastService.showToastUpdateProfile('Password Update Successfully');
        //     }
        //   });
    };
    EditProfilePage.prototype.back = function () {
        this.navCtrl.back();
    };
    EditProfilePage.prototype.uploadPhoto = function () {
        this.imageUploadingService.uploadPhoto();
    };
    EditProfilePage = __decorate([
        core_1.Component({
            selector: 'app-edit-profile',
            templateUrl: './edit-profile.page.html',
            styleUrls: ['./edit-profile.page.scss']
        })
    ], EditProfilePage);
    return EditProfilePage;
}());
exports.EditProfilePage = EditProfilePage;
