import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { EventsService } from 'src/app/services/events.service';
import { ImageUploadingService } from 'src/app/services/image-uploading.service';
import { LoaderService } from 'src/app/services/loader.service';
import { ToastService } from 'src/app/services/toast.service';
import { UpdateProfileService } from 'src/app/services/update-profile.service';
import { environment } from 'src/environments/environment';
import { mustMatch } from '../_helpers/must-match.validator';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  public onEditProfile: FormGroup;
  public onChangePassword: FormGroup;

  userData: any = {
    aid: '',
    firstname: '',
    lastname: '',
    email: '',
    profileimage: '',
    address: '',
    city: '',
    country: '',
    phone: '',
    username: '',
    zipcode: '',
    state: '',
  }
  environment: any;
  public eye: boolean = false;
  public passwordType:any='password';
  public c_eye: boolean = false;
  public c_passwordType:any='password';
  constructor(
    public navCtrl: NavController,
    private imageUploadingService: ImageUploadingService,
    private formBuilder: FormBuilder,
    private updateProfileService: UpdateProfileService,
    private toastService: ToastService,
    private eventsService: EventsService,
    public loaderService: LoaderService
  ) {
    this.environment = environment;
    this.eventsService.subscribe('pictureChanged', (data) => {
      this.userData.profileimage = data.profileimage;
      // this.profileImage = environment.profileImageUrl+this.userData.profileimage
      // console.log("this.profileImage 2 : ",this.profileImage)
      // setTimeout(() => {
      //   this.cdr.detectChanges()
      // }, 1000);
    });
   }
   showPassword(value){
    if(value == 0){
      this.eye = true;
      this.passwordType =  'text';
    }else{
      this.c_eye = true;
      this.c_passwordType =  'text';
    }
  }
  hidePassword(value){
    if(value == 0){
      this.eye = false;
      this.passwordType =  'password';
    }else{
      this.c_eye = false;
      this.c_passwordType =  'password';
    }
  }
  ngOnInit() {
    this.userData.aid = localStorage.getItem('user_id');
    this.userData.firstname = localStorage.getItem('firstname');
    this.userData.lastname = localStorage.getItem('lastname');
    this.userData.email = localStorage.getItem('email');
    this.userData.profileimage = localStorage.getItem('profileimage');
    this.userData.address = localStorage.getItem('address');
    this.userData.city = localStorage.getItem('city');
    this.userData.country = localStorage.getItem('country');
    this.userData.phone = localStorage.getItem('phone');
    this.userData.username = localStorage.getItem('username');
    this.userData.zipcode = localStorage.getItem('zipcode');
    this.userData.state = localStorage.getItem('state');

    console.log(this.userData)
    this.onEditProfile = this.formBuilder.group({
      firstname: [this.userData.firstname, Validators.compose([Validators.required],)],
      lastname: [this.userData.lastname, Validators.compose([Validators.required],)],
      city: [this.userData.city, Validators.compose([Validators.required])],
      country: [this.userData.country, Validators.compose([Validators.required])],
      address: [this.userData.address, Validators.compose([])],
      phone: [this.userData.phone, Validators.compose([])],
      zipcode: [this.userData.zipcode, Validators.compose([])],
      state: [this.userData.state, Validators.compose([])],
    }, {
      // validator : nonZero ('age' )
    }),
      this.onChangePassword = this.formBuilder.group({
        password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
        confirm_password: ['', Validators.compose([Validators.required,])],
      },
        {
          validator: mustMatch('password', 'confirm_password')
        },
      );
  }
  async updateProfile() {
    this.loaderService.showLoader()
    let formData = new FormData();
    formData.append("aid", this.userData.aid);
    formData.append("firstname", this.onEditProfile.value['firstname']);
    formData.append("lastname", this.onEditProfile.value['lastname']);
    formData.append("city", this.onEditProfile.value['city']);
    formData.append("country", this.onEditProfile.value['country']);
    formData.append("phone", this.onEditProfile.value['phone']);
    formData.append("address", this.onEditProfile.value['address']);
    formData.append("zipcode", this.onEditProfile.value['zipcode']);
    formData.append("state", this.onEditProfile.value['state']);
    this.updateProfileService.updateprofile(formData)
      .then((res: any) => {
        // console.log(res);
        if (res.status !== false) {
          localStorage.setItem('firstname', res.firstname);
          localStorage.setItem('lastname', res.lastname);
          localStorage.setItem('address', res.address);
          localStorage.setItem('city', res.city);
          localStorage.setItem('country', res.country);
          localStorage.setItem('phone', res.phone);
          localStorage.setItem('zipcode', res.zipcode);
          localStorage.setItem('state', res.state);
          const userData = {
            firstname : res.firstname,
            lastname : res.lastname
          };
          this.eventsService.publish('nameChanged', userData);
          this.loaderService.hideLoader()
          this.toastService.showToastUpdateProfile('Profile Update Successfully');
        }else{
          this.loaderService.hideLoader()
        }
      })
  }
  updatePassword() {
    // this.userService.updatePassword(data)
    //   .subscribe((res: any) => {
    //     // console.log(res);
    //     if (res.status !== false) {
    //       this.onChangePassword.reset('password');
    //       this.toastService.showToastUpdateProfile('Password Update Successfully');
    //     }
    //   });
  }
  back() {
    this.navCtrl.back();
  }
  uploadPhoto() {
    this.imageUploadingService.uploadPhoto()
  }
}
