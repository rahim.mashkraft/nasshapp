import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ContactGroupsService } from 'src/app/services/contact-groups.service';
import { LoaderService } from 'src/app/services/loader.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-select-group',
  templateUrl: './select-group.page.html',
  styleUrls: ['./select-group.page.scss'],
})
export class SelectGroupPage implements OnInit {

  contact_groups: any;
  checkedItems: any

  postData: any;
  isChecked: any = false;
  radio_check: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public navCtrl: NavController,
    private contactGroupService: ContactGroupsService,
    public loaderService: LoaderService,
    private toastService: ToastService
  ) {
    this.postData = []
    this.radio_check = 1;
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.checkedItems = this.router.getCurrentNavigation().extras.state.checkedItems;
        console.log("this.checkedItems : ", this.checkedItems)
        this.checkedItems.forEach(obj => {
          console.log(obj)
        });
        for (let i = 0; i < this.checkedItems.length; i++) {
          let obj
          obj = {
            firstname: '',
            lastname: '',
            email: '',
            phone: 0
          }
          obj.firstname = this.checkedItems[i]._objectInstance.name.givenName
          obj.lastname = this.checkedItems[i]._objectInstance.name.familyName
          if(this.checkedItems[i]._objectInstance.emails){
            obj.email = this.checkedItems[i]._objectInstance.emails[0].value
          }
          if(this.checkedItems[i]._objectInstance.ims){
            obj.email = this.checkedItems[i]._objectInstance.ims[0].value
          }
          if(this.checkedItems[i]._objectInstance.phoneNumbers){
            obj.phone = this.checkedItems[i]._objectInstance.phoneNumbers[0].value
          }
          this.postData.push(obj)
        }
        console.log("this.postData : ", this.postData)
      }
    });
  }

  ngOnInit() {
    this.getContactGroups()
  }

  getContactGroups() {
    this.contactGroupService.getContactGroups()
      .then((res: any) => {
        console.log(res);
        if (res.status !== false) {
          this.contact_groups = res.contactgroup
        }
      });
  }
  responseData
  submit() {
    this.loaderService.showLoader()
    let jj = 0;
    let formData = new FormData();
    console.log("this.radio_check : ", this.radio_check)
    for (let data of this.postData) {
      this.responseData = data;
      console.log(this.responseData)
      if(this.responseData.firstname == undefined){
        formData.append("postdata[" + jj + "][firstname]", null);
      }else{
        formData.append("postdata[" + jj + "][firstname]", this.responseData.firstname);
      }
      if(this.responseData.lastname == undefined){
        formData.append("postdata[" + jj + "][lastname]", null);
      }else{
        formData.append("postdata[" + jj + "][lastname]", this.responseData.lastname);
      }
      if(this.responseData.email == undefined){
        formData.append("postdata[" + jj + "][email]", null);
      }else{
        formData.append("postdata[" + jj + "][email]", this.responseData.email);
      }
      formData.append("postdata[" + jj + "][phone]", this.responseData.phone);
      jj++;
    }
    formData.append("aid", localStorage.getItem('user_id'));
    formData.append("group_id", this.radio_check);
    this.contactGroupService.contactsImport(formData).then((res: any) => {
      console.log(res);
      if (res.status !== false) {
        this.toastService.showToast('Contacts successfully imported form mobile to your account.')

        this.loaderService.hideLoader()
        // this.back();
        setTimeout(() => {
          this.navCtrl.navigateRoot('/tabs');
        },500)
        // this.contact_groups = res.contactgroup
      }else{
        this.loaderService.hideLoader()
      }
    })
  }
  back() {
    this.navCtrl.back();
  }
}
