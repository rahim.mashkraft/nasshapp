"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SelectGroupPage = void 0;
var core_1 = require("@angular/core");
var SelectGroupPage = /** @class */ (function () {
    function SelectGroupPage(router, route, navCtrl, contactGroupService, loaderService, toastService) {
        var _this = this;
        this.router = router;
        this.route = route;
        this.navCtrl = navCtrl;
        this.contactGroupService = contactGroupService;
        this.loaderService = loaderService;
        this.toastService = toastService;
        this.isChecked = false;
        this.postData = [];
        this.radio_check = 1;
        this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
                _this.checkedItems = _this.router.getCurrentNavigation().extras.state.checkedItems;
                console.log("this.checkedItems : ", _this.checkedItems);
                _this.checkedItems.forEach(function (obj) {
                    console.log(obj);
                });
                for (var i = 0; i < _this.checkedItems.length; i++) {
                    var obj = void 0;
                    obj = {
                        firstname: '',
                        lastname: '',
                        email: '',
                        phone: 0
                    };
                    obj.firstname = _this.checkedItems[i]._objectInstance.name.givenName;
                    obj.lastname = _this.checkedItems[i]._objectInstance.name.familyName;
                    if (_this.checkedItems[i]._objectInstance.emails) {
                        obj.email = _this.checkedItems[i]._objectInstance.emails[0].value;
                    }
                    if (_this.checkedItems[i]._objectInstance.ims) {
                        obj.email = _this.checkedItems[i]._objectInstance.ims[0].value;
                    }
                    if (_this.checkedItems[i]._objectInstance.phoneNumbers) {
                        obj.phone = _this.checkedItems[i]._objectInstance.phoneNumbers[0].value;
                    }
                    _this.postData.push(obj);
                }
                console.log("this.postData : ", _this.postData);
            }
        });
    }
    SelectGroupPage.prototype.ngOnInit = function () {
        this.getContactGroups();
    };
    SelectGroupPage.prototype.getContactGroups = function () {
        var _this = this;
        this.contactGroupService.getContactGroups()
            .then(function (res) {
            console.log(res);
            if (res.status !== false) {
                _this.contact_groups = res.contactgroup;
            }
        });
    };
    SelectGroupPage.prototype.submit = function () {
        var _this = this;
        this.loaderService.showLoader();
        var jj = 0;
        var formData = new FormData();
        console.log("this.radio_check : ", this.radio_check);
        for (var _i = 0, _a = this.postData; _i < _a.length; _i++) {
            var data = _a[_i];
            this.responseData = data;
            console.log(this.responseData);
            if (this.responseData.firstname == undefined) {
                formData.append("postdata[" + jj + "][firstname]", null);
            }
            else {
                formData.append("postdata[" + jj + "][firstname]", this.responseData.firstname);
            }
            if (this.responseData.lastname == undefined) {
                formData.append("postdata[" + jj + "][lastname]", null);
            }
            else {
                formData.append("postdata[" + jj + "][lastname]", this.responseData.lastname);
            }
            if (this.responseData.email == undefined) {
                formData.append("postdata[" + jj + "][email]", null);
            }
            else {
                formData.append("postdata[" + jj + "][email]", this.responseData.email);
            }
            formData.append("postdata[" + jj + "][phone]", this.responseData.phone);
            jj++;
        }
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("group_id", this.radio_check);
        this.contactGroupService.contactsImport(formData).then(function (res) {
            console.log(res);
            if (res.status !== false) {
                _this.toastService.showToast('Contacts successfully imported form mobile to your account.');
                _this.loaderService.hideLoader();
                // this.back();
                setTimeout(function () {
                    _this.navCtrl.navigateRoot('/tabs');
                }, 500);
                // this.contact_groups = res.contactgroup
            }
            else {
                _this.loaderService.hideLoader();
            }
        });
    };
    SelectGroupPage.prototype.back = function () {
        this.navCtrl.back();
    };
    SelectGroupPage = __decorate([
        core_1.Component({
            selector: 'app-select-group',
            templateUrl: './select-group.page.html',
            styleUrls: ['./select-group.page.scss']
        })
    ], SelectGroupPage);
    return SelectGroupPage;
}());
exports.SelectGroupPage = SelectGroupPage;
