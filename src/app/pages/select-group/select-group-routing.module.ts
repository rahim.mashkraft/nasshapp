import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectGroupPage } from './select-group.page';

const routes: Routes = [
  {
    path: '',
    component: SelectGroupPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectGroupPageRoutingModule {}
