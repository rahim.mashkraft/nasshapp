import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectGroupPageRoutingModule } from './select-group-routing.module';

import { SelectGroupPage } from './select-group.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectGroupPageRoutingModule
  ],
  declarations: [SelectGroupPage]
})
export class SelectGroupPageModule {}
