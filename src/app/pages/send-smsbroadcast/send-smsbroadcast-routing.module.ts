import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SendSMSBroadcastPage } from './send-smsbroadcast.page';

const routes: Routes = [
  {
    path: '',
    component: SendSMSBroadcastPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SendSMSBroadcastPageRoutingModule {}
