"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SendSMSBroadcastPage = void 0;
var core_1 = require("@angular/core");
var moment = require("moment");
var SendSMSBroadcastPage = /** @class */ (function () {
    function SendSMSBroadcastPage(navCtrl, contactGroupService, toastService, broadcastService, loaderService) {
        this.navCtrl = navCtrl;
        this.contactGroupService = contactGroupService;
        this.toastService = toastService;
        this.broadcastService = broadcastService;
        this.loaderService = loaderService;
        this.radio_check = 0;
        this.myDate = '';
        this.myTime = '';
        this.message = '';
        this.radio_check_error = '';
        this.myDate_error = '';
        this.myTime_error = '';
        this.message_error = '';
        this.check_validation = false;
    }
    SendSMSBroadcastPage.prototype.ngOnInit = function () {
    };
    SendSMSBroadcastPage.prototype.ionFocus = function (event) {
        this.message_error = '';
        console.log(event);
    };
    SendSMSBroadcastPage.prototype.validation = function () {
        if (this.message == '') {
            this.message_error = 'This field is required';
            return;
        }
        if (this.radio_check == 0) {
            this.radio_check_error = 'This field is required';
            return;
        }
        if (this.myDate == '') {
            this.myDate_error = 'This field is required';
            return;
        }
        if (this.myTime == '') {
            this.myTime_error = 'This field is required';
            return;
        }
        this.check_validation = true;
    };
    SendSMSBroadcastPage.prototype.showdate = function () {
        this.date_format = moment(this.myDate).format("MM/D/YYYY");
    };
    SendSMSBroadcastPage.prototype.showtime = function () {
        this.hours = moment(this.myTime).format('h A');
        this.minutes = moment(this.myTime).format('mm');
    };
    SendSMSBroadcastPage.prototype.ionViewWillEnter = function () {
        this.myDate = new Date().toISOString();
        this.myTime = new Date().toISOString();
        this.radio_check = 1;
        this.getContactGroups();
    };
    SendSMSBroadcastPage.prototype.getContactGroups = function () {
        var _this = this;
        this.contactGroupService.getContactGroups().then(function (res) {
            console.log(res);
            if (res.status !== false) {
                _this.contact_groups = res.contactgroup;
            }
        });
    };
    SendSMSBroadcastPage.prototype.submit = function () {
        var _this = this;
        this.validation();
        if (this.check_validation == true) {
            this.loaderService.showLoader();
            this.date_format = moment(this.myDate).format("MM/D/YYYY");
            this.hours = moment(this.myTime).format('h A');
            this.minutes = moment(this.myTime).format('mm');
            var formData = new FormData();
            formData.append("aid", localStorage.getItem('user_id'));
            formData.append("bdate", this.date_format);
            formData.append("bhour", this.hours);
            formData.append("bmin", this.minutes);
            formData.append("cid", this.radio_check);
            formData.append("message", this.message);
            this.broadcastService.smsbroadcast(formData).then(function (res) {
                if (res.status !== false) {
                    _this.toastService.showToast('SMS Broadcast sent successfully.');
                    _this.radio_check = 0;
                    _this.myDate = '';
                    _this.myTime = '';
                    _this.message = '';
                    _this.loaderService.hideLoader();
                    _this.back();
                    // this.contact_groups = res.contactgroup
                }
            });
        }
    };
    SendSMSBroadcastPage.prototype.back = function () {
        this.navCtrl.back();
    };
    SendSMSBroadcastPage = __decorate([
        core_1.Component({
            selector: 'app-send-smsbroadcast',
            templateUrl: './send-smsbroadcast.page.html',
            styleUrls: ['./send-smsbroadcast.page.scss']
        })
    ], SendSMSBroadcastPage);
    return SendSMSBroadcastPage;
}());
exports.SendSMSBroadcastPage = SendSMSBroadcastPage;
