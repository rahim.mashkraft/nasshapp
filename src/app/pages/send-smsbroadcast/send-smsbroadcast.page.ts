import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ContactGroupsService } from 'src/app/services/contact-groups.service';
import * as moment from 'moment';
import { ToastService } from 'src/app/services/toast.service';
import { BroadcastService } from 'src/app/services/broadcast.service';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-send-smsbroadcast',
  templateUrl: './send-smsbroadcast.page.html',
  styleUrls: ['./send-smsbroadcast.page.scss'],
})
export class SendSMSBroadcastPage implements OnInit {

  public contact_groups: any;
  public radio_check: any = 0;
  public myDate: any = '';
  public myTime: any = '';
  public hours: any;
  public minutes: any;
  public date_format: any;
  public message: any = '';

  public radio_check_error: any = '';
  public myDate_error: any = '';
  public myTime_error: any = '';
  public message_error: any = '';

  public check_validation: any = false;
  constructor(
    public navCtrl: NavController,
    private contactGroupService: ContactGroupsService,
    private toastService: ToastService,
    private broadcastService: BroadcastService,
    public loaderService: LoaderService
  ) { }

  ngOnInit() {
  }
  ionFocus(event) {
    this.message_error = ''
    console.log(event)
  }
  validation() {
    if (this.message == '') {
      this.message_error = 'This field is required'
      return;
    }
    if (this.radio_check == 0) {
      this.radio_check_error = 'This field is required'
      return;
    }
    if (this.myDate == '') {
      this.myDate_error = 'This field is required'
      return;
    }
    if (this.myTime == '') {
      this.myTime_error = 'This field is required'
      return;
    }
    this.check_validation = true
  }
  showdate() {
    this.date_format = moment(this.myDate).format("MM/D/YYYY")
  }
  showtime() {
    this.hours = moment(this.myTime).format('h A')
    this.minutes = moment(this.myTime).format('mm')
  }
  ionViewWillEnter() {
    this.myDate = new Date().toISOString()
    this.myTime = new Date().toISOString()
    this.radio_check = 1;
    this.getContactGroups();
  }
  getContactGroups() {
      this.contactGroupService.getContactGroups().then((res: any) => {
        console.log(res);
        if (res.status !== false) {
          this.contact_groups = res.contactgroup
        }
      });
  }
  submit(){
    this.validation()
    if (this.check_validation == true) {
      this.loaderService.showLoader();
      this.date_format = moment(this.myDate).format("MM/D/YYYY")
      this.hours = moment(this.myTime).format('h A')
      this.minutes = moment(this.myTime).format('mm')
      let formData = new FormData();
      formData.append("aid", localStorage.getItem('user_id'));
      formData.append("bdate", this.date_format);
      formData.append("bhour", this.hours);
      formData.append("bmin", this.minutes);
      formData.append("cid", this.radio_check);
      formData.append("message", this.message);
      this.broadcastService.smsbroadcast(formData).then((res:any) => {
        if (res.status !== false) {
          this.toastService.showToast('SMS Broadcast sent successfully.')
          this.radio_check = 0;
          this.myDate = '';
          this.myTime = '';
          this.message = '';
          this.loaderService.hideLoader()
          this.back();
          // this.contact_groups = res.contactgroup
        }
      })
        
    }
  }
  back() {
    this.navCtrl.back();
  }

}
