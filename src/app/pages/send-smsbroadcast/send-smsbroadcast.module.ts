import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SendSMSBroadcastPageRoutingModule } from './send-smsbroadcast-routing.module';

import { SendSMSBroadcastPage } from './send-smsbroadcast.page';
import { SharedModule } from 'src/app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SendSMSBroadcastPageRoutingModule,
    SharedModule
  ],
  declarations: [SendSMSBroadcastPage]
})
export class SendSMSBroadcastPageModule {}
