"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ImageModelPage = void 0;
var core_1 = require("@angular/core");
var angular_1 = require("@ionic/angular");
var ImageModelPage = /** @class */ (function () {
    function ImageModelPage(navContrl, router, route) {
        var _this = this;
        this.navContrl = navContrl;
        this.router = router;
        this.route = route;
        this.sliderOpts = {
            zoom: {
                maxRatio: 5
            }
        };
        this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
                _this.image = _this.router.getCurrentNavigation().extras.state.imageArray;
                console.log("this.imageArr111111 : ", _this.image);
            }
        });
        // this.image = navParams.get('imageArray'); 
    }
    ImageModelPage.prototype.ngOnInit = function () {
    };
    ImageModelPage.prototype.zoom = function (zoomIn) {
        var zoom = this.slider.nativeElement.swiper.zoom;
        if (zoomIn) {
            zoom["in"]();
        }
        else {
            zoom.out();
        }
    };
    ImageModelPage.prototype.slideChanged = function () {
        var currentIndex = this.slides.getSwiper();
        console.log('Current index is', currentIndex);
    };
    ImageModelPage.prototype.dismiss = function () {
        this.navContrl.back();
        // this.router.navigateByUrl('/')
        // this.modalController.dismiss();
    };
    __decorate([
        core_1.ViewChild('slider', { read: core_1.ElementRef })
    ], ImageModelPage.prototype, "slider");
    __decorate([
        core_1.ViewChild(angular_1.IonSlides)
    ], ImageModelPage.prototype, "slides");
    ImageModelPage = __decorate([
        core_1.Component({
            selector: 'app-image-model',
            templateUrl: './image-model.page.html',
            styleUrls: ['./image-model.page.scss']
        })
    ], ImageModelPage);
    return ImageModelPage;
}());
exports.ImageModelPage = ImageModelPage;
