import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonSlides, ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-image-model',
  templateUrl: './image-model.page.html',
  styleUrls: ['./image-model.page.scss'],
})
export class ImageModelPage implements OnInit {
  @ViewChild('slider', { read: ElementRef }) slider: ElementRef;
  @ViewChild(IonSlides) slides: IonSlides;
  image: any;
  sliderOpts = {
    zoom: {
      maxRatio: 5
    }
  }
  imageArray: any;
  imageArrayPergola: any;

  constructor(
    public navContrl: NavController,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.image = this.router.getCurrentNavigation().extras.state.imageArray;
        console.log("this.imageArr111111 : ", this.image);
      }
    });
    // this.image = navParams.get('imageArray'); 
  }

  ngOnInit() {
  }
  zoom(zoomIn: boolean) {
    let zoom = this.slider.nativeElement.swiper.zoom;
    if (zoomIn) {
      zoom.in();
    } else {
      zoom.out();
    }
  }
  slideChanged() {
    let currentIndex = this.slides.getSwiper();
    console.log('Current index is', currentIndex);
  }
  dismiss() {
    this.navContrl.back()
    // this.router.navigateByUrl('/')
    // this.modalController.dismiss();
  }

}
