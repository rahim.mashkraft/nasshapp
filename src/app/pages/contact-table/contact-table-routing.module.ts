import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContactTablePage } from './contact-table.page';

const routes: Routes = [
  {
    path: '',
    component: ContactTablePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContactTablePageRoutingModule {}
