import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContactTablePageRoutingModule } from './contact-table-routing.module';

import { ContactTablePage } from './contact-table.page';
import { SharedModule } from 'src/app/modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactTablePageRoutingModule,
    SharedModule
  ],
  declarations: [ContactTablePage]
})
export class ContactTablePageModule {}
