import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { IonSelect, NavController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert.service';
import { ContactGroupsService } from 'src/app/services/contact-groups.service';
import { LoaderService } from 'src/app/services/loader.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-contact-table',
  templateUrl: './contact-table.page.html',
  styleUrls: ['./contact-table.page.scss'],
})
export class ContactTablePage implements OnInit {
  // @ViewChild('select_value', { static: false }) select_value: IonSelect;
  customAlertOptions: any = {
    // header: 'Pizza Toppings',
    // subHeader: 'Select your toppings',
    // message: '$1.00 per topping',
    // translucent: true
  };
  select_value:any;
  loading: any = false;
  contactsData: any;
  dataObj: any;

  currentPage = 1;
  lastPage = 1;

  isCheck: any =  false;

  sortBy:any;
  search_value:any;

  constructor(
    public navCtrl: NavController,
    private contactGroupsService: ContactGroupsService,
    public loaderService: LoaderService,
    private alertService: AlertService,
    private router: Router,
    private cdr: ChangeDetectorRef
  ) { 
    this.sortBy = 'n'
  }

  ngOnInit() {
  }
  ionViewDidLoad() {
  }

  selectValue(value,i,item_obj){
    console.log(value.detail.value)
    console.log(i)
    console.log(item_obj)
    if(value.detail.value == 'email'){
      if(item_obj.email == 'null'){
        // this.select_value = null
        setTimeout(() => {
          this.cdr.detectChanges()
        },500)
  
        console.log("this.select_value : ",this.select_value)
        this.alertService.generalAlert('No Email Found.')
      }else{
        let navigationExtras: NavigationExtras = {
          state: {
            item_obj: item_obj,
            pageName: 'contact'
          }
        };
        // this.router.navigate(['post-add-second', navigationExtras]);
        this.router.navigate(['send-email-contact'], navigationExtras);
      }
    }
    if(value.detail.value == 'sms'){
      if(item_obj.phone == 'null'){
        this.alertService.generalAlert('No Phone Found.')
      }else{
        let navigationExtras: NavigationExtras = {
          state: {
            item_obj: item_obj,
            pageName: 'contact'
          }
        };
        // this.router.navigate(['post-add-second', navigationExtras]);
        this.router.navigate(['send-sms-contact'], navigationExtras);
      }
    }
    if(value.detail.value == 'contact'){
      if(item_obj.phone == 'null'){
        this.alertService.generalAlert('This contact has no phone number.')
      }else{
        // let navigationExtras: NavigationExtras = {
        //   state: {
        //     item_obj: item_obj,
            // pageName: 'contact'
        //   }
        // };
        // // this.router.navigate(['post-add-second', navigationExtras]);
        // this.router.navigate(['send-sms-contact'], navigationExtras);
      }
    }
    if(value.detail.value == 'edit'){
      if(item_obj.phone == 'null'){
        this.alertService.generalAlert('This contact has no phone number.')
      }else{
        // let navigationExtras: NavigationExtras = {
        //   state: {
        //     item_obj: item_obj,
            // pageName: 'contact'
        //   }
        // };
        // // this.router.navigate(['post-add-second', navigationExtras]);
        // this.router.navigate(['send-sms-contact'], navigationExtras);
      }
    }

  }
  sortFunction(sortBy){
    console.log(sortBy)
    if(sortBy == 'n'){
      this.getContacts(this.currentPage,this.search_value,'');
    }else{
      this.getContacts(this.currentPage,this.search_value,this.sortBy);
    }
  }
  claerSearcher() {
    console.log("claerSearcher")
    this.contactsData = [];
    this.currentPage = 1;
    this.getContacts(this.currentPage,'',this.sortBy);
  }
  setFilteredItems(ev: any, currentPage) {
    console.log(currentPage)
    const val = ev.target.value;
    console.log(val)
    var str = new String(val);
    var len = str.length;
    // if the value is an empty string don't filter the items
    if (len >= 3 && val && val.trim() !== '') {
      if(this.sortBy == 'n'){
        this.getContacts(this.currentPage,this.search_value,'');
      }else{
        this.getContacts(this.currentPage,this.search_value,this.sortBy);
      }
      // this.isItemAvailable = true;
      // this.currentPage = 1;
      // let formData = new FormData();
      // formData.append("PageNumber", currentPage);
      // formData.append("searchstring", val);
      // formData.append("aid", localStorage.getItem('user_id'));
      // this.searchService.searchContacts(formData).then((data: any) => {
      //   console.log("data : ", data)
      //   this.dataObj = data
      //   // this.currentPage = this.dataObj.CurrentPage;
      //   // this.lastPage = this.dataObj.total;
      //   // if (this.currentPage == 1) {
      //     this.contactsData = this.dataObj.contacts;
      //     if(this.contactsData.length == 0){
      //       this.isCheck = true;
      //     }
      //   // } else {
      //   //   this.contactsData = [...this.contactsData, ...this.dataObj.contacts];
      //   // }
      //   // this.loaderService.hideLoader();

      // },err => {
      //   this.isCheck = true;
      //     // this.loaderService.hideLoader();
      //   });
    } else {
      // this.isItemAvailable = false;
    }
    // this.items = this.filterItems(this.searchTerm);

  }

  ionViewWillEnter() {
    this.contactsData = [];
    this.currentPage = 1;
    this.getContacts(this.currentPage,'','');
  }
  getContacts(currentPage, search_value, sort_value, event = null) {
    this.loaderService.showLoader();
    let formData = new FormData();
    formData.append("PageNumber", currentPage);
    if(search_value){
      formData.append("searchstring", search_value);
    }
    if(sort_value){
      formData.append("sort", sort_value);
    }

    formData.append("aid", localStorage.getItem('user_id'));
    this.contactGroupsService.getContacts(formData).then((data: any) => {
      console.log("data : ", data)
      this.dataObj = data
      this.currentPage = this.dataObj.CurrentPage;
      this.lastPage = this.dataObj.total;
      if (this.currentPage == 1) {
        this.contactsData = this.dataObj.contacts;
        if(this.contactsData.length == 0){
          this.isCheck = true;
        }
      } else {
        this.contactsData = [...this.contactsData, ...this.dataObj.contacts];
      }
      this.loaderService.hideLoader();
      if (event) {
        event.target.complete();
      }
    },err => {
        this.loaderService.hideLoader();
        this.isCheck = true;
        if (event) {
          event.target.complete();
        }
      });
  }
  refreshData(event) {
    this.currentPage = 1;
    this.getContacts(1,'','', event);
  }

  ConvertToInt(currentPage) {
    return parseInt(currentPage);
  }
  loadMoreData(value, event) {
    this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value)
    this.getContacts(this.currentPage,this.search_value,this.sortBy, event)
  }
  back() {
    this.navCtrl.back();
  }
}
