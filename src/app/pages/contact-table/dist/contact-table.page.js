"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.ContactTablePage = void 0;
var core_1 = require("@angular/core");
var ContactTablePage = /** @class */ (function () {
    function ContactTablePage(navCtrl, contactGroupsService, loaderService, alertService, router, cdr) {
        this.navCtrl = navCtrl;
        this.contactGroupsService = contactGroupsService;
        this.loaderService = loaderService;
        this.alertService = alertService;
        this.router = router;
        this.cdr = cdr;
        // @ViewChild('select_value', { static: false }) select_value: IonSelect;
        this.customAlertOptions = {
        // header: 'Pizza Toppings',
        // subHeader: 'Select your toppings',
        // message: '$1.00 per topping',
        // translucent: true
        };
        this.loading = false;
        this.currentPage = 1;
        this.lastPage = 1;
        this.isCheck = false;
        this.sortBy = 'n';
    }
    ContactTablePage.prototype.ngOnInit = function () {
    };
    ContactTablePage.prototype.ionViewDidLoad = function () {
    };
    ContactTablePage.prototype.selectValue = function (value, i, item_obj) {
        var _this = this;
        console.log(value.detail.value);
        console.log(i);
        console.log(item_obj);
        if (value.detail.value == 'email') {
            if (item_obj.email == 'null') {
                // this.select_value = null
                setTimeout(function () {
                    _this.cdr.detectChanges();
                }, 500);
                console.log("this.select_value : ", this.select_value);
                this.alertService.generalAlert('No Email Found.');
            }
            else {
                var navigationExtras = {
                    state: {
                        item_obj: item_obj,
                        pageName: 'contact'
                    }
                };
                // this.router.navigate(['post-add-second', navigationExtras]);
                this.router.navigate(['send-email-contact'], navigationExtras);
            }
        }
        if (value.detail.value == 'sms') {
            if (item_obj.phone == 'null') {
                this.alertService.generalAlert('No Phone Found.');
            }
            else {
                var navigationExtras = {
                    state: {
                        item_obj: item_obj,
                        pageName: 'contact'
                    }
                };
                // this.router.navigate(['post-add-second', navigationExtras]);
                this.router.navigate(['send-sms-contact'], navigationExtras);
            }
        }
        if (value.detail.value == 'contact') {
            if (item_obj.phone == 'null') {
                this.alertService.generalAlert('This contact has no phone number.');
            }
            else {
                // let navigationExtras: NavigationExtras = {
                //   state: {
                //     item_obj: item_obj,
                // pageName: 'contact'
                //   }
                // };
                // // this.router.navigate(['post-add-second', navigationExtras]);
                // this.router.navigate(['send-sms-contact'], navigationExtras);
            }
        }
        if (value.detail.value == 'edit') {
            if (item_obj.phone == 'null') {
                this.alertService.generalAlert('This contact has no phone number.');
            }
            else {
                // let navigationExtras: NavigationExtras = {
                //   state: {
                //     item_obj: item_obj,
                // pageName: 'contact'
                //   }
                // };
                // // this.router.navigate(['post-add-second', navigationExtras]);
                // this.router.navigate(['send-sms-contact'], navigationExtras);
            }
        }
    };
    ContactTablePage.prototype.sortFunction = function (sortBy) {
        console.log(sortBy);
        if (sortBy == 'n') {
            this.getContacts(this.currentPage, this.search_value, '');
        }
        else {
            this.getContacts(this.currentPage, this.search_value, this.sortBy);
        }
    };
    ContactTablePage.prototype.claerSearcher = function () {
        console.log("claerSearcher");
        this.contactsData = [];
        this.currentPage = 1;
        this.getContacts(this.currentPage, '', this.sortBy);
    };
    ContactTablePage.prototype.setFilteredItems = function (ev, currentPage) {
        console.log(currentPage);
        var val = ev.target.value;
        console.log(val);
        var str = new String(val);
        var len = str.length;
        // if the value is an empty string don't filter the items
        if (len >= 3 && val && val.trim() !== '') {
            if (this.sortBy == 'n') {
                this.getContacts(this.currentPage, this.search_value, '');
            }
            else {
                this.getContacts(this.currentPage, this.search_value, this.sortBy);
            }
            // this.isItemAvailable = true;
            // this.currentPage = 1;
            // let formData = new FormData();
            // formData.append("PageNumber", currentPage);
            // formData.append("searchstring", val);
            // formData.append("aid", localStorage.getItem('user_id'));
            // this.searchService.searchContacts(formData).then((data: any) => {
            //   console.log("data : ", data)
            //   this.dataObj = data
            //   // this.currentPage = this.dataObj.CurrentPage;
            //   // this.lastPage = this.dataObj.total;
            //   // if (this.currentPage == 1) {
            //     this.contactsData = this.dataObj.contacts;
            //     if(this.contactsData.length == 0){
            //       this.isCheck = true;
            //     }
            //   // } else {
            //   //   this.contactsData = [...this.contactsData, ...this.dataObj.contacts];
            //   // }
            //   // this.loaderService.hideLoader();
            // },err => {
            //   this.isCheck = true;
            //     // this.loaderService.hideLoader();
            //   });
        }
        else {
            // this.isItemAvailable = false;
        }
        // this.items = this.filterItems(this.searchTerm);
    };
    ContactTablePage.prototype.ionViewWillEnter = function () {
        this.contactsData = [];
        this.currentPage = 1;
        this.getContacts(this.currentPage, '', '');
    };
    ContactTablePage.prototype.getContacts = function (currentPage, search_value, sort_value, event) {
        var _this = this;
        if (event === void 0) { event = null; }
        this.loaderService.showLoader();
        var formData = new FormData();
        formData.append("PageNumber", currentPage);
        if (search_value) {
            formData.append("searchstring", search_value);
        }
        if (sort_value) {
            formData.append("sort", sort_value);
        }
        formData.append("aid", localStorage.getItem('user_id'));
        this.contactGroupsService.getContacts(formData).then(function (data) {
            console.log("data : ", data);
            _this.dataObj = data;
            _this.currentPage = _this.dataObj.CurrentPage;
            _this.lastPage = _this.dataObj.total;
            if (_this.currentPage == 1) {
                _this.contactsData = _this.dataObj.contacts;
                if (_this.contactsData.length == 0) {
                    _this.isCheck = true;
                }
            }
            else {
                _this.contactsData = __spreadArrays(_this.contactsData, _this.dataObj.contacts);
            }
            _this.loaderService.hideLoader();
            if (event) {
                event.target.complete();
            }
        }, function (err) {
            _this.loaderService.hideLoader();
            _this.isCheck = true;
            if (event) {
                event.target.complete();
            }
        });
    };
    ContactTablePage.prototype.refreshData = function (event) {
        this.currentPage = 1;
        this.getContacts(1, '', '', event);
    };
    ContactTablePage.prototype.ConvertToInt = function (currentPage) {
        return parseInt(currentPage);
    };
    ContactTablePage.prototype.loadMoreData = function (value, event) {
        this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value);
        this.getContacts(this.currentPage, this.search_value, this.sortBy, event);
    };
    ContactTablePage.prototype.back = function () {
        this.navCtrl.back();
    };
    ContactTablePage = __decorate([
        core_1.Component({
            selector: 'app-contact-table',
            templateUrl: './contact-table.page.html',
            styleUrls: ['./contact-table.page.scss']
        })
    ], ContactTablePage);
    return ContactTablePage;
}());
exports.ContactTablePage = ContactTablePage;
