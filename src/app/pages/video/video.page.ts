import { LoaderService } from 'src/app/services/loader.service';
import { QuizService } from './../../services/quiz.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { get } from 'scriptjs';
import { File } from '@ionic-native/file/ngx';
import { VideoLogService } from 'src/app/services/video-log.service';
import { AlertController, ModalController, NavController, Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { ToastService } from 'src/app/services/toast.service';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { AlertService } from 'src/app/services/alert.service';
import { Storage } from '@ionic/storage';
import { SuccessModelPage } from '../success-model/success-model.page';
import { IframePage } from '../iframe/iframe.page';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

// Define On Top 
declare var HKVideoPlayer;
@Component({
  selector: 'app-video',
  templateUrl: './video.page.html',
  styleUrls: ['./video.page.scss'],
})
export class VideoPage implements OnInit {

  public video_link: any;
  public btnPlay: any;
  public btnPause: any;
  public btnunMute: any;
  public btnMute: any;
  public endTime: any;

  public videoStartTime: any = 0;
  public adsVideoStartTime: any = 0;

  public hide_show_icon: any = true;
  public adsDisabled: any = false;

  public today = Date.now();
  public myValue: any = 0;
  public videotitle: any;
  public videourl: any;
  public videoid: any;
  public videoimage: any;
  public take_quiz: any = true;
  public pdf: any = true;
  public pdfFile: any;




  stringwithhtml: any;
  videotext: any;
  videoEndTime: number = 0;
  videoPaused: boolean = true;
  showTextarea: boolean = false;
  endAssigned: boolean;
  quizid: any;
  quiztime: any;
  data: any;
  constructor(
    private zone: NgZone,
    public router: Router,
    public route: ActivatedRoute,
    private videoLogService: VideoLogService,
    private quizService: QuizService,
    private file: File,
    public platform: Platform,
    private transfer: FileTransfer,
    public alertController: AlertController,
    private documentViewer: DocumentViewer,
    private alertService: AlertService,
    public loaderService: LoaderService,
    public storage: Storage,
    public modalController: ModalController,
    public navCntl: NavController,
    private iab: InAppBrowser
  ) {
    this.videoStartTime = 0
    this.adsVideoStartTime = 0
    this.btnPlay = true
    this.btnunMute = true
    // this.video_link = "https://s3.amazonaws.com/AST/9+min.mp4"
    // this.video_link = "http://static.videogular.com/assets/videos/elephants-dream.mp4"
    // this.video_link = "https://www.youtube.com/embed/r98doIWKudg"
    // this.video_link = "https://www.youtube.com/watch?v=01MJuw-xgQo"

    let listaFrames = document.getElementsByTagName("iframe");
    console.log("listaFrames : ",)
    // this.StartTimer();
    //   let timeout = this.randomIntFromInterval(10, 10000)
    //   console.log("randomIntFromInterval : ", timeout)
    //   setTimeout(() => {
    //     console.log("setTimeout : ")
    //   }, timeout);
    this.loaderService.showLoader()
  }

  openUrl(){
    this.platform.ready().then(() => {
      const options: InAppBrowserOptions = {
        location: 'no',//Or 'no'
        //clearcache : 'no',
        //clearsessioncache : 'yes',
        //toolbar : 'yes', //iOS only
        toolbar: 'yes', //iOS only
        closebuttoncaption: "X",
        closebuttoncolor: '#000000',
        toolbarcolor: '#f8f8f8',
        hideurlbar: 'yes',
        navigationbuttoncolor: '#ffffff',
        hidenavigationbuttons: 'yes',
        toolbarposition: 'top',
        zoom: 'yes',
        title: {
          //color: '#000000',
          // showPageTitle: true,
          // staticText: 'Это нужная ссылка',
        },
        enableViewportScale: 'yes',
  
  
      }
  
      const browser = this.iab.create('https://mindandheartuniversity.com/admin/appapi/webPage.php','_blank',options);
  
      browser.show()

  });
  }

  getQuiz() {
    let formData = new FormData();
    formData.append("aid", localStorage.getItem('user_id'));
    formData.append("videoid", this.videoid);
    this.quizService.getQuiz(formData).then((res: any) => {
      console.log("getQuiz : ", res);
      if (res.quizcount > 0) {
        this.take_quiz = false
      }
      this.quizid = res.quizid;
      console.log("this.quiztime : ", this.quiztime)
      if (res.downloadcount > 0) {
        this.pdf = false
        this.pdfFile = environment.downloadurl + res.downloadlink;
        console.log("this.pdfFile : ", this.pdfFile)
      }
    });
  }
  randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  // iframeOnLoad(obj) { //this call when iframe do loading
  //   let doc = obj.contentDocument || obj.contentWindow;
  //   doc.addEventListener("click", this.iframeClickHandler.bind(this, 
  //   this.menuCtrl), false);
  //  }
  maxTime: any = 30
  timer: any;
  hidevalue: any;
  StartTimer(maxTime) {
    console.log("maxTime : ", maxTime)
    this.timer = setTimeout(x => {
      if (maxTime <= 0) { }
      maxTime -= 1;

      if (maxTime > 0) {
        this.hidevalue = false;
        // this.UpdateTheTimeForPlay_video('play', this.interval)
        // this.SetSeekBar(this.videoStartTime)
        this.StartTimer(maxTime);
      }
      else {
        this.hidevalue = true;
      }

    }, 1000);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.videotitle = this.router.getCurrentNavigation().extras.state.videotitle;
        this.videourl = this.router.getCurrentNavigation().extras.state.videourl;
        this.videoid = this.router.getCurrentNavigation().extras.state.videoid;
        this.videoimage = this.router.getCurrentNavigation().extras.state.videoimage;
        console.log("videoimage : ", this.videoimage)
        this.videoimage = environment.imageUrl + this.videoimage
        console.log("videoimage after : ", this.videoimage)
        this.javaScript_Function(this.videourl)
        this.getQuiz();
        // this.loaderService.showLoader()
      }
    });
    get("assets/javaScript.js", (data) => {
      console.log(data)
    });
  }
  video: any;
  seekbar: any = 0;
  volumeRange: any;
  javaScript_Function(url) {
    this.video_link = url

    // get the video, volume and seekbar elements
    this.video = document.getElementById("video1");
    this.volumeRange = document.getElementById('volume');
    this.seekbar = document.getElementById('seekbar');

    console.log("video : ", this.video)
    console.log("volumeRange : ", this.volumeRange)
    console.log("seekbar : ", this.seekbar)
    // attach timeupdate, durationchange event to the video element
    if (this.video) {
      window.onload = function () {
        // go to http://www.w3.org/TR/DOM-Level-2-Events/events.html#Events-
        // EventTarget-addEventListener to know more about addEventListener
        // (false is for bubbling and true is for event capturing)
        this.video.addEventListener('timeupdate', this.UpdateTheTime(), false);
        this.video.addEventListener('durationchange', this.SetSeekBar(), false);
        // this.video.addEventListener('durationchange', this.SetSeekBar2(), false);
        this.volumeRange.value = this.video.volume;
      }
    }
  }


  onMetadata(e, video) {
    // this.video = video
    console.log("video onMetadata : ", video)
    console.log("video.duration : ", video.duration)

    // this.duration = video.duration;
    // //alert(this.duration);
    // this.lowerValue = 0;
    // this.upperValue = this.duration;
    // this.videoStartTime = 0;
    // this.videoEndTime = this.duration;
    video.removeAttribute('controls');
    video.setAttribute("webkit-playsinline", "true");
    video.setAttribute("playsinline", "true");
    // this.video = document.getElementById("video1");
    // window.onload = function () {
    //   // go to http://www.w3.org/TR/DOM-Level-2-Events/events.html#Events-
    //   // EventTarget-addEventListener to know more about addEventListener
    //   // (false is for bubbling and true is for event capturing)
    //   this.video.addEventListener('timeupdate', this.UpdateTheTime(), false);
    //   this.video.addEventListener('durationchange', this.SetSeekBar(), false);
    //   this.video.addEventListener('durationchange', this.SetSeekBar2(), false);
    //   this.volumeRange.value = this.video.volume;
    //   this.video.currentTime = this.videoStartTime;
    // }
    // this.storage.get("videoStartTime" + this.videoid).then((data) => {
    //   console.log("data : ", data)
    //   if (data) {
    //     this.videoStartTime = data
    //   } else {
    //     this.videoStartTime = 0
    //   }
    //   console.log("this.videoStartTime : ", this.videoStartTime)
    // })
    // this.videoStartTime = localStorage.getItem("videoStartTime"+this.videoid)
    // this.seekbar.min = this.videoStartTime;
    this.setEndTime()
    // this.setCurrentTimeOnLoad(this.videoStartTime)
    // this.SetSeekBar(this.videoStartTime)
    this.seekbar.value = 0;
    setTimeout(() => {
      this.loaderService.hideLoader();
    }, 500)
    //webkit-playsinline="true" playsinline="true"
    // console.log(this.upperValue);
    // const dualRange = document.querySelector('#dual-range') as HTMLIonRangeElement;
    // dualRange.value = { lower: this.lowerValue, upper: this.upperValue };
  }
  hideShow() {
    this.hide_show_icon = !this.hide_show_icon;
  }
  // fires when volume element is changed
  ChangeVolume() {
    var myVol = this.volumeRange.value;
    this.video.volume = myVol;
    if (myVol == 0) {
      this.video.muted = true;
    } else {
      this.video.muted = false;
    }
    console.log("change valume")
  }

  // fires when page loads, it sets the min and max range of the this.video
  SetSeekBar() {
    this.seekbar.min = 0;
    this.seekbar.max = this.video.duration;
  }
  SetSeekBar2() {
    this.seekbar.min = 0;
    this.seekbar.max = this.video.duration;
  }
  // fires when this.seekbar is changed
  ChangeTheTime(event) {
    console.log("event : ", event)
    /// Refresh the UI
    this.zone.run(() => {
      console.log('UI has refreshed');
      // this.seekbar.value = this.video.currentTime;
      this.video.currentTime = this.seekbar.value;
    });
    console.log("this.video.currentTime : ", this.video.currentTime)
    // this.video.play();
  }
  min_: any
  setCurrentTime(value, interval) {
    console.log("this.endTime : ", this.endTime)
    if (this.video.currentTime >= this.endTime) {
      clearInterval(interval);
      this.btnPause = false
      this.btnPlay = true
      this.videoLog(2)
      // console.log("UpdateTheTimeForPlay_video video end : ")

      // this.videoStartTime=0;
      // this.videoStartTime = 0;
      // this.javaScript_Function("https://s3.amazonaws.com/AST/9+min.mp4")
      // setTimeout(() => {
      //   this.PlayNow()
      // }, 1000)
    } else {
      console.log("this.videoStartTime setCurrentTime : ", this.videoStartTime)
      var sec = this.videoStartTime;
      var h = Math.floor(sec / 3600);
      sec = sec % 3600;
      var min = Math.floor(sec / 60);
      this.min_ = min;
      sec = Math.floor(sec % 60);
      if (sec.toString().length < 2) sec = "0" + sec;
      if (this.min_.toString().length < 2) this.min_ = "0" + this.min_;
      if (h == 0) {
        document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
      } else {
        document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
      }
      this.seekbar.min = this.video.startTime;
      this.seekbar.max = this.video.duration;
      this.seekbar.value = this.video.currentTime;
      this.videoStartTime = this.video.currentTime;
      // // document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
      // // console.log("this.video.startTime : ", this.video.startTime)
      // console.log("this.video.currentTime : ", this.video.currentTime)


      // console.log("this.videoStartTime afdqasd: ", this.videoStartTime)

      // console.log("this.seekbar.min : ", this.seekbar.min)
      // console.log("this.seekbar.max : ", this.seekbar.max)
      // console.log("this.seekbar.value : ", this.seekbar.value)
      // // this.ChangeTheTime()
    }

  }
  // setCurrentTime(data, video) {
  //   if (this.video.currentTime >= this.endTime) {
  //     this.btnPause = false
  //     this.btnPlay = true
  //     this.videoLog(2)
  //   } else {
  //     // console.log("data : ", data)
  //     // if (data.target.currentTime < 0.511) {
  //     //   data.target.currentTime = this.videoStartTime;
  //     //   video.play()
  //     // }
  //     console.log("this.videoStartTime setCurrentTime : ", this.videoStartTime)
  //     var sec = this.videoStartTime;
  //     var h = Math.floor(sec / 3600);
  //     sec = sec % 3600;
  //     var min = Math.floor(sec / 60);
  //     this.min_ = min;
  //     sec = Math.floor(sec % 60);
  //     if (sec.toString().length < 2) sec = "0" + sec;
  //     if (this.min_.toString().length < 2) this.min_ = "0" + this.min_;
  //     if (h == 0) {
  //       document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
  //     } else {
  //       document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
  //     }
  //     // data.target.currentTime = this.videoStartTime;
  //     // video.play()
  //     this.video.seekbar.min = this.video.startTime;
  //     this.video.seekbar.max = this.video.duration;
  //     this.video.seekbar.value = this.video.currentTime;
  //     this.video.videoStartTime = this.video.currentTime;
  //     // }

  //   }
  // }
  UpdateTheTimeForAds(value, interval) {
    console.log("this.endTime : ", this.endTime)
    if (this.video.currentTime >= this.endTime) {
      clearInterval(interval);
      this.btnPause = false
      this.btnPlay = true
      this.javaScript_Function(this.videourl)
      setTimeout(() => {
        this.video.currentTime = this.videoStartTime;
        this.adsDisabled = false
        // this.PlayNow()
      }, 1000)
    } else {
      var sec = this.video.currentTime;
      var h = Math.floor(sec / 3600);
      sec = sec % 3600;
      var min = Math.floor(sec / 60);
      this.min_ = min;
      sec = Math.floor(sec % 60);
      if (sec.toString().length < 2) sec = "0" + sec;
      if (this.min_.toString().length < 2) this.min_ = "0" + this.min_;
      if (h == 0) {
        document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
      } else {
        document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
      }
      // document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
      this.seekbar.min = this.video.startTime;
      this.seekbar.max = this.video.duration;
      this.seekbar.value = this.video.currentTime;
      // this.adsVideoStartTime = this.video.currentTime;
      console.log("this.seekbar.min : ", this.seekbar.min)
      console.log("this.seekbar.max : ", this.seekbar.max)
      console.log("this.seekbar.value : ", this.seekbar.value)
      // this.ChangeTheTime()
    }
  }
  setCurrentTimeOnLoad(videoStartTime) {
    console.log("videoStartTime setCurrentTimeOnLoad : ", videoStartTime)
    var sec = videoStartTime;
    var h = Math.floor(sec / 3600);
    sec = sec % 3600;
    var min = Math.floor(sec / 60);
    this.min_ = min;
    sec = Math.floor(sec % 60);
    if (sec.toString().length < 2) sec = "0" + sec;
    if (this.min_.toString().length < 2) this.min_ = "0" + this.min_;
    if (h == 0) {
      document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
    } else {
      document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
    }
  }
  setEndTime() {
    var sec = this.video.duration;
    this.endTime = this.video.duration;
    console.log("setEndTime : ", sec)
    var h = Math.floor(sec / 3600);
    sec = sec % 3600;
    var min = Math.floor(sec / 60);
    this.min_ = min;
    sec = Math.floor(sec % 60);
    if (sec.toString().length < 2) sec = "0" + sec;
    if (this.min_.toString().length < 2) this.min_ = "0" + this.min_;
    if (h == 0) {
      document.getElementById('lblTime2').innerHTML = this.min_ + ":" + sec;
    } else {
      document.getElementById('lblTime2').innerHTML = h + ":" + this.min_ + ":" + sec;
    }
  }
  interval: any;
  interval2: any;
  async PlayNow() {
    // Play Video from Application Directory or Stream Online
    // HKVideoPlayer.play(this.videourl);

    this.openUrl()

    // const modal = await this.modalController.create({
    //   component: IframePage,
    //   componentProps: {
    //     videoLink: this.videourl,
    //   }
    // })
    // modal.onDidDismiss().then((data) => {
    //   console.log(data);
    // })
    // await modal.present();
    this.btnPlay = true


    // // let mainVideo = <HTMLMediaElement>document.getElementById('video1');
    // // console.log("mainVideo : ",mainVideo)
    // // this.videoPaused = true;
    // // this.setCurrentTimeOnLoad(this.videoStartTime)
    // video1.currentTime = this.videoStartTime;
    // video1.play()

    // // this.video.currentTime = this.videoStartTime;
    // // this.video.play();
    // // this.video.currentTime = this.videoStartTime;
    // this.btnPause = true
    // this.btnPlay = false
    // // this.video.load()
    // // setTimeout(() => {
    // console.log("this.this.setCurrentTimeOnLoad(this.videoStartTime) : ", this.videoStartTime)

    // // this.setEndTime()
    // // this.video.play();
    // // }, 500)
    setTimeout(() => {
      this.videoLog(1)
    }, 1000)
    // setTimeout(() => {
    //   this.hideShow()
    // }, 2000)
    // // setTimeout(() => {
    // //   console.log("this.endTime : ", this.endTime)
    // //   // this.StartTimer(this.endTime)
    // // }, 1000)
    // // setTimeout(() => {
    // //   // this.adsDisabled = true
    // //   /////// /////////////////////////// if this function uncomment then the ads code working ////      
    // //   // this.playAds()
    // // }, 10000)
    // // this.StartTimer(this.endTime)
    // this.interval = setInterval(() => {
    //   console.log("this.video.currentTime hjdshfaj : ", this.video.currentTime)
    //   this.setCurrentTime('play', this.interval)
    //   this.SetSeekBar()
    //   // this.playAds()
    // }, 1000)

  }
  videoEnded() {
    console.log("videoEnded : ",)
  }
  playAds() {
    clearInterval(this.interval);
    this.javaScript_Function("http://techslides.com/demos/sample-videos/small.mp4")
    setTimeout(() => {
      // this.video.currentTime = this.adsVideoStartTime;
      this.video.play();
      this.endTime = this.video.duration;
      this.setEndTime()
      this.interval2 = setInterval(() => {
        this.UpdateTheTimeForAds('ads', this.interval2)
        this.SetSeekBar2()
      }, 1000)
      console.log("video  : ", this.video.end)
    }, 1000)

  }
  PauseNow() {
    this.videoPaused = false;
    // let mainVideo = <HTMLMediaElement>document.getElementById('video1');
    // this.video.currentTime = 400;
    this.video.pause();
    this.btnPause = false;
    this.btnPlay = true;
    // this.btnPause = false
    // this.btnPlay = true
    // console.log("pause video : btn click")
    // if (this.video.play) {
    //   this.video.pause();
    this.videoLog(0)
    // }
    // // localStorage.setItem("videoStartTime",this.videoStartTime)
    // console.log("pause")
    clearInterval(this.interval);

  }
  videoLog(state) {
    let formData = new FormData();
    formData.append("aid", localStorage.getItem('user_id'));
    formData.append("videoid", this.videoid);
    formData.append("state", state);
    this.videoLogService.videoLog(formData).then((res: any) => {
      console.log(res);
    });
  }
  unMute() {
    this.btnunMute = false
    this.btnMute = true
  }
  Mute() {
    this.btnunMute = true
    this.btnMute = false
  }
  openPDF(url) {
    console.log("url : ", url)
    this.pdfOpenAlert2('Open', 'Download', 'You can open and download the PDF.', url)
  }
  downloadPdf(url) {
    const fileTransfer: FileTransferObject = this.transfer.create();
    let targetPath;
    if (this.platform.is("ios")) {
      targetPath = this.file.documentsDirectory;
    } else {
      targetPath = this.file.dataDirectory;
    }
    fileTransfer.download(url, targetPath + '/my_downloads/' + this.videotitle + '.pdf').then((entry) => {
      console.log('file download response', entry);
      if (entry) {
        // this.toastService.showToast("File download successfully.")
        this.pdfOpenAlert('No', 'Yes', 'PDF downloaded successfully. Do you want to open it?', entry)
      }
    }).catch((err) => {
      console.log('error in file download', err);
    });
  }
  openPdf(entry) {
    const options: DocumentViewerOptions = {
      title: entry.name
    }
    this.documentViewer.viewDocument(entry.nativeURL, 'application/pdf', options)
  }
  async pdfOpenAlert(btn1, btn2, message, entry) {
    const alert = await this.alertController.create({
      // header: 'PDF',
      message: message,
      buttons: [
        {
          text: btn1,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            // console.log('Confirm Cancel');
          }
        }, {
          text: btn2,
          handler: async (alertData) => {
            this.openPdf(entry)
          }
        }
      ]
    });

    await alert.present();
  }
  async pdfOpenAlert2(btn1, btn2, message, url) {
    const alert = await this.alertController.create({
      // header: 'PDF',
      message: message,
      buttons: [
        {
          text: btn1,
          cssClass: 'secondary',
          handler: () => {
            let navigationExtras: NavigationExtras = {
              state: {
                name: this.videotitle,
                url: url,
              }
            };
            // this.router.navigate(['post-add-second', navigationExtras]);
            this.router.navigate(['pdf-viewer'], navigationExtras);
          }
        }, {
          text: btn2,
          handler: async (alertData) => {
            this.downloadPdf(url)
          }
        }
      ]
    });

    await alert.present();
  }
  quizdetails() {
    let formData = new FormData();
    formData.append("aid", localStorage.getItem('user_id'));
    formData.append("quizid", this.quizid);
    this.quizService.quizdetails(formData).then((data: any) => {
      if (data.status) {
        this.data = data;
        this.showAlert(this.data)
      } else {
        this.alertService.presentAlertError(data.message);
      }
    })
  }

  startQuiz() {
    // console.log("this.quizid : ",this.quizid)
    // let navigationExtras: NavigationExtras = {
    //   state: {
    //     quizids: this.quizid
    //   }
    // };
    // // this.router.navigate(['post-add-second', navigationExtras]);
    // // this.router.navigate(['tabs/tab1/sub-categories/video/quiz', navigationExtras]);
    // // this.alertService.showAlertForQuiz(quiz)

    // this.navCntl.navigateForward('tabs/tab1/sub-categories/video/quiz', navigationExtras)
    // // this.navCntl.
    this.quizdetails();

  }
  async showAlert(quiz) {
    // let firstTime = false;
    // let attemptsRemaining = 0;
    // if (quiz.is_attempt == 0) {
    //   firstTime = true;
    // }
    // console.log(quiz)
    // if (quiz.redo && (quiz.is_attempt < quiz.redo_attempt + 1)) {
    //   attemptsRemaining = (quiz.redo_attempt + 1) - quiz.is_attempt;
    // }

    const buttons = [];
    buttons.push(
      {
        text: 'Dismiss',
        role: 'cancel'
      }
    );

    // if (firstTime || attemptsRemaining > 0) {
    buttons.push(
      {
        text: 'Start quiz',
        handler: () => {
          let navigationExtras: NavigationExtras = {
            state: {
              quizids: this.quizid,
              quiztimes: quiz.quiztime
            }
          };
          this.navCntl.navigateForward('tabs/tab1/sub-categories/video/quiz', navigationExtras)
        }
      }
    );
    // }

    // if (firstTime && attemptsRemaining == 0) {
    //   attemptsRemaining = 1;
    // }

    const alert = await this.alertController.create({
      header: quiz.title,
      message: `
        <p>${quiz.description}</p>
        <p class="font-weight-600">Details</p>
        <ul class="text-align-left">
          <li>Time: ${this.transform(quiz.quiztime)}</li>
          <li>Total points: ${quiz.totalscore}</li>
          <li>Passing points: ${quiz.passcore}</li>
        </ul>
      `,
      buttons: buttons
    });
    // <li>Attempt remaining: ${attemptsRemaining}</li>
    alert.present();
  }
  transform(time: number): string {

    if (time >= 60) {
      const timeInMinutes: number = Math.floor(time / 60);
      const hours: number = Math.floor(time / 3600);
      let minutes = timeInMinutes;
      if (minutes >= 60) {
        minutes = timeInMinutes % 60;
      }
      return (hours > 0 ? '0' + hours + 'hr ' : '')
        + (minutes < 10 ? '0' + minutes : minutes) +
        'mins ' + ((time - timeInMinutes * 60) < 10 ? '0' +
          (time - timeInMinutes * 60) : (time - timeInMinutes * 60)) + 'secs';
    } else {
      return (time < 10 ? '00: 0' + time : '00:' + time) + 'secs';
    }
  }
  ionViewWillLeave() {
    console.log("ionViewWillLeave : ")
    // this.storage.set("videoStartTime" + this.videoid, this.videoStartTime)
    // clearInterval(this.interval);
  }

}
