"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.VideoPage = void 0;
var core_1 = require("@angular/core");
var scriptjs_1 = require("scriptjs");
var environment_1 = require("src/environments/environment");
var VideoPage = /** @class */ (function () {
    function VideoPage(zone, router, route, videoLogService, quizService, file, platform, transfer, alertController, documentViewer, alertService, loaderService, storage, modalController, navCntl, iab) {
        this.zone = zone;
        this.router = router;
        this.route = route;
        this.videoLogService = videoLogService;
        this.quizService = quizService;
        this.file = file;
        this.platform = platform;
        this.transfer = transfer;
        this.alertController = alertController;
        this.documentViewer = documentViewer;
        this.alertService = alertService;
        this.loaderService = loaderService;
        this.storage = storage;
        this.modalController = modalController;
        this.navCntl = navCntl;
        this.iab = iab;
        this.videoStartTime = 0;
        this.adsVideoStartTime = 0;
        this.hide_show_icon = true;
        this.adsDisabled = false;
        this.today = Date.now();
        this.myValue = 0;
        this.take_quiz = true;
        this.pdf = true;
        this.videoEndTime = 0;
        this.videoPaused = true;
        this.showTextarea = false;
        // iframeOnLoad(obj) { //this call when iframe do loading
        //   let doc = obj.contentDocument || obj.contentWindow;
        //   doc.addEventListener("click", this.iframeClickHandler.bind(this, 
        //   this.menuCtrl), false);
        //  }
        this.maxTime = 30;
        this.seekbar = 0;
        this.videoStartTime = 0;
        this.adsVideoStartTime = 0;
        this.btnPlay = true;
        this.btnunMute = true;
        // this.video_link = "https://s3.amazonaws.com/AST/9+min.mp4"
        // this.video_link = "http://static.videogular.com/assets/videos/elephants-dream.mp4"
        // this.video_link = "https://www.youtube.com/embed/r98doIWKudg"
        // this.video_link = "https://www.youtube.com/watch?v=01MJuw-xgQo"
        var listaFrames = document.getElementsByTagName("iframe");
        console.log("listaFrames : ");
        // this.StartTimer();
        //   let timeout = this.randomIntFromInterval(10, 10000)
        //   console.log("randomIntFromInterval : ", timeout)
        //   setTimeout(() => {
        //     console.log("setTimeout : ")
        //   }, timeout);
        this.loaderService.showLoader();
    }
    VideoPage.prototype.openUrl = function () {
        var _this = this;
        this.platform.ready().then(function () {
            var options = {
                location: 'no',
                //clearcache : 'no',
                //clearsessioncache : 'yes',
                //toolbar : 'yes', //iOS only
                toolbar: 'yes',
                closebuttoncaption: "X",
                closebuttoncolor: '#000000',
                toolbarcolor: '#f8f8f8',
                hideurlbar: 'yes',
                navigationbuttoncolor: '#ffffff',
                hidenavigationbuttons: 'yes',
                toolbarposition: 'top',
                zoom: 'yes',
                title: {
                //color: '#000000',
                // showPageTitle: true,
                // staticText: 'Это нужная ссылка',
                },
                enableViewportScale: 'yes'
            };
            var browser = _this.iab.create('https://mindandheartuniversity.com/admin/appapi/webPage.php', '_blank', options);
            browser.show();
        });
    };
    VideoPage.prototype.getQuiz = function () {
        var _this = this;
        var formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("videoid", this.videoid);
        this.quizService.getQuiz(formData).then(function (res) {
            console.log("getQuiz : ", res);
            if (res.quizcount > 0) {
                _this.take_quiz = false;
            }
            _this.quizid = res.quizid;
            console.log("this.quiztime : ", _this.quiztime);
            if (res.downloadcount > 0) {
                _this.pdf = false;
                _this.pdfFile = environment_1.environment.downloadurl + res.downloadlink;
                console.log("this.pdfFile : ", _this.pdfFile);
            }
        });
    };
    VideoPage.prototype.randomIntFromInterval = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    VideoPage.prototype.StartTimer = function (maxTime) {
        var _this = this;
        console.log("maxTime : ", maxTime);
        this.timer = setTimeout(function (x) {
            if (maxTime <= 0) { }
            maxTime -= 1;
            if (maxTime > 0) {
                _this.hidevalue = false;
                // this.UpdateTheTimeForPlay_video('play', this.interval)
                // this.SetSeekBar(this.videoStartTime)
                _this.StartTimer(maxTime);
            }
            else {
                _this.hidevalue = true;
            }
        }, 1000);
    };
    VideoPage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
                _this.videotitle = _this.router.getCurrentNavigation().extras.state.videotitle;
                _this.videourl = _this.router.getCurrentNavigation().extras.state.videourl;
                _this.videoid = _this.router.getCurrentNavigation().extras.state.videoid;
                _this.videoimage = _this.router.getCurrentNavigation().extras.state.videoimage;
                console.log("videoimage : ", _this.videoimage);
                _this.videoimage = environment_1.environment.imageUrl + _this.videoimage;
                console.log("videoimage after : ", _this.videoimage);
                _this.javaScript_Function(_this.videourl);
                _this.getQuiz();
                // this.loaderService.showLoader()
            }
        });
        scriptjs_1.get("assets/javaScript.js", function (data) {
            console.log(data);
        });
    };
    VideoPage.prototype.javaScript_Function = function (url) {
        this.video_link = url;
        // get the video, volume and seekbar elements
        this.video = document.getElementById("video1");
        this.volumeRange = document.getElementById('volume');
        this.seekbar = document.getElementById('seekbar');
        console.log("video : ", this.video);
        console.log("volumeRange : ", this.volumeRange);
        console.log("seekbar : ", this.seekbar);
        // attach timeupdate, durationchange event to the video element
        if (this.video) {
            window.onload = function () {
                // go to http://www.w3.org/TR/DOM-Level-2-Events/events.html#Events-
                // EventTarget-addEventListener to know more about addEventListener
                // (false is for bubbling and true is for event capturing)
                this.video.addEventListener('timeupdate', this.UpdateTheTime(), false);
                this.video.addEventListener('durationchange', this.SetSeekBar(), false);
                // this.video.addEventListener('durationchange', this.SetSeekBar2(), false);
                this.volumeRange.value = this.video.volume;
            };
        }
    };
    VideoPage.prototype.onMetadata = function (e, video) {
        var _this = this;
        // this.video = video
        console.log("video onMetadata : ", video);
        console.log("video.duration : ", video.duration);
        // this.duration = video.duration;
        // //alert(this.duration);
        // this.lowerValue = 0;
        // this.upperValue = this.duration;
        // this.videoStartTime = 0;
        // this.videoEndTime = this.duration;
        video.removeAttribute('controls');
        video.setAttribute("webkit-playsinline", "true");
        video.setAttribute("playsinline", "true");
        // this.video = document.getElementById("video1");
        // window.onload = function () {
        //   // go to http://www.w3.org/TR/DOM-Level-2-Events/events.html#Events-
        //   // EventTarget-addEventListener to know more about addEventListener
        //   // (false is for bubbling and true is for event capturing)
        //   this.video.addEventListener('timeupdate', this.UpdateTheTime(), false);
        //   this.video.addEventListener('durationchange', this.SetSeekBar(), false);
        //   this.video.addEventListener('durationchange', this.SetSeekBar2(), false);
        //   this.volumeRange.value = this.video.volume;
        //   this.video.currentTime = this.videoStartTime;
        // }
        // this.storage.get("videoStartTime" + this.videoid).then((data) => {
        //   console.log("data : ", data)
        //   if (data) {
        //     this.videoStartTime = data
        //   } else {
        //     this.videoStartTime = 0
        //   }
        //   console.log("this.videoStartTime : ", this.videoStartTime)
        // })
        // this.videoStartTime = localStorage.getItem("videoStartTime"+this.videoid)
        // this.seekbar.min = this.videoStartTime;
        this.setEndTime();
        // this.setCurrentTimeOnLoad(this.videoStartTime)
        // this.SetSeekBar(this.videoStartTime)
        this.seekbar.value = 0;
        setTimeout(function () {
            _this.loaderService.hideLoader();
        }, 500);
        //webkit-playsinline="true" playsinline="true"
        // console.log(this.upperValue);
        // const dualRange = document.querySelector('#dual-range') as HTMLIonRangeElement;
        // dualRange.value = { lower: this.lowerValue, upper: this.upperValue };
    };
    VideoPage.prototype.hideShow = function () {
        this.hide_show_icon = !this.hide_show_icon;
    };
    // fires when volume element is changed
    VideoPage.prototype.ChangeVolume = function () {
        var myVol = this.volumeRange.value;
        this.video.volume = myVol;
        if (myVol == 0) {
            this.video.muted = true;
        }
        else {
            this.video.muted = false;
        }
        console.log("change valume");
    };
    // fires when page loads, it sets the min and max range of the this.video
    VideoPage.prototype.SetSeekBar = function () {
        this.seekbar.min = 0;
        this.seekbar.max = this.video.duration;
    };
    VideoPage.prototype.SetSeekBar2 = function () {
        this.seekbar.min = 0;
        this.seekbar.max = this.video.duration;
    };
    // fires when this.seekbar is changed
    VideoPage.prototype.ChangeTheTime = function (event) {
        var _this = this;
        console.log("event : ", event);
        /// Refresh the UI
        this.zone.run(function () {
            console.log('UI has refreshed');
            // this.seekbar.value = this.video.currentTime;
            _this.video.currentTime = _this.seekbar.value;
        });
        console.log("this.video.currentTime : ", this.video.currentTime);
        // this.video.play();
    };
    VideoPage.prototype.setCurrentTime = function (value, interval) {
        console.log("this.endTime : ", this.endTime);
        if (this.video.currentTime >= this.endTime) {
            clearInterval(interval);
            this.btnPause = false;
            this.btnPlay = true;
            this.videoLog(2);
            // console.log("UpdateTheTimeForPlay_video video end : ")
            // this.videoStartTime=0;
            // this.videoStartTime = 0;
            // this.javaScript_Function("https://s3.amazonaws.com/AST/9+min.mp4")
            // setTimeout(() => {
            //   this.PlayNow()
            // }, 1000)
        }
        else {
            console.log("this.videoStartTime setCurrentTime : ", this.videoStartTime);
            var sec = this.videoStartTime;
            var h = Math.floor(sec / 3600);
            sec = sec % 3600;
            var min = Math.floor(sec / 60);
            this.min_ = min;
            sec = Math.floor(sec % 60);
            if (sec.toString().length < 2)
                sec = "0" + sec;
            if (this.min_.toString().length < 2)
                this.min_ = "0" + this.min_;
            if (h == 0) {
                document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
            }
            else {
                document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
            }
            this.seekbar.min = this.video.startTime;
            this.seekbar.max = this.video.duration;
            this.seekbar.value = this.video.currentTime;
            this.videoStartTime = this.video.currentTime;
            // // document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
            // // console.log("this.video.startTime : ", this.video.startTime)
            // console.log("this.video.currentTime : ", this.video.currentTime)
            // console.log("this.videoStartTime afdqasd: ", this.videoStartTime)
            // console.log("this.seekbar.min : ", this.seekbar.min)
            // console.log("this.seekbar.max : ", this.seekbar.max)
            // console.log("this.seekbar.value : ", this.seekbar.value)
            // // this.ChangeTheTime()
        }
    };
    // setCurrentTime(data, video) {
    //   if (this.video.currentTime >= this.endTime) {
    //     this.btnPause = false
    //     this.btnPlay = true
    //     this.videoLog(2)
    //   } else {
    //     // console.log("data : ", data)
    //     // if (data.target.currentTime < 0.511) {
    //     //   data.target.currentTime = this.videoStartTime;
    //     //   video.play()
    //     // }
    //     console.log("this.videoStartTime setCurrentTime : ", this.videoStartTime)
    //     var sec = this.videoStartTime;
    //     var h = Math.floor(sec / 3600);
    //     sec = sec % 3600;
    //     var min = Math.floor(sec / 60);
    //     this.min_ = min;
    //     sec = Math.floor(sec % 60);
    //     if (sec.toString().length < 2) sec = "0" + sec;
    //     if (this.min_.toString().length < 2) this.min_ = "0" + this.min_;
    //     if (h == 0) {
    //       document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
    //     } else {
    //       document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
    //     }
    //     // data.target.currentTime = this.videoStartTime;
    //     // video.play()
    //     this.video.seekbar.min = this.video.startTime;
    //     this.video.seekbar.max = this.video.duration;
    //     this.video.seekbar.value = this.video.currentTime;
    //     this.video.videoStartTime = this.video.currentTime;
    //     // }
    //   }
    // }
    VideoPage.prototype.UpdateTheTimeForAds = function (value, interval) {
        var _this = this;
        console.log("this.endTime : ", this.endTime);
        if (this.video.currentTime >= this.endTime) {
            clearInterval(interval);
            this.btnPause = false;
            this.btnPlay = true;
            this.javaScript_Function(this.videourl);
            setTimeout(function () {
                _this.video.currentTime = _this.videoStartTime;
                _this.adsDisabled = false;
                // this.PlayNow()
            }, 1000);
        }
        else {
            var sec = this.video.currentTime;
            var h = Math.floor(sec / 3600);
            sec = sec % 3600;
            var min = Math.floor(sec / 60);
            this.min_ = min;
            sec = Math.floor(sec % 60);
            if (sec.toString().length < 2)
                sec = "0" + sec;
            if (this.min_.toString().length < 2)
                this.min_ = "0" + this.min_;
            if (h == 0) {
                document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
            }
            else {
                document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
            }
            // document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
            this.seekbar.min = this.video.startTime;
            this.seekbar.max = this.video.duration;
            this.seekbar.value = this.video.currentTime;
            // this.adsVideoStartTime = this.video.currentTime;
            console.log("this.seekbar.min : ", this.seekbar.min);
            console.log("this.seekbar.max : ", this.seekbar.max);
            console.log("this.seekbar.value : ", this.seekbar.value);
            // this.ChangeTheTime()
        }
    };
    VideoPage.prototype.setCurrentTimeOnLoad = function (videoStartTime) {
        console.log("videoStartTime setCurrentTimeOnLoad : ", videoStartTime);
        var sec = videoStartTime;
        var h = Math.floor(sec / 3600);
        sec = sec % 3600;
        var min = Math.floor(sec / 60);
        this.min_ = min;
        sec = Math.floor(sec % 60);
        if (sec.toString().length < 2)
            sec = "0" + sec;
        if (this.min_.toString().length < 2)
            this.min_ = "0" + this.min_;
        if (h == 0) {
            document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
        }
        else {
            document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
        }
    };
    VideoPage.prototype.setEndTime = function () {
        var sec = this.video.duration;
        this.endTime = this.video.duration;
        console.log("setEndTime : ", sec);
        var h = Math.floor(sec / 3600);
        sec = sec % 3600;
        var min = Math.floor(sec / 60);
        this.min_ = min;
        sec = Math.floor(sec % 60);
        if (sec.toString().length < 2)
            sec = "0" + sec;
        if (this.min_.toString().length < 2)
            this.min_ = "0" + this.min_;
        if (h == 0) {
            document.getElementById('lblTime2').innerHTML = this.min_ + ":" + sec;
        }
        else {
            document.getElementById('lblTime2').innerHTML = h + ":" + this.min_ + ":" + sec;
        }
    };
    VideoPage.prototype.PlayNow = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                // Play Video from Application Directory or Stream Online
                // HKVideoPlayer.play(this.videourl);
                this.openUrl();
                // const modal = await this.modalController.create({
                //   component: IframePage,
                //   componentProps: {
                //     videoLink: this.videourl,
                //   }
                // })
                // modal.onDidDismiss().then((data) => {
                //   console.log(data);
                // })
                // await modal.present();
                this.btnPlay = true;
                // // let mainVideo = <HTMLMediaElement>document.getElementById('video1');
                // // console.log("mainVideo : ",mainVideo)
                // // this.videoPaused = true;
                // // this.setCurrentTimeOnLoad(this.videoStartTime)
                // video1.currentTime = this.videoStartTime;
                // video1.play()
                // // this.video.currentTime = this.videoStartTime;
                // // this.video.play();
                // // this.video.currentTime = this.videoStartTime;
                // this.btnPause = true
                // this.btnPlay = false
                // // this.video.load()
                // // setTimeout(() => {
                // console.log("this.this.setCurrentTimeOnLoad(this.videoStartTime) : ", this.videoStartTime)
                // // this.setEndTime()
                // // this.video.play();
                // // }, 500)
                setTimeout(function () {
                    _this.videoLog(1);
                }, 1000);
                return [2 /*return*/];
            });
        });
    };
    VideoPage.prototype.videoEnded = function () {
        console.log("videoEnded : ");
    };
    VideoPage.prototype.playAds = function () {
        var _this = this;
        clearInterval(this.interval);
        this.javaScript_Function("http://techslides.com/demos/sample-videos/small.mp4");
        setTimeout(function () {
            // this.video.currentTime = this.adsVideoStartTime;
            _this.video.play();
            _this.endTime = _this.video.duration;
            _this.setEndTime();
            _this.interval2 = setInterval(function () {
                _this.UpdateTheTimeForAds('ads', _this.interval2);
                _this.SetSeekBar2();
            }, 1000);
            console.log("video  : ", _this.video.end);
        }, 1000);
    };
    VideoPage.prototype.PauseNow = function () {
        this.videoPaused = false;
        // let mainVideo = <HTMLMediaElement>document.getElementById('video1');
        // this.video.currentTime = 400;
        this.video.pause();
        this.btnPause = false;
        this.btnPlay = true;
        // this.btnPause = false
        // this.btnPlay = true
        // console.log("pause video : btn click")
        // if (this.video.play) {
        //   this.video.pause();
        this.videoLog(0);
        // }
        // // localStorage.setItem("videoStartTime",this.videoStartTime)
        // console.log("pause")
        clearInterval(this.interval);
    };
    VideoPage.prototype.videoLog = function (state) {
        var formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("videoid", this.videoid);
        formData.append("state", state);
        this.videoLogService.videoLog(formData).then(function (res) {
            console.log(res);
        });
    };
    VideoPage.prototype.unMute = function () {
        this.btnunMute = false;
        this.btnMute = true;
    };
    VideoPage.prototype.Mute = function () {
        this.btnunMute = true;
        this.btnMute = false;
    };
    VideoPage.prototype.openPDF = function (url) {
        console.log("url : ", url);
        this.pdfOpenAlert2('Open', 'Download', 'You can open and download the PDF.', url);
    };
    VideoPage.prototype.downloadPdf = function (url) {
        var _this = this;
        var fileTransfer = this.transfer.create();
        var targetPath;
        if (this.platform.is("ios")) {
            targetPath = this.file.documentsDirectory;
        }
        else {
            targetPath = this.file.dataDirectory;
        }
        fileTransfer.download(url, targetPath + '/my_downloads/' + this.videotitle + '.pdf').then(function (entry) {
            console.log('file download response', entry);
            if (entry) {
                // this.toastService.showToast("File download successfully.")
                _this.pdfOpenAlert('No', 'Yes', 'PDF downloaded successfully. Do you want to open it?', entry);
            }
        })["catch"](function (err) {
            console.log('error in file download', err);
        });
    };
    VideoPage.prototype.openPdf = function (entry) {
        var options = {
            title: entry.name
        };
        this.documentViewer.viewDocument(entry.nativeURL, 'application/pdf', options);
    };
    VideoPage.prototype.pdfOpenAlert = function (btn1, btn2, message, entry) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            // header: 'PDF',
                            message: message,
                            buttons: [
                                {
                                    text: btn1,
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                        // console.log('Confirm Cancel');
                                    }
                                }, {
                                    text: btn2,
                                    handler: function (alertData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            this.openPdf(entry);
                                            return [2 /*return*/];
                                        });
                                    }); }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    VideoPage.prototype.pdfOpenAlert2 = function (btn1, btn2, message, url) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            // header: 'PDF',
                            message: message,
                            buttons: [
                                {
                                    text: btn1,
                                    cssClass: 'secondary',
                                    handler: function () {
                                        var navigationExtras = {
                                            state: {
                                                name: _this.videotitle,
                                                url: url
                                            }
                                        };
                                        // this.router.navigate(['post-add-second', navigationExtras]);
                                        _this.router.navigate(['pdf-viewer'], navigationExtras);
                                    }
                                }, {
                                    text: btn2,
                                    handler: function (alertData) { return __awaiter(_this, void 0, void 0, function () {
                                        return __generator(this, function (_a) {
                                            this.downloadPdf(url);
                                            return [2 /*return*/];
                                        });
                                    }); }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    VideoPage.prototype.quizdetails = function () {
        var _this = this;
        var formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("quizid", this.quizid);
        this.quizService.quizdetails(formData).then(function (data) {
            if (data.status) {
                _this.data = data;
                _this.showAlert(_this.data);
            }
            else {
                _this.alertService.presentAlertError(data.message);
            }
        });
    };
    VideoPage.prototype.startQuiz = function () {
        // console.log("this.quizid : ",this.quizid)
        // let navigationExtras: NavigationExtras = {
        //   state: {
        //     quizids: this.quizid
        //   }
        // };
        // // this.router.navigate(['post-add-second', navigationExtras]);
        // // this.router.navigate(['tabs/tab1/sub-categories/video/quiz', navigationExtras]);
        // // this.alertService.showAlertForQuiz(quiz)
        // this.navCntl.navigateForward('tabs/tab1/sub-categories/video/quiz', navigationExtras)
        // // this.navCntl.
        this.quizdetails();
    };
    VideoPage.prototype.showAlert = function (quiz) {
        return __awaiter(this, void 0, void 0, function () {
            var buttons, alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        buttons = [];
                        buttons.push({
                            text: 'Dismiss',
                            role: 'cancel'
                        });
                        // if (firstTime || attemptsRemaining > 0) {
                        buttons.push({
                            text: 'Start quiz',
                            handler: function () {
                                var navigationExtras = {
                                    state: {
                                        quizids: _this.quizid,
                                        quiztimes: quiz.quiztime
                                    }
                                };
                                _this.navCntl.navigateForward('tabs/tab1/sub-categories/video/quiz', navigationExtras);
                            }
                        });
                        return [4 /*yield*/, this.alertController.create({
                                header: quiz.title,
                                message: "\n        <p>" + quiz.description + "</p>\n        <p class=\"font-weight-600\">Details</p>\n        <ul class=\"text-align-left\">\n          <li>Time: " + this.transform(quiz.quiztime) + "</li>\n          <li>Total points: " + quiz.totalscore + "</li>\n          <li>Passing points: " + quiz.passcore + "</li>\n        </ul>\n      ",
                                buttons: buttons
                            })];
                    case 1:
                        alert = _a.sent();
                        // <li>Attempt remaining: ${attemptsRemaining}</li>
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    VideoPage.prototype.transform = function (time) {
        if (time >= 60) {
            var timeInMinutes = Math.floor(time / 60);
            var hours = Math.floor(time / 3600);
            var minutes = timeInMinutes;
            if (minutes >= 60) {
                minutes = timeInMinutes % 60;
            }
            return (hours > 0 ? '0' + hours + 'hr ' : '')
                + (minutes < 10 ? '0' + minutes : minutes) +
                'mins ' + ((time - timeInMinutes * 60) < 10 ? '0' +
                (time - timeInMinutes * 60) : (time - timeInMinutes * 60)) + 'secs';
        }
        else {
            return (time < 10 ? '00: 0' + time : '00:' + time) + 'secs';
        }
    };
    VideoPage.prototype.ionViewWillLeave = function () {
        console.log("ionViewWillLeave : ");
        // this.storage.set("videoStartTime" + this.videoid, this.videoStartTime)
        // clearInterval(this.interval);
    };
    VideoPage = __decorate([
        core_1.Component({
            selector: 'app-video',
            templateUrl: './video.page.html',
            styleUrls: ['./video.page.scss']
        })
    ], VideoPage);
    return VideoPage;
}());
exports.VideoPage = VideoPage;
