import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-model',
  templateUrl: './model.page.html',
  styleUrls: ['./model.page.scss'],
})
export class ModelPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }
  modelDismiss(value){
    if(value == 'no'){
      this.modalController.dismiss(false);
    }else{
      this.modalController.dismiss(true);
    }
  }
  dismiss(){
    this.modalController.dismiss();
  }

}
