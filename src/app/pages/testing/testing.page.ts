import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { LoaderService } from 'src/app/services/loader.service';
import { get } from 'scriptjs';
@Component({
  selector: 'app-testing',
  templateUrl: './testing.page.html',
  styleUrls: ['./testing.page.scss'],
})
export class TestingPage implements OnInit {
  videoPaused: boolean = true;
  stringwithhtml: any;
  videotext: any;
  assigned: boolean = false;
  endAssigned: boolean;
  currentTimeText: 'N/A';
  currentTime: any;
  text: any;
  btnPlay: boolean = true;
  btnPause: boolean = false;
  videoUrl: any;
  duration: any;
  lowerValue: any;
  videoStartTime: number = 200;
  videoEndTime: number = 0;
  upperValue: any;
  public hide_show_icon: any = true;
  public videoimage: any;
  public take_quiz: any = true;
  public pdf: any = true;
  public pdfFile: any;
  public adsDisabled: any = false;

  public videotitle: any;
  public videourl: any;
  public videoid: any;


  seekbar: any = 0;
  min_: any
  sec_: any
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public storage: Storage,
    public loaderService: LoaderService,
  ) {
    // this.videoUrl = 'https://s3.amazonaws.com/AST/9+min.mp4'
  }
  // onMetadata(e, video) {
  //   this.duration = video.duration;
  //   //alert(this.duration);
  //   this.lowerValue = 0;
  //   this.upperValue = this.duration;
  //   // this.videoStartTime = 200;
  //   this.storage.get("videoStartTime" + this.videoid).then((data) => {
  //     console.log("data : ", data)
  //     if (data) {
  //       this.videoStartTime = data
  //     } else {
  //       this.videoStartTime = 0
  //     }
  //     console.log("this.videoStartTime : ", this.videoStartTime)
  //     // this.seekbar = document.getElementById('seekbar');
  //     // console.log("seekbar : ", this.seekbar)
  //     // video.addEventListener('durationchange', this.SetSeekBar(this.videoStartTime), false);
  //   })
  //   this.videoEndTime = this.duration;
  //   video.removeAttribute('controls');
  //   video.setAttribute("webkit-playsinline", "true");
  //   video.setAttribute("playsinline", "true");

  //   setTimeout(() => {
  //     this.loaderService.hideLoader();
  //   }, 500)
  // }
  // // fires when page loads, it sets the min and max range of the this.video
  // SetSeekBar(videoStartTime) {
  //   this.seekbar.min = videoStartTime;
  //   let mainVideo = <HTMLMediaElement>document.getElementById('video');
  //   this.seekbar.max = mainVideo.duration;
  // }

  // num_chk:any = 0;
  // setCurrentTime(data) {
  //   this.stringwithhtml = '';
  //   this.videotext = '';
  //   if (this.videoPaused == false) {
  //     this.stringwithhtml = this.text;
  //   }
  //   console.log("setCurrentTime enter : ", data)
  //     if (this.videoPaused == true) {
  //       console.log("setCurrentTime enter this.videoPaused == true : ",)
  //       if (this.assigned == false) {
  //         console.log("setCurrentTime enter this.videoPaused == true assigned : ", this.videoStartTime)
  //         data.target.currentTime = this.videoStartTime;
  //         console.log("setCurrentTime enter this.videoPaused == true assigned : ", data.target.currentTime)
  //         this.assigned = true;
  //       }
  //       if (this.endAssigned == false) {
  //         data.target.currentTime = this.videoEndTime;
  //         this.endAssigned = true;
  //       }
  //       this.currentTime = data.target.currentTime;
  //       console.log("this.currentTime : ", this.currentTime)
  //       // if (this.currentTime > 0 && this.currentTime < this.videoStartTime) {
  //       //   this.pauseVideo();
  //       //   console.log("this.pauseVideo() 1 : ", this.pauseVideo())
  //       // } else if (this.currentTime >= this.videoEndTime) {
  //       //   this.pauseVideo();
  //       //   data.target.currentTime = this.videoStartTime;
  //       //   this.videoPaused = false;
  //       //   console.log("this.pauseVideo() 2 : ", this.pauseVideo())
  //       // } else {
  //       //   this.playVideo();
  //       //   console.log("this.playVideo() 1 : ", this.playVideo())
  //       //   if(this.num_chk == 0){
  //       //     // this.loaderService.hideLoader()
  //       //     this.num_chk = 1
  //       //   }
  //       // }
  //     }
  // }
  // playVideo() {
  //   let mainVideo = <HTMLMediaElement>document.getElementById('video');
  //   var promise = mainVideo.play();
  //   console.log("promise ",promise)
  //   if (promise !== undefined) {
  //     promise.then(function() {
  //       console.log("true")
  //     }).catch(function(error) {
  //         //If there was an error autoplaying the video, then display a button to play the video
  //         console.log("false")

  //     });
  // }
  //   this.btnPause = true;
  //   this.btnPlay = false;
  // }
  // pauseVideo() {
  //   let mainVideo = <HTMLMediaElement>document.getElementById('video');
  //   mainVideo.pause();
  //   this.btnPause = false;
  //   this.btnPlay = true;
  // }
  // btnPlayVideo() {
  //   let mainVideo = <HTMLMediaElement>document.getElementById('video');
  //   this.videoPaused = true;
  //   var promise = mainVideo.play();
  //   console.log("promise btnPlayVideo ",promise)
  //   if (promise !== undefined) {
  //     promise.then(function() {
  //       console.log("true btnPlayVideo")
  //     }).catch(function(error) {
  //         //If there was an error autoplaying the video, then display a button to play the video
  //         console.log("false btnPlayVideo")

  //     });
  // }
  //   this.btnPause = true;
  //   this.btnPlay = false;
  //   // this.loaderService.showLoader();
  //   // this.showTextarea = false;
  //   // this.posterValue = 'assets/poster.gif';
  // }
  // btnpauseVideo() {
  //   this.videoPaused = false;
  //   let mainVideo = <HTMLMediaElement>document.getElementById('video');
  //   mainVideo.pause();
  //   this.btnPause = false;
  //   this.btnPlay = true;
  //   // this.posterValue = 'assets/player.svg';
  // }
  // hideShow() {
  //   this.hide_show_icon = !this.hide_show_icon;
  // }
  ngOnInit() {
    // this.route.queryParams.subscribe(params => {
    //   if (this.router.getCurrentNavigation().extras.state) {
    //     this.videotitle = this.router.getCurrentNavigation().extras.state.videotitle;
    //     this.videoUrl = this.router.getCurrentNavigation().extras.state.videourl;
    //     this.videoid = this.router.getCurrentNavigation().extras.state.videoid;
    //     this.videoimage = this.router.getCurrentNavigation().extras.state.videoimage;
    //     console.log("videoimage : ", this.videoimage)
    //     this.videoimage = environment.imageUrl + this.videoimage
    //     this.loaderService.showLoader()
    //   }
    // });
    // get("assets/javaScript.js", (data) => {
    //   console.log(data)
    // });
  }
  // ionViewWillLeave() {
  //   console.log("ionViewWillLeave : ")
  //   console.log("ionViewWillLeave this.currentTime : ", this.currentTime)
  //   console.log("ionViewWillLeave this.videoStartTime : ", this.videoStartTime)

  //   this.storage.set("videoStartTime" + this.videoid, this.currentTime)
  // }
}
