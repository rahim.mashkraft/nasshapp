import { ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MenuController, NavController, Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { AlertService } from './services/alert.service';
import { EventsService } from './services/events.service';
import { Storage } from '@ionic/storage';
import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic/ngx";
// import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic";
import { INotificationPayload } from 'plugins/cordova-plugin-fcm-with-dependecy-updated/typings';

const w: any = window;

interface DeeplinkMatch {
  userId: string;
}
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public onlineOffline: boolean = navigator.onLine;
  userData: any = {
    aid: '',
    firstname: '',
    lastname: '',
    email: '',
    profileimage: '',
    address: '',
    city: '',
    country: '',
    phone: '',
    username: '',
    zipcode: '',
    state: '',
  }
  profileImage: any;
  environment: any;

  public hasPermission: boolean;
  public token: string;
  public pushPayload: INotificationPayload;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public navCtrl: NavController,
    private menu: MenuController,
    private alertService: AlertService,
    private eventsService: EventsService,
    private cdr: ChangeDetectorRef,
    private router: Router,
    public storage: Storage,
    public fcm: FCM,
  ) {
    this.setupFCM();
    this.environment = environment;
    this.initializeApp()
    window.addEventListener('offline', () => {
      this.alertService.presentNetworkAlert();
    });
  }
  private async setupFCM() {
    await this.platform.ready();
    console.log('FCM setup started');

    if (!this.platform.is('cordova')) {
      return;
    }
    console.log('In cordova platform');

    console.log('Subscribing to token updates');
    this.fcm.onTokenRefresh().subscribe((newToken) => {
      this.token = newToken;
      console.log('onTokenRefresh received event with: ', newToken);
    });

    console.log('Subscribing to new notifications');
    this.fcm.onNotification().subscribe((payload) => {
      this.pushPayload = payload;
      console.log('onNotification received event with: ', payload);
    });

    this.hasPermission = await this.fcm.requestPushPermission();
    console.log('requestPushPermission result: ', this.hasPermission);

    this.token = await this.fcm.getToken();
    console.log('getToken result: ', this.token);

    this.pushPayload = await this.fcm.getInitialPushPayload();
    console.log('getInitialPushPayload result: ', this.pushPayload);
  }

  public get pushPayloadString() {
    return JSON.stringify(this.pushPayload, null, 4);
  }
  async initializeApp() {
    if (!navigator.onLine) {
      this.alertService.presentNetworkAlert();
    }
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      setTimeout(() => {
        this.splashScreen.hide();
      }, 2000)
      // let thisObj = this;
      // thisObj.fcm.requestPushPermission().then(() => {
      //   console.log("requestPushPermission true: ")
      //   // alert("suecce ")
      // }).catch((e) => {
      //   console.log("requestPushPermission : ",e)
      //   // alert("errr "+e)
      // })
      this.eventsService.subscribe('userLogged', (data) => {
        console.log("userLogged : ", data)
        this.userData.firstname = data.firstname;
        this.userData.lastname = data.lastname;
        this.userData.email = data.email;
        this.userData.profileimage = data.profileimage;
        this.profileImage = environment.profileImageUrl + this.userData.profileimage
        console.log("this.userData.profileImage : ", this.userData.profileImage)
        console.log("this.profileImage 1 : ", this.profileImage)
        setTimeout(() => {
          this.cdr.detectChanges()
        }, 1000);
      });
      this.eventsService.subscribe('nameChanged', (data) => {
        this.userData.firstname = data.firstname;
        this.userData.lastname = data.lastname;
      });
      this.eventsService.subscribe('pictureChanged', (data) => {
        this.userData.profileimage = data.profileimage;
        this.profileImage = environment.profileImageUrl + this.userData.profileimage
        console.log("this.profileImage 2 : ", this.profileImage)
        setTimeout(() => {
          this.cdr.detectChanges()
        }, 1000);
      });
      setTimeout(() => {
        this.userData.firstname = localStorage.getItem('firstname');
        this.userData.lastname = localStorage.getItem('lastname');
        this.userData.email = localStorage.getItem('email');
        this.userData.profileimage = localStorage.getItem('profileimage');
        if (this.userData.profileimage) {
          this.profileImage = environment.profileImageUrl + this.userData.profileimage
          console.log("this.userData.profileImage : ", this.userData.profileImage)
          console.log("this.profileImage 3 : ", this.profileImage)
        }
        setTimeout(() => {
          this.cdr.detectChanges()
        }, 1000);
      }, 1000);
    });
    // this.importContactList();
    // this.checkPermission()
    const loginUser = localStorage.getItem('user_id');
    console.log("loginUser : ", loginUser)
    if (loginUser) {
      this.navCtrl.navigateRoot('/tabs');
    } else {
      this.navCtrl.navigateRoot('');
    }
    // if (loginUser) {
    // this.checkWelcomeScreen(loginUser);
    // }
    // if (!loginUser) {
    //   await this.initDeepLinking();
    // } else {
    //   this.checkWelcomeScreen(loginUser);
    // }
  }

  checkWelcomeScreen(loginUser = null) {
    const firstTimeAppOpened = localStorage.getItem('firstTime');
    if (firstTimeAppOpened) {
      this.navCtrl.navigateRoot('welcome-screen');
    } else {
      if (loginUser) {
        this.navCtrl.navigateRoot('/tabs');
      } else {
        this.navCtrl.navigateRoot('');
      }
    }
  }
  private async initDeepLinking(): Promise<void> {
    if (this.platform.is('cordova')) {
      await this.initDeepLinkingBranchio();
    } else {
      await this.initDeepLinkingWeb();
    }
  }

  private async initDeepLinkingWeb(): Promise<void> {
    const userId: string =
      this.platform.getQueryParam('$userId') ||
      this.platform.getQueryParam('userId') ||
      this.platform.getQueryParam('%24userId');
    this.navCtrl.navigateRoot('');
    console.log('Parameter', userId);
  }

  private async initDeepLinkingBranchio(): Promise<void> {
    try {
      const branchIo = window['Branch'];
      if (branchIo) {
        const data: DeeplinkMatch =
          await branchIo.initSession();
        console.log(data);
        if (data.userId !== undefined) {
          console.log('Parameter', data.userId);
          // this.userService.inviteId = data.userId;
          this.navCtrl.navigateRoot('register');
        } else {
          // this.checkWelcomeScreen();
          this.navCtrl.navigateRoot('/login');
        }
      }
    } catch (err) {
      console.error(err);
    }
  }
  importContact() {
    this.router.navigate(['tabs/import-contacts']);
    this.menu.close()
  }
  contactsTable() {
    this.router.navigate(['tabs/contact-table']);
    this.menu.close()
  }
  sendBroadcast() {
    this.router.navigate(['tabs/send-broadcast']);
    this.menu.close()
  }
  sendSmsBroadcast() {
    this.router.navigate(['tabs/send-smsbroadcast']);
    this.menu.close()
  }
  goToEditProfile() {
    this.router.navigate(['tabs/edit-profile']);
    this.menu.close()
  }
  member() {
    this.router.navigate(['tabs/member']);
    this.menu.close()
  }
  contactGroups() {
    this.router.navigate(['tabs/contact-groups']);
    this.menu.close()
  }
  video() {
    this.router.navigate(['testing']);
    this.menu.close()
  }
  successModel() {
    this.router.navigate(['success-model']);
    this.menu.close()
  }
  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
  ionViewWillEnter() {
    this.menu.enable(true);
  }
  ionViewDidLeave(): void {
    this.menu.enable(true);
  }
  async logout() {
    localStorage.clear();
    await this.menu.enable(true)
    this.navCtrl.navigateRoot('/');
    // this.fcm.getToken().then(token => {
    //   localStorage.setItem('device_token', token);
    //   console.log('This is to test ');
    //   console.log(token);
    // });
    // this.initializeApp();
  }
}
// ionic cordova plugin add cordova-plugin-facebook-connect --variable APP_ID="1636116319932991" --variable APP_NAME="Nash Consulting"