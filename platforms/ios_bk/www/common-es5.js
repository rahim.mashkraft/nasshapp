(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

  function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"], {
    /***/
    "4B9S":
    /*!********************************************************!*\
      !*** ./src/app/pages/_helpers/must-match.validator.ts ***!
      \********************************************************/

    /*! exports provided: mustMatch */

    /***/
    function B9S(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "mustMatch", function () {
        return mustMatch;
      }); // custom validator to check that two fields match


      function mustMatch(controlName, matchingControlName) {
        return function (formGroup) {
          var control = formGroup.controls[controlName];
          var matchingControl = formGroup.controls[matchingControlName];

          if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
          } // set error on matchingControl if validation fails


          if (control.value !== matchingControl.value) {
            matchingControl.setErrors({
              mustMatch: true
            });
          } else {
            matchingControl.setErrors(null);
          }
        };
      }
      /***/

    },

    /***/
    "74mu":
    /*!*************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm/theme-ff3fc52f.js ***!
      \*************************************************************/

    /*! exports provided: c, g, h, o */

    /***/
    function mu(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "c", function () {
        return createColorClasses;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "g", function () {
        return getClassMap;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "h", function () {
        return hostContext;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "o", function () {
        return openURL;
      });

      var hostContext = function hostContext(selector, el) {
        return el.closest(selector) !== null;
      };
      /**
       * Create the mode and color classes for the component based on the classes passed in
       */


      var createColorClasses = function createColorClasses(color, cssClassMap) {
        return typeof color === 'string' && color.length > 0 ? Object.assign(_defineProperty({
          'ion-color': true
        }, "ion-color-".concat(color), true), cssClassMap) : cssClassMap;
      };

      var getClassList = function getClassList(classes) {
        if (classes !== undefined) {
          var array = Array.isArray(classes) ? classes : classes.split(' ');
          return array.filter(function (c) {
            return c != null;
          }).map(function (c) {
            return c.trim();
          }).filter(function (c) {
            return c !== '';
          });
        }

        return [];
      };

      var getClassMap = function getClassMap(classes) {
        var map = {};
        getClassList(classes).forEach(function (c) {
          return map[c] = true;
        });
        return map;
      };

      var SCHEME = /^[a-z][a-z0-9+\-.]*:/;

      var openURL = /*#__PURE__*/function () {
        var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(url, ev, direction, animation) {
          var router;
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  if (!(url != null && url[0] !== '#' && !SCHEME.test(url))) {
                    _context.next = 5;
                    break;
                  }

                  router = document.querySelector('ion-router');

                  if (!router) {
                    _context.next = 5;
                    break;
                  }

                  if (ev != null) {
                    ev.preventDefault();
                  }

                  return _context.abrupt("return", router.push(url, direction, animation));

                case 5:
                  return _context.abrupt("return", false);

                case 6:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee);
        }));

        return function openURL(_x, _x2, _x3, _x4) {
          return _ref.apply(this, arguments);
        };
      }();
      /***/

    },

    /***/
    "9AS+":
    /*!*******************************************!*\
      !*** ./src/app/pages/model/model.page.ts ***!
      \*******************************************/

    /*! exports provided: ModelPage */

    /***/
    function AS(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ModelPage", function () {
        return ModelPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_model_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./model.page.html */
      "frY/");
      /* harmony import */


      var _model_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./model.page.scss */
      "9Sjj");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var ModelPage = /*#__PURE__*/function () {
        function ModelPage(modalController) {
          _classCallCheck(this, ModelPage);

          this.modalController = modalController;
        }

        _createClass(ModelPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "modelDismiss",
          value: function modelDismiss(value) {
            if (value == 'no') {
              this.modalController.dismiss(false);
            } else {
              this.modalController.dismiss(true);
            }
          }
        }, {
          key: "dismiss",
          value: function dismiss() {
            this.modalController.dismiss();
          }
        }]);

        return ModelPage;
      }();

      ModelPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }];
      };

      ModelPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-model',
        template: _raw_loader_model_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_model_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], ModelPage);
      /***/
    },

    /***/
    "9Sjj":
    /*!*********************************************!*\
      !*** ./src/app/pages/model/model.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function Sjj(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-icon {\n  font-size: 22px;\n  margin-left: 8px;\n  margin-top: 8px;\n  margin-right: 8px;\n}\n\n.btn_display_block {\n  text-align: end;\n  display: block;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL21vZGVsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUNFO0VBQ0UsZUFBQTtFQUNBLGNBQUE7QUFFSiIsImZpbGUiOiJtb2RlbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taWNvbiB7XG4gICAgZm9udC1zaXplOiAyMnB4O1xuICAgIG1hcmdpbi1sZWZ0OiA4cHg7XG4gICAgbWFyZ2luLXRvcDogOHB4O1xuICAgIG1hcmdpbi1yaWdodDogOHB4O1xuICB9XG4gIC5idG5fZGlzcGxheV9ibG9jayB7XG4gICAgdGV4dC1hbGlnbjogZW5kO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICB9XG4gICJdfQ== */";
      /***/
    },

    /***/
    "EUvm":
    /*!***********************************************!*\
      !*** ./src/app/pages/iframe/iframe.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function EUvm(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".close-fake {\n  --background: transparent;\n  margin-top: 20px;\n  margin-top: constant(safe-area-inset-top);\n  margin-top: env(safe-area-inset-top);\n}\n.close-fake ion-icon {\n  font-size: 22px;\n  color: #ffffff;\n}\nion-content {\n  --background: rgb(0 0 0);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2lmcmFtZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EseUNBQUE7RUFDQSxvQ0FBQTtBQUNKO0FBQ0k7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQUNSO0FBRUE7RUFDSSx3QkFBQTtBQUNKIiwiZmlsZSI6ImlmcmFtZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2xvc2UtZmFrZSB7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIG1hcmdpbi10b3A6IGNvbnN0YW50KHNhZmUtYXJlYS1pbnNldC10b3ApO1xuICAgIG1hcmdpbi10b3A6IGVudihzYWZlLWFyZWEtaW5zZXQtdG9wKTtcblxuICAgIGlvbi1pY29uIHtcbiAgICAgICAgZm9udC1zaXplOiAyMnB4O1xuICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICB9XG59XG5pb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiByZ2IoMCAwIDApO1xuICAgIC8vIC0tYmFja2dyb3VuZDogcmdiKDU2IDU2IDU2KTtcbiAgfSJdfQ== */";
      /***/
    },

    /***/
    "JbSX":
    /*!*********************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm/button-active-4927a4c1.js ***!
      \*********************************************************************/

    /*! exports provided: c */

    /***/
    function JbSX(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "c", function () {
        return createButtonActiveGesture;
      });
      /* harmony import */


      var _index_7a8b7a1c_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./index-7a8b7a1c.js */
      "wEJo");
      /* harmony import */


      var _haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./haptic-27b3f981.js */
      "qULd");
      /* harmony import */


      var _index_f49d994d_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./index-f49d994d.js */
      "iWo5");

      var createButtonActiveGesture = function createButtonActiveGesture(el, isButton) {
        var currentTouchedButton;
        var initialTouchedButton;

        var activateButtonAtPoint = function activateButtonAtPoint(x, y, hapticFeedbackFn) {
          if (typeof document === 'undefined') {
            return;
          }

          var target = document.elementFromPoint(x, y);

          if (!target || !isButton(target)) {
            clearActiveButton();
            return;
          }

          if (target !== currentTouchedButton) {
            clearActiveButton();
            setActiveButton(target, hapticFeedbackFn);
          }
        };

        var setActiveButton = function setActiveButton(button, hapticFeedbackFn) {
          currentTouchedButton = button;

          if (!initialTouchedButton) {
            initialTouchedButton = currentTouchedButton;
          }

          var buttonToModify = currentTouchedButton;
          Object(_index_7a8b7a1c_js__WEBPACK_IMPORTED_MODULE_0__["c"])(function () {
            return buttonToModify.classList.add('ion-activated');
          });
          hapticFeedbackFn();
        };

        var clearActiveButton = function clearActiveButton() {
          var dispatchClick = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

          if (!currentTouchedButton) {
            return;
          }

          var buttonToModify = currentTouchedButton;
          Object(_index_7a8b7a1c_js__WEBPACK_IMPORTED_MODULE_0__["c"])(function () {
            return buttonToModify.classList.remove('ion-activated');
          });
          /**
           * Clicking on one button, but releasing on another button
           * does not dispatch a click event in browsers, so we
           * need to do it manually here. Some browsers will
           * dispatch a click if clicking on one button, dragging over
           * another button, and releasing on the original button. In that
           * case, we need to make sure we do not cause a double click there.
           */

          if (dispatchClick && initialTouchedButton !== currentTouchedButton) {
            currentTouchedButton.click();
          }

          currentTouchedButton = undefined;
        };

        return Object(_index_f49d994d_js__WEBPACK_IMPORTED_MODULE_2__["createGesture"])({
          el: el,
          gestureName: 'buttonActiveDrag',
          threshold: 0,
          onStart: function onStart(ev) {
            return activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_1__["a"]);
          },
          onMove: function onMove(ev) {
            return activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_1__["b"]);
          },
          onEnd: function onEnd() {
            clearActiveButton(true);
            Object(_haptic_27b3f981_js__WEBPACK_IMPORTED_MODULE_1__["h"])();
            initialTouchedButton = undefined;
          }
        });
      };
      /***/

    },

    /***/
    "RN+R":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/success-model/success-model.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function RNR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Congratulations</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"ion-text-center\">\n    <img src=\"assets/icon/check.svg\">\n  </div>\n\n  <ion-grid>\n    <!-- <ion-row>\n      <ion-col class=\"ion-text-center\"> -->\n      <!-- </ion-col>\n    </ion-row> -->\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <h4>{{data}}</h4>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n<ion-footer>\n  <ion-row padding-vertical>\n    <ion-col margin-left margin-right no-padding>\n      <ion-button expand=\"full\" (click)=\"dismiss()\">Ok</ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>";
      /***/
    },

    /***/
    "TX3h":
    /*!*****************************************************!*\
      !*** ./src/app/services/image-uploading.service.ts ***!
      \*****************************************************/

    /*! exports provided: ImageUploadingService */

    /***/
    function TX3h(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ImageUploadingService", function () {
        return ImageUploadingService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/camera/ngx */
      "a/9d");
      /* harmony import */


      var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic-native/file-transfer/ngx */
      "B7Rs");
      /* harmony import */


      var _loader_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./loader.service */
      "5dVO");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");
      /* harmony import */


      var _toast_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./toast.service */
      "2g2N");
      /* harmony import */


      var _events_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./events.service */
      "riPR");

      var ImageUploadingService = /*#__PURE__*/function () {
        function ImageUploadingService(camera, actionSheetCtrl, transfer, loaderService, toastService, eventsService) {
          _classCallCheck(this, ImageUploadingService);

          this.camera = camera;
          this.actionSheetCtrl = actionSheetCtrl;
          this.transfer = transfer;
          this.loaderService = loaderService;
          this.toastService = toastService;
          this.eventsService = eventsService;
          this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiUrl + 'updateprofileimage.php';
          this.member_name = localStorage.getItem('name');
        }

        _createClass(ImageUploadingService, [{
          key: "uploadPhoto",
          value: function uploadPhoto() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this = this;

              var actionSheet;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.actionSheetCtrl.create({
                        header: 'Choose image from',
                        cssClass: 'alertCustom',
                        buttons: [{
                          text: 'Camera',
                          role: 'destructive',
                          handler: function handler() {
                            // console.log('Destructive clicked');
                            _this.launchCamera('camera');
                          }
                        }, {
                          text: 'Gallery',
                          handler: function handler() {
                            _this.launchCamera('gallery'); // console.log('Archive clicked');

                          }
                        }, {
                          text: 'Cancel',
                          role: 'cancel',
                          handler: function handler() {// console.log('Cancel clicked');
                          }
                        }]
                      });

                    case 2:
                      actionSheet = _context2.sent;
                      _context2.next = 5;
                      return actionSheet.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "launchCamera",
          value: function launchCamera(source) {
            var _this2 = this;

            var options = {
              quality: 80,
              sourceType: source === 'camera' ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY,
              destinationType: this.camera.DestinationType.DATA_URL,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              targetHeight: 400,
              targetWidth: 400,
              correctOrientation: true,
              allowEdit: false
            };
            this.camera.getPicture(options).then(function (imageData) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                var base64Image;
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                  while (1) {
                    switch (_context3.prev = _context3.next) {
                      case 0:
                        base64Image = 'data:image/png;base64,' + imageData;
                        this.imgData = base64Image;
                        console.log("imData : ", this.imgData);
                        this.uploadImg();

                      case 4:
                      case "end":
                        return _context3.stop();
                    }
                  }
                }, _callee3, this);
              }));
            }, function (err) {
              console.log(err);
            });
          }
        }, {
          key: "uploadImg",
          value: function uploadImg() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this3 = this;

              var fileTransfer, imgData;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      this.loaderService.showLoader();
                      fileTransfer = this.transfer.create();
                      imgData = {
                        fileKey: 'image',
                        // fileName: 'name.png',
                        fileName: Date.now() + localStorage.getItem('user_id') + '.jpg',
                        chunkedMode: false,
                        mimeType: 'image/jpeg',
                        headers: {}
                      };
                      console.log("imgData 1 ", imgData);
                      console.log(this.apiUrl + '?aid=' + localStorage.getItem('user_id'));
                      return _context4.abrupt("return", fileTransfer.upload(this.imgData, this.apiUrl + '?aid=' + localStorage.getItem('user_id'), imgData).then(function (data) {
                        console.log("data : ", data);
                        _this3.imagedata = JSON.parse(data.response);
                        _this3.profileImage = _this3.imagedata.image;
                        localStorage.removeItem('profileimage');
                        localStorage.setItem('profileimage', _this3.profileImage);
                        console.log("profileImage : ", _this3.profileImage);

                        _this3.loaderService.hideLoader();

                        _this3.toastService.showToast('Picture Update Successfully');

                        var userData = {
                          profileimage: _this3.imagedata.image
                        };

                        _this3.eventsService.publish('pictureChanged', userData); //   // alert("success");
                        //  // loading.dismiss();
                        //  // this.GetProfilePhoto();
                        //   // this.navCtrl.push(ElectronicPage);

                      }, function (err) {
                        _this3.loaderService.hideLoader();

                        console.log(err); // loading.dismiss();
                        // this.navCtrl.push(ElectronicPage);
                        // alert("error" + JSON.stringify(err));
                      }));

                    case 6:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }]);

        return ImageUploadingService;
      }();

      ImageUploadingService.ctorParameters = function () {
        return [{
          type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"]
        }, {
          type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_4__["FileTransfer"]
        }, {
          type: _loader_service__WEBPACK_IMPORTED_MODULE_5__["LoaderService"]
        }, {
          type: _toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"]
        }, {
          type: _events_service__WEBPACK_IMPORTED_MODULE_8__["EventsService"]
        }];
      };

      ImageUploadingService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ImageUploadingService);
      /***/
    },

    /***/
    "WFBn":
    /*!*************************************************************!*\
      !*** ./src/app/pages/success-model/success-model.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function WFBn(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "img {\n  height: 200px;\n  margin-top: 50px;\n}\n\nion-footer {\n  padding: 0 constant(safe-area-inset-right) constant(safe-area-inset-bottom) constant(safe-area-inset-left);\n  padding: 0 env(safe-area-inset-right) env(safe-area-inset-bottom) env(safe-area-inset-left);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3N1Y2Nlc3MtbW9kZWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBQ0E7RUFDSSwwR0FBQTtFQUNBLDJGQUFBO0FBRUoiLCJmaWxlIjoic3VjY2Vzcy1tb2RlbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpbWcge1xuICAgIGhlaWdodDogMjAwcHg7XG4gICAgbWFyZ2luLXRvcDogNTBweDtcbn1cbmlvbi1mb290ZXIge1xuICAgIHBhZGRpbmc6IDAgY29uc3RhbnQoc2FmZS1hcmVhLWluc2V0LXJpZ2h0KSBjb25zdGFudChzYWZlLWFyZWEtaW5zZXQtYm90dG9tKSBjb25zdGFudChzYWZlLWFyZWEtaW5zZXQtbGVmdCk7IC8vaU9TIDExLjJcbiAgICBwYWRkaW5nOiAwIGVudihzYWZlLWFyZWEtaW5zZXQtcmlnaHQpIGVudihzYWZlLWFyZWEtaW5zZXQtYm90dG9tKSBlbnYoc2FmZS1hcmVhLWluc2V0LWxlZnQpO1xufSJdfQ== */";
      /***/
    },

    /***/
    "Xu8g":
    /*!***************************************************!*\
      !*** ./src/app/services/send-smsemail.service.ts ***!
      \***************************************************/

    /*! exports provided: SendSMSEmailService */

    /***/
    function Xu8g(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SendSMSEmailService", function () {
        return SendSMSEmailService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");

      var SendSMSEmailService = /*#__PURE__*/function () {
        function SendSMSEmailService(http) {
          _classCallCheck(this, SendSMSEmailService);

          this.http = http;
          this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
        }

        _createClass(SendSMSEmailService, [{
          key: "sendemail",
          value: function sendemail(formData) {
            var _this4 = this;

            return new Promise(function (resolve, reject) {
              console.log(_this4.apiUrl + 'sendemail.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this4.http.post(_this4.apiUrl + 'sendemail.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }, {
          key: "sendsms",
          value: function sendsms(formData) {
            var _this5 = this;

            return new Promise(function (resolve, reject) {
              console.log(_this5.apiUrl + 'sendsms.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this5.http.post(_this5.apiUrl + 'sendsms.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }]);

        return SendSMSEmailService;
      }();

      SendSMSEmailService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      };

      SendSMSEmailService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], SendSMSEmailService);
      /***/
    },

    /***/
    "ZdFT":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/iframe/iframe.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function ZdFT(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"close\" (click)=\"dismiss()\" slot=\"end\"></ion-icon>\n    <ion-title color=\"secondary\">Setup Twilio</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n<ion-content fullscreen>\n  <ion-item class=\"close-fake\" lines=\"none\" text-center>\n    <ion-button (click)=\"dismiss()\" fill=\"clear\" color=\"light\">\n      <ion-icon name=\"close\" slot=\"start\"></ion-icon>\n    </ion-button>\n  </ion-item>\n  <iframe style=\" min-width: 100%; \n  /* min-height: 100%; */\n  height: 85%;\n  width: auto; \n  /* height: auto;  */\n  \" \n  [src]=\"trustedVideoUrl\" frameborder=\"0\" allowfullscreen></iframe>\n</ion-content>\n\n\n";
      /***/
    },

    /***/
    "acej":
    /*!**************************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm/framework-delegate-4392cd63.js ***!
      \**************************************************************************/

    /*! exports provided: a, d */

    /***/
    function acej(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "a", function () {
        return attachComponent;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "d", function () {
        return detachComponent;
      });
      /* harmony import */


      var _helpers_dd7e4b7b_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./helpers-dd7e4b7b.js */
      "1vRN");

      var attachComponent = /*#__PURE__*/function () {
        var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(delegate, container, component, cssClasses, componentProps) {
          var el;
          return regeneratorRuntime.wrap(function _callee5$(_context5) {
            while (1) {
              switch (_context5.prev = _context5.next) {
                case 0:
                  if (!delegate) {
                    _context5.next = 2;
                    break;
                  }

                  return _context5.abrupt("return", delegate.attachViewToDom(container, component, componentProps, cssClasses));

                case 2:
                  if (!(typeof component !== 'string' && !(component instanceof HTMLElement))) {
                    _context5.next = 4;
                    break;
                  }

                  throw new Error('framework delegate is missing');

                case 4:
                  el = typeof component === 'string' ? container.ownerDocument && container.ownerDocument.createElement(component) : component;

                  if (cssClasses) {
                    cssClasses.forEach(function (c) {
                      return el.classList.add(c);
                    });
                  }

                  if (componentProps) {
                    Object.assign(el, componentProps);
                  }

                  container.appendChild(el);
                  _context5.next = 10;
                  return new Promise(function (resolve) {
                    return Object(_helpers_dd7e4b7b_js__WEBPACK_IMPORTED_MODULE_0__["c"])(el, resolve);
                  });

                case 10:
                  return _context5.abrupt("return", el);

                case 11:
                case "end":
                  return _context5.stop();
              }
            }
          }, _callee5);
        }));

        return function attachComponent(_x5, _x6, _x7, _x8, _x9) {
          return _ref2.apply(this, arguments);
        };
      }();

      var detachComponent = function detachComponent(delegate, element) {
        if (element) {
          if (delegate) {
            var container = element.parentElement;
            return delegate.removeViewFromDom(container, element);
          }

          element.remove();
        }

        return Promise.resolve();
      };
      /***/

    },

    /***/
    "dwY0":
    /*!*********************************************!*\
      !*** ./src/app/services/network.service.ts ***!
      \*********************************************/

    /*! exports provided: ConnectionStatus, NetworkService */

    /***/
    function dwY0(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConnectionStatus", function () {
        return ConnectionStatus;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NetworkService", function () {
        return NetworkService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic-native/network/ngx */
      "kwrG");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs */
      "qCKp");

      var ConnectionStatus;

      (function (ConnectionStatus) {
        ConnectionStatus[ConnectionStatus["Online"] = 0] = "Online";
        ConnectionStatus[ConnectionStatus["Offline"] = 1] = "Offline";
      })(ConnectionStatus || (ConnectionStatus = {}));

      var NetworkService = /*#__PURE__*/function () {
        function NetworkService(network, toastController, plt) {
          var _this6 = this;

          _classCallCheck(this, NetworkService);

          this.network = network;
          this.toastController = toastController;
          this.plt = plt;
          this.status = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](ConnectionStatus.Offline);
          this.plt.ready().then(function () {
            _this6.initializeNetworkEvents();

            var status = _this6.network.type !== 'none' ? ConnectionStatus.Online : ConnectionStatus.Offline;

            _this6.status.next(status);
          });
        }

        _createClass(NetworkService, [{
          key: "initializeNetworkEvents",
          value: function initializeNetworkEvents() {
            var _this7 = this;

            this.network.onDisconnect().subscribe(function () {
              if (_this7.status.getValue() === ConnectionStatus.Online) {
                console.log('WE ARE OFFLINE');

                _this7.updateNetworkStatus(ConnectionStatus.Offline);
              }
            });
            this.network.onConnect().subscribe(function () {
              if (_this7.status.getValue() === ConnectionStatus.Offline) {
                console.log('WE ARE ONLINE');

                _this7.updateNetworkStatus(ConnectionStatus.Online);
              }
            });
          }
        }, {
          key: "updateNetworkStatus",
          value: function updateNetworkStatus(status) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var connection, toast;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      this.status.next(status);
                      connection = status == ConnectionStatus.Offline ? 'Offline' : 'Online';
                      toast = this.toastController.create({
                        message: "You are now ".concat(connection),
                        duration: 3000,
                        position: 'bottom'
                      });
                      toast.then(function (toast) {
                        return toast.present();
                      });

                    case 4:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "onNetworkChange",
          value: function onNetworkChange() {
            return this.status.asObservable();
          }
        }, {
          key: "getCurrentNetworkStatus",
          value: function getCurrentNetworkStatus() {
            return this.status.getValue();
          }
        }]);

        return NetworkService;
      }();

      NetworkService.ctorParameters = function () {
        return [{
          type: _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_2__["Network"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
        }];
      };

      NetworkService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], NetworkService);
      /***/
    },

    /***/
    "frY/":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/model/model.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function frY(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <ion-buttons class=\"btn_display_block\" (click)=\"dismiss()\">\n    <ion-icon class=\"close_icon \" name=\"close\"></ion-icon>\n  </ion-buttons>\n  <div class=\"ion-text-center\">\n    <img src=\"assets/img/alert.png\">\n  </div>\n  <p class=\"ion-padding-horizontal ion-text-center\">\n    <b style=\"color: #ff0022;\">Attention:</b> Contacts importing time depends on your number of contacts in phone. Please do not\n    navigate away\n    from app while this process is performed. It may take up to 2 minutes in case of large contacts list.\n  </p>\n</ion-content>\n<ion-footer>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button expand=\"block\" color=\"danger\" (click)=\"modelDismiss('no')\"> No\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-button expand=\"block\" class=\"success\" (click)=\"modelDismiss('yes')\"> Yes, I Understand\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer>";
      /***/
    },

    /***/
    "h3R7":
    /*!***********************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm/spinner-configs-cd7845af.js ***!
      \***********************************************************************/

    /*! exports provided: S */

    /***/
    function h3R7(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "S", function () {
        return SPINNERS;
      });

      var spinners = {
        'bubbles': {
          dur: 1000,
          circles: 9,
          fn: function fn(dur, index, total) {
            var animationDelay = "".concat(dur * index / total - dur, "ms");
            var angle = 2 * Math.PI * index / total;
            return {
              r: 5,
              style: {
                'top': "".concat(9 * Math.sin(angle), "px"),
                'left': "".concat(9 * Math.cos(angle), "px"),
                'animation-delay': animationDelay
              }
            };
          }
        },
        'circles': {
          dur: 1000,
          circles: 8,
          fn: function fn(dur, index, total) {
            var step = index / total;
            var animationDelay = "".concat(dur * step - dur, "ms");
            var angle = 2 * Math.PI * step;
            return {
              r: 5,
              style: {
                'top': "".concat(9 * Math.sin(angle), "px"),
                'left': "".concat(9 * Math.cos(angle), "px"),
                'animation-delay': animationDelay
              }
            };
          }
        },
        'circular': {
          dur: 1400,
          elmDuration: true,
          circles: 1,
          fn: function fn() {
            return {
              r: 20,
              cx: 48,
              cy: 48,
              fill: 'none',
              viewBox: '24 24 48 48',
              transform: 'translate(0,0)',
              style: {}
            };
          }
        },
        'crescent': {
          dur: 750,
          circles: 1,
          fn: function fn() {
            return {
              r: 26,
              style: {}
            };
          }
        },
        'dots': {
          dur: 750,
          circles: 3,
          fn: function fn(_, index) {
            var animationDelay = -(110 * index) + 'ms';
            return {
              r: 6,
              style: {
                'left': "".concat(9 - 9 * index, "px"),
                'animation-delay': animationDelay
              }
            };
          }
        },
        'lines': {
          dur: 1000,
          lines: 12,
          fn: function fn(dur, index, total) {
            var transform = "rotate(".concat(30 * index + (index < 6 ? 180 : -180), "deg)");
            var animationDelay = "".concat(dur * index / total - dur, "ms");
            return {
              y1: 17,
              y2: 29,
              style: {
                'transform': transform,
                'animation-delay': animationDelay
              }
            };
          }
        },
        'lines-small': {
          dur: 1000,
          lines: 12,
          fn: function fn(dur, index, total) {
            var transform = "rotate(".concat(30 * index + (index < 6 ? 180 : -180), "deg)");
            var animationDelay = "".concat(dur * index / total - dur, "ms");
            return {
              y1: 12,
              y2: 20,
              style: {
                'transform': transform,
                'animation-delay': animationDelay
              }
            };
          }
        }
      };
      var SPINNERS = spinners;
      /***/
    },

    /***/
    "hnmG":
    /*!***********************************************************!*\
      !*** ./src/app/pages/success-model/success-model.page.ts ***!
      \***********************************************************/

    /*! exports provided: SuccessModelPage */

    /***/
    function hnmG(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SuccessModelPage", function () {
        return SuccessModelPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_success_model_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./success-model.page.html */
      "RN+R");
      /* harmony import */


      var _success_model_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./success-model.page.scss */
      "WFBn");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var SuccessModelPage = /*#__PURE__*/function () {
        function SuccessModelPage(navParams, modalController) {
          _classCallCheck(this, SuccessModelPage);

          this.modalController = modalController;
          this.data = navParams.get('value');
          console.log("this.data : ", this.data);
        }

        _createClass(SuccessModelPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "dismiss",
          value: function dismiss() {
            this.modalController.dismiss();
          }
        }]);

        return SuccessModelPage;
      }();

      SuccessModelPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavParams"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }];
      };

      SuccessModelPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-success-model',
        template: _raw_loader_success_model_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_success_model_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], SuccessModelPage);
      /***/
    },

    /***/
    "ofzi":
    /*!******************************************!*\
      !*** ./src/app/services/quiz.service.ts ***!
      \******************************************/

    /*! exports provided: QuizService */

    /***/
    function ofzi(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "QuizService", function () {
        return QuizService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");

      var QuizService = /*#__PURE__*/function () {
        function QuizService(http) {
          _classCallCheck(this, QuizService);

          this.http = http;
          this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
        }

        _createClass(QuizService, [{
          key: "getQuiz",
          value: function getQuiz(formData) {
            var _this8 = this;

            return new Promise(function (resolve, reject) {
              console.log(_this8.apiUrl + 'video.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this8.http.post(_this8.apiUrl + 'video.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }, {
          key: "getAllQuizes",
          value: function getAllQuizes(formData) {
            var _this9 = this;

            return new Promise(function (resolve, reject) {
              console.log(_this9.apiUrl + 'quizdetails.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this9.http.post(_this9.apiUrl + 'quizdetails.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }, {
          key: "saveQuizAttempt",
          value: function saveQuizAttempt(formData) {
            var _this10 = this;

            return new Promise(function (resolve, reject) {
              console.log(_this10.apiUrl + 'video.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this10.http.post(_this10.apiUrl + 'video.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }, {
          key: "saveQuizAnswer",
          value: function saveQuizAnswer(formData) {
            var _this11 = this;

            return new Promise(function (resolve, reject) {
              console.log(_this11.apiUrl + 'video.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this11.http.post(_this11.apiUrl + 'video.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }, {
          key: "quizdetails",
          value: function quizdetails(formData) {
            var _this12 = this;

            return new Promise(function (resolve, reject) {
              console.log(_this12.apiUrl + 'quizdetails.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this12.http.post(_this12.apiUrl + 'quizdetails.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }, {
          key: "performquiz",
          value: function performquiz(formData) {
            var _this13 = this;

            return new Promise(function (resolve, reject) {
              console.log(_this13.apiUrl + 'performquiz.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this13.http.post(_this13.apiUrl + 'performquiz.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }]);

        return QuizService;
      }();

      QuizService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      };

      QuizService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], QuizService);
      /***/
    },

    /***/
    "ojxP":
    /*!**********************************************!*\
      !*** ./node_modules/scriptjs/dist/script.js ***!
      \**********************************************/

    /*! no static exports found */

    /***/
    function ojxP(module, exports, __webpack_require__) {
      var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;
      /*!
      * $script.js JS loader & dependency manager
      * https://github.com/ded/script.js
      * (c) Dustin Diaz 2014 | License MIT
      */


      (function (name, definition) {
        if (true && module.exports) module.exports = definition();else if (true) !(__WEBPACK_AMD_DEFINE_FACTORY__ = definition, __WEBPACK_AMD_DEFINE_RESULT__ = typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? __WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module) : __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));else {}
      })('$script', function () {
        var doc = document,
            head = doc.getElementsByTagName('head')[0],
            s = 'string',
            f = false,
            push = 'push',
            readyState = 'readyState',
            onreadystatechange = 'onreadystatechange',
            list = {},
            ids = {},
            delay = {},
            scripts = {},
            scriptpath,
            urlArgs;

        function every(ar, fn) {
          for (var i = 0, j = ar.length; i < j; ++i) {
            if (!fn(ar[i])) return f;
          }

          return 1;
        }

        function each(ar, fn) {
          every(ar, function (el) {
            fn(el);
            return 1;
          });
        }

        function $script(paths, idOrDone, optDone) {
          paths = paths[push] ? paths : [paths];
          var idOrDoneIsDone = idOrDone && idOrDone.call,
              done = idOrDoneIsDone ? idOrDone : optDone,
              id = idOrDoneIsDone ? paths.join('') : idOrDone,
              queue = paths.length;

          function loopFn(item) {
            return item.call ? item() : list[item];
          }

          function callback() {
            if (! --queue) {
              list[id] = 1;
              done && done();

              for (var dset in delay) {
                every(dset.split('|'), loopFn) && !each(delay[dset], loopFn) && (delay[dset] = []);
              }
            }
          }

          setTimeout(function () {
            each(paths, function loading(path, force) {
              if (path === null) return callback();

              if (!force && !/^https?:\/\//.test(path) && scriptpath) {
                path = path.indexOf('.js') === -1 ? scriptpath + path + '.js' : scriptpath + path;
              }

              if (scripts[path]) {
                if (id) ids[id] = 1;
                return scripts[path] == 2 ? callback() : setTimeout(function () {
                  loading(path, true);
                }, 0);
              }

              scripts[path] = 1;
              if (id) ids[id] = 1;
              create(path, callback);
            });
          }, 0);
          return $script;
        }

        function create(path, fn) {
          var el = doc.createElement('script'),
              loaded;

          el.onload = el.onerror = el[onreadystatechange] = function () {
            if (el[readyState] && !/^c|loade/.test(el[readyState]) || loaded) return;
            el.onload = el[onreadystatechange] = null;
            loaded = 1;
            scripts[path] = 2;
            fn();
          };

          el.async = 1;
          el.src = urlArgs ? path + (path.indexOf('?') === -1 ? '?' : '&') + urlArgs : path;
          head.insertBefore(el, head.lastChild);
        }

        $script.get = create;

        $script.order = function (scripts, id, done) {
          (function callback(s) {
            s = scripts.shift();
            !scripts.length ? $script(s, id, done) : $script(s, callback);
          })();
        };

        $script.path = function (p) {
          scriptpath = p;
        };

        $script.urlArgs = function (str) {
          urlArgs = str;
        };

        $script.ready = function (deps, ready, req) {
          deps = deps[push] ? deps : [deps];
          var missing = [];
          !each(deps, function (dep) {
            list[dep] || missing[push](dep);
          }) && every(deps, function (dep) {
            return list[dep];
          }) ? ready() : !function (key) {
            delay[key] = delay[key] || [];
            delay[key][push](ready);
            req && req(missing);
          }(deps.join('|'));
          return $script;
        };

        $script.done = function (idOrDone) {
          $script([null], idOrDone);
        };

        return $script;
      });
      /***/

    },

    /***/
    "qULd":
    /*!**************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm/haptic-27b3f981.js ***!
      \**************************************************************/

    /*! exports provided: a, b, c, d, h */

    /***/
    function qULd(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "a", function () {
        return hapticSelectionStart;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "b", function () {
        return hapticSelectionChanged;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "c", function () {
        return hapticSelection;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "d", function () {
        return hapticImpact;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "h", function () {
        return hapticSelectionEnd;
      });

      var HapticEngine = {
        getEngine: function getEngine() {
          var win = window;
          return win.TapticEngine || win.Capacitor && win.Capacitor.isPluginAvailable('Haptics') && win.Capacitor.Plugins.Haptics;
        },
        available: function available() {
          return !!this.getEngine();
        },
        isCordova: function isCordova() {
          return !!window.TapticEngine;
        },
        isCapacitor: function isCapacitor() {
          var win = window;
          return !!win.Capacitor;
        },
        impact: function impact(options) {
          var engine = this.getEngine();

          if (!engine) {
            return;
          }

          var style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
          engine.impact({
            style: style
          });
        },
        notification: function notification(options) {
          var engine = this.getEngine();

          if (!engine) {
            return;
          }

          var style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
          engine.notification({
            style: style
          });
        },
        selection: function selection() {
          this.impact({
            style: 'light'
          });
        },
        selectionStart: function selectionStart() {
          var engine = this.getEngine();

          if (!engine) {
            return;
          }

          if (this.isCapacitor()) {
            engine.selectionStart();
          } else {
            engine.gestureSelectionStart();
          }
        },
        selectionChanged: function selectionChanged() {
          var engine = this.getEngine();

          if (!engine) {
            return;
          }

          if (this.isCapacitor()) {
            engine.selectionChanged();
          } else {
            engine.gestureSelectionChanged();
          }
        },
        selectionEnd: function selectionEnd() {
          var engine = this.getEngine();

          if (!engine) {
            return;
          }

          if (this.isCapacitor()) {
            engine.selectionEnd();
          } else {
            engine.gestureSelectionEnd();
          }
        }
      };
      /**
       * Trigger a selection changed haptic event. Good for one-time events
       * (not for gestures)
       */

      var hapticSelection = function hapticSelection() {
        HapticEngine.selection();
      };
      /**
       * Tell the haptic engine that a gesture for a selection change is starting.
       */


      var hapticSelectionStart = function hapticSelectionStart() {
        HapticEngine.selectionStart();
      };
      /**
       * Tell the haptic engine that a selection changed during a gesture.
       */


      var hapticSelectionChanged = function hapticSelectionChanged() {
        HapticEngine.selectionChanged();
      };
      /**
       * Tell the haptic engine we are done with a gesture. This needs to be
       * called lest resources are not properly recycled.
       */


      var hapticSelectionEnd = function hapticSelectionEnd() {
        HapticEngine.selectionEnd();
      };
      /**
       * Use this to indicate success/failure/warning to the user.
       * options should be of the type `{ style: 'light' }` (or `medium`/`heavy`)
       */


      var hapticImpact = function hapticImpact(options) {
        HapticEngine.impact(options);
      };
      /***/

    },

    /***/
    "sjK5":
    /*!******************************************!*\
      !*** ./src/app/services/chat.service.ts ***!
      \******************************************/

    /*! exports provided: ChatService */

    /***/
    function sjK5(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChatService", function () {
        return ChatService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");

      var ChatService = /*#__PURE__*/function () {
        function ChatService(http) {
          _classCallCheck(this, ChatService);

          this.http = http;
          this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
        }

        _createClass(ChatService, [{
          key: "smschatcheck",
          value: function smschatcheck(formData) {
            var _this14 = this;

            return new Promise(function (resolve, reject) {
              // console.log(this.apiUrl + 'smschatcheck.php');
              var api = "https://wvfitness.net/admin/appapi/";
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this14.http.post(api + 'smschatcheck.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }, {
          key: "smschathistory",
          value: function smschathistory(formData) {
            var _this15 = this;

            return new Promise(function (resolve, reject) {
              // console.log(this.apiUrl + 'smschatcheck.php');
              var api = "https://wvfitness.net/admin/appapi/";
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this15.http.post(api + 'smschathistory.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }, {
          key: "updatetwilio",
          value: function updatetwilio(formData) {
            var _this16 = this;

            return new Promise(function (resolve, reject) {
              console.log(formData);
              console.log(_this16.apiUrl + 'updatetwilio.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this16.http.post(_this16.apiUrl + 'updatetwilio.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }]);

        return ChatService;
      }();

      ChatService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      };

      ChatService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], ChatService);
      /***/
    },

    /***/
    "tKxX":
    /*!*********************************************!*\
      !*** ./src/app/pages/iframe/iframe.page.ts ***!
      \*********************************************/

    /*! exports provided: IframePage */

    /***/
    function tKxX(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "IframePage", function () {
        return IframePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_iframe_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./iframe.page.html */
      "ZdFT");
      /* harmony import */


      var _iframe_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./iframe.page.scss */
      "EUvm");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");

      var IframePage = /*#__PURE__*/function () {
        function IframePage(navParam, domSanitizer, modalController) {
          _classCallCheck(this, IframePage);

          this.navParam = navParam;
          this.domSanitizer = domSanitizer;
          this.modalController = modalController;
          this.trustedVideoUrl = null;
        }

        _createClass(IframePage, [{
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.video = this.navParam.get('videoLink');
            this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.video);
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "dismiss",
          value: function dismiss() {
            this.modalController.dismiss();
          }
        }]);

        return IframePage;
      }();

      IframePage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"]
        }, {
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }];
      };

      IframePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-iframe',
        template: _raw_loader_iframe_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_iframe_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], IframePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=common-es5.js.map