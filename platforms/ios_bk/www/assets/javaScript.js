    // get the video, volume and seekbar elements
    var video = document.getElementById("video1");
    // var volumeRange = document.getElementById('volume');
    var seekbar = document.getElementById('seekbar');

    // attach timeupdate, durationchange event to the video element
    if (video) {
        window.onload = function () {
            // go to http://www.w3.org/TR/DOM-Level-2-Events/events.html#Events-
            // EventTarget-addEventListener to know more about addEventListener
            // (false is for bubbling and true is for event capturing)
            video.addEventListener('timeupdate', UpdateTheTime, false);
            video.addEventListener('durationchange', SetSeekBar, false);
            volumeRange.value = video.volume;
        }
    }
    // testvar = "heloo from external js";

    // // fires when volume element is changed
    // function ChangeVolume() {
    //     var myVol = volumeRange.value;
    //     video.volume = myVol;
    //     if (myVol == 0) {
    //         video.muted = true;
    //     } else {
    //         video.muted = false;
    //     }
    // }

    // // fires when page loads, it sets the min and max range of the video
    // function SetSeekBar() {
    //     seekbar.min = 0;
    //     seekbar.max = video.duration;
    // }

    // fires when seekbar is changed
    function ChangeTheTime() {
        video.currentTime = seekbar.value;
    }

    // // fires when video plays and the time is updated in the video element, this
    // // writes the current duration elapsed in the label element
    // function UpdateTheTime() {
    //     var sec = video.currentTime;
    //     var h = Math.floor(sec / 3600);
    //     sec = sec % 3600;
    //     var min = Math.floor(sec / 60);
    //     sec = Math.floor(sec % 60);
    //     if (sec.toString().length < 2) sec = "0" + sec;
    //     if (min.toString().length < 2) min = "0" + min;
    //     document.getElementById('lblTime').innerHTML = h + ":" + min + ":" + sec;
    //     seekbar.min = video.startTime;
    //     seekbar.max = video.duration;
    //     seekbar.value = video.currentTime;
    //     console.log("seekbar.min : ", seekbar.min)
    //     console.log("seekbar.max : ", seekbar.max)
    //     console.log("seekbar.value : ", seekbar.value)
    // }

    // // fires when Play button is clicked
    // function PlayNow() {
    //     console.log("video : ",video)
    //     if (video.paused) {
    //         video.currentTime = 100;
    //         video.play();
    //     } else if (video.ended) {
    //         video.currentTime = 0;
    //         video.play();
    //     }
    //     console.log("play")
    //     console.log("start time :", video.startTime)
    //     console.log("end time : ", video.duration)
    //     let unix_timestamp = video.duration
    //     // Create a new JavaScript Date object based on the timestamp
    //     // multiplied by 1000 so that the argument is in milliseconds, not seconds.
    //     var date = new Date(unix_timestamp * 1000);
    //     // Hours part from the timestamp
    //     var hours = date.getHours();
    //     // Minutes part from the timestamp
    //     var minutes = "0" + date.getMinutes();
    //     // Seconds part from the timestamp
    //     var seconds = "0" + date.getSeconds();

    //     // Will display time in 10:30:23 format
    //     var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

    //     console.log("formattedTime : ", formattedTime);
    //     setInterval(() => {
    //         UpdateTheTime()
    //         SetSeekBar()
    //     }, 1000)
    // }

    // // fires when Pause button is clicked
    // function PauseNow() {
    //     if (video.play) {
    //         video.pause();
    //     }
    //     console.log("pause")
    //     clearInterval();
    // }

    // // fires when Mute button is clicked
    // function MuteNow() {
    //     if (video.muted) {
    //         video.muted = false;
    //         volumeRange.value = video.volume;
    //     } else {
    //         video.muted = true;
    //         volumeRange.value = 0;
    //     }
    // }