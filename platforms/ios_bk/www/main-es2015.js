(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/customer/Documents/ionic/NashApp/src/main.ts */"zUnb");


/***/ }),

/***/ 1:
/*!************************!*\
  !*** canvas (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ "2MiI":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_header_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./header.component.html */ "yxfS");
/* harmony import */ var _header_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./header.component.scss */ "oHuE");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_events_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/events.service */ "riPR");
/* harmony import */ var src_app_services_notification_list_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/notification-list.service */ "lpcq");








let HeaderComponent = class HeaderComponent {
    constructor(router, navCtrl, eventsService, notificationListService) {
        this.router = router;
        this.navCtrl = navCtrl;
        this.eventsService = eventsService;
        this.notificationListService = notificationListService;
        this.title = "";
        this.notification = false;
        this.count = 0;
        this.checkPage = false;
        this.eventsService.subscribe('notification_count', (data) => {
            this.notificationsCount();
        });
    }
    ngOnInit() {
        this.notificationsCount();
    }
    notificationsCount() {
        // this.loaderService.showLoader()
        let formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        this.notificationListService.notificationsCount(formData).then((res) => {
            console.log(res);
            if (res.status !== false) {
                this.count = res.count;
                console.log("this.notification_count : ", this.count);
                // this.loaderService.hideLoader()
            }
            else {
                // this.loaderService.hideLoader()
            }
        });
    }
    get hasNotification() {
        return (this.notification) ? true : false;
    }
    notification_fun() {
        this.router.navigate(['tabs/notification']);
    }
    home() {
        this.navCtrl.navigateRoot('/tabs/tab1');
    }
    back() {
        this.navCtrl.back();
    }
};
HeaderComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: src_app_services_events_service__WEBPACK_IMPORTED_MODULE_6__["EventsService"] },
    { type: src_app_services_notification_list_service__WEBPACK_IMPORTED_MODULE_7__["NotificationListService"] }
];
HeaderComponent.propDecorators = {
    title: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    notification: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    count: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    checkPage: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
HeaderComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'header',
        template: _raw_loader_header_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_header_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], HeaderComponent);



/***/ }),

/***/ 3:
/*!**********************!*\
  !*** zlib (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ "3LUQ":
/*!*******************************************!*\
  !*** ./src/app/services/alert.service.ts ***!
  \*******************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let AlertService = class AlertService {
    constructor(alertController, loadingCtrl, navCtrl) {
        this.alertController = alertController;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.isCheck = false;
        this.isCheck = false;
    }
    presentAlertError(error) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'alertCustom',
                header: 'Warning',
                // subHeader: 'Subtitle',
                message: error,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    showPwdSuccessAlert(email) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'SUCCESS',
                backdropDismiss: false,
                message: `
        <p> Password reset email has been sent to ${email}.</p>
        <p> The email contains a URL that you will need to visit to confirm your email. Once you've confirmed
        your email you may reset your password.</p>
      `,
                buttons: [
                    {
                        text: 'Close',
                        role: 'cancel'
                    }
                ]
            });
            alert.present();
        });
    }
    presentAlertSuccess() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Success',
                cssClass: 'alertCustom',
                // subHeader: 'You have Successfully ',
                message: 'You have successfully logged In ',
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    forgotPass() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Forgot Password?',
                message: 'Enter you email address to send a reset link password.',
                inputs: [
                    {
                        name: 'email',
                        type: 'email',
                        placeholder: 'Email'
                    }
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            // console.log('Confirm Cancel');
                        }
                    },
                    {
                        text: 'Confirm',
                        handler: (alertData) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                            this.showPwdLoader(alertData);
                        })
                    }
                ]
            });
            yield alert.present();
        });
    }
    showPwdLoader(alertData) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.loadingCtrl.create({
                spinner: 'circles',
                animated: true,
                showBackdrop: true,
                translucent: true,
                mode: 'ios',
                message: 'Please wait...'
            }).then((load) => {
                this.loaderObj = load;
                this.loaderObj.present().then(() => {
                });
            });
        });
    }
    showDisableAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                backdropDismiss: false,
                message: 'Please finish workout to continue.',
                buttons: [
                    {
                        text: 'Okay',
                        role: 'cancel'
                    }
                ]
            });
            yield alert.present();
        });
    }
    presentNetworkAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Warning',
                cssClass: 'alertCustom',
                // subHeader: 'You have Successfully ',
                message: ' No Internet Available ',
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    generalAlert(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Warning',
                cssClass: 'alertCustom',
                // subHeader: 'You have Successfully ',
                message: message,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    contactList(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                // header: 'Warning',
                cssClass: 'alertCustom',
                // subHeader: 'You have Successfully ',
                message: message,
                buttons: [{
                        text: 'OK',
                        handler: () => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                            this.isCheck = true;
                        })
                    }]
            });
            yield alert.present();
        });
    }
    showAlertForQuiz(quiz) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let firstTime = false;
            let attemptsRemaining = 0;
            if (quiz.is_attempt == 0) {
                firstTime = true;
            }
            if (quiz.redo && (quiz.is_attempt < quiz.redo_attempt + 1)) {
                attemptsRemaining = (quiz.redo_attempt + 1) - quiz.is_attempt;
            }
            const buttons = [];
            buttons.push({
                text: 'Dismiss',
                role: 'cancel'
            });
            if (firstTime || attemptsRemaining > 0) {
                buttons.push({
                    text: 'Continue',
                    handler: () => {
                        this.navCtrl.navigateForward(['home-results/quiz/start-quiz/' + quiz.id]);
                    }
                });
            }
            if (firstTime && attemptsRemaining == 0) {
                attemptsRemaining = 1;
            }
            const alert = yield this.alertController.create({
                header: quiz.title,
                message: `
        <p>${quiz.description}</p>
        <p class="font-weight-600">Details</p>
        <ul class="text-align-left">
          <li>Time: ${this.transform(quiz.quiztime)}</li>
          <li>Total points: ${quiz.totalscore}</li>
          <li>Passing points: ${quiz.passcore}</li>
          <li>Attempt remaining: ${attemptsRemaining}</li>
        </ul>
      `,
                buttons: buttons
            });
            alert.present();
        });
    }
    transform(time) {
        if (time >= 60) {
            const timeInMinutes = Math.floor(time / 60);
            const hours = Math.floor(time / 3600);
            let minutes = timeInMinutes;
            if (minutes >= 60) {
                minutes = timeInMinutes % 60;
            }
            return (hours > 0 ? '0' + hours + 'hr ' : '')
                + (minutes < 10 ? '0' + minutes : minutes) +
                'mins ' + ((time - timeInMinutes * 60) < 10 ? '0' +
                (time - timeInMinutes * 60) : (time - timeInMinutes * 60)) + 'secs';
        }
        else {
            return (time < 10 ? '00: 0' + time : '00:' + time) + 'secs';
        }
    }
    showResults(results, header = 'Time is up!') {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: header,
                backdropDismiss: false,
                message: `
        <p> Results of your quiz are:</p>
        <ul class="text-align-left">
          <li>Points earned: ${results.totalpoints} out of ${results.totalscore}</li>
          <li>Passing score: ${results.passcore}</li>
          <li>Time elapsed: ${results.total_time}</li>
        </ul>
      `,
                buttons: [
                    {
                        text: 'Close',
                        handler: () => {
                            this.navCtrl.navigateBack(['home-results/quiz']);
                        }
                    }
                ]
            });
            alert.present();
        });
    }
};
AlertService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
AlertService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AlertService);



/***/ }),

/***/ 4:
/*!**********************!*\
  !*** http (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 5:
/*!***********************!*\
  !*** https (ignored) ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 6:
/*!*********************!*\
  !*** url (ignored) ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    // imageUrl: 'http:',
    // apiUrl: 'http://umrtariq.prospectingmagic.com/admin/appapi/',
    // profileImageUrl: 'http://umrtariq.prospectingmagic.com/'
    imageUrl: 'https:',
    apiUrl: 'https://mindandheartuniversity.com/admin/appapi/',
    profileImageUrl: 'https://mindandheartuniversity.com/',
    auth_token: '4d409766604ad82ba5eeb96077c977c9',
    downloadurl: 'https://mindandheartuniversity.com/admin/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "EFyh":
/*!*******************************************!*\
  !*** ./src/app/services/login.service.ts ***!
  \*******************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/environments/environment */ "AytR");







let LoginService = class LoginService {
    constructor(alertController, http) {
        this.alertController = alertController;
        this.http = http;
        this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiUrl;
    }
    handleError(operation = 'operation', result) {
        return (error) => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(result);
        };
    }
    /** Log a HeroService message with the MessageService */
    log(message) {
        this.presentAlertError(message);
        console.log(message);
    }
    presentAlertError(error) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'alertCustom',
                header: 'Error',
                // subHeader: 'Subtitle',
                message: error,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    login(formData) {
        formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].auth_token);
        return this.http.post(this.apiUrl + 'login.php', formData)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(res => res), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError('login', [])));
    }
};
LoginService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
LoginService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoginService);



/***/ }),

/***/ "FpXt":
/*!*************************************************!*\
  !*** ./src/app/modules/shared/shared.module.ts ***!
  \*************************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/components/header/header.component */ "2MiI");




let SharedModule = class SharedModule {
};
SharedModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_3__["HeaderComponent"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
        ],
        exports: [
            src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_3__["HeaderComponent"]
        ],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
    })
], SharedModule);



/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./app.component.html */ "VzVu");
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component.scss */ "ynWL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "54vc");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "VYYF");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/environments/environment */ "AytR");
/* harmony import */ var _services_alert_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/alert.service */ "3LUQ");
/* harmony import */ var _services_events_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/events.service */ "riPR");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/storage */ "e8h1");
/* harmony import */ var cordova_plugin_fcm_with_dependecy_updated_ionic_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! cordova-plugin-fcm-with-dependecy-updated/ionic/ngx */ "lOSq");













const w = window;
let AppComponent = class AppComponent {
    constructor(platform, splashScreen, statusBar, navCtrl, menu, alertService, eventsService, cdr, router, storage, fcm) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.alertService = alertService;
        this.eventsService = eventsService;
        this.cdr = cdr;
        this.router = router;
        this.storage = storage;
        this.fcm = fcm;
        this.onlineOffline = navigator.onLine;
        this.userData = {
            aid: '',
            firstname: '',
            lastname: '',
            email: '',
            profileimage: '',
            address: '',
            city: '',
            country: '',
            phone: '',
            username: '',
            zipcode: '',
            state: '',
        };
        this.setupFCM();
        this.environment = src_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"];
        this.initializeApp();
        window.addEventListener('offline', () => {
            this.alertService.presentNetworkAlert();
        });
    }
    setupFCM() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.platform.ready();
            console.log('FCM setup started');
            if (!this.platform.is('cordova')) {
                return;
            }
            console.log('In cordova platform');
            console.log('Subscribing to token updates');
            this.fcm.onTokenRefresh().subscribe((newToken) => {
                this.token = newToken;
                console.log('onTokenRefresh received event with: ', newToken);
            });
            console.log('Subscribing to new notifications');
            this.fcm.onNotification().subscribe((payload) => {
                this.pushPayload = payload;
                console.log('onNotification received event with: ', payload);
            });
            this.hasPermission = yield this.fcm.requestPushPermission();
            console.log('requestPushPermission result: ', this.hasPermission);
            this.token = yield this.fcm.getToken();
            console.log('getToken result: ', this.token);
            this.pushPayload = yield this.fcm.getInitialPushPayload();
            console.log('getInitialPushPayload result: ', this.pushPayload);
        });
    }
    get pushPayloadString() {
        return JSON.stringify(this.pushPayload, null, 4);
    }
    initializeApp() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (!navigator.onLine) {
                this.alertService.presentNetworkAlert();
            }
            this.platform.ready().then(() => {
                this.statusBar.styleDefault();
                setTimeout(() => {
                    this.splashScreen.hide();
                }, 2000);
                // let thisObj = this;
                // thisObj.fcm.requestPushPermission().then(() => {
                //   console.log("requestPushPermission true: ")
                //   // alert("suecce ")
                // }).catch((e) => {
                //   console.log("requestPushPermission : ",e)
                //   // alert("errr "+e)
                // })
                this.eventsService.subscribe('userLogged', (data) => {
                    console.log("userLogged : ", data);
                    this.userData.firstname = data.firstname;
                    this.userData.lastname = data.lastname;
                    this.userData.email = data.email;
                    this.userData.profileimage = data.profileimage;
                    this.profileImage = src_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].profileImageUrl + this.userData.profileimage;
                    console.log("this.userData.profileImage : ", this.userData.profileImage);
                    console.log("this.profileImage 1 : ", this.profileImage);
                    setTimeout(() => {
                        this.cdr.detectChanges();
                    }, 1000);
                });
                this.eventsService.subscribe('nameChanged', (data) => {
                    this.userData.firstname = data.firstname;
                    this.userData.lastname = data.lastname;
                });
                this.eventsService.subscribe('pictureChanged', (data) => {
                    this.userData.profileimage = data.profileimage;
                    this.profileImage = src_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].profileImageUrl + this.userData.profileimage;
                    console.log("this.profileImage 2 : ", this.profileImage);
                    setTimeout(() => {
                        this.cdr.detectChanges();
                    }, 1000);
                });
                setTimeout(() => {
                    this.userData.firstname = localStorage.getItem('firstname');
                    this.userData.lastname = localStorage.getItem('lastname');
                    this.userData.email = localStorage.getItem('email');
                    this.userData.profileimage = localStorage.getItem('profileimage');
                    if (this.userData.profileimage) {
                        this.profileImage = src_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].profileImageUrl + this.userData.profileimage;
                        console.log("this.userData.profileImage : ", this.userData.profileImage);
                        console.log("this.profileImage 3 : ", this.profileImage);
                    }
                    setTimeout(() => {
                        this.cdr.detectChanges();
                    }, 1000);
                }, 1000);
            });
            // this.importContactList();
            // this.checkPermission()
            const loginUser = localStorage.getItem('user_id');
            console.log("loginUser : ", loginUser);
            if (loginUser) {
                this.navCtrl.navigateRoot('/tabs');
            }
            else {
                this.navCtrl.navigateRoot('');
            }
            // if (loginUser) {
            // this.checkWelcomeScreen(loginUser);
            // }
            // if (!loginUser) {
            //   await this.initDeepLinking();
            // } else {
            //   this.checkWelcomeScreen(loginUser);
            // }
        });
    }
    checkWelcomeScreen(loginUser = null) {
        const firstTimeAppOpened = localStorage.getItem('firstTime');
        if (firstTimeAppOpened) {
            this.navCtrl.navigateRoot('welcome-screen');
        }
        else {
            if (loginUser) {
                this.navCtrl.navigateRoot('/tabs');
            }
            else {
                this.navCtrl.navigateRoot('');
            }
        }
    }
    initDeepLinking() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.platform.is('cordova')) {
                yield this.initDeepLinkingBranchio();
            }
            else {
                yield this.initDeepLinkingWeb();
            }
        });
    }
    initDeepLinkingWeb() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const userId = this.platform.getQueryParam('$userId') ||
                this.platform.getQueryParam('userId') ||
                this.platform.getQueryParam('%24userId');
            this.navCtrl.navigateRoot('');
            console.log('Parameter', userId);
        });
    }
    initDeepLinkingBranchio() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const branchIo = window['Branch'];
                if (branchIo) {
                    const data = yield branchIo.initSession();
                    console.log(data);
                    if (data.userId !== undefined) {
                        console.log('Parameter', data.userId);
                        // this.userService.inviteId = data.userId;
                        this.navCtrl.navigateRoot('register');
                    }
                    else {
                        // this.checkWelcomeScreen();
                        this.navCtrl.navigateRoot('/login');
                    }
                }
            }
            catch (err) {
                console.error(err);
            }
        });
    }
    importContact() {
        this.router.navigate(['tabs/import-contacts']);
        this.menu.close();
    }
    contactsTable() {
        this.router.navigate(['tabs/contact-table']);
        this.menu.close();
    }
    sendBroadcast() {
        this.router.navigate(['tabs/send-broadcast']);
        this.menu.close();
    }
    sendSmsBroadcast() {
        this.router.navigate(['tabs/send-smsbroadcast']);
        this.menu.close();
    }
    goToEditProfile() {
        this.router.navigate(['tabs/edit-profile']);
        this.menu.close();
    }
    member() {
        this.router.navigate(['tabs/member']);
        this.menu.close();
    }
    contactGroups() {
        this.router.navigate(['tabs/contact-groups']);
        this.menu.close();
    }
    video() {
        this.router.navigate(['testing']);
        this.menu.close();
    }
    successModel() {
        this.router.navigate(['success-model']);
        this.menu.close();
    }
    openFirst() {
        this.menu.enable(true, 'first');
        this.menu.open('first');
    }
    openEnd() {
        this.menu.open('end');
    }
    openCustom() {
        this.menu.enable(true, 'custom');
        this.menu.open('custom');
    }
    ionViewWillEnter() {
        this.menu.enable(true);
    }
    ionViewDidLeave() {
        this.menu.enable(true);
    }
    logout() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            localStorage.clear();
            yield this.menu.enable(true);
            this.navCtrl.navigateRoot('/');
            // this.fcm.getToken().then(token => {
            //   localStorage.setItem('device_token', token);
            //   console.log('This is to test ');
            //   console.log(token);
            // });
            // this.initializeApp();
        });
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"] },
    { type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"] },
    { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"] },
    { type: _services_alert_service__WEBPACK_IMPORTED_MODULE_9__["AlertService"] },
    { type: _services_events_service__WEBPACK_IMPORTED_MODULE_10__["EventsService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ChangeDetectorRef"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_11__["Storage"] },
    { type: cordova_plugin_fcm_with_dependecy_updated_ionic_ngx__WEBPACK_IMPORTED_MODULE_12__["FCM"] }
];
AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AppComponent);

// ionic cordova plugin add cordova-plugin-facebook-connect --variable APP_ID="1636116319932991" --variable APP_NAME="Nash Consulting"


/***/ }),

/***/ "VzVu":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\n  <ion-menu side=\"start\" menuId=\"custom\" contentId=\"main\" class=\"my-custom-menu\">\n    <ion-header>\n      <ion-toolbar class=\"user-profile\">\n        <ion-item margin-bottom class=\"input\">\n          <ion-avatar slot=\"start\" class=\"user-avatar\">\n            <img [src]=\"profileImage\">\n          </ion-avatar>\n          <ion-label>\n            <ion-text color=\"secondary\">\n              <h1><strong>{{userData.firstname}} {{userData.lastname}}</strong></h1>\n            </ion-text>\n            <ion-text color=\"secondary\">\n              <h3 class=\"ion-text-wrap\">\n                {{userData.email}}\n              </h3>\n            </ion-text>\n            <!-- <ion-menu-toggle class=\"mto\" auto-hide=\"false\"> -->\n            <a class=\"text08\" tappable (click)=\"goToEditProfile()\">\n              <ion-text color=\"secondary\">\n                <ion-icon name=\"person-circle-outline\"></ion-icon>\n                <strong>Edit profile</strong>\n              </ion-text>\n            </a>\n            <ion-text color=\"secondary\"> | </ion-text>\n            <a class=\"text08\" tappable (click)=\"logout()\">\n              <ion-text color=\"secondary\">\n                <ion-icon name=\"log-out\"></ion-icon>\n                <strong>logout</strong>\n              </ion-text>\n            </a>\n            <!-- </ion-menu-toggle> -->\n          </ion-label>\n        </ion-item>\n      </ion-toolbar>\n    </ion-header>\n    <ion-content>\n      <ion-list>\n        <ion-item (click)=\"member()\">Employees</ion-item>\n        <ion-item (click)=\"importContact()\">Import contacts</ion-item>\n        <ion-item (click)=\"contactsTable()\">Contacts</ion-item>\n        <ion-item (click)=\"sendBroadcast()\">Send Email Broadcast</ion-item>\n        <ion-item (click)=\"sendSmsBroadcast()\">Send SMS Broadcast</ion-item>\n        <ion-item (click)=\"contactGroups()\">Contact Groups</ion-item>\n        <!-- <ion-item (click)=\"successModel()\">Success Model</ion-item> -->\n        <!-- <ion-item (click)=\"video()\">Video</ion-item> -->\n      </ion-list>\n    </ion-content>\n  </ion-menu>\n  <ion-router-outlet id=\"main\"></ion-router-outlet>\n</ion-app>");

/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/network/ngx */ "kwrG");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "54vc");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "VYYF");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/login.service */ "EFyh");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "B7Rs");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "a/9d");
/* harmony import */ var _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/contacts/ngx */ "TzAO");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-quill */ "CzEO");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "GGTb");
/* harmony import */ var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ionic-native/google-plus/ngx */ "up+p");
/* harmony import */ var _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ionic-native/device/ngx */ "xS7M");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ionic-native/file/ngx */ "FAH8");
/* harmony import */ var _ionic_native_document_viewer_ngx__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @ionic-native/document-viewer/ngx */ "LfQc");
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ng2-pdf-viewer */ "IkSl");
/* harmony import */ var _ionic_native_diagnostic_ngx__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @ionic-native/diagnostic/ngx */ "mtRb");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @ionic/storage */ "e8h1");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "Bfh1");
/* harmony import */ var cordova_plugin_fcm_with_dependecy_updated_ionic_ngx__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! cordova-plugin-fcm-with-dependecy-updated/ionic/ngx */ "lOSq");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./modules/shared/shared.module */ "FpXt");


























// import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic";


let AppModule = class AppModule {
};
AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"],
        ],
        entryComponents: [],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["BrowserModule"],
            _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_27__["SharedModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["IonicModule"].forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_10__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_24__["IonicStorageModule"].forRoot(),
            ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_22__["PdfViewerModule"],
            ngx_quill__WEBPACK_IMPORTED_MODULE_16__["QuillModule"].forRoot({
                customOptions: [{
                        import: 'formats/font',
                        whitelist: ['mirza', 'roboto', 'aref', 'serif', 'sansserif', 'monospace']
                    }]
            }),
        ],
        providers: [
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__["StatusBar"],
            _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_7__["SplashScreen"],
            _services_login_service__WEBPACK_IMPORTED_MODULE_12__["LoginService"],
            _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_6__["Network"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_14__["Camera"],
            _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_13__["FileTransfer"],
            _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_15__["Contacts"],
            _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_17__["Facebook"],
            _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_18__["GooglePlus"],
            _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_19__["Device"],
            cordova_plugin_fcm_with_dependecy_updated_ionic_ngx__WEBPACK_IMPORTED_MODULE_26__["FCM"],
            _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_20__["File"],
            _ionic_native_document_viewer_ngx__WEBPACK_IMPORTED_MODULE_21__["DocumentViewer"],
            _ionic_native_diagnostic_ngx__WEBPACK_IMPORTED_MODULE_23__["Diagnostic"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_25__["Geolocation"],
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["IonicRouteStrategy"] },
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["CUSTOM_ELEMENTS_SCHEMA"]]
    })
], AppModule);

// cordova plugin add cordova-plugin-googleplus@8.5.1 --save --variable REVERSED_CLIENT_ID=com.googleusercontent.apps.584704282410-peo3u9r6to27b0r46imes0o1jo90idl2


/***/ }),

/***/ "kLfG":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet.entry.js": [
		"dUtr",
		"common",
		0
	],
	"./ion-alert.entry.js": [
		"Q8AI",
		"common",
		1
	],
	"./ion-app_8.entry.js": [
		"hgI1",
		"common",
		2
	],
	"./ion-avatar_3.entry.js": [
		"CfoV",
		"common",
		3
	],
	"./ion-back-button.entry.js": [
		"Nt02",
		"common",
		4
	],
	"./ion-backdrop.entry.js": [
		"Q2Bp",
		5
	],
	"./ion-button_2.entry.js": [
		"0Pbj",
		"common",
		6
	],
	"./ion-card_5.entry.js": [
		"ydQj",
		"common",
		7
	],
	"./ion-checkbox.entry.js": [
		"4fMi",
		"common",
		8
	],
	"./ion-chip.entry.js": [
		"czK9",
		"common",
		9
	],
	"./ion-col_3.entry.js": [
		"/CAe",
		10
	],
	"./ion-datetime_3.entry.js": [
		"WgF3",
		"common",
		11
	],
	"./ion-fab_3.entry.js": [
		"uQcF",
		"common",
		12
	],
	"./ion-img.entry.js": [
		"wHD8",
		13
	],
	"./ion-infinite-scroll_2.entry.js": [
		"2lz6",
		14
	],
	"./ion-input.entry.js": [
		"ercB",
		"common",
		15
	],
	"./ion-item-option_3.entry.js": [
		"MGMP",
		"common",
		16
	],
	"./ion-item_8.entry.js": [
		"9bur",
		"common",
		17
	],
	"./ion-loading.entry.js": [
		"cABk",
		"common",
		18
	],
	"./ion-menu_3.entry.js": [
		"kyFE",
		"common",
		19
	],
	"./ion-modal.entry.js": [
		"TvZU",
		"common",
		20
	],
	"./ion-nav_2.entry.js": [
		"vnES",
		"common",
		21
	],
	"./ion-popover.entry.js": [
		"qCuA",
		"common",
		22
	],
	"./ion-progress-bar.entry.js": [
		"0tOe",
		"common",
		23
	],
	"./ion-radio_2.entry.js": [
		"h11V",
		"common",
		24
	],
	"./ion-range.entry.js": [
		"XGij",
		"common",
		25
	],
	"./ion-refresher_2.entry.js": [
		"nYbb",
		"common",
		26
	],
	"./ion-reorder_2.entry.js": [
		"smMY",
		"common",
		27
	],
	"./ion-ripple-effect.entry.js": [
		"STjf",
		28
	],
	"./ion-route_4.entry.js": [
		"k5eQ",
		"common",
		29
	],
	"./ion-searchbar.entry.js": [
		"OR5t",
		"common",
		30
	],
	"./ion-segment_2.entry.js": [
		"fSgp",
		"common",
		31
	],
	"./ion-select_3.entry.js": [
		"lfGF",
		"common",
		32
	],
	"./ion-slide_2.entry.js": [
		"5xYT",
		33
	],
	"./ion-spinner.entry.js": [
		"nI0H",
		"common",
		34
	],
	"./ion-split-pane.entry.js": [
		"NAQR",
		35
	],
	"./ion-tab-bar_2.entry.js": [
		"knkW",
		"common",
		36
	],
	"./ion-tab_2.entry.js": [
		"TpdJ",
		"common",
		37
	],
	"./ion-text.entry.js": [
		"ISmu",
		"common",
		38
	],
	"./ion-textarea.entry.js": [
		"U7LX",
		"common",
		39
	],
	"./ion-toast.entry.js": [
		"L3sA",
		"common",
		40
	],
	"./ion-toggle.entry.js": [
		"IUOf",
		"common",
		41
	],
	"./ion-virtual-scroll.entry.js": [
		"8Mb5",
		42
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "kLfG";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "lpcq":
/*!*******************************************************!*\
  !*** ./src/app/services/notification-list.service.ts ***!
  \*******************************************************/
/*! exports provided: NotificationListService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationListService", function() { return NotificationListService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");




let NotificationListService = class NotificationListService {
    constructor(http) {
        this.http = http;
        this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    notificationList(formData) {
        return new Promise((resolve, reject) => {
            console.log(this.apiUrl + 'pushnotifications.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'pushnotifications.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
        });
    }
    notificationsCount(formData) {
        return new Promise((resolve, reject) => {
            console.log(this.apiUrl + 'notificationscount.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'notificationscount.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
        });
    }
};
NotificationListService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
NotificationListService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], NotificationListService);



/***/ }),

/***/ "oHuE":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJoZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "riPR":
/*!********************************************!*\
  !*** ./src/app/services/events.service.ts ***!
  \********************************************/
/*! exports provided: EventsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventsService", function() { return EventsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "qCKp");



let EventsService = class EventsService {
    constructor() {
        this.channels = {};
    }
    subscribe(topic, observer) {
        if (!this.channels[topic]) {
            // You can also use ReplaySubject with one concequence
            this.channels[topic] = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        }
        return this.channels[topic].subscribe(observer);
    }
    publish(topic, data) {
        const subject = this.channels[topic];
        if (!subject) {
            // Or you can create a new subject for future subscribers
            return;
        }
        subject.next(data);
    }
    destroy(topic) {
        const subject = this.channels[topic];
        if (!subject) {
            return;
        }
        subject.complete();
        delete this.channels[topic];
    }
};
EventsService.ctorParameters = () => [];
EventsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], EventsService);



/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");



const routes = [
    {
        path: 'tabs',
        loadChildren: () => __webpack_require__.e(/*! import() | tabs-tabs-module */ "tabs-tabs-module").then(__webpack_require__.bind(null, /*! ./tabs/tabs.module */ "hO9l")).then(m => m.TabsPageModule)
    },
    // { path: '', redirectTo: 'Auth/login', pathMatch: 'full' },
    {
        path: '',
        loadChildren: () => Promise.all(/*! import() | pages-Auth-login-login-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-Auth-login-login-module")]).then(__webpack_require__.bind(null, /*! ./pages/Auth/login/login.module */ "loDh")).then(m => m.LoginPageModule)
    },
    {
        path: 'register',
        loadChildren: () => Promise.all(/*! import() | pages-Auth-register-register-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-Auth-register-register-module")]).then(__webpack_require__.bind(null, /*! ./pages/Auth/register/register.module */ "/oMm")).then(m => m.RegisterPageModule)
    },
    {
        path: 'tabs/edit-profile',
        loadChildren: () => Promise.all(/*! import() | pages-edit-profile-edit-profile-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-edit-profile-edit-profile-module")]).then(__webpack_require__.bind(null, /*! ./pages/edit-profile/edit-profile.module */ "k578")).then(m => m.EditProfilePageModule)
    },
    {
        path: 'tabs/select-group',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-select-group-select-group-module */ "pages-select-group-select-group-module").then(__webpack_require__.bind(null, /*! ./pages/select-group/select-group.module */ "5Rhe")).then(m => m.SelectGroupPageModule)
    },
    {
        path: 'send-email-contact',
        loadChildren: () => Promise.all(/*! import() | pages-send-email-contact-send-email-contact-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-send-email-contact-send-email-contact-module")]).then(__webpack_require__.bind(null, /*! ./pages/send-email-contact/send-email-contact.module */ "VuRq")).then(m => m.SendEmailContactPageModule)
    },
    {
        path: 'send-sms-contact',
        loadChildren: () => Promise.all(/*! import() | pages-send-sms-contact-send-sms-contact-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-send-sms-contact-send-sms-contact-module")]).then(__webpack_require__.bind(null, /*! ./pages/send-sms-contact/send-sms-contact.module */ "fp8Y")).then(m => m.SendSmsContactPageModule)
    },
    {
        path: 'model',
        loadChildren: () => Promise.all(/*! import() | pages-model-model-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-model-model-module")]).then(__webpack_require__.bind(null, /*! ./pages/model/model.module */ "8q6H")).then(m => m.ModelPageModule)
    },
    {
        path: 'pdf-viewer',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-pdf-viewer-pdf-viewer-module */ "pages-pdf-viewer-pdf-viewer-module").then(__webpack_require__.bind(null, /*! ./pages/pdf-viewer/pdf-viewer.module */ "yV6V")).then(m => m.PdfViewerPageModule)
    },
    {
        path: 'testing',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-testing-testing-module */ "pages-testing-testing-module").then(__webpack_require__.bind(null, /*! ./pages/testing/testing.module */ "WC81")).then(m => m.TestingPageModule)
    },
    {
        path: 'success-model',
        loadChildren: () => Promise.all(/*! import() | pages-success-model-success-model-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-success-model-success-model-module")]).then(__webpack_require__.bind(null, /*! ./pages/success-model/success-model.module */ "sHii")).then(m => m.SuccessModelPageModule)
    },
    {
        path: 'chat',
        loadChildren: () => Promise.all(/*! import() | pages-chat-chat-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-chat-chat-module")]).then(__webpack_require__.bind(null, /*! ./pages/chat/chat.module */ "EdD2")).then(m => m.ChatPageModule)
    },
    {
        path: 'video-player',
        loadChildren: () => Promise.all(/*! import() | pages-video-player-video-player-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-video-player-video-player-module")]).then(__webpack_require__.bind(null, /*! ./pages/video-player/video-player.module */ "GCVo")).then(m => m.VideoPlayerPageModule)
    },
    {
        path: 'twillio-model',
        loadChildren: () => Promise.all(/*! import() | pages-twillio-model-twillio-model-module */[__webpack_require__.e("default~pages-twillio-model-twillio-model-module~tab4-tab4-module"), __webpack_require__.e("pages-twillio-model-twillio-model-module")]).then(__webpack_require__.bind(null, /*! ./pages/twillio-model/twillio-model.module */ "KQjX")).then(m => m.TwillioModelPageModule)
    },
    {
        path: 'image-model',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-image-model-image-model-module */ "pages-image-model-image-model-module").then(__webpack_require__.bind(null, /*! ./pages/image-model/image-model.module */ "+6nL")).then(m => m.ImageModelPageModule)
    },
    {
        path: 'iframe',
        loadChildren: () => Promise.all(/*! import() | pages-iframe-iframe-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-iframe-iframe-module")]).then(__webpack_require__.bind(null, /*! ./pages/iframe/iframe.module */ "jK97")).then(m => m.IframePageModule)
    },
    {
        path: 'forgot-password',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-forgot-password-forgot-password-module */ "pages-forgot-password-forgot-password-module").then(__webpack_require__.bind(null, /*! ./pages/forgot-password/forgot-password.module */ "7CEM")).then(m => m.ForgotPasswordPageModule)
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "ynWL":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: #25223b;\n}\n:host ion-list.list-md {\n  padding: 0;\n  background: transparent;\n}\n:host ion-list ion-item {\n  color: white;\n}\n:host .list-ios {\n  background: transparent;\n}\n.input {\n  --border-style: none;\n}\n.item-divider {\n  --background: var(--ion-background-primary);\n  border-bottom: 2px solid var(--ion-item-border-color, var(--ion-border-color, var(--ion-color-step-150, #e4cf14)));\n}\nh1 {\n  font-size: 16px;\n  white-space: normal;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL2FwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNDLHFCQUFBO0FBQUw7QUFHUTtFQUNJLFVBQUE7RUFDQSx1QkFBQTtBQURaO0FBR1E7RUFDSSxZQUFBO0FBRFo7QUFJSTtFQUNJLHVCQUFBO0FBRlI7QUFLQTtFQUNJLG9CQUFBO0FBRko7QUFJQTtFQUNJLDJDQUFBO0VBQ0Esa0hBQUE7QUFESjtBQUdBO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0FBQUoiLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuICAgIGlvbi1jb250ZW50IHtcbiAgICAgLS1iYWNrZ3JvdW5kOiAjMjUyMjNiO1xuICAgIH1cbiAgICBpb24tbGlzdCB7XG4gICAgICAgICYubGlzdC1tZCB7XG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgICAgIH1cbiAgICAgICAgaW9uLWl0ZW0ge1xuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICB9XG4gICAgfVxuICAgIC5saXN0LWlvc3tcbiAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgfVxufVxuLmlucHV0e1xuICAgIC0tYm9yZGVyLXN0eWxlOiBub25lO1xufVxuLml0ZW0tZGl2aWRlcntcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLXByaW1hcnkpO1xuICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCB2YXIoLS1pb24taXRlbS1ib3JkZXItY29sb3IsdmFyKC0taW9uLWJvcmRlci1jb2xvcix2YXIoLS1pb24tY29sb3Itc3RlcC0xNTAscmdiKDIyOCwgMjA3LCAyMCkpKSk7XG59XG5oMSB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIHdoaXRlLXNwYWNlOiBub3JtYWw7XG59Il19 */");

/***/ }),

/***/ "yxfS":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/header/header.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header *ngIf=\"checkPage == false\">\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">{{title}}</ion-title>\n    <ion-buttons class=\"notification-button \" slot=\"end\" (click)=\"notification_fun()\">\n      <ion-badge *ngIf=\"count != 0\" color=\"danger\" class=\"notifications-badge\">{{count}}</ion-badge>\n      <ion-icon slot=\"icon-only\" name=\"notifications\"></ion-icon>\n    </ion-buttons>\n    <ion-icon slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" (click)=\"home()\" routerLink=\"/tabs\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n<ion-header *ngIf=\"checkPage == true\">\n  <ion-toolbar class=\"header_clas\">\n    <img slot=\"start\" src=\"assets/logo/logo_c.png\" />\n    <!-- <img slot=\"start\" src=\"assets/logo/logo.png\" /> -->\n    <ion-title color=\"secondary\">{{title}}</ion-title>\n    <ion-buttons class=\"notification-button \" slot=\"end\" (click)=\"notification_fun()\">\n      <ion-badge *ngIf=\"count != 0\" color=\"danger\" class=\"notifications-badge\">{{count}}</ion-badge>\n      <ion-icon slot=\"icon-only\" name=\"notifications\"></ion-icon>\n    </ion-buttons>\n    <ion-icon slot=\"end\" size=\"large\" name=\"home\" (click)=\"home()\" routerLink=\"/tabs\"></ion-icon>\n  </ion-toolbar>\n</ion-header>");

/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "a3Wg");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map