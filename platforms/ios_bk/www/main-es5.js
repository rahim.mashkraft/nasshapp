(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! /Users/customer/Documents/ionic/NashApp/src/main.ts */
      "zUnb");
      /***/
    },

    /***/
    1:
    /*!************************!*\
      !*** canvas (ignored) ***!
      \************************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    2:
    /*!********************!*\
      !*** fs (ignored) ***!
      \********************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    "2MiI":
    /*!*******************************************************!*\
      !*** ./src/app/components/header/header.component.ts ***!
      \*******************************************************/

    /*! exports provided: HeaderComponent */

    /***/
    function MiI(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderComponent", function () {
        return HeaderComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_header_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./header.component.html */
      "yxfS");
      /* harmony import */


      var _header_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./header.component.scss */
      "oHuE");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_services_events_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/events.service */
      "riPR");
      /* harmony import */


      var src_app_services_notification_list_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/notification-list.service */
      "lpcq");

      var HeaderComponent = /*#__PURE__*/function () {
        function HeaderComponent(router, navCtrl, eventsService, notificationListService) {
          var _this = this;

          _classCallCheck(this, HeaderComponent);

          this.router = router;
          this.navCtrl = navCtrl;
          this.eventsService = eventsService;
          this.notificationListService = notificationListService;
          this.title = "";
          this.notification = false;
          this.count = 0;
          this.checkPage = false;
          this.eventsService.subscribe('notification_count', function (data) {
            _this.notificationsCount();
          });
        }

        _createClass(HeaderComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.notificationsCount();
          }
        }, {
          key: "notificationsCount",
          value: function notificationsCount() {
            var _this2 = this;

            // this.loaderService.showLoader()
            var formData = new FormData();
            formData.append("aid", localStorage.getItem('user_id'));
            this.notificationListService.notificationsCount(formData).then(function (res) {
              console.log(res);

              if (res.status !== false) {
                _this2.count = res.count;
                console.log("this.notification_count : ", _this2.count); // this.loaderService.hideLoader()
              } else {// this.loaderService.hideLoader()
                }
            });
          }
        }, {
          key: "hasNotification",
          get: function get() {
            return this.notification ? true : false;
          }
        }, {
          key: "notification_fun",
          value: function notification_fun() {
            this.router.navigate(['tabs/notification']);
          }
        }, {
          key: "home",
          value: function home() {
            this.navCtrl.navigateRoot('/tabs/tab1');
          }
        }, {
          key: "back",
          value: function back() {
            this.navCtrl.back();
          }
        }]);

        return HeaderComponent;
      }();

      HeaderComponent.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: src_app_services_events_service__WEBPACK_IMPORTED_MODULE_6__["EventsService"]
        }, {
          type: src_app_services_notification_list_service__WEBPACK_IMPORTED_MODULE_7__["NotificationListService"]
        }];
      };

      HeaderComponent.propDecorators = {
        title: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }],
        notification: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }],
        count: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }],
        checkPage: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }]
      };
      HeaderComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'header',
        template: _raw_loader_header_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_header_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], HeaderComponent);
      /***/
    },

    /***/
    3:
    /*!**********************!*\
      !*** zlib (ignored) ***!
      \**********************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    "3LUQ":
    /*!*******************************************!*\
      !*** ./src/app/services/alert.service.ts ***!
      \*******************************************/

    /*! exports provided: AlertService */

    /***/
    function LUQ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AlertService", function () {
        return AlertService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var AlertService = /*#__PURE__*/function () {
        function AlertService(alertController, loadingCtrl, navCtrl) {
          _classCallCheck(this, AlertService);

          this.alertController = alertController;
          this.loadingCtrl = loadingCtrl;
          this.navCtrl = navCtrl;
          this.isCheck = false;
          this.isCheck = false;
        }

        _createClass(AlertService, [{
          key: "presentAlertError",
          value: function presentAlertError(error) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var alert;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.alertController.create({
                        cssClass: 'alertCustom',
                        header: 'Warning',
                        // subHeader: 'Subtitle',
                        message: error,
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context.sent;
                      _context.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "showPwdSuccessAlert",
          value: function showPwdSuccessAlert(email) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var alert;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.alertController.create({
                        header: 'SUCCESS',
                        backdropDismiss: false,
                        message: "\n        <p> Password reset email has been sent to ".concat(email, ".</p>\n        <p> The email contains a URL that you will need to visit to confirm your email. Once you've confirmed\n        your email you may reset your password.</p>\n      "),
                        buttons: [{
                          text: 'Close',
                          role: 'cancel'
                        }]
                      });

                    case 2:
                      alert = _context2.sent;
                      alert.present();

                    case 4:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "presentAlertSuccess",
          value: function presentAlertSuccess() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var alert;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.alertController.create({
                        header: 'Success',
                        cssClass: 'alertCustom',
                        // subHeader: 'You have Successfully ',
                        message: 'You have successfully logged In ',
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context3.sent;
                      _context3.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "forgotPass",
          value: function forgotPass() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var _this3 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.alertController.create({
                        header: 'Forgot Password?',
                        message: 'Enter you email address to send a reset link password.',
                        inputs: [{
                          name: 'email',
                          type: 'email',
                          placeholder: 'Email'
                        }],
                        buttons: [{
                          text: 'Cancel',
                          role: 'cancel',
                          cssClass: 'secondary',
                          handler: function handler() {// console.log('Confirm Cancel');
                          }
                        }, {
                          text: 'Confirm',
                          handler: function handler(alertData) {
                            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                while (1) {
                                  switch (_context4.prev = _context4.next) {
                                    case 0:
                                      this.showPwdLoader(alertData);

                                    case 1:
                                    case "end":
                                      return _context4.stop();
                                  }
                                }
                              }, _callee4, this);
                            }));
                          }
                        }]
                      });

                    case 2:
                      alert = _context5.sent;
                      _context5.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "showPwdLoader",
          value: function showPwdLoader(alertData) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var _this4 = this;

              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.loadingCtrl.create({
                        spinner: 'circles',
                        animated: true,
                        showBackdrop: true,
                        translucent: true,
                        mode: 'ios',
                        message: 'Please wait...'
                      }).then(function (load) {
                        _this4.loaderObj = load;

                        _this4.loaderObj.present().then(function () {});
                      });

                    case 2:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "showDisableAlert",
          value: function showDisableAlert() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var alert;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      _context7.next = 2;
                      return this.alertController.create({
                        backdropDismiss: false,
                        message: 'Please finish workout to continue.',
                        buttons: [{
                          text: 'Okay',
                          role: 'cancel'
                        }]
                      });

                    case 2:
                      alert = _context7.sent;
                      _context7.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }, {
          key: "presentNetworkAlert",
          value: function presentNetworkAlert() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
              var alert;
              return regeneratorRuntime.wrap(function _callee8$(_context8) {
                while (1) {
                  switch (_context8.prev = _context8.next) {
                    case 0:
                      _context8.next = 2;
                      return this.alertController.create({
                        header: 'Warning',
                        cssClass: 'alertCustom',
                        // subHeader: 'You have Successfully ',
                        message: ' No Internet Available ',
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context8.sent;
                      _context8.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context8.stop();
                  }
                }
              }, _callee8, this);
            }));
          }
        }, {
          key: "generalAlert",
          value: function generalAlert(message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
              var alert;
              return regeneratorRuntime.wrap(function _callee9$(_context9) {
                while (1) {
                  switch (_context9.prev = _context9.next) {
                    case 0:
                      _context9.next = 2;
                      return this.alertController.create({
                        header: 'Warning',
                        cssClass: 'alertCustom',
                        // subHeader: 'You have Successfully ',
                        message: message,
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context9.sent;
                      _context9.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context9.stop();
                  }
                }
              }, _callee9, this);
            }));
          }
        }, {
          key: "contactList",
          value: function contactList(message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
              var _this5 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee11$(_context11) {
                while (1) {
                  switch (_context11.prev = _context11.next) {
                    case 0:
                      _context11.next = 2;
                      return this.alertController.create({
                        // header: 'Warning',
                        cssClass: 'alertCustom',
                        // subHeader: 'You have Successfully ',
                        message: message,
                        buttons: [{
                          text: 'OK',
                          handler: function handler() {
                            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
                              return regeneratorRuntime.wrap(function _callee10$(_context10) {
                                while (1) {
                                  switch (_context10.prev = _context10.next) {
                                    case 0:
                                      this.isCheck = true;

                                    case 1:
                                    case "end":
                                      return _context10.stop();
                                  }
                                }
                              }, _callee10, this);
                            }));
                          }
                        }]
                      });

                    case 2:
                      alert = _context11.sent;
                      _context11.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context11.stop();
                  }
                }
              }, _callee11, this);
            }));
          }
        }, {
          key: "showAlertForQuiz",
          value: function showAlertForQuiz(quiz) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
              var _this6 = this;

              var firstTime, attemptsRemaining, buttons, alert;
              return regeneratorRuntime.wrap(function _callee12$(_context12) {
                while (1) {
                  switch (_context12.prev = _context12.next) {
                    case 0:
                      firstTime = false;
                      attemptsRemaining = 0;

                      if (quiz.is_attempt == 0) {
                        firstTime = true;
                      }

                      if (quiz.redo && quiz.is_attempt < quiz.redo_attempt + 1) {
                        attemptsRemaining = quiz.redo_attempt + 1 - quiz.is_attempt;
                      }

                      buttons = [];
                      buttons.push({
                        text: 'Dismiss',
                        role: 'cancel'
                      });

                      if (firstTime || attemptsRemaining > 0) {
                        buttons.push({
                          text: 'Continue',
                          handler: function handler() {
                            _this6.navCtrl.navigateForward(['home-results/quiz/start-quiz/' + quiz.id]);
                          }
                        });
                      }

                      if (firstTime && attemptsRemaining == 0) {
                        attemptsRemaining = 1;
                      }

                      _context12.next = 10;
                      return this.alertController.create({
                        header: quiz.title,
                        message: "\n        <p>".concat(quiz.description, "</p>\n        <p class=\"font-weight-600\">Details</p>\n        <ul class=\"text-align-left\">\n          <li>Time: ").concat(this.transform(quiz.quiztime), "</li>\n          <li>Total points: ").concat(quiz.totalscore, "</li>\n          <li>Passing points: ").concat(quiz.passcore, "</li>\n          <li>Attempt remaining: ").concat(attemptsRemaining, "</li>\n        </ul>\n      "),
                        buttons: buttons
                      });

                    case 10:
                      alert = _context12.sent;
                      alert.present();

                    case 12:
                    case "end":
                      return _context12.stop();
                  }
                }
              }, _callee12, this);
            }));
          }
        }, {
          key: "transform",
          value: function transform(time) {
            if (time >= 60) {
              var timeInMinutes = Math.floor(time / 60);
              var hours = Math.floor(time / 3600);
              var minutes = timeInMinutes;

              if (minutes >= 60) {
                minutes = timeInMinutes % 60;
              }

              return (hours > 0 ? '0' + hours + 'hr ' : '') + (minutes < 10 ? '0' + minutes : minutes) + 'mins ' + (time - timeInMinutes * 60 < 10 ? '0' + (time - timeInMinutes * 60) : time - timeInMinutes * 60) + 'secs';
            } else {
              return (time < 10 ? '00: 0' + time : '00:' + time) + 'secs';
            }
          }
        }, {
          key: "showResults",
          value: function showResults(results) {
            var header = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'Time is up!';
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
              var _this7 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee13$(_context13) {
                while (1) {
                  switch (_context13.prev = _context13.next) {
                    case 0:
                      _context13.next = 2;
                      return this.alertController.create({
                        header: header,
                        backdropDismiss: false,
                        message: "\n        <p> Results of your quiz are:</p>\n        <ul class=\"text-align-left\">\n          <li>Points earned: ".concat(results.totalpoints, " out of ").concat(results.totalscore, "</li>\n          <li>Passing score: ").concat(results.passcore, "</li>\n          <li>Time elapsed: ").concat(results.total_time, "</li>\n        </ul>\n      "),
                        buttons: [{
                          text: 'Close',
                          handler: function handler() {
                            _this7.navCtrl.navigateBack(['home-results/quiz']);
                          }
                        }]
                      });

                    case 2:
                      alert = _context13.sent;
                      alert.present();

                    case 4:
                    case "end":
                      return _context13.stop();
                  }
                }
              }, _callee13, this);
            }));
          }
        }]);

        return AlertService;
      }();

      AlertService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      AlertService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AlertService);
      /***/
    },

    /***/
    4:
    /*!**********************!*\
      !*** http (ignored) ***!
      \**********************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    5:
    /*!***********************!*\
      !*** https (ignored) ***!
      \***********************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    6:
    /*!*********************!*\
      !*** url (ignored) ***!
      \*********************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    "AytR":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function AytR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      }); // This file can be replaced during build by using the `fileReplacements` array.
      // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
      // The list of file replacements can be found in `angular.json`.


      var environment = {
        production: false,
        // imageUrl: 'http:',
        // apiUrl: 'http://umrtariq.prospectingmagic.com/admin/appapi/',
        // profileImageUrl: 'http://umrtariq.prospectingmagic.com/'
        imageUrl: 'https:',
        apiUrl: 'https://mindandheartuniversity.com/admin/appapi/',
        profileImageUrl: 'https://mindandheartuniversity.com/',
        auth_token: '4d409766604ad82ba5eeb96077c977c9',
        downloadurl: 'https://mindandheartuniversity.com/admin/'
      };
      /*
       * For easier debugging in development mode, you can import the following file
       * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
       *
       * This import should be commented out in production mode because it will have a negative impact
       * on performance if an error is thrown.
       */
      // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

      /***/
    },

    /***/
    "EFyh":
    /*!*******************************************!*\
      !*** ./src/app/services/login.service.ts ***!
      \*******************************************/

    /*! exports provided: LoginService */

    /***/
    function EFyh(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginService", function () {
        return LoginService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");

      var LoginService = /*#__PURE__*/function () {
        function LoginService(alertController, http) {
          _classCallCheck(this, LoginService);

          this.alertController = alertController;
          this.http = http;
          this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].apiUrl;
        }

        _createClass(LoginService, [{
          key: "handleError",
          value: function handleError() {
            var _this8 = this;

            var operation = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'operation';
            var result = arguments.length > 1 ? arguments[1] : undefined;
            return function (error) {
              // TODO: send the error to remote logging infrastructure
              console.error(error); // log to console instead
              // TODO: better job of transforming error for user consumption

              _this8.log("".concat(operation, " failed: ").concat(error.message)); // Let the app keep running by returning an empty result.


              return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(result);
            };
          }
          /** Log a HeroService message with the MessageService */

        }, {
          key: "log",
          value: function log(message) {
            this.presentAlertError(message);
            console.log(message);
          }
        }, {
          key: "presentAlertError",
          value: function presentAlertError(error) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee14() {
              var alert;
              return regeneratorRuntime.wrap(function _callee14$(_context14) {
                while (1) {
                  switch (_context14.prev = _context14.next) {
                    case 0:
                      _context14.next = 2;
                      return this.alertController.create({
                        cssClass: 'alertCustom',
                        header: 'Error',
                        // subHeader: 'Subtitle',
                        message: error,
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context14.sent;
                      _context14.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context14.stop();
                  }
                }
              }, _callee14, this);
            }));
          }
        }, {
          key: "login",
          value: function login(formData) {
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].auth_token);
            return this.http.post(this.apiUrl + 'login.php', formData).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (res) {
              return res;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError('login', [])));
          }
        }]);

        return LoginService;
      }();

      LoginService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
        }];
      };

      LoginService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoginService);
      /***/
    },

    /***/
    "FpXt":
    /*!*************************************************!*\
      !*** ./src/app/modules/shared/shared.module.ts ***!
      \*************************************************/

    /*! exports provided: SharedModule */

    /***/
    function FpXt(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SharedModule", function () {
        return SharedModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/components/header/header.component */
      "2MiI");

      var SharedModule = function SharedModule() {
        _classCallCheck(this, SharedModule);
      };

      SharedModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_3__["HeaderComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
        exports: [src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_3__["HeaderComponent"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
      })], SharedModule);
      /***/
    },

    /***/
    "Sy1n":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function Sy1n(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./app.component.html */
      "VzVu");
      /* harmony import */


      var _app_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app.component.scss */
      "ynWL");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic-native/splash-screen/ngx */
      "54vc");
      /* harmony import */


      var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/status-bar/ngx */
      "VYYF");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");
      /* harmony import */


      var _services_alert_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./services/alert.service */
      "3LUQ");
      /* harmony import */


      var _services_events_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./services/events.service */
      "riPR");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @ionic/storage */
      "e8h1");
      /* harmony import */


      var cordova_plugin_fcm_with_dependecy_updated_ionic_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! cordova-plugin-fcm-with-dependecy-updated/ionic/ngx */
      "lOSq");

      var w = window;

      var AppComponent = /*#__PURE__*/function () {
        function AppComponent(platform, splashScreen, statusBar, navCtrl, menu, alertService, eventsService, cdr, router, storage, fcm) {
          var _this9 = this;

          _classCallCheck(this, AppComponent);

          this.platform = platform;
          this.splashScreen = splashScreen;
          this.statusBar = statusBar;
          this.navCtrl = navCtrl;
          this.menu = menu;
          this.alertService = alertService;
          this.eventsService = eventsService;
          this.cdr = cdr;
          this.router = router;
          this.storage = storage;
          this.fcm = fcm;
          this.onlineOffline = navigator.onLine;
          this.userData = {
            aid: '',
            firstname: '',
            lastname: '',
            email: '',
            profileimage: '',
            address: '',
            city: '',
            country: '',
            phone: '',
            username: '',
            zipcode: '',
            state: ''
          };
          this.setupFCM();
          this.environment = src_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"];
          this.initializeApp();
          window.addEventListener('offline', function () {
            _this9.alertService.presentNetworkAlert();
          });
        }

        _createClass(AppComponent, [{
          key: "setupFCM",
          value: function setupFCM() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee15() {
              var _this10 = this;

              return regeneratorRuntime.wrap(function _callee15$(_context15) {
                while (1) {
                  switch (_context15.prev = _context15.next) {
                    case 0:
                      _context15.next = 2;
                      return this.platform.ready();

                    case 2:
                      console.log('FCM setup started');

                      if (this.platform.is('cordova')) {
                        _context15.next = 5;
                        break;
                      }

                      return _context15.abrupt("return");

                    case 5:
                      console.log('In cordova platform');
                      console.log('Subscribing to token updates');
                      this.fcm.onTokenRefresh().subscribe(function (newToken) {
                        _this10.token = newToken;
                        console.log('onTokenRefresh received event with: ', newToken);
                      });
                      console.log('Subscribing to new notifications');
                      this.fcm.onNotification().subscribe(function (payload) {
                        _this10.pushPayload = payload;
                        console.log('onNotification received event with: ', payload);
                      });
                      _context15.next = 12;
                      return this.fcm.requestPushPermission();

                    case 12:
                      this.hasPermission = _context15.sent;
                      console.log('requestPushPermission result: ', this.hasPermission);
                      _context15.next = 16;
                      return this.fcm.getToken();

                    case 16:
                      this.token = _context15.sent;
                      console.log('getToken result: ', this.token);
                      _context15.next = 20;
                      return this.fcm.getInitialPushPayload();

                    case 20:
                      this.pushPayload = _context15.sent;
                      console.log('getInitialPushPayload result: ', this.pushPayload);

                    case 22:
                    case "end":
                      return _context15.stop();
                  }
                }
              }, _callee15, this);
            }));
          }
        }, {
          key: "pushPayloadString",
          get: function get() {
            return JSON.stringify(this.pushPayload, null, 4);
          }
        }, {
          key: "initializeApp",
          value: function initializeApp() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee16() {
              var _this11 = this;

              var loginUser;
              return regeneratorRuntime.wrap(function _callee16$(_context16) {
                while (1) {
                  switch (_context16.prev = _context16.next) {
                    case 0:
                      if (!navigator.onLine) {
                        this.alertService.presentNetworkAlert();
                      }

                      this.platform.ready().then(function () {
                        _this11.statusBar.styleDefault();

                        setTimeout(function () {
                          _this11.splashScreen.hide();
                        }, 2000); // let thisObj = this;
                        // thisObj.fcm.requestPushPermission().then(() => {
                        //   console.log("requestPushPermission true: ")
                        //   // alert("suecce ")
                        // }).catch((e) => {
                        //   console.log("requestPushPermission : ",e)
                        //   // alert("errr "+e)
                        // })

                        _this11.eventsService.subscribe('userLogged', function (data) {
                          console.log("userLogged : ", data);
                          _this11.userData.firstname = data.firstname;
                          _this11.userData.lastname = data.lastname;
                          _this11.userData.email = data.email;
                          _this11.userData.profileimage = data.profileimage;
                          _this11.profileImage = src_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].profileImageUrl + _this11.userData.profileimage;
                          console.log("this.userData.profileImage : ", _this11.userData.profileImage);
                          console.log("this.profileImage 1 : ", _this11.profileImage);
                          setTimeout(function () {
                            _this11.cdr.detectChanges();
                          }, 1000);
                        });

                        _this11.eventsService.subscribe('nameChanged', function (data) {
                          _this11.userData.firstname = data.firstname;
                          _this11.userData.lastname = data.lastname;
                        });

                        _this11.eventsService.subscribe('pictureChanged', function (data) {
                          _this11.userData.profileimage = data.profileimage;
                          _this11.profileImage = src_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].profileImageUrl + _this11.userData.profileimage;
                          console.log("this.profileImage 2 : ", _this11.profileImage);
                          setTimeout(function () {
                            _this11.cdr.detectChanges();
                          }, 1000);
                        });

                        setTimeout(function () {
                          _this11.userData.firstname = localStorage.getItem('firstname');
                          _this11.userData.lastname = localStorage.getItem('lastname');
                          _this11.userData.email = localStorage.getItem('email');
                          _this11.userData.profileimage = localStorage.getItem('profileimage');

                          if (_this11.userData.profileimage) {
                            _this11.profileImage = src_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].profileImageUrl + _this11.userData.profileimage;
                            console.log("this.userData.profileImage : ", _this11.userData.profileImage);
                            console.log("this.profileImage 3 : ", _this11.profileImage);
                          }

                          setTimeout(function () {
                            _this11.cdr.detectChanges();
                          }, 1000);
                        }, 1000);
                      }); // this.importContactList();
                      // this.checkPermission()

                      loginUser = localStorage.getItem('user_id');
                      console.log("loginUser : ", loginUser);

                      if (loginUser) {
                        this.navCtrl.navigateRoot('/tabs');
                      } else {
                        this.navCtrl.navigateRoot('');
                      } // if (loginUser) {
                      // this.checkWelcomeScreen(loginUser);
                      // }
                      // if (!loginUser) {
                      //   await this.initDeepLinking();
                      // } else {
                      //   this.checkWelcomeScreen(loginUser);
                      // }


                    case 5:
                    case "end":
                      return _context16.stop();
                  }
                }
              }, _callee16, this);
            }));
          }
        }, {
          key: "checkWelcomeScreen",
          value: function checkWelcomeScreen() {
            var loginUser = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            var firstTimeAppOpened = localStorage.getItem('firstTime');

            if (firstTimeAppOpened) {
              this.navCtrl.navigateRoot('welcome-screen');
            } else {
              if (loginUser) {
                this.navCtrl.navigateRoot('/tabs');
              } else {
                this.navCtrl.navigateRoot('');
              }
            }
          }
        }, {
          key: "initDeepLinking",
          value: function initDeepLinking() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee17() {
              return regeneratorRuntime.wrap(function _callee17$(_context17) {
                while (1) {
                  switch (_context17.prev = _context17.next) {
                    case 0:
                      if (!this.platform.is('cordova')) {
                        _context17.next = 5;
                        break;
                      }

                      _context17.next = 3;
                      return this.initDeepLinkingBranchio();

                    case 3:
                      _context17.next = 7;
                      break;

                    case 5:
                      _context17.next = 7;
                      return this.initDeepLinkingWeb();

                    case 7:
                    case "end":
                      return _context17.stop();
                  }
                }
              }, _callee17, this);
            }));
          }
        }, {
          key: "initDeepLinkingWeb",
          value: function initDeepLinkingWeb() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee18() {
              var userId;
              return regeneratorRuntime.wrap(function _callee18$(_context18) {
                while (1) {
                  switch (_context18.prev = _context18.next) {
                    case 0:
                      userId = this.platform.getQueryParam('$userId') || this.platform.getQueryParam('userId') || this.platform.getQueryParam('%24userId');
                      this.navCtrl.navigateRoot('');
                      console.log('Parameter', userId);

                    case 3:
                    case "end":
                      return _context18.stop();
                  }
                }
              }, _callee18, this);
            }));
          }
        }, {
          key: "initDeepLinkingBranchio",
          value: function initDeepLinkingBranchio() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee19() {
              var branchIo, data;
              return regeneratorRuntime.wrap(function _callee19$(_context19) {
                while (1) {
                  switch (_context19.prev = _context19.next) {
                    case 0:
                      _context19.prev = 0;
                      branchIo = window['Branch'];

                      if (!branchIo) {
                        _context19.next = 8;
                        break;
                      }

                      _context19.next = 5;
                      return branchIo.initSession();

                    case 5:
                      data = _context19.sent;
                      console.log(data);

                      if (data.userId !== undefined) {
                        console.log('Parameter', data.userId); // this.userService.inviteId = data.userId;

                        this.navCtrl.navigateRoot('register');
                      } else {
                        // this.checkWelcomeScreen();
                        this.navCtrl.navigateRoot('/login');
                      }

                    case 8:
                      _context19.next = 13;
                      break;

                    case 10:
                      _context19.prev = 10;
                      _context19.t0 = _context19["catch"](0);
                      console.error(_context19.t0);

                    case 13:
                    case "end":
                      return _context19.stop();
                  }
                }
              }, _callee19, this, [[0, 10]]);
            }));
          }
        }, {
          key: "importContact",
          value: function importContact() {
            this.router.navigate(['tabs/import-contacts']);
            this.menu.close();
          }
        }, {
          key: "contactsTable",
          value: function contactsTable() {
            this.router.navigate(['tabs/contact-table']);
            this.menu.close();
          }
        }, {
          key: "sendBroadcast",
          value: function sendBroadcast() {
            this.router.navigate(['tabs/send-broadcast']);
            this.menu.close();
          }
        }, {
          key: "sendSmsBroadcast",
          value: function sendSmsBroadcast() {
            this.router.navigate(['tabs/send-smsbroadcast']);
            this.menu.close();
          }
        }, {
          key: "goToEditProfile",
          value: function goToEditProfile() {
            this.router.navigate(['tabs/edit-profile']);
            this.menu.close();
          }
        }, {
          key: "member",
          value: function member() {
            this.router.navigate(['tabs/member']);
            this.menu.close();
          }
        }, {
          key: "contactGroups",
          value: function contactGroups() {
            this.router.navigate(['tabs/contact-groups']);
            this.menu.close();
          }
        }, {
          key: "video",
          value: function video() {
            this.router.navigate(['testing']);
            this.menu.close();
          }
        }, {
          key: "successModel",
          value: function successModel() {
            this.router.navigate(['success-model']);
            this.menu.close();
          }
        }, {
          key: "openFirst",
          value: function openFirst() {
            this.menu.enable(true, 'first');
            this.menu.open('first');
          }
        }, {
          key: "openEnd",
          value: function openEnd() {
            this.menu.open('end');
          }
        }, {
          key: "openCustom",
          value: function openCustom() {
            this.menu.enable(true, 'custom');
            this.menu.open('custom');
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.menu.enable(true);
          }
        }, {
          key: "ionViewDidLeave",
          value: function ionViewDidLeave() {
            this.menu.enable(true);
          }
        }, {
          key: "logout",
          value: function logout() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee20() {
              return regeneratorRuntime.wrap(function _callee20$(_context20) {
                while (1) {
                  switch (_context20.prev = _context20.next) {
                    case 0:
                      localStorage.clear();
                      _context20.next = 3;
                      return this.menu.enable(true);

                    case 3:
                      this.navCtrl.navigateRoot('/'); // this.fcm.getToken().then(token => {
                      //   localStorage.setItem('device_token', token);
                      //   console.log('This is to test ');
                      //   console.log(token);
                      // });
                      // this.initializeApp();

                    case 4:
                    case "end":
                      return _context20.stop();
                  }
                }
              }, _callee20, this);
            }));
          }
        }]);

        return AppComponent;
      }();

      AppComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]
        }, {
          type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"]
        }, {
          type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"]
        }, {
          type: _services_alert_service__WEBPACK_IMPORTED_MODULE_9__["AlertService"]
        }, {
          type: _services_events_service__WEBPACK_IMPORTED_MODULE_10__["EventsService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ChangeDetectorRef"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_11__["Storage"]
        }, {
          type: cordova_plugin_fcm_with_dependecy_updated_ionic_ngx__WEBPACK_IMPORTED_MODULE_12__["FCM"]
        }];
      };

      AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], AppComponent); // ionic cordova plugin add cordova-plugin-facebook-connect --variable APP_ID="1636116319932991" --variable APP_NAME="Nash Consulting"

      /***/
    },

    /***/
    "VzVu":
    /*!**************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
      \**************************************************************************/

    /*! exports provided: default */

    /***/
    function VzVu(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-app>\n  <ion-menu side=\"start\" menuId=\"custom\" contentId=\"main\" class=\"my-custom-menu\">\n    <ion-header>\n      <ion-toolbar class=\"user-profile\">\n        <ion-item margin-bottom class=\"input\">\n          <ion-avatar slot=\"start\" class=\"user-avatar\">\n            <img [src]=\"profileImage\">\n          </ion-avatar>\n          <ion-label>\n            <ion-text color=\"secondary\">\n              <h1><strong>{{userData.firstname}} {{userData.lastname}}</strong></h1>\n            </ion-text>\n            <ion-text color=\"secondary\">\n              <h3 class=\"ion-text-wrap\">\n                {{userData.email}}\n              </h3>\n            </ion-text>\n            <!-- <ion-menu-toggle class=\"mto\" auto-hide=\"false\"> -->\n            <a class=\"text08\" tappable (click)=\"goToEditProfile()\">\n              <ion-text color=\"secondary\">\n                <ion-icon name=\"person-circle-outline\"></ion-icon>\n                <strong>Edit profile</strong>\n              </ion-text>\n            </a>\n            <ion-text color=\"secondary\"> | </ion-text>\n            <a class=\"text08\" tappable (click)=\"logout()\">\n              <ion-text color=\"secondary\">\n                <ion-icon name=\"log-out\"></ion-icon>\n                <strong>logout</strong>\n              </ion-text>\n            </a>\n            <!-- </ion-menu-toggle> -->\n          </ion-label>\n        </ion-item>\n      </ion-toolbar>\n    </ion-header>\n    <ion-content>\n      <ion-list>\n        <ion-item (click)=\"member()\">Employees</ion-item>\n        <ion-item (click)=\"importContact()\">Import contacts</ion-item>\n        <ion-item (click)=\"contactsTable()\">Contacts</ion-item>\n        <ion-item (click)=\"sendBroadcast()\">Send Email Broadcast</ion-item>\n        <ion-item (click)=\"sendSmsBroadcast()\">Send SMS Broadcast</ion-item>\n        <ion-item (click)=\"contactGroups()\">Contact Groups</ion-item>\n        <!-- <ion-item (click)=\"successModel()\">Success Model</ion-item> -->\n        <!-- <ion-item (click)=\"video()\">Video</ion-item> -->\n      </ion-list>\n    </ion-content>\n  </ion-menu>\n  <ion-router-outlet id=\"main\"></ion-router-outlet>\n</ion-app>";
      /***/
    },

    /***/
    "ZAI4":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: AppModule */

    /***/
    function ZAI4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/network/ngx */
      "kwrG");
      /* harmony import */


      var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/splash-screen/ngx */
      "54vc");
      /* harmony import */


      var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/status-bar/ngx */
      "VYYF");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _app_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./app-routing.module */
      "vY5A");
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./app.component */
      "Sy1n");
      /* harmony import */


      var _services_login_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ./services/login.service */
      "EFyh");
      /* harmony import */


      var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @ionic-native/file-transfer/ngx */
      "B7Rs");
      /* harmony import */


      var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! @ionic-native/camera/ngx */
      "a/9d");
      /* harmony import */


      var _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! @ionic-native/contacts/ngx */
      "TzAO");
      /* harmony import */


      var ngx_quill__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! ngx-quill */
      "CzEO");
      /* harmony import */


      var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! @ionic-native/facebook/ngx */
      "GGTb");
      /* harmony import */


      var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! @ionic-native/google-plus/ngx */
      "up+p");
      /* harmony import */


      var _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
      /*! @ionic-native/device/ngx */
      "xS7M");
      /* harmony import */


      var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
      /*! @ionic-native/file/ngx */
      "FAH8");
      /* harmony import */


      var _ionic_native_document_viewer_ngx__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
      /*! @ionic-native/document-viewer/ngx */
      "LfQc");
      /* harmony import */


      var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
      /*! ng2-pdf-viewer */
      "IkSl");
      /* harmony import */


      var _ionic_native_diagnostic_ngx__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
      /*! @ionic-native/diagnostic/ngx */
      "mtRb");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
      /*! @ionic/storage */
      "e8h1");
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "Bfh1");
      /* harmony import */


      var cordova_plugin_fcm_with_dependecy_updated_ionic_ngx__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(
      /*! cordova-plugin-fcm-with-dependecy-updated/ionic/ngx */
      "lOSq");
      /* harmony import */


      var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(
      /*! ./modules/shared/shared.module */
      "FpXt"); // import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic";


      var AppModule = function AppModule() {
        _classCallCheck(this, AppModule);
      };

      AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]],
        entryComponents: [],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["BrowserModule"], _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_27__["SharedModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["IonicModule"].forRoot(), _app_routing_module__WEBPACK_IMPORTED_MODULE_10__["AppRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_storage__WEBPACK_IMPORTED_MODULE_24__["IonicStorageModule"].forRoot(), ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_22__["PdfViewerModule"], ngx_quill__WEBPACK_IMPORTED_MODULE_16__["QuillModule"].forRoot({
          customOptions: [{
            "import": 'formats/font',
            whitelist: ['mirza', 'roboto', 'aref', 'serif', 'sansserif', 'monospace']
          }]
        })],
        providers: [_ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__["StatusBar"], _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_7__["SplashScreen"], _services_login_service__WEBPACK_IMPORTED_MODULE_12__["LoginService"], _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_6__["Network"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_14__["Camera"], _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_13__["FileTransfer"], _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_15__["Contacts"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_17__["Facebook"], _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_18__["GooglePlus"], _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_19__["Device"], cordova_plugin_fcm_with_dependecy_updated_ionic_ngx__WEBPACK_IMPORTED_MODULE_26__["FCM"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_20__["File"], _ionic_native_document_viewer_ngx__WEBPACK_IMPORTED_MODULE_21__["DocumentViewer"], _ionic_native_diagnostic_ngx__WEBPACK_IMPORTED_MODULE_23__["Diagnostic"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_25__["Geolocation"], {
          provide: _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouteReuseStrategy"],
          useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["IonicRouteStrategy"]
        }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["CUSTOM_ELEMENTS_SCHEMA"]]
      })], AppModule); // cordova plugin add cordova-plugin-googleplus@8.5.1 --save --variable REVERSED_CLIENT_ID=com.googleusercontent.apps.584704282410-peo3u9r6to27b0r46imes0o1jo90idl2

      /***/
    },

    /***/
    "kLfG":
    /*!*****************************************************************************************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
      \*****************************************************************************************************************************************/

    /*! no static exports found */

    /***/
    function kLfG(module, exports, __webpack_require__) {
      var map = {
        "./ion-action-sheet.entry.js": ["dUtr", "common", 0],
        "./ion-alert.entry.js": ["Q8AI", "common", 1],
        "./ion-app_8.entry.js": ["hgI1", "common", 2],
        "./ion-avatar_3.entry.js": ["CfoV", "common", 3],
        "./ion-back-button.entry.js": ["Nt02", "common", 4],
        "./ion-backdrop.entry.js": ["Q2Bp", 5],
        "./ion-button_2.entry.js": ["0Pbj", "common", 6],
        "./ion-card_5.entry.js": ["ydQj", "common", 7],
        "./ion-checkbox.entry.js": ["4fMi", "common", 8],
        "./ion-chip.entry.js": ["czK9", "common", 9],
        "./ion-col_3.entry.js": ["/CAe", 10],
        "./ion-datetime_3.entry.js": ["WgF3", "common", 11],
        "./ion-fab_3.entry.js": ["uQcF", "common", 12],
        "./ion-img.entry.js": ["wHD8", 13],
        "./ion-infinite-scroll_2.entry.js": ["2lz6", 14],
        "./ion-input.entry.js": ["ercB", "common", 15],
        "./ion-item-option_3.entry.js": ["MGMP", "common", 16],
        "./ion-item_8.entry.js": ["9bur", "common", 17],
        "./ion-loading.entry.js": ["cABk", "common", 18],
        "./ion-menu_3.entry.js": ["kyFE", "common", 19],
        "./ion-modal.entry.js": ["TvZU", "common", 20],
        "./ion-nav_2.entry.js": ["vnES", "common", 21],
        "./ion-popover.entry.js": ["qCuA", "common", 22],
        "./ion-progress-bar.entry.js": ["0tOe", "common", 23],
        "./ion-radio_2.entry.js": ["h11V", "common", 24],
        "./ion-range.entry.js": ["XGij", "common", 25],
        "./ion-refresher_2.entry.js": ["nYbb", "common", 26],
        "./ion-reorder_2.entry.js": ["smMY", "common", 27],
        "./ion-ripple-effect.entry.js": ["STjf", 28],
        "./ion-route_4.entry.js": ["k5eQ", "common", 29],
        "./ion-searchbar.entry.js": ["OR5t", "common", 30],
        "./ion-segment_2.entry.js": ["fSgp", "common", 31],
        "./ion-select_3.entry.js": ["lfGF", "common", 32],
        "./ion-slide_2.entry.js": ["5xYT", 33],
        "./ion-spinner.entry.js": ["nI0H", "common", 34],
        "./ion-split-pane.entry.js": ["NAQR", 35],
        "./ion-tab-bar_2.entry.js": ["knkW", "common", 36],
        "./ion-tab_2.entry.js": ["TpdJ", "common", 37],
        "./ion-text.entry.js": ["ISmu", "common", 38],
        "./ion-textarea.entry.js": ["U7LX", "common", 39],
        "./ion-toast.entry.js": ["L3sA", "common", 40],
        "./ion-toggle.entry.js": ["IUOf", "common", 41],
        "./ion-virtual-scroll.entry.js": ["8Mb5", 42]
      };

      function webpackAsyncContext(req) {
        if (!__webpack_require__.o(map, req)) {
          return Promise.resolve().then(function () {
            var e = new Error("Cannot find module '" + req + "'");
            e.code = 'MODULE_NOT_FOUND';
            throw e;
          });
        }

        var ids = map[req],
            id = ids[0];
        return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function () {
          return __webpack_require__(id);
        });
      }

      webpackAsyncContext.keys = function webpackAsyncContextKeys() {
        return Object.keys(map);
      };

      webpackAsyncContext.id = "kLfG";
      module.exports = webpackAsyncContext;
      /***/
    },

    /***/
    "lpcq":
    /*!*******************************************************!*\
      !*** ./src/app/services/notification-list.service.ts ***!
      \*******************************************************/

    /*! exports provided: NotificationListService */

    /***/
    function lpcq(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationListService", function () {
        return NotificationListService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");

      var NotificationListService = /*#__PURE__*/function () {
        function NotificationListService(http) {
          _classCallCheck(this, NotificationListService);

          this.http = http;
          this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
        }

        _createClass(NotificationListService, [{
          key: "notificationList",
          value: function notificationList(formData) {
            var _this12 = this;

            return new Promise(function (resolve, reject) {
              console.log(_this12.apiUrl + 'pushnotifications.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this12.http.post(_this12.apiUrl + 'pushnotifications.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }, {
          key: "notificationsCount",
          value: function notificationsCount(formData) {
            var _this13 = this;

            return new Promise(function (resolve, reject) {
              console.log(_this13.apiUrl + 'notificationscount.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this13.http.post(_this13.apiUrl + 'notificationscount.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              });
            });
          }
        }]);

        return NotificationListService;
      }();

      NotificationListService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      };

      NotificationListService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], NotificationListService);
      /***/
    },

    /***/
    "oHuE":
    /*!*********************************************************!*\
      !*** ./src/app/components/header/header.component.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function oHuE(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJoZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "riPR":
    /*!********************************************!*\
      !*** ./src/app/services/events.service.ts ***!
      \********************************************/

    /*! exports provided: EventsService */

    /***/
    function riPR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EventsService", function () {
        return EventsService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "qCKp");

      var EventsService = /*#__PURE__*/function () {
        function EventsService() {
          _classCallCheck(this, EventsService);

          this.channels = {};
        }

        _createClass(EventsService, [{
          key: "subscribe",
          value: function subscribe(topic, observer) {
            if (!this.channels[topic]) {
              // You can also use ReplaySubject with one concequence
              this.channels[topic] = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
            }

            return this.channels[topic].subscribe(observer);
          }
        }, {
          key: "publish",
          value: function publish(topic, data) {
            var subject = this.channels[topic];

            if (!subject) {
              // Or you can create a new subject for future subscribers
              return;
            }

            subject.next(data);
          }
        }, {
          key: "destroy",
          value: function destroy(topic) {
            var subject = this.channels[topic];

            if (!subject) {
              return;
            }

            subject.complete();
            delete this.channels[topic];
          }
        }]);

        return EventsService;
      }();

      EventsService.ctorParameters = function () {
        return [];
      };

      EventsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], EventsService);
      /***/
    },

    /***/
    "vY5A":
    /*!***************************************!*\
      !*** ./src/app/app-routing.module.ts ***!
      \***************************************/

    /*! exports provided: AppRoutingModule */

    /***/
    function vY5A(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
        return AppRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var routes = [{
        path: 'tabs',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | tabs-tabs-module */
          "tabs-tabs-module").then(__webpack_require__.bind(null,
          /*! ./tabs/tabs.module */
          "hO9l")).then(function (m) {
            return m.TabsPageModule;
          });
        }
      }, // { path: '', redirectTo: 'Auth/login', pathMatch: 'full' },
      {
        path: '',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-Auth-login-login-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-Auth-login-login-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/Auth/login/login.module */
          "loDh")).then(function (m) {
            return m.LoginPageModule;
          });
        }
      }, {
        path: 'register',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-Auth-register-register-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-Auth-register-register-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/Auth/register/register.module */
          "/oMm")).then(function (m) {
            return m.RegisterPageModule;
          });
        }
      }, {
        path: 'tabs/edit-profile',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-edit-profile-edit-profile-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-edit-profile-edit-profile-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/edit-profile/edit-profile.module */
          "k578")).then(function (m) {
            return m.EditProfilePageModule;
          });
        }
      }, {
        path: 'tabs/select-group',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-select-group-select-group-module */
          "pages-select-group-select-group-module").then(__webpack_require__.bind(null,
          /*! ./pages/select-group/select-group.module */
          "5Rhe")).then(function (m) {
            return m.SelectGroupPageModule;
          });
        }
      }, {
        path: 'send-email-contact',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-send-email-contact-send-email-contact-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-send-email-contact-send-email-contact-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/send-email-contact/send-email-contact.module */
          "VuRq")).then(function (m) {
            return m.SendEmailContactPageModule;
          });
        }
      }, {
        path: 'send-sms-contact',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-send-sms-contact-send-sms-contact-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-send-sms-contact-send-sms-contact-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/send-sms-contact/send-sms-contact.module */
          "fp8Y")).then(function (m) {
            return m.SendSmsContactPageModule;
          });
        }
      }, {
        path: 'model',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-model-model-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-model-model-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/model/model.module */
          "8q6H")).then(function (m) {
            return m.ModelPageModule;
          });
        }
      }, {
        path: 'pdf-viewer',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-pdf-viewer-pdf-viewer-module */
          "pages-pdf-viewer-pdf-viewer-module").then(__webpack_require__.bind(null,
          /*! ./pages/pdf-viewer/pdf-viewer.module */
          "yV6V")).then(function (m) {
            return m.PdfViewerPageModule;
          });
        }
      }, {
        path: 'testing',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-testing-testing-module */
          "pages-testing-testing-module").then(__webpack_require__.bind(null,
          /*! ./pages/testing/testing.module */
          "WC81")).then(function (m) {
            return m.TestingPageModule;
          });
        }
      }, {
        path: 'success-model',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-success-model-success-model-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-success-model-success-model-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/success-model/success-model.module */
          "sHii")).then(function (m) {
            return m.SuccessModelPageModule;
          });
        }
      }, {
        path: 'chat',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-chat-chat-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-chat-chat-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/chat/chat.module */
          "EdD2")).then(function (m) {
            return m.ChatPageModule;
          });
        }
      }, {
        path: 'video-player',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-video-player-video-player-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-video-player-video-player-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/video-player/video-player.module */
          "GCVo")).then(function (m) {
            return m.VideoPlayerPageModule;
          });
        }
      }, {
        path: 'twillio-model',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-twillio-model-twillio-model-module */
          [__webpack_require__.e("default~pages-twillio-model-twillio-model-module~tab4-tab4-module"), __webpack_require__.e("pages-twillio-model-twillio-model-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/twillio-model/twillio-model.module */
          "KQjX")).then(function (m) {
            return m.TwillioModelPageModule;
          });
        }
      }, {
        path: 'image-model',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-image-model-image-model-module */
          "pages-image-model-image-model-module").then(__webpack_require__.bind(null,
          /*! ./pages/image-model/image-model.module */
          "+6nL")).then(function (m) {
            return m.ImageModelPageModule;
          });
        }
      }, {
        path: 'iframe',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-iframe-iframe-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-iframe-iframe-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/iframe/iframe.module */
          "jK97")).then(function (m) {
            return m.IframePageModule;
          });
        }
      }, {
        path: 'forgot-password',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-forgot-password-forgot-password-module */
          "pages-forgot-password-forgot-password-module").then(__webpack_require__.bind(null,
          /*! ./pages/forgot-password/forgot-password.module */
          "7CEM")).then(function (m) {
            return m.ForgotPasswordPageModule;
          });
        }
      }];

      var AppRoutingModule = function AppRoutingModule() {
        _classCallCheck(this, AppRoutingModule);
      };

      AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, {
          preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"]
        })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AppRoutingModule);
      /***/
    },

    /***/
    "ynWL":
    /*!************************************!*\
      !*** ./src/app/app.component.scss ***!
      \************************************/

    /*! exports provided: default */

    /***/
    function ynWL(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host ion-content {\n  --background: #25223b;\n}\n:host ion-list.list-md {\n  padding: 0;\n  background: transparent;\n}\n:host ion-list ion-item {\n  color: white;\n}\n:host .list-ios {\n  background: transparent;\n}\n.input {\n  --border-style: none;\n}\n.item-divider {\n  --background: var(--ion-background-primary);\n  border-bottom: 2px solid var(--ion-item-border-color, var(--ion-border-color, var(--ion-color-step-150, #e4cf14)));\n}\nh1 {\n  font-size: 16px;\n  white-space: normal;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL2FwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNDLHFCQUFBO0FBQUw7QUFHUTtFQUNJLFVBQUE7RUFDQSx1QkFBQTtBQURaO0FBR1E7RUFDSSxZQUFBO0FBRFo7QUFJSTtFQUNJLHVCQUFBO0FBRlI7QUFLQTtFQUNJLG9CQUFBO0FBRko7QUFJQTtFQUNJLDJDQUFBO0VBQ0Esa0hBQUE7QUFESjtBQUdBO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0FBQUoiLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuICAgIGlvbi1jb250ZW50IHtcbiAgICAgLS1iYWNrZ3JvdW5kOiAjMjUyMjNiO1xuICAgIH1cbiAgICBpb24tbGlzdCB7XG4gICAgICAgICYubGlzdC1tZCB7XG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgICAgIH1cbiAgICAgICAgaW9uLWl0ZW0ge1xuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICB9XG4gICAgfVxuICAgIC5saXN0LWlvc3tcbiAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgfVxufVxuLmlucHV0e1xuICAgIC0tYm9yZGVyLXN0eWxlOiBub25lO1xufVxuLml0ZW0tZGl2aWRlcntcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLXByaW1hcnkpO1xuICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCB2YXIoLS1pb24taXRlbS1ib3JkZXItY29sb3IsdmFyKC0taW9uLWJvcmRlci1jb2xvcix2YXIoLS1pb24tY29sb3Itc3RlcC0xNTAscmdiKDIyOCwgMjA3LCAyMCkpKSk7XG59XG5oMSB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIHdoaXRlLXNwYWNlOiBub3JtYWw7XG59Il19 */";
      /***/
    },

    /***/
    "yxfS":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/header/header.component.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function yxfS(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header *ngIf=\"checkPage == false\">\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">{{title}}</ion-title>\n    <ion-buttons class=\"notification-button \" slot=\"end\" (click)=\"notification_fun()\">\n      <ion-badge *ngIf=\"count != 0\" color=\"danger\" class=\"notifications-badge\">{{count}}</ion-badge>\n      <ion-icon slot=\"icon-only\" name=\"notifications\"></ion-icon>\n    </ion-buttons>\n    <ion-icon slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" (click)=\"home()\" routerLink=\"/tabs\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n<ion-header *ngIf=\"checkPage == true\">\n  <ion-toolbar class=\"header_clas\">\n    <img slot=\"start\" src=\"assets/logo/logo_c.png\" />\n    <!-- <img slot=\"start\" src=\"assets/logo/logo.png\" /> -->\n    <ion-title color=\"secondary\">{{title}}</ion-title>\n    <ion-buttons class=\"notification-button \" slot=\"end\" (click)=\"notification_fun()\">\n      <ion-badge *ngIf=\"count != 0\" color=\"danger\" class=\"notifications-badge\">{{count}}</ion-badge>\n      <ion-icon slot=\"icon-only\" name=\"notifications\"></ion-icon>\n    </ion-buttons>\n    <ion-icon slot=\"end\" size=\"large\" name=\"home\" (click)=\"home()\" routerLink=\"/tabs\"></ion-icon>\n  </ion-toolbar>\n</ion-header>";
      /***/
    },

    /***/
    "zUnb":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function zUnb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/platform-browser-dynamic */
      "a3Wg");
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app/app.module */
      "ZAI4");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./environments/environment */
      "AytR");

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
      }

      Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
        return console.log(err);
      });
      /***/
    },

    /***/
    "zn8P":
    /*!******************************************************!*\
      !*** ./$$_lazy_route_resource lazy namespace object ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function zn8P(module, exports) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      module.exports = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = "zn8P";
      /***/
    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.js.map