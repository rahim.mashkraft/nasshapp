(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-video-video-module"],{

/***/ "1odT":
/*!*********************************************!*\
  !*** ./src/app/pages/video/video.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-button {\n  --border-radius: 0px;\n}\n\nion-icon {\n  font-size: 24px;\n}\n\n.controlIcon {\n  font-size: 20px;\n}\n\nion-range {\n  height: 20px;\n  width: 90%;\n  position: absolute;\n  top: 0;\n  margin: 0 0 0 5% !important;\n  padding: 0 !important;\n  --knob-background: #b5c827;\n  --bar-height: 8px;\n  --bar-background: white;\n  --bar-background-active: #b5c827;\n  --bar-border-radius: 6px;\n  --knob-size: 30px;\n}\n\n.duration-container {\n  height: 20px;\n  position: absolute;\n  top: 20px;\n  width: 100%;\n  margin: 0 !important;\n  padding: 0 !important;\n  display: flex;\n  flex-direction: row;\n  font-size: 16px;\n  line-height: 20px;\n  font-weight: 700;\n  color: #b5c827;\n}\n\n#albumPhoto {\n  position: absolute;\n  width: 100%;\n  background: black;\n  display: flex;\n  align-items: center;\n  flex-direction: column;\n  justify-content: center;\n}\n\n#image {\n  position: absolute;\n}\n\n.player-controls-middle {\n  position: absolute;\n  top: 50%;\n  width: 100%;\n  transform: translateY(-82%);\n}\n\n.center {\n  justify-content: center;\n}\n\n.pos_play_btn {\n  position: absolute;\n  top: 23%;\n  width: 100%;\n  transform: translateY(-82%);\n}\n\n.time-display-content {\n  pointer-events: none;\n}\n\n.cbox, .vbox, .center {\n  display: flex;\n  align-items: center;\n}\n\n.time-first, .time-second, .time-delimiter {\n  color: #ffffff;\n}\n\n.time-delimiter {\n  opacity: 0.7;\n  margin: 0 4px;\n}\n\nimg {\n  height: 50px;\n}\n\n.opacity2 {\n  opacity: 1;\n}\n\n.opity {\n  opacity: 0;\n}\n\n.ion_toolbar_time_seekbar {\n  top: -48px;\n  --background: transparent;\n  --background: #7979795c;\n}\n\n.ion_toolbar_play_pause {\n  top: -92px;\n  --background: transparent;\n}\n\n.ion_toolbar_time_seekbar1 {\n  top: -92px;\n  --background: transparent;\n  --background: #7979795c;\n}\n\n.skip_ads_button {\n  border: 2px solid #fff;\n  background: #909090;\n  margin-right: -5px;\n}\n\n.skip_ads_button ion-button {\n  --color: white;\n}\n\nion-grid {\n  padding: 0px;\n}\n\nion-grid ion-toolbar {\n  padding: 0px;\n}\n\nvideo {\n  width: 100%;\n  min-height: 300px;\n  height: 300px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\nion-range {\n  --knob-background: #606060;\n  --bar-height: 6px;\n  --bar-background: white;\n  --bar-background-active: #606060;\n  --bar-border-radius: 6px;\n  --knob-size: 16px;\n  height: 34px;\n}\n\nion-input {\n  border: 1px solid #606060;\n  --padding-start: 16px;\n}\n\nion-textarea {\n  border: 1px solid #606060;\n  --padding-start: 16px;\n}\n\n.quiz_button {\n  --background: #EB6A42;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3ZpZGVvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9CQUFBO0FBQ0o7O0FBR0E7RUFDSSxlQUFBO0FBQUo7O0FBSUU7RUFDRSxlQUFBO0FBREo7O0FBSUU7RUFDRSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLDJCQUFBO0VBQ0EscUJBQUE7RUFDQSwwQkFBQTtFQUNBLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQ0FBQTtFQUNBLHdCQUFBO0VBQ0EsaUJBQUE7QUFESjs7QUFJRTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQkFBQTtFQUNBLGFBQUE7RUFBZSxtQkFBQTtFQUNmLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQUFKOztBQUdFO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0FBQUo7O0FBR0U7RUFDRSxrQkFBQTtBQUFKOztBQUtBO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtFQUVFLDJCQUFBO0FBRk47O0FBSUE7RUFHRSx1QkFBQTtBQURGOztBQUlBO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtFQUVFLDJCQUFBO0FBREo7O0FBR0E7RUFDRSxvQkFBQTtBQUFGOztBQUVBO0VBR0UsYUFBQTtFQUdBLG1CQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxjQUFBO0FBRUY7O0FBQUE7RUFDRSxZQUFBO0VBQ0EsYUFBQTtBQUdGOztBQURBO0VBQ0UsWUFBQTtBQUlGOztBQUZBO0VBQ0UsVUFBQTtBQUtGOztBQUhBO0VBQ0UsVUFBQTtBQU1GOztBQUpBO0VBRUUsVUFBQTtFQUNBLHlCQUFBO0VBQ0EsdUJBQUE7QUFNRjs7QUFKQTtFQUVFLFVBQUE7RUFDQSx5QkFBQTtBQU1GOztBQUhBO0VBRUUsVUFBQTtFQUNBLHlCQUFBO0VBQ0EsdUJBQUE7QUFLRjs7QUFIQTtFQUNFLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQU1GOztBQUxFO0VBQ0UsY0FBQTtBQU9KOztBQUpBO0VBQ0UsWUFBQTtBQU9GOztBQU5FO0VBQ0UsWUFBQTtBQVFKOztBQUxBO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7QUFRRjs7QUFOQTtFQUNFLDBCQUFBO0VBQ0EsaUJBQUE7RUFDQSx1QkFBQTtFQUNBLGdDQUFBO0VBQ0Esd0JBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUFTRjs7QUFQQTtFQUNFLHlCQUFBO0VBQ0EscUJBQUE7QUFVRjs7QUFSQTtFQUNFLHlCQUFBO0VBQ0EscUJBQUE7QUFXRjs7QUFUQTtFQUNFLHFCQUFBO0FBWUYiLCJmaWxlIjoidmlkZW8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWJ1dHRvbiB7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgLy8gLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cblxuaW9uLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICAvLyBjb2xvcjogd2hpdGU7XG4gIH1cbiAgXG4gIC5jb250cm9sSWNvbiB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICB9XG4gIFxuICBpb24tcmFuZ2Uge1xuICAgIGhlaWdodDogMjBweDtcbiAgICB3aWR0aDogOTAlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgbWFyZ2luOiAwIDAgMCA1JSFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xuICAgIC0ta25vYi1iYWNrZ3JvdW5kOiAjYjVjODI3O1xuICAgIC0tYmFyLWhlaWdodDogOHB4O1xuICAgIC0tYmFyLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgIC0tYmFyLWJhY2tncm91bmQtYWN0aXZlOiAjYjVjODI3O1xuICAgIC0tYmFyLWJvcmRlci1yYWRpdXM6IDZweDtcbiAgICAtLWtub2Itc2l6ZTogMzBweDtcbiAgfVxuICBcbiAgLmR1cmF0aW9uLWNvbnRhaW5lciB7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDIwcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xuICAgIGRpc3BsYXk6IGZsZXg7IGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgY29sb3I6ICNiNWM4Mjc7XG4gIH1cbiAgXG4gICNhbGJ1bVBob3RvIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZDogYmxhY2s7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH1cbiAgXG4gICNpbWFnZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICB9XG5cblxuXG4ucGxheWVyLWNvbnRyb2xzLW1pZGRsZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNTAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC04MiUpO1xufVxuLmNlbnRlciB7XG4gIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5wb3NfcGxheV9idG4ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMjMlO1xuICB3aWR0aDogMTAwJTtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC04MiUpO1xufVxuLnRpbWUtZGlzcGxheS1jb250ZW50IHtcbiAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG59XG4uY2JveCwgLnZib3gsIC5jZW50ZXIge1xuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4udGltZS1maXJzdCwgLnRpbWUtc2Vjb25kLCAudGltZS1kZWxpbWl0ZXIge1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cbi50aW1lLWRlbGltaXRlciB7XG4gIG9wYWNpdHk6IC43O1xuICBtYXJnaW46IDAgNHB4O1xufVxuaW1nIHtcbiAgaGVpZ2h0OiA1MHB4O1xufVxuLm9wYWNpdHkyIHtcbiAgb3BhY2l0eTogMTtcbn1cbi5vcGl0eXtcbiAgb3BhY2l0eTogMDtcbn1cbi5pb25fdG9vbGJhcl90aW1lX3NlZWtiYXIge1xuICAvLyAtLWJhY2tncm91bmQ6IGJsYWNrO1xuICB0b3A6IC00OHB4O1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAtLWJhY2tncm91bmQ6ICM3OTc5Nzk1Yztcbn1cbi5pb25fdG9vbGJhcl9wbGF5X3BhdXNlIHtcbiAgLy8gLS1iYWNrZ3JvdW5kOiBibGFjaztcbiAgdG9wOiAtOTJweDtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgLy8gLS1iYWNrZ3JvdW5kOiAjNzk3OTc5NWM7XG59XG4uaW9uX3Rvb2xiYXJfdGltZV9zZWVrYmFyMSB7XG4gIC8vIC0tYmFja2dyb3VuZDogYmxhY2s7XG4gIHRvcDogLTkycHg7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIC0tYmFja2dyb3VuZDogIzc5Nzk3OTVjO1xufVxuLnNraXBfYWRzX2J1dHRvbiB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNmZmY7XG4gIGJhY2tncm91bmQ6ICM5MDkwOTA7XG4gIG1hcmdpbi1yaWdodDogLTVweDtcbiAgaW9uLWJ1dHRvbiB7XG4gICAgLS1jb2xvcjogd2hpdGU7XG4gIH1cbn1cbmlvbi1ncmlkIHtcbiAgcGFkZGluZzogMHB4O1xuICBpb24tdG9vbGJhciB7XG4gICAgcGFkZGluZzogMHB4O1xuICB9XG59XG52aWRlbyB7XG4gIHdpZHRoOiAxMDAlO1xuICBtaW4taGVpZ2h0OiAzMDBweDtcbiAgaGVpZ2h0OiAzMDBweDtcbiAgb2JqZWN0LWZpdDpjb3Zlcjtcbn1cbmlvbi1yYW5nZSB7XG4gIC0ta25vYi1iYWNrZ3JvdW5kOiAjNjA2MDYwO1xuICAtLWJhci1oZWlnaHQ6IDZweDtcbiAgLS1iYXItYmFja2dyb3VuZDogd2hpdGU7XG4gIC0tYmFyLWJhY2tncm91bmQtYWN0aXZlOiAjNjA2MDYwO1xuICAtLWJhci1ib3JkZXItcmFkaXVzOiA2cHg7XG4gIC0ta25vYi1zaXplOiAxNnB4O1xuICBoZWlnaHQ6IDM0cHg7XG59XG5pb24taW5wdXQge1xuICBib3JkZXI6IDFweCBzb2xpZCAjNjA2MDYwO1xuICAtLXBhZGRpbmctc3RhcnQ6IDE2cHg7XG59XG5pb24tdGV4dGFyZWEge1xuICBib3JkZXI6IDFweCBzb2xpZCAjNjA2MDYwO1xuICAtLXBhZGRpbmctc3RhcnQ6IDE2cHg7XG59XG4ucXVpel9idXR0b24ge1xuICAtLWJhY2tncm91bmQ6ICNFQjZBNDI7XG59Il19 */");

/***/ }),

/***/ "2J3j":
/*!*****************************************************!*\
  !*** ./src/app/pages/video/video-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: VideoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoPageRoutingModule", function() { return VideoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _video_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./video.page */ "i0EM");




const routes = [
    {
        path: '',
        component: _video_page__WEBPACK_IMPORTED_MODULE_3__["VideoPage"]
    },
    {
        path: 'quiz',
        loadChildren: () => Promise.all(/*! import() | pages-quiz-quiz-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-quiz-quiz-module")]).then(__webpack_require__.bind(null, /*! ..//../pages/quiz/quiz.module */ "F2WD")).then(m => m.QuizPageModule)
    },
];
let VideoPageRoutingModule = class VideoPageRoutingModule {
};
VideoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], VideoPageRoutingModule);



/***/ }),

/***/ "5nVJ":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/video/video.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header title=\"{{videotitle}}\"></header>\n<ion-content>\n  <ion-grid style=\"height: 100%\">\n    <!-- <h2 class=\"ion-padding-horizontal\">\n      Step 1: Video\n    </h2> -->\n    <div class=\"ion-text-center\" style=\"height: 300px;\">\n      <!--src=\"{{videoUrl}}\"-->\n\n      <!-- (timeupdate)=\"setCurrentTime()\"  -->\n      <video id=\"video1\" #video1 src=\"{{videourl}}\" (click)=\"hideShow()\"\n      (loadedmetadata)=\"onMetadata($event, video1)\"\n      (timeupdate)=\"setCurrentTime($event, video1)\"\n        playsinline poster=\"{{videoimage}}\">\n      </video>\n      <!-- <img [src]=\"videoimage\" class=\"img-container\"> -->\n      <!-- <video poster=\"{{videoimage}}\" src=\"{{video_link}}\" id=\"video1\" (click)=\"hideShow()\" playsinline allowfullscreen>\n      </video> -->\n      <ion-row class=\"pos_play_btn ion-align-items-center ion-justify-content-center\" *ngIf=\"hide_show_icon\">\n        <ion-col size=\"4\">\n        </ion-col>\n        <ion-col size=\"4\" class=\"text-center\" *ngIf=\"!adsDisabled\">\n          <!-- <img id=\"btnPlay\" *ngIf=\"btnPlay\" value=\"Play\" (click)=\"PlayNow(video1)\" src=\"assets/icon/play.svg\"> -->\n          <img id=\"btnPlay\" *ngIf=\"btnPlay\" value=\"Play\" (click)=\"PlayNow()\" src=\"assets/icon/play.svg\">\n          <img id=\"btnPause\" *ngIf=\"btnPause\" value=\"Pause\" (click)=\"PauseNow()\" src=\"assets/icon/pause.svg\">\n        </ion-col>\n        <ion-col size=\"4\" class=\"text-center\">\n        </ion-col>\n      </ion-row>\n      <ion-toolbar class=\"ion_toolbar_time_seekbar\">\n        <ion-row>\n          <ion-col size=\"3\">\n            <div class=\"time-display-content cbox\" aria-label=\"\">\n              <span class=\"time-first\" aria-label=\"Time elapsed0:01\" role=\"text\" id=\"lblTime\">00:00</span>\n              <span class=\"time-delimiter\" aria-hidden=\"true\">/</span>\n              <span class=\"time-second\" aria-label=\"Time duration4:53\" id=\"lblTime2\" role=\"text\">00:00</span>\n            </div>\n          </ion-col>\n          <ion-col size=\"7\" class=\"text-center\">\n            <!-- <ion-range [disabled]=\"adsDisabled\" [(ngModel)]=\"myValue\" min=\"1000\" max=\"2000\" step=\"any\" snaps=\"true\" id=\"seekbar\" color=\"primary\" (ionChange)=\"ChangeTheTime($event)\"></ion-range> -->\n            <!-- <ion-input type=\"range\" step=\"any\" id=\"seekbar\" (ionChange)=\"ChangeTheTime()\"></ion-input> -->\n            <input type=\"range\" step=\"any\" id=\"seekbar\" onchange=\"ChangeTheTime()\">\n            <!-- //////////////////// the below code for seekbar ////////////      -->\n            <!-- <input type=\"range\" step=\"any\" id=\"seekbar\" onchange=\"ChangeTheTime()\" (click)=\"ChangeTheTime()\"> -->\n          </ion-col>\n          <ion-col size=\"2\" class=\"text-center\">\n            <!-- <label id=\"lblTime\">-:--:--</label> -->\n          </ion-col>\n        </ion-row>\n      </ion-toolbar>\n      <!-- <ion-button id=\"btnMute\" *ngIf=\"btnunMute\" value=\"Mute\" onclick=\"MuteNow()\" (click)=\"unMute()\"><img\n          src=\"assets/volume.svg\"></ion-button>\n      <ion-button id=\"btnMute\" *ngIf=\"btnMute\" value=\"Mute\" onclick=\"MuteNow()\" (click)=\"Mute()\"><img\n          src=\"assets/mute.svg\"></ion-button>\n  \n      <br />\n      Volume :\n      <input type=\"range\" min=\"0\" max=\"1\" step=\"0.1\" id=\"volume\" onchange=\"ChangeVolume()\" (click)=\"ChangeVolume()\">\n      <br />\n      Time lapsed:\n      <input type=\"range\" step=\"any\" id=\"seekbar\" onchange=\"ChangeTheTime()\" (click)=\"ChangeTheTime()\">\n      <label id=\"lblTime\">-:--:--</label> -->\n    </div>\n    <!-- <div>{{timer}}</div>\n    <div>{{maxTime}}</div> -->\n    <h2 class=\"ion-padding-horizontal\">\n      Step 2: Supporting Class Dcuments\n    </h2>\n    <ion-row class=\"ion-padding-horizontal\">\n      <ion-col size=\"12\">\n        <ion-button expand=\"full\" [disabled]=\"pdf\" (click)=\"downloadPdf(pdfFile)\">\n          <ion-icon slot=\"start\" src=\"assets/icon/pdf.svg\"></ion-icon>Print Materials\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-input placeholder=\"Entry one (Fill in)\"></ion-input>\n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-input placeholder=\"Entry two (Fill in)\"></ion-input>\n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-textarea rows=\"6\" cols=\"20\" placeholder=\"Entry Three (Fill in)\"></ion-textarea>\n      </ion-col>\n    </ion-row>\n    <h2 class=\"ion-padding-horizontal\">\n      Step 3: Quiz\n    </h2>\n    <ion-row class=\"ion-padding-horizontal ion-margin-bottom\">\n      <ion-col size=\"12\">\n        <ion-button class=\"quiz_button\" expand=\"full\" [disabled]=\"take_quiz\" (click)=\"startQuiz()\">\n          Take quiz now\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>");

/***/ }),

/***/ "R2cY":
/*!*********************************************!*\
  !*** ./src/app/pages/video/video.module.ts ***!
  \*********************************************/
/*! exports provided: VideoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoPageModule", function() { return VideoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _video_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./video-routing.module */ "2J3j");
/* harmony import */ var _video_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./video.page */ "i0EM");
/* harmony import */ var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modules/shared/shared.module */ "FpXt");








let VideoPageModule = class VideoPageModule {
};
VideoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _video_routing_module__WEBPACK_IMPORTED_MODULE_5__["VideoPageRoutingModule"],
            src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_video_page__WEBPACK_IMPORTED_MODULE_6__["VideoPage"]]
    })
], VideoPageModule);



/***/ }),

/***/ "TIQ1":
/*!***********************************************!*\
  !*** ./src/app/services/video-log.service.ts ***!
  \***********************************************/
/*! exports provided: VideoLogService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoLogService", function() { return VideoLogService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");




let VideoLogService = class VideoLogService {
    constructor(http) {
        this.http = http;
        this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    videoLog(formData) {
        return new Promise((resolve, reject) => {
            console.log(this.apiUrl + 'videolog.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'videolog.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
        });
    }
};
VideoLogService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
VideoLogService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], VideoLogService);



/***/ }),

/***/ "i0EM":
/*!*******************************************!*\
  !*** ./src/app/pages/video/video.page.ts ***!
  \*******************************************/
/*! exports provided: VideoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoPage", function() { return VideoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_video_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./video.page.html */ "5nVJ");
/* harmony import */ var _video_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./video.page.scss */ "1odT");
/* harmony import */ var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/loader.service */ "5dVO");
/* harmony import */ var _services_quiz_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../services/quiz.service */ "ofzi");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var scriptjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! scriptjs */ "ojxP");
/* harmony import */ var scriptjs__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(scriptjs__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file/ngx */ "FAH8");
/* harmony import */ var src_app_services_video_log_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/video-log.service */ "TIQ1");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/environments/environment */ "AytR");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "B7Rs");
/* harmony import */ var _ionic_native_document_viewer_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/document-viewer/ngx */ "LfQc");
/* harmony import */ var src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! src/app/services/alert.service */ "3LUQ");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic/storage */ "e8h1");
/* harmony import */ var _iframe_iframe_page__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../iframe/iframe.page */ "tKxX");

















let VideoPage = class VideoPage {
    constructor(zone, router, route, videoLogService, quizService, file, platform, transfer, alertController, documentViewer, alertService, loaderService, storage, modalController, navCntl) {
        this.zone = zone;
        this.router = router;
        this.route = route;
        this.videoLogService = videoLogService;
        this.quizService = quizService;
        this.file = file;
        this.platform = platform;
        this.transfer = transfer;
        this.alertController = alertController;
        this.documentViewer = documentViewer;
        this.alertService = alertService;
        this.loaderService = loaderService;
        this.storage = storage;
        this.modalController = modalController;
        this.navCntl = navCntl;
        this.videoStartTime = 0;
        this.adsVideoStartTime = 0;
        this.hide_show_icon = true;
        this.adsDisabled = false;
        this.today = Date.now();
        this.myValue = 0;
        this.take_quiz = true;
        this.pdf = true;
        this.videoEndTime = 0;
        this.videoPaused = true;
        this.showTextarea = false;
        // iframeOnLoad(obj) { //this call when iframe do loading
        //   let doc = obj.contentDocument || obj.contentWindow;
        //   doc.addEventListener("click", this.iframeClickHandler.bind(this, 
        //   this.menuCtrl), false);
        //  }
        this.maxTime = 30;
        this.seekbar = 0;
        this.videoStartTime = 0;
        this.adsVideoStartTime = 0;
        this.btnPlay = true;
        this.btnunMute = true;
        // this.video_link = "https://s3.amazonaws.com/AST/9+min.mp4"
        // this.video_link = "http://static.videogular.com/assets/videos/elephants-dream.mp4"
        // this.video_link = "https://www.youtube.com/embed/r98doIWKudg"
        // this.video_link = "https://www.youtube.com/watch?v=01MJuw-xgQo"
        let listaFrames = document.getElementsByTagName("iframe");
        console.log("listaFrames : ");
        // this.StartTimer();
        //   let timeout = this.randomIntFromInterval(10, 10000)
        //   console.log("randomIntFromInterval : ", timeout)
        //   setTimeout(() => {
        //     console.log("setTimeout : ")
        //   }, timeout);
        this.loaderService.showLoader();
    }
    getQuiz() {
        let formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("videoid", this.videoid);
        this.quizService.getQuiz(formData).then((res) => {
            console.log("getQuiz : ", res);
            if (res.quizcount > 0) {
                this.take_quiz = false;
            }
            this.quizid = res.quizid;
            console.log("this.quiztime : ", this.quiztime);
            if (res.downloadcount > 0) {
                this.pdf = false;
                this.pdfFile = src_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].downloadurl + res.downloadlink;
                console.log("this.pdfFile : ", this.pdfFile);
            }
        });
    }
    randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
    StartTimer(maxTime) {
        console.log("maxTime : ", maxTime);
        this.timer = setTimeout(x => {
            if (maxTime <= 0) { }
            maxTime -= 1;
            if (maxTime > 0) {
                this.hidevalue = false;
                // this.UpdateTheTimeForPlay_video('play', this.interval)
                // this.SetSeekBar(this.videoStartTime)
                this.StartTimer(maxTime);
            }
            else {
                this.hidevalue = true;
            }
        }, 1000);
    }
    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.videotitle = this.router.getCurrentNavigation().extras.state.videotitle;
                this.videourl = this.router.getCurrentNavigation().extras.state.videourl;
                this.videoid = this.router.getCurrentNavigation().extras.state.videoid;
                this.videoimage = this.router.getCurrentNavigation().extras.state.videoimage;
                console.log("videoimage : ", this.videoimage);
                this.videoimage = src_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"].imageUrl + this.videoimage;
                console.log("videoimage after : ", this.videoimage);
                this.javaScript_Function(this.videourl);
                this.getQuiz();
                // this.loaderService.showLoader()
            }
        });
        Object(scriptjs__WEBPACK_IMPORTED_MODULE_7__["get"])("assets/javaScript.js", (data) => {
            console.log(data);
        });
    }
    javaScript_Function(url) {
        this.video_link = url;
        // get the video, volume and seekbar elements
        this.video = document.getElementById("video1");
        this.volumeRange = document.getElementById('volume');
        this.seekbar = document.getElementById('seekbar');
        console.log("video : ", this.video);
        console.log("volumeRange : ", this.volumeRange);
        console.log("seekbar : ", this.seekbar);
        // attach timeupdate, durationchange event to the video element
        if (this.video) {
            window.onload = function () {
                // go to http://www.w3.org/TR/DOM-Level-2-Events/events.html#Events-
                // EventTarget-addEventListener to know more about addEventListener
                // (false is for bubbling and true is for event capturing)
                this.video.addEventListener('timeupdate', this.UpdateTheTime(), false);
                this.video.addEventListener('durationchange', this.SetSeekBar(), false);
                // this.video.addEventListener('durationchange', this.SetSeekBar2(), false);
                this.volumeRange.value = this.video.volume;
            };
        }
    }
    onMetadata(e, video) {
        // this.video = video
        console.log("video onMetadata : ", video);
        console.log("video.duration : ", video.duration);
        // this.duration = video.duration;
        // //alert(this.duration);
        // this.lowerValue = 0;
        // this.upperValue = this.duration;
        // this.videoStartTime = 0;
        // this.videoEndTime = this.duration;
        video.removeAttribute('controls');
        video.setAttribute("webkit-playsinline", "true");
        video.setAttribute("playsinline", "true");
        // this.video = document.getElementById("video1");
        // window.onload = function () {
        //   // go to http://www.w3.org/TR/DOM-Level-2-Events/events.html#Events-
        //   // EventTarget-addEventListener to know more about addEventListener
        //   // (false is for bubbling and true is for event capturing)
        //   this.video.addEventListener('timeupdate', this.UpdateTheTime(), false);
        //   this.video.addEventListener('durationchange', this.SetSeekBar(), false);
        //   this.video.addEventListener('durationchange', this.SetSeekBar2(), false);
        //   this.volumeRange.value = this.video.volume;
        //   this.video.currentTime = this.videoStartTime;
        // }
        // this.storage.get("videoStartTime" + this.videoid).then((data) => {
        //   console.log("data : ", data)
        //   if (data) {
        //     this.videoStartTime = data
        //   } else {
        //     this.videoStartTime = 0
        //   }
        //   console.log("this.videoStartTime : ", this.videoStartTime)
        // })
        // this.videoStartTime = localStorage.getItem("videoStartTime"+this.videoid)
        // this.seekbar.min = this.videoStartTime;
        this.setEndTime();
        // this.setCurrentTimeOnLoad(this.videoStartTime)
        // this.SetSeekBar(this.videoStartTime)
        this.seekbar.value = 0;
        setTimeout(() => {
            this.loaderService.hideLoader();
        }, 500);
        //webkit-playsinline="true" playsinline="true"
        // console.log(this.upperValue);
        // const dualRange = document.querySelector('#dual-range') as HTMLIonRangeElement;
        // dualRange.value = { lower: this.lowerValue, upper: this.upperValue };
    }
    hideShow() {
        this.hide_show_icon = !this.hide_show_icon;
    }
    // fires when volume element is changed
    ChangeVolume() {
        var myVol = this.volumeRange.value;
        this.video.volume = myVol;
        if (myVol == 0) {
            this.video.muted = true;
        }
        else {
            this.video.muted = false;
        }
        console.log("change valume");
    }
    // fires when page loads, it sets the min and max range of the this.video
    SetSeekBar() {
        this.seekbar.min = 0;
        this.seekbar.max = this.video.duration;
    }
    SetSeekBar2() {
        this.seekbar.min = 0;
        this.seekbar.max = this.video.duration;
    }
    // fires when this.seekbar is changed
    ChangeTheTime(event) {
        console.log("event : ", event);
        /// Refresh the UI
        this.zone.run(() => {
            console.log('UI has refreshed');
            // this.seekbar.value = this.video.currentTime;
            this.video.currentTime = this.seekbar.value;
        });
        console.log("this.video.currentTime : ", this.video.currentTime);
        // this.video.play();
    }
    setCurrentTime(value, interval) {
        console.log("this.endTime : ", this.endTime);
        if (this.video.currentTime >= this.endTime) {
            clearInterval(interval);
            this.btnPause = false;
            this.btnPlay = true;
            this.videoLog(2);
            // console.log("UpdateTheTimeForPlay_video video end : ")
            // this.videoStartTime=0;
            // this.videoStartTime = 0;
            // this.javaScript_Function("https://s3.amazonaws.com/AST/9+min.mp4")
            // setTimeout(() => {
            //   this.PlayNow()
            // }, 1000)
        }
        else {
            console.log("this.videoStartTime setCurrentTime : ", this.videoStartTime);
            var sec = this.videoStartTime;
            var h = Math.floor(sec / 3600);
            sec = sec % 3600;
            var min = Math.floor(sec / 60);
            this.min_ = min;
            sec = Math.floor(sec % 60);
            if (sec.toString().length < 2)
                sec = "0" + sec;
            if (this.min_.toString().length < 2)
                this.min_ = "0" + this.min_;
            if (h == 0) {
                document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
            }
            else {
                document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
            }
            this.seekbar.min = this.video.startTime;
            this.seekbar.max = this.video.duration;
            this.seekbar.value = this.video.currentTime;
            this.videoStartTime = this.video.currentTime;
            // // document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
            // // console.log("this.video.startTime : ", this.video.startTime)
            // console.log("this.video.currentTime : ", this.video.currentTime)
            // console.log("this.videoStartTime afdqasd: ", this.videoStartTime)
            // console.log("this.seekbar.min : ", this.seekbar.min)
            // console.log("this.seekbar.max : ", this.seekbar.max)
            // console.log("this.seekbar.value : ", this.seekbar.value)
            // // this.ChangeTheTime()
        }
    }
    // setCurrentTime(data, video) {
    //   if (this.video.currentTime >= this.endTime) {
    //     this.btnPause = false
    //     this.btnPlay = true
    //     this.videoLog(2)
    //   } else {
    //     // console.log("data : ", data)
    //     // if (data.target.currentTime < 0.511) {
    //     //   data.target.currentTime = this.videoStartTime;
    //     //   video.play()
    //     // }
    //     console.log("this.videoStartTime setCurrentTime : ", this.videoStartTime)
    //     var sec = this.videoStartTime;
    //     var h = Math.floor(sec / 3600);
    //     sec = sec % 3600;
    //     var min = Math.floor(sec / 60);
    //     this.min_ = min;
    //     sec = Math.floor(sec % 60);
    //     if (sec.toString().length < 2) sec = "0" + sec;
    //     if (this.min_.toString().length < 2) this.min_ = "0" + this.min_;
    //     if (h == 0) {
    //       document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
    //     } else {
    //       document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
    //     }
    //     // data.target.currentTime = this.videoStartTime;
    //     // video.play()
    //     this.video.seekbar.min = this.video.startTime;
    //     this.video.seekbar.max = this.video.duration;
    //     this.video.seekbar.value = this.video.currentTime;
    //     this.video.videoStartTime = this.video.currentTime;
    //     // }
    //   }
    // }
    UpdateTheTimeForAds(value, interval) {
        console.log("this.endTime : ", this.endTime);
        if (this.video.currentTime >= this.endTime) {
            clearInterval(interval);
            this.btnPause = false;
            this.btnPlay = true;
            this.javaScript_Function(this.videourl);
            setTimeout(() => {
                this.video.currentTime = this.videoStartTime;
                this.adsDisabled = false;
                // this.PlayNow()
            }, 1000);
        }
        else {
            var sec = this.video.currentTime;
            var h = Math.floor(sec / 3600);
            sec = sec % 3600;
            var min = Math.floor(sec / 60);
            this.min_ = min;
            sec = Math.floor(sec % 60);
            if (sec.toString().length < 2)
                sec = "0" + sec;
            if (this.min_.toString().length < 2)
                this.min_ = "0" + this.min_;
            if (h == 0) {
                document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
            }
            else {
                document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
            }
            // document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
            this.seekbar.min = this.video.startTime;
            this.seekbar.max = this.video.duration;
            this.seekbar.value = this.video.currentTime;
            // this.adsVideoStartTime = this.video.currentTime;
            console.log("this.seekbar.min : ", this.seekbar.min);
            console.log("this.seekbar.max : ", this.seekbar.max);
            console.log("this.seekbar.value : ", this.seekbar.value);
            // this.ChangeTheTime()
        }
    }
    setCurrentTimeOnLoad(videoStartTime) {
        console.log("videoStartTime setCurrentTimeOnLoad : ", videoStartTime);
        var sec = videoStartTime;
        var h = Math.floor(sec / 3600);
        sec = sec % 3600;
        var min = Math.floor(sec / 60);
        this.min_ = min;
        sec = Math.floor(sec % 60);
        if (sec.toString().length < 2)
            sec = "0" + sec;
        if (this.min_.toString().length < 2)
            this.min_ = "0" + this.min_;
        if (h == 0) {
            document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
        }
        else {
            document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
        }
    }
    setEndTime() {
        var sec = this.video.duration;
        this.endTime = this.video.duration;
        console.log("setEndTime : ", sec);
        var h = Math.floor(sec / 3600);
        sec = sec % 3600;
        var min = Math.floor(sec / 60);
        this.min_ = min;
        sec = Math.floor(sec % 60);
        if (sec.toString().length < 2)
            sec = "0" + sec;
        if (this.min_.toString().length < 2)
            this.min_ = "0" + this.min_;
        if (h == 0) {
            document.getElementById('lblTime2').innerHTML = this.min_ + ":" + sec;
        }
        else {
            document.getElementById('lblTime2').innerHTML = h + ":" + this.min_ + ":" + sec;
        }
    }
    PlayNow() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // Play Video from Application Directory or Stream Online
            // HKVideoPlayer.play(this.videourl);
            const modal = yield this.modalController.create({
                component: _iframe_iframe_page__WEBPACK_IMPORTED_MODULE_16__["IframePage"],
                componentProps: {
                    videoLink: this.videourl,
                }
            });
            modal.onDidDismiss().then((data) => {
                console.log(data);
            });
            yield modal.present();
            this.btnPlay = true;
            // // let mainVideo = <HTMLMediaElement>document.getElementById('video1');
            // // console.log("mainVideo : ",mainVideo)
            // // this.videoPaused = true;
            // // this.setCurrentTimeOnLoad(this.videoStartTime)
            // video1.currentTime = this.videoStartTime;
            // video1.play()
            // // this.video.currentTime = this.videoStartTime;
            // // this.video.play();
            // // this.video.currentTime = this.videoStartTime;
            // this.btnPause = true
            // this.btnPlay = false
            // // this.video.load()
            // // setTimeout(() => {
            // console.log("this.this.setCurrentTimeOnLoad(this.videoStartTime) : ", this.videoStartTime)
            // // this.setEndTime()
            // // this.video.play();
            // // }, 500)
            setTimeout(() => {
                this.videoLog(1);
            }, 1000);
            // setTimeout(() => {
            //   this.hideShow()
            // }, 2000)
            // // setTimeout(() => {
            // //   console.log("this.endTime : ", this.endTime)
            // //   // this.StartTimer(this.endTime)
            // // }, 1000)
            // // setTimeout(() => {
            // //   // this.adsDisabled = true
            // //   /////// /////////////////////////// if this function uncomment then the ads code working ////      
            // //   // this.playAds()
            // // }, 10000)
            // // this.StartTimer(this.endTime)
            // this.interval = setInterval(() => {
            //   console.log("this.video.currentTime hjdshfaj : ", this.video.currentTime)
            //   this.setCurrentTime('play', this.interval)
            //   this.SetSeekBar()
            //   // this.playAds()
            // }, 1000)
        });
    }
    videoEnded() {
        console.log("videoEnded : ");
    }
    playAds() {
        clearInterval(this.interval);
        this.javaScript_Function("http://techslides.com/demos/sample-videos/small.mp4");
        setTimeout(() => {
            // this.video.currentTime = this.adsVideoStartTime;
            this.video.play();
            this.endTime = this.video.duration;
            this.setEndTime();
            this.interval2 = setInterval(() => {
                this.UpdateTheTimeForAds('ads', this.interval2);
                this.SetSeekBar2();
            }, 1000);
            console.log("video  : ", this.video.end);
        }, 1000);
    }
    PauseNow() {
        this.videoPaused = false;
        // let mainVideo = <HTMLMediaElement>document.getElementById('video1');
        // this.video.currentTime = 400;
        this.video.pause();
        this.btnPause = false;
        this.btnPlay = true;
        // this.btnPause = false
        // this.btnPlay = true
        // console.log("pause video : btn click")
        // if (this.video.play) {
        //   this.video.pause();
        this.videoLog(0);
        // }
        // // localStorage.setItem("videoStartTime",this.videoStartTime)
        // console.log("pause")
        clearInterval(this.interval);
    }
    videoLog(state) {
        let formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("videoid", this.videoid);
        formData.append("state", state);
        this.videoLogService.videoLog(formData).then((res) => {
            console.log(res);
        });
    }
    unMute() {
        this.btnunMute = false;
        this.btnMute = true;
    }
    Mute() {
        this.btnunMute = true;
        this.btnMute = false;
    }
    openPDF(url) {
        console.log("url : ", url);
        this.pdfOpenAlert2('Open', 'Download', 'You can open and download the PDF.', url);
    }
    downloadPdf(url) {
        const fileTransfer = this.transfer.create();
        let targetPath;
        if (this.platform.is("ios")) {
            targetPath = this.file.documentsDirectory;
        }
        else {
            targetPath = this.file.dataDirectory;
        }
        fileTransfer.download(url, targetPath + '/my_downloads/' + this.videotitle + '.pdf').then((entry) => {
            console.log('file download response', entry);
            if (entry) {
                // this.toastService.showToast("File download successfully.")
                this.pdfOpenAlert('No', 'Yes', 'PDF downloaded successfully. Do you want to open it?', entry);
            }
        }).catch((err) => {
            console.log('error in file download', err);
        });
    }
    openPdf(entry) {
        const options = {
            title: entry.name
        };
        this.documentViewer.viewDocument(entry.nativeURL, 'application/pdf', options);
    }
    pdfOpenAlert(btn1, btn2, message, entry) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                // header: 'PDF',
                message: message,
                buttons: [
                    {
                        text: btn1,
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            // console.log('Confirm Cancel');
                        }
                    },
                    {
                        text: btn2,
                        handler: (alertData) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                            this.openPdf(entry);
                        })
                    }
                ]
            });
            yield alert.present();
        });
    }
    pdfOpenAlert2(btn1, btn2, message, url) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                // header: 'PDF',
                message: message,
                buttons: [
                    {
                        text: btn1,
                        cssClass: 'secondary',
                        handler: () => {
                            let navigationExtras = {
                                state: {
                                    name: this.videotitle,
                                    url: url,
                                }
                            };
                            // this.router.navigate(['post-add-second', navigationExtras]);
                            this.router.navigate(['pdf-viewer'], navigationExtras);
                        }
                    }, {
                        text: btn2,
                        handler: (alertData) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                            this.downloadPdf(url);
                        })
                    }
                ]
            });
            yield alert.present();
        });
    }
    quizdetails() {
        let formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("quizid", this.quizid);
        this.quizService.quizdetails(formData).then((data) => {
            if (data.status) {
                this.data = data;
                this.showAlert(this.data);
            }
            else {
                this.alertService.presentAlertError(data.message);
            }
        });
    }
    startQuiz() {
        // console.log("this.quizid : ",this.quizid)
        // let navigationExtras: NavigationExtras = {
        //   state: {
        //     quizids: this.quizid
        //   }
        // };
        // // this.router.navigate(['post-add-second', navigationExtras]);
        // // this.router.navigate(['tabs/tab1/sub-categories/video/quiz', navigationExtras]);
        // // this.alertService.showAlertForQuiz(quiz)
        // this.navCntl.navigateForward('tabs/tab1/sub-categories/video/quiz', navigationExtras)
        // // this.navCntl.
        this.quizdetails();
    }
    showAlert(quiz) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // let firstTime = false;
            // let attemptsRemaining = 0;
            // if (quiz.is_attempt == 0) {
            //   firstTime = true;
            // }
            // console.log(quiz)
            // if (quiz.redo && (quiz.is_attempt < quiz.redo_attempt + 1)) {
            //   attemptsRemaining = (quiz.redo_attempt + 1) - quiz.is_attempt;
            // }
            const buttons = [];
            buttons.push({
                text: 'Dismiss',
                role: 'cancel'
            });
            // if (firstTime || attemptsRemaining > 0) {
            buttons.push({
                text: 'Start quiz',
                handler: () => {
                    let navigationExtras = {
                        state: {
                            quizids: this.quizid,
                            quiztimes: quiz.quiztime
                        }
                    };
                    this.navCntl.navigateForward('tabs/tab1/sub-categories/video/quiz', navigationExtras);
                }
            });
            // }
            // if (firstTime && attemptsRemaining == 0) {
            //   attemptsRemaining = 1;
            // }
            const alert = yield this.alertController.create({
                header: quiz.title,
                message: `
        <p>${quiz.description}</p>
        <p class="font-weight-600">Details</p>
        <ul class="text-align-left">
          <li>Time: ${this.transform(quiz.quiztime)}</li>
          <li>Total points: ${quiz.totalscore}</li>
          <li>Passing points: ${quiz.passcore}</li>
        </ul>
      `,
                buttons: buttons
            });
            // <li>Attempt remaining: ${attemptsRemaining}</li>
            alert.present();
        });
    }
    transform(time) {
        if (time >= 60) {
            const timeInMinutes = Math.floor(time / 60);
            const hours = Math.floor(time / 3600);
            let minutes = timeInMinutes;
            if (minutes >= 60) {
                minutes = timeInMinutes % 60;
            }
            return (hours > 0 ? '0' + hours + 'hr ' : '')
                + (minutes < 10 ? '0' + minutes : minutes) +
                'mins ' + ((time - timeInMinutes * 60) < 10 ? '0' +
                (time - timeInMinutes * 60) : (time - timeInMinutes * 60)) + 'secs';
        }
        else {
            return (time < 10 ? '00: 0' + time : '00:' + time) + 'secs';
        }
    }
    ionViewWillLeave() {
        console.log("ionViewWillLeave : ");
        // this.storage.set("videoStartTime" + this.videoid, this.videoStartTime)
        // clearInterval(this.interval);
    }
};
VideoPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__["NgZone"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
    { type: src_app_services_video_log_service__WEBPACK_IMPORTED_MODULE_9__["VideoLogService"] },
    { type: _services_quiz_service__WEBPACK_IMPORTED_MODULE_4__["QuizService"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__["File"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Platform"] },
    { type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_12__["FileTransfer"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["AlertController"] },
    { type: _ionic_native_document_viewer_ngx__WEBPACK_IMPORTED_MODULE_13__["DocumentViewer"] },
    { type: src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_14__["AlertService"] },
    { type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_15__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["NavController"] }
];
VideoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Component"])({
        selector: 'app-video',
        template: _raw_loader_video_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_video_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], VideoPage);



/***/ })

}]);
//# sourceMappingURL=pages-video-video-module-es2015.js.map