(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-testing-testing-module"],{

/***/ "5dVO":
/*!********************************************!*\
  !*** ./src/app/services/loader.service.ts ***!
  \********************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let LoaderService = class LoaderService {
    constructor(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    showLoader() {
        this.isBusy = true;
        // this.loaderToShow = this.loadingCtrl.create({
        //   message: 'Please Wait..'
        // }).then((res) => {
        //   res.present();
        //   // res.onDidDismiss().then((dis) => {
        //   //    console.log('Loading dismissed!',dis);
        //   // });
        // });
        // // this.hideLoader();
    }
    hideLoader() {
        // setTimeout(()=>{
        //   this.loadingCtrl.dismiss();
        // },100)
        this.isBusy = false;
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ "ECk0":
/*!***********************************************!*\
  !*** ./src/app/pages/testing/testing.page.ts ***!
  \***********************************************/
/*! exports provided: TestingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestingPage", function() { return TestingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_testing_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./testing.page.html */ "eL+S");
/* harmony import */ var _testing_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./testing.page.scss */ "ZEaj");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "e8h1");
/* harmony import */ var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/loader.service */ "5dVO");







let TestingPage = class TestingPage {
    constructor(router, route, storage, loaderService) {
        this.router = router;
        this.route = route;
        this.storage = storage;
        this.loaderService = loaderService;
        this.videoPaused = true;
        this.assigned = false;
        this.btnPlay = true;
        this.btnPause = false;
        this.videoStartTime = 200;
        this.videoEndTime = 0;
        this.hide_show_icon = true;
        this.take_quiz = true;
        this.pdf = true;
        this.adsDisabled = false;
        this.seekbar = 0;
        // this.videoUrl = 'https://s3.amazonaws.com/AST/9+min.mp4'
    }
    // onMetadata(e, video) {
    //   this.duration = video.duration;
    //   //alert(this.duration);
    //   this.lowerValue = 0;
    //   this.upperValue = this.duration;
    //   // this.videoStartTime = 200;
    //   this.storage.get("videoStartTime" + this.videoid).then((data) => {
    //     console.log("data : ", data)
    //     if (data) {
    //       this.videoStartTime = data
    //     } else {
    //       this.videoStartTime = 0
    //     }
    //     console.log("this.videoStartTime : ", this.videoStartTime)
    //     // this.seekbar = document.getElementById('seekbar');
    //     // console.log("seekbar : ", this.seekbar)
    //     // video.addEventListener('durationchange', this.SetSeekBar(this.videoStartTime), false);
    //   })
    //   this.videoEndTime = this.duration;
    //   video.removeAttribute('controls');
    //   video.setAttribute("webkit-playsinline", "true");
    //   video.setAttribute("playsinline", "true");
    //   setTimeout(() => {
    //     this.loaderService.hideLoader();
    //   }, 500)
    // }
    // // fires when page loads, it sets the min and max range of the this.video
    // SetSeekBar(videoStartTime) {
    //   this.seekbar.min = videoStartTime;
    //   let mainVideo = <HTMLMediaElement>document.getElementById('video');
    //   this.seekbar.max = mainVideo.duration;
    // }
    // num_chk:any = 0;
    // setCurrentTime(data) {
    //   this.stringwithhtml = '';
    //   this.videotext = '';
    //   if (this.videoPaused == false) {
    //     this.stringwithhtml = this.text;
    //   }
    //   console.log("setCurrentTime enter : ", data)
    //     if (this.videoPaused == true) {
    //       console.log("setCurrentTime enter this.videoPaused == true : ",)
    //       if (this.assigned == false) {
    //         console.log("setCurrentTime enter this.videoPaused == true assigned : ", this.videoStartTime)
    //         data.target.currentTime = this.videoStartTime;
    //         console.log("setCurrentTime enter this.videoPaused == true assigned : ", data.target.currentTime)
    //         this.assigned = true;
    //       }
    //       if (this.endAssigned == false) {
    //         data.target.currentTime = this.videoEndTime;
    //         this.endAssigned = true;
    //       }
    //       this.currentTime = data.target.currentTime;
    //       console.log("this.currentTime : ", this.currentTime)
    //       // if (this.currentTime > 0 && this.currentTime < this.videoStartTime) {
    //       //   this.pauseVideo();
    //       //   console.log("this.pauseVideo() 1 : ", this.pauseVideo())
    //       // } else if (this.currentTime >= this.videoEndTime) {
    //       //   this.pauseVideo();
    //       //   data.target.currentTime = this.videoStartTime;
    //       //   this.videoPaused = false;
    //       //   console.log("this.pauseVideo() 2 : ", this.pauseVideo())
    //       // } else {
    //       //   this.playVideo();
    //       //   console.log("this.playVideo() 1 : ", this.playVideo())
    //       //   if(this.num_chk == 0){
    //       //     // this.loaderService.hideLoader()
    //       //     this.num_chk = 1
    //       //   }
    //       // }
    //     }
    // }
    // playVideo() {
    //   let mainVideo = <HTMLMediaElement>document.getElementById('video');
    //   var promise = mainVideo.play();
    //   console.log("promise ",promise)
    //   if (promise !== undefined) {
    //     promise.then(function() {
    //       console.log("true")
    //     }).catch(function(error) {
    //         //If there was an error autoplaying the video, then display a button to play the video
    //         console.log("false")
    //     });
    // }
    //   this.btnPause = true;
    //   this.btnPlay = false;
    // }
    // pauseVideo() {
    //   let mainVideo = <HTMLMediaElement>document.getElementById('video');
    //   mainVideo.pause();
    //   this.btnPause = false;
    //   this.btnPlay = true;
    // }
    // btnPlayVideo() {
    //   let mainVideo = <HTMLMediaElement>document.getElementById('video');
    //   this.videoPaused = true;
    //   var promise = mainVideo.play();
    //   console.log("promise btnPlayVideo ",promise)
    //   if (promise !== undefined) {
    //     promise.then(function() {
    //       console.log("true btnPlayVideo")
    //     }).catch(function(error) {
    //         //If there was an error autoplaying the video, then display a button to play the video
    //         console.log("false btnPlayVideo")
    //     });
    // }
    //   this.btnPause = true;
    //   this.btnPlay = false;
    //   // this.loaderService.showLoader();
    //   // this.showTextarea = false;
    //   // this.posterValue = 'assets/poster.gif';
    // }
    // btnpauseVideo() {
    //   this.videoPaused = false;
    //   let mainVideo = <HTMLMediaElement>document.getElementById('video');
    //   mainVideo.pause();
    //   this.btnPause = false;
    //   this.btnPlay = true;
    //   // this.posterValue = 'assets/player.svg';
    // }
    // hideShow() {
    //   this.hide_show_icon = !this.hide_show_icon;
    // }
    ngOnInit() {
        // this.route.queryParams.subscribe(params => {
        //   if (this.router.getCurrentNavigation().extras.state) {
        //     this.videotitle = this.router.getCurrentNavigation().extras.state.videotitle;
        //     this.videoUrl = this.router.getCurrentNavigation().extras.state.videourl;
        //     this.videoid = this.router.getCurrentNavigation().extras.state.videoid;
        //     this.videoimage = this.router.getCurrentNavigation().extras.state.videoimage;
        //     console.log("videoimage : ", this.videoimage)
        //     this.videoimage = environment.imageUrl + this.videoimage
        //     this.loaderService.showLoader()
        //   }
        // });
        // get("assets/javaScript.js", (data) => {
        //   console.log(data)
        // });
    }
};
TestingPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"] }
];
TestingPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-testing',
        template: _raw_loader_testing_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_testing_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TestingPage);



/***/ }),

/***/ "WC81":
/*!*************************************************!*\
  !*** ./src/app/pages/testing/testing.module.ts ***!
  \*************************************************/
/*! exports provided: TestingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestingPageModule", function() { return TestingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _testing_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./testing-routing.module */ "WMOM");
/* harmony import */ var _testing_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./testing.page */ "ECk0");
/* harmony import */ var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modules/shared/shared.module */ "FpXt");








let TestingPageModule = class TestingPageModule {
};
TestingPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _testing_routing_module__WEBPACK_IMPORTED_MODULE_5__["TestingPageRoutingModule"],
            src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_testing_page__WEBPACK_IMPORTED_MODULE_6__["TestingPage"]]
    })
], TestingPageModule);



/***/ }),

/***/ "WMOM":
/*!*********************************************************!*\
  !*** ./src/app/pages/testing/testing-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: TestingPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestingPageRoutingModule", function() { return TestingPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _testing_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./testing.page */ "ECk0");




const routes = [
    {
        path: '',
        component: _testing_page__WEBPACK_IMPORTED_MODULE_3__["TestingPage"]
    }
];
let TestingPageRoutingModule = class TestingPageRoutingModule {
};
TestingPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TestingPageRoutingModule);



/***/ }),

/***/ "ZEaj":
/*!*************************************************!*\
  !*** ./src/app/pages/testing/testing.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-button {\n  --border-radius: 0px;\n}\n\nion-icon {\n  font-size: 24px;\n}\n\n.controlIcon {\n  font-size: 20px;\n}\n\nion-range {\n  height: 20px;\n  width: 90%;\n  position: absolute;\n  top: 0;\n  margin: 0 0 0 5% !important;\n  padding: 0 !important;\n  --knob-background: #b5c827;\n  --bar-height: 8px;\n  --bar-background: white;\n  --bar-background-active: #b5c827;\n  --bar-border-radius: 6px;\n  --knob-size: 30px;\n}\n\n.duration-container {\n  height: 20px;\n  position: absolute;\n  top: 20px;\n  width: 100%;\n  margin: 0 !important;\n  padding: 0 !important;\n  display: flex;\n  flex-direction: row;\n  font-size: 16px;\n  line-height: 20px;\n  font-weight: 700;\n  color: #b5c827;\n}\n\n#albumPhoto {\n  position: absolute;\n  width: 100%;\n  background: black;\n  display: flex;\n  align-items: center;\n  flex-direction: column;\n  justify-content: center;\n}\n\n#image {\n  position: absolute;\n}\n\n.player-controls-middle {\n  position: absolute;\n  top: 50%;\n  width: 100%;\n  transform: translateY(-82%);\n}\n\n.center {\n  justify-content: center;\n}\n\n.pos_play_btn {\n  position: absolute;\n  top: 23%;\n  width: 100%;\n  transform: translateY(-82%);\n}\n\n.time-display-content {\n  pointer-events: none;\n}\n\n.cbox, .vbox, .center {\n  display: flex;\n  align-items: center;\n}\n\n.time-first, .time-second, .time-delimiter {\n  color: #ffffff;\n}\n\n.time-delimiter {\n  opacity: 0.7;\n  margin: 0 4px;\n}\n\nimg {\n  height: 50px;\n}\n\n.opacity2 {\n  opacity: 1;\n}\n\n.opity {\n  opacity: 0;\n}\n\n.ion_toolbar_time_seekbar {\n  top: -48px;\n  --background: transparent;\n  --background: #7979795c;\n}\n\n.ion_toolbar_play_pause {\n  top: -92px;\n  --background: transparent;\n}\n\n.ion_toolbar_time_seekbar1 {\n  top: -92px;\n  --background: transparent;\n  --background: #7979795c;\n}\n\n.skip_ads_button {\n  border: 2px solid #fff;\n  background: #909090;\n  margin-right: -5px;\n}\n\n.skip_ads_button ion-button {\n  --color: white;\n}\n\nion-grid {\n  padding: 0px;\n}\n\nion-grid ion-toolbar {\n  padding: 0px;\n}\n\nvideo {\n  width: 100%;\n  min-height: 300px;\n  height: 300px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\nion-range {\n  --knob-background: #606060;\n  --bar-height: 6px;\n  --bar-background: white;\n  --bar-background-active: #606060;\n  --bar-border-radius: 6px;\n  --knob-size: 16px;\n  height: 34px;\n}\n\nion-input {\n  border: 1px solid #606060;\n  --padding-start: 16px;\n}\n\nion-textarea {\n  border: 1px solid #606060;\n  --padding-start: 16px;\n}\n\n.quiz_button {\n  --background: #EB6A42;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Rlc3RpbmcucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0JBQUE7QUFDSjs7QUFHQTtFQUNJLGVBQUE7QUFBSjs7QUFJRTtFQUNFLGVBQUE7QUFESjs7QUFJRTtFQUNFLFlBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsMkJBQUE7RUFDQSxxQkFBQTtFQUNBLDBCQUFBO0VBQ0EsaUJBQUE7RUFDQSx1QkFBQTtFQUNBLGdDQUFBO0VBQ0Esd0JBQUE7RUFDQSxpQkFBQTtBQURKOztBQUlFO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtFQUNBLHFCQUFBO0VBQ0EsYUFBQTtFQUFlLG1CQUFBO0VBQ2YsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBQUo7O0FBR0U7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7QUFBSjs7QUFHRTtFQUNFLGtCQUFBO0FBQUo7O0FBS0E7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0VBRUUsMkJBQUE7QUFGTjs7QUFJQTtFQUdFLHVCQUFBO0FBREY7O0FBSUE7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0VBRUUsMkJBQUE7QUFESjs7QUFHQTtFQUNFLG9CQUFBO0FBQUY7O0FBRUE7RUFHRSxhQUFBO0VBR0EsbUJBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7QUFFRjs7QUFBQTtFQUNFLFlBQUE7RUFDQSxhQUFBO0FBR0Y7O0FBREE7RUFDRSxZQUFBO0FBSUY7O0FBRkE7RUFDRSxVQUFBO0FBS0Y7O0FBSEE7RUFDRSxVQUFBO0FBTUY7O0FBSkE7RUFFRSxVQUFBO0VBQ0EseUJBQUE7RUFDQSx1QkFBQTtBQU1GOztBQUpBO0VBRUUsVUFBQTtFQUNBLHlCQUFBO0FBTUY7O0FBSEE7RUFFRSxVQUFBO0VBQ0EseUJBQUE7RUFDQSx1QkFBQTtBQUtGOztBQUhBO0VBQ0Usc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBTUY7O0FBTEU7RUFDRSxjQUFBO0FBT0o7O0FBSkE7RUFDRSxZQUFBO0FBT0Y7O0FBTkU7RUFDRSxZQUFBO0FBUUo7O0FBTEE7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtBQVFGOztBQU5BO0VBQ0UsMEJBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0NBQUE7RUFDQSx3QkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQVNGOztBQVBBO0VBQ0UseUJBQUE7RUFDQSxxQkFBQTtBQVVGOztBQVJBO0VBQ0UseUJBQUE7RUFDQSxxQkFBQTtBQVdGOztBQVRBO0VBQ0UscUJBQUE7QUFZRiIsImZpbGUiOiJ0ZXN0aW5nLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1idXR0b24ge1xuICAgIC0tYm9yZGVyLXJhZGl1czogMHB4O1xuICAgIC8vIC0tYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbmlvbi1pY29uIHtcbiAgICBmb250LXNpemU6IDI0cHg7XG4gICAgLy8gY29sb3I6IHdoaXRlO1xuICB9XG4gIFxuICAuY29udHJvbEljb24ge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgfVxuICBcbiAgaW9uLXJhbmdlIHtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIG1hcmdpbjogMCAwIDAgNSUhaW1wb3J0YW50O1xuICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbiAgICAtLWtub2ItYmFja2dyb3VuZDogI2I1YzgyNztcbiAgICAtLWJhci1oZWlnaHQ6IDhweDtcbiAgICAtLWJhci1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAtLWJhci1iYWNrZ3JvdW5kLWFjdGl2ZTogI2I1YzgyNztcbiAgICAtLWJhci1ib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgLS1rbm9iLXNpemU6IDMwcHg7XG4gIH1cbiAgXG4gIC5kdXJhdGlvbi1jb250YWluZXIge1xuICAgIGhlaWdodDogMjBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAyMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbjogMCAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbiAgICBkaXNwbGF5OiBmbGV4OyBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICBmb250LXdlaWdodDogNzAwO1xuICAgIGNvbG9yOiAjYjVjODI3O1xuICB9XG4gIFxuICAjYWxidW1QaG90byB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB9XG4gIFxuICAjaW1hZ2Uge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgfVxuXG5cblxuLnBsYXllci1jb250cm9scy1taWRkbGUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDUwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtODIlKTtcbn1cbi5jZW50ZXIge1xuICAtd2Via2l0LWJveC1wYWNrOiBjZW50ZXI7XG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4ucG9zX3BsYXlfYnRuIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDIzJTtcbiAgd2lkdGg6IDEwMCU7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtODIlKTtcbn1cbi50aW1lLWRpc3BsYXktY29udGVudCB7XG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuLmNib3gsIC52Ym94LCAuY2VudGVyIHtcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgZGlzcGxheTogZmxleDtcbiAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLnRpbWUtZmlyc3QsIC50aW1lLXNlY29uZCwgLnRpbWUtZGVsaW1pdGVyIHtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4udGltZS1kZWxpbWl0ZXIge1xuICBvcGFjaXR5OiAuNztcbiAgbWFyZ2luOiAwIDRweDtcbn1cbmltZyB7XG4gIGhlaWdodDogNTBweDtcbn1cbi5vcGFjaXR5MiB7XG4gIG9wYWNpdHk6IDE7XG59XG4ub3BpdHl7XG4gIG9wYWNpdHk6IDA7XG59XG4uaW9uX3Rvb2xiYXJfdGltZV9zZWVrYmFyIHtcbiAgLy8gLS1iYWNrZ3JvdW5kOiBibGFjaztcbiAgdG9wOiAtNDhweDtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgLS1iYWNrZ3JvdW5kOiAjNzk3OTc5NWM7XG59XG4uaW9uX3Rvb2xiYXJfcGxheV9wYXVzZSB7XG4gIC8vIC0tYmFja2dyb3VuZDogYmxhY2s7XG4gIHRvcDogLTkycHg7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIC8vIC0tYmFja2dyb3VuZDogIzc5Nzk3OTVjO1xufVxuLmlvbl90b29sYmFyX3RpbWVfc2Vla2JhcjEge1xuICAvLyAtLWJhY2tncm91bmQ6IGJsYWNrO1xuICB0b3A6IC05MnB4O1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAtLWJhY2tncm91bmQ6ICM3OTc5Nzk1Yztcbn1cbi5za2lwX2Fkc19idXR0b24ge1xuICBib3JkZXI6IDJweCBzb2xpZCAjZmZmO1xuICBiYWNrZ3JvdW5kOiAjOTA5MDkwO1xuICBtYXJnaW4tcmlnaHQ6IC01cHg7XG4gIGlvbi1idXR0b24ge1xuICAgIC0tY29sb3I6IHdoaXRlO1xuICB9XG59XG5pb24tZ3JpZCB7XG4gIHBhZGRpbmc6IDBweDtcbiAgaW9uLXRvb2xiYXIge1xuICAgIHBhZGRpbmc6IDBweDtcbiAgfVxufVxudmlkZW8ge1xuICB3aWR0aDogMTAwJTtcbiAgbWluLWhlaWdodDogMzAwcHg7XG4gIGhlaWdodDogMzAwcHg7XG4gIG9iamVjdC1maXQ6Y292ZXI7XG59XG5pb24tcmFuZ2Uge1xuICAtLWtub2ItYmFja2dyb3VuZDogIzYwNjA2MDtcbiAgLS1iYXItaGVpZ2h0OiA2cHg7XG4gIC0tYmFyLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLWJhci1iYWNrZ3JvdW5kLWFjdGl2ZTogIzYwNjA2MDtcbiAgLS1iYXItYm9yZGVyLXJhZGl1czogNnB4O1xuICAtLWtub2Itc2l6ZTogMTZweDtcbiAgaGVpZ2h0OiAzNHB4O1xufVxuaW9uLWlucHV0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzYwNjA2MDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxNnB4O1xufVxuaW9uLXRleHRhcmVhIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzYwNjA2MDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxNnB4O1xufVxuLnF1aXpfYnV0dG9uIHtcbiAgLS1iYWNrZ3JvdW5kOiAjRUI2QTQyO1xufSJdfQ== */");

/***/ }),

/***/ "eL+S":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/testing/testing.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header title=\"{{videotitle}}\"></header>\n\n<ion-content>\n\n  <!-- <video id=\"video\" #video  (timeupdate)=\"setCurrentTime($event)\"   (loadedmetadata)=\"onMetadata($event, video)\" width=\"100%\" height=\"150\" >\n    <source src=\"{{videoUrl}}\" >\n   </video>\n\n   <ion-col (click)=\"btnPlayVideo()\" size=\"4\" *ngIf=\"btnPlay\" class=\"text-center\" ><img src=\"assets/play.svg\"></ion-col>\n   <ion-col (click)=\"btnpauseVideo()\" size=\"4\" *ngIf=\"btnPause\"  class=\"text-center\" ><img src=\"assets/pause.svg\"></ion-col> -->\n<!-- \n   <ion-grid style=\"height: 100%\">\n    <div class=\"ion-text-center\" style=\"height: 300px;\">\n      <video id=\"video\" #video (timeupdate)=\"setCurrentTime($event)\" (loadedmetadata)=\"onMetadata($event, video)\" (click)=\"hideShow()\" \n        playsinline poster=\"{{videoimage}}\" controlsList=\"nodownload\" controls>\n        <source src=\"{{videoUrl}}\" >\n      </video>\n      <ion-row class=\"pos_play_btn ion-align-items-center ion-justify-content-center\" *ngIf=\"hide_show_icon\">\n        <ion-col size=\"4\">\n        </ion-col>\n        <ion-col size=\"4\" class=\"text-center\" *ngIf=\"!adsDisabled\">\n          <img id=\"btnPlay\" *ngIf=\"btnPlay\" value=\"Play\" (click)=\"btnPlayVideo()\" src=\"assets/icon/play.svg\">\n          <img id=\"btnPause\" *ngIf=\"btnPause\" value=\"Pause\" (click)=\"btnpauseVideo()\" src=\"assets/icon/pause.svg\">\n        </ion-col>\n        <ion-col size=\"4\" class=\"text-center\">\n        </ion-col>\n      </ion-row>\n      <ion-toolbar class=\"ion_toolbar_time_seekbar\">\n        <ion-row>\n          <ion-col size=\"3\">\n            <div class=\"time-display-content cbox\" aria-label=\"\"><span class=\"time-first\" aria-label=\"Time elapsed0:01\"\n                role=\"text\" id=\"lblTime\">00:00</span><span class=\"time-delimiter\" aria-hidden=\"true\">/</span><span\n                class=\"time-second\" aria-label=\"Time duration4:53\" id=\"lblTime2\" role=\"text\">00:00</span></div>\n          </ion-col>\n          <ion-col size=\"7\" class=\"text-center\">\n            <input type=\"range\" step=\"any\" id=\"seekbar\" onchange=\"ChangeTheTime()\">\n          </ion-col>\n          <ion-col size=\"2\" class=\"text-center\">\n          </ion-col>\n        </ion-row>\n      </ion-toolbar>\n    </div> -->\n    <!-- <h2 class=\"ion-padding-horizontal\">\n      Step 2: Supporting Class Dcuments\n    </h2>\n    <ion-row class=\"ion-padding-horizontal\">\n      <ion-col size=\"12\">\n        <ion-button expand=\"full\" [disabled]=\"pdf\" (click)=\"downloadPdf(pdfFile)\">\n          <ion-icon slot=\"start\" src=\"assets/icon/pdf.svg\"></ion-icon>Print Materials\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-input placeholder=\"Entry one (Fill in)\"></ion-input>\n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-input placeholder=\"Entry two (Fill in)\"></ion-input>\n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-textarea rows=\"6\" cols=\"20\" placeholder=\"Entry Three (Fill in)\"></ion-textarea>\n      </ion-col>\n    </ion-row>\n    <h2 class=\"ion-padding-horizontal\">\n      Step 3: Quiz\n    </h2>\n    <ion-row class=\"ion-padding-horizontal ion-margin-bottom\">\n      <ion-col size=\"12\">\n        <ion-button class=\"quiz_button\" expand=\"full\" [disabled]=\"take_quiz\" (click)=\"startQuiz()\">\n          Take quiz now\n        </ion-button>\n      </ion-col>\n    </ion-row> -->\n  <!-- </ion-grid> -->\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>");

/***/ })

}]);
//# sourceMappingURL=pages-testing-testing-module-es2015.js.map