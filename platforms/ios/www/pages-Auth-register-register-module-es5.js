(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-Auth-register-register-module"], {
    /***/
    "/oMm":
    /*!********************************************************!*\
      !*** ./src/app/pages/Auth/register/register.module.ts ***!
      \********************************************************/

    /*! exports provided: RegisterPageModule */

    /***/
    function oMm(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function () {
        return RegisterPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _register_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./register-routing.module */
      "esGQ");
      /* harmony import */


      var _register_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./register.page */
      "7qFq");

      var RegisterPageModule = function RegisterPageModule() {
        _classCallCheck(this, RegisterPageModule);
      };

      RegisterPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _register_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegisterPageRoutingModule"]],
        declarations: [_register_page__WEBPACK_IMPORTED_MODULE_6__["RegisterPage"]]
      })], RegisterPageModule);
      /***/
    },

    /***/
    "5dVO":
    /*!********************************************!*\
      !*** ./src/app/services/loader.service.ts ***!
      \********************************************/

    /*! exports provided: LoaderService */

    /***/
    function dVO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoaderService", function () {
        return LoaderService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var LoaderService = /*#__PURE__*/function () {
        function LoaderService(loadingCtrl) {
          _classCallCheck(this, LoaderService);

          this.loadingCtrl = loadingCtrl;
        }

        _createClass(LoaderService, [{
          key: "showLoader",
          value: function showLoader() {
            this.isBusy = true; // this.loaderToShow = this.loadingCtrl.create({
            //   message: 'Please Wait..'
            // }).then((res) => {
            //   res.present();
            //   // res.onDidDismiss().then((dis) => {
            //   //    console.log('Loading dismissed!',dis);
            //   // });
            // });
            // // this.hideLoader();
          }
        }, {
          key: "hideLoader",
          value: function hideLoader() {
            // setTimeout(()=>{
            //   this.loadingCtrl.dismiss();
            // },100)
            this.isBusy = false;
          }
        }]);

        return LoaderService;
      }();

      LoaderService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoaderService);
      /***/
    },

    /***/
    "7qFq":
    /*!******************************************************!*\
      !*** ./src/app/pages/Auth/register/register.page.ts ***!
      \******************************************************/

    /*! exports provided: RegisterPage */

    /***/
    function qFq(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterPage", function () {
        return RegisterPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_register_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./register.page.html */
      "8vxJ");
      /* harmony import */


      var _register_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./register.page.scss */
      "WQ/L");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/loader.service */
      "5dVO");
      /* harmony import */


      var src_app_services_register_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/register.service */
      "mdPV");
      /* harmony import */


      var _helpers_must_match_validator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../_helpers/must-match.validator */
      "4B9S");
      /* harmony import */


      var _terms_conditions_terms_conditions_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./terms-conditions/terms-conditions.page */
      "Wsv4");

      var RegisterPage = /*#__PURE__*/function () {
        function RegisterPage(navCtrl, menuCtrl, loadingCtrl, formBuilder, alertController, modalCtrl, registerService, loaderService) {
          _classCallCheck(this, RegisterPage);

          this.navCtrl = navCtrl;
          this.menuCtrl = menuCtrl;
          this.loadingCtrl = loadingCtrl;
          this.formBuilder = formBuilder;
          this.alertController = alertController;
          this.modalCtrl = modalCtrl;
          this.registerService = registerService;
          this.loaderService = loaderService;
          this.weight = [];
          this.termConditionsCheck = false;
          this.communityData = [];
          this.showOtherCommunityField = false;
          this.formValue = [];
          this.eye = false;
          this.passwordType = 'password';
          this.c_eye = false;
          this.c_passwordType = 'password';
          this.jsonData = {
            numbers: [{
              description: '4 Feet'
            }, {
              description: '5 Feet'
            }, {
              description: '6 Feet'
            }, {
              description: '7 Feet'
            }],
            inches: [{
              description: '0 inches'
            }, {
              description: '1 inches'
            }, {
              description: '2 inches'
            }, {
              description: '3 inches'
            }, {
              description: '4 inches'
            }, {
              description: '5 inches'
            }, {
              description: '6 inches'
            }, {
              description: '7 inches'
            }, {
              description: '8 inches'
            }, {
              description: '9 inches'
            }, {
              description: '10 inches'
            }, {
              description: '11 inches'
            }, {
              description: '12 inches'
            }]
          };
        }

        _createClass(RegisterPage, [{
          key: "showPassword",
          value: function showPassword(value) {
            if (value == 0) {
              this.eye = true;
              this.passwordType = 'text';
            } else {
              this.c_eye = true;
              this.c_passwordType = 'text';
            }
          }
        }, {
          key: "hidePassword",
          value: function hidePassword(value) {
            if (value == 0) {
              this.eye = false;
              this.passwordType = 'password';
            } else {
              this.c_eye = false;
              this.c_passwordType = 'password';
            }
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.menuCtrl.enable(false);
          }
        }, {
          key: "fetchCommunityData",
          value: function fetchCommunityData() {// this.userService.getCommunityData()
            // .subscribe((res: any) => {
            //   this.communityData = res.data;
            // });
          }
        }, {
          key: "termConditions",
          value: function termConditions() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var modal;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.modalCtrl.create({
                        component: _terms_conditions_terms_conditions_page__WEBPACK_IMPORTED_MODULE_9__["TermsConditionsPage"]
                      });

                    case 2:
                      modal = _context.sent;
                      modal.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "back",
          value: function back() {
            this.navCtrl.back();
          }
        }, {
          key: "unCheckFocusEmail",
          value: function unCheckFocusEmail() {
            var _this = this;

            var formData = new FormData();
            formData.append("email", this.onRegisterForm.value['email']);

            if (this.onRegisterForm.value['email'] != '') {
              this.registerService.checkemail(formData).then(function (data) {
                console.log("data : ", data);
                _this.data_check_any = data;

                if (_this.data_check_any.status == false) {
                  _this.presentAlertError(_this.data_check_any.error_msg);

                  _this.onRegisterForm = _this.formBuilder.group({
                    fname: [{
                      value: _this.onRegisterForm.value['fname'],
                      disabled: false
                    }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                    lname: [{
                      value: _this.onRegisterForm.value['lname'],
                      disabled: false
                    }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                    email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
                    phone_number: [{
                      value: _this.onRegisterForm.value['phone_number'],
                      disabled: false
                    }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                    Username: [{
                      value: _this.onRegisterForm.value['Username'],
                      disabled: false
                    }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                    // password: [null, Validators.compose([Validators.required])],
                    password: [_this.onRegisterForm.value['password'], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])],
                    confirm_password: [_this.onRegisterForm.value['confirm_password'], [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]]
                  });
                }
              });
            }
          }
        }, {
          key: "unCheckFocusPhone",
          value: function unCheckFocusPhone() {
            var _this2 = this;

            var formData = new FormData();
            formData.append("phone", this.onRegisterForm.value['phone_number']); // formData.append("phone", value);

            if (this.onRegisterForm.value['phone_number'] != '') {
              this.registerService.checkphone(formData).then(function (data) {
                console.log("data : ", data);
                _this2.data_check_any = data;

                if (_this2.data_check_any.status == false) {
                  _this2.presentAlertError(_this2.data_check_any.error_msg);

                  _this2.onRegisterForm = _this2.formBuilder.group({
                    fname: [{
                      value: _this2.onRegisterForm.value['fname'],
                      disabled: false
                    }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                    lname: [{
                      value: _this2.onRegisterForm.value['lname'],
                      disabled: false
                    }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                    email: [_this2.onRegisterForm.value['email'], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
                    phone_number: [{
                      value: '',
                      disabled: false
                    }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                    Username: [{
                      value: _this2.onRegisterForm.value['Username'],
                      disabled: false
                    }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                    // password: [null, Validators.compose([Validators.required])],
                    password: [_this2.onRegisterForm.value['password'], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])],
                    confirm_password: [_this2.onRegisterForm.value['confirm_password'], [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]]
                  });
                }
              });
            }
          }
        }, {
          key: "unCheckFocusUsername",
          value: function unCheckFocusUsername() {
            var _this3 = this;

            var formData = new FormData();
            formData.append("username", this.onRegisterForm.value['Username']);

            if (this.onRegisterForm.value['Username'] != '') {
              this.registerService.checkusername(formData).then(function (data) {
                console.log("data : ", data);
                _this3.data_check_any = data;

                if (_this3.data_check_any.status == false) {
                  _this3.presentAlertError(_this3.data_check_any.error_msg);

                  _this3.onRegisterForm = _this3.formBuilder.group({
                    fname: [{
                      value: _this3.onRegisterForm.value['fname'],
                      disabled: false
                    }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                    lname: [{
                      value: _this3.onRegisterForm.value['lname'],
                      disabled: false
                    }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                    email: [_this3.onRegisterForm.value['email'], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
                    phone_number: [{
                      value: _this3.onRegisterForm.value['phone_number'],
                      disabled: false
                    }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                    Username: [{
                      value: '',
                      disabled: false
                    }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                    // password: [null, Validators.compose([Validators.required])],
                    password: [_this3.onRegisterForm.value['password'], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])],
                    confirm_password: [_this3.onRegisterForm.value['confirm_password'], [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]]
                  });
                }
              });
            }
          }
        }, {
          key: "onKeyUp",
          value: function onKeyUp(event) {
            var newValue = event.target.value; // console.log(event.target.value);

            var regExp = new RegExp('^[A-Za-z0-9? ]+$');

            if (!regExp.test(newValue)) {
              event.target.value = newValue.slice(0, -1);
            }
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.onRegisterForm = this.formBuilder.group({
              fname: [{
                value: '',
                disabled: false
              }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
              lname: [{
                value: '',
                disabled: false
              }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
              email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
              phone_number: [{
                value: '',
                disabled: false
              }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
              Username: [{
                value: '',
                disabled: false
              }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
              // password: [null, Validators.compose([Validators.required])],
              password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])],
              confirm_password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]]
            }, {
              validator: Object(_helpers_must_match_validator__WEBPACK_IMPORTED_MODULE_8__["mustMatch"])('password', 'confirm_password')
            });
            this.fetchCommunityData();
          }
        }, {
          key: "setTermsConditions",
          value: function setTermsConditions(event) {
            console.log("this.detail : ", event);
            this.termConditionsCheck = event.detail.checked;
            console.log("this.termConditionsCheck : ", this.termConditionsCheck);
          }
        }, {
          key: "presentAlertSuccess",
          value: function presentAlertSuccess() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this4 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.alertController.create({
                        header: 'Success',
                        cssClass: 'alertCustom',
                        subHeader: 'You have Successfully Registered',
                        message: 'Please Log In to Continue',
                        buttons: [{
                          text: 'Ok',
                          handler: function handler() {
                            console.log;

                            _this4.navCtrl.navigateBack('/');
                          }
                        }]
                      });

                    case 2:
                      alert = _context2.sent;
                      _context2.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "presentAlertError",
          value: function presentAlertError(error) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var alert;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.alertController.create({
                        cssClass: 'alertCustom',
                        header: 'Error',
                        // subHeader: 'Subtitle',
                        message: error,
                        buttons: ['OK']
                      });

                    case 2:
                      alert = _context3.sent;
                      _context3.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "signUp",
          value: function signUp() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this5 = this;

              var formData;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      // this.onRegisterForm.value();
                      this.loaderService.showLoader();
                      console.log(this.onRegisterForm.value);
                      formData = new FormData();
                      formData.append("firstname", this.onRegisterForm.value['fname']);
                      formData.append("lastname", this.onRegisterForm.value['lname']);
                      formData.append("email", this.onRegisterForm.value['email']);
                      formData.append("username", this.onRegisterForm.value['Username']);
                      formData.append("phone", this.onRegisterForm.value['phone_number']);
                      formData.append("password", this.onRegisterForm.value['password']);
                      this.registerService.register(formData).then(function (data) {
                        console.log("data : ", data);
                        var dataObj;
                        dataObj = data;

                        if (dataObj.status) {
                          _this5.presentAlertSuccess();

                          _this5.loaderService.hideLoader();
                        } else {
                          _this5.loaderService.hideLoader();
                        }
                      }); // this.userService
                      //   .register(this.onRegisterForm.value)
                      //   .subscribe((res: any) => {
                      //       console.log(res);
                      //     if (res.status !== false) {
                      //       this.presentAlertSuccess();
                      //       this.navCtrl.navigateRoot('/');
                      //     } else {
                      //       this.presentAlertError(res.error_msg);
                      //     }
                      //   });
                      // const loader = await this.loadingCtrl.create({
                      //   duration: 2000
                      // });
                      // loader.present();
                      // loader.onWillDismiss().then(() => {
                      //   this.navCtrl.navigateRoot('/home-results');
                      // });

                    case 10:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          } // // //

        }, {
          key: "goToLogin",
          value: function goToLogin() {
            this.navCtrl.navigateForward('login');
          }
        }]);

        return RegisterPage;
      }();

      RegisterPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"]
        }, {
          type: src_app_services_register_service__WEBPACK_IMPORTED_MODULE_7__["RegisterService"]
        }, {
          type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"]
        }];
      };

      RegisterPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-register',
        template: _raw_loader_register_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_register_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], RegisterPage);
      /***/
    },

    /***/
    "8vxJ":
    /*!**********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/Auth/register/register.page.html ***!
      \**********************************************************************************************/

    /*! exports provided: default */

    /***/
    function vxJ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar class=\"header_clas ion-text-center\">\n    <ion-icon style=\"zoom:1.5\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <img src=\"assets/logo/logo.png\" />\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n\n  <form [formGroup]=\"onRegisterForm\" class=\"registrationForm\">\n\n    <h2>Create Account</h2>\n\n    <ion-item lines=\"none\">\n      <ion-icon name=\"information-outline\" slot=\"start\"></ion-icon>\n      <ion-input class=\"placeholder\" (keyup)=\"onKeyUp($event)\" placeholder=\"First Name\" type=\"text\" formControlName=\"fname\"></ion-input>\n    </ion-item>\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\"\n        *ngIf=\"onRegisterForm.get('fname').touched && onRegisterForm.get('fname').hasError('required')\">\n        <ion-text color=\"warning\">\n          This field is required\n        </ion-text>\n      </p>\n    </div>\n\n    <ion-item lines=\"none\">\n      <ion-icon name=\"information-outline\" slot=\"start\"></ion-icon>\n      <ion-input class=\"placeholder\" (keyup)=\"onKeyUp($event)\" placeholder=\"Last Name\" type=\"text\" formControlName=\"lname\"></ion-input>\n    </ion-item>\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\"\n        *ngIf=\"onRegisterForm.get('lname').touched && onRegisterForm.get('lname').hasError('required')\">\n        <ion-text color=\"warning\">\n          This field is required\n        </ion-text>\n      </p>\n    </div>\n\n    <ion-item lines=\"none\">\n      <ion-icon name=\"mail-outline\" slot=\"start\"></ion-icon>\n      <ion-input class=\"placeholder\" (ngModelChange) =\"unCheckFocusEmail()\" value=\"email\" placeholder=\"Email\" type=\"email\" formControlName=\"email\"></ion-input>\n    </ion-item>\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\"\n        *ngIf=\"onRegisterForm.get('email').touched && onRegisterForm.get('email').hasError('required')\">\n        <ion-text color=\"warning\">\n          This field is required\n        </ion-text>\n      </p>\n      <p ion-text class=\"text08\"\n        *ngIf=\"onRegisterForm.get('email').touched && onRegisterForm.get('email').hasError('pattern')\">\n        <ion-text color=\"warning\">\n          Please provide a valid email\n        </ion-text>\n      </p>\n    </div>\n\n    <ion-item lines=\"none\">\n      <ion-icon name=\"call-outline\" slot=\"start\"></ion-icon>\n      <ion-input class=\"placeholder\" (ngModelChange) =\"unCheckFocusPhone()\" value=\"phone\" placeholder=\"Phone Number\" type=\"Number\" formControlName=\"phone_number\"></ion-input>\n    </ion-item>\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\"\n        *ngIf=\"onRegisterForm.get('phone_number').touched && onRegisterForm.get('phone_number').hasError('required')\">\n        <ion-text color=\"warning\">\n          This field is required\n        </ion-text>\n      </p>\n    </div>\n\n    <ion-item lines=\"none\">\n      <ion-icon name=\"person-outline\" slot=\"start\"></ion-icon>\n      <ion-input class=\"placeholder\" (keyup)=\"onKeyUp($event)\" (ngModelChange) =\"unCheckFocusUsername()\" value=\"Username\" placeholder=\"Username\" type=\"text\" formControlName=\"Username\"></ion-input>\n    </ion-item>\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\"\n        *ngIf=\"onRegisterForm.get('Username').touched && onRegisterForm.get('Username').hasError('required')\">\n        <ion-text color=\"warning\">\n          This field is required\n        </ion-text>\n      </p>\n    </div>\n\n\n    <ion-item lines=\"none\">\n      <ion-icon name=\"lock-closed-outline\" slot=\"start\"></ion-icon>\n      <ion-input class=\"placeholder\" placeholder=\"Password\" type=\"{{passwordType}}\" formControlName=\"password\"></ion-input>\n      <ion-icon slot=\"end\" color=\"dark\" name=\"eye\" (click)=\"showPassword(0)\" *ngIf=\"!eye\"></ion-icon>\n      <ion-icon slot=\"end\" color=\"dark\" name=\"eye-off\"  (click)=\"hidePassword(0)\" *ngIf=\"eye\"></ion-icon>\n    </ion-item>\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\"\n        *ngIf=\"onRegisterForm.get('password').touched && onRegisterForm.get('password').hasError('required')\">\n        <ion-text color=\"warning\">\n          This field is required\n        </ion-text>\n      </p>\n    </div>\n\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\" *ngIf=\"onRegisterForm.get('password').touched && onRegisterForm.get('password').hasError( 'minlength')\">\n        <ion-text color=\"warning\">\n          Password must be at least 6 characters\n        </ion-text>\n      </p>\n    </div>\n\n    \n    <ion-item lines=\"none\">\n      <ion-icon name=\"lock-closed-outline\" slot=\"start\"></ion-icon>\n      <ion-input class=\"placeholder\" placeholder=\"Repeat Password\" type=\"{{c_passwordType}}\" formControlName=\"confirm_password\"></ion-input>\n      <ion-icon slot=\"end\" color=\"dark\" name=\"eye\" (click)=\"showPassword(1)\" *ngIf=\"!c_eye\"></ion-icon>\n      <ion-icon slot=\"end\" color=\"dark\" name=\"eye-off\"  (click)=\"hidePassword(1)\" *ngIf=\"c_eye\"></ion-icon>\n    </ion-item>\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\"\n        *ngIf=\"onRegisterForm.get('confirm_password').touched && onRegisterForm.get('confirm_password').hasError('required')\">\n        <ion-text color=\"warning\">\n          This field is required\n        </ion-text>\n      </p>\n    </div>\n   \n    <div class=\"error-div\">\n      <p ion-text class=\"text08\"\n        *ngIf=\"onRegisterForm.get('confirm_password').touched && onRegisterForm.get('confirm_password').hasError( 'mustMatch')\">\n        <ion-text color=\"warning\">\n          Passwords Must Match\n        </ion-text>\n      </p>\n    </div>\n\n    <div class=\"termsConditions\">\n      <ion-checkbox (ionChange)=\"setTermsConditions($event)\" class=\"m-checkbox\" mode=\"ios\"></ion-checkbox>\n      <p>Agree to <span (click)=\"termConditions()\" class=\"hyper-link\"><b><u>Terms & Conditions</u></b></span></p>\n    </div>\n    <ion-button (click)=\"signUp()\" [disabled]=\"!onRegisterForm.valid || !termConditionsCheck\">JOIN</ion-button>\n\n  </form>\n\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>";
      /***/
    },

    /***/
    "AIRF":
    /*!***********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/Auth/register/terms-conditions/terms-conditions.page.html ***!
      \***********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function AIRF(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header >\n  <ion-toolbar class=\"header_clas ion-text-center\">\n    <ion-title>Terms & Conditions</ion-title>\n    <ion-icon  slot=\"end\" size=\"large\" name=\"close\" (click)=\"close()\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <div class=\"position-relative text-align-center h-100 w-100\" *ngIf=\"loading\">\n    <h4>Please wait...</h4>\n    <ion-spinner></ion-spinner>\n  </div>\n  <div [innerHTML]=\"termsConditions\" class=\"h-100 w-100\" *ngIf=\"!loading\">\n  </div>\n</ion-content>\n";
      /***/
    },

    /***/
    "WQ/L":
    /*!********************************************************!*\
      !*** ./src/app/pages/Auth/register/register.page.scss ***!
      \********************************************************/

    /*! exports provided: default */

    /***/
    function WQL(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "img {\n  max-width: 50%;\n}\n\n.registrationForm {\n  position: relative;\n  display: flex;\n  flex-direction: column;\n  align-items: stretch;\n}\n\n.registrationForm h2 {\n  font-weight: 600;\n  text-align: center;\n}\n\nion-item {\n  border: 1px solid #0000006e;\n  border-radius: 5px;\n  height: 60px;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  margin: 5px 10px;\n}\n\nion-item ion-label {\n  margin-left: 5px;\n  --color: gray !important;\n}\n\nion-item ion-input {\n  height: 100%;\n  margin-left: 5px;\n  padding: 0px !important;\n}\n\nion-item ion-select {\n  margin-left: 5px;\n}\n\nion-item ion-icon {\n  margin: 0;\n  zoom: 0.7;\n}\n\n.error-div {\n  display: flex;\n  width: 100%;\n  padding-left: 10px;\n}\n\n.error-div p.text08 {\n  margin: 0;\n  margin-top: 2px;\n}\n\n.select-text {\n  position: absolute !important;\n  font-size: 13px !important;\n  right: 32px !important;\n}\n\nion-button {\n  margin: 16px 16px;\n  --background: #079199;\n  font-size: 18px;\n  height: 45px;\n}\n\n.termsConditions {\n  display: flex;\n  align-items: center;\n}\n\n.m-checkbox {\n  margin-left: 15px;\n  margin-right: 10px;\n}\n\n.hyper-link {\n  color: cadetblue;\n}\n\nion-checkbox {\n  background: #FFFFFF;\n  border-width: 1px !important;\n  border-style: solid !important;\n  border-radius: 4px;\n  border-color: blue !important;\n  --height: 25px;\n  --width: 25px;\n  --checkmark-color:blue;\n  --background-checked: #FFFFFF;\n  --border-color: #FFFFFF;\n  --border-color-checked:#FFFFFF;\n  padding: 1px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3JlZ2lzdGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLGNBQUE7QUFBSjs7QUFFSTtFQUNFLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7QUFDTjs7QUFBTTtFQUNFLGdCQUFBO0VBQ0Esa0JBQUE7QUFFUjs7QUFDSTtFQUNFLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtBQUVOOztBQURNO0VBQ0UsZ0JBQUE7RUFDQSx3QkFBQTtBQUdSOztBQURNO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7QUFHUjs7QUFETTtFQUNFLGdCQUFBO0FBR1I7O0FBRE07RUFDRSxTQUFBO0VBQ0EsU0FBQTtBQUdSOztBQUFJO0VBQ0UsYUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQUdOOztBQUZNO0VBQ0UsU0FBQTtFQUNBLGVBQUE7QUFJUjs7QUFESTtFQUNFLDZCQUFBO0VBQ0EsMEJBQUE7RUFDQSxzQkFBQTtBQUlOOztBQUZJO0VBQ0UsaUJBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FBS047O0FBSEk7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7QUFNTjs7QUFKSTtFQUNFLGlCQUFBO0VBQ0Esa0JBQUE7QUFPTjs7QUFMSTtFQUNFLGdCQUFBO0FBUU47O0FBTkk7RUFDRSxtQkFBQTtFQUNDLDRCQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsY0FBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLDZCQUFBO0VBQ0EsdUJBQUE7RUFDQSw4QkFBQTtFQUNBLFlBQUE7QUFTUCIsImZpbGUiOiJyZWdpc3Rlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBhcHAtcmVnaXN0ZXIge1xuICBpbWd7XG4gICAgbWF4LXdpZHRoOiA1MCU7XG4gIH1cbiAgICAucmVnaXN0cmF0aW9uRm9ybSB7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGFsaWduLWl0ZW1zOiBzdHJldGNoO1xuICAgICAgaDIge1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICB9XG4gICAgfVxuICAgIGlvbi1pdGVtIHtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMDAwMDA2ZTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAgIGhlaWdodDogNjBweDtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBtYXJnaW46IDVweCAxMHB4O1xuICAgICAgaW9uLWxhYmVsIHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgICAgICAgLS1jb2xvcjogZ3JheSAhaW1wb3J0YW50O1xuICAgICAgfVxuICAgICAgaW9uLWlucHV0IHtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xuICAgICAgICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbiAgICAgIH1cbiAgICAgIGlvbi1zZWxlY3Qge1xuICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xuICAgICAgfVxuICAgICAgaW9uLWljb24ge1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIHpvb206IDAuNztcbiAgICAgIH1cbiAgICB9XG4gICAgLmVycm9yLWRpdiB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgICBwLnRleHQwOCB7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgbWFyZ2luLXRvcDogMnB4O1xuICAgICAgfVxuICAgIH1cbiAgICAuc2VsZWN0LXRleHQge1xuICAgICAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XG4gICAgICBmb250LXNpemU6IDEzcHggIWltcG9ydGFudDtcbiAgICAgIHJpZ2h0OiAzMnB4ICFpbXBvcnRhbnQ7XG4gICAgfVxuICAgIGlvbi1idXR0b24ge1xuICAgICAgbWFyZ2luOiAxNnB4IDE2cHg7XG4gICAgICAtLWJhY2tncm91bmQ6ICMwNzkxOTk7XG4gICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICBoZWlnaHQ6IDQ1cHg7XG4gICAgfVxuICAgIC50ZXJtc0NvbmRpdGlvbnMge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgfVxuICAgIC5tLWNoZWNrYm94IHtcbiAgICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xuICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgIH1cbiAgICAuaHlwZXItbGluayB7XG4gICAgICBjb2xvcjogY2FkZXRibHVlO1xuICAgIH1cbiAgICBpb24tY2hlY2tib3h7XG4gICAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgICAgIGJvcmRlci13aWR0aDogMXB4ICFpbXBvcnRhbnQ7XG4gICAgICAgYm9yZGVyLXN0eWxlOiBzb2xpZCAhaW1wb3J0YW50O1xuICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgICBib3JkZXItY29sb3IgOiBibHVlICFpbXBvcnRhbnQ7XG4gICAgICAgLS1oZWlnaHQgOiAyNXB4O1xuICAgICAgIC0td2lkdGg6IDI1cHg7XG4gICAgICAgLS1jaGVja21hcmstY29sb3I6Ymx1ZTtcbiAgICAgICAtLWJhY2tncm91bmQtY2hlY2tlZDogI0ZGRkZGRjtcbiAgICAgICAtLWJvcmRlci1jb2xvcjogI0ZGRkZGRjtcbiAgICAgICAtLWJvcmRlci1jb2xvci1jaGVja2VkIDojRkZGRkZGO1xuICAgICAgIHBhZGRpbmc6IDFweDtcbiAgICAgfVxuLy8gICB9Il19 */";
      /***/
    },

    /***/
    "WY8I":
    /*!*********************************************************************************!*\
      !*** ./src/app/pages/Auth/register/terms-conditions/terms-conditions.page.scss ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function WY8I(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0ZXJtcy1jb25kaXRpb25zLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "Wsv4":
    /*!*******************************************************************************!*\
      !*** ./src/app/pages/Auth/register/terms-conditions/terms-conditions.page.ts ***!
      \*******************************************************************************/

    /*! exports provided: TermsConditionsPage */

    /***/
    function Wsv4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TermsConditionsPage", function () {
        return TermsConditionsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_terms_conditions_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./terms-conditions.page.html */
      "AIRF");
      /* harmony import */


      var _terms_conditions_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./terms-conditions.page.scss */
      "WY8I");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var TermsConditionsPage = /*#__PURE__*/function () {
        function TermsConditionsPage(modalCtrl) {
          _classCallCheck(this, TermsConditionsPage);

          this.modalCtrl = modalCtrl;
        }

        _createClass(TermsConditionsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "close",
          value: function close() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.modalCtrl.dismiss();

                    case 2:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }]);

        return TermsConditionsPage;
      }();

      TermsConditionsPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }];
      };

      TermsConditionsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-terms-conditions',
        template: _raw_loader_terms_conditions_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_terms_conditions_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], TermsConditionsPage);
      /***/
    },

    /***/
    "esGQ":
    /*!****************************************************************!*\
      !*** ./src/app/pages/Auth/register/register-routing.module.ts ***!
      \****************************************************************/

    /*! exports provided: RegisterPageRoutingModule */

    /***/
    function esGQ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterPageRoutingModule", function () {
        return RegisterPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _register_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./register.page */
      "7qFq");

      var routes = [{
        path: '',
        component: _register_page__WEBPACK_IMPORTED_MODULE_3__["RegisterPage"]
      }, {
        path: 'terms-conditions',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | Auth-register-terms-conditions-terms-conditions-module */
          "Auth-register-terms-conditions-terms-conditions-module").then(__webpack_require__.bind(null,
          /*! ../../Auth/register/terms-conditions/terms-conditions.module */
          "Rbu+")).then(function (m) {
            return m.TermsConditionsPageModule;
          });
        }
      }];

      var RegisterPageRoutingModule = function RegisterPageRoutingModule() {
        _classCallCheck(this, RegisterPageRoutingModule);
      };

      RegisterPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], RegisterPageRoutingModule);
      /***/
    },

    /***/
    "mdPV":
    /*!**********************************************!*\
      !*** ./src/app/services/register.service.ts ***!
      \**********************************************/

    /*! exports provided: RegisterService */

    /***/
    function mdPV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterService", function () {
        return RegisterService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");
      /* harmony import */


      var _network_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./network.service */
      "dwY0");

      var RegisterService = /*#__PURE__*/function () {
        function RegisterService(http, networkService) {
          _classCallCheck(this, RegisterService);

          this.http = http;
          this.networkService = networkService;
          this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
        }

        _createClass(RegisterService, [{
          key: "checkemail",
          value: function checkemail(formData) {
            var _this6 = this;

            return new Promise(function (resolve, reject) {
              _this6.networkService.onNetworkChange().subscribe(function (status) {
                if (status == _network_service__WEBPACK_IMPORTED_MODULE_4__["ConnectionStatus"].Online) {
                  console.log(_this6.apiUrl + 'checkemail.php');
                  formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

                  _this6.http.post(_this6.apiUrl + 'checkemail.php', formData).subscribe(function (res) {
                    resolve(res);
                  }, function (err) {
                    reject(err);
                    console.log('something went wrong please try again');
                  });
                }
              });
            });
          }
        }, {
          key: "checkusername",
          value: function checkusername(formData) {
            var _this7 = this;

            return new Promise(function (resolve, reject) {
              _this7.networkService.onNetworkChange().subscribe(function (status) {
                if (status == _network_service__WEBPACK_IMPORTED_MODULE_4__["ConnectionStatus"].Online) {
                  console.log(_this7.apiUrl + 'checkusername.php');
                  formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

                  _this7.http.post(_this7.apiUrl + 'checkusername.php', formData).subscribe(function (res) {
                    resolve(res);
                  }, function (err) {
                    reject(err);
                    console.log('something went wrong please try again');
                  });
                }
              });
            });
          }
        }, {
          key: "checkphone",
          value: function checkphone(formData) {
            var _this8 = this;

            return new Promise(function (resolve, reject) {
              _this8.networkService.onNetworkChange().subscribe(function (status) {
                if (status == _network_service__WEBPACK_IMPORTED_MODULE_4__["ConnectionStatus"].Online) {
                  console.log(_this8.apiUrl + 'checkphone.php');
                  formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

                  _this8.http.post(_this8.apiUrl + 'checkphone.php', formData).subscribe(function (res) {
                    resolve(res);
                  }, function (err) {
                    reject(err);
                    console.log('something went wrong please try again');
                  });
                }
              });
            });
          }
        }, {
          key: "register",
          value: function register(formData) {
            var _this9 = this;

            return new Promise(function (resolve, reject) {
              _this9.networkService.onNetworkChange().subscribe(function (status) {
                if (status == _network_service__WEBPACK_IMPORTED_MODULE_4__["ConnectionStatus"].Online) {
                  console.log(_this9.apiUrl + 'register.php');
                  formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

                  _this9.http.post(_this9.apiUrl + 'register.php', formData).subscribe(function (res) {
                    resolve(res);
                  }, function (err) {
                    reject(err);
                    console.log('something went wrong please try again');
                  });
                }
              });
            });
          }
        }]);

        return RegisterService;
      }();

      RegisterService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }, {
          type: _network_service__WEBPACK_IMPORTED_MODULE_4__["NetworkService"]
        }];
      };

      RegisterService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], RegisterService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-Auth-register-register-module-es5.js.map