(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-member-member-module"],{

/***/ "2iAc":
/*!*******************************************************!*\
  !*** ./src/app/pages/member/member-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: MemberPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberPageRoutingModule", function() { return MemberPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _member_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./member.page */ "lmpU");




const routes = [
    {
        path: '',
        component: _member_page__WEBPACK_IMPORTED_MODULE_3__["MemberPage"]
    }
];
let MemberPageRoutingModule = class MemberPageRoutingModule {
};
MemberPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MemberPageRoutingModule);



/***/ }),

/***/ "31be":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/member/member.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header title=\"Employees\"></header>\n<ion-content>\n\n  <ion-row>\n    <ion-col size=\"8\">\n      <ion-searchbar type=\"text\" debounce=\"500\" [(ngModel)]=\"search_value\" (ionChange)=\"setFilteredItems($event,1)\"\n        (ionClear)=\"claerSearcher()\"></ion-searchbar>\n    </ion-col>\n    <ion-col size=\"4\">\n      <p class=\"p_style\">Sort by</p>\n      <ion-select class=\"ion_select\" interface=\"popover\" [(ngModel)]=\"sortBy\"\n        (ionChange)=\"sortFunction(sortBy)\">\n        <ion-select-option value=\"n\">Newest</ion-select-option>\n        <ion-select-option value=\"o\">Oldest</ion-select-option>\n        <ion-select-option value=\"f\">First Name</ion-select-option>\n        <ion-select-option value=\"l\">Last Name</ion-select-option>\n        <ion-select-option value=\"p\">Phone</ion-select-option>\n        <ion-select-option value=\"e\">Email</ion-select-option>\n      </ion-select>\n    </ion-col>\n  </ion-row>\n  <!-- <div *ngIf=\"searching\" class=\"spinner-container\">\n    <ion-spinner></ion-spinner>\n  </div> -->\n\n  <ion-list>\n    <ion-item *ngFor=\"let item of items\">\n      {{ item.firstname }}\n    </ion-item>\n  </ion-list>\n\n\n  <!-- Custom Table -->\n  <div class=\"bg-white\">\n    <!-- Header -->\n    <ion-grid>\n      <ion-row class=\"table-wrapper \">\n        <!-- <ion-item-divider> -->\n        <ion-col size=\"4\">\n          <ion-label>\n            <h5>Name</h5>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-label>\n            <h5>Phone / Email</h5>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\" class=\"ion-text-end\">\n          <ion-label>\n            <!-- <h5>Setting</h5> -->\n            <ion-icon name=\"settings\"></ion-icon>\n          </ion-label>\n        </ion-col>\n        <!-- </ion-item-divider> -->\n\n      </ion-row>\n      <ion-row *ngFor=\"let item of membersData; let i=index\">\n        <ion-col size=\"4\">\n          <ion-label>\n            <p *ngIf=\"item.firstname || item.lastname\" class=\"col_style\">{{item.firstname}} {{item.lastname}}</p>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-label>\n            <p *ngIf=\"item.phone\" class=\"d-block\">{{item.phone}}</p>\n            <p *ngIf=\"item.email != null || item.email != ''\" class=\"d-block\">{{item.email}}</p>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-select value=\"\" [interfaceOptions]=\"customAlertOptions\" (ionChange)=\"selectValue($event, i,item)\"\n            okText=\"Okay\" cancelText=\"Dismiss\">\n            <ion-select-option value=\"sms\">Send SMS</ion-select-option>\n            <ion-select-option value=\"email\">Send Email</ion-select-option>\n            <ion-select-option value=\"contact\">Change Contact Group</ion-select-option>\n            <ion-select-option value=\"edit\">Edit Contact</ion-select-option>\n          </ion-select>\n        </ion-col>\n        <hr>\n      </ion-row>\n\n    </ion-grid>\n    <!-- No Data -->\n    <h4 class=\"clr-medium ion-margin-vertical ion-text-center\" *ngIf=\"isCheck == true\">\n      No Record Found\n    </h4>\n  </div>\n  <!-- ​REFRESHER -->\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"refreshData($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <!-- INFINITE SCROLL -->\n  <ion-infinite-scroll *ngIf=\"currentPage<lastPage\" threshold=\"10px\" (ionInfinite)=\"loadMoreData(1, $event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more items...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>");

/***/ }),

/***/ "5dVO":
/*!********************************************!*\
  !*** ./src/app/services/loader.service.ts ***!
  \********************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let LoaderService = class LoaderService {
    constructor(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    showLoader() {
        this.isBusy = true;
        // this.loaderToShow = this.loadingCtrl.create({
        //   message: 'Please Wait..'
        // }).then((res) => {
        //   res.present();
        //   // res.onDidDismiss().then((dis) => {
        //   //    console.log('Loading dismissed!',dis);
        //   // });
        // });
        // // this.hideLoader();
    }
    hideLoader() {
        // setTimeout(()=>{
        //   this.loadingCtrl.dismiss();
        // },100)
        this.isBusy = false;
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ "87QP":
/*!***********************************************!*\
  !*** ./src/app/pages/member/member.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".table-wrapper ion-label {\n  white-space: nowrap !important;\n}\n.table-wrapper ion-label p {\n  font-size: 12px;\n}\n.table-wrapper .mb-0 {\n  margin-bottom: 0px !important;\n}\n.col_style {\n  color: #7d7d7d !important;\n  font-size: 12px !important;\n}\nion-row {\n  border-bottom: 1px solid #dcdcdc;\n}\n.p_style {\n  margin: 0px;\n  padding: 5px 0px 0px 5px;\n}\n.ion_select {\n  color: #7d7d7d !important;\n  font-size: 12px !important;\n  padding: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL21lbWJlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDRSw4QkFBQTtBQUFOO0FBRU07RUFDRSxlQUFBO0FBQVI7QUFJSTtFQUNFLDZCQUFBO0FBRk47QUFNRTtFQUNFLHlCQUFBO0VBQ0EsMEJBQUE7QUFISjtBQU1FO0VBQ0UsZ0NBQUE7QUFISjtBQU1FO0VBQ0UsV0FBQTtFQUNBLHdCQUFBO0FBSEo7QUFNRTtFQUNFLHlCQUFBO0VBQ0EsMEJBQUE7RUFDQSxZQUFBO0FBSEoiLCJmaWxlIjoibWVtYmVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50YWJsZS13cmFwcGVyIHtcbiAgICBpb24tbGFiZWwge1xuICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcCAhaW1wb3J0YW50O1xuICBcbiAgICAgIHAge1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICB9XG4gICAgfVxuICBcbiAgICAubWItMCB7XG4gICAgICBtYXJnaW4tYm90dG9tOiAwcHggIWltcG9ydGFudDtcbiAgICB9XG4gIH1cbiAgXG4gIC5jb2xfc3R5bGUge1xuICAgIGNvbG9yOiAjN2Q3ZDdkICFpbXBvcnRhbnQ7XG4gICAgZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnQ7XG4gIH1cbiAgXG4gIGlvbi1yb3cge1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGNkY2RjO1xuICB9XG4gIFxuICAucF9zdHlsZSB7XG4gICAgbWFyZ2luOiAwcHg7XG4gICAgcGFkZGluZzogNXB4IDBweCAwcHggNXB4O1xuICB9XG4gIFxuICAuaW9uX3NlbGVjdCB7XG4gICAgY29sb3I6ICM3ZDdkN2QgIWltcG9ydGFudDtcbiAgICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiA1cHg7XG4gIH0iXX0= */");

/***/ }),

/***/ "9Qnf":
/*!***********************************************!*\
  !*** ./src/app/pages/member/member.module.ts ***!
  \***********************************************/
/*! exports provided: MemberPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberPageModule", function() { return MemberPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _member_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./member-routing.module */ "2iAc");
/* harmony import */ var _member_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./member.page */ "lmpU");
/* harmony import */ var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modules/shared/shared.module */ "FpXt");








let MemberPageModule = class MemberPageModule {
};
MemberPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _member_routing_module__WEBPACK_IMPORTED_MODULE_5__["MemberPageRoutingModule"],
            src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_member_page__WEBPACK_IMPORTED_MODULE_6__["MemberPage"]]
    })
], MemberPageModule);



/***/ }),

/***/ "XPT/":
/*!********************************************!*\
  !*** ./src/app/services/member.service.ts ***!
  \********************************************/
/*! exports provided: MemberService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberService", function() { return MemberService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");




let MemberService = class MemberService {
    constructor(http) {
        this.http = http;
        this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    getMembers(formData) {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'members.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'members.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
};
MemberService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
MemberService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], MemberService);



/***/ }),

/***/ "lmpU":
/*!*********************************************!*\
  !*** ./src/app/pages/member/member.page.ts ***!
  \*********************************************/
/*! exports provided: MemberPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MemberPage", function() { return MemberPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_member_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./member.page.html */ "31be");
/* harmony import */ var _member_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./member.page.scss */ "87QP");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/alert.service */ "3LUQ");
/* harmony import */ var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/loader.service */ "5dVO");
/* harmony import */ var src_app_services_member_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/member.service */ "XPT/");









let MemberPage = class MemberPage {
    constructor(navCtrl, memberService, loaderService, alertService, router, cdr) {
        this.navCtrl = navCtrl;
        this.memberService = memberService;
        this.loaderService = loaderService;
        this.alertService = alertService;
        this.router = router;
        this.cdr = cdr;
        // @ViewChild('select_value', { static: false }) select_value: IonSelect;
        this.customAlertOptions = {
        // header: 'Pizza Toppings',
        // subHeader: 'Select your toppings',
        // message: '$1.00 per topping',
        // translucent: true
        };
        this.loading = false;
        this.currentPage = 1;
        this.lastPage = 1;
        this.isCheck = false;
        this.sortBy = 'n';
    }
    ngOnInit() {
    }
    ionViewDidLoad() {
    }
    sortFunction(sortBy) {
        console.log(sortBy);
        if (sortBy == 'n') {
            this.getMembers(this.currentPage, this.search_value, '');
        }
        else {
            this.getMembers(this.currentPage, this.search_value, this.sortBy);
        }
    }
    selectValue(value, i, item_obj) {
        console.log(value.detail.value);
        console.log(i);
        console.log(item_obj);
        if (value.detail.value == 'email') {
            if (item_obj.email == 'null') {
                // this.select_value = null
                setTimeout(() => {
                    this.cdr.detectChanges();
                }, 500);
                console.log("this.select_value : ", this.select_value);
                this.alertService.generalAlert('No Email Found.');
            }
            else {
                let navigationExtras = {
                    state: {
                        item_obj: item_obj,
                        pageName: 'member'
                    }
                };
                // this.router.navigate(['post-add-second', navigationExtras]);
                this.router.navigate(['send-email-contact'], navigationExtras);
            }
        }
        if (value.detail.value == 'sms') {
            if (item_obj.phone == 'null') {
                this.alertService.generalAlert('No Phone Found.');
            }
            else {
                let navigationExtras = {
                    state: {
                        item_obj: item_obj,
                        pageName: 'member'
                    }
                };
                // this.router.navigate(['post-add-second', navigationExtras]);
                this.router.navigate(['send-sms-contact'], navigationExtras);
            }
        }
        if (value.detail.value == 'contact') {
            if (item_obj.phone == 'null') {
                this.alertService.generalAlert('This contact has no phone number.');
            }
            else {
                // let navigationExtras: NavigationExtras = {
                //   state: {
                //     item_obj: item_obj,
                // pageName: 'member'
                //   }
                // };
                // // this.router.navigate(['post-add-second', navigationExtras]);
                // this.router.navigate(['send-sms-contact'], navigationExtras);
            }
        }
        if (value.detail.value == 'edit') {
            if (item_obj.phone == 'null') {
                this.alertService.generalAlert('This contact has no phone number.');
            }
            else {
                // let navigationExtras: NavigationExtras = {
                //   state: {
                //     item_obj: item_obj,
                // pageName: 'member'
                //   }
                // };
                // // this.router.navigate(['post-add-second', navigationExtras]);
                // this.router.navigate(['send-sms-contact'], navigationExtras);
            }
        }
    }
    claerSearcher() {
        console.log("claerSearcher");
        this.membersData = [];
        this.currentPage = 1;
        this.getMembers(this.currentPage, '', this.sortBy);
    }
    setFilteredItems(ev, currentPage) {
        console.log(currentPage);
        const val = ev.target.value;
        console.log(val);
        var str = new String(val);
        var len = str.length;
        // if the value is an empty string don't filter the items
        if (len >= 3 && val && val.trim() !== '') {
            if (this.sortBy == 'n') {
                this.getMembers(this.currentPage, this.search_value, '');
            }
            else {
                this.getMembers(this.currentPage, this.search_value, this.sortBy);
            }
            // // this.isItemAvailable = true;
            // // this.currentPage = 1;
            // let formData = new FormData();
            // formData.append("PageNumber", currentPage);
            // formData.append("searchstring", val);
            // formData.append("aid", localStorage.getItem('user_id'));
            // this.searchService.searchContacts(formData).then((data: any) => {
            //   console.log("data : ", data)
            //   this.dataObj = data
            //   // this.currentPage = this.dataObj.CurrentPage;
            //   // this.lastPage = this.dataObj.total;
            //   // if (this.currentPage == 1) {
            //     this.membersData = this.dataObj.contacts;
            //     if(this.membersData.length == 0){
            //       this.isCheck = true;
            //     }
            //   // } else {
            //   //   this.membersData = [...this.membersData, ...this.dataObj.contacts];
            //   // }
            //   // this.loaderService.hideLoader();
            // },err => {
            //   this.isCheck = true;
            //     // this.loaderService.hideLoader();
            //   });
        }
        else {
            // this.isItemAvailable = false;
        }
        // this.items = this.filterItems(this.searchTerm);
    }
    ionViewWillEnter() {
        this.membersData = [];
        this.currentPage = 1;
        this.getMembers(this.currentPage, '', '');
    }
    getMembers(currentPage, search_value, sort_value, event = null) {
        this.loaderService.showLoader();
        let formData = new FormData();
        formData.append("PageNumber", currentPage);
        if (search_value) {
            formData.append("searchstring", search_value);
        }
        if (sort_value) {
            formData.append("sort", sort_value);
        }
        formData.append("aid", localStorage.getItem('user_id'));
        this.memberService.getMembers(formData).then((data) => {
            console.log("data : ", data);
            this.dataObj = data;
            this.currentPage = this.dataObj.CurrentPage;
            this.lastPage = this.dataObj.total;
            if (this.currentPage == 1) {
                this.membersData = this.dataObj.members;
                if (this.membersData.length == 0) {
                    this.isCheck = true;
                }
            }
            else {
                this.membersData = [...this.membersData, ...this.dataObj.members];
            }
            this.loaderService.hideLoader();
            if (event) {
                event.target.complete();
            }
        }, err => {
            this.loaderService.hideLoader();
            this.isCheck = true;
            if (event) {
                event.target.complete();
            }
        });
    }
    refreshData(event) {
        this.currentPage = 1;
        this.getMembers(1, '', '', event);
    }
    ConvertToInt(currentPage) {
        return parseInt(currentPage);
    }
    loadMoreData(value, event) {
        this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value);
        this.getMembers(this.currentPage, this.search_value, this.sortBy, event);
    }
    back() {
        this.navCtrl.back();
    }
};
MemberPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: src_app_services_member_service__WEBPACK_IMPORTED_MODULE_8__["MemberService"] },
    { type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_7__["LoaderService"] },
    { type: src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ChangeDetectorRef"] }
];
MemberPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-member',
        template: _raw_loader_member_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_member_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MemberPage);



/***/ })

}]);
//# sourceMappingURL=pages-member-member-module-es2015.js.map