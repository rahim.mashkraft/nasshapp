(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-contact-table-contact-table-module"],{

/***/ "5dVO":
/*!********************************************!*\
  !*** ./src/app/services/loader.service.ts ***!
  \********************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let LoaderService = class LoaderService {
    constructor(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    showLoader() {
        this.isBusy = true;
        // this.loaderToShow = this.loadingCtrl.create({
        //   message: 'Please Wait..'
        // }).then((res) => {
        //   res.present();
        //   // res.onDidDismiss().then((dis) => {
        //   //    console.log('Loading dismissed!',dis);
        //   // });
        // });
        // // this.hideLoader();
    }
    hideLoader() {
        // setTimeout(()=>{
        //   this.loadingCtrl.dismiss();
        // },100)
        this.isBusy = false;
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ "9cAt":
/*!*************************************************************!*\
  !*** ./src/app/pages/contact-table/contact-table.module.ts ***!
  \*************************************************************/
/*! exports provided: ContactTablePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactTablePageModule", function() { return ContactTablePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _contact_table_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contact-table-routing.module */ "nqI4");
/* harmony import */ var _contact_table_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contact-table.page */ "WgAM");
/* harmony import */ var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modules/shared/shared.module */ "FpXt");








let ContactTablePageModule = class ContactTablePageModule {
};
ContactTablePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _contact_table_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactTablePageRoutingModule"],
            src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_contact_table_page__WEBPACK_IMPORTED_MODULE_6__["ContactTablePage"]]
    })
], ContactTablePageModule);



/***/ }),

/***/ "WgAM":
/*!***********************************************************!*\
  !*** ./src/app/pages/contact-table/contact-table.page.ts ***!
  \***********************************************************/
/*! exports provided: ContactTablePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactTablePage", function() { return ContactTablePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_contact_table_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./contact-table.page.html */ "bd5s");
/* harmony import */ var _contact_table_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./contact-table.page.scss */ "qO6M");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/alert.service */ "3LUQ");
/* harmony import */ var src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/contact-groups.service */ "rtEV");
/* harmony import */ var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/loader.service */ "5dVO");









let ContactTablePage = class ContactTablePage {
    constructor(navCtrl, contactGroupsService, loaderService, alertService, router, cdr) {
        this.navCtrl = navCtrl;
        this.contactGroupsService = contactGroupsService;
        this.loaderService = loaderService;
        this.alertService = alertService;
        this.router = router;
        this.cdr = cdr;
        // @ViewChild('select_value', { static: false }) select_value: IonSelect;
        this.customAlertOptions = {
        // header: 'Pizza Toppings',
        // subHeader: 'Select your toppings',
        // message: '$1.00 per topping',
        // translucent: true
        };
        this.loading = false;
        this.currentPage = 1;
        this.lastPage = 1;
        this.isCheck = false;
        this.sortBy = 'n';
    }
    ngOnInit() {
    }
    ionViewDidLoad() {
    }
    selectValue(value, i, item_obj) {
        console.log(value.detail.value);
        console.log(i);
        console.log(item_obj);
        if (value.detail.value == 'email') {
            if (item_obj.email == 'null') {
                // this.select_value = null
                setTimeout(() => {
                    this.cdr.detectChanges();
                }, 500);
                console.log("this.select_value : ", this.select_value);
                this.alertService.generalAlert('No Email Found.');
            }
            else {
                let navigationExtras = {
                    state: {
                        item_obj: item_obj,
                        pageName: 'contact'
                    }
                };
                // this.router.navigate(['post-add-second', navigationExtras]);
                this.router.navigate(['send-email-contact'], navigationExtras);
            }
        }
        if (value.detail.value == 'sms') {
            if (item_obj.phone == 'null') {
                this.alertService.generalAlert('No Phone Found.');
            }
            else {
                let navigationExtras = {
                    state: {
                        item_obj: item_obj,
                        pageName: 'contact'
                    }
                };
                // this.router.navigate(['post-add-second', navigationExtras]);
                this.router.navigate(['send-sms-contact'], navigationExtras);
            }
        }
        if (value.detail.value == 'contact') {
            if (item_obj.phone == 'null') {
                this.alertService.generalAlert('This contact has no phone number.');
            }
            else {
                // let navigationExtras: NavigationExtras = {
                //   state: {
                //     item_obj: item_obj,
                // pageName: 'contact'
                //   }
                // };
                // // this.router.navigate(['post-add-second', navigationExtras]);
                // this.router.navigate(['send-sms-contact'], navigationExtras);
            }
        }
        if (value.detail.value == 'edit') {
            if (item_obj.phone == 'null') {
                this.alertService.generalAlert('This contact has no phone number.');
            }
            else {
                // let navigationExtras: NavigationExtras = {
                //   state: {
                //     item_obj: item_obj,
                // pageName: 'contact'
                //   }
                // };
                // // this.router.navigate(['post-add-second', navigationExtras]);
                // this.router.navigate(['send-sms-contact'], navigationExtras);
            }
        }
    }
    sortFunction(sortBy) {
        console.log(sortBy);
        if (sortBy == 'n') {
            this.getContacts(this.currentPage, this.search_value, '');
        }
        else {
            this.getContacts(this.currentPage, this.search_value, this.sortBy);
        }
    }
    claerSearcher() {
        console.log("claerSearcher");
        this.contactsData = [];
        this.currentPage = 1;
        this.getContacts(this.currentPage, '', this.sortBy);
    }
    setFilteredItems(ev, currentPage) {
        console.log(currentPage);
        const val = ev.target.value;
        console.log(val);
        var str = new String(val);
        var len = str.length;
        // if the value is an empty string don't filter the items
        if (len >= 3 && val && val.trim() !== '') {
            if (this.sortBy == 'n') {
                this.getContacts(this.currentPage, this.search_value, '');
            }
            else {
                this.getContacts(this.currentPage, this.search_value, this.sortBy);
            }
            // this.isItemAvailable = true;
            // this.currentPage = 1;
            // let formData = new FormData();
            // formData.append("PageNumber", currentPage);
            // formData.append("searchstring", val);
            // formData.append("aid", localStorage.getItem('user_id'));
            // this.searchService.searchContacts(formData).then((data: any) => {
            //   console.log("data : ", data)
            //   this.dataObj = data
            //   // this.currentPage = this.dataObj.CurrentPage;
            //   // this.lastPage = this.dataObj.total;
            //   // if (this.currentPage == 1) {
            //     this.contactsData = this.dataObj.contacts;
            //     if(this.contactsData.length == 0){
            //       this.isCheck = true;
            //     }
            //   // } else {
            //   //   this.contactsData = [...this.contactsData, ...this.dataObj.contacts];
            //   // }
            //   // this.loaderService.hideLoader();
            // },err => {
            //   this.isCheck = true;
            //     // this.loaderService.hideLoader();
            //   });
        }
        else {
            // this.isItemAvailable = false;
        }
        // this.items = this.filterItems(this.searchTerm);
    }
    ionViewWillEnter() {
        this.contactsData = [];
        this.currentPage = 1;
        this.getContacts(this.currentPage, '', '');
    }
    getContacts(currentPage, search_value, sort_value, event = null) {
        this.loaderService.showLoader();
        let formData = new FormData();
        formData.append("PageNumber", currentPage);
        if (search_value) {
            formData.append("searchstring", search_value);
        }
        if (sort_value) {
            formData.append("sort", sort_value);
        }
        formData.append("aid", localStorage.getItem('user_id'));
        this.contactGroupsService.getContacts(formData).then((data) => {
            console.log("data : ", data);
            this.dataObj = data;
            this.currentPage = this.dataObj.CurrentPage;
            this.lastPage = this.dataObj.total;
            if (this.currentPage == 1) {
                this.contactsData = this.dataObj.contacts;
                if (this.contactsData.length == 0) {
                    this.isCheck = true;
                }
            }
            else {
                this.contactsData = [...this.contactsData, ...this.dataObj.contacts];
            }
            this.loaderService.hideLoader();
            if (event) {
                event.target.complete();
            }
        }, err => {
            this.loaderService.hideLoader();
            this.isCheck = true;
            if (event) {
                event.target.complete();
            }
        });
    }
    refreshData(event) {
        this.currentPage = 1;
        this.getContacts(1, '', '', event);
    }
    ConvertToInt(currentPage) {
        return parseInt(currentPage);
    }
    loadMoreData(value, event) {
        this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value);
        this.getContacts(this.currentPage, this.search_value, this.sortBy, event);
    }
    back() {
        this.navCtrl.back();
    }
};
ContactTablePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_7__["ContactGroupsService"] },
    { type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_8__["LoaderService"] },
    { type: src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ChangeDetectorRef"] }
];
ContactTablePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-contact-table',
        template: _raw_loader_contact_table_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_contact_table_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ContactTablePage);



/***/ }),

/***/ "bd5s":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contact-table/contact-table.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">Contacts</ion-title>\n    <ion-icon slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" routerLink=\"/tabs/tab1\"></ion-icon>\n  </ion-toolbar>\n</ion-header> -->\n<header title=\"Contacts\"></header>\n<ion-content>\n\n<ion-row>\n  <ion-col size=\"8\">  <ion-searchbar\n    type=\"text\" debounce=\"500\"\n    [(ngModel)]=\"search_value\"\n      (ionChange)=\"setFilteredItems($event,1)\"\n      (ionClear)=\"claerSearcher()\"\n    ></ion-searchbar></ion-col>\n  <ion-col size=\"4\">\n    <p class=\"p_style\">Sort by</p>\n    <ion-select class=\"ion_select\" interface=\"popover\" [(ngModel)]=\"sortBy\" (ionChange)=\"sortFunction(sortBy)\">\n      <ion-select-option value=\"n\">Newest</ion-select-option>\n      <ion-select-option value=\"o\">Oldest</ion-select-option>\n      <ion-select-option value=\"f\">First Name</ion-select-option>\n      <ion-select-option value=\"l\">Last Name</ion-select-option>\n      <ion-select-option value=\"p\">Phone</ion-select-option>\n      <ion-select-option value=\"e\">Email</ion-select-option>\n    </ion-select>\n  </ion-col>\n</ion-row>\n  <!-- <div *ngIf=\"searching\" class=\"spinner-container\">\n    <ion-spinner></ion-spinner>\n  </div> -->\n\n  <!-- <ion-list>\n    <ion-item *ngFor=\"let item of items\">\n      {{ item.firstname }}\n    </ion-item>\n  </ion-list> -->\n\n  \n  <!-- Custom Table -->\n  <div class=\"bg-white\">\n    <!-- Header -->\n    <ion-grid>\n      <ion-row class=\"table-wrapper \">\n        <!-- <ion-item-divider> -->\n        <ion-col size=\"4\">\n          <ion-label>\n            <h5>Name</h5>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-label>\n            <h5>Phone / Email</h5>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\" class=\"ion-text-end\">\n          <ion-label>\n            <!-- <h5>Setting</h5> -->\n            <ion-icon name=\"settings\"></ion-icon>\n          </ion-label>\n        </ion-col>\n      <!-- </ion-item-divider> -->\n\n      </ion-row>\n      <ion-row *ngFor=\"let item of contactsData; let i=index\">\n        <ion-col size=\"4\">\n          <ion-label>\n            <p *ngIf=\"item.firstname || item.lastname\" class=\"col_style\">{{item.firstname}} {{item.lastname}}</p>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-label>\n            <p *ngIf=\"item.phone\" class=\"d-block\">{{item.phone}}</p>\n            <p *ngIf=\"item.email != null || item.email != ''\" class=\"d-block\">{{item.email}}</p>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-select value=\"\" [interfaceOptions]=\"customAlertOptions\" (ionChange)=\"selectValue($event, i,item)\" okText=\"Okay\" cancelText=\"Dismiss\">\n            <ion-select-option value=\"sms\">Send SMS</ion-select-option>\n            <ion-select-option value=\"email\">Send Email</ion-select-option>\n            <ion-select-option value=\"contact\">Change Contact Group</ion-select-option>\n            <ion-select-option value=\"edit\">Edit Contact</ion-select-option>\n          </ion-select>\n        </ion-col>\n        <hr>\n      </ion-row>\n\n    </ion-grid>\n    <!-- No Data -->\n    <h4 class=\"clr-medium ion-margin-vertical ion-text-center\" *ngIf=\"isCheck == true\">\n      No Record Found\n    </h4>\n  </div>\n  <!-- ​REFRESHER -->\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"refreshData($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n    <!-- INFINITE SCROLL -->\n    <ion-infinite-scroll *ngIf=\"currentPage<lastPage\"  threshold=\"10px\" (ionInfinite)=\"loadMoreData(1, $event)\">\n      <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more items...\">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>");

/***/ }),

/***/ "nqI4":
/*!*********************************************************************!*\
  !*** ./src/app/pages/contact-table/contact-table-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: ContactTablePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactTablePageRoutingModule", function() { return ContactTablePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _contact_table_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contact-table.page */ "WgAM");




const routes = [
    {
        path: '',
        component: _contact_table_page__WEBPACK_IMPORTED_MODULE_3__["ContactTablePage"]
    }
];
let ContactTablePageRoutingModule = class ContactTablePageRoutingModule {
};
ContactTablePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactTablePageRoutingModule);



/***/ }),

/***/ "qO6M":
/*!*************************************************************!*\
  !*** ./src/app/pages/contact-table/contact-table.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".table-wrapper ion-label {\n  white-space: nowrap !important;\n}\n.table-wrapper ion-label p {\n  font-size: 12px;\n}\n.table-wrapper .mb-0 {\n  margin-bottom: 0px !important;\n}\n.col_style {\n  color: #7d7d7d !important;\n  font-size: 12px !important;\n}\nion-row {\n  border-bottom: 1px solid #dcdcdc;\n}\n.p_style {\n  margin: 0px;\n  padding: 5px 0px 0px 5px;\n}\n.ion_select {\n  color: #7d7d7d !important;\n  font-size: 12px !important;\n  padding: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2NvbnRhY3QtdGFibGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsOEJBQUE7QUFBSjtBQUVJO0VBQ0UsZUFBQTtBQUFOO0FBSUU7RUFDRSw2QkFBQTtBQUZKO0FBTUE7RUFDRSx5QkFBQTtFQUNBLDBCQUFBO0FBSEY7QUFNQTtFQUNFLGdDQUFBO0FBSEY7QUFNQTtFQUNFLFdBQUE7RUFDQSx3QkFBQTtBQUhGO0FBTUE7RUFDRSx5QkFBQTtFQUNBLDBCQUFBO0VBQ0EsWUFBQTtBQUhGIiwiZmlsZSI6ImNvbnRhY3QtdGFibGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRhYmxlLXdyYXBwZXIge1xuICBpb24tbGFiZWwge1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXAgIWltcG9ydGFudDtcblxuICAgIHAge1xuICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgIH1cbiAgfVxuXG4gIC5tYi0wIHtcbiAgICBtYXJnaW4tYm90dG9tOiAwcHggIWltcG9ydGFudDtcbiAgfVxufVxuXG4uY29sX3N0eWxlIHtcbiAgY29sb3I6ICM3ZDdkN2QgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1yb3cge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RjZGNkYztcbn1cblxuLnBfc3R5bGUge1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogNXB4IDBweCAwcHggNXB4O1xufVxuXG4uaW9uX3NlbGVjdCB7XG4gIGNvbG9yOiAjN2Q3ZDdkICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTJweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiA1cHg7XG59Il19 */");

/***/ }),

/***/ "rtEV":
/*!****************************************************!*\
  !*** ./src/app/services/contact-groups.service.ts ***!
  \****************************************************/
/*! exports provided: ContactGroupsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactGroupsService", function() { return ContactGroupsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");




let ContactGroupsService = class ContactGroupsService {
    constructor(http) {
        this.http = http;
        this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    getContactGroups() {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'contactgroups.php');
            this.http.get(this.apiUrl + 'contactgroups.php')
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
    getContactGroupsList(formData) {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'contactgroupslist.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'contactgroupslist.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
    contactsImport(formData) {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'contactsimport.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'contactsimport.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
    getContacts(formData) {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'contacts.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'contacts.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
};
ContactGroupsService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
ContactGroupsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], ContactGroupsService);



/***/ })

}]);
//# sourceMappingURL=pages-contact-table-contact-table-module-es2015.js.map