(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-pdf-viewer-pdf-viewer-module"], {
    /***/
    "AGiy":
    /*!***************************************************************!*\
      !*** ./src/app/pages/pdf-viewer/pdf-viewer-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: PdfViewerPageRoutingModule */

    /***/
    function AGiy(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PdfViewerPageRoutingModule", function () {
        return PdfViewerPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _pdf_viewer_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./pdf-viewer.page */
      "DXJM");

      var routes = [{
        path: '',
        component: _pdf_viewer_page__WEBPACK_IMPORTED_MODULE_3__["PdfViewerPage"]
      }];

      var PdfViewerPageRoutingModule = function PdfViewerPageRoutingModule() {
        _classCallCheck(this, PdfViewerPageRoutingModule);
      };

      PdfViewerPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], PdfViewerPageRoutingModule);
      /***/
    },

    /***/
    "BCmj":
    /*!*******************************************************!*\
      !*** ./src/app/pages/pdf-viewer/pdf-viewer.page.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function BCmj(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwZGYtdmlld2VyLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "DXJM":
    /*!*****************************************************!*\
      !*** ./src/app/pages/pdf-viewer/pdf-viewer.page.ts ***!
      \*****************************************************/

    /*! exports provided: PdfViewerPage */

    /***/
    function DXJM(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PdfViewerPage", function () {
        return PdfViewerPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_pdf_viewer_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./pdf-viewer.page.html */
      "rcWY");
      /* harmony import */


      var _pdf_viewer_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./pdf-viewer.page.scss */
      "BCmj");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var PdfViewerPage = /*#__PURE__*/function () {
        function PdfViewerPage(platform, navCtrl, router, route) {
          var _this = this;

          _classCallCheck(this, PdfViewerPage);

          this.platform = platform;
          this.navCtrl = navCtrl;
          this.router = router;
          this.route = route;

          if (this.platform.is('android')) {
            this.check_platform = 1;
          } else {
            this.check_platform = 0;
          }

          this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
              _this.heading_name = _this.router.getCurrentNavigation().extras.state.name;
              _this.pdfSrc = _this.router.getCurrentNavigation().extras.state.url;
              console.log("this.pdfSrc : ", _this.pdfSrc);
              console.log("this.heading_name : ", _this.heading_name);
              var win = window; // hack compilator

              var safeURL = win.Ionic.WebView.convertFileSrc(_this.pdfSrc);
              _this.pdfSrc = safeURL;
              console.log("this.pdfSrc : ", _this.pdfSrc);
            }
          });
        }

        _createClass(PdfViewerPage, [{
          key: "ionViewDidLoad",
          value: function ionViewDidLoad() {
            console.log('ionViewDidLoad PdfViewerPage');
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "back",
          value: function back() {
            this.navCtrl.back();
          }
        }]);

        return PdfViewerPage;
      }();

      PdfViewerPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }];
      };

      PdfViewerPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-pdf-viewer',
        template: _raw_loader_pdf_viewer_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_pdf_viewer_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], PdfViewerPage);
      /***/
    },

    /***/
    "rcWY":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pdf-viewer/pdf-viewer.page.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function rcWY(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">{{heading_name}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <div *ngIf=\"check_platform == 0\">\n    <pdf-viewer [src]=\"pdfSrc\" [autoresize]=\"true\" [original-size]=\"false\" style=\"display: block;\" [zoom]=\"1\"\n      [fit-to-page]=\"false\"></pdf-viewer>\n  </div>\n  <div *ngIf=\"check_platform == 1\">\n    <pdf-viewer [src]=\"pdfSrc\" [original-size]=\"false\" style=\"display: block;\" [zoom]=\"0.5\" [fit-to-page]=\"false\">\n    </pdf-viewer>\n  </div>\n  <!-- <pdf-viewer [src]=\"pdfSrc\" [external-link-target]=\"'blank'\" style=\"display: block;\"  [original-size]=\"false\"  >\n  </pdf-viewer>  -->\n</ion-content>";
      /***/
    },

    /***/
    "yV6V":
    /*!*******************************************************!*\
      !*** ./src/app/pages/pdf-viewer/pdf-viewer.module.ts ***!
      \*******************************************************/

    /*! exports provided: PdfViewerPageModule */

    /***/
    function yV6V(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PdfViewerPageModule", function () {
        return PdfViewerPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _pdf_viewer_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./pdf-viewer-routing.module */
      "AGiy");
      /* harmony import */


      var _pdf_viewer_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./pdf-viewer.page */
      "DXJM");
      /* harmony import */


      var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ng2-pdf-viewer */
      "IkSl");

      var PdfViewerPageModule = function PdfViewerPageModule() {
        _classCallCheck(this, PdfViewerPageModule);
      };

      PdfViewerPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _pdf_viewer_routing_module__WEBPACK_IMPORTED_MODULE_5__["PdfViewerPageRoutingModule"], ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_7__["PdfViewerModule"]],
        declarations: [_pdf_viewer_page__WEBPACK_IMPORTED_MODULE_6__["PdfViewerPage"]]
      })], PdfViewerPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-pdf-viewer-pdf-viewer-module-es5.js.map