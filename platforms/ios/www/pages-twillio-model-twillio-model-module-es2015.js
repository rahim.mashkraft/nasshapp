(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-twillio-model-twillio-model-module"],{

/***/ "KQjX":
/*!*************************************************************!*\
  !*** ./src/app/pages/twillio-model/twillio-model.module.ts ***!
  \*************************************************************/
/*! exports provided: TwillioModelPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TwillioModelPageModule", function() { return TwillioModelPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _twillio_model_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./twillio-model-routing.module */ "gB9j");
/* harmony import */ var _twillio_model_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./twillio-model.page */ "bHg0");







let TwillioModelPageModule = class TwillioModelPageModule {
};
TwillioModelPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _twillio_model_routing_module__WEBPACK_IMPORTED_MODULE_5__["TwillioModelPageRoutingModule"]
        ],
        declarations: [_twillio_model_page__WEBPACK_IMPORTED_MODULE_6__["TwillioModelPage"]]
    })
], TwillioModelPageModule);



/***/ }),

/***/ "gB9j":
/*!*********************************************************************!*\
  !*** ./src/app/pages/twillio-model/twillio-model-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: TwillioModelPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TwillioModelPageRoutingModule", function() { return TwillioModelPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _twillio_model_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./twillio-model.page */ "bHg0");




const routes = [
    {
        path: '',
        component: _twillio_model_page__WEBPACK_IMPORTED_MODULE_3__["TwillioModelPage"]
    }
];
let TwillioModelPageRoutingModule = class TwillioModelPageRoutingModule {
};
TwillioModelPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TwillioModelPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-twillio-model-twillio-model-module-es2015.js.map