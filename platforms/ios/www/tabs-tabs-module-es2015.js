(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tabs-tabs-module"],{

/***/ "AHrv":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tabs/tabs.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-tabs>\n\n  <ion-tab-bar slot=\"bottom\">\n    <ion-tab-button tab=\"tab1\">\n      <ion-icon name=\"triangle\"></ion-icon>\n      <ion-label>Tab 1</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"tab2\">\n      <ion-icon name=\"ellipse\"></ion-icon>\n      <ion-label>Tab 2</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"tab3\">\n      <ion-icon name=\"square\"></ion-icon>\n      <ion-label>Tab 3</ion-label>\n    </ion-tab-button>\n  </ion-tab-bar>\n\n</ion-tabs> -->\n<ion-app>\n\n  <ion-router-outlet id=\"tabsView\"></ion-router-outlet>\n\n  <ion-tabs>\n    <ion-tab-bar slot=\"bottom\" class=\"tab-bar-clr\">\n\n      <ion-tab-button class=\"ion-activatable ripple-parent\" (click)=\"toggleMenu()\">\n        <ion-icon name=\"menu-outline\" class=\"theme-clr\"></ion-icon>\n        <ion-label class=\"p-5 ion-text-uppercase\" class=\"theme-clr\">RESOURCES</ion-label>\n        <ion-ripple-effect></ion-ripple-effect>\n      </ion-tab-button>\n      \n      <ion-tab-button class=\"ion-activatable ripple-parent\" (click)=\"chat()\">\n        <ion-icon name=\"chatbubble-ellipses-outline\" class=\"theme-clr\"></ion-icon>\n        <ion-label class=\"p-5 ion-text-uppercase\" class=\"theme-clr\">SMS Chat</ion-label>\n        <ion-ripple-effect></ion-ripple-effect>\n      </ion-tab-button>\n\n      <ion-tab-button class=\"ion-activatable ripple-parent\" (click)=\"checkin()\">\n        <ion-icon name=\"checkmark-circle-outline\" class=\"theme-clr\"></ion-icon>\n        <ion-label class=\"p-5 ion-text-uppercase\" class=\"theme-clr\">CHECK IN</ion-label>\n        <ion-ripple-effect></ion-ripple-effect>\n      </ion-tab-button>\n  \n      <ion-tab-button class=\"ion-activatable ripple-parent\" (click)=\"goToContactTable()\">\n        <ion-icon name=\"stats-chart-outline\" class=\"theme-clr\"></ion-icon>\n        <ion-label class=\"p-5 ion-text-uppercase\" class=\"theme-clr\">MY PROGRESS</ion-label>\n        <ion-ripple-effect></ion-ripple-effect>\n      </ion-tab-button>\n    </ion-tab-bar>\n  </ion-tabs> \n\n</ion-app>");

/***/ }),

/***/ "MJr+":
/*!***********************************!*\
  !*** ./src/app/tabs/tabs.page.ts ***!
  \***********************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_tabs_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./tabs.page.html */ "AHrv");
/* harmony import */ var _tabs_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tabs.page.scss */ "PkIa");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/alert.service */ "3LUQ");
/* harmony import */ var cordova_plugin_fcm_with_dependecy_updated_ionic_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! cordova-plugin-fcm-with-dependecy-updated/ionic/ngx */ "lOSq");
/* harmony import */ var _services_events_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/events.service */ "riPR");
/* harmony import */ var _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/contacts/ngx */ "TzAO");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/storage */ "e8h1");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "Bfh1");
/* harmony import */ var _ionic_native_diagnostic_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/diagnostic/ngx */ "mtRb");







// import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic";






let TabsPage = class TabsPage {
    constructor(menuCtrl, alertService, router, fcm, platform, contacts, storage, eventsService, geolocation, diagnostic, alertController, navCtrl) {
        this.menuCtrl = menuCtrl;
        this.alertService = alertService;
        this.router = router;
        this.fcm = fcm;
        this.platform = platform;
        this.contacts = contacts;
        this.storage = storage;
        this.eventsService = eventsService;
        this.geolocation = geolocation;
        this.diagnostic = diagnostic;
        this.alertController = alertController;
        this.navCtrl = navCtrl;
        this.disableTabs = false;
        if (this.platform.is('cordova')) {
            console.log("cordova : ");
            this.platform.ready().then(() => {
                // setTimeout(() => {
                //   this.eventsService.publish("startContact");
                // },2000)
                // console.log("this.platform.ready() : ");
                // console.log("this.fcm : ", this.fcm);
                // alert(JSON.stringify(this.fcm))
                let thisObj = this;
                thisObj.fcm.onNotification().subscribe(data => {
                    console.log("data : ", data);
                    // alert(JSON.stringify(this.fcm.onNotification()))
                    console.log("this.fcm : ", this.fcm);
                    if (data.wasTapped) {
                        console.log("Received in background");
                        console.log(JSON.stringify(data));
                    }
                    else {
                        console.log("Received in foreground");
                        console.log(JSON.stringify(data));
                        // alert(data)
                        var result = JSON.stringify(data);
                        // alert(result)
                        var res = JSON.parse(result);
                        this.eventsService.publish("notification_receive");
                        // alert(res)
                    }
                    ;
                });
                this.checkLocation();
            });
        }
    }
    checkLocation() {
        if (this.platform.is('cordova')) {
            this.diagnostic.getLocationAuthorizationStatus().then((status) => {
                setTimeout(() => {
                    console.log("status.NOT_REQUESTED : ", this.diagnostic.permissionStatus.NOT_REQUESTED);
                    console.log("status.DENIED_ALWAYS : ", this.diagnostic.permissionStatus.DENIED_ALWAYS);
                    console.log("status.GRANTED : ", this.diagnostic.permissionStatus.GRANTED);
                    console.log("status.GRANTED_WHEN_IN_USE : ", this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE);
                }, 100);
                // alert(status) ;
                if (status == this.diagnostic.permissionStatus.GRANTED || status == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
                    this.diagnostic.isLocationEnabled().then((res) => {
                        console.log("this.diagnostic.isLocationEnabled() : ", res);
                        if (res == true) {
                            this.getCurrentLocation();
                            setInterval(() => {
                                this.getCurrentLocation();
                            }, 100000);
                        }
                    });
                }
                else if (status == this.diagnostic.permissionStatus.NOT_REQUESTED) {
                    console.log("status == this.diagnostic.permissionStatus.NOT_REQUESTED");
                }
                else {
                    // this.further();
                }
                console.log("status : ", status);
            });
        }
    }
    Diagnostic_push() {
        this.diagnostic.getLocationAuthorizationStatus()
            .then((status) => {
            alert(status);
            if (status) {
            }
            else {
                this.further();
            }
        }).catch(e => console.error(e));
    }
    further() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Turn on Location Services',
                message: 'Please turn on location services',
                buttons: [
                    {
                        text: 'Not now',
                        handler: () => {
                            console.log('Cancel No');
                        }
                    },
                    {
                        text: 'Settings',
                        handler: () => {
                            //console.log('Cancel Yes');
                            this.switchToSettings();
                        }
                    }
                ]
            });
            // this.currentAlert = alert;
            yield alert.present();
        });
    }
    switchToSettings() {
        this.diagnostic.switchToSettings()
            .then(() => {
            console.log("Successfully switched to Settings app");
        }).catch(e => console.error(e));
    }
    getCurrentLocation() {
        this.geolocation.getCurrentPosition().then((resp) => {
            resp.coords.latitude;
            resp.coords.longitude;
            console.log('latitude 1', resp.coords.latitude);
            console.log('longitude 1', resp.coords.longitude);
        }).catch((error) => {
            console.log('Error getting location', error);
        });
        let watch = this.geolocation.watchPosition();
        console.log("watch : ", watch);
        watch.subscribe((data) => {
            console.log("data : ", data);
            // data can be a set of coordinates, or an error (if an error occurred).
            // data.coords.latitude
            // data.coords.longitude
        });
    }
    toggleMenu() {
        if (this.disableTabs) {
            this.alertService.showDisableAlert();
        }
        else {
            this.menuCtrl.toggle();
        }
    }
    chat() {
        this.navCtrl.navigateRoot(['tabs/tab4']);
    }
    checkin() {
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(true);
    }
    ionViewDidLeave() {
        this.menuCtrl.enable(true);
    }
    goToContactTable() {
        // this.router.navigate(['tabs/tab3']);
    }
};
TabsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"] },
    { type: _services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: cordova_plugin_fcm_with_dependecy_updated_ionic_ngx__WEBPACK_IMPORTED_MODULE_7__["FCM"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] },
    { type: _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_9__["Contacts"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_10__["Storage"] },
    { type: _services_events_service__WEBPACK_IMPORTED_MODULE_8__["EventsService"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_11__["Geolocation"] },
    { type: _ionic_native_diagnostic_ngx__WEBPACK_IMPORTED_MODULE_12__["Diagnostic"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] }
];
TabsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-tabs',
        template: _raw_loader_tabs_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_tabs_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TabsPage);



/***/ }),

/***/ "PkIa":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-tab-bar {\n  --background: #25223b;\n  --color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3RhYnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7RUFDQSxjQUFBO0FBQ0oiLCJmaWxlIjoidGFicy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdGFiLWJhciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMjUyMjNiO1xuICAgIC0tY29sb3I6IHdoaXRlO1xufSJdfQ== */");

/***/ }),

/***/ "hO9l":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.module.ts ***!
  \*************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tabs-routing.module */ "kB8F");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tabs.page */ "MJr+");







let TabsPageModule = class TabsPageModule {
};
TabsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__["TabsPageRoutingModule"]
        ],
        declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"]]
    })
], TabsPageModule);



/***/ }),

/***/ "kB8F":
/*!*********************************************!*\
  !*** ./src/app/tabs/tabs-routing.module.ts ***!
  \*********************************************/
/*! exports provided: TabsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageRoutingModule", function() { return TabsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tabs.page */ "MJr+");




const routes = [
    {
        path: '',
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_3__["TabsPage"],
        children: [
            {
                path: 'tab1',
                loadChildren: () => Promise.all(/*! import() | tab1-tab1-module */[__webpack_require__.e("common"), __webpack_require__.e("tab1-tab1-module")]).then(__webpack_require__.bind(null, /*! ../tab1/tab1.module */ "tmrb")).then(m => m.Tab1PageModule)
            },
            {
                path: 'tab2',
                loadChildren: () => __webpack_require__.e(/*! import() | tab2-tab2-module */ "tab2-tab2-module").then(__webpack_require__.bind(null, /*! ../tab2/tab2.module */ "TUkU")).then(m => m.Tab2PageModule)
            },
            {
                path: 'tab3',
                loadChildren: () => __webpack_require__.e(/*! import() | tab3-tab3-module */ "tab3-tab3-module").then(__webpack_require__.bind(null, /*! ../tab3/tab3.module */ "k+ul")).then(m => m.Tab3PageModule)
            },
            {
                path: 'tab4',
                loadChildren: () => Promise.all(/*! import() | tab4-tab4-module */[__webpack_require__.e("default~pages-twillio-model-twillio-model-module~tab4-tab4-module"), __webpack_require__.e("common"), __webpack_require__.e("tab4-tab4-module")]).then(__webpack_require__.bind(null, /*! ../tab4/tab4.module */ "1GDv")).then(m => m.Tab4PageModule)
            },
            {
                path: 'import-contacts',
                loadChildren: () => Promise.all(/*! import() | pages-import-contacts-import-contacts-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-import-contacts-import-contacts-module")]).then(__webpack_require__.bind(null, /*! ../pages/import-contacts/import-contacts.module */ "7chK")).then(m => m.ImportContactsPageModule)
            },
            {
                path: 'contact-table',
                loadChildren: () => __webpack_require__.e(/*! import() | pages-contact-table-contact-table-module */ "pages-contact-table-contact-table-module").then(__webpack_require__.bind(null, /*! ../pages/contact-table/contact-table.module */ "9cAt")).then(m => m.ContactTablePageModule)
            },
            {
                path: 'send-broadcast',
                loadChildren: () => Promise.all(/*! import() | pages-send-broadcast-send-broadcast-module */[__webpack_require__.e("default~pages-send-broadcast-send-broadcast-module~pages-send-smsbroadcast-send-smsbroadcast-module"), __webpack_require__.e("pages-send-broadcast-send-broadcast-module")]).then(__webpack_require__.bind(null, /*! ../pages/send-broadcast/send-broadcast.module */ "0wld")).then(m => m.SendBroadcastPageModule)
            },
            {
                path: 'send-smsbroadcast',
                loadChildren: () => Promise.all(/*! import() | pages-send-smsbroadcast-send-smsbroadcast-module */[__webpack_require__.e("default~pages-send-broadcast-send-broadcast-module~pages-send-smsbroadcast-send-smsbroadcast-module"), __webpack_require__.e("pages-send-smsbroadcast-send-smsbroadcast-module")]).then(__webpack_require__.bind(null, /*! ../pages/send-smsbroadcast/send-smsbroadcast.module */ "vw/y")).then(m => m.SendSMSBroadcastPageModule)
            },
            {
                path: 'notification',
                loadChildren: () => __webpack_require__.e(/*! import() | pages-notification-notification-module */ "pages-notification-notification-module").then(__webpack_require__.bind(null, /*! ../pages/notification/notification.module */ "UUPU")).then(m => m.NotificationPageModule)
            },
            {
                path: 'member',
                loadChildren: () => __webpack_require__.e(/*! import() | pages-member-member-module */ "pages-member-member-module").then(__webpack_require__.bind(null, /*! ../pages/member/member.module */ "9Qnf")).then(m => m.MemberPageModule)
            },
            {
                path: 'contact-groups',
                loadChildren: () => Promise.all(/*! import() | pages-contact-groups-contact-groups-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-contact-groups-contact-groups-module")]).then(__webpack_require__.bind(null, /*! ../pages/contact-groups/contact-groups.module */ "Z5qd")).then(m => m.ContactGroupsPageModule)
            },
            {
                path: '',
                redirectTo: '/tabs/tab1',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
    }
];
let TabsPageRoutingModule = class TabsPageRoutingModule {
};
TabsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
    })
], TabsPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=tabs-tabs-module-es2015.js.map