(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-send-broadcast-send-broadcast-module"], {
    /***/
    "0wld":
    /*!***************************************************************!*\
      !*** ./src/app/pages/send-broadcast/send-broadcast.module.ts ***!
      \***************************************************************/

    /*! exports provided: SendBroadcastPageModule */

    /***/
    function wld(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SendBroadcastPageModule", function () {
        return SendBroadcastPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _send_broadcast_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./send-broadcast-routing.module */
      "Vas1");
      /* harmony import */


      var _send_broadcast_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./send-broadcast.page */
      "IrDg");
      /* harmony import */


      var ngx_quill__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ngx-quill */
      "CzEO");
      /* harmony import */


      var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/modules/shared/shared.module */
      "FpXt");

      var SendBroadcastPageModule = function SendBroadcastPageModule() {
        _classCallCheck(this, SendBroadcastPageModule);
      };

      SendBroadcastPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _send_broadcast_routing_module__WEBPACK_IMPORTED_MODULE_5__["SendBroadcastPageRoutingModule"], ngx_quill__WEBPACK_IMPORTED_MODULE_7__["QuillModule"], src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_8__["SharedModule"]],
        declarations: [_send_broadcast_page__WEBPACK_IMPORTED_MODULE_6__["SendBroadcastPage"]]
      })], SendBroadcastPageModule);
      /***/
    },

    /***/
    "B1Fw":
    /*!*****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/send-broadcast/send-broadcast.page.html ***!
      \*****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function B1Fw(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">Send Email Broadcast</ion-title>\n    <ion-icon slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" routerLink=\"/tabs/tab1\"></ion-icon>\n  </ion-toolbar>\n</ion-header> -->\n<header title=\"Send Email Broadcast\"></header>\n\n<ion-content>\n  <!-- <form [formGroup]=\"sendBroadcast\" class=\"form\"> -->\n    <ion-item>\n      <ion-label class=\"heading_font\" style=\"font-size: 26px !important\" position=\"stacked\"><strong>Subject</strong></ion-label>\n      <ion-input placeholder=\"Title\" (ionFocus)=\"ionFocus($event)\" [(ngModel)]=\"subject\"></ion-input>\n    </ion-item>\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\" *ngIf=\"subject_error != ''\">\n        <ion-text color=\"warning\">\n          {{subject_error}}\n        </ion-text>\n      </p>\n    </div>\n    <ion-item>\n      <ion-label class=\"heading_font\"><strong>Message</strong></ion-label>\n    </ion-item>\n    <quill-editor class=\"content-editor\" [(ngModel)]=\"message\" [placeholder]=\"''\">\n    </quill-editor>\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\" *ngIf=\"message_error != ''\">\n        <ion-text color=\"warning\">\n          {{message_error}}\n        </ion-text>\n      </p>\n    </div>\n    <ion-item>\n      <ion-label class=\"heading_font\"><strong>Select Contact Group</strong></ion-label>\n    </ion-item>\n    <ion-list mode=\"md\">\n      <ion-radio-group value=\"radio_check\" [(ngModel)]=\"radio_check\">\n        <ion-item *ngFor=\"let item of contact_groups\">\n          <ion-label>{{item.title}}</ion-label>\n          <ion-radio slot=\"start\" value=\"{{item.id}}\"></ion-radio>\n        </ion-item>\n      </ion-radio-group>\n    </ion-list>\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\" *ngIf=\"radio_check_error != ''\">\n        <ion-text color=\"warning\">\n          {{radio_check_error}}\n        </ion-text>\n      </p>\n    </div>\n    <ion-item>\n      <ion-label class=\"heading_font\"><strong>Select Date / Time</strong></ion-label>\n    </ion-item>\n    <ion-item mode=\"ios\">\n      <ion-label>Date</ion-label>\n      <ion-datetime display-format=\"MMM D, YYYY\" picker-format=\"MMM D, YYYY\" [min]=\"myDate\" [(ngModel)]=\"myDate\"\n        value=\"1990-02-19\" placeholder=\"Select Date\">\n      </ion-datetime>\n    </ion-item>\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\" *ngIf=\"myDate != ''\">\n        <ion-text color=\"warning\">\n          {{myDate_error}}\n        </ion-text>\n      </p>\n    </div>\n    <ion-item mode=\"ios\">\n      <ion-label>Time</ion-label>\n      <ion-datetime display-format=\"h:mm A\" picker-format=\"h:mm A\" [min]=\"myTime\" [(ngModel)]=\"myTime\"\n        value=\"1990-02-19T07:43Z\">\n      </ion-datetime>\n    </ion-item>\n    <div class=\"error-div\">\n      <p ion-text class=\"text08\" *ngIf=\"myTime_error != ''\">\n        <ion-text color=\"warning\">\n          {{myTime_error}}\n        </ion-text>\n      </p>\n    </div>\n  <!-- </form> -->\n</ion-content>\n<ion-footer>\n  <ion-row padding-vertical>\n    <ion-col margin-left margin-right no-padding>\n      <ion-button expand=\"full\" (click)=\"broadcast()\">Submit</ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>";
      /***/
    },

    /***/
    "Bj/Y":
    /*!***************************************************************!*\
      !*** ./src/app/pages/send-broadcast/send-broadcast.page.scss ***!
      \***************************************************************/

    /*! exports provided: default */

    /***/
    function BjY(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzZW5kLWJyb2FkY2FzdC5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "IrDg":
    /*!*************************************************************!*\
      !*** ./src/app/pages/send-broadcast/send-broadcast.page.ts ***!
      \*************************************************************/

    /*! exports provided: SendBroadcastPage */

    /***/
    function IrDg(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SendBroadcastPage", function () {
        return SendBroadcastPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_send_broadcast_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./send-broadcast.page.html */
      "B1Fw");
      /* harmony import */


      var _send_broadcast_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./send-broadcast.page.scss */
      "Bj/Y");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/contact-groups.service */
      "rtEV");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! moment */
      "wd/R");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
      /* harmony import */


      var src_app_services_broadcast_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/broadcast.service */
      "Ev3c");
      /* harmony import */


      var src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/services/toast.service */
      "2g2N");
      /* harmony import */


      var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! src/app/services/loader.service */
      "5dVO");

      var SendBroadcastPage = /*#__PURE__*/function () {
        function SendBroadcastPage(navCtrl, contactGroupService, broadcastService, toastService, loaderService) {
          _classCallCheck(this, SendBroadcastPage);

          this.navCtrl = navCtrl;
          this.contactGroupService = contactGroupService;
          this.broadcastService = broadcastService;
          this.toastService = toastService;
          this.loaderService = loaderService;
          this.radio_check = 0;
          this.myDate = '';
          this.myTime = '';
          this.subject = '';
          this.message = '';
          this.radio_check_error = '';
          this.myDate_error = '';
          this.myTime_error = '';
          this.subject_error = '';
          this.message_error = '';
          this.check_validation = false;
        }

        _createClass(SendBroadcastPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.myDate = new Date().toISOString();
            this.myTime = new Date().toISOString();
            this.radio_check = 1;
            this.getContactGroups();
          }
        }, {
          key: "getContactGroups",
          value: function getContactGroups() {
            var _this = this;

            this.contactGroupService.getContactGroups().then(function (res) {
              console.log(res);

              if (res.status !== false) {
                _this.contact_groups = res.contactgroup;
              }
            });
          }
        }, {
          key: "ionFocus",
          value: function ionFocus(event) {
            this.subject_error = '';
            console.log(event);
          }
        }, {
          key: "validation",
          value: function validation() {
            if (this.subject == '') {
              this.subject_error = 'This field is required';
              return;
            }

            if (this.message == '') {
              this.message_error = 'This field is required';
              return;
            }

            if (this.radio_check == 0) {
              this.radio_check_error = 'This field is required';
              return;
            }

            if (this.myDate == '') {
              this.myDate_error = 'This field is required';
              return;
            }

            if (this.myTime == '') {
              this.myTime_error = 'This field is required';
              return;
            }

            this.check_validation = true;
          }
        }, {
          key: "broadcast",
          value: function broadcast() {
            var _this2 = this;

            this.validation();

            if (this.check_validation == true) {
              this.loaderService.showLoader();
              this.date_format = moment__WEBPACK_IMPORTED_MODULE_6__(this.myDate).format("MM/D/YYYY");
              this.hours = moment__WEBPACK_IMPORTED_MODULE_6__(this.myTime).format('h A');
              this.minutes = moment__WEBPACK_IMPORTED_MODULE_6__(this.myTime).format('mm');
              var formData = new FormData();
              formData.append("aid", localStorage.getItem('user_id'));
              formData.append("subject", this.subject);
              formData.append("bdate", this.date_format);
              formData.append("bhour", this.hours);
              formData.append("bmin", this.minutes);
              formData.append("cid", this.radio_check);
              formData.append("message", this.message);
              this.broadcastService.broadcast(formData).then(function (res) {
                console.log(res);

                if (res.status !== false) {
                  _this2.toastService.showToast('Email Broadcast sent successfully.');

                  _this2.radio_check = 0;
                  _this2.myDate = '';
                  _this2.myTime = '';
                  _this2.subject = '';
                  _this2.message = '';

                  _this2.loaderService.hideLoader();

                  _this2.back(); // this.contact_groups = res.contactgroup

                } else {
                  _this2.loaderService.hideLoader();
                }
              });
            }
          }
        }, {
          key: "back",
          value: function back() {
            this.navCtrl.back();
          }
        }]);

        return SendBroadcastPage;
      }();

      SendBroadcastPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_5__["ContactGroupsService"]
        }, {
          type: src_app_services_broadcast_service__WEBPACK_IMPORTED_MODULE_7__["BroadcastService"]
        }, {
          type: src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"]
        }, {
          type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_9__["LoaderService"]
        }];
      };

      SendBroadcastPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-send-broadcast',
        template: _raw_loader_send_broadcast_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_send_broadcast_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], SendBroadcastPage);
      /***/
    },

    /***/
    "Vas1":
    /*!***********************************************************************!*\
      !*** ./src/app/pages/send-broadcast/send-broadcast-routing.module.ts ***!
      \***********************************************************************/

    /*! exports provided: SendBroadcastPageRoutingModule */

    /***/
    function Vas1(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SendBroadcastPageRoutingModule", function () {
        return SendBroadcastPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _send_broadcast_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./send-broadcast.page */
      "IrDg");

      var routes = [{
        path: '',
        component: _send_broadcast_page__WEBPACK_IMPORTED_MODULE_3__["SendBroadcastPage"]
      }];

      var SendBroadcastPageRoutingModule = function SendBroadcastPageRoutingModule() {
        _classCallCheck(this, SendBroadcastPageRoutingModule);
      };

      SendBroadcastPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SendBroadcastPageRoutingModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-send-broadcast-send-broadcast-module-es5.js.map