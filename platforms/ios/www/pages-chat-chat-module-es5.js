(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-chat-chat-module"], {
    /***/
    "0MZs":
    /*!*******************************************!*\
      !*** ./src/app/pages/chat/chat.page.scss ***!
      \*******************************************/

    /*! exports provided: default */

    /***/
    function MZs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".header_pd {\n  padding: 0.3em 0.2em;\n}\n\nion-searchbar {\n  --border-radius: 22px;\n  --box-shadow: none;\n}\n\n.cstomHeader {\n  --background: #fff;\n}\n\n.segment-button-checked {\n  background: #F7F8FA;\n  color: #464646;\n}\n\n.fotter_bottom {\n  bottom: 4px;\n  border: 1px solid #25223b;\n  background: #ffffff;\n}\n\n.w-full {\n  background: #25223b;\n  padding: 8px 10px;\n  margin-right: 1em;\n  color: #fff;\n  border-radius: 10px;\n  border: 1px solid #fff;\n}\n\n.w-full p {\n  margin: 0px;\n}\n\n.sender {\n  float: right;\n  width: 100%;\n  margin-top: 10px;\n}\n\n.sender .inner {\n  float: right;\n  max-width: 70%;\n  min-width: 52%;\n  padding-bottom: 10px;\n}\n\n.me {\n  background: #7676a0ab;\n  padding: 8px 10px;\n  margin-left: 1em;\n  color: #ffffff;\n  border-radius: 10px;\n}\n\n.me p {\n  margin: 0px;\n}\n\n.me_ {\n  float: left;\n  max-width: 70%;\n  min-width: 52%;\n  margin-top: 10px;\n  padding-bottom: 10px;\n}\n\n.imgae_case {\n  background: none;\n  padding: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2NoYXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usb0JBQUE7QUFDRjs7QUFFQTtFQUNFLHFCQUFBO0VBQ0Esa0JBQUE7QUFDRjs7QUFFQTtFQUNFLGtCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxtQkFBQTtFQUNBLGNBQUE7QUFDRjs7QUFFQTtFQUVFLFdBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FBQUY7O0FBR0E7RUFDRSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQUFGOztBQUdBO0VBQ0UsV0FBQTtBQUFGOztBQUdBO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQUFGOztBQUdBO0VBQ0UsWUFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7QUFBRjs7QUFHQTtFQUNFLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQUFGOztBQUdBO0VBQ0UsV0FBQTtBQUFGOztBQUdBO0VBQ0UsV0FBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtBQUFGOztBQUdBO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0FBQUYiLCJmaWxlIjoiY2hhdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyX3BkIHtcbiAgcGFkZGluZzogMC4zZW0gMC4yZW07XG59XG5cbmlvbi1zZWFyY2hiYXIge1xuICAtLWJvcmRlci1yYWRpdXM6IDIycHg7XG4gIC0tYm94LXNoYWRvdzogbm9uZTtcbn1cblxuLmNzdG9tSGVhZGVyIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xufVxuXG4uc2VnbWVudC1idXR0b24tY2hlY2tlZCB7XG4gIGJhY2tncm91bmQ6ICNGN0Y4RkE7XG4gIGNvbG9yOiAjNDY0NjQ2O1xufVxuXG4uZm90dGVyX2JvdHRvbSB7XG4gIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiA0cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMyNTIyM2I7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi53LWZ1bGwge1xuICBiYWNrZ3JvdW5kOiAjMjUyMjNiO1xuICBwYWRkaW5nOiA4cHggMTBweDtcbiAgbWFyZ2luLXJpZ2h0OiAxZW07XG4gIGNvbG9yOiAjZmZmO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xufVxuXG4udy1mdWxsIHAge1xuICBtYXJnaW46IDBweDtcbn1cblxuLnNlbmRlciB7XG4gIGZsb2F0OiByaWdodDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbi5zZW5kZXIgLmlubmVyIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXgtd2lkdGg6IDcwJTtcbiAgbWluLXdpZHRoOiA1MiU7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuXG4ubWUge1xuICBiYWNrZ3JvdW5kOiAjNzY3NmEwYWI7XG4gIHBhZGRpbmc6IDhweCAxMHB4O1xuICBtYXJnaW4tbGVmdDogMWVtO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cblxuLm1lIHAge1xuICBtYXJnaW46IDBweDtcbn1cblxuLm1lXyB7XG4gIGZsb2F0OiBsZWZ0O1xuICBtYXgtd2lkdGg6IDcwJTtcbiAgbWluLXdpZHRoOiA1MiU7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuXG4uaW1nYWVfY2FzZSB7XG4gIGJhY2tncm91bmQ6IG5vbmU7XG4gIHBhZGRpbmc6IDBweDtcbn0iXX0= */";
      /***/
    },

    /***/
    "2g2N":
    /*!*******************************************!*\
      !*** ./src/app/services/toast.service.ts ***!
      \*******************************************/

    /*! exports provided: ToastService */

    /***/
    function g2N(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ToastService", function () {
        return ToastService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var ToastService = /*#__PURE__*/function () {
        function ToastService(toastCtrl) {
          _classCallCheck(this, ToastService);

          this.toastCtrl = toastCtrl;
        }

        _createClass(ToastService, [{
          key: "showToast",
          value: function showToast() {
            var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Successfully Logged in!';
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastCtrl.create({
                        cssClass: 'bg-toast',
                        message: message,
                        duration: 3000,
                        position: 'bottom'
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "showToastUpdateProfile",
          value: function showToastUpdateProfile(message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var toast;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.toastCtrl.create({
                        cssClass: 'bg-toast',
                        message: message,
                        duration: 3000,
                        position: 'bottom'
                      });

                    case 2:
                      toast = _context2.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return ToastService;
      }();

      ToastService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      ToastService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ToastService);
      /***/
    },

    /***/
    "5dVO":
    /*!********************************************!*\
      !*** ./src/app/services/loader.service.ts ***!
      \********************************************/

    /*! exports provided: LoaderService */

    /***/
    function dVO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoaderService", function () {
        return LoaderService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var LoaderService = /*#__PURE__*/function () {
        function LoaderService(loadingCtrl) {
          _classCallCheck(this, LoaderService);

          this.loadingCtrl = loadingCtrl;
        }

        _createClass(LoaderService, [{
          key: "showLoader",
          value: function showLoader() {
            this.isBusy = true; // this.loaderToShow = this.loadingCtrl.create({
            //   message: 'Please Wait..'
            // }).then((res) => {
            //   res.present();
            //   // res.onDidDismiss().then((dis) => {
            //   //    console.log('Loading dismissed!',dis);
            //   // });
            // });
            // // this.hideLoader();
          }
        }, {
          key: "hideLoader",
          value: function hideLoader() {
            // setTimeout(()=>{
            //   this.loadingCtrl.dismiss();
            // },100)
            this.isBusy = false;
          }
        }]);

        return LoaderService;
      }();

      LoaderService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoaderService);
      /***/
    },

    /***/
    "EAAc":
    /*!*********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/chat/chat.page.html ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function EAAc(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<header title=\"{{firstname}} {{lastname}}\"></header>\n\n<ion-content #IonContent>\n  <div class=\"rounded-lg border border-red-700 h-64 mt-3 px-2 py-3 overflow-y-scroll scroll\">\n    <ng-container *ngFor=\"let chat_history of chatHistory\">\n      <ng-container *ngIf=\"chat_history.direction == 'out'\">\n        <div class=\"flex justify-start sender\">\n          <div class=\"flex w-2/3 mt-2 inner\">\n            <div class=\"rounded-lg px-4 py-3 bg-gray\">\n              <div class=\"w-full\">\n                <div class=\"py-1\">\n                  <span class=\"text-gray-900\">{{chat_history.messagebody}}</span>\n                </div>\n              </div>\n              <span class=\"text-xs text-gray-700\">{{chat_history.messagetime}}</span>\n            </div>\n            <div class=\"flex-grow\"></div>\n          </div>\n        </div>\n        <!-- <div class=\"flex justify-start sender\">\n          <div class=\"flex w-2/3 mt-2 inner\">\n            <div class=\"rounded-lg px-4 py-3 bg-gray\">\n              <div class=\"w-full\">\n                <div class=\"py-1\">\n                  <span class=\"text-gray-900\">jhsfghsjgkhskfghskjfhgkjsdhfigusidfigusfigidfighsdigidfgfdhiughsidufhgiudhfiguhidfughisdhfuigsdf</span>\n                </div>\n              </div>\n              <span class=\"text-xs text-gray-700\">11:20</span>\n            </div>\n            <div class=\"flex-grow\"></div>\n          </div>\n        </div>\n        <div class=\"flex justify-start sender\">\n          <div class=\"flex w-2/3 mt-2 inner\">\n            <div class=\"rounded-lg px-4 py-3 bg-gray\">\n              <div class=\"w-full imgae_case\">\n                <div class=\"py-1\">\n                  <img src=\"assets/img/background.png\">\n                </div>\n              </div>\n              <span class=\"text-xs text-gray-700\">11:20</span>\n            </div>\n            <div class=\"flex-grow\"></div>\n          </div>\n        </div> -->\n      </ng-container>\n\n      <ng-container *ngIf=\"chat_history.direction == 'in'\">\n        <div class=\"flex justify-end me_\">\n          <div class=\"flex w-2/3 mt-2\">\n            <div class=\"flex-grow\"></div>\n            <div class=\"rounded-lg px-4 py-3 text-right bg-prim \">\n              <div class=\"me\">\n                <div class=\"py-1\">\n                  <span class=\"text-gray-900\">{{chat_history.messagebody}}</span>\n                </div>\n              </div>\n            </div>\n            <span class=\"text-xs ml-4 text-gray-700 ion-margin-start\">{{chat_history.messagetime}}</span>\n          </div>\n        </div>\n        <!-- <div class=\"flex justify-end me_\">\n          <div class=\"flex w-2/3 mt-2\">\n            <div class=\"flex-grow\"></div>\n            <div class=\"rounded-lg px-4 py-3 text-right bg-prim \">\n              <div class=\"me imgae_case\">\n                <div class=\"py-1\">\n                  <img src=\"assets/img/background.png\">\n                </div>\n              </div>\n            </div>\n            <span class=\"text-xs ml-4 text-gray-700 ion-margin-start\">11:22</span>\n          </div>\n        </div>\n        <div class=\"flex justify-end me_\">\n          <div class=\"flex w-2/3 mt-2\">\n            <div class=\"flex-grow\"></div>\n            <div class=\"rounded-lg px-4 py-3 text-right bg-prim \">\n              <div class=\"me imgae_case\">\n                <div class=\"py-1\">\n                  <img src=\"assets/img/background.png\">\n                </div>\n              </div>\n            </div>\n            <span class=\"text-xs ml-4 text-gray-700 ion-margin-start\">11:22</span>\n          </div>\n        </div>\n        <div class=\"flex justify-end me_\">\n          <div class=\"flex w-2/3 mt-2\">\n            <div class=\"flex-grow\"></div>\n            <div class=\"rounded-lg px-4 py-3 text-right bg-prim \">\n              <div class=\"me imgae_case\">\n                <div class=\"py-1\">\n                  <img src=\"assets/img/background.png\">\n                </div>\n              </div>\n            </div>\n            <span class=\"text-xs ml-4 text-gray-700 ion-margin-start\">11:22</span>\n          </div>\n        </div>\n        <div class=\"flex justify-end me_\">\n          <div class=\"flex w-2/3 mt-2\">\n            <div class=\"flex-grow\"></div>\n            <div class=\"rounded-lg px-4 py-3 text-right bg-prim \">\n              <div class=\"me imgae_case\">\n                <div class=\"py-1\">\n                  <img src=\"assets/img/background.png\">\n                </div>\n              </div>\n            </div>\n            <span class=\"text-xs ml-4 text-gray-700 ion-margin-start\">11:22</span>\n          </div>\n        </div> -->\n\n      </ng-container>\n    </ng-container>\n    <!--div class=\"p-4 text-bold\" *ngIf=\"chat.length == 0\">\n          <p>No chat found!</p>\n      </div-->\n  </div>\n    <!-- ​REFRESHER -->\n    <ion-refresher *ngIf=\"isCheck == true\" slot=\"fixed\" (ionRefresh)=\"refreshDataList($event)\">\n      <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n</ion-content>\n<ion-footer class=\"fotter_bottom\">\n  <ion-item class=\"chat\" lines=\"none\">\n    <ion-icon slot=\"end\" name=\"attach\" expand=\"icon-only\" color=\"primary\" (click)=\"uploadPhoto()\" class=\"footerIcon\"></ion-icon>\n    <ion-icon slot=\"end\" name=\"send\" expand=\"icon-only\" color=\"primary\" (click)=\"sendMsg()\" class=\"footerIcon\"></ion-icon>\n    <ion-input class=\"input-box\" type=\"text\" placeholder=\"Type your message here\" [(ngModel)]=\"message\"\n      (keypress)=\"userTyping($event)\"></ion-input>\n  </ion-item>\n</ion-footer>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>";
      /***/
    },

    /***/
    "EdD2":
    /*!*******************************************!*\
      !*** ./src/app/pages/chat/chat.module.ts ***!
      \*******************************************/

    /*! exports provided: ChatPageModule */

    /***/
    function EdD2(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChatPageModule", function () {
        return ChatPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./chat-routing.module */
      "fp8f");
      /* harmony import */


      var _chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./chat.page */
      "oflK");
      /* harmony import */


      var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/modules/shared/shared.module */
      "FpXt");

      var ChatPageModule = function ChatPageModule() {
        _classCallCheck(this, ChatPageModule);
      };

      ChatPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__["ChatPageRoutingModule"], src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]]
      })], ChatPageModule);
      /***/
    },

    /***/
    "fp8f":
    /*!***************************************************!*\
      !*** ./src/app/pages/chat/chat-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: ChatPageRoutingModule */

    /***/
    function fp8f(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChatPageRoutingModule", function () {
        return ChatPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _chat_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./chat.page */
      "oflK");

      var routes = [{
        path: '',
        component: _chat_page__WEBPACK_IMPORTED_MODULE_3__["ChatPage"]
      }];

      var ChatPageRoutingModule = function ChatPageRoutingModule() {
        _classCallCheck(this, ChatPageRoutingModule);
      };

      ChatPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ChatPageRoutingModule);
      /***/
    },

    /***/
    "oflK":
    /*!*****************************************!*\
      !*** ./src/app/pages/chat/chat.page.ts ***!
      \*****************************************/

    /*! exports provided: ChatPage */

    /***/
    function oflK(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChatPage", function () {
        return ChatPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_chat_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./chat.page.html */
      "EAAc");
      /* harmony import */


      var _chat_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./chat.page.scss */
      "0MZs");
      /* harmony import */


      var _services_chat_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../../services/chat.service */
      "sjK5");
      /* harmony import */


      var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/loader.service */
      "5dVO");
      /* harmony import */


      var src_app_services_image_uploading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/image-uploading.service */
      "TX3h");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var ChatPage = /*#__PURE__*/function () {
        function ChatPage(imageUploadingService, router, route, loaderService, chatService) {
          var _this = this;

          _classCallCheck(this, ChatPage);

          this.imageUploadingService = imageUploadingService;
          this.router = router;
          this.route = route;
          this.loaderService = loaderService;
          this.chatService = chatService;
          this.currentPage = 1;
          this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
              _this.affiliatenumber = _this.router.getCurrentNavigation().extras.state.affiliatenumber;
              _this.contactnumber = _this.router.getCurrentNavigation().extras.state.contactnumber;
              _this.firstname = _this.router.getCurrentNavigation().extras.state.firstname;
              _this.lastname = _this.router.getCurrentNavigation().extras.state.lastname;
              console.log("this.affiliatenumber : ", _this.affiliatenumber);
              console.log("this.contactnumber : ", _this.contactnumber);

              _this.smschathistory(_this.currentPage);
            }
          });
        }

        _createClass(ChatPage, [{
          key: "smschathistory",
          value: function smschathistory(currentPage) {
            var _this2 = this;

            var event = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            this.loaderService.showLoader();
            var formData = new FormData(); // formData.append("aid", localStorage.getItem('user_id'));

            formData.append("affiliatenumber", this.affiliatenumber);
            formData.append("contactnumber", this.contactnumber);
            formData.append("PageNumber", currentPage);
            this.chatService.smschathistory(formData).then(function (data) {
              console.log("data : ", data);

              if (data.status) {
                _this2.isCheck = true;
                _this2.chatHistory = data.contacts;

                _this2.loaderService.hideLoader();

                if (currentPage == 1) {
                  _this2.scrollDown();
                }
              } else {
                _this2.isCheck = false;

                _this2.loaderService.hideLoader();
              }

              if (event) {
                event.target.complete();

                if (currentPage == 1) {
                  _this2.scrollDown();
                }
              }
            }, function (err) {
              _this2.isCheck = false;

              _this2.loaderService.hideLoader();

              if (event) {
                event.target.complete();
              }
            });
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.scrollDown();
          }
        }, {
          key: "uploadPhoto",
          value: function uploadPhoto() {
            var image;
            image = this.imageUploadingService.uploadPhoto().then(function (data) {
              console.log("data : ", data);
            });
            console.log("image : ", image);
          }
        }, {
          key: "sendMsg",
          value: function sendMsg() {
            if (this.message) {
              console.log(this.message.match(/.{1,155}/g));
            }
          }
        }, {
          key: "scrollDown",
          value: function scrollDown() {
            var _this3 = this;

            // document.querySelector('.chat_8').scrollIntoView();
            setTimeout(function () {
              // this.content.scrollToBottom(50)
              _this3.content.scrollToBottom(50);
            }, 170);
          }
        }, {
          key: "userTyping",
          value: function userTyping(event) {
            console.log(event);
            this.start_typing = event.target.value;
            this.scrollDown();
          }
        }, {
          key: "refreshDataList",
          value: function refreshDataList(event) {
            this.currentPage = this.currentPage + 1;
            this.smschathistory(this.currentPage, event);
          }
        }]);

        return ChatPage;
      }();

      ChatPage.ctorParameters = function () {
        return [{
          type: src_app_services_image_uploading_service__WEBPACK_IMPORTED_MODULE_5__["ImageUploadingService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"]
        }, {
          type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"]
        }, {
          type: _services_chat_service__WEBPACK_IMPORTED_MODULE_3__["ChatService"]
        }];
      };

      ChatPage.propDecorators = {
        content: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_6__["ViewChild"],
          args: ['IonContent']
        }]
      };
      ChatPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["Component"])({
        selector: 'app-chat',
        template: _raw_loader_chat_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_chat_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], ChatPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-chat-chat-module-es5.js.map