(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-send-sms-contact-send-sms-contact-module"],{

/***/ "2g2N":
/*!*******************************************!*\
  !*** ./src/app/services/toast.service.ts ***!
  \*******************************************/
/*! exports provided: ToastService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastService", function() { return ToastService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let ToastService = class ToastService {
    constructor(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    showToast(message = 'Successfully Logged in!') {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                cssClass: 'bg-toast',
                message: message,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }
    showToastUpdateProfile(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                cssClass: 'bg-toast',
                message: message,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }
};
ToastService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
ToastService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ToastService);



/***/ }),

/***/ "5dVO":
/*!********************************************!*\
  !*** ./src/app/services/loader.service.ts ***!
  \********************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let LoaderService = class LoaderService {
    constructor(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    showLoader() {
        this.isBusy = true;
        // this.loaderToShow = this.loadingCtrl.create({
        //   message: 'Please Wait..'
        // }).then((res) => {
        //   res.present();
        //   // res.onDidDismiss().then((dis) => {
        //   //    console.log('Loading dismissed!',dis);
        //   // });
        // });
        // // this.hideLoader();
    }
    hideLoader() {
        // setTimeout(()=>{
        //   this.loadingCtrl.dismiss();
        // },100)
        this.isBusy = false;
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ "Bqzo":
/*!***************************************************************************!*\
  !*** ./src/app/pages/send-sms-contact/send-sms-contact-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: SendSmsContactPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendSmsContactPageRoutingModule", function() { return SendSmsContactPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _send_sms_contact_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./send-sms-contact.page */ "gP5U");




const routes = [
    {
        path: '',
        component: _send_sms_contact_page__WEBPACK_IMPORTED_MODULE_3__["SendSmsContactPage"]
    }
];
let SendSmsContactPageRoutingModule = class SendSmsContactPageRoutingModule {
};
SendSmsContactPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SendSmsContactPageRoutingModule);



/***/ }),

/***/ "CLQu":
/*!*******************************************************************!*\
  !*** ./src/app/pages/send-sms-contact/send-sms-contact.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzZW5kLXNtcy1jb250YWN0LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "fp8Y":
/*!*******************************************************************!*\
  !*** ./src/app/pages/send-sms-contact/send-sms-contact.module.ts ***!
  \*******************************************************************/
/*! exports provided: SendSmsContactPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendSmsContactPageModule", function() { return SendSmsContactPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _send_sms_contact_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./send-sms-contact-routing.module */ "Bqzo");
/* harmony import */ var _send_sms_contact_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./send-sms-contact.page */ "gP5U");
/* harmony import */ var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modules/shared/shared.module */ "FpXt");








let SendSmsContactPageModule = class SendSmsContactPageModule {
};
SendSmsContactPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _send_sms_contact_routing_module__WEBPACK_IMPORTED_MODULE_5__["SendSmsContactPageRoutingModule"],
            src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_send_sms_contact_page__WEBPACK_IMPORTED_MODULE_6__["SendSmsContactPage"]]
    })
], SendSmsContactPageModule);



/***/ }),

/***/ "gKmf":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/send-sms-contact/send-sms-contact.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">Send SMS To Contact</ion-title>\n    <ion-icon slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" routerLink=\"/tabs/tab1\"></ion-icon>\n  </ion-toolbar>\n</ion-header> -->\n<header title=\"Send SMS To Contact\"></header>\n<ion-content>\n  <ion-item>\n    <ion-label class=\"heading_font\" style=\"font-size: 26px !important\" position=\"stacked\"><strong>Phone</strong></ion-label>\n    <ion-input placeholder=\"Phone Number\" type=\"text\" [(ngModel)]=\"phone_number\"></ion-input>\n  </ion-item>\n  <div class=\"error-div\">\n    <p ion-text class=\"text08\" *ngIf=\"Phone_error != ''\">\n      <ion-text color=\"warning\">\n        {{Phone_error}}\n      </ion-text>\n    </p>\n  </div>\n  <ion-item>\n    <ion-label class=\"heading_font\" style=\"\" position=\"stacked\"><strong>Name</strong></ion-label>\n    <ion-input placeholder=\"Title\" [(ngModel)]=\"name\"></ion-input>\n  </ion-item>\n  <div class=\"error-div\">\n    <p ion-text class=\"text08\" *ngIf=\"name_error != ''\">\n      <ion-text color=\"warning\">\n        {{name_error}}\n      </ion-text>\n    </p>\n  </div>\n  <ion-item>\n    <ion-label class=\"heading_font\" style=\"font-size: 26px !important\" position=\"stacked\"><strong>Message</strong></ion-label>\n    <ion-input placeholder=\"Message\" [(ngModel)]=\"message\"></ion-input>\n  </ion-item>\n  <div class=\"error-div\">\n    <p ion-text class=\"text08\" *ngIf=\"message_error != ''\">\n      <ion-text color=\"warning\">\n        {{message_error}}\n      </ion-text>\n    </p>\n  </div>\n</ion-content>\n<ion-footer>\n  <ion-row padding-vertical>\n    <ion-col margin-left margin-right no-padding>\n      <ion-button expand=\"full\" (click)=\"sendsms()\">Submit</ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>");

/***/ }),

/***/ "gP5U":
/*!*****************************************************************!*\
  !*** ./src/app/pages/send-sms-contact/send-sms-contact.page.ts ***!
  \*****************************************************************/
/*! exports provided: SendSmsContactPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendSmsContactPage", function() { return SendSmsContactPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_send_sms_contact_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./send-sms-contact.page.html */ "gKmf");
/* harmony import */ var _send_sms_contact_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./send-sms-contact.page.scss */ "CLQu");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/loader.service */ "5dVO");
/* harmony import */ var src_app_services_send_smsemail_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/send-smsemail.service */ "Xu8g");
/* harmony import */ var src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/toast.service */ "2g2N");









let SendSmsContactPage = class SendSmsContactPage {
    constructor(router, route, loaderService, sendSmsemailService, toastService, navCtrl) {
        this.router = router;
        this.route = route;
        this.loaderService = loaderService;
        this.sendSmsemailService = sendSmsemailService;
        this.toastService = toastService;
        this.navCtrl = navCtrl;
        // error_variable
        this.Phone_error = '';
        this.name_error = '';
        this.message_error = '';
        this.check_validation = false;
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.item_obj = this.router.getCurrentNavigation().extras.state.item_obj;
                this.pageName = this.router.getCurrentNavigation().extras.state.pageName;
                console.log("this.item_obj : ", this.item_obj);
                this.phone_number = this.item_obj.phone;
                this.name = this.item_obj.firstname + ' ' + this.item_obj.lastname;
                if (this.pageName == 'member') {
                    this.affaid = this.item_obj.affaid;
                }
                else {
                    this.cid = this.item_obj.cid;
                }
            }
        });
    }
    ngOnInit() {
    }
    validation() {
        if (this.phone_number == '') {
            this.Phone_error = 'This field is required';
            return;
        }
        if (this.name == '') {
            this.name_error = 'This field is required';
            return;
        }
        if (this.message == '') {
            this.message_error = 'This field is required';
            return;
        }
        this.check_validation = true;
    }
    sendsms() {
        this.validation();
        if (this.check_validation == true) {
            this.loaderService.showLoader();
            let formData = new FormData();
            formData.append("aid", localStorage.getItem('user_id'));
            if (this.pageName == 'member') {
                formData.append("affaid", this.affaid);
            }
            else {
                formData.append("cid", this.cid);
            }
            formData.append("name", this.name);
            formData.append("phone", this.phone_number);
            formData.append("message", this.message);
            this.sendSmsemailService.sendsms(formData).then((res) => {
                console.log(res);
                if (res.status !== false) {
                    this.toastService.showToast(res.message);
                    this.loaderService.hideLoader();
                    // this.navCtrl.navigateRoot('/tabs');
                    this.back();
                    // this.contact_groups = res.contactgroup
                }
                else {
                    this.toastService.showToast(res.message);
                    this.loaderService.hideLoader();
                }
            });
        }
    }
    back() {
        this.navCtrl.back();
    }
};
SendSmsContactPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"] },
    { type: src_app_services_send_smsemail_service__WEBPACK_IMPORTED_MODULE_7__["SendSMSEmailService"] },
    { type: src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] }
];
SendSmsContactPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-send-sms-contact',
        template: _raw_loader_send_sms_contact_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_send_sms_contact_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SendSmsContactPage);



/***/ })

}]);
//# sourceMappingURL=pages-send-sms-contact-send-sms-contact-module-es2015.js.map