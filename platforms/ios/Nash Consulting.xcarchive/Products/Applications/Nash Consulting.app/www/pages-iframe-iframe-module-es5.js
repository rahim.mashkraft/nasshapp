(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-iframe-iframe-module"], {
    /***/
    "jK97":
    /*!***********************************************!*\
      !*** ./src/app/pages/iframe/iframe.module.ts ***!
      \***********************************************/

    /*! exports provided: IframePageModule */

    /***/
    function jK97(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "IframePageModule", function () {
        return IframePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _iframe_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./iframe-routing.module */
      "ofxs");
      /* harmony import */


      var _iframe_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./iframe.page */
      "tKxX");

      var IframePageModule = function IframePageModule() {
        _classCallCheck(this, IframePageModule);
      };

      IframePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _iframe_routing_module__WEBPACK_IMPORTED_MODULE_5__["IframePageRoutingModule"]],
        declarations: [_iframe_page__WEBPACK_IMPORTED_MODULE_6__["IframePage"]]
      })], IframePageModule);
      /***/
    },

    /***/
    "ofxs":
    /*!*******************************************************!*\
      !*** ./src/app/pages/iframe/iframe-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: IframePageRoutingModule */

    /***/
    function ofxs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "IframePageRoutingModule", function () {
        return IframePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _iframe_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./iframe.page */
      "tKxX");

      var routes = [{
        path: '',
        component: _iframe_page__WEBPACK_IMPORTED_MODULE_3__["IframePage"]
      }];

      var IframePageRoutingModule = function IframePageRoutingModule() {
        _classCallCheck(this, IframePageRoutingModule);
      };

      IframePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], IframePageRoutingModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-iframe-iframe-module-es5.js.map