(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-video-player-video-player-module"], {
    /***/
    "+noA":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/video-player/video-player-routing.module.ts ***!
      \*******************************************************************/

    /*! exports provided: VideoPlayerPageRoutingModule */

    /***/
    function noA(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "VideoPlayerPageRoutingModule", function () {
        return VideoPlayerPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _video_player_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./video-player.page */
      "0lAo");

      var routes = [{
        path: '',
        component: _video_player_page__WEBPACK_IMPORTED_MODULE_3__["VideoPlayerPage"]
      }];

      var VideoPlayerPageRoutingModule = function VideoPlayerPageRoutingModule() {
        _classCallCheck(this, VideoPlayerPageRoutingModule);
      };

      VideoPlayerPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], VideoPlayerPageRoutingModule);
      /***/
    },

    /***/
    "0lAo":
    /*!*********************************************************!*\
      !*** ./src/app/pages/video-player/video-player.page.ts ***!
      \*********************************************************/

    /*! exports provided: VideoPlayerPage */

    /***/
    function lAo(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "VideoPlayerPage", function () {
        return VideoPlayerPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_video_player_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./video-player.page.html */
      "LLa6");
      /* harmony import */


      var _video_player_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./video-player.page.scss */
      "cEIH");
      /* harmony import */


      var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/loader.service */
      "5dVO");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");
      /* harmony import */


      var scriptjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! scriptjs */
      "ojxP");
      /* harmony import */


      var scriptjs__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(scriptjs__WEBPACK_IMPORTED_MODULE_7__);
      /* harmony import */


      var src_app_services_quiz_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/services/quiz.service */
      "ofzi");

      var VideoPlayerPage = /*#__PURE__*/function () {
        function VideoPlayerPage(router, route, quizService, loaderService) {
          var _this = this;

          _classCallCheck(this, VideoPlayerPage);

          this.router = router;
          this.route = route;
          this.quizService = quizService;
          this.loaderService = loaderService;
          this.today = Date.now();
          this.myValue = 0;
          this.take_quiz = true;
          this.pdf = true;
          this.loaderService.showLoader();
          this.btnPlay = true;
          this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
              _this.videotitle = _this.router.getCurrentNavigation().extras.state.videotitle;
              _this.videourl = _this.router.getCurrentNavigation().extras.state.videourl;
              _this.videoid = _this.router.getCurrentNavigation().extras.state.videoid;
              _this.videoimage = _this.router.getCurrentNavigation().extras.state.videoimage;
              console.log("videoimage : ", _this.videoimage);
              _this.videoimage = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].imageUrl + _this.videoimage;
              console.log("videoimage after : ", _this.videoimage);

              _this.getQuiz(); // this.loaderService.showLoader()

            }
          });
          Object(scriptjs__WEBPACK_IMPORTED_MODULE_7__["get"])("assets/javaScript.js", function (data) {
            console.log(data);
          });
        }

        _createClass(VideoPlayerPage, [{
          key: "setCurrentTime",
          value: function setCurrentTime(data, video) {
            var sec = video.currentTime;
            var h = Math.floor(sec / 3600);
            sec = sec % 3600;
            var min = Math.floor(sec / 60);
            this.min_ = min;
            sec = Math.floor(sec % 60);
            if (sec.toString().length < 2) sec = "0" + sec;
            if (this.min_.toString().length < 2) this.min_ = "0" + this.min_;

            if (h == 0) {
              document.getElementById('lblTime').innerHTML = this.min_ + ":" + sec;
            } else {
              document.getElementById('lblTime').innerHTML = h + ":" + this.min_ + ":" + sec;
            }
          }
        }, {
          key: "onMetadata",
          value: function onMetadata(e, video) {
            var _this2 = this;

            // this.seekbar.min = 0;
            var sec = video.duration;
            var h = Math.floor(sec / 3600);
            sec = sec % 3600;
            var min = Math.floor(sec / 60);
            this.min_ = min;
            sec = Math.floor(sec % 60);
            if (sec.toString().length < 2) sec = "0" + sec;
            if (this.min_.toString().length < 2) this.min_ = "0" + this.min_;

            if (h == 0) {
              document.getElementById('lblTime2').innerHTML = this.min_ + ":" + sec;
            } else {
              document.getElementById('lblTime2').innerHTML = h + ":" + this.min_ + ":" + sec;
            }

            setTimeout(function () {
              _this2.loaderService.hideLoader();

              video.play();
            }, 500);
          }
        }, {
          key: "PlayNow",
          value: function PlayNow(video) {
            video.play();
            this.btnPause = true;
            this.btnPlay = false;
          }
        }, {
          key: "PauseNow",
          value: function PauseNow(video) {
            video.pause();
            this.btnPause = false;
            this.btnPlay = true;
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "getQuiz",
          value: function getQuiz() {
            var _this3 = this;

            var formData = new FormData();
            formData.append("aid", localStorage.getItem('user_id'));
            formData.append("videoid", this.videoid);
            this.quizService.getQuiz(formData).then(function (res) {
              console.log("getQuiz : ", res);

              if (res.quizcount > 0) {
                _this3.take_quiz = false;
              }

              _this3.quizid = res.quizid;

              if (res.downloadcount > 0) {
                _this3.pdf = false;
                _this3.pdfFile = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].downloadurl + res.downloadlink;
                console.log("this.pdfFile : ", _this3.pdfFile);
              }
            });
          }
        }]);

        return VideoPlayerPage;
      }();

      VideoPlayerPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]
        }, {
          type: src_app_services_quiz_service__WEBPACK_IMPORTED_MODULE_8__["QuizService"]
        }, {
          type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"]
        }];
      };

      VideoPlayerPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-video-player',
        template: _raw_loader_video_player_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_video_player_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], VideoPlayerPage);
      /***/
    },

    /***/
    "5dVO":
    /*!********************************************!*\
      !*** ./src/app/services/loader.service.ts ***!
      \********************************************/

    /*! exports provided: LoaderService */

    /***/
    function dVO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoaderService", function () {
        return LoaderService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var LoaderService = /*#__PURE__*/function () {
        function LoaderService(loadingCtrl) {
          _classCallCheck(this, LoaderService);

          this.loadingCtrl = loadingCtrl;
        }

        _createClass(LoaderService, [{
          key: "showLoader",
          value: function showLoader() {
            this.isBusy = true; // this.loaderToShow = this.loadingCtrl.create({
            //   message: 'Please Wait..'
            // }).then((res) => {
            //   res.present();
            //   // res.onDidDismiss().then((dis) => {
            //   //    console.log('Loading dismissed!',dis);
            //   // });
            // });
            // // this.hideLoader();
          }
        }, {
          key: "hideLoader",
          value: function hideLoader() {
            // setTimeout(()=>{
            //   this.loadingCtrl.dismiss();
            // },100)
            this.isBusy = false;
          }
        }]);

        return LoaderService;
      }();

      LoaderService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoaderService);
      /***/
    },

    /***/
    "GCVo":
    /*!***********************************************************!*\
      !*** ./src/app/pages/video-player/video-player.module.ts ***!
      \***********************************************************/

    /*! exports provided: VideoPlayerPageModule */

    /***/
    function GCVo(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "VideoPlayerPageModule", function () {
        return VideoPlayerPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _video_player_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./video-player-routing.module */
      "+noA");
      /* harmony import */


      var _video_player_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./video-player.page */
      "0lAo");
      /* harmony import */


      var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/modules/shared/shared.module */
      "FpXt");

      var VideoPlayerPageModule = function VideoPlayerPageModule() {
        _classCallCheck(this, VideoPlayerPageModule);
      };

      VideoPlayerPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _video_player_routing_module__WEBPACK_IMPORTED_MODULE_5__["VideoPlayerPageRoutingModule"], src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_video_player_page__WEBPACK_IMPORTED_MODULE_6__["VideoPlayerPage"]]
      })], VideoPlayerPageModule);
      /***/
    },

    /***/
    "LLa6":
    /*!*************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/video-player/video-player.page.html ***!
      \*************************************************************************************************/

    /*! exports provided: default */

    /***/
    function LLa6(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<header title=\"{{videotitle}}\"></header>\n<ion-content>\n  <ion-grid style=\"height: 100%\">\n\n    <div class=\"ion-text-center\" style=\"height: 300px;\">\n      <video id=\"video1\" #video1 src=\"{{videourl}}\" playsinline\n        (timeupdate)=\"setCurrentTime($event,video1)\" (loadedmetadata)=\"onMetadata($event, video1)\">\n      </video>\n      <!-- <video id=\"video1\" #video1 src=\"{{video_link}}\" (click)=\"hideShow()\"\n      (loadedmetadata)=\"onMetadata($event, video1)\"\n      (timeupdate)=\"setCurrentTime($event, video1)\"\n        playsinline poster=\"{{videoimage}}\">\n\n      </video> -->\n      <ion-row class=\"pos_play_btn ion-align-items-center ion-justify-content-center\">\n        <ion-col size=\"4\">\n        </ion-col>\n        <ion-col size=\"4\" class=\"text-center\">\n          <img id=\"btnPlay\" *ngIf=\"btnPlay\" value=\"Play\" (click)=\"PlayNow(video1)\" src=\"assets/icon/play.svg\">\n          <img id=\"btnPause\" *ngIf=\"btnPause\" value=\"Pause\" (click)=\"PauseNow(video1)\" src=\"assets/icon/pause.svg\">\n        </ion-col>\n        <ion-col size=\"4\" class=\"text-center\">\n        </ion-col>\n      </ion-row>\n      <ion-toolbar class=\"ion_toolbar_time_seekbar\">\n        <ion-row>\n          <ion-col size=\"3\">\n            <div class=\"time-display-content cbox\" aria-label=\"\">\n              <span class=\"time-first\" aria-label=\"Time elapsed0:01\" role=\"text\" id=\"lblTime\">00:00</span>\n              <span class=\"time-delimiter\" aria-hidden=\"true\">/</span>\n              <span class=\"time-second\" aria-label=\"Time duration4:53\" id=\"lblTime2\" role=\"text\">00:00</span>\n            </div>\n          </ion-col>\n          <ion-col size=\"7\" class=\"text-center\">\n            <input type=\"range\" step=\"any\" id=\"seekbar\" onchange=\"ChangeTheTime()\">\n          </ion-col>\n          <ion-col size=\"2\" class=\"text-center\">\n          </ion-col>\n        </ion-row>\n      </ion-toolbar>\n    </div>\n    <h2 class=\"ion-padding-horizontal\">\n      Step 2: Supporting Class Dcuments\n    </h2>\n    <ion-row class=\"ion-padding-horizontal\">\n      <ion-col size=\"12\">\n        <ion-button expand=\"full\" [disabled]=\"pdf\">\n          <ion-icon slot=\"start\" src=\"assets/icon/pdf.svg\"></ion-icon>Print Materials\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-input placeholder=\"Entry one (Fill in)\"></ion-input>\n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-input placeholder=\"Entry two (Fill in)\"></ion-input>\n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-textarea rows=\"6\" cols=\"20\" placeholder=\"Entry Three (Fill in)\"></ion-textarea>\n      </ion-col>\n    </ion-row>\n    <h2 class=\"ion-padding-horizontal\">\n      Step 3: Quiz\n    </h2>\n    <ion-row class=\"ion-padding-horizontal ion-margin-bottom\">\n      <ion-col size=\"12\">\n        <ion-button class=\"quiz_button\" expand=\"full\" [disabled]=\"take_quiz\" >\n          Take quiz now\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>";
      /***/
    },

    /***/
    "cEIH":
    /*!***********************************************************!*\
      !*** ./src/app/pages/video-player/video-player.page.scss ***!
      \***********************************************************/

    /*! exports provided: default */

    /***/
    function cEIH(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-button {\n  --border-radius: 0px;\n}\n\nion-icon {\n  font-size: 24px;\n}\n\n.controlIcon {\n  font-size: 20px;\n}\n\nion-range {\n  height: 20px;\n  width: 90%;\n  position: absolute;\n  top: 0;\n  margin: 0 0 0 5% !important;\n  padding: 0 !important;\n  --knob-background: #b5c827;\n  --bar-height: 8px;\n  --bar-background: white;\n  --bar-background-active: #b5c827;\n  --bar-border-radius: 6px;\n  --knob-size: 30px;\n}\n\n.duration-container {\n  height: 20px;\n  position: absolute;\n  top: 20px;\n  width: 100%;\n  margin: 0 !important;\n  padding: 0 !important;\n  display: flex;\n  flex-direction: row;\n  font-size: 16px;\n  line-height: 20px;\n  font-weight: 700;\n  color: #b5c827;\n}\n\n#albumPhoto {\n  position: absolute;\n  width: 100%;\n  background: black;\n  display: flex;\n  align-items: center;\n  flex-direction: column;\n  justify-content: center;\n}\n\n#image {\n  position: absolute;\n}\n\n.player-controls-middle {\n  position: absolute;\n  top: 50%;\n  width: 100%;\n  transform: translateY(-82%);\n}\n\n.center {\n  justify-content: center;\n}\n\n.pos_play_btn {\n  position: absolute;\n  top: 23%;\n  width: 100%;\n  transform: translateY(-82%);\n}\n\n.time-display-content {\n  pointer-events: none;\n}\n\n.cbox, .vbox, .center {\n  display: flex;\n  align-items: center;\n}\n\n.time-first, .time-second, .time-delimiter {\n  color: #ffffff;\n}\n\n.time-delimiter {\n  opacity: 0.7;\n  margin: 0 4px;\n}\n\nimg {\n  height: 50px;\n}\n\n.opacity2 {\n  opacity: 1;\n}\n\n.opity {\n  opacity: 0;\n}\n\n.ion_toolbar_time_seekbar {\n  top: -48px;\n  --background: transparent;\n  --background: #7979795c;\n}\n\n.ion_toolbar_play_pause {\n  top: -92px;\n  --background: transparent;\n}\n\n.ion_toolbar_time_seekbar1 {\n  top: -92px;\n  --background: transparent;\n  --background: #7979795c;\n}\n\n.skip_ads_button {\n  border: 2px solid #fff;\n  background: #909090;\n  margin-right: -5px;\n}\n\n.skip_ads_button ion-button {\n  --color: white;\n}\n\nion-grid {\n  padding: 0px;\n}\n\nion-grid ion-toolbar {\n  padding: 0px;\n}\n\nvideo {\n  width: 100%;\n  min-height: 300px;\n  height: 300px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\nion-range {\n  --knob-background: #606060;\n  --bar-height: 6px;\n  --bar-background: white;\n  --bar-background-active: #606060;\n  --bar-border-radius: 6px;\n  --knob-size: 16px;\n  height: 34px;\n}\n\nion-input {\n  border: 1px solid #606060;\n  --padding-start: 16px;\n}\n\nion-textarea {\n  border: 1px solid #606060;\n  --padding-start: 16px;\n}\n\n.quiz_button {\n  --background: #EB6A42;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3ZpZGVvLXBsYXllci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQkFBQTtBQUNKOztBQUdBO0VBQ0ksZUFBQTtBQUFKOztBQUlFO0VBQ0UsZUFBQTtBQURKOztBQUlFO0VBQ0UsWUFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFDQSwyQkFBQTtFQUNBLHFCQUFBO0VBQ0EsMEJBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0NBQUE7RUFDQSx3QkFBQTtFQUNBLGlCQUFBO0FBREo7O0FBSUU7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLG9CQUFBO0VBQ0EscUJBQUE7RUFDQSxhQUFBO0VBQWUsbUJBQUE7RUFDZixlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFBSjs7QUFHRTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtBQUFKOztBQUdFO0VBQ0Usa0JBQUE7QUFBSjs7QUFLQTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7RUFFRSwyQkFBQTtBQUZOOztBQUlBO0VBR0UsdUJBQUE7QUFERjs7QUFJQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7RUFFRSwyQkFBQTtBQURKOztBQUdBO0VBQ0Usb0JBQUE7QUFBRjs7QUFFQTtFQUdFLGFBQUE7RUFHQSxtQkFBQTtBQUNGOztBQUNBO0VBQ0UsY0FBQTtBQUVGOztBQUFBO0VBQ0UsWUFBQTtFQUNBLGFBQUE7QUFHRjs7QUFEQTtFQUNFLFlBQUE7QUFJRjs7QUFGQTtFQUNFLFVBQUE7QUFLRjs7QUFIQTtFQUNFLFVBQUE7QUFNRjs7QUFKQTtFQUVFLFVBQUE7RUFDQSx5QkFBQTtFQUNBLHVCQUFBO0FBTUY7O0FBSkE7RUFFRSxVQUFBO0VBQ0EseUJBQUE7QUFNRjs7QUFIQTtFQUVFLFVBQUE7RUFDQSx5QkFBQTtFQUNBLHVCQUFBO0FBS0Y7O0FBSEE7RUFDRSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUFNRjs7QUFMRTtFQUNFLGNBQUE7QUFPSjs7QUFKQTtFQUNFLFlBQUE7QUFPRjs7QUFORTtFQUNFLFlBQUE7QUFRSjs7QUFMQTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxvQkFBQTtLQUFBLGlCQUFBO0FBUUY7O0FBTkE7RUFDRSwwQkFBQTtFQUNBLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQ0FBQTtFQUNBLHdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0FBU0Y7O0FBUEE7RUFDRSx5QkFBQTtFQUNBLHFCQUFBO0FBVUY7O0FBUkE7RUFDRSx5QkFBQTtFQUNBLHFCQUFBO0FBV0Y7O0FBVEE7RUFDRSxxQkFBQTtBQVlGIiwiZmlsZSI6InZpZGVvLXBsYXllci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tYnV0dG9uIHtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDBweDtcbiAgICAvLyAtLWJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG5pb24taWNvbiB7XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICAgIC8vIGNvbG9yOiB3aGl0ZTtcbiAgfVxuICBcbiAgLmNvbnRyb2xJY29uIHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gIH1cbiAgXG4gIGlvbi1yYW5nZSB7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIHdpZHRoOiA5MCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICBtYXJnaW46IDAgMCAwIDUlIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XG4gICAgLS1rbm9iLWJhY2tncm91bmQ6ICNiNWM4Mjc7XG4gICAgLS1iYXItaGVpZ2h0OiA4cHg7XG4gICAgLS1iYXItYmFja2dyb3VuZDogd2hpdGU7XG4gICAgLS1iYXItYmFja2dyb3VuZC1hY3RpdmU6ICNiNWM4Mjc7XG4gICAgLS1iYXItYm9yZGVyLXJhZGl1czogNnB4O1xuICAgIC0ta25vYi1zaXplOiAzMHB4O1xuICB9XG4gIFxuICAuZHVyYXRpb24tY29udGFpbmVyIHtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMjBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW46IDAgIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XG4gICAgZGlzcGxheTogZmxleDsgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICBjb2xvcjogI2I1YzgyNztcbiAgfVxuICBcbiAgI2FsYnVtUGhvdG8ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgfVxuICBcbiAgI2ltYWdlIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIH1cblxuXG5cbi5wbGF5ZXItY29udHJvbHMtbWlkZGxlIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA1MCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTgyJSk7XG59XG4uY2VudGVyIHtcbiAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLnBvc19wbGF5X2J0biB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAyMyU7XG4gIHdpZHRoOiAxMDAlO1xuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTgyJSk7XG59XG4udGltZS1kaXNwbGF5LWNvbnRlbnQge1xuICBwb2ludGVyLWV2ZW50czogbm9uZTtcbn1cbi5jYm94LCAudmJveCwgLmNlbnRlciB7XG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XG4gIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi50aW1lLWZpcnN0LCAudGltZS1zZWNvbmQsIC50aW1lLWRlbGltaXRlciB7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuLnRpbWUtZGVsaW1pdGVyIHtcbiAgb3BhY2l0eTogLjc7XG4gIG1hcmdpbjogMCA0cHg7XG59XG5pbWcge1xuICBoZWlnaHQ6IDUwcHg7XG59XG4ub3BhY2l0eTIge1xuICBvcGFjaXR5OiAxO1xufVxuLm9waXR5e1xuICBvcGFjaXR5OiAwO1xufVxuLmlvbl90b29sYmFyX3RpbWVfc2Vla2JhciB7XG4gIC8vIC0tYmFja2dyb3VuZDogYmxhY2s7XG4gIHRvcDogLTQ4cHg7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIC0tYmFja2dyb3VuZDogIzc5Nzk3OTVjO1xufVxuLmlvbl90b29sYmFyX3BsYXlfcGF1c2Uge1xuICAvLyAtLWJhY2tncm91bmQ6IGJsYWNrO1xuICB0b3A6IC05MnB4O1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAvLyAtLWJhY2tncm91bmQ6ICM3OTc5Nzk1Yztcbn1cbi5pb25fdG9vbGJhcl90aW1lX3NlZWtiYXIxIHtcbiAgLy8gLS1iYWNrZ3JvdW5kOiBibGFjaztcbiAgdG9wOiAtOTJweDtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgLS1iYWNrZ3JvdW5kOiAjNzk3OTc5NWM7XG59XG4uc2tpcF9hZHNfYnV0dG9uIHtcbiAgYm9yZGVyOiAycHggc29saWQgI2ZmZjtcbiAgYmFja2dyb3VuZDogIzkwOTA5MDtcbiAgbWFyZ2luLXJpZ2h0OiAtNXB4O1xuICBpb24tYnV0dG9uIHtcbiAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgfVxufVxuaW9uLWdyaWQge1xuICBwYWRkaW5nOiAwcHg7XG4gIGlvbi10b29sYmFyIHtcbiAgICBwYWRkaW5nOiAwcHg7XG4gIH1cbn1cbnZpZGVvIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1pbi1oZWlnaHQ6IDMwMHB4O1xuICBoZWlnaHQ6IDMwMHB4O1xuICBvYmplY3QtZml0OmNvdmVyO1xufVxuaW9uLXJhbmdlIHtcbiAgLS1rbm9iLWJhY2tncm91bmQ6ICM2MDYwNjA7XG4gIC0tYmFyLWhlaWdodDogNnB4O1xuICAtLWJhci1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgLS1iYXItYmFja2dyb3VuZC1hY3RpdmU6ICM2MDYwNjA7XG4gIC0tYmFyLWJvcmRlci1yYWRpdXM6IDZweDtcbiAgLS1rbm9iLXNpemU6IDE2cHg7XG4gIGhlaWdodDogMzRweDtcbn1cbmlvbi1pbnB1dCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM2MDYwNjA7XG4gIC0tcGFkZGluZy1zdGFydDogMTZweDtcbn1cbmlvbi10ZXh0YXJlYSB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM2MDYwNjA7XG4gIC0tcGFkZGluZy1zdGFydDogMTZweDtcbn1cbi5xdWl6X2J1dHRvbiB7XG4gIC0tYmFja2dyb3VuZDogI0VCNkE0Mjtcbn0iXX0= */";
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-video-player-video-player-module-es5.js.map