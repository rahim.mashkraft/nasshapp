(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-send-smsbroadcast-send-smsbroadcast-module"],{

/***/ "3P0T":
/*!*********************************************************************!*\
  !*** ./src/app/pages/send-smsbroadcast/send-smsbroadcast.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzZW5kLXNtc2Jyb2FkY2FzdC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "59pq":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/send-smsbroadcast/send-smsbroadcast-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: SendSMSBroadcastPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendSMSBroadcastPageRoutingModule", function() { return SendSMSBroadcastPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _send_smsbroadcast_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./send-smsbroadcast.page */ "Xedt");




const routes = [
    {
        path: '',
        component: _send_smsbroadcast_page__WEBPACK_IMPORTED_MODULE_3__["SendSMSBroadcastPage"]
    }
];
let SendSMSBroadcastPageRoutingModule = class SendSMSBroadcastPageRoutingModule {
};
SendSMSBroadcastPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SendSMSBroadcastPageRoutingModule);



/***/ }),

/***/ "SkaZ":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/send-smsbroadcast/send-smsbroadcast.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">Send SMS Broadcast</ion-title>\n    <ion-icon slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" routerLink=\"/tabs/tab1\"></ion-icon>\n  </ion-toolbar>\n</ion-header> -->\n<header title=\"Send SMS Broadcast\"></header>\n<ion-content>\n  <ion-item>\n    <ion-label class=\"heading_font\" style=\"font-size: 26px !important\" position=\"stacked\"><strong>Message</strong></ion-label>\n    <ion-input placeholder=\"Message\" (ionFocus)=\"ionFocus($event)\" [(ngModel)]=\"message\"></ion-input>\n  </ion-item>\n  <div class=\"error-div\">\n    <p ion-text class=\"text08\" *ngIf=\"message_error != ''\">\n      <ion-text color=\"warning\">\n        {{message_error}}\n      </ion-text>\n    </p>\n  </div>\n  <ion-item>\n    <ion-label class=\"heading_font\"><strong>Select Contact Group</strong></ion-label>\n  </ion-item>\n  <ion-list *ngIf=\"contact_groups\" mode=\"md\">\n    <ion-radio-group value=\"radio_check\" [(ngModel)]=\"radio_check\">\n      <ion-item *ngFor=\"let item of contact_groups\">\n        <ion-label>{{item.title}}</ion-label>\n        <ion-radio slot=\"start\" value=\"{{item.id}}\"></ion-radio>\n      </ion-item>\n    </ion-radio-group>\n  </ion-list>\n  <div class=\"error-div\">\n    <p ion-text class=\"text08\" *ngIf=\"radio_check_error != ''\">\n      <ion-text color=\"warning\">\n        {{radio_check_error}}\n      </ion-text>\n    </p>\n  </div>\n  <ion-item>\n    <ion-label class=\"heading_font\"><strong>Select Date / Time</strong></ion-label>\n  </ion-item>\n  <ion-item mode=\"ios\">\n    <ion-label>Date</ion-label>\n    <ion-datetime display-format=\"MMM D, YYYY\" picker-format=\"MMM D, YYYY\" [(ngModel)]=\"myDate\" (ionChange)=\"showdate()\"\n      value=\"1990-02-19\" placeholder=\"Select Date\">\n    </ion-datetime>\n  </ion-item>\n  <div class=\"error-div\">\n    <p ion-text class=\"text08\" *ngIf=\"myDate != ''\">\n      <ion-text color=\"warning\">\n        {{myDate_error}}\n      </ion-text>\n    </p>\n  </div>\n  <ion-item mode=\"ios\">\n    <ion-label>Time</ion-label>\n    <ion-datetime display-format=\"h:mm A\" picker-format=\"h:mm A\" [(ngModel)]=\"myTime\" (ionChange)=\"showtime()\"\n      value=\"1990-02-19T07:43Z\">\n    </ion-datetime>\n  </ion-item>\n  <div class=\"error-div\">\n    <p ion-text class=\"text08\" *ngIf=\"myTime_error != ''\">\n      <ion-text color=\"warning\">\n        {{myTime_error}}\n      </ion-text>\n    </p>\n  </div>\n</ion-content>\n<ion-footer>\n  <ion-row padding-vertical>\n    <ion-col margin-left margin-right no-padding>\n      <ion-button expand=\"full\" (click)=\"submit()\">Submit</ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>");

/***/ }),

/***/ "Xedt":
/*!*******************************************************************!*\
  !*** ./src/app/pages/send-smsbroadcast/send-smsbroadcast.page.ts ***!
  \*******************************************************************/
/*! exports provided: SendSMSBroadcastPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendSMSBroadcastPage", function() { return SendSMSBroadcastPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_send_smsbroadcast_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./send-smsbroadcast.page.html */ "SkaZ");
/* harmony import */ var _send_smsbroadcast_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./send-smsbroadcast.page.scss */ "3P0T");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/contact-groups.service */ "rtEV");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "wd/R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/toast.service */ "2g2N");
/* harmony import */ var src_app_services_broadcast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/broadcast.service */ "Ev3c");
/* harmony import */ var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/loader.service */ "5dVO");










let SendSMSBroadcastPage = class SendSMSBroadcastPage {
    constructor(navCtrl, contactGroupService, toastService, broadcastService, loaderService) {
        this.navCtrl = navCtrl;
        this.contactGroupService = contactGroupService;
        this.toastService = toastService;
        this.broadcastService = broadcastService;
        this.loaderService = loaderService;
        this.radio_check = 0;
        this.myDate = '';
        this.myTime = '';
        this.message = '';
        this.radio_check_error = '';
        this.myDate_error = '';
        this.myTime_error = '';
        this.message_error = '';
        this.check_validation = false;
    }
    ngOnInit() {
    }
    ionFocus(event) {
        this.message_error = '';
        console.log(event);
    }
    validation() {
        if (this.message == '') {
            this.message_error = 'This field is required';
            return;
        }
        if (this.radio_check == 0) {
            this.radio_check_error = 'This field is required';
            return;
        }
        if (this.myDate == '') {
            this.myDate_error = 'This field is required';
            return;
        }
        if (this.myTime == '') {
            this.myTime_error = 'This field is required';
            return;
        }
        this.check_validation = true;
    }
    showdate() {
        this.date_format = moment__WEBPACK_IMPORTED_MODULE_6__(this.myDate).format("MM/D/YYYY");
    }
    showtime() {
        this.hours = moment__WEBPACK_IMPORTED_MODULE_6__(this.myTime).format('h A');
        this.minutes = moment__WEBPACK_IMPORTED_MODULE_6__(this.myTime).format('mm');
    }
    ionViewWillEnter() {
        this.myDate = new Date().toISOString();
        this.myTime = new Date().toISOString();
        this.radio_check = 1;
        this.getContactGroups();
    }
    getContactGroups() {
        this.contactGroupService.getContactGroups().then((res) => {
            console.log(res);
            if (res.status !== false) {
                this.contact_groups = res.contactgroup;
            }
        });
    }
    submit() {
        this.validation();
        if (this.check_validation == true) {
            this.loaderService.showLoader();
            this.date_format = moment__WEBPACK_IMPORTED_MODULE_6__(this.myDate).format("MM/D/YYYY");
            this.hours = moment__WEBPACK_IMPORTED_MODULE_6__(this.myTime).format('h A');
            this.minutes = moment__WEBPACK_IMPORTED_MODULE_6__(this.myTime).format('mm');
            let formData = new FormData();
            formData.append("aid", localStorage.getItem('user_id'));
            formData.append("bdate", this.date_format);
            formData.append("bhour", this.hours);
            formData.append("bmin", this.minutes);
            formData.append("cid", this.radio_check);
            formData.append("message", this.message);
            this.broadcastService.smsbroadcast(formData).then((res) => {
                if (res.status !== false) {
                    this.toastService.showToast('SMS Broadcast sent successfully.');
                    this.radio_check = 0;
                    this.myDate = '';
                    this.myTime = '';
                    this.message = '';
                    this.loaderService.hideLoader();
                    this.back();
                    // this.contact_groups = res.contactgroup
                }
            });
        }
    }
    back() {
        this.navCtrl.back();
    }
};
SendSMSBroadcastPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_5__["ContactGroupsService"] },
    { type: src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"] },
    { type: src_app_services_broadcast_service__WEBPACK_IMPORTED_MODULE_8__["BroadcastService"] },
    { type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_9__["LoaderService"] }
];
SendSMSBroadcastPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-send-smsbroadcast',
        template: _raw_loader_send_smsbroadcast_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_send_smsbroadcast_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SendSMSBroadcastPage);



/***/ }),

/***/ "vw/y":
/*!*********************************************************************!*\
  !*** ./src/app/pages/send-smsbroadcast/send-smsbroadcast.module.ts ***!
  \*********************************************************************/
/*! exports provided: SendSMSBroadcastPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendSMSBroadcastPageModule", function() { return SendSMSBroadcastPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _send_smsbroadcast_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./send-smsbroadcast-routing.module */ "59pq");
/* harmony import */ var _send_smsbroadcast_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./send-smsbroadcast.page */ "Xedt");
/* harmony import */ var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modules/shared/shared.module */ "FpXt");








let SendSMSBroadcastPageModule = class SendSMSBroadcastPageModule {
};
SendSMSBroadcastPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _send_smsbroadcast_routing_module__WEBPACK_IMPORTED_MODULE_5__["SendSMSBroadcastPageRoutingModule"],
            src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_send_smsbroadcast_page__WEBPACK_IMPORTED_MODULE_6__["SendSMSBroadcastPage"]]
    })
], SendSMSBroadcastPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-send-smsbroadcast-send-smsbroadcast-module-es2015.js.map