(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-send-sms-contact-send-sms-contact-module"], {
    /***/
    "2g2N":
    /*!*******************************************!*\
      !*** ./src/app/services/toast.service.ts ***!
      \*******************************************/

    /*! exports provided: ToastService */

    /***/
    function g2N(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ToastService", function () {
        return ToastService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var ToastService = /*#__PURE__*/function () {
        function ToastService(toastCtrl) {
          _classCallCheck(this, ToastService);

          this.toastCtrl = toastCtrl;
        }

        _createClass(ToastService, [{
          key: "showToast",
          value: function showToast() {
            var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Successfully Logged in!';
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastCtrl.create({
                        cssClass: 'bg-toast',
                        message: message,
                        duration: 3000,
                        position: 'bottom'
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "showToastUpdateProfile",
          value: function showToastUpdateProfile(message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var toast;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.toastCtrl.create({
                        cssClass: 'bg-toast',
                        message: message,
                        duration: 3000,
                        position: 'bottom'
                      });

                    case 2:
                      toast = _context2.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return ToastService;
      }();

      ToastService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      ToastService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ToastService);
      /***/
    },

    /***/
    "5dVO":
    /*!********************************************!*\
      !*** ./src/app/services/loader.service.ts ***!
      \********************************************/

    /*! exports provided: LoaderService */

    /***/
    function dVO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoaderService", function () {
        return LoaderService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var LoaderService = /*#__PURE__*/function () {
        function LoaderService(loadingCtrl) {
          _classCallCheck(this, LoaderService);

          this.loadingCtrl = loadingCtrl;
        }

        _createClass(LoaderService, [{
          key: "showLoader",
          value: function showLoader() {
            this.isBusy = true; // this.loaderToShow = this.loadingCtrl.create({
            //   message: 'Please Wait..'
            // }).then((res) => {
            //   res.present();
            //   // res.onDidDismiss().then((dis) => {
            //   //    console.log('Loading dismissed!',dis);
            //   // });
            // });
            // // this.hideLoader();
          }
        }, {
          key: "hideLoader",
          value: function hideLoader() {
            // setTimeout(()=>{
            //   this.loadingCtrl.dismiss();
            // },100)
            this.isBusy = false;
          }
        }]);

        return LoaderService;
      }();

      LoaderService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoaderService);
      /***/
    },

    /***/
    "Bqzo":
    /*!***************************************************************************!*\
      !*** ./src/app/pages/send-sms-contact/send-sms-contact-routing.module.ts ***!
      \***************************************************************************/

    /*! exports provided: SendSmsContactPageRoutingModule */

    /***/
    function Bqzo(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SendSmsContactPageRoutingModule", function () {
        return SendSmsContactPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _send_sms_contact_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./send-sms-contact.page */
      "gP5U");

      var routes = [{
        path: '',
        component: _send_sms_contact_page__WEBPACK_IMPORTED_MODULE_3__["SendSmsContactPage"]
      }];

      var SendSmsContactPageRoutingModule = function SendSmsContactPageRoutingModule() {
        _classCallCheck(this, SendSmsContactPageRoutingModule);
      };

      SendSmsContactPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SendSmsContactPageRoutingModule);
      /***/
    },

    /***/
    "CLQu":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/send-sms-contact/send-sms-contact.page.scss ***!
      \*******************************************************************/

    /*! exports provided: default */

    /***/
    function CLQu(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzZW5kLXNtcy1jb250YWN0LnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "fp8Y":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/send-sms-contact/send-sms-contact.module.ts ***!
      \*******************************************************************/

    /*! exports provided: SendSmsContactPageModule */

    /***/
    function fp8Y(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SendSmsContactPageModule", function () {
        return SendSmsContactPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _send_sms_contact_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./send-sms-contact-routing.module */
      "Bqzo");
      /* harmony import */


      var _send_sms_contact_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./send-sms-contact.page */
      "gP5U");
      /* harmony import */


      var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/modules/shared/shared.module */
      "FpXt");

      var SendSmsContactPageModule = function SendSmsContactPageModule() {
        _classCallCheck(this, SendSmsContactPageModule);
      };

      SendSmsContactPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _send_sms_contact_routing_module__WEBPACK_IMPORTED_MODULE_5__["SendSmsContactPageRoutingModule"], src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_send_sms_contact_page__WEBPACK_IMPORTED_MODULE_6__["SendSmsContactPage"]]
      })], SendSmsContactPageModule);
      /***/
    },

    /***/
    "gKmf":
    /*!*********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/send-sms-contact/send-sms-contact.page.html ***!
      \*********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function gKmf(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">Send SMS To Contact</ion-title>\n    <ion-icon slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" routerLink=\"/tabs/tab1\"></ion-icon>\n  </ion-toolbar>\n</ion-header> -->\n<header title=\"Send SMS To Contact\"></header>\n<ion-content>\n  <ion-item>\n    <ion-label class=\"heading_font\" style=\"font-size: 26px !important\" position=\"stacked\"><strong>Phone</strong></ion-label>\n    <ion-input placeholder=\"Phone Number\" type=\"text\" [(ngModel)]=\"phone_number\"></ion-input>\n  </ion-item>\n  <div class=\"error-div\">\n    <p ion-text class=\"text08\" *ngIf=\"Phone_error != ''\">\n      <ion-text color=\"warning\">\n        {{Phone_error}}\n      </ion-text>\n    </p>\n  </div>\n  <ion-item>\n    <ion-label class=\"heading_font\" style=\"\" position=\"stacked\"><strong>Name</strong></ion-label>\n    <ion-input placeholder=\"Title\" [(ngModel)]=\"name\"></ion-input>\n  </ion-item>\n  <div class=\"error-div\">\n    <p ion-text class=\"text08\" *ngIf=\"name_error != ''\">\n      <ion-text color=\"warning\">\n        {{name_error}}\n      </ion-text>\n    </p>\n  </div>\n  <ion-item>\n    <ion-label class=\"heading_font\" style=\"font-size: 26px !important\" position=\"stacked\"><strong>Message</strong></ion-label>\n    <ion-input placeholder=\"Message\" [(ngModel)]=\"message\"></ion-input>\n  </ion-item>\n  <div class=\"error-div\">\n    <p ion-text class=\"text08\" *ngIf=\"message_error != ''\">\n      <ion-text color=\"warning\">\n        {{message_error}}\n      </ion-text>\n    </p>\n  </div>\n</ion-content>\n<ion-footer>\n  <ion-row padding-vertical>\n    <ion-col margin-left margin-right no-padding>\n      <ion-button expand=\"full\" (click)=\"sendsms()\">Submit</ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>";
      /***/
    },

    /***/
    "gP5U":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/send-sms-contact/send-sms-contact.page.ts ***!
      \*****************************************************************/

    /*! exports provided: SendSmsContactPage */

    /***/
    function gP5U(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SendSmsContactPage", function () {
        return SendSmsContactPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_send_sms_contact_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./send-sms-contact.page.html */
      "gKmf");
      /* harmony import */


      var _send_sms_contact_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./send-sms-contact.page.scss */
      "CLQu");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/loader.service */
      "5dVO");
      /* harmony import */


      var src_app_services_send_smsemail_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/send-smsemail.service */
      "Xu8g");
      /* harmony import */


      var src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/services/toast.service */
      "2g2N");

      var SendSmsContactPage = /*#__PURE__*/function () {
        function SendSmsContactPage(router, route, loaderService, sendSmsemailService, toastService, navCtrl) {
          var _this = this;

          _classCallCheck(this, SendSmsContactPage);

          this.router = router;
          this.route = route;
          this.loaderService = loaderService;
          this.sendSmsemailService = sendSmsemailService;
          this.toastService = toastService;
          this.navCtrl = navCtrl; // error_variable

          this.Phone_error = '';
          this.name_error = '';
          this.message_error = '';
          this.check_validation = false;
          this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
              _this.item_obj = _this.router.getCurrentNavigation().extras.state.item_obj;
              _this.pageName = _this.router.getCurrentNavigation().extras.state.pageName;
              console.log("this.item_obj : ", _this.item_obj);
              _this.phone_number = _this.item_obj.phone;
              _this.name = _this.item_obj.firstname + ' ' + _this.item_obj.lastname;

              if (_this.pageName == 'member') {
                _this.affaid = _this.item_obj.affaid;
              } else {
                _this.cid = _this.item_obj.cid;
              }
            }
          });
        }

        _createClass(SendSmsContactPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "validation",
          value: function validation() {
            if (this.phone_number == '') {
              this.Phone_error = 'This field is required';
              return;
            }

            if (this.name == '') {
              this.name_error = 'This field is required';
              return;
            }

            if (this.message == '') {
              this.message_error = 'This field is required';
              return;
            }

            this.check_validation = true;
          }
        }, {
          key: "sendsms",
          value: function sendsms() {
            var _this2 = this;

            this.validation();

            if (this.check_validation == true) {
              this.loaderService.showLoader();
              var formData = new FormData();
              formData.append("aid", localStorage.getItem('user_id'));

              if (this.pageName == 'member') {
                formData.append("affaid", this.affaid);
              } else {
                formData.append("cid", this.cid);
              }

              formData.append("name", this.name);
              formData.append("phone", this.phone_number);
              formData.append("message", this.message);
              this.sendSmsemailService.sendsms(formData).then(function (res) {
                console.log(res);

                if (res.status !== false) {
                  _this2.toastService.showToast(res.message);

                  _this2.loaderService.hideLoader(); // this.navCtrl.navigateRoot('/tabs');


                  _this2.back(); // this.contact_groups = res.contactgroup

                } else {
                  _this2.toastService.showToast(res.message);

                  _this2.loaderService.hideLoader();
                }
              });
            }
          }
        }, {
          key: "back",
          value: function back() {
            this.navCtrl.back();
          }
        }]);

        return SendSmsContactPage;
      }();

      SendSmsContactPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"]
        }, {
          type: src_app_services_send_smsemail_service__WEBPACK_IMPORTED_MODULE_7__["SendSMSEmailService"]
        }, {
          type: src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }];
      };

      SendSmsContactPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-send-sms-contact',
        template: _raw_loader_send_sms_contact_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_send_sms_contact_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], SendSmsContactPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-send-sms-contact-send-sms-contact-module-es5.js.map