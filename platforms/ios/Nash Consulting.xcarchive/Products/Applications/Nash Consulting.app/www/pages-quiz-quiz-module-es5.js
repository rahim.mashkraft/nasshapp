(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-quiz-quiz-module"], {
    /***/
    "8qqv":
    /*!*******************************************!*\
      !*** ./src/app/pages/quiz/quiz.page.scss ***!
      \*******************************************/

    /*! exports provided: default */

    /***/
    function qqv(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".f-27 {\n  font-size: 27px;\n}\n\n.timer {\n  display: flex;\n  align-items: center;\n}\n\n.timer-text {\n  transform: rotate(90deg);\n  transform-origin: center;\n  font-size: 36px;\n  text-anchor: middle;\n  font-weight: 600;\n  fill: #333;\n}\n\n#progress-circle {\n  margin-top: 50px;\n  transform: rotate(-90deg);\n}\n\n.progress-wrapper {\n  position: relative;\n  margin: 20px auto;\n  font-size: 21px;\n}\n\n.progress {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  line-height: 9em;\n  font-size: 1em;\n  text-align: center;\n  font-weight: 100;\n}\n\n.flex-container {\n  display: flex;\n  flex-direction: row;\n  height: 100%;\n  align-items: center;\n  justify-content: center;\n  overflow: hidden;\n}\n\n@media screen and (max-width: 600px) {\n  .flex-container {\n    flex-wrap: wrap;\n  }\n  .flex-container .progress {\n    height: unset;\n  }\n}\n\n.flex-container .progress {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3F1aXoucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtBQUNKOztBQUNBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FBRUo7O0FBQUE7RUFDSSx3QkFBQTtFQUNBLHdCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0FBR0o7O0FBREE7RUFDSSxnQkFBQTtFQUNBLHlCQUFBO0FBSUo7O0FBRkE7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQUtKOztBQUZFO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUFLSjs7QUFIRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7QUFNSjs7QUFKSTtFQUNFO0lBQ0UsZUFBQTtFQU1OO0VBSkk7SUFDRSxhQUFBO0VBTU47QUFDRjs7QUFISTtFQUNFLFlBQUE7QUFLTiIsImZpbGUiOiJxdWl6LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mLTI3IHtcbiAgICBmb250LXNpemU6IDI3cHg7XG59XG4udGltZXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi50aW1lci10ZXh0IHtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG4gICAgdHJhbnNmb3JtLW9yaWdpbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMzZweDtcbiAgICB0ZXh0LWFuY2hvcjogbWlkZGxlO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZmlsbDogIzMzMztcbn1cbiNwcm9ncmVzcy1jaXJjbGUge1xuICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoLTkwZGVnKTtcbn1cbi5wcm9ncmVzcy13cmFwcGVyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luOiAyMHB4IGF1dG87XG4gICAgZm9udC1zaXplOiAyMXB4O1xuICB9XG4gIFxuICAucHJvZ3Jlc3Mge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgbGluZS1oZWlnaHQ6IDllbTtcbiAgICBmb250LXNpemU6IDFlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcbiAgfVxuICAuZmxleC1jb250YWluZXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIFxuICAgIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gICAgICAmIHtcbiAgICAgICAgZmxleC13cmFwOiB3cmFwO1xuICAgICAgfVxuICAgICAgLnByb2dyZXNzIHtcbiAgICAgICAgaGVpZ2h0OiB1bnNldDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAucHJvZ3Jlc3Mge1xuICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgfSJdfQ== */";
      /***/
    },

    /***/
    "CQ+c":
    /*!*********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/quiz/quiz.page.html ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function CQC(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back(0)\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">Quiz Title</ion-title>\n    <div slot=\"end\" class=\"mr-5 timer\">\n      <ion-icon class=\"f-27 mr-5\" color=\"secondary\" name=\"time\"></ion-icon>\n      <ion-text color=\"secondary\">{{ time | async}}</ion-text>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <!-- <ion-grid *ngIf=\"!start_quiz\">\n    <ion-row class=\"ion-padding-\">\n      <ion-col size=\"12\">\n        <h2 class=\"ion-no-margin\">{{data?.title}}</h2>\n      </ion-col>\n      <ion-col size=\"12\">\n        <p class=\"ion-no-margin-\">{{data?.description}}</p>\n      </ion-col>\n      <ion-col size=\"12\">\n        <h4 class=\"ion-no-margin\">Total Question {{data?.maxquestions}}</h4>\n      </ion-col>\n      <ion-col size=\"12\">\n        <h4 class=\"ion-no-margin\">Quiz Time {{data?.quiztime}}</h4>\n      </ion-col>\n      <ion-col size=\"12\">\n        <h4 class=\"ion-no-margin\">Pass Score {{data?.passcore}}</h4>\n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-button (click)=\"startQuiz()\">\n          Start Quiz\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid> -->\n  <!-- <ion-slides #slides>\n    <ion-slide> -->\n      <ion-row>\n        <ion-col size=\"12\">\n          <h3 class=\"ion-text-center\">Question: {{questions?.questionid}}</h3>\n        </ion-col>\n        <ion-col size=\"12\">\n          <h3> {{questions?.questiontext}}</h3>\n        </ion-col>\n        <ion-col size=\"12\" *ngIf=\"questions?.type == 'mcqs'\">\n          <ion-list>\n            <ion-radio-group mode=\"ios\">\n              <ion-item *ngFor=\"let answer of questions.answers; let i = index;\">\n                <ion-label>{{i+1}}. {{answer.answertext}}</ion-label>\n                <ion-radio (click)=\"selectAnswer(answer, questions)\" [disabled]=\"hasAnswered\"></ion-radio>\n              </ion-item>\n            </ion-radio-group>\n          </ion-list>\n        </ion-col>\n        <ion-col size=\"12\" *ngIf=\"questions?.type == 'text'\">\n          <ion-item>\n            <!-- <ion-label>Notes</ion-label> -->\n            <ion-textarea rows=\"6\" cols=\"20\" placeholder=\"Write your answer here...\"></ion-textarea>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    <!-- </ion-slide>\n  </ion-slides> -->\n  <div class=\"ion-text-end\" *ngIf=\"questions\">\n    <ion-button (click)=\"nextSlide()\">\n      Next Question\n    </ion-button>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "EnSQ":
    /*!******************************************!*\
      !*** ./src/app/services/data.service.ts ***!
      \******************************************/

    /*! exports provided: DataService */

    /***/
    function EnSQ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DataService", function () {
        return DataService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var DataService = /*#__PURE__*/function () {
        function DataService(http) {
          _classCallCheck(this, DataService);

          this.http = http;
        }

        _createClass(DataService, [{
          key: "load",
          value: function load() {
            var _this = this;

            if (this.data) {
              return Promise.resolve(this.data);
            }

            return new Promise(function (resolve) {
              _this.http.get('assets/data/questions.json').subscribe(function (data) {
                resolve(data);
              });
            });
          }
        }]);

        return DataService;
      }();

      DataService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      };

      DataService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], DataService);
      /***/
    },

    /***/
    "F2WD":
    /*!*******************************************!*\
      !*** ./src/app/pages/quiz/quiz.module.ts ***!
      \*******************************************/

    /*! exports provided: QuizPageModule */

    /***/
    function F2WD(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "QuizPageModule", function () {
        return QuizPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _quiz_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./quiz-routing.module */
      "fVPh");
      /* harmony import */


      var _quiz_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./quiz.page */
      "naDh");

      var QuizPageModule = function QuizPageModule() {
        _classCallCheck(this, QuizPageModule);
      };

      QuizPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _quiz_routing_module__WEBPACK_IMPORTED_MODULE_5__["QuizPageRoutingModule"]],
        declarations: [_quiz_page__WEBPACK_IMPORTED_MODULE_6__["QuizPage"]]
      })], QuizPageModule);
      /***/
    },

    /***/
    "fVPh":
    /*!***************************************************!*\
      !*** ./src/app/pages/quiz/quiz-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: QuizPageRoutingModule */

    /***/
    function fVPh(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "QuizPageRoutingModule", function () {
        return QuizPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _quiz_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./quiz.page */
      "naDh");

      var routes = [{
        path: '',
        component: _quiz_page__WEBPACK_IMPORTED_MODULE_3__["QuizPage"]
      }];

      var QuizPageRoutingModule = function QuizPageRoutingModule() {
        _classCallCheck(this, QuizPageRoutingModule);
      };

      QuizPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], QuizPageRoutingModule);
      /***/
    },

    /***/
    "naDh":
    /*!*****************************************!*\
      !*** ./src/app/pages/quiz/quiz.page.ts ***!
      \*****************************************/

    /*! exports provided: QuizPage */

    /***/
    function naDh(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "QuizPage", function () {
        return QuizPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_quiz_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./quiz.page.html */
      "CQ+c");
      /* harmony import */


      var _quiz_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./quiz.page.scss */
      "8qqv");
      /* harmony import */


      var _success_model_success_model_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./../success-model/success-model.page */
      "hnmG");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_app_services_quiz_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/quiz.service */
      "ofzi");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! src/app/services/data.service */
      "EnSQ");

      var circleR = 80;
      var circleDasharray = 2 * Math.PI * circleR;

      var QuizPage = /*#__PURE__*/function () {
        function QuizPage(navCtrl, alertController, route, quizService, router, modalController, dataService, changeDetector) {
          var _this2 = this;

          _classCallCheck(this, QuizPage);

          this.navCtrl = navCtrl;
          this.alertController = alertController;
          this.route = route;
          this.quizService = quizService;
          this.router = router;
          this.modalController = modalController;
          this.dataService = dataService;
          this.changeDetector = changeDetector;
          this.hasAnswered = false;
          this.score = 0;
          this.start_timer = false;
          this.circleR = circleR;
          this.circleDasharray = circleDasharray;
          this.startDuration = 5;
          this.time = new rxjs__WEBPACK_IMPORTED_MODULE_8__["BehaviorSubject"]('00:00');
          this.percent = new rxjs__WEBPACK_IMPORTED_MODULE_8__["BehaviorSubject"](100);
          this.state = 'stop';
          this.start_quiz = false;
          this.questionid = 0;
          this.answerid = 0;
          this.attemptid = 0; // this.dataService.load().then((data:any) => {
          //   this.questions = data.questions;
          //   this.nextSlide()
          // });

          this.route.queryParams.subscribe(function (params) {
            if (_this2.router.getCurrentNavigation().extras.state) {
              _this2.quizid = _this2.router.getCurrentNavigation().extras.state.quizids;
              _this2.quiztime = _this2.router.getCurrentNavigation().extras.state.quiztimes;
              console.log("videoimage after : ", _this2.quizid);
              console.log("quiztime after : ", _this2.quiztime);

              _this2.performquiz();
            }
          }); // this.startQuiz();
        }

        _createClass(QuizPage, [{
          key: "performquiz",
          value: function performquiz() {
            var _this3 = this;

            var formData = new FormData();
            formData.append("aid", localStorage.getItem('user_id'));
            formData.append("quizid", this.quizid);

            if (this.questionid != 0) {
              formData.append("questionid", this.questionid);
              formData.append("answerid", this.answerid);
              formData.append("attemptid", this.attemptid);
            }

            this.quizService.performquiz(formData).then(function (data) {
              if (data.status) {
                _this3.questions = data; // this.showAlert(this.data)
                // this.nextSlide();

                _this3.startTimer(_this3.quiztime);
              } else {
                _this3.openGifModel(data.message); // this.alertService.presentAlertError(data.message);

              }
            });
          }
        }, {
          key: "openGifModel",
          value: function openGifModel(data) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this4 = this;

              var modal;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.modalController.create({
                        component: _success_model_success_model_page__WEBPACK_IMPORTED_MODULE_3__["SuccessModelPage"],
                        componentProps: {
                          value: data
                        }
                      });

                    case 2:
                      modal = _context.sent;
                      modal.onDidDismiss().then(function (data) {
                        console.log(data);

                        _this4.navCtrl.back();
                      });
                      _context.next = 6;
                      return modal.present();

                    case 6:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "startQuiz",
          value: function startQuiz() {
            var _this5 = this;

            this.start_quiz = true;
            this.dataService.load().then(function (data) {
              _this5.questions = data.questions;

              _this5.nextSlide();

              _this5.startTimer(_this5.startDuration);
            });
          }
        }, {
          key: "startTimer",
          value: function startTimer(duration) {
            var _this6 = this;

            this.state = 'start';
            clearInterval(this.interval);
            this.timer = duration * 60; // this.updateTimeValue();

            this.interval = setInterval(function () {
              _this6.updateTimeValue();
            }, 1000);
          }
        }, {
          key: "stopTimer",
          value: function stopTimer() {
            clearInterval(this.interval);
            this.time.next('00:00');
            this.state = 'stop';
          }
        }, {
          key: "updateTimeValue",
          value: function updateTimeValue() {
            var minutes = this.timer / 60;
            var seconds = this.timer % 60;
            minutes = String('0' + Math.floor(minutes)).slice(-2);
            seconds = String('0' + Math.floor(seconds)).slice(-2);
            var text = minutes + ':' + seconds;
            this.time.next(text);
            var totalTime = this.startDuration * 5;
            var percentage = (totalTime - this.timer) / totalTime * 100;
            this.percent.next(percentage);
            --this.timer;

            if (this.timer < 0) {
              // this.startTimer(this.startDuration);
              this.stopTimer();
            }
          }
        }, {
          key: "ionViewDidLoad",
          value: function ionViewDidLoad() {
            this.slides.lockSwipes(true);
          }
        }, {
          key: "nextSlide",
          value: function nextSlide() {
            this.start_timer = true;
            this.performquiz(); // this.startTimer(this.startDuration)
            // this.slides.lockSwipes(false);
            // this.slides.slideNext();
            // this.slides.lockSwipes(true);
          }
        }, {
          key: "selectAnswer",
          value: function selectAnswer(answer, question) {
            console.log("answer : ", answer);
            console.log("question : ", question);
            this.questionid = answer.id;
            this.answerid = question.questionid;
            this.attemptid = question.attemptid; // this.hasAnswered = true;
            // answer.selected = true;
            // question.flashCardFlipped = true;
            // if (answer.correct) {
            //   this.score++;
            // }
            // setTimeout(() => {
            // this.hasAnswered = false;
            // // this.nextSlide();
            // answer.selected = false;
            // question.flashCardFlipped = false;
            // }, 1000);
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {// this.route.queryParams.subscribe(params => {
            //   // if (params) {
            //   //   console.log(params)
            //   //   // let queryParams = JSON.parse(params);
            //   //   // console.log(queryParams)
            //   // }
            //   if (this.router.getCurrentNavigation().extras.state) {
            //     this.quizid = this.router.getCurrentNavigation().extras.state.quizid;
            //     console.log("quizid : ", this.quizid)
            //     if(this.quizid){
            //       this.quizdetails()
            //     }
            //   }
            // });
            // this.questionsForm = this.formBuilder.group({
            //   id: null,
            //   questionsArray: this.formBuilder.array([])
            // });
            // this.questionsArray = this.questionsForm.get('questionsArray') as FormArray;
          } // startCountDown() {
          //   this.countDownInterval = setInterval(() => {
          //     this.counter--;
          //     if (this.counter === 0) {
          //       clearInterval(this.countDownInterval);
          //       this.submit(true);
          //     }
          //     this.changeDetector.detectChanges();
          //   }, 1000);
          // }
          // transform(time: number): string {
          //   if (time >= 60) {
          //     const timeInMinutes: number = Math.floor(time / 60);
          //     const hours: number = Math.floor(time / 3600);
          //     let minutes = timeInMinutes;
          //     if (minutes >= 60) {
          //       minutes = timeInMinutes % 60;
          //     }
          //     return (hours > 0 ? '0' + hours + ': ' : '')
          //       + (minutes < 10 ? '0' + minutes : minutes) +
          //       ': ' + ((time - timeInMinutes * 60) < 10 ? '0' +
          //         (time - timeInMinutes * 60) : (time - timeInMinutes * 60));
          //   } else {
          //     return (time < 10 ? '00: 0' + time : '00:' + time) + '';
          //   }
          // }
          // initializeForm() {
          //   this.questions.forEach(element => {
          //     this.questionsArray.push(this.questionGroup());
          //   });
          // }
          // questionGroup(): FormGroup {
          //   return this.formBuilder.group({
          //     questionanswerid: [null, Validators.required],
          //     points: [null]
          //   });
          // }

        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.showLoader();

                    case 2:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          } // getAllQuizes() {
          //   let formData = new FormData();
          //   formData.append("aid", localStorage.getItem('user_id'));
          //   formData.append("quizid", '1');
          //   this.quizService.getAllQuizes(formData).then((res: any) => {
          //     console.log("getAllQuizes : ", res);
          //     // if (res.quizcount > 0) {
          //     //   this.take_quiz = false
          //     // }
          //     // if (res.downloadcount > 0) {
          //     //   this.pdf = false
          //     //   this.pdfFile = environment.downloadurl + res.downloadlink;
          //     //   console.log("this.pdfFile : ", this.pdfFile)
          //     // }
          //   });
          // }
          // fetchQuizQuestions() {
          //   let formData = new FormData();
          //   formData.append("aid", localStorage.getItem('user_id'));
          //   formData.append("quizid", '1');
          //   this.quizService.getAllQuizes(formData).then((res: any) => {
          //     this.counter = res.quiztime;
          //     this.questions = res.quiz_questions;
          //     this.totalpoints = res.totalscore;
          //     if (this.counter && this.questions.length) {
          //       this.saveQuizAttempt();
          //     } else {
          //       this.loading = false;
          //     }
          //   },
          //   err => {
          //     this.loading = false;
          //   });
          //   // this.quizService.getAllQuizes({
          //   //   quizid: this.id,
          //   //   memberid: localStorage.getItem('user_id'),
          //   // })
          //   //   .then((res: any) => {
          //   //     this.counter = res.data[0].quiztime;
          //   //     this.questions = res.data[0].quiz_questions;
          //   //     this.totalpoints = res.data[0].totalscore;
          //   //     if (this.counter && this.questions.length) {
          //   //       this.saveQuizAttempt();
          //   //     } else {
          //   //       this.loading = false;
          //   //     }
          //   //   },
          //   //     err => {
          //   //       this.loading = false;
          //   //     });
          // }

        }, {
          key: "ionViewWillLeave",
          value: function ionViewWillLeave() {
            if (this.alertObj) {
              this.alertObj.dismiss();
            }

            this.changeDetector.detectChanges();
          } // startTimer2() {
          //   this.timer = 3;
          //   this.interval = setInterval(() => {
          //     this.timer--;
          //     this.loaderObj.message = `Staring in ${this.timer} seconds`;
          //     if (this.timer === 0) {
          //       clearInterval(this.interval);
          //       this.hideLoader();
          //       this.fetchQuizQuestions();
          //     }
          //   }, 1000);
          // }
          // saveQuizAttempt() {
          //   const obj = {
          //     memberid: localStorage.getItem('user_id'),
          //     quizid: this.id,
          //     // totalpoints: this.totalpoints
          //   };
          //   this.quizService.saveQuizAttempt(obj)
          //     .then((res: any) => {
          //       this.loading = false;
          //       this.attemptid = res.attemptid;
          //       this.initializeForm();
          //       this.startCountDown();
          //       this.changeDetector.detectChanges();
          //     },
          //       err => {
          //         this.loading = false;
          //       });
          // }

        }, {
          key: "showLoader",
          value: function showLoader() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3);
            }));
          }
        }, {
          key: "hideLoader",
          value: function hideLoader() {// this.loaderObj.dismiss();
          }
        }, {
          key: "back",
          value: function back() {
            var check = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

            if (check) {
              // this.navCtrl.navigateBack(['home-results/quiz']);
              this.navCtrl.back();
            } else {
              this.backConfirm();
            }
          }
        }, {
          key: "backConfirm",
          value: function backConfirm() {
            var header = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Confirmation';
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this7 = this;

              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.alertController.create({
                        header: header,
                        message: "\n        <p> Are you sure you want to leave?</p>\n      ",
                        buttons: [{
                          text: 'No',
                          role: 'cancel'
                        }, {
                          text: 'Yes',
                          handler: function handler() {
                            _this7.back();
                          }
                        }]
                      });

                    case 2:
                      this.alertObj = _context4.sent;
                      this.alertObj.present();

                    case 4:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }]);

        return QuizPage;
      }();

      QuizPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: src_app_services_quiz_service__WEBPACK_IMPORTED_MODULE_7__["QuizService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"]
        }, {
          type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_9__["DataService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_6__["ChangeDetectorRef"]
        }];
      };

      QuizPage.propDecorators = {
        slides: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_6__["ViewChild"],
          args: ['slides']
        }]
      };
      QuizPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["Component"])({
        selector: 'app-quiz',
        template: _raw_loader_quiz_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_quiz_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], QuizPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-quiz-quiz-module-es5.js.map