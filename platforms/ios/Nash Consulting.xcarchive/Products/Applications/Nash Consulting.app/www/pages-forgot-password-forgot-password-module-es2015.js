(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-forgot-password-forgot-password-module"],{

/***/ "5dVO":
/*!********************************************!*\
  !*** ./src/app/services/loader.service.ts ***!
  \********************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let LoaderService = class LoaderService {
    constructor(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    showLoader() {
        this.isBusy = true;
        // this.loaderToShow = this.loadingCtrl.create({
        //   message: 'Please Wait..'
        // }).then((res) => {
        //   res.present();
        //   // res.onDidDismiss().then((dis) => {
        //   //    console.log('Loading dismissed!',dis);
        //   // });
        // });
        // // this.hideLoader();
    }
    hideLoader() {
        // setTimeout(()=>{
        //   this.loadingCtrl.dismiss();
        // },100)
        this.isBusy = false;
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ "7CEM":
/*!*****************************************************************!*\
  !*** ./src/app/pages/forgot-password/forgot-password.module.ts ***!
  \*****************************************************************/
/*! exports provided: ForgotPasswordPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPageModule", function() { return ForgotPasswordPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _forgot_password_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./forgot-password-routing.module */ "uf9z");
/* harmony import */ var _forgot_password_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./forgot-password.page */ "cZHL");







let ForgotPasswordPageModule = class ForgotPasswordPageModule {
};
ForgotPasswordPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _forgot_password_routing_module__WEBPACK_IMPORTED_MODULE_5__["ForgotPasswordPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_forgot_password_page__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordPage"]]
    })
], ForgotPasswordPageModule);



/***/ }),

/***/ "A5aJ":
/*!*****************************************************************!*\
  !*** ./src/app/pages/forgot-password/forgot-password.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".main-div {\n  position: relative;\n  display: flex;\n  flex-direction: column;\n}\n.main-div .outer-div {\n  width: 100%;\n  height: 20vh;\n  opacity: 0.3;\n}\n.close-fake {\n  --background: transparent;\n  margin-top: 20px;\n  /* Status bar height on iOS 10 */\n  margin-top: constant(safe-area-inset-top);\n  /* Status bar height on iOS 11.0 */\n  margin-top: env(safe-area-inset-top);\n}\n.close-fake ion-icon {\n  font-size: 22px;\n  color: #ffffff;\n}\n.background {\n  --background: #000 url(\"/assets/img/background.png\") 0 0/100% 25vh no-repeat ;\n}\n.content-div .error-div {\n  display: flex;\n  width: 100%;\n  padding-left: 10px;\n}\n.content-div .error-div p.text08 {\n  margin: 0;\n  margin-top: 2px;\n}\n.content-div h2 {\n  margin: 0;\n  font-size: 26px;\n  margin-top: 20px;\n  color: #fff;\n}\n.content-div .toggle {\n  margin: 10px;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  color: #fff;\n  --background: white !important;\n  justify-content: center;\n}\n.content-div .toggle ion-text {\n  margin-right: 5px;\n  font-weight: 600;\n}\n.content-div ion-toggle {\n  --background: white !important;\n}\n.content-div h4 {\n  margin: 0;\n  font-size: 16px;\n  font-weight: 600;\n  color: #fff;\n}\n.content-div h5 {\n  margin: 0;\n  font-size: 12px;\n  text-decoration: underline;\n  font-weight: 600;\n  color: #fff;\n}\n.content-div ion-button {\n  color: #fff;\n  font-size: 18px;\n  --background: #079199;\n  height: 45px;\n}\n.content-div ion-item {\n  border: 1px solid #0000006e;\n  border-radius: 5px;\n  margin-top: 25px;\n  border-radius: 10px;\n  background: white;\n}\n.content-div ion-item ion-input {\n  height: 100%;\n  margin-left: 5px;\n  padding: 10px !important;\n}\n.content-div ion-item ion-icon {\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2ZvcmdvdC1wYXNzd29yZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQUNKO0FBRUk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUVBLFlBQUE7QUFEUjtBQUlBO0VBQ0kseUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdDQUFBO0VBQ0EseUNBQUE7RUFDQSxrQ0FBQTtFQUNBLG9DQUFBO0FBREo7QUFHSTtFQUNFLGVBQUE7RUFDQSxjQUFBO0FBRE47QUFNQTtFQUNJLDZFQUFBO0FBSEo7QUFRSTtFQUNJLGFBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFMUjtBQU9RO0VBQ0ksU0FBQTtFQUNBLGVBQUE7QUFMWjtBQVNJO0VBQ0ksU0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUFQUjtBQVVJO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsdUJBQUE7QUFSUjtBQVVRO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtBQVJaO0FBWUk7RUFDSSw4QkFBQTtBQVZSO0FBYUk7RUFDSSxTQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQVhSO0FBY0k7RUFDSSxTQUFBO0VBQ0EsZUFBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0FBWlI7QUFlSTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0FBYlI7QUFnQkk7RUFDSSwyQkFBQTtFQUNBLGtCQUFBO0VBS0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FBbEJSO0FBb0JRO0VBQ0ksWUFBQTtFQUNBLGdCQUFBO0VBQ0Esd0JBQUE7QUFsQlo7QUFxQlE7RUFDSSxTQUFBO0FBbkJaIiwiZmlsZSI6ImZvcmdvdC1wYXNzd29yZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbi1kaXYge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgLy8gYWxpZ24taXRlbXM6IGNlbnRlcjtcblxuICAgIC5vdXRlci1kaXYge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAyMHZoO1xuICAgICAgICAvLyAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1nL2xvZ2luQkcuanBnKSBuby1yZXBlYXQgY2VudGVyIGNlbnRlciAvIGNvdmVyO1xuICAgICAgICBvcGFjaXR5OiAwLjM7XG4gICAgfVxufVxuLmNsb3NlLWZha2Uge1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICAvKiBTdGF0dXMgYmFyIGhlaWdodCBvbiBpT1MgMTAgKi9cbiAgICBtYXJnaW4tdG9wOiBjb25zdGFudChzYWZlLWFyZWEtaW5zZXQtdG9wKTtcbiAgICAvKiBTdGF0dXMgYmFyIGhlaWdodCBvbiBpT1MgMTEuMCAqL1xuICAgIG1hcmdpbi10b3A6IGVudihzYWZlLWFyZWEtaW5zZXQtdG9wKTtcbiAgXG4gICAgaW9uLWljb24ge1xuICAgICAgZm9udC1zaXplOiAyMnB4O1xuICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgfVxuICBcbiAgICAvLyBtYXJnaW4tdG9wOiA0MHB4O1xuICB9XG4uYmFja2dyb3VuZCB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAwIHVybChcIi9hc3NldHMvaW1nL2JhY2tncm91bmQucG5nXCIpIDAgMC8xMDAlIDI1dmggbm8tcmVwZWF0XG59XG5cbi5jb250ZW50LWRpdiB7XG5cbiAgICAuZXJyb3ItZGl2IHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcblxuICAgICAgICBwLnRleHQwOCB7XG4gICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAycHg7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBoMiB7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgZm9udC1zaXplOiAyNnB4O1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICB9XG5cbiAgICAudG9nZ2xlIHtcbiAgICAgICAgbWFyZ2luOiAxMHB4O1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgICAgICBpb24tdGV4dCB7XG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBpb24tdG9nZ2xlIHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIGg0IHtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgIH1cblxuICAgIGg1IHtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICB9XG5cbiAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjMDc5MTk5O1xuICAgICAgICBoZWlnaHQ6IDQ1cHg7XG4gICAgfVxuXG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMDAwMDAwNmU7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAgICAgLy8gICBoZWlnaHQ6IDYwcHg7XG4gICAgICAgIC8vICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgLy8gICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAvLyAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBtYXJnaW4tdG9wOiAyNXB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcblxuICAgICAgICBpb24taW5wdXQge1xuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHggIWltcG9ydGFudDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlvbi1pY29uIHtcbiAgICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgfVxuICAgIH1cbn0iXX0= */");

/***/ }),

/***/ "cZHL":
/*!***************************************************************!*\
  !*** ./src/app/pages/forgot-password/forgot-password.page.ts ***!
  \***************************************************************/
/*! exports provided: ForgotPasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPage", function() { return ForgotPasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_forgot_password_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./forgot-password.page.html */ "hnog");
/* harmony import */ var _forgot_password_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./forgot-password.page.scss */ "A5aJ");
/* harmony import */ var src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/alert.service */ "3LUQ");
/* harmony import */ var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/loader.service */ "5dVO");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var src_app_services_forgot_password_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/forgot-password.service */ "j8Zn");









let ForgotPasswordPage = class ForgotPasswordPage {
    constructor(formBuilder, NavController, loaderService, forgotPasswordService, AlertService) {
        this.formBuilder = formBuilder;
        this.NavController = NavController;
        this.loaderService = loaderService;
        this.forgotPasswordService = forgotPasswordService;
        this.AlertService = AlertService;
    }
    ngOnInit() {
        this.forgotPasswordForm = this.formBuilder.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
            toggle: ['false'],
        });
    }
    submit() {
        this.forgotpassword();
    }
    forgotpassword() {
        this.loaderService.showLoader();
        let formData = new FormData();
        formData.append("email", this.forgotPasswordForm.value['email']);
        this.forgotPasswordService.forgotpassword(formData).then((data) => {
            console.log("data : ", data);
            if (data.status) {
                this.loaderService.hideLoader();
                this.AlertService.showPwdSuccessAlert(this.forgotPasswordForm.value['email']);
                this.dismiss();
            }
            else {
                this.AlertService.presentAlertError(data.message);
                this.loaderService.hideLoader();
            }
        }, err => {
            this.loaderService.hideLoader();
        });
    }
    dismiss() {
        this.NavController.back();
    }
};
ForgotPasswordPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"] },
    { type: src_app_services_forgot_password_service__WEBPACK_IMPORTED_MODULE_8__["ForgotPasswordService"] },
    { type: src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_3__["AlertService"] }
];
ForgotPasswordPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["Component"])({
        selector: 'app-forgot-password',
        template: _raw_loader_forgot_password_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_forgot_password_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ForgotPasswordPage);



/***/ }),

/***/ "hnog":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgot-password/forgot-password.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"background\">\n  <form [formGroup]=\"forgotPasswordForm\" class=\"main-div\">\n    <ion-item class=\"close-fake\" lines=\"none\" text-center>\n      <ion-button (click)=\"dismiss()\" fill=\"clear\" color=\"light\">\n        <ion-icon name=\"arrow-back-outline\" slot=\"start\"></ion-icon>\n      </ion-button>\n    </ion-item>\n\n    <div class=\"outer-div\">\n    </div>\n    <div class=\"main-container\">\n      <div class=\"inner-div\">\n        <!-- <img src=\"assets/logo/logo.png\" /> -->\n      </div>\n      <div class=\"ion-margin-horizontal\">\n        <img src=\"assets/logo/logo.png\" />\n      </div>\n\n      <div class=\"content-div ion-text-center\">\n        <ion-item lines=\"none\" class=\"ion-margin-horizontal\">\n          <ion-input placeholder=\"Email\" type=\"email\" formControlName=\"email\"></ion-input>\n          <ion-icon name=\"mail-outline\" slot=\"start\"></ion-icon>\n        </ion-item>\n        <div class=\"error-div\">\n          <p ion-text class=\"text08\" *ngIf=\"forgotPasswordForm.get('email').touched && forgotPasswordForm.get('email').hasError('required')\">\n            <ion-text color=\"warning\">\n              Required Field\n            </ion-text>\n          </p>\n          <p ion-text class=\"text08\" *ngIf=\"forgotPasswordForm.get('email').touched && forgotPasswordForm.get('email').hasError('pattern')\">\n            <ion-text color=\"warning\">\n              Invalid Email\n            </ion-text>\n          </p>\n        </div>\n        <ion-button class=\"ion-margin-horizontal ion-text-uppercase ion-margin-vertical\"  expand=\"block\" (click)=\"submit()\" [disabled]=\"!forgotPasswordForm.valid\">Submit</ion-button>\n      </div>\n    </div>\n  </form>\n\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>");

/***/ }),

/***/ "j8Zn":
/*!*****************************************************!*\
  !*** ./src/app/services/forgot-password.service.ts ***!
  \*****************************************************/
/*! exports provided: ForgotPasswordService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordService", function() { return ForgotPasswordService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");




let ForgotPasswordService = class ForgotPasswordService {
    constructor(http) {
        this.http = http;
        this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    forgotpassword(formData) {
        return new Promise((resolve, reject) => {
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'forgotpassword.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
        });
    }
};
ForgotPasswordService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
ForgotPasswordService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], ForgotPasswordService);



/***/ }),

/***/ "uf9z":
/*!*************************************************************************!*\
  !*** ./src/app/pages/forgot-password/forgot-password-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: ForgotPasswordPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPageRoutingModule", function() { return ForgotPasswordPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _forgot_password_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./forgot-password.page */ "cZHL");




const routes = [
    {
        path: '',
        component: _forgot_password_page__WEBPACK_IMPORTED_MODULE_3__["ForgotPasswordPage"]
    }
];
let ForgotPasswordPageRoutingModule = class ForgotPasswordPageRoutingModule {
};
ForgotPasswordPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ForgotPasswordPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-forgot-password-forgot-password-module-es2015.js.map