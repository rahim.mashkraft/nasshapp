(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-edit-profile-edit-profile-module"], {
    /***/
    "02SC":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/edit-profile/edit-profile-routing.module.ts ***!
      \*******************************************************************/

    /*! exports provided: EditProfilePageRoutingModule */

    /***/
    function SC(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditProfilePageRoutingModule", function () {
        return EditProfilePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _edit_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./edit-profile.page */
      "kmVb");

      var routes = [{
        path: '',
        component: _edit_profile_page__WEBPACK_IMPORTED_MODULE_3__["EditProfilePage"]
      }];

      var EditProfilePageRoutingModule = function EditProfilePageRoutingModule() {
        _classCallCheck(this, EditProfilePageRoutingModule);
      };

      EditProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], EditProfilePageRoutingModule);
      /***/
    },

    /***/
    "2g2N":
    /*!*******************************************!*\
      !*** ./src/app/services/toast.service.ts ***!
      \*******************************************/

    /*! exports provided: ToastService */

    /***/
    function g2N(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ToastService", function () {
        return ToastService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var ToastService = /*#__PURE__*/function () {
        function ToastService(toastCtrl) {
          _classCallCheck(this, ToastService);

          this.toastCtrl = toastCtrl;
        }

        _createClass(ToastService, [{
          key: "showToast",
          value: function showToast() {
            var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Successfully Logged in!';
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastCtrl.create({
                        cssClass: 'bg-toast',
                        message: message,
                        duration: 3000,
                        position: 'bottom'
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "showToastUpdateProfile",
          value: function showToastUpdateProfile(message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var toast;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.toastCtrl.create({
                        cssClass: 'bg-toast',
                        message: message,
                        duration: 3000,
                        position: 'bottom'
                      });

                    case 2:
                      toast = _context2.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return ToastService;
      }();

      ToastService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      ToastService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ToastService);
      /***/
    },

    /***/
    "5dVO":
    /*!********************************************!*\
      !*** ./src/app/services/loader.service.ts ***!
      \********************************************/

    /*! exports provided: LoaderService */

    /***/
    function dVO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoaderService", function () {
        return LoaderService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var LoaderService = /*#__PURE__*/function () {
        function LoaderService(loadingCtrl) {
          _classCallCheck(this, LoaderService);

          this.loadingCtrl = loadingCtrl;
        }

        _createClass(LoaderService, [{
          key: "showLoader",
          value: function showLoader() {
            this.isBusy = true; // this.loaderToShow = this.loadingCtrl.create({
            //   message: 'Please Wait..'
            // }).then((res) => {
            //   res.present();
            //   // res.onDidDismiss().then((dis) => {
            //   //    console.log('Loading dismissed!',dis);
            //   // });
            // });
            // // this.hideLoader();
          }
        }, {
          key: "hideLoader",
          value: function hideLoader() {
            // setTimeout(()=>{
            //   this.loadingCtrl.dismiss();
            // },100)
            this.isBusy = false;
          }
        }]);

        return LoaderService;
      }();

      LoaderService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoaderService);
      /***/
    },

    /***/
    "S5E+":
    /*!*************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/edit-profile/edit-profile.page.html ***!
      \*************************************************************************************************/

    /*! exports provided: default */

    /***/
    function S5E(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">Edit Profile</ion-title>\n    <ion-icon slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" routerLink=\"/tabs/tab1\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"profile\">\n  <ion-card no-margin>\n    <ion-card-content class=\"bg-profile\">\n      <img [src]=\"environment.profileImageUrl+userData.profileimage\">\n      <h1 class=\"fw500\">{{userData.firstname}} {{userData.lastname}}</h1>\n      <ion-button icon-left color=\"secondary\" (click)='uploadPhoto()'>\n        <ion-icon name=\"photos\"></ion-icon>\n        Edit / Add Avatar\n      </ion-button>\n    </ion-card-content>\n    <ion-grid fixed no-padding>\n      <ion-row>\n        <ion-col size=\"12\" padding>\n\n          <ion-list margin-bottom>\n            <form [formGroup]=\"onEditProfile\" class=\"form\">\n              <ion-list-header color=\"primary\">\n                <ion-label class=\"fw700\">Form</ion-label>\n              </ion-list-header>\n\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">First Name:</ion-label>\n                <ion-input inputmode=\"text\" placeholder=\"First Name\" formControlName=\"firstname\"\n                  value=\"{{userData.firstname}}\"></ion-input>\n                <p ion-text class=\"text08\"\n                  *ngIf=\"onEditProfile.get('firstname').touched && onEditProfile.get('firstname').hasError('required')\">\n                  <ion-text color=\"warning\">\n                    Required Field\n                  </ion-text>\n                </p>\n              </ion-item>\n\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">Last Name:</ion-label>\n                <ion-input inputmode=\"text\" placeholder=\"Last Name\" formControlName=\"lastname\"\n                  value=\"{{userData.lastname}}\"></ion-input>\n                <p ion-text class=\"text08\"\n                  *ngIf=\"onEditProfile.get('lastname').touched && onEditProfile.get('lastname').hasError('required')\">\n                  <ion-text color=\"warning\">\n                    Required Field\n                  </ion-text>\n                </p>\n              </ion-item>\n\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">Phone:</ion-label>\n                <ion-input inputmode=\"text\" placeholder=\"Phone\" value=\"{{userData.phone}}\" formControlName=\"phone\"\n                  type=\"number\"></ion-input>\n                <p ion-text class=\"text08\"\n                  *ngIf=\"onEditProfile.get('phone').touched && onEditProfile.get('phone').hasError('required')\">\n                  <ion-text color=\"warning\">\n                    Required Field\n                  </ion-text>\n                </p>\n              </ion-item>\n\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">Address:</ion-label>\n                <ion-input inputmode=\"text\" placeholder=\"Address\" formControlName=\"address\"\n                  value=\"{{userData.address}}\"></ion-input>\n              </ion-item>\n\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">City:</ion-label>\n                <ion-input inputmode=\"text\" placeholder=\"City\" value=\"{{userData.city}}\" formControlName=\"city\"\n                  type=\"text\">\n                </ion-input>\n                <p ion-text class=\"text08\"\n                  *ngIf=\"onEditProfile.get('city').touched && onEditProfile.get('city').hasError('required')\">\n                  <ion-text color=\"warning\">\n                    Required Field\n                  </ion-text>\n                </p>\n              </ion-item>\n\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">State:</ion-label>\n                <ion-input inputmode=\"text\" placeholder=\"State\" formControlName=\"state\" type=\"text\"></ion-input>\n                <p ion-text class=\"text08\"\n                  *ngIf=\"onEditProfile.get('state').touched && onEditProfile.get('state').hasError('required')\">\n                  <ion-text color=\"warning\">\n                    Required Field\n                  </ion-text>\n                </p>\n              </ion-item>\n\n\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">Country:</ion-label>\n                <ion-input inputmode=\"text\" placeholder=\"Country\" value=\"{{userData.country}}\" formControlName=\"country\"\n                  type=\"text\"></ion-input>\n                <p ion-text class=\"text08\"\n                  *ngIf=\"onEditProfile.get('country').touched && onEditProfile.get('country').hasError('required')\">\n                  <ion-text color=\"warning\">\n                    Required Field\n                  </ion-text>\n                </p>\n              </ion-item>\n\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">Zipcode:</ion-label>\n                <ion-input inputmode=\"text\" placeholder=\"Zipcode\" value=\"{{userData.zipcode}}\" formControlName=\"zipcode\"\n                  type=\"text\"></ion-input>\n                <p ion-text class=\"text08\"\n                  *ngIf=\"onEditProfile.get('zipcode').touched && onEditProfile.get('zipcode').hasError('required')\">\n                  <ion-text color=\"warning\">\n                    Required Field\n                  </ion-text>\n                </p>\n              </ion-item>\n\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">Email:</ion-label>\n                <ion-input inputmode=\"email\" placeholder=\"Email\" value=\"{{userData.email}}\" disabled></ion-input>\n              </ion-item>\n\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">Username:</ion-label>\n                <ion-input inputmode=\"username\" placeholder=\"Username\" value=\"{{userData.username}}\" disabled>\n                </ion-input>\n              </ion-item>\n\n            </form>\n          </ion-list>\n          <div class=\"btn-div-update\">\n            <ion-button size=\"medium\" shape=\"round\" color=\"dark\" [disabled]=\"!onEditProfile.valid\"\n              (click)=\"updateProfile()\">\n              Update</ion-button>\n          </div>\n          <ion-list class=\"margin-top\">\n            <form [formGroup]=\"onChangePassword\" class=\"form\">\n              <ion-list-header color=\"primary\">\n                <ion-label class=\"fw700\">Change Your Password</ion-label>\n              </ion-list-header>\n\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">New Password:</ion-label>\n                <ion-input type=\"{{passwordType}}\" formControlName=\"password\"></ion-input>\n                <!-- <ion-icon slot=\"end\" color=\"dark\" name=\"eye\" (click)=\"showPassword(0)\" *ngIf=\"!eye\"></ion-icon>\n                <ion-icon slot=\"end\" color=\"dark\" name=\"eye-off\" (click)=\"hidePassword(0)\" *ngIf=\"eye\"></ion-icon> -->\n                <p ion-text class=\"text08\"\n                  *ngIf=\"onChangePassword.get('password').touched && onChangePassword.get('password').hasError('required')\">\n                  <ion-text color=\"warning\">\n                    Required Field\n                  </ion-text>\n                </p>\n                <p ion-text class=\"text08\"\n                  *ngIf=\"onChangePassword.get('password').touched && onChangePassword.get('password').hasError( 'minlength')\">\n                  <ion-text color=\"warning\">\n                    Password must be at least 6 characters\n                  </ion-text>\n                </p>\n              </ion-item>\n              <ion-item>\n                <ion-label color=\"primary\" position=\"stacked\">Confirm Password:</ion-label>\n                <ion-input type=\"{{c_passwordType}}\" formControlName=\"confirm_password\"></ion-input>\n                <!-- <ion-icon slot=\"end\" color=\"dark\" name=\"eye\" (click)=\"showPassword(1)\" *ngIf=\"!c_eye\"></ion-icon>\n                <ion-icon slot=\"end\" color=\"dark\" name=\"eye-off\" (click)=\"hidePassword(1)\" *ngIf=\"c_eye\"></ion-icon> -->\n                <p ion-text class=\"text08\"\n                  *ngIf=\"onChangePassword.get('confirm_password').touched && onChangePassword.get('confirm_password').hasError('required')\">\n                  <ion-text color=\"warning\">\n                    Required Field\n                  </ion-text>\n                </p>\n                <p ion-text class=\"text08\"\n                  *ngIf=\"onChangePassword.get('confirm_password').touched && onChangePassword.get('confirm_password').hasError( 'mustMatch')\">\n                  <ion-text color=\"warning\">\n                    Passwords Must Match\n                  </ion-text>\n                </p>\n\n              </ion-item>\n            </form>\n          </ion-list>\n          <ion-button size=\"medium\" shape=\"round\" color=\"dark\" (click)=\"updatePassword()\"\n            [disabled]=\"!onChangePassword.valid\">Save</ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>";
      /***/
    },

    /***/
    "ULdC":
    /*!****************************************************!*\
      !*** ./src/app/services/update-profile.service.ts ***!
      \****************************************************/

    /*! exports provided: UpdateProfileService */

    /***/
    function ULdC(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UpdateProfileService", function () {
        return UpdateProfileService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");
      /* harmony import */


      var _network_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./network.service */
      "dwY0");

      var UpdateProfileService = /*#__PURE__*/function () {
        function UpdateProfileService(http, networkService) {
          _classCallCheck(this, UpdateProfileService);

          this.http = http;
          this.networkService = networkService;
          this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
        }

        _createClass(UpdateProfileService, [{
          key: "updateprofile",
          value: function updateprofile(formData) {
            var _this = this;

            return new Promise(function (resolve, reject) {
              _this.networkService.onNetworkChange().subscribe(function (status) {
                if (status == _network_service__WEBPACK_IMPORTED_MODULE_4__["ConnectionStatus"].Online) {
                  console.log(_this.apiUrl + 'updateprofile.php');
                  formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

                  _this.http.post(_this.apiUrl + 'updateprofile.php', formData).subscribe(function (res) {
                    resolve(res);
                  }, function (err) {
                    reject(err);
                    console.log('something went wrong please try again');
                  });
                }
              });
            });
          }
        }]);

        return UpdateProfileService;
      }();

      UpdateProfileService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }, {
          type: _network_service__WEBPACK_IMPORTED_MODULE_4__["NetworkService"]
        }];
      };

      UpdateProfileService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], UpdateProfileService);
      /***/
    },

    /***/
    "k578":
    /*!***********************************************************!*\
      !*** ./src/app/pages/edit-profile/edit-profile.module.ts ***!
      \***********************************************************/

    /*! exports provided: EditProfilePageModule */

    /***/
    function k578(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditProfilePageModule", function () {
        return EditProfilePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./edit-profile-routing.module */
      "02SC");
      /* harmony import */


      var _edit_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./edit-profile.page */
      "kmVb");

      var EditProfilePageModule = function EditProfilePageModule() {
        _classCallCheck(this, EditProfilePageModule);
      };

      EditProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditProfilePageRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]],
        declarations: [_edit_profile_page__WEBPACK_IMPORTED_MODULE_6__["EditProfilePage"]]
      })], EditProfilePageModule);
      /***/
    },

    /***/
    "kmVb":
    /*!*********************************************************!*\
      !*** ./src/app/pages/edit-profile/edit-profile.page.ts ***!
      \*********************************************************/

    /*! exports provided: EditProfilePage */

    /***/
    function kmVb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditProfilePage", function () {
        return EditProfilePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_edit_profile_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./edit-profile.page.html */
      "S5E+");
      /* harmony import */


      var _edit_profile_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./edit-profile.page.scss */
      "tGd5");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_services_events_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/events.service */
      "riPR");
      /* harmony import */


      var src_app_services_image_uploading_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/image-uploading.service */
      "TX3h");
      /* harmony import */


      var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/services/loader.service */
      "5dVO");
      /* harmony import */


      var src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! src/app/services/toast.service */
      "2g2N");
      /* harmony import */


      var src_app_services_update_profile_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! src/app/services/update-profile.service */
      "ULdC");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");
      /* harmony import */


      var _helpers_must_match_validator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ../_helpers/must-match.validator */
      "4B9S");

      var EditProfilePage = /*#__PURE__*/function () {
        function EditProfilePage(navCtrl, imageUploadingService, formBuilder, updateProfileService, toastService, eventsService, loaderService) {
          var _this2 = this;

          _classCallCheck(this, EditProfilePage);

          this.navCtrl = navCtrl;
          this.imageUploadingService = imageUploadingService;
          this.formBuilder = formBuilder;
          this.updateProfileService = updateProfileService;
          this.toastService = toastService;
          this.eventsService = eventsService;
          this.loaderService = loaderService;
          this.userData = {
            aid: '',
            firstname: '',
            lastname: '',
            email: '',
            profileimage: '',
            address: '',
            city: '',
            country: '',
            phone: '',
            username: '',
            zipcode: '',
            state: ''
          };
          this.eye = false;
          this.passwordType = 'password';
          this.c_eye = false;
          this.c_passwordType = 'password';
          this.environment = src_environments_environment__WEBPACK_IMPORTED_MODULE_11__["environment"];
          this.eventsService.subscribe('pictureChanged', function (data) {
            _this2.userData.profileimage = data.profileimage; // this.profileImage = environment.profileImageUrl+this.userData.profileimage
            // console.log("this.profileImage 2 : ",this.profileImage)
            // setTimeout(() => {
            //   this.cdr.detectChanges()
            // }, 1000);
          });
        }

        _createClass(EditProfilePage, [{
          key: "showPassword",
          value: function showPassword(value) {
            if (value == 0) {
              this.eye = true;
              this.passwordType = 'text';
            } else {
              this.c_eye = true;
              this.c_passwordType = 'text';
            }
          }
        }, {
          key: "hidePassword",
          value: function hidePassword(value) {
            if (value == 0) {
              this.eye = false;
              this.passwordType = 'password';
            } else {
              this.c_eye = false;
              this.c_passwordType = 'password';
            }
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.userData.aid = localStorage.getItem('user_id');
            this.userData.firstname = localStorage.getItem('firstname');
            this.userData.lastname = localStorage.getItem('lastname');
            this.userData.email = localStorage.getItem('email');
            this.userData.profileimage = localStorage.getItem('profileimage');
            this.userData.address = localStorage.getItem('address');
            this.userData.city = localStorage.getItem('city');
            this.userData.country = localStorage.getItem('country');
            this.userData.phone = localStorage.getItem('phone');
            this.userData.username = localStorage.getItem('username');
            this.userData.zipcode = localStorage.getItem('zipcode');
            this.userData.state = localStorage.getItem('state');
            console.log(this.userData);
            this.onEditProfile = this.formBuilder.group({
              firstname: [this.userData.firstname, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])],
              lastname: [this.userData.lastname, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])],
              city: [this.userData.city, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])],
              country: [this.userData.country, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])],
              address: [this.userData.address, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([])],
              phone: [this.userData.phone, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([])],
              zipcode: [this.userData.zipcode, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([])],
              state: [this.userData.state, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([])]
            }, {// validator : nonZero ('age' )
            }), this.onChangePassword = this.formBuilder.group({
              password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(6)])],
              confirm_password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])]
            }, {
              validator: Object(_helpers_must_match_validator__WEBPACK_IMPORTED_MODULE_12__["mustMatch"])('password', 'confirm_password')
            });
          }
        }, {
          key: "updateProfile",
          value: function updateProfile() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this3 = this;

              var formData;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      this.loaderService.showLoader();
                      formData = new FormData();
                      formData.append("aid", this.userData.aid);
                      formData.append("firstname", this.onEditProfile.value['firstname']);
                      formData.append("lastname", this.onEditProfile.value['lastname']);
                      formData.append("city", this.onEditProfile.value['city']);
                      formData.append("country", this.onEditProfile.value['country']);
                      formData.append("phone", this.onEditProfile.value['phone']);
                      formData.append("address", this.onEditProfile.value['address']);
                      formData.append("zipcode", this.onEditProfile.value['zipcode']);
                      formData.append("state", this.onEditProfile.value['state']);
                      this.updateProfileService.updateprofile(formData).then(function (res) {
                        // console.log(res);
                        if (res.status !== false) {
                          localStorage.setItem('firstname', res.firstname);
                          localStorage.setItem('lastname', res.lastname);
                          localStorage.setItem('address', res.address);
                          localStorage.setItem('city', res.city);
                          localStorage.setItem('country', res.country);
                          localStorage.setItem('phone', res.phone);
                          localStorage.setItem('zipcode', res.zipcode);
                          localStorage.setItem('state', res.state);
                          var userData = {
                            firstname: res.firstname,
                            lastname: res.lastname
                          };

                          _this3.eventsService.publish('nameChanged', userData);

                          _this3.loaderService.hideLoader();

                          _this3.toastService.showToastUpdateProfile('Profile Update Successfully');
                        } else {
                          _this3.loaderService.hideLoader();
                        }
                      });

                    case 12:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "updatePassword",
          value: function updatePassword() {// this.userService.updatePassword(data)
            //   .subscribe((res: any) => {
            //     // console.log(res);
            //     if (res.status !== false) {
            //       this.onChangePassword.reset('password');
            //       this.toastService.showToastUpdateProfile('Password Update Successfully');
            //     }
            //   });
          }
        }, {
          key: "back",
          value: function back() {
            this.navCtrl.back();
          }
        }, {
          key: "uploadPhoto",
          value: function uploadPhoto() {
            this.imageUploadingService.uploadPhoto();
          }
        }]);

        return EditProfilePage;
      }();

      EditProfilePage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: src_app_services_image_uploading_service__WEBPACK_IMPORTED_MODULE_7__["ImageUploadingService"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]
        }, {
          type: src_app_services_update_profile_service__WEBPACK_IMPORTED_MODULE_10__["UpdateProfileService"]
        }, {
          type: src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_9__["ToastService"]
        }, {
          type: src_app_services_events_service__WEBPACK_IMPORTED_MODULE_6__["EventsService"]
        }, {
          type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_8__["LoaderService"]
        }];
      };

      EditProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-edit-profile',
        template: _raw_loader_edit_profile_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_edit_profile_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], EditProfilePage);
      /***/
    },

    /***/
    "tGd5":
    /*!***********************************************************!*\
      !*** ./src/app/pages/edit-profile/edit-profile.page.scss ***!
      \***********************************************************/

    /*! exports provided: default */

    /***/
    function tGd5(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".profile ion-card {\n  border-radius: 0;\n  background-color: #fff;\n}\n.profile ion-card ion-card-content {\n  padding: 32px;\n  background-color: var(--ion-color-primary);\n  color: #fff;\n  text-align: center;\n}\n.profile ion-card ion-card-content img {\n  height: 128px;\n  width: 128px;\n  border-radius: 50%;\n  border: solid 4px #fff;\n  display: inline;\n  box-shadow: 0 0 28px rgba(255, 255, 255, 0.65);\n}\n.profile ion-card ion-card-content h1 {\n  margin-top: 0.5rem;\n}\n.profile ion-item ion-input {\n  border-bottom: 1px solid var(--ion-color-tertiary);\n}\n.profile ion-buttom button {\n  margin: 0;\n}\n.profile .warning {\n  color: var(--ion-color-secondary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2VkaXQtcHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFFRSxnQkFBQTtFQUNBLHNCQUFBO0FBRE47QUFFTTtFQUNFLGFBQUE7RUFDQSwwQ0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQUFSO0FBRVE7RUFDRSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsOENBQUE7QUFBVjtBQUdRO0VBQ0Usa0JBQUE7QUFEVjtBQU9NO0VBQ0Usa0RBQUE7QUFMUjtBQVVNO0VBQ0UsU0FBQTtBQVJSO0FBV0k7RUFDRSxpQ0FBQTtBQVROIiwiZmlsZSI6ImVkaXQtcHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucHJvZmlsZSB7XG4gICAgaW9uLWNhcmQge1xuICAgIC8vICAgd2lkdGg6IDEwMCU7XG4gICAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICAgIGlvbi1jYXJkLWNvbnRlbnQge1xuICAgICAgICBwYWRkaW5nOiAzMnB4O1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIFxuICAgICAgICBpbWcge1xuICAgICAgICAgIGhlaWdodDogMTI4cHg7XG4gICAgICAgICAgd2lkdGg6IDEyOHB4O1xuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICBib3JkZXI6IHNvbGlkIDRweCAjZmZmO1xuICAgICAgICAgIGRpc3BsYXk6IGlubGluZTtcbiAgICAgICAgICBib3gtc2hhZG93OiAwIDAgMjhweCByZ2JhKDI1NSwyNTUsMjU1LCAuNjUpO1xuICAgICAgICB9XG4gIFxuICAgICAgICBoMSB7XG4gICAgICAgICAgbWFyZ2luLXRvcDogLjVyZW07XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIFxuICAgIGlvbi1pdGVtIHtcbiAgICAgIGlvbi1pbnB1dCB7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItdGVydGlhcnkpO1xuICAgICAgfVxuICAgIH1cbiAgXG4gICAgaW9uLWJ1dHRvbSB7XG4gICAgICBidXR0b24ge1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICB9XG4gICAgfVxuICAgIC53YXJuaW5ne1xuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpXG4gICAgfVxuICB9Il19 */";
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-edit-profile-edit-profile-module-es5.js.map