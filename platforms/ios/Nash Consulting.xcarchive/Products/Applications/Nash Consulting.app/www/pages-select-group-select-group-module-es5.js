(function () {
  function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-select-group-select-group-module"], {
    /***/
    "2g2N":
    /*!*******************************************!*\
      !*** ./src/app/services/toast.service.ts ***!
      \*******************************************/

    /*! exports provided: ToastService */

    /***/
    function g2N(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ToastService", function () {
        return ToastService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var ToastService = /*#__PURE__*/function () {
        function ToastService(toastCtrl) {
          _classCallCheck(this, ToastService);

          this.toastCtrl = toastCtrl;
        }

        _createClass(ToastService, [{
          key: "showToast",
          value: function showToast() {
            var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Successfully Logged in!';
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastCtrl.create({
                        cssClass: 'bg-toast',
                        message: message,
                        duration: 3000,
                        position: 'bottom'
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "showToastUpdateProfile",
          value: function showToastUpdateProfile(message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var toast;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.toastCtrl.create({
                        cssClass: 'bg-toast',
                        message: message,
                        duration: 3000,
                        position: 'bottom'
                      });

                    case 2:
                      toast = _context2.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return ToastService;
      }();

      ToastService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      ToastService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ToastService);
      /***/
    },

    /***/
    "5Rhe":
    /*!***********************************************************!*\
      !*** ./src/app/pages/select-group/select-group.module.ts ***!
      \***********************************************************/

    /*! exports provided: SelectGroupPageModule */

    /***/
    function Rhe(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SelectGroupPageModule", function () {
        return SelectGroupPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _select_group_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./select-group-routing.module */
      "Uc+a");
      /* harmony import */


      var _select_group_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./select-group.page */
      "KOIj");

      var SelectGroupPageModule = function SelectGroupPageModule() {
        _classCallCheck(this, SelectGroupPageModule);
      };

      SelectGroupPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _select_group_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectGroupPageRoutingModule"]],
        declarations: [_select_group_page__WEBPACK_IMPORTED_MODULE_6__["SelectGroupPage"]]
      })], SelectGroupPageModule);
      /***/
    },

    /***/
    "5dVO":
    /*!********************************************!*\
      !*** ./src/app/services/loader.service.ts ***!
      \********************************************/

    /*! exports provided: LoaderService */

    /***/
    function dVO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoaderService", function () {
        return LoaderService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var LoaderService = /*#__PURE__*/function () {
        function LoaderService(loadingCtrl) {
          _classCallCheck(this, LoaderService);

          this.loadingCtrl = loadingCtrl;
        }

        _createClass(LoaderService, [{
          key: "showLoader",
          value: function showLoader() {
            this.isBusy = true; // this.loaderToShow = this.loadingCtrl.create({
            //   message: 'Please Wait..'
            // }).then((res) => {
            //   res.present();
            //   // res.onDidDismiss().then((dis) => {
            //   //    console.log('Loading dismissed!',dis);
            //   // });
            // });
            // // this.hideLoader();
          }
        }, {
          key: "hideLoader",
          value: function hideLoader() {
            // setTimeout(()=>{
            //   this.loadingCtrl.dismiss();
            // },100)
            this.isBusy = false;
          }
        }]);

        return LoaderService;
      }();

      LoaderService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoaderService);
      /***/
    },

    /***/
    "KOIj":
    /*!*********************************************************!*\
      !*** ./src/app/pages/select-group/select-group.page.ts ***!
      \*********************************************************/

    /*! exports provided: SelectGroupPage */

    /***/
    function KOIj(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SelectGroupPage", function () {
        return SelectGroupPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_select_group_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./select-group.page.html */
      "rGps");
      /* harmony import */


      var _select_group_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./select-group.page.scss */
      "OdVg");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/contact-groups.service */
      "rtEV");
      /* harmony import */


      var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/loader.service */
      "5dVO");
      /* harmony import */


      var src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/services/toast.service */
      "2g2N");

      var SelectGroupPage = /*#__PURE__*/function () {
        function SelectGroupPage(router, route, navCtrl, contactGroupService, loaderService, toastService) {
          var _this = this;

          _classCallCheck(this, SelectGroupPage);

          this.router = router;
          this.route = route;
          this.navCtrl = navCtrl;
          this.contactGroupService = contactGroupService;
          this.loaderService = loaderService;
          this.toastService = toastService;
          this.isChecked = false;
          this.postData = [];
          this.radio_check = 1;
          this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
              _this.checkedItems = _this.router.getCurrentNavigation().extras.state.checkedItems;
              console.log("this.checkedItems : ", _this.checkedItems);

              _this.checkedItems.forEach(function (obj) {
                console.log(obj);
              });

              for (var i = 0; i < _this.checkedItems.length; i++) {
                var obj = void 0;
                obj = {
                  firstname: '',
                  lastname: '',
                  email: '',
                  phone: 0
                };
                obj.firstname = _this.checkedItems[i]._objectInstance.name.givenName;
                obj.lastname = _this.checkedItems[i]._objectInstance.name.familyName;

                if (_this.checkedItems[i]._objectInstance.emails) {
                  obj.email = _this.checkedItems[i]._objectInstance.emails[0].value;
                }

                if (_this.checkedItems[i]._objectInstance.ims) {
                  obj.email = _this.checkedItems[i]._objectInstance.ims[0].value;
                }

                if (_this.checkedItems[i]._objectInstance.phoneNumbers) {
                  obj.phone = _this.checkedItems[i]._objectInstance.phoneNumbers[0].value;
                }

                _this.postData.push(obj);
              }

              console.log("this.postData : ", _this.postData);
            }
          });
        }

        _createClass(SelectGroupPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getContactGroups();
          }
        }, {
          key: "getContactGroups",
          value: function getContactGroups() {
            var _this2 = this;

            this.contactGroupService.getContactGroups().then(function (res) {
              console.log(res);

              if (res.status !== false) {
                _this2.contact_groups = res.contactgroup;
              }
            });
          }
        }, {
          key: "submit",
          value: function submit() {
            var _this3 = this;

            this.loaderService.showLoader();
            var jj = 0;
            var formData = new FormData();
            console.log("this.radio_check : ", this.radio_check);

            var _iterator = _createForOfIteratorHelper(this.postData),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var data = _step.value;
                this.responseData = data;
                console.log(this.responseData);

                if (this.responseData.firstname == undefined) {
                  formData.append("postdata[" + jj + "][firstname]", null);
                } else {
                  formData.append("postdata[" + jj + "][firstname]", this.responseData.firstname);
                }

                if (this.responseData.lastname == undefined) {
                  formData.append("postdata[" + jj + "][lastname]", null);
                } else {
                  formData.append("postdata[" + jj + "][lastname]", this.responseData.lastname);
                }

                if (this.responseData.email == undefined) {
                  formData.append("postdata[" + jj + "][email]", null);
                } else {
                  formData.append("postdata[" + jj + "][email]", this.responseData.email);
                }

                formData.append("postdata[" + jj + "][phone]", this.responseData.phone);
                jj++;
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }

            formData.append("aid", localStorage.getItem('user_id'));
            formData.append("group_id", this.radio_check);
            this.contactGroupService.contactsImport(formData).then(function (res) {
              console.log(res);

              if (res.status !== false) {
                _this3.toastService.showToast('Contacts successfully imported form mobile to your account.');

                _this3.loaderService.hideLoader(); // this.back();


                setTimeout(function () {
                  _this3.navCtrl.navigateRoot('/tabs');
                }, 500); // this.contact_groups = res.contactgroup
              } else {
                _this3.loaderService.hideLoader();
              }
            });
          }
        }, {
          key: "back",
          value: function back() {
            this.navCtrl.back();
          }
        }]);

        return SelectGroupPage;
      }();

      SelectGroupPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_6__["ContactGroupsService"]
        }, {
          type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_7__["LoaderService"]
        }, {
          type: src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"]
        }];
      };

      SelectGroupPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-group',
        template: _raw_loader_select_group_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_select_group_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], SelectGroupPage);
      /***/
    },

    /***/
    "OdVg":
    /*!***********************************************************!*\
      !*** ./src/app/pages/select-group/select-group.page.scss ***!
      \***********************************************************/

    /*! exports provided: default */

    /***/
    function OdVg(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzZWxlY3QtZ3JvdXAucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "Uc+a":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/select-group/select-group-routing.module.ts ***!
      \*******************************************************************/

    /*! exports provided: SelectGroupPageRoutingModule */

    /***/
    function UcA(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SelectGroupPageRoutingModule", function () {
        return SelectGroupPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _select_group_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./select-group.page */
      "KOIj");

      var routes = [{
        path: '',
        component: _select_group_page__WEBPACK_IMPORTED_MODULE_3__["SelectGroupPage"]
      }];

      var SelectGroupPageRoutingModule = function SelectGroupPageRoutingModule() {
        _classCallCheck(this, SelectGroupPageRoutingModule);
      };

      SelectGroupPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SelectGroupPageRoutingModule);
      /***/
    },

    /***/
    "rGps":
    /*!*************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/select-group/select-group.page.html ***!
      \*************************************************************************************************/

    /*! exports provided: default */

    /***/
    function rGps(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">Select Group</ion-title>\n    <ion-icon slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" routerLink=\"/tabs/tab1\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list mode=\"md\">\n    <ion-radio-group value=\"biff\" [(ngModel)]=\"radio_check\">\n  \n      <ion-item *ngFor=\"let item of contact_groups\">\n        <ion-label>{{item.title}}</ion-label>\n        <ion-radio slot=\"start\" value=\"{{item.id}}\"></ion-radio>\n      </ion-item>\n  \n    </ion-radio-group>\n  </ion-list>\n</ion-content>\n<ion-footer>\n  <ion-row padding-vertical>\n    <ion-col margin-left margin-right no-padding>\n      <ion-button expand=\"full\" (click)=\"submit()\">Submit</ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>";
      /***/
    },

    /***/
    "rtEV":
    /*!****************************************************!*\
      !*** ./src/app/services/contact-groups.service.ts ***!
      \****************************************************/

    /*! exports provided: ContactGroupsService */

    /***/
    function rtEV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactGroupsService", function () {
        return ContactGroupsService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");

      var ContactGroupsService = /*#__PURE__*/function () {
        function ContactGroupsService(http) {
          _classCallCheck(this, ContactGroupsService);

          this.http = http;
          this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
        }

        _createClass(ContactGroupsService, [{
          key: "getContactGroups",
          value: function getContactGroups() {
            var _this4 = this;

            return new Promise(function (resolve, reject) {
              // if (status == ConnectionStatus.Online) {
              console.log(_this4.apiUrl + 'contactgroups.php');

              _this4.http.get(_this4.apiUrl + 'contactgroups.php').subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              }); // }

            });
          }
        }, {
          key: "getContactGroupsList",
          value: function getContactGroupsList(formData) {
            var _this5 = this;

            return new Promise(function (resolve, reject) {
              // if (status == ConnectionStatus.Online) {
              console.log(_this5.apiUrl + 'contactgroupslist.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this5.http.post(_this5.apiUrl + 'contactgroupslist.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              }); // }

            });
          }
        }, {
          key: "contactsImport",
          value: function contactsImport(formData) {
            var _this6 = this;

            return new Promise(function (resolve, reject) {
              // if (status == ConnectionStatus.Online) {
              console.log(_this6.apiUrl + 'contactsimport.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this6.http.post(_this6.apiUrl + 'contactsimport.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              }); // }

            });
          }
        }, {
          key: "getContacts",
          value: function getContacts(formData) {
            var _this7 = this;

            return new Promise(function (resolve, reject) {
              // if (status == ConnectionStatus.Online) {
              console.log(_this7.apiUrl + 'contacts.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this7.http.post(_this7.apiUrl + 'contacts.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              }); // }

            });
          }
        }]);

        return ContactGroupsService;
      }();

      ContactGroupsService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      };

      ContactGroupsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], ContactGroupsService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-select-group-select-group-module-es5.js.map