(function () {
  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sub-categories-sub-categories-module"], {
    /***/
    "0i2D":
    /*!**************************************************************!*\
      !*** ./src/app/tab1/sub-categories/sub-categories.module.ts ***!
      \**************************************************************/

    /*! exports provided: SubCategoriesPageModule */

    /***/
    function i2D(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SubCategoriesPageModule", function () {
        return SubCategoriesPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _sub_categories_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./sub-categories-routing.module */
      "oaDq");
      /* harmony import */


      var _sub_categories_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./sub-categories.page */
      "6uY0");
      /* harmony import */


      var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/modules/shared/shared.module */
      "FpXt");

      var SubCategoriesPageModule = function SubCategoriesPageModule() {
        _classCallCheck(this, SubCategoriesPageModule);
      };

      SubCategoriesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _sub_categories_routing_module__WEBPACK_IMPORTED_MODULE_5__["SubCategoriesPageRoutingModule"], src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_sub_categories_page__WEBPACK_IMPORTED_MODULE_6__["SubCategoriesPage"]]
      })], SubCategoriesPageModule);
      /***/
    },

    /***/
    "6uY0":
    /*!************************************************************!*\
      !*** ./src/app/tab1/sub-categories/sub-categories.page.ts ***!
      \************************************************************/

    /*! exports provided: SubCategoriesPage */

    /***/
    function uY0(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SubCategoriesPage", function () {
        return SubCategoriesPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_sub_categories_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./sub-categories.page.html */
      "oKKc");
      /* harmony import */


      var _sub_categories_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./sub-categories.page.scss */
      "GqI8");
      /* harmony import */


      var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/loader.service */
      "5dVO");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_services_sub_category_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/sub-category.service */
      "ent6");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");

      var SubCategoriesPage = /*#__PURE__*/function () {
        function SubCategoriesPage(navCtrl, router, route, subCategoryService, loaderService) {
          var _this = this;

          _classCallCheck(this, SubCategoriesPage);

          this.navCtrl = navCtrl;
          this.router = router;
          this.route = route;
          this.subCategoryService = subCategoryService;
          this.loaderService = loaderService;
          this.loading = false;
          this.currentPage = 1;
          this.lastPage = 1;
          this.data = [];
          this.videos_data = [];
          this.name = '';
          this.environment = src_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"];
          this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
              _this.name = _this.router.getCurrentNavigation().extras.state.name;
              _this.id = _this.router.getCurrentNavigation().extras.state.id;
              _this.currentPage = 1;
              _this.lastPage = 1;
              _this.data = [];
              _this.videos_data = []; // const c_id = localStorage.getItem('c_id');
              // console.log("c_id : ",c_id)
              // if(c_id){
              //   this.getSubCategory(this.currentPage, c_id, this.name)
              // }else{

              _this.getSubCategory(_this.currentPage, _this.id); // }

            }
          });
        }

        _createClass(SubCategoriesPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {// this.environment = environment;
            // this.route.queryParams.subscribe(params => {
            //   if (this.router.getCurrentNavigation().extras.state) {
            //     this.name = this.router.getCurrentNavigation().extras.state.name;
            //     this.id = this.router.getCurrentNavigation().extras.state.id;
            //   }
            // });
          }
        }, {
          key: "getSubCategory",
          value: function getSubCategory(currentPage, c_id) {
            var _this2 = this;

            var event = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            this.loaderService.showLoader();
            this.loading = true;
            var formData = new FormData(); // localStorage.setItem('c_id', c_id);

            formData.append("PageNumber", currentPage);
            formData.append("categoryid", c_id);
            this.subCategoryService.getSubCategory(formData).then(function (res) {
              console.log(res);

              if (res.status != false) {
                _this2.loading = false;
                _this2.currentPage = res.CurrentPage;
                _this2.lastPage = res.total;

                if (_this2.currentPage == 1) {
                  _this2.data = res.categories;

                  if (res.videocount > 0) {
                    _this2.videos_data = res.videos;
                  }

                  _this2.loaderService.hideLoader();
                } else {
                  _this2.data = [].concat(_toConsumableArray(_this2.data), _toConsumableArray(res.categories));

                  if (res.videocount > 0) {
                    _this2.videos_data = [].concat(_toConsumableArray(_this2.videos_data), _toConsumableArray(res.videos));
                  }

                  _this2.loaderService.hideLoader();
                }

                if (event) {
                  event.target.complete();
                }
              } else {
                _this2.loaderService.hideLoader();

                _this2.loading = false;
              }
            }, function (err) {
              _this2.loaderService.hideLoader();

              _this2.loading = false;

              if (event) {
                event.target.complete();
              }
            });
          }
        }, {
          key: "subCategoryDetail",
          value: function subCategoryDetail(id, name) {
            var navigationExtras = {
              state: {
                id: id,
                name: name
              }
            };
            this.router.navigate(['tabs/tab1/sub-categories/sub-categories-detail'], navigationExtras);
          }
        }, {
          key: "refreshData",
          value: function refreshData(event) {
            this.currentPage = 1;
            this.getSubCategory(1, event);
          }
        }, {
          key: "ConvertToInt",
          value: function ConvertToInt(currentPage) {
            return parseInt(currentPage);
          }
        }, {
          key: "loadMoreData",
          value: function loadMoreData(value, event) {
            this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value);
            this.getSubCategory(this.currentPage, event);
          }
        }, {
          key: "back",
          value: function back() {
            this.navCtrl.back();
          }
        }]);

        return SubCategoriesPage;
      }();

      SubCategoriesPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]
        }, {
          type: src_app_services_sub_category_service__WEBPACK_IMPORTED_MODULE_7__["SubCategoryService"]
        }, {
          type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"]
        }];
      };

      SubCategoriesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-sub-categories',
        template: _raw_loader_sub_categories_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_sub_categories_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], SubCategoriesPage);
      /***/
    },

    /***/
    "GqI8":
    /*!**************************************************************!*\
      !*** ./src/app/tab1/sub-categories/sub-categories.page.scss ***!
      \**************************************************************/

    /*! exports provided: default */

    /***/
    function GqI8(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".quiz-categories {\n  position: relative;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  margin-bottom: 2px;\n  margin-top: 2px;\n}\n\n.quiz-categories h3 {\n  position: absolute;\n  text-align: center;\n  width: 100%;\n  color: white;\n  font-size: 50px;\n  overflow-wrap: break-word;\n  margin: 0px;\n  line-height: 45px;\n  padding: 5px;\n  font-family: Ubuntu;\n}\n\n.img-container {\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3N1Yi1jYXRlZ29yaWVzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUVBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBQ0E7RUFDSSxXQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtBQUVKIiwiZmlsZSI6InN1Yi1jYXRlZ29yaWVzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5xdWl6LWNhdGVnb3JpZXMge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG4gICAgbWFyZ2luLXRvcDogMnB4O1xufVxuLnF1aXotY2F0ZWdvcmllcyBoM3tcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDUwcHg7XG4gIC8vICBiYWNrZ3JvdW5kOiAjNDY0NjQ2NGQ7XG4gICAgb3ZlcmZsb3ctd3JhcDogYnJlYWstd29yZDtcbiAgICBtYXJnaW46IDBweDtcbiAgICBsaW5lLWhlaWdodDogNDVweDtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgZm9udC1mYW1pbHk6IFVidW50dTtcbn1cbi5pbWctY29udGFpbmVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBvYmplY3QtZml0OiBjb3Zlcjtcbn0iXX0= */";
      /***/
    },

    /***/
    "ent6":
    /*!**************************************************!*\
      !*** ./src/app/services/sub-category.service.ts ***!
      \**************************************************/

    /*! exports provided: SubCategoryService */

    /***/
    function ent6(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SubCategoryService", function () {
        return SubCategoryService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");
      /* harmony import */


      var _network_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./network.service */
      "dwY0");

      var SubCategoryService = /*#__PURE__*/function () {
        function SubCategoryService(http, networkService) {
          _classCallCheck(this, SubCategoryService);

          this.http = http;
          this.networkService = networkService;
          this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
        }

        _createClass(SubCategoryService, [{
          key: "getSubCategory",
          value: function getSubCategory(formData) {
            var _this3 = this;

            return new Promise(function (resolve, reject) {
              _this3.networkService.onNetworkChange().subscribe(function (status) {
                if (status == _network_service__WEBPACK_IMPORTED_MODULE_4__["ConnectionStatus"].Online) {
                  console.log(_this3.apiUrl + 'subcategory.php');
                  formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

                  _this3.http.post(_this3.apiUrl + 'subcategory.php', formData).subscribe(function (res) {
                    resolve(res);
                  }, function (err) {
                    reject(err);
                    console.log('something went wrong please try again');
                  });
                }
              });
            });
          }
        }]);

        return SubCategoryService;
      }();

      SubCategoryService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }, {
          type: _network_service__WEBPACK_IMPORTED_MODULE_4__["NetworkService"]
        }];
      };

      SubCategoryService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], SubCategoryService);
      /***/
    },

    /***/
    "oKKc":
    /*!****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/sub-categories/sub-categories.page.html ***!
      \****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function oKKc(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar  class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">{{name}}</ion-title>\n    <ion-icon  slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" routerLink=\"/tabs/tab1\"></ion-icon>\n  </ion-toolbar>\n</ion-header> -->\n<header title=\"{{name}}\"></header>\n\n<ion-content>\n  <div class=\"bg-white\">\n    <div class=\"position-relative text-align-center h-100 w-100\" *ngIf=\"!loading && data.length == 0 && videos_data.length == 0\">\n      <h3>No Data Found</h3>\n    </div>\n\n    <div *ngFor=\"let item of data\" class=\"quiz-categories\" (click)=subCategoryDetail(item.id,item.title)>\n      <!-- <h3>{{item.title}}</h3> -->\n      <img [src]=\"environment.imageUrl + item.image\" class=\"img-container\">\n    </div>\n\n  </div>\n\n    <!-- ​REFRESHER -->\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"refreshData($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n\n  <!-- INFINITE SCROLL -->\n  <ion-infinite-scroll *ngIf=\"!loading && currentPage<lastPage\" threshold=\"100px\" (ionInfinite)=\"loadMoreData(1, $event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more items...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>";
      /***/
    },

    /***/
    "oaDq":
    /*!**********************************************************************!*\
      !*** ./src/app/tab1/sub-categories/sub-categories-routing.module.ts ***!
      \**********************************************************************/

    /*! exports provided: SubCategoriesPageRoutingModule */

    /***/
    function oaDq(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SubCategoriesPageRoutingModule", function () {
        return SubCategoriesPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _sub_categories_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./sub-categories.page */
      "6uY0");

      var routes = [{
        path: '',
        component: _sub_categories_page__WEBPACK_IMPORTED_MODULE_3__["SubCategoriesPage"]
      }, {
        path: 'video',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-video-video-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-video-video-module")]).then(__webpack_require__.bind(null,
          /*! ..//../pages/video/video.module */
          "R2cY")).then(function (m) {
            return m.VideoPageModule;
          });
        }
      }, {
        path: 'video-player',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-video-player-video-player-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-video-player-video-player-module")]).then(__webpack_require__.bind(null,
          /*! ..//../pages/video-player/video-player.module */
          "GCVo")).then(function (m) {
            return m.VideoPlayerPageModule;
          });
        }
      }, {
        path: 'sub-categories-detail',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | sub-categories-detail-sub-categories-detail-module */
          "sub-categories-detail-sub-categories-detail-module").then(__webpack_require__.bind(null,
          /*! ./sub-categories-detail/sub-categories-detail.module */
          "PJLp")).then(function (m) {
            return m.SubCategoriesDetailPageModule;
          });
        }
      }];

      var SubCategoriesPageRoutingModule = function SubCategoriesPageRoutingModule() {
        _classCallCheck(this, SubCategoriesPageRoutingModule);
      };

      SubCategoriesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SubCategoriesPageRoutingModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=sub-categories-sub-categories-module-es5.js.map