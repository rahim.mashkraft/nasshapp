(function () {
  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sub-categories-detail-sub-categories-detail-module"], {
    /***/
    "MKaY":
    /*!*******************************************************************************************!*\
      !*** ./src/app/tab1/sub-categories/sub-categories-detail/sub-categories-detail.page.scss ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function MKaY(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzdWItY2F0ZWdvcmllcy1kZXRhaWwucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "PJLp":
    /*!*******************************************************************************************!*\
      !*** ./src/app/tab1/sub-categories/sub-categories-detail/sub-categories-detail.module.ts ***!
      \*******************************************************************************************/

    /*! exports provided: SubCategoriesDetailPageModule */

    /***/
    function PJLp(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SubCategoriesDetailPageModule", function () {
        return SubCategoriesDetailPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _sub_categories_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./sub-categories-detail-routing.module */
      "lXZj");
      /* harmony import */


      var _sub_categories_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./sub-categories-detail.page */
      "zNbp");
      /* harmony import */


      var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/modules/shared/shared.module */
      "FpXt");

      var SubCategoriesDetailPageModule = function SubCategoriesDetailPageModule() {
        _classCallCheck(this, SubCategoriesDetailPageModule);
      };

      SubCategoriesDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _sub_categories_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["SubCategoriesDetailPageRoutingModule"], src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_sub_categories_detail_page__WEBPACK_IMPORTED_MODULE_6__["SubCategoriesDetailPage"]]
      })], SubCategoriesDetailPageModule);
      /***/
    },

    /***/
    "VukA":
    /*!*********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/sub-categories/sub-categories-detail/sub-categories-detail.page.html ***!
      \*********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function VukA(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<header title=\"{{name}}\"></header>\n\n<ion-content>\n  <div class=\"bg-white\">\n    <div class=\"position-relative text-align-center h-100 w-100\" *ngIf=\"!loading && videos_data.length == 0\">\n      <h3>No Data Found</h3>\n    </div>\n\n    <div *ngFor=\"let video of videos_data\" class=\"quiz-categories\" (click)=openVideo(video.videotitle,video.videoimage,video.videourl,video.id)>\n      <!-- <h3>{{item.title}}</h3> -->\n      <img [src]=\"environment.imageUrl + video.videoimage\" class=\"img-container\">\n    </div>\n  </div>\n\n    <!-- ​REFRESHER -->\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"refreshData($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n\n  <!-- INFINITE SCROLL -->\n  <ion-infinite-scroll *ngIf=\"!loading && currentPage<lastPage\" threshold=\"100px\" (ionInfinite)=\"loadMoreData(1, $event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more items...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n  </div>\n</div>";
      /***/
    },

    /***/
    "cxbk":
    /*!**********************************************!*\
      !*** ./src/environments/environment.prod.ts ***!
      \**********************************************/

    /*! exports provided: environment */

    /***/
    function cxbk(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      });

      var environment = {
        production: true,
        imageUrl: 'https:',
        apiUrl: 'https://mindandheartuniversity.com/admin/appapi/',
        profileImageUrl: 'https://mindandheartuniversity.com/',
        auth_token: '4d409766604ad82ba5eeb96077c977c9',
        downloadurl: 'https://mindandheartuniversity.com/admin/'
      };
      /***/
    },

    /***/
    "lXZj":
    /*!***************************************************************************************************!*\
      !*** ./src/app/tab1/sub-categories/sub-categories-detail/sub-categories-detail-routing.module.ts ***!
      \***************************************************************************************************/

    /*! exports provided: SubCategoriesDetailPageRoutingModule */

    /***/
    function lXZj(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SubCategoriesDetailPageRoutingModule", function () {
        return SubCategoriesDetailPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _sub_categories_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./sub-categories-detail.page */
      "zNbp");

      var routes = [{
        path: '',
        component: _sub_categories_detail_page__WEBPACK_IMPORTED_MODULE_3__["SubCategoriesDetailPage"]
      }];

      var SubCategoriesDetailPageRoutingModule = function SubCategoriesDetailPageRoutingModule() {
        _classCallCheck(this, SubCategoriesDetailPageRoutingModule);
      };

      SubCategoriesDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SubCategoriesDetailPageRoutingModule);
      /***/
    },

    /***/
    "zNbp":
    /*!*****************************************************************************************!*\
      !*** ./src/app/tab1/sub-categories/sub-categories-detail/sub-categories-detail.page.ts ***!
      \*****************************************************************************************/

    /*! exports provided: SubCategoriesDetailPage */

    /***/
    function zNbp(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SubCategoriesDetailPage", function () {
        return SubCategoriesDetailPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_sub_categories_detail_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./sub-categories-detail.page.html */
      "VukA");
      /* harmony import */


      var _sub_categories_detail_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./sub-categories-detail.page.scss */
      "MKaY");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/loader.service */
      "5dVO");
      /* harmony import */


      var src_app_services_sub_category_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/sub-category.service */
      "ent6");
      /* harmony import */


      var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/environments/environment.prod */
      "cxbk");

      var SubCategoriesDetailPage = /*#__PURE__*/function () {
        function SubCategoriesDetailPage(navCtrl, router, route, subCategoryService, loaderService) {
          var _this = this;

          _classCallCheck(this, SubCategoriesDetailPage);

          this.navCtrl = navCtrl;
          this.router = router;
          this.route = route;
          this.subCategoryService = subCategoryService;
          this.loaderService = loaderService;
          this.name = '';
          this.currentPage = 1;
          this.lastPage = 1;
          this.loading = false;
          this.environment = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_8__["environment"];
          this.route.queryParams.subscribe(function (params) {
            if (_this.router.getCurrentNavigation().extras.state) {
              _this.name = _this.router.getCurrentNavigation().extras.state.name;
              _this.id = _this.router.getCurrentNavigation().extras.state.id;
              _this.currentPage = 1;
              _this.lastPage = 1;
              _this.videos_data = [];

              _this.getSubCategory(_this.currentPage, _this.id);
            }
          });
        }

        _createClass(SubCategoriesDetailPage, [{
          key: "getSubCategory",
          value: function getSubCategory(currentPage, c_id) {
            var _this2 = this;

            var event = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            this.loaderService.showLoader();
            this.loading = true;
            var formData = new FormData();
            formData.append("PageNumber", currentPage);
            formData.append("categoryid", c_id);
            this.subCategoryService.getSubCategory(formData).then(function (res) {
              console.log(res);

              if (res.status != false) {
                _this2.loading = false;
                _this2.currentPage = res.CurrentPage;
                _this2.lastPage = res.total;

                if (_this2.currentPage == 1) {
                  if (res.videocount > 0) {
                    _this2.videos_data = res.videos;
                  }

                  _this2.loaderService.hideLoader();
                } else {
                  if (res.videocount > 0) {
                    _this2.videos_data = [].concat(_toConsumableArray(_this2.videos_data), _toConsumableArray(res.videos));
                  }

                  _this2.loaderService.hideLoader();
                }

                if (event) {
                  event.target.complete();
                }
              } else {
                _this2.loaderService.hideLoader();

                _this2.loading = false;
              }
            }, function (err) {
              _this2.loaderService.hideLoader();

              _this2.loading = false;

              if (event) {
                event.target.complete();
              }
            });
          }
        }, {
          key: "refreshData",
          value: function refreshData(event) {
            this.currentPage = 1;
            this.getSubCategory(1, event);
          }
        }, {
          key: "ConvertToInt",
          value: function ConvertToInt(currentPage) {
            return parseInt(currentPage);
          }
        }, {
          key: "loadMoreData",
          value: function loadMoreData(value, event) {
            this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value);
            this.getSubCategory(this.currentPage, event);
          }
        }, {
          key: "openVideo",
          value: function openVideo(videotitle, videoimage, videourl, videoid) {
            var navigationExtras = {
              state: {
                videotitle: videotitle,
                videoimage: videoimage,
                videourl: videourl,
                videoid: videoid
              }
            }; // this.router.navigate(['post-add-second', navigationExtras]);

            this.router.navigate(['tabs/tab1/sub-categories/video'], navigationExtras); // this.router.navigate(['testing'], navigationExtras);
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return SubCategoriesDetailPage;
      }();

      SubCategoriesDetailPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: src_app_services_sub_category_service__WEBPACK_IMPORTED_MODULE_7__["SubCategoryService"]
        }, {
          type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"]
        }];
      };

      SubCategoriesDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-sub-categories-detail',
        template: _raw_loader_sub_categories_detail_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_sub_categories_detail_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], SubCategoriesDetailPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=sub-categories-detail-sub-categories-detail-module-es5.js.map