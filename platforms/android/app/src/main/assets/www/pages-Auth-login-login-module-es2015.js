(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-Auth-login-login-module"],{

/***/ "+fk9":
/*!***********************************************!*\
  !*** ./src/app/services/device-id.service.ts ***!
  \***********************************************/
/*! exports provided: DeviceIdService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceIdService", function() { return DeviceIdService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");




let DeviceIdService = class DeviceIdService {
    constructor(http) {
        this.http = http;
        this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    device_store(formData) {
        return new Promise((resolve, reject) => {
            console.log(this.apiUrl + 'updatedeviceid.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'updatedeviceid.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
        });
    }
};
DeviceIdService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
DeviceIdService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], DeviceIdService);



/***/ }),

/***/ "2g2N":
/*!*******************************************!*\
  !*** ./src/app/services/toast.service.ts ***!
  \*******************************************/
/*! exports provided: ToastService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastService", function() { return ToastService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let ToastService = class ToastService {
    constructor(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    showToast(message = 'Successfully Logged in!') {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                cssClass: 'bg-toast',
                message: message,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }
    showToastUpdateProfile(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                cssClass: 'bg-toast',
                message: message,
                duration: 3000,
                position: 'bottom'
            });
            toast.present();
        });
    }
};
ToastService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
ToastService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ToastService);



/***/ }),

/***/ "53Cd":
/*!**************************************************!*\
  !*** ./src/app/pages/Auth/login/login.page.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".main-div {\n  position: relative;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n}\n.main-div .outer-div {\n  width: 100%;\n  height: 20vh;\n  opacity: 0.3;\n}\n.background {\n  --background: #000 url(\"/assets/img/background.png\") 0 0/100% 25vh no-repeat ;\n}\n.content-div .error-div {\n  display: flex;\n  width: 100%;\n  padding-left: 10px;\n}\n.content-div .error-div p.text08 {\n  margin: 0;\n  margin-top: 2px;\n}\n.content-div h2 {\n  margin: 0;\n  font-size: 26px;\n  margin-top: 20px;\n  color: #fff;\n}\n.content-div .toggle {\n  margin: 10px;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  color: #fff;\n  --background: white !important;\n  justify-content: center;\n}\n.content-div .toggle ion-text {\n  margin-right: 5px;\n  font-weight: 600;\n}\n.content-div ion-toggle {\n  --background: white !important;\n}\n.content-div h4 {\n  margin: 0;\n  font-size: 16px;\n  font-weight: 600;\n  color: #fff;\n}\n.content-div h5 {\n  margin: 0;\n  font-size: 12px;\n  text-decoration: underline;\n  font-weight: 600;\n  color: #fff;\n}\n.content-div ion-button {\n  color: #fff;\n  font-size: 18px;\n  --background: #079199;\n  height: 45px;\n}\n.content-div ion-item {\n  border: 1px solid #0000006e;\n  border-radius: 5px;\n  margin-top: 25px;\n  border-radius: 10px;\n  background: white;\n}\n.content-div ion-item ion-input {\n  height: 100%;\n  margin-left: 5px;\n  padding: 10px !important;\n}\n.content-div ion-item ion-icon {\n  margin: 0;\n}\n.apple-btn {\n  color: #000000 !important;\n  --background: #ffffff !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7QUFDRjtBQUNFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFFQSxZQUFBO0FBQUo7QUFJQTtFQUNFLDZFQUFBO0FBREY7QUFVRTtFQUNFLGFBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFQSjtBQVNJO0VBQ0UsU0FBQTtFQUNBLGVBQUE7QUFQTjtBQVdFO0VBQ0UsU0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUFUSjtBQVlFO0VBQ0UsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsdUJBQUE7QUFWSjtBQVlJO0VBQ0UsaUJBQUE7RUFDQSxnQkFBQTtBQVZOO0FBY0U7RUFDRSw4QkFBQTtBQVpKO0FBZUU7RUFDRSxTQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQWJKO0FBZ0JFO0VBQ0UsU0FBQTtFQUNBLGVBQUE7RUFDQSwwQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQWRKO0FBaUJFO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7QUFmSjtBQWtCRTtFQUNFLDJCQUFBO0VBQ0Esa0JBQUE7RUFLQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUFwQko7QUFzQkk7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSx3QkFBQTtBQXBCTjtBQXVCSTtFQUNFLFNBQUE7QUFyQk47QUF5QkE7RUFHRSx5QkFBQTtFQUNBLGdDQUFBO0FBeEJGIiwiZmlsZSI6ImxvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluLWRpdiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcblxuICAub3V0ZXItZGl2IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDIwdmg7XG4gICAgLy8gICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZy9sb2dpbkJHLmpwZykgbm8tcmVwZWF0IGNlbnRlciBjZW50ZXIgLyBjb3ZlcjtcbiAgICBvcGFjaXR5OiAwLjM7XG4gIH1cbn1cblxuLmJhY2tncm91bmQge1xuICAtLWJhY2tncm91bmQ6ICMwMDAgdXJsKFwiL2Fzc2V0cy9pbWcvYmFja2dyb3VuZC5wbmdcIikgMCAwLzEwMCUgMjV2aCBuby1yZXBlYXRcbn1cblxuLmNvbnRlbnQtZGl2IHtcblxuICAvLyBkaXNwbGF5OiBmbGV4O1xuICAvLyBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAvLyBtYXJnaW4tdG9wOiA1dmg7XG4gIC8vIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIC5lcnJvci1kaXYge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuXG4gICAgcC50ZXh0MDgge1xuICAgICAgbWFyZ2luOiAwO1xuICAgICAgbWFyZ2luLXRvcDogMnB4O1xuICAgIH1cbiAgfVxuXG4gIGgyIHtcbiAgICBtYXJnaW46IDA7XG4gICAgZm9udC1zaXplOiAyNnB4O1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgY29sb3I6ICNmZmY7XG4gIH1cblxuICAudG9nZ2xlIHtcbiAgICBtYXJnaW46IDEwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgaW9uLXRleHQge1xuICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gICAgICBmb250LXdlaWdodDogNjAwO1xuICAgIH1cbiAgfVxuXG4gIGlvbi10b2dnbGUge1xuICAgIC0tYmFja2dyb3VuZDogd2hpdGUgIWltcG9ydGFudDtcbiAgfVxuXG4gIGg0IHtcbiAgICBtYXJnaW46IDA7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY29sb3I6ICNmZmY7XG4gIH1cblxuICBoNSB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGNvbG9yOiAjZmZmO1xuICB9XG5cbiAgaW9uLWJ1dHRvbiB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIC0tYmFja2dyb3VuZDogIzA3OTE5OTtcbiAgICBoZWlnaHQ6IDQ1cHg7XG4gIH1cblxuICBpb24taXRlbSB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzAwMDAwMDZlO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAvLyAgIGhlaWdodDogNjBweDtcbiAgICAvLyAgIGRpc3BsYXk6IGZsZXg7XG4gICAgLy8gICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIC8vICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogMjVweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuXG4gICAgaW9uLWlucHV0IHtcbiAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gICAgICBwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7XG4gICAgfVxuXG4gICAgaW9uLWljb24ge1xuICAgICAgbWFyZ2luOiAwO1xuICAgIH1cbiAgfVxufVxuLmFwcGxlLWJ0bntcbiAgLy8gYmFja2dyb3VuZC1jb2xvcjogICNjZWNlY2UgIWltcG9ydGFudDtcbiAgLy8gY29sb3I6ICMwMDAwMDA7XG4gIGNvbG9yOiAjMDAwMDAwICFpbXBvcnRhbnQ7XG4gIC0tYmFja2dyb3VuZDogI2ZmZmZmZiAhaW1wb3J0YW50O1xufVxuXG4iXX0= */");

/***/ }),

/***/ "5dVO":
/*!********************************************!*\
  !*** ./src/app/services/loader.service.ts ***!
  \********************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let LoaderService = class LoaderService {
    constructor(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    showLoader() {
        this.isBusy = true;
        // this.loaderToShow = this.loadingCtrl.create({
        //   message: 'Please Wait..'
        // }).then((res) => {
        //   res.present();
        //   // res.onDidDismiss().then((dis) => {
        //   //    console.log('Loading dismissed!',dis);
        //   // });
        // });
        // // this.hideLoader();
    }
    hideLoader() {
        // setTimeout(()=>{
        //   this.loadingCtrl.dismiss();
        // },100)
        this.isBusy = false;
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ "D+V3":
/*!************************************************!*\
  !*** ./src/app/pages/Auth/login/login.page.ts ***!
  \************************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./login.page.html */ "WUfb");
/* harmony import */ var _login_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.page.scss */ "53Cd");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "GGTb");
/* harmony import */ var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/google-plus/ngx */ "up+p");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/alert.service */ "3LUQ");
/* harmony import */ var src_app_services_events_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/events.service */ "riPR");
/* harmony import */ var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/loader.service */ "5dVO");
/* harmony import */ var src_app_services_login_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/services/login.service */ "EFyh");
/* harmony import */ var src_app_services_network_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/services/network.service */ "dwY0");
/* harmony import */ var src_app_services_social_sign_up_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/app/services/social-sign-up.service */ "YUD2");
/* harmony import */ var src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! src/app/services/toast.service */ "2g2N");
/* harmony import */ var cordova_plugin_fcm_with_dependecy_updated_ionic_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! cordova-plugin-fcm-with-dependecy-updated/ionic/ngx */ "lOSq");
/* harmony import */ var _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/device/ngx */ "xS7M");
/* harmony import */ var src_app_services_device_id_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! src/app/services/device-id.service */ "+fk9");















// import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic";



let LoginPage = class LoginPage {
    constructor(navCtrl, menuCtrl, formBuilder, loginService, networkService, loaderService, toastService, alertService, eventsService, socialSignUpService, fb, googlePlus, platform, fcm, device, deviceIdService) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.formBuilder = formBuilder;
        this.loginService = loginService;
        this.networkService = networkService;
        this.loaderService = loaderService;
        this.toastService = toastService;
        this.alertService = alertService;
        this.eventsService = eventsService;
        this.socialSignUpService = socialSignUpService;
        this.fb = fb;
        this.googlePlus = googlePlus;
        this.platform = platform;
        this.fcm = fcm;
        this.device = device;
        this.deviceIdService = deviceIdService;
        this.formData = new FormData();
        this.eye = false;
        this.passwordType = 'password';
        this.setupFCM();
    }
    showPassword() {
        this.eye = true;
        this.passwordType = 'text';
    }
    hidePassword() {
        this.eye = false;
        this.passwordType = 'password';
    }
    ionViewDidLoad() {
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(false);
    }
    ngOnInit() {
        this.onLoginForm = this.formBuilder.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required])],
            toggle: ['false'],
        });
    }
    // // //
    goToRegister() {
        this.navCtrl.navigateForward('/register');
    }
    goToHome() {
        // this.udId = localStorage.getItem('device_token')
        this.formData.append("email", this.onLoginForm.value['email']);
        this.formData.append("password", this.onLoginForm.value['password']);
        this.networkService.onNetworkChange().subscribe((status) => {
            if (status == src_app_services_network_service__WEBPACK_IMPORTED_MODULE_12__["ConnectionStatus"].Online) {
                this.loaderService.showLoader();
                this.loginService.login(this.formData).subscribe((res) => {
                    console.log(res);
                    if (res.status !== false) {
                        this.get_device_id_token(res.aid);
                        localStorage.setItem('user_id', res.aid);
                        localStorage.setItem('firstname', res.firstname);
                        localStorage.setItem('lastname', res.lastname);
                        localStorage.setItem('email', res.email);
                        localStorage.setItem('profileimage', res.profileimage);
                        localStorage.setItem('address', res.address);
                        localStorage.setItem('city', res.city);
                        localStorage.setItem('country', res.country);
                        localStorage.setItem('phone', res.phone);
                        localStorage.setItem('username', res.username);
                        localStorage.setItem('zipcode', res.zipcode);
                        localStorage.setItem('state', res.state);
                        localStorage.setItem('toggle', this.onLoginForm.value['toggle']);
                        this.loaderService.hideLoader();
                        this.toastService.showToast();
                        const userData = {
                            firstname: res.firstname,
                            lastname: res.lastname,
                            email: res.email,
                            profileimage: res.profileimage
                        };
                        this.eventsService.publish('userLogged', userData);
                        this.navCtrl.navigateRoot('/tabs');
                    }
                    else if (res.status !== true) {
                        this.loaderService.hideLoader();
                        this.alertService.presentAlertError(res.error_msg);
                    }
                    else {
                        this.loaderService.hideLoader();
                        this.alertService.presentAlertError('Error Connecting to Internet');
                    }
                });
            }
            else {
                console.log("network not available");
                this.alertService.presentNetworkAlert();
            }
        });
    }
    facebookLogin() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.fb.login(['public_profile', 'email']).then((res) => {
                if (res.status == "connected") {
                    var fb_id = res.authResponse.userID;
                    var fb_token = res.authResponse.accessToken;
                    this.fb.api("/me?fields=name,email,first_name,last_name,picture", []).then((user) => {
                        console.log('facebookLogin : ', user);
                        var name = user.name;
                        if (user.email) {
                            var email = user.email;
                        }
                        else {
                            email = '';
                        }
                        var first_name = user.first_name;
                        var last_name = user.last_name;
                        console.log("=== USER INFOS ===");
                        console.log("Name : " + name);
                        console.log("Email : " + email);
                        console.log("fb_id : " + fb_id);
                        var res = name.split();
                        let firstname = res[0];
                        let lastname = res[1];
                        var picture = user.picture.data.url;
                        if (picture == undefined || picture == null) {
                            picture = '';
                        }
                        let item = { 'first_name': first_name, 'last_name': last_name, 'image': picture, 'email': email, 'signuptype': 'FACEBOOK', 'signupid': fb_id };
                        this.social_login(item);
                    });
                }
                else {
                    console.log("An error occurred...");
                }
            })
                .catch((e) => {
                console.log('Error logging into Facebook', e);
            });
        });
    }
    login_google() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.googlePlus.login({}).then(res => {
                console.log("googlePlus : ", res);
                var name = res.displayName;
                var email = res.email;
                var userId = res.userId;
                var firstname = res.givenName;
                var lastname = res.familyName;
                var picture = res.imageUrl;
                if (picture == undefined || picture == null) {
                    picture = '';
                }
                var name3 = '';
                if (lastname != '') {
                    name3 = firstname + " " + lastname;
                }
                else {
                    name3 = firstname;
                }
                console.log("Name3 : " + name3);
                if (name3 != '') {
                    name = name3;
                    console.log("Name4 : " + name);
                }
                console.log("=== USER INFOS ===");
                console.log("Name : " + name);
                console.log("Email : " + email);
                console.log("userId : " + userId);
                console.log("picture : " + picture);
                let item = { 'firstname': firstname, 'lastname': lastname, 'email': email, 'image': picture, 'signuptype': 'GOOGLE', 'signupid': userId };
                this.social_login(item);
            }, err => {
                console.log(err);
            });
        });
    }
    loginWithApple() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // this.loaderService.showLoader();
            // let loading = this.loadingCtrl.create({
            //   content: ''
            // });
            let self = this;
            yield cordova.plugins.SignInWithApple.signin({ requestedScopes: [0, 1] }, function (succ) {
                console.log(succ);
                console.log("loginWithApple : ", succ);
                // console.error('error222')
                var apple_id = 0;
                var givenName = '';
                var familyName = '';
                var email = '';
                var picture = '';
                if (succ.user) {
                    apple_id = succ.user;
                }
                if (succ.email) {
                    email = succ.email;
                }
                if (succ.fullName.givenName) {
                    givenName = succ.fullName.givenName;
                }
                if (succ.fullName.familyName) {
                    familyName = succ.fullName.familyName;
                }
                var name = givenName + ' ' + familyName;
                var name2 = givenName + '' + familyName;
                var fname = '';
                var lname = '';
                fname = givenName;
                lname = familyName;
                console.log("Name : " + name);
                console.log("Email : " + email);
                console.log("userId : " + apple_id);
                // alert(JSON.stringify(succ));
                // alert(JSON.stringify("userId : " + apple_id));
                // alert(JSON.stringify('email='+succ.email));
                // alert(JSON.stringify('email='+email));
                // if (email == '') {
                //   email = self.profile.email;
                // }
                // if (name2 == '') {
                //   name = self.profile.fname + ' ' + self.profile.lname;
                //   fname = self.profile.fname;
                //   lname = self.profile.lname;
                // }
                let item = { 'firstname': fname, 'lastname': lname, 'email': email, 'image': picture, 'signuptype': 'APPLE', 'signupid': apple_id };
                self.social_login(item);
                // setTimeout(() => {
                //   self.social_login(email, apple_id, name, fname, lname);
                // }, 500);
            }, function (err) {
                setTimeout(() => {
                    self.loaderService.hideLoader();
                }, 500);
                console.error('errorAEE');
                console.error(err);
                console.log(JSON.stringify(err));
            });
        });
    }
    social_login(item) {
        this.loaderService.showLoader();
        let formData = new FormData();
        formData.append("firstname", item.firstname);
        formData.append("lastname", item.lastname);
        formData.append("email", item.email);
        formData.append("image", item.image);
        formData.append("signuptype", item.signuptype);
        formData.append("signupid", item.signupid);
        this.socialSignUpService.social_login(formData).then((res) => {
            console.log("data : ", res);
            if (res.status != false) {
                this.get_device_id_token(res.aid);
                localStorage.setItem('user_id', res.aid);
                localStorage.setItem('firstname', res.firstname);
                localStorage.setItem('lastname', res.lastname);
                localStorage.setItem('email', res.email);
                localStorage.setItem('profileimage', res.profileimage);
                localStorage.setItem('address', res.address);
                localStorage.setItem('city', res.city);
                localStorage.setItem('country', res.country);
                localStorage.setItem('phone', res.phone);
                localStorage.setItem('username', res.username);
                localStorage.setItem('zipcode', res.zipcode);
                localStorage.setItem('state', res.state);
                localStorage.setItem('toggle', this.onLoginForm.value['toggle']);
                this.loaderService.hideLoader();
                this.toastService.showToast();
                const userData = {
                    firstname: res.firstname,
                    lastname: res.lastname,
                    email: res.email,
                    profileimage: res.profileimage
                };
                this.eventsService.publish('userLogged', userData);
                this.navCtrl.navigateRoot('/tabs');
            }
            else if (res.status != true) {
                this.loaderService.hideLoader();
                this.alertService.presentAlertError(res.error_msg);
            }
            else {
                this.loaderService.hideLoader();
                this.alertService.presentAlertError('Error Connecting to Internet');
            }
        }, err => {
            this.loaderService.hideLoader();
        });
    }
    get_device_id_token(aid) {
        if (this.platform.is('cordova')) {
            let thisObj = this;
            console.log("device token 1 =");
            // setTimeout(() => {
            thisObj.fcm.onTokenRefresh().subscribe(token => {
                console.log("device token=" + token);
                this.device_token = token;
                console.log('Device UUID is: ' + this.device.uuid);
                this.device_store(this.device.uuid, token, aid);
            });
            // }, 1000);
            // setTimeout(() => {
            thisObj.fcm.getToken().then(token => {
                this.device_token = token;
                console.log("device token2=" + token);
                console.log('Device UUID is2: ' + this.device.uuid);
                this.device_store(this.device.uuid, token, aid);
            });
            // }, 1000);
        }
    }
    device_store(uuid, fcmtoken, aid) {
        let formData = new FormData();
        formData.append("udid", uuid);
        formData.append("device_token", fcmtoken);
        formData.append("aid", aid);
        this.deviceIdService.device_store(formData).then(data => {
            console.log(data);
        });
    }
    setupFCM() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.platform.ready();
            console.log('FCM setup started');
            if (!this.platform.is('cordova')) {
                return;
            }
            console.log('In cordova platform');
            console.log('Subscribing to token updates');
            this.fcm.onTokenRefresh().subscribe((newToken) => {
                this.token = newToken;
                console.log('onTokenRefresh received event with: ', newToken);
            });
            console.log('Subscribing to new notifications');
            this.fcm.onNotification().subscribe((payload) => {
                this.pushPayload = payload;
                console.log('onNotification received event with: ', payload);
            });
            this.hasPermission = yield this.fcm.requestPushPermission();
            console.log('requestPushPermission result: ', this.hasPermission);
            this.token = yield this.fcm.getToken();
            console.log('getToken result: ', this.token);
            this.pushPayload = yield this.fcm.getInitialPushPayload();
            console.log('getInitialPushPayload result: ', this.pushPayload);
        });
    }
    get pushPayloadString() {
        return JSON.stringify(this.pushPayload, null, 4);
    }
    forgotPass() {
        this.navCtrl.navigateForward(['forgot-password']);
    }
};
LoginPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: src_app_services_login_service__WEBPACK_IMPORTED_MODULE_11__["LoginService"] },
    { type: src_app_services_network_service__WEBPACK_IMPORTED_MODULE_12__["NetworkService"] },
    { type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_10__["LoaderService"] },
    { type: src_app_services_toast_service__WEBPACK_IMPORTED_MODULE_14__["ToastService"] },
    { type: src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_8__["AlertService"] },
    { type: src_app_services_events_service__WEBPACK_IMPORTED_MODULE_9__["EventsService"] },
    { type: src_app_services_social_sign_up_service__WEBPACK_IMPORTED_MODULE_13__["SocialSignUPService"] },
    { type: _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_5__["Facebook"] },
    { type: _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_6__["GooglePlus"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"] },
    { type: cordova_plugin_fcm_with_dependecy_updated_ionic_ngx__WEBPACK_IMPORTED_MODULE_15__["FCM"] },
    { type: _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_16__["Device"] },
    { type: src_app_services_device_id_service__WEBPACK_IMPORTED_MODULE_17__["DeviceIdService"] }
];
LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], LoginPage);

// cordova plugin add cordova-plugin-googleplus --save --variable REVERSED_CLIENT_ID=com.googleusercontent.apps.584704282410-peo3u9r6to27b0r46imes0o1jo90idl2 --variable WEB_APPLICATION_CLIENT_ID=584704282410-sf9ug9rupeug4oj39ecotqnoa9lvkf37.apps.googleusercontent.com


/***/ }),

/***/ "WUfb":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/Auth/login/login.page.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"background\">\n  <form [formGroup]=\"onLoginForm\" class=\"main-div\">\n    <div class=\"outer-div\">\n    </div>\n    <div class=\"main-container\">\n      <div class=\"inner-div\">\n        <!-- <img src=\"assets/logo/logo.png\" /> -->\n      </div>\n      <div class=\"ion-margin-horizontal\">\n        <img src=\"assets/logo/logo.png\" />\n      </div>\n\n      <div class=\"content-div ion-text-center\">\n        <ion-item lines=\"none\" class=\"ion-margin-horizontal\">\n          <ion-input placeholder=\"Email\" type=\"email\" formControlName=\"email\"></ion-input>\n          <ion-icon name=\"mail-outline\" slot=\"start\"></ion-icon>\n        </ion-item>\n        <div class=\"error-div\">\n          <p ion-text class=\"text08\" *ngIf=\"onLoginForm.get('email').touched && onLoginForm.get('email').hasError('required')\">\n            <ion-text color=\"warning\">\n              Required Field\n            </ion-text>\n          </p>\n          <p ion-text class=\"text08\" *ngIf=\"onLoginForm.get('email').touched && onLoginForm.get('email').hasError('pattern')\">\n            <ion-text color=\"warning\">\n              Invalid Email\n            </ion-text>\n          </p>\n        </div>\n        <ion-item class=\"mt-10\" lines=\"none\" class=\"ion-margin-horizontal\">\n          <ion-input placeholder=\"Password\" type=\"{{passwordType}}\" formControlName=\"password\"></ion-input>\n          <ion-icon name=\"lock-closed-outline\" slot=\"start\"></ion-icon>\n          <ion-icon slot=\"end\" color=\"dark\" name=\"eye\" (click)=\"showPassword()\" *ngIf=\"!eye\"></ion-icon>\n          <ion-icon slot=\"end\" color=\"dark\" name=\"eye-off\"  (click)=\"hidePassword()\" *ngIf=\"eye\"></ion-icon>\n        </ion-item>\n        <div class=\"error-div\">\n          <p ion-text color=\"warning\" class=\"text08\"\n            *ngIf=\"onLoginForm.get('password').touched && onLoginForm.get('password').hasError('required')\">\n            <ion-text color=\"warning\">\n              Required Field\n            </ion-text>\n          </p>\n        </div>\n        <div class=\"toggle\">\n          <ion-text class=\"textSign\">KEEP ME SIGNED IN</ion-text>\n          <ion-toggle color=\"primary\" formControlName=\"toggle\"></ion-toggle>\n        </div>\n        <h5 (click)=\"forgotPass()\">FORGOT MY PASSWORD</h5>\n        <ion-button class=\"ion-margin-horizontal ion-text-uppercase ion-margin-vertical\"  expand=\"block\" (click)=\"goToHome()\" [disabled]=\"!onLoginForm.valid\">LOGIN</ion-button>\n        <ion-button class=\"ion-margin-horizontal ion-text-uppercase ion-margin-vertical facebook_color\"  expand=\"block\" (click)=\"facebookLogin()\">Sign in with FACEBOOK</ion-button>\n        <ion-button class=\"ion-margin-horizontal ion-text-uppercase ion-margin-vertical google_color\"  expand=\"block\" (click)=\"login_google()\">Sign in with GOOGLE</ion-button>\n        <ion-button *ngIf=\"platform.is('ios')\" class=\"ion-margin-horizontal ion-text-uppercase ion-margin-vertical apple-btn\"  expand=\"block\" (click)=\"loginWithApple()\">Sign in with Apple</ion-button>\n        <h4 class=\"ion-padding-vertical ion-margin-bottom\" (click)=\"goToRegister()\">CREATE NEW ACCOUNT</h4>\n      </div>\n    </div>\n  </form>\n\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>");

/***/ }),

/***/ "XR9V":
/*!**********************************************************!*\
  !*** ./src/app/pages/Auth/login/login-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "D+V3");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "YUD2":
/*!****************************************************!*\
  !*** ./src/app/services/social-sign-up.service.ts ***!
  \****************************************************/
/*! exports provided: SocialSignUPService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SocialSignUPService", function() { return SocialSignUPService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");




let SocialSignUPService = class SocialSignUPService {
    constructor(http) {
        this.http = http;
        this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    social_login(formData) {
        return new Promise((resolve, reject) => {
            console.log(this.apiUrl + 'thirdpartyauth.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'thirdpartyauth.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
        });
    }
};
SocialSignUPService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
SocialSignUPService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], SocialSignUPService);



/***/ }),

/***/ "loDh":
/*!**************************************************!*\
  !*** ./src/app/pages/Auth/login/login.module.ts ***!
  \**************************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "XR9V");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "D+V3");







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-Auth-login-login-module-es2015.js.map