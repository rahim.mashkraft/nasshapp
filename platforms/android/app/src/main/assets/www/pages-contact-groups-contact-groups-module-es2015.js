(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-contact-groups-contact-groups-module"],{

/***/ "5dVO":
/*!********************************************!*\
  !*** ./src/app/services/loader.service.ts ***!
  \********************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let LoaderService = class LoaderService {
    constructor(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    showLoader() {
        this.isBusy = true;
        // this.loaderToShow = this.loadingCtrl.create({
        //   message: 'Please Wait..'
        // }).then((res) => {
        //   res.present();
        //   // res.onDidDismiss().then((dis) => {
        //   //    console.log('Loading dismissed!',dis);
        //   // });
        // });
        // // this.hideLoader();
    }
    hideLoader() {
        // setTimeout(()=>{
        //   this.loadingCtrl.dismiss();
        // },100)
        this.isBusy = false;
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ "9dCw":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contact-groups/contact-groups.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header title=\"Contact Groups\"></header>\n\n<ion-content>\n  <ion-button  expand=\"full\" (click)=\"addContact()\">Add</ion-button>\n   <!-- Custom Table -->\n   <div class=\"bg-white\">\n    <!-- Header -->\n    <ion-grid>\n      <ion-row class=\"table-wrapper \">\n        <!-- <ion-item-divider> -->\n        <ion-col size=\"6\">\n          <ion-label>\n            <h5>Name</h5>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-label>\n            <h5>Phone / Email</h5>\n          </ion-label>\n        </ion-col>\n        \n      <!-- </ion-item-divider> -->\n\n      </ion-row>\n      <ion-row *ngFor=\"let item of contactsData; let i=index\">\n        <ion-col size=\"6\">\n          <ion-label>\n            <p *ngIf=\"item.firstname || item.lastname\" class=\"col_style\">{{item.firstname}} {{item.lastname}}</p>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-label>\n            <p *ngIf=\"item.phone\" class=\"d-block\">{{item.phone}}</p>\n            <p *ngIf=\"item.email != null || item.email != ''\" class=\"d-block\">{{item.email}}</p>\n          </ion-label>\n        </ion-col>\n        <hr>\n      </ion-row>\n\n    </ion-grid>\n    <!-- No Data -->\n    <h4 class=\"clr-medium ion-margin-vertical ion-text-center\" *ngIf=\"isCheck == true\">\n      No Record Found\n    </h4>\n  </div>\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>");

/***/ }),

/***/ "Qalu":
/*!*************************************************************!*\
  !*** ./src/app/pages/contact-groups/contact-groups.page.ts ***!
  \*************************************************************/
/*! exports provided: ContactGroupsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactGroupsPage", function() { return ContactGroupsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_contact_groups_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./contact-groups.page.html */ "9dCw");
/* harmony import */ var _contact_groups_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./contact-groups.page.scss */ "lY+8");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/contact-groups.service */ "rtEV");
/* harmony import */ var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/loader.service */ "5dVO");
/* harmony import */ var _model_model_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../model/model.page */ "9AS+");








let ContactGroupsPage = class ContactGroupsPage {
    constructor(loaderService, contactGroupsService, modalController) {
        this.loaderService = loaderService;
        this.contactGroupsService = contactGroupsService;
        this.modalController = modalController;
        this.currentPage = 1;
        this.lastPage = 1;
        this.isCheck = false;
    }
    ngOnInit() {
        this.getContactGroups(this.currentPage);
    }
    getContactGroups(currentPage, event = null) {
        this.loaderService.showLoader();
        let formData = new FormData();
        formData.append("PageNumber", currentPage);
        formData.append("aid", localStorage.getItem('user_id'));
        this.contactGroupsService.getContactGroupsList(formData).then((data) => {
            console.log("data : ", data);
            this.dataObj = data;
            this.currentPage = this.dataObj.CurrentPage;
            this.lastPage = this.dataObj.total;
            if (this.currentPage == 1) {
                this.contactsGroupsData = this.dataObj.contactgroups;
                if (this.contactsGroupsData.length == 0) {
                    this.isCheck = true;
                }
            }
            else {
                this.contactsGroupsData = [...this.contactsGroupsData, ...this.dataObj.contactgroups];
            }
            this.loaderService.hideLoader();
            if (event) {
                event.target.complete();
            }
        }, err => {
            this.loaderService.hideLoader();
            this.isCheck = true;
            if (event) {
                event.target.complete();
            }
        });
    }
    addContact() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _model_model_page__WEBPACK_IMPORTED_MODULE_7__["ModelPage"],
                cssClass: 'my-custom-modal-class'
            });
            return yield modal.present();
        });
    }
};
ContactGroupsPage.ctorParameters = () => [
    { type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"] },
    { type: src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_5__["ContactGroupsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] }
];
ContactGroupsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-contact-groups',
        template: _raw_loader_contact_groups_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_contact_groups_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ContactGroupsPage);



/***/ }),

/***/ "Z5qd":
/*!***************************************************************!*\
  !*** ./src/app/pages/contact-groups/contact-groups.module.ts ***!
  \***************************************************************/
/*! exports provided: ContactGroupsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactGroupsPageModule", function() { return ContactGroupsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _contact_groups_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contact-groups-routing.module */ "lISs");
/* harmony import */ var _contact_groups_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contact-groups.page */ "Qalu");
/* harmony import */ var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modules/shared/shared.module */ "FpXt");








let ContactGroupsPageModule = class ContactGroupsPageModule {
};
ContactGroupsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _contact_groups_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactGroupsPageRoutingModule"],
            src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_contact_groups_page__WEBPACK_IMPORTED_MODULE_6__["ContactGroupsPage"]]
    })
], ContactGroupsPageModule);



/***/ }),

/***/ "lISs":
/*!***********************************************************************!*\
  !*** ./src/app/pages/contact-groups/contact-groups-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: ContactGroupsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactGroupsPageRoutingModule", function() { return ContactGroupsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _contact_groups_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contact-groups.page */ "Qalu");




const routes = [
    {
        path: '',
        component: _contact_groups_page__WEBPACK_IMPORTED_MODULE_3__["ContactGroupsPage"]
    }
];
let ContactGroupsPageRoutingModule = class ContactGroupsPageRoutingModule {
};
ContactGroupsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactGroupsPageRoutingModule);



/***/ }),

/***/ "lY+8":
/*!***************************************************************!*\
  !*** ./src/app/pages/contact-groups/contact-groups.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb250YWN0LWdyb3Vwcy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "rtEV":
/*!****************************************************!*\
  !*** ./src/app/services/contact-groups.service.ts ***!
  \****************************************************/
/*! exports provided: ContactGroupsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactGroupsService", function() { return ContactGroupsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");




let ContactGroupsService = class ContactGroupsService {
    constructor(http) {
        this.http = http;
        this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    getContactGroups() {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'contactgroups.php');
            this.http.get(this.apiUrl + 'contactgroups.php')
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
    getContactGroupsList(formData) {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'contactgroupslist.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'contactgroupslist.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
    contactsImport(formData) {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'contactsimport.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'contactsimport.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
    getContacts(formData) {
        return new Promise((resolve, reject) => {
            // if (status == ConnectionStatus.Online) {
            console.log(this.apiUrl + 'contacts.php');
            formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
            this.http.post(this.apiUrl + 'contacts.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
            // }
        });
    }
};
ContactGroupsService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
ContactGroupsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], ContactGroupsService);



/***/ })

}]);
//# sourceMappingURL=pages-contact-groups-contact-groups-module-es2015.js.map