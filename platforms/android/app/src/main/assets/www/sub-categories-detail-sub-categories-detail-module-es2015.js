(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sub-categories-detail-sub-categories-detail-module"],{

/***/ "MKaY":
/*!*******************************************************************************************!*\
  !*** ./src/app/tab1/sub-categories/sub-categories-detail/sub-categories-detail.page.scss ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzdWItY2F0ZWdvcmllcy1kZXRhaWwucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "PJLp":
/*!*******************************************************************************************!*\
  !*** ./src/app/tab1/sub-categories/sub-categories-detail/sub-categories-detail.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: SubCategoriesDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubCategoriesDetailPageModule", function() { return SubCategoriesDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _sub_categories_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sub-categories-detail-routing.module */ "lXZj");
/* harmony import */ var _sub_categories_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sub-categories-detail.page */ "zNbp");
/* harmony import */ var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/modules/shared/shared.module */ "FpXt");








let SubCategoriesDetailPageModule = class SubCategoriesDetailPageModule {
};
SubCategoriesDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _sub_categories_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["SubCategoriesDetailPageRoutingModule"],
            src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_sub_categories_detail_page__WEBPACK_IMPORTED_MODULE_6__["SubCategoriesDetailPage"]]
    })
], SubCategoriesDetailPageModule);



/***/ }),

/***/ "VukA":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/sub-categories/sub-categories-detail/sub-categories-detail.page.html ***!
  \*********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header title=\"{{name}}\"></header>\n\n<ion-content>\n  <div class=\"bg-white\">\n    <div class=\"position-relative text-align-center h-100 w-100\" *ngIf=\"!loading && videos_data.length == 0\">\n      <h3>No Data Found</h3>\n    </div>\n\n    <div *ngFor=\"let video of videos_data\" class=\"quiz-categories\" (click)=openVideo(video.videotitle,video.videoimage,video.videourl,video.id)>\n      <!-- <h3>{{item.title}}</h3> -->\n      <img [src]=\"environment.imageUrl + video.videoimage\" class=\"img-container\">\n    </div>\n  </div>\n\n    <!-- ​REFRESHER -->\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"refreshData($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n\n  <!-- INFINITE SCROLL -->\n  <ion-infinite-scroll *ngIf=\"!loading && currentPage<lastPage\" threshold=\"100px\" (ionInfinite)=\"loadMoreData(1, $event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more items...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n  </div>\n</div>");

/***/ }),

/***/ "cxbk":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
const environment = {
    production: true,
    imageUrl: 'https:',
    apiUrl: 'https://mindandheartuniversity.com/admin/appapi/',
    profileImageUrl: 'https://mindandheartuniversity.com/',
    auth_token: '4d409766604ad82ba5eeb96077c977c9',
    downloadurl: 'https://mindandheartuniversity.com/admin/'
};


/***/ }),

/***/ "lXZj":
/*!***************************************************************************************************!*\
  !*** ./src/app/tab1/sub-categories/sub-categories-detail/sub-categories-detail-routing.module.ts ***!
  \***************************************************************************************************/
/*! exports provided: SubCategoriesDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubCategoriesDetailPageRoutingModule", function() { return SubCategoriesDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _sub_categories_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sub-categories-detail.page */ "zNbp");




const routes = [
    {
        path: '',
        component: _sub_categories_detail_page__WEBPACK_IMPORTED_MODULE_3__["SubCategoriesDetailPage"]
    }
];
let SubCategoriesDetailPageRoutingModule = class SubCategoriesDetailPageRoutingModule {
};
SubCategoriesDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SubCategoriesDetailPageRoutingModule);



/***/ }),

/***/ "zNbp":
/*!*****************************************************************************************!*\
  !*** ./src/app/tab1/sub-categories/sub-categories-detail/sub-categories-detail.page.ts ***!
  \*****************************************************************************************/
/*! exports provided: SubCategoriesDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubCategoriesDetailPage", function() { return SubCategoriesDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_sub_categories_detail_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./sub-categories-detail.page.html */ "VukA");
/* harmony import */ var _sub_categories_detail_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sub-categories-detail.page.scss */ "MKaY");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/loader.service */ "5dVO");
/* harmony import */ var src_app_services_sub_category_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/sub-category.service */ "ent6");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/environments/environment.prod */ "cxbk");









let SubCategoriesDetailPage = class SubCategoriesDetailPage {
    constructor(navCtrl, router, route, subCategoryService, loaderService) {
        this.navCtrl = navCtrl;
        this.router = router;
        this.route = route;
        this.subCategoryService = subCategoryService;
        this.loaderService = loaderService;
        this.name = '';
        this.currentPage = 1;
        this.lastPage = 1;
        this.loading = false;
        this.environment = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_8__["environment"];
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.name = this.router.getCurrentNavigation().extras.state.name;
                this.id = this.router.getCurrentNavigation().extras.state.id;
                this.currentPage = 1;
                this.lastPage = 1;
                this.videos_data = [];
                this.getSubCategory(this.currentPage, this.id);
            }
        });
    }
    getSubCategory(currentPage, c_id, event = null) {
        this.loaderService.showLoader();
        this.loading = true;
        let formData = new FormData();
        formData.append("PageNumber", currentPage);
        formData.append("categoryid", c_id);
        this.subCategoryService.getSubCategory(formData).then((res) => {
            console.log(res);
            if (res.status != false) {
                this.loading = false;
                this.currentPage = res.CurrentPage;
                this.lastPage = res.total;
                if (this.currentPage == 1) {
                    if (res.videocount > 0) {
                        this.videos_data = res.videos;
                    }
                    this.loaderService.hideLoader();
                }
                else {
                    if (res.videocount > 0) {
                        this.videos_data = [...this.videos_data, ...res.videos];
                    }
                    this.loaderService.hideLoader();
                }
                if (event) {
                    event.target.complete();
                }
            }
            else {
                this.loaderService.hideLoader();
                this.loading = false;
            }
        }, err => {
            this.loaderService.hideLoader();
            this.loading = false;
            if (event) {
                event.target.complete();
            }
        });
    }
    refreshData(event) {
        this.currentPage = 1;
        this.getSubCategory(1, event);
    }
    ConvertToInt(currentPage) {
        return parseInt(currentPage);
    }
    loadMoreData(value, event) {
        this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value);
        this.getSubCategory(this.currentPage, event);
    }
    openVideo(videotitle, videoimage, videourl, videoid) {
        let navigationExtras = {
            state: {
                videotitle: videotitle,
                videoimage: videoimage,
                videourl: videourl,
                videoid: videoid
            }
        };
        // this.router.navigate(['post-add-second', navigationExtras]);
        this.router.navigate(['tabs/tab1/sub-categories/video'], navigationExtras);
        // this.router.navigate(['testing'], navigationExtras);
    }
    ngOnInit() {
    }
};
SubCategoriesDetailPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: src_app_services_sub_category_service__WEBPACK_IMPORTED_MODULE_7__["SubCategoryService"] },
    { type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"] }
];
SubCategoriesDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-sub-categories-detail',
        template: _raw_loader_sub_categories_detail_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_sub_categories_detail_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SubCategoriesDetailPage);



/***/ })

}]);
//# sourceMappingURL=sub-categories-detail-sub-categories-detail-module-es2015.js.map