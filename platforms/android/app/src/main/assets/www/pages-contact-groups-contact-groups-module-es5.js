(function () {
  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-contact-groups-contact-groups-module"], {
    /***/
    "5dVO":
    /*!********************************************!*\
      !*** ./src/app/services/loader.service.ts ***!
      \********************************************/

    /*! exports provided: LoaderService */

    /***/
    function dVO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoaderService", function () {
        return LoaderService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var LoaderService = /*#__PURE__*/function () {
        function LoaderService(loadingCtrl) {
          _classCallCheck(this, LoaderService);

          this.loadingCtrl = loadingCtrl;
        }

        _createClass(LoaderService, [{
          key: "showLoader",
          value: function showLoader() {
            this.isBusy = true; // this.loaderToShow = this.loadingCtrl.create({
            //   message: 'Please Wait..'
            // }).then((res) => {
            //   res.present();
            //   // res.onDidDismiss().then((dis) => {
            //   //    console.log('Loading dismissed!',dis);
            //   // });
            // });
            // // this.hideLoader();
          }
        }, {
          key: "hideLoader",
          value: function hideLoader() {
            // setTimeout(()=>{
            //   this.loadingCtrl.dismiss();
            // },100)
            this.isBusy = false;
          }
        }]);

        return LoaderService;
      }();

      LoaderService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoaderService);
      /***/
    },

    /***/
    "9dCw":
    /*!*****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contact-groups/contact-groups.page.html ***!
      \*****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function dCw(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<header title=\"Contact Groups\"></header>\n\n<ion-content>\n  <ion-button  expand=\"full\" (click)=\"addContact()\">Add</ion-button>\n   <!-- Custom Table -->\n   <div class=\"bg-white\">\n    <!-- Header -->\n    <ion-grid>\n      <ion-row class=\"table-wrapper \">\n        <!-- <ion-item-divider> -->\n        <ion-col size=\"6\">\n          <ion-label>\n            <h5>Name</h5>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-label>\n            <h5>Phone / Email</h5>\n          </ion-label>\n        </ion-col>\n        \n      <!-- </ion-item-divider> -->\n\n      </ion-row>\n      <ion-row *ngFor=\"let item of contactsData; let i=index\">\n        <ion-col size=\"6\">\n          <ion-label>\n            <p *ngIf=\"item.firstname || item.lastname\" class=\"col_style\">{{item.firstname}} {{item.lastname}}</p>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-label>\n            <p *ngIf=\"item.phone\" class=\"d-block\">{{item.phone}}</p>\n            <p *ngIf=\"item.email != null || item.email != ''\" class=\"d-block\">{{item.email}}</p>\n          </ion-label>\n        </ion-col>\n        <hr>\n      </ion-row>\n\n    </ion-grid>\n    <!-- No Data -->\n    <h4 class=\"clr-medium ion-margin-vertical ion-text-center\" *ngIf=\"isCheck == true\">\n      No Record Found\n    </h4>\n  </div>\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>";
      /***/
    },

    /***/
    "Qalu":
    /*!*************************************************************!*\
      !*** ./src/app/pages/contact-groups/contact-groups.page.ts ***!
      \*************************************************************/

    /*! exports provided: ContactGroupsPage */

    /***/
    function Qalu(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactGroupsPage", function () {
        return ContactGroupsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_contact_groups_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./contact-groups.page.html */
      "9dCw");
      /* harmony import */


      var _contact_groups_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./contact-groups.page.scss */
      "lY+8");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/contact-groups.service */
      "rtEV");
      /* harmony import */


      var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/loader.service */
      "5dVO");
      /* harmony import */


      var _model_model_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../model/model.page */
      "9AS+");

      var ContactGroupsPage = /*#__PURE__*/function () {
        function ContactGroupsPage(loaderService, contactGroupsService, modalController) {
          _classCallCheck(this, ContactGroupsPage);

          this.loaderService = loaderService;
          this.contactGroupsService = contactGroupsService;
          this.modalController = modalController;
          this.currentPage = 1;
          this.lastPage = 1;
          this.isCheck = false;
        }

        _createClass(ContactGroupsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getContactGroups(this.currentPage);
          }
        }, {
          key: "getContactGroups",
          value: function getContactGroups(currentPage) {
            var _this = this;

            var event = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            this.loaderService.showLoader();
            var formData = new FormData();
            formData.append("PageNumber", currentPage);
            formData.append("aid", localStorage.getItem('user_id'));
            this.contactGroupsService.getContactGroupsList(formData).then(function (data) {
              console.log("data : ", data);
              _this.dataObj = data;
              _this.currentPage = _this.dataObj.CurrentPage;
              _this.lastPage = _this.dataObj.total;

              if (_this.currentPage == 1) {
                _this.contactsGroupsData = _this.dataObj.contactgroups;

                if (_this.contactsGroupsData.length == 0) {
                  _this.isCheck = true;
                }
              } else {
                _this.contactsGroupsData = [].concat(_toConsumableArray(_this.contactsGroupsData), _toConsumableArray(_this.dataObj.contactgroups));
              }

              _this.loaderService.hideLoader();

              if (event) {
                event.target.complete();
              }
            }, function (err) {
              _this.loaderService.hideLoader();

              _this.isCheck = true;

              if (event) {
                event.target.complete();
              }
            });
          }
        }, {
          key: "addContact",
          value: function addContact() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var modal;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.modalController.create({
                        component: _model_model_page__WEBPACK_IMPORTED_MODULE_7__["ModelPage"],
                        cssClass: 'my-custom-modal-class'
                      });

                    case 2:
                      modal = _context.sent;
                      _context.next = 5;
                      return modal.present();

                    case 5:
                      return _context.abrupt("return", _context.sent);

                    case 6:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return ContactGroupsPage;
      }();

      ContactGroupsPage.ctorParameters = function () {
        return [{
          type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_6__["LoaderService"]
        }, {
          type: src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_5__["ContactGroupsService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }];
      };

      ContactGroupsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-contact-groups',
        template: _raw_loader_contact_groups_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_contact_groups_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], ContactGroupsPage);
      /***/
    },

    /***/
    "Z5qd":
    /*!***************************************************************!*\
      !*** ./src/app/pages/contact-groups/contact-groups.module.ts ***!
      \***************************************************************/

    /*! exports provided: ContactGroupsPageModule */

    /***/
    function Z5qd(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactGroupsPageModule", function () {
        return ContactGroupsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _contact_groups_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./contact-groups-routing.module */
      "lISs");
      /* harmony import */


      var _contact_groups_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./contact-groups.page */
      "Qalu");
      /* harmony import */


      var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/modules/shared/shared.module */
      "FpXt");

      var ContactGroupsPageModule = function ContactGroupsPageModule() {
        _classCallCheck(this, ContactGroupsPageModule);
      };

      ContactGroupsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _contact_groups_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactGroupsPageRoutingModule"], src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_contact_groups_page__WEBPACK_IMPORTED_MODULE_6__["ContactGroupsPage"]]
      })], ContactGroupsPageModule);
      /***/
    },

    /***/
    "lISs":
    /*!***********************************************************************!*\
      !*** ./src/app/pages/contact-groups/contact-groups-routing.module.ts ***!
      \***********************************************************************/

    /*! exports provided: ContactGroupsPageRoutingModule */

    /***/
    function lISs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactGroupsPageRoutingModule", function () {
        return ContactGroupsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _contact_groups_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./contact-groups.page */
      "Qalu");

      var routes = [{
        path: '',
        component: _contact_groups_page__WEBPACK_IMPORTED_MODULE_3__["ContactGroupsPage"]
      }];

      var ContactGroupsPageRoutingModule = function ContactGroupsPageRoutingModule() {
        _classCallCheck(this, ContactGroupsPageRoutingModule);
      };

      ContactGroupsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ContactGroupsPageRoutingModule);
      /***/
    },

    /***/
    "lY+8":
    /*!***************************************************************!*\
      !*** ./src/app/pages/contact-groups/contact-groups.page.scss ***!
      \***************************************************************/

    /*! exports provided: default */

    /***/
    function lY8(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb250YWN0LWdyb3Vwcy5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "rtEV":
    /*!****************************************************!*\
      !*** ./src/app/services/contact-groups.service.ts ***!
      \****************************************************/

    /*! exports provided: ContactGroupsService */

    /***/
    function rtEV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactGroupsService", function () {
        return ContactGroupsService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");

      var ContactGroupsService = /*#__PURE__*/function () {
        function ContactGroupsService(http) {
          _classCallCheck(this, ContactGroupsService);

          this.http = http;
          this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
        }

        _createClass(ContactGroupsService, [{
          key: "getContactGroups",
          value: function getContactGroups() {
            var _this2 = this;

            return new Promise(function (resolve, reject) {
              // if (status == ConnectionStatus.Online) {
              console.log(_this2.apiUrl + 'contactgroups.php');

              _this2.http.get(_this2.apiUrl + 'contactgroups.php').subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              }); // }

            });
          }
        }, {
          key: "getContactGroupsList",
          value: function getContactGroupsList(formData) {
            var _this3 = this;

            return new Promise(function (resolve, reject) {
              // if (status == ConnectionStatus.Online) {
              console.log(_this3.apiUrl + 'contactgroupslist.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this3.http.post(_this3.apiUrl + 'contactgroupslist.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              }); // }

            });
          }
        }, {
          key: "contactsImport",
          value: function contactsImport(formData) {
            var _this4 = this;

            return new Promise(function (resolve, reject) {
              // if (status == ConnectionStatus.Online) {
              console.log(_this4.apiUrl + 'contactsimport.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this4.http.post(_this4.apiUrl + 'contactsimport.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              }); // }

            });
          }
        }, {
          key: "getContacts",
          value: function getContacts(formData) {
            var _this5 = this;

            return new Promise(function (resolve, reject) {
              // if (status == ConnectionStatus.Online) {
              console.log(_this5.apiUrl + 'contacts.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this5.http.post(_this5.apiUrl + 'contacts.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              }); // }

            });
          }
        }]);

        return ContactGroupsService;
      }();

      ContactGroupsService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      };

      ContactGroupsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], ContactGroupsService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-contact-groups-contact-groups-module-es5.js.map