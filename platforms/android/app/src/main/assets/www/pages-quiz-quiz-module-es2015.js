(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-quiz-quiz-module"],{

/***/ "8qqv":
/*!*******************************************!*\
  !*** ./src/app/pages/quiz/quiz.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".f-27 {\n  font-size: 27px;\n}\n\n.timer {\n  display: flex;\n  align-items: center;\n}\n\n.timer-text {\n  transform: rotate(90deg);\n  transform-origin: center;\n  font-size: 36px;\n  text-anchor: middle;\n  font-weight: 600;\n  fill: #333;\n}\n\n#progress-circle {\n  margin-top: 50px;\n  transform: rotate(-90deg);\n}\n\n.progress-wrapper {\n  position: relative;\n  margin: 20px auto;\n  font-size: 21px;\n}\n\n.progress {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  line-height: 9em;\n  font-size: 1em;\n  text-align: center;\n  font-weight: 100;\n}\n\n.flex-container {\n  display: flex;\n  flex-direction: row;\n  height: 100%;\n  align-items: center;\n  justify-content: center;\n  overflow: hidden;\n}\n\n@media screen and (max-width: 600px) {\n  .flex-container {\n    flex-wrap: wrap;\n  }\n  .flex-container .progress {\n    height: unset;\n  }\n}\n\n.flex-container .progress {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3F1aXoucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtBQUNKOztBQUNBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FBRUo7O0FBQUE7RUFDSSx3QkFBQTtFQUNBLHdCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0FBR0o7O0FBREE7RUFDSSxnQkFBQTtFQUNBLHlCQUFBO0FBSUo7O0FBRkE7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQUtKOztBQUZFO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUFLSjs7QUFIRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7QUFNSjs7QUFKSTtFQUNFO0lBQ0UsZUFBQTtFQU1OO0VBSkk7SUFDRSxhQUFBO0VBTU47QUFDRjs7QUFISTtFQUNFLFlBQUE7QUFLTiIsImZpbGUiOiJxdWl6LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mLTI3IHtcbiAgICBmb250LXNpemU6IDI3cHg7XG59XG4udGltZXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi50aW1lci10ZXh0IHtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG4gICAgdHJhbnNmb3JtLW9yaWdpbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMzZweDtcbiAgICB0ZXh0LWFuY2hvcjogbWlkZGxlO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZmlsbDogIzMzMztcbn1cbiNwcm9ncmVzcy1jaXJjbGUge1xuICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoLTkwZGVnKTtcbn1cbi5wcm9ncmVzcy13cmFwcGVyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luOiAyMHB4IGF1dG87XG4gICAgZm9udC1zaXplOiAyMXB4O1xuICB9XG4gIFxuICAucHJvZ3Jlc3Mge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgbGluZS1oZWlnaHQ6IDllbTtcbiAgICBmb250LXNpemU6IDFlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcbiAgfVxuICAuZmxleC1jb250YWluZXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIFxuICAgIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gICAgICAmIHtcbiAgICAgICAgZmxleC13cmFwOiB3cmFwO1xuICAgICAgfVxuICAgICAgLnByb2dyZXNzIHtcbiAgICAgICAgaGVpZ2h0OiB1bnNldDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAucHJvZ3Jlc3Mge1xuICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbiAgfSJdfQ== */");

/***/ }),

/***/ "CQ+c":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/quiz/quiz.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back(0)\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">Quiz Title</ion-title>\n    <div slot=\"end\" class=\"mr-5 timer\">\n      <ion-icon class=\"f-27 mr-5\" color=\"secondary\" name=\"time\"></ion-icon>\n      <ion-text color=\"secondary\">{{ time | async}}</ion-text>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <!-- <ion-grid *ngIf=\"!start_quiz\">\n    <ion-row class=\"ion-padding-\">\n      <ion-col size=\"12\">\n        <h2 class=\"ion-no-margin\">{{data?.title}}</h2>\n      </ion-col>\n      <ion-col size=\"12\">\n        <p class=\"ion-no-margin-\">{{data?.description}}</p>\n      </ion-col>\n      <ion-col size=\"12\">\n        <h4 class=\"ion-no-margin\">Total Question {{data?.maxquestions}}</h4>\n      </ion-col>\n      <ion-col size=\"12\">\n        <h4 class=\"ion-no-margin\">Quiz Time {{data?.quiztime}}</h4>\n      </ion-col>\n      <ion-col size=\"12\">\n        <h4 class=\"ion-no-margin\">Pass Score {{data?.passcore}}</h4>\n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-button (click)=\"startQuiz()\">\n          Start Quiz\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid> -->\n  <!-- <ion-slides #slides>\n    <ion-slide> -->\n      <ion-row>\n        <ion-col size=\"12\">\n          <h3 class=\"ion-text-center\">Question: {{questions?.questionid}}</h3>\n        </ion-col>\n        <ion-col size=\"12\">\n          <h3> {{questions?.questiontext}}</h3>\n        </ion-col>\n        <ion-col size=\"12\" *ngIf=\"questions?.type == 'mcqs'\">\n          <ion-list>\n            <ion-radio-group mode=\"ios\">\n              <ion-item *ngFor=\"let answer of questions.answers; let i = index;\">\n                <ion-label>{{i+1}}. {{answer.answertext}}</ion-label>\n                <ion-radio (click)=\"selectAnswer(answer, questions)\" [disabled]=\"hasAnswered\"></ion-radio>\n              </ion-item>\n            </ion-radio-group>\n          </ion-list>\n        </ion-col>\n        <ion-col size=\"12\" *ngIf=\"questions?.type == 'text'\">\n          <ion-item>\n            <!-- <ion-label>Notes</ion-label> -->\n            <ion-textarea rows=\"6\" cols=\"20\" placeholder=\"Write your answer here...\"></ion-textarea>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    <!-- </ion-slide>\n  </ion-slides> -->\n  <div class=\"ion-text-end\" *ngIf=\"questions\">\n    <ion-button (click)=\"nextSlide()\">\n      Next Question\n    </ion-button>\n  </div>\n</ion-content>");

/***/ }),

/***/ "EnSQ":
/*!******************************************!*\
  !*** ./src/app/services/data.service.ts ***!
  \******************************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");



let DataService = class DataService {
    constructor(http) {
        this.http = http;
    }
    load() {
        if (this.data) {
            return Promise.resolve(this.data);
        }
        return new Promise(resolve => {
            this.http.get('assets/data/questions.json').subscribe(data => {
                resolve(data);
            });
        });
    }
};
DataService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
DataService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], DataService);



/***/ }),

/***/ "F2WD":
/*!*******************************************!*\
  !*** ./src/app/pages/quiz/quiz.module.ts ***!
  \*******************************************/
/*! exports provided: QuizPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuizPageModule", function() { return QuizPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _quiz_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./quiz-routing.module */ "fVPh");
/* harmony import */ var _quiz_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./quiz.page */ "naDh");







let QuizPageModule = class QuizPageModule {
};
QuizPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _quiz_routing_module__WEBPACK_IMPORTED_MODULE_5__["QuizPageRoutingModule"]
        ],
        declarations: [_quiz_page__WEBPACK_IMPORTED_MODULE_6__["QuizPage"]]
    })
], QuizPageModule);



/***/ }),

/***/ "fVPh":
/*!***************************************************!*\
  !*** ./src/app/pages/quiz/quiz-routing.module.ts ***!
  \***************************************************/
/*! exports provided: QuizPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuizPageRoutingModule", function() { return QuizPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _quiz_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./quiz.page */ "naDh");




const routes = [
    {
        path: '',
        component: _quiz_page__WEBPACK_IMPORTED_MODULE_3__["QuizPage"]
    }
];
let QuizPageRoutingModule = class QuizPageRoutingModule {
};
QuizPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], QuizPageRoutingModule);



/***/ }),

/***/ "naDh":
/*!*****************************************!*\
  !*** ./src/app/pages/quiz/quiz.page.ts ***!
  \*****************************************/
/*! exports provided: QuizPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuizPage", function() { return QuizPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_quiz_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./quiz.page.html */ "CQ+c");
/* harmony import */ var _quiz_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./quiz.page.scss */ "8qqv");
/* harmony import */ var _success_model_success_model_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../success-model/success-model.page */ "hnmG");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_quiz_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/quiz.service */ "ofzi");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/data.service */ "EnSQ");










const circleR = 80;
const circleDasharray = 2 * Math.PI * circleR;
let QuizPage = class QuizPage {
    constructor(navCtrl, alertController, route, quizService, router, modalController, dataService, changeDetector) {
        this.navCtrl = navCtrl;
        this.alertController = alertController;
        this.route = route;
        this.quizService = quizService;
        this.router = router;
        this.modalController = modalController;
        this.dataService = dataService;
        this.changeDetector = changeDetector;
        this.hasAnswered = false;
        this.score = 0;
        this.start_timer = false;
        this.circleR = circleR;
        this.circleDasharray = circleDasharray;
        this.startDuration = 5;
        this.time = new rxjs__WEBPACK_IMPORTED_MODULE_8__["BehaviorSubject"]('00:00');
        this.percent = new rxjs__WEBPACK_IMPORTED_MODULE_8__["BehaviorSubject"](100);
        this.state = 'stop';
        this.start_quiz = false;
        this.questionid = 0;
        this.answerid = 0;
        this.attemptid = 0;
        // this.dataService.load().then((data:any) => {
        //   this.questions = data.questions;
        //   this.nextSlide()
        // });
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.quizid = this.router.getCurrentNavigation().extras.state.quizids;
                this.quiztime = this.router.getCurrentNavigation().extras.state.quiztimes;
                console.log("videoimage after : ", this.quizid);
                console.log("quiztime after : ", this.quiztime);
                this.performquiz();
            }
        });
        // this.startQuiz();
    }
    performquiz() {
        let formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("quizid", this.quizid);
        if (this.questionid != 0) {
            formData.append("questionid", this.questionid);
            formData.append("answerid", this.answerid);
            formData.append("attemptid", this.attemptid);
        }
        this.quizService.performquiz(formData).then((data) => {
            if (data.status) {
                this.questions = data;
                // this.showAlert(this.data)
                // this.nextSlide();
                this.startTimer(this.quiztime);
            }
            else {
                this.openGifModel(data.message);
                // this.alertService.presentAlertError(data.message);
            }
        });
    }
    openGifModel(data) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _success_model_success_model_page__WEBPACK_IMPORTED_MODULE_3__["SuccessModelPage"],
                componentProps: {
                    value: data
                }
            });
            modal.onDidDismiss().then((data) => {
                console.log(data);
                this.navCtrl.back();
            });
            yield modal.present();
        });
    }
    startQuiz() {
        this.start_quiz = true;
        this.dataService.load().then((data) => {
            this.questions = data.questions;
            this.nextSlide();
            this.startTimer(this.startDuration);
        });
    }
    startTimer(duration) {
        this.state = 'start';
        clearInterval(this.interval);
        this.timer = duration * 60;
        // this.updateTimeValue();
        this.interval = setInterval(() => {
            this.updateTimeValue();
        }, 1000);
    }
    stopTimer() {
        clearInterval(this.interval);
        this.time.next('00:00');
        this.state = 'stop';
    }
    updateTimeValue() {
        let minutes = this.timer / 60;
        let seconds = this.timer % 60;
        minutes = String('0' + Math.floor(minutes)).slice(-2);
        seconds = String('0' + Math.floor(seconds)).slice(-2);
        const text = minutes + ':' + seconds;
        this.time.next(text);
        const totalTime = this.startDuration * 5;
        const percentage = ((totalTime - this.timer) / totalTime) * 100;
        this.percent.next(percentage);
        --this.timer;
        if (this.timer < 0) {
            // this.startTimer(this.startDuration);
            this.stopTimer();
        }
    }
    ionViewDidLoad() {
        this.slides.lockSwipes(true);
    }
    nextSlide() {
        this.start_timer = true;
        this.performquiz();
        // this.startTimer(this.startDuration)
        // this.slides.lockSwipes(false);
        // this.slides.slideNext();
        // this.slides.lockSwipes(true);
    }
    selectAnswer(answer, question) {
        console.log("answer : ", answer);
        console.log("question : ", question);
        this.questionid = answer.id;
        this.answerid = question.questionid;
        this.attemptid = question.attemptid;
        // this.hasAnswered = true;
        // answer.selected = true;
        // question.flashCardFlipped = true;
        // if (answer.correct) {
        //   this.score++;
        // }
        // setTimeout(() => {
        // this.hasAnswered = false;
        // // this.nextSlide();
        // answer.selected = false;
        // question.flashCardFlipped = false;
        // }, 1000);
    }
    ngOnInit() {
        // this.route.queryParams.subscribe(params => {
        //   // if (params) {
        //   //   console.log(params)
        //   //   // let queryParams = JSON.parse(params);
        //   //   // console.log(queryParams)
        //   // }
        //   if (this.router.getCurrentNavigation().extras.state) {
        //     this.quizid = this.router.getCurrentNavigation().extras.state.quizid;
        //     console.log("quizid : ", this.quizid)
        //     if(this.quizid){
        //       this.quizdetails()
        //     }
        //   }
        // });
        // this.questionsForm = this.formBuilder.group({
        //   id: null,
        //   questionsArray: this.formBuilder.array([])
        // });
        // this.questionsArray = this.questionsForm.get('questionsArray') as FormArray;
    }
    // startCountDown() {
    //   this.countDownInterval = setInterval(() => {
    //     this.counter--;
    //     if (this.counter === 0) {
    //       clearInterval(this.countDownInterval);
    //       this.submit(true);
    //     }
    //     this.changeDetector.detectChanges();
    //   }, 1000);
    // }
    // transform(time: number): string {
    //   if (time >= 60) {
    //     const timeInMinutes: number = Math.floor(time / 60);
    //     const hours: number = Math.floor(time / 3600);
    //     let minutes = timeInMinutes;
    //     if (minutes >= 60) {
    //       minutes = timeInMinutes % 60;
    //     }
    //     return (hours > 0 ? '0' + hours + ': ' : '')
    //       + (minutes < 10 ? '0' + minutes : minutes) +
    //       ': ' + ((time - timeInMinutes * 60) < 10 ? '0' +
    //         (time - timeInMinutes * 60) : (time - timeInMinutes * 60));
    //   } else {
    //     return (time < 10 ? '00: 0' + time : '00:' + time) + '';
    //   }
    // }
    // initializeForm() {
    //   this.questions.forEach(element => {
    //     this.questionsArray.push(this.questionGroup());
    //   });
    // }
    // questionGroup(): FormGroup {
    //   return this.formBuilder.group({
    //     questionanswerid: [null, Validators.required],
    //     points: [null]
    //   });
    // }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // this.counter = 0;
            // this.timer = 3;
            // this.selectedQuestion = 0;
            // this.questions = [];
            // this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
            yield this.showLoader();
        });
    }
    // getAllQuizes() {
    //   let formData = new FormData();
    //   formData.append("aid", localStorage.getItem('user_id'));
    //   formData.append("quizid", '1');
    //   this.quizService.getAllQuizes(formData).then((res: any) => {
    //     console.log("getAllQuizes : ", res);
    //     // if (res.quizcount > 0) {
    //     //   this.take_quiz = false
    //     // }
    //     // if (res.downloadcount > 0) {
    //     //   this.pdf = false
    //     //   this.pdfFile = environment.downloadurl + res.downloadlink;
    //     //   console.log("this.pdfFile : ", this.pdfFile)
    //     // }
    //   });
    // }
    // fetchQuizQuestions() {
    //   let formData = new FormData();
    //   formData.append("aid", localStorage.getItem('user_id'));
    //   formData.append("quizid", '1');
    //   this.quizService.getAllQuizes(formData).then((res: any) => {
    //     this.counter = res.quiztime;
    //     this.questions = res.quiz_questions;
    //     this.totalpoints = res.totalscore;
    //     if (this.counter && this.questions.length) {
    //       this.saveQuizAttempt();
    //     } else {
    //       this.loading = false;
    //     }
    //   },
    //   err => {
    //     this.loading = false;
    //   });
    //   // this.quizService.getAllQuizes({
    //   //   quizid: this.id,
    //   //   memberid: localStorage.getItem('user_id'),
    //   // })
    //   //   .then((res: any) => {
    //   //     this.counter = res.data[0].quiztime;
    //   //     this.questions = res.data[0].quiz_questions;
    //   //     this.totalpoints = res.data[0].totalscore;
    //   //     if (this.counter && this.questions.length) {
    //   //       this.saveQuizAttempt();
    //   //     } else {
    //   //       this.loading = false;
    //   //     }
    //   //   },
    //   //     err => {
    //   //       this.loading = false;
    //   //     });
    // }
    ionViewWillLeave() {
        if (this.alertObj) {
            this.alertObj.dismiss();
        }
        this.changeDetector.detectChanges();
    }
    // startTimer2() {
    //   this.timer = 3;
    //   this.interval = setInterval(() => {
    //     this.timer--;
    //     this.loaderObj.message = `Staring in ${this.timer} seconds`;
    //     if (this.timer === 0) {
    //       clearInterval(this.interval);
    //       this.hideLoader();
    //       this.fetchQuizQuestions();
    //     }
    //   }, 1000);
    // }
    // saveQuizAttempt() {
    //   const obj = {
    //     memberid: localStorage.getItem('user_id'),
    //     quizid: this.id,
    //     // totalpoints: this.totalpoints
    //   };
    //   this.quizService.saveQuizAttempt(obj)
    //     .then((res: any) => {
    //       this.loading = false;
    //       this.attemptid = res.attemptid;
    //       this.initializeForm();
    //       this.startCountDown();
    //       this.changeDetector.detectChanges();
    //     },
    //       err => {
    //         this.loading = false;
    //       });
    // }
    showLoader() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // await this.loadingCtrl.create({
            //   spinner: 'circles',
            //   animated: true,
            //   showBackdrop: true,
            //   translucent: true,
            //   mode: 'ios',
            //   message: `Starting in ${this.timer} seconds`
            // }).then((load: HTMLIonLoadingElement) => {
            //   this.loaderObj = load;
            //   this.loaderObj.present().then(() => {
            //     this.hideLoader();
            //     // this.startTimer2();
            //   });
            // });
        });
    }
    hideLoader() {
        // this.loaderObj.dismiss();
    }
    back(check = 1) {
        if (check) {
            // this.navCtrl.navigateBack(['home-results/quiz']);
            this.navCtrl.back();
        }
        else {
            this.backConfirm();
        }
    }
    backConfirm(header = 'Confirmation') {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.alertObj = yield this.alertController.create({
                header: header,
                message: `
        <p> Are you sure you want to leave?</p>
      `,
                buttons: [
                    {
                        text: 'No',
                        role: 'cancel'
                    },
                    {
                        text: 'Yes',
                        handler: () => {
                            this.back();
                        }
                    }
                ]
            });
            this.alertObj.present();
        });
    }
};
QuizPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: src_app_services_quiz_service__WEBPACK_IMPORTED_MODULE_7__["QuizService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
    { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_9__["DataService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_6__["ChangeDetectorRef"] }
];
QuizPage.propDecorators = {
    slides: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__["ViewChild"], args: ['slides',] }]
};
QuizPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["Component"])({
        selector: 'app-quiz',
        template: _raw_loader_quiz_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_quiz_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], QuizPage);



/***/ })

}]);
//# sourceMappingURL=pages-quiz-quiz-module-es2015.js.map