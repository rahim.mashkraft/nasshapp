(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab4-tab4-module"],{

/***/ "1GDv":
/*!*************************************!*\
  !*** ./src/app/tab4/tab4.module.ts ***!
  \*************************************/
/*! exports provided: Tab4PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab4PageModule", function() { return Tab4PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _tab4_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tab4-routing.module */ "dkQB");
/* harmony import */ var _tab4_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab4.page */ "Wxre");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../modules/shared/shared.module */ "FpXt");








let Tab4PageModule = class Tab4PageModule {
};
Tab4PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _tab4_routing_module__WEBPACK_IMPORTED_MODULE_5__["Tab4PageRoutingModule"],
            _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_tab4_page__WEBPACK_IMPORTED_MODULE_6__["Tab4Page"]]
    })
], Tab4PageModule);



/***/ }),

/***/ "5dVO":
/*!********************************************!*\
  !*** ./src/app/services/loader.service.ts ***!
  \********************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let LoaderService = class LoaderService {
    constructor(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    showLoader() {
        this.isBusy = true;
        // this.loaderToShow = this.loadingCtrl.create({
        //   message: 'Please Wait..'
        // }).then((res) => {
        //   res.present();
        //   // res.onDidDismiss().then((dis) => {
        //   //    console.log('Loading dismissed!',dis);
        //   // });
        // });
        // // this.hideLoader();
    }
    hideLoader() {
        // setTimeout(()=>{
        //   this.loadingCtrl.dismiss();
        // },100)
        this.isBusy = false;
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ "Wxre":
/*!***********************************!*\
  !*** ./src/app/tab4/tab4.page.ts ***!
  \***********************************/
/*! exports provided: Tab4Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab4Page", function() { return Tab4Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_tab4_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./tab4.page.html */ "kfDu");
/* harmony import */ var _tab4_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tab4.page.scss */ "vacZ");
/* harmony import */ var _pages_iframe_iframe_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../pages/iframe/iframe.page */ "tKxX");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../services/chat.service */ "sjK5");
/* harmony import */ var _pages_twillio_model_twillio_model_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../pages/twillio-model/twillio-model.page */ "bHg0");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/loader.service */ "5dVO");
/* harmony import */ var _services_events_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/events.service */ "riPR");











let Tab4Page = class Tab4Page {
    constructor(modalController, router, chatService, loaderService, eventsService) {
        this.modalController = modalController;
        this.router = router;
        this.chatService = chatService;
        this.loaderService = loaderService;
        this.eventsService = eventsService;
        this.imageArray = [
            {
                imageUrl: '',
                title: ''
            },
            {
                imageUrl: '',
                title: ''
            },
            {
                imageUrl: '',
                title: ''
            },
            {
                imageUrl: '',
                title: ''
            },
        ];
        this.currentPage = 1;
        this.avatar_url = "../assets/user-placeholder.png";
        this.sortBy = 'n';
        this.eventsService.subscribe('notification_receive', (data) => {
            this.currentPage = 1;
            this.smschatcheckEvent(this.currentPage);
        });
    }
    ;
    smschatcheckEvent(currentPage) {
        let formData = new FormData();
        formData.append("aid", "531");
        formData.append("PageNumber", currentPage);
        this.chatService.smschatcheck(formData).then((data) => {
            console.log("smschatcheckEvent : ", data);
            if (data.status) {
                this.isCheck = true;
                this.chatList = data.contacts;
                this.affiliatenumber = data.affiliatenumber;
            }
        });
    }
    setFilteredItems(ev, currentPage) {
        console.log(currentPage);
        const val = ev.target.value;
        console.log(val);
        var str = new String(val);
        var len = str.length;
        // if the value is an empty string don't filter the items
        if (len >= 3 && val && val.trim() !== '') {
            if (this.sortBy == 'n') {
                this.smschatcheck(this.currentPage, this.search_value, '');
            }
            else {
                this.smschatcheck(this.currentPage, this.search_value, this.sortBy);
            }
        }
    }
    claerSearcher() {
        console.log("claerSearcher");
        // this.contactsData = [];
        this.currentPage = 1;
        this.smschatcheck(this.currentPage, '', this.sortBy);
    }
    sortFunction(sortBy) {
        console.log(sortBy);
        if (sortBy == 'n') {
            this.smschatcheck(this.currentPage, this.search_value, '');
        }
        else {
            this.smschatcheck(this.currentPage, this.search_value, this.sortBy);
        }
    }
    ionViewWillEnter() {
        this.imageArray = [
            {
                imageUrl: '',
                title: ''
            },
            {
                imageUrl: '',
                title: ''
            },
            {
                imageUrl: '',
                title: ''
            },
            {
                imageUrl: '',
                title: ''
            },
        ];
        this.smschatcheck(this.currentPage, '', '');
    }
    ngOnInit() {
    }
    smschatcheck(currentPage, search_value, sort_value, event = null) {
        this.loaderService.showLoader();
        let formData = new FormData();
        // formData.append("aid", localStorage.getItem('user_id'));
        formData.append("aid", "531");
        formData.append("PageNumber", currentPage);
        if (search_value) {
            formData.append("searchstring", search_value);
        }
        if (sort_value) {
            formData.append("sort", sort_value);
        }
        this.chatService.smschatcheck(formData).then((data) => {
            console.log("data : ", data);
            if (data.status) {
                this.isCheck = true;
                this.chatList = data.contacts;
                this.affiliatenumber = data.affiliatenumber;
                this.loaderService.hideLoader();
            }
            else {
                this.imageArray[0].imageUrl = data === null || data === void 0 ? void 0 : data.image1;
                this.imageArray[0].title = 'Login to twilio';
                this.imageArray[1].imageUrl = data === null || data === void 0 ? void 0 : data.image2;
                this.imageArray[1].title = 'Click on PhoneNumbers';
                this.imageArray[2].imageUrl = data.image3;
                this.imageArray[2].title = 'Click on your Twilio phone number';
                this.imageArray[3].imageUrl = data.image4;
                this.imageArray[3].title = 'Enter this url into field labeled "A MESSAGE COMES IN":';
                this.videoLink = data.videolink;
                this.isCheck = false;
                this.loaderService.hideLoader();
                console.log("imageArray : ", this.imageArray);
                // this.twillioModel(this.imageArray, this.videoLink);
            }
            if (event) {
                event.target.complete();
            }
        }, err => {
            this.isCheck = false;
            this.loaderService.hideLoader();
            if (event) {
                event.target.complete();
            }
        });
    }
    twillioModel(imageArray, videoLink) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _pages_twillio_model_twillio_model_page__WEBPACK_IMPORTED_MODULE_5__["TwillioModelPage"],
                componentProps: {
                    imageArray: imageArray,
                    videoLink: videoLink
                }
            });
            modal.onDidDismiss().then((data) => {
                console.log(data);
            });
            yield modal.present();
        });
    }
    goToChat(contactnumber, firstname, lastname) {
        let navigationExtras = {
            state: {
                affiliatenumber: this.affiliatenumber,
                contactnumber: contactnumber,
                firstname: firstname,
                lastname: lastname
            }
        };
        this.router.navigate(['chat'], navigationExtras);
    }
    refreshDataList(event) {
        this.currentPage = 1;
        this.smschatcheck(this.currentPage, '', '', event);
    }
    //////////////////////////// TWILIO  /////////////////////////
    playVideo() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // HKVideoPlayer.play(this.videoLink);
            const modal = yield this.modalController.create({
                component: _pages_iframe_iframe_page__WEBPACK_IMPORTED_MODULE_3__["IframePage"],
                componentProps: {
                    videoLink: this.videoLink,
                }
            });
            modal.onDidDismiss().then((data) => {
                console.log(data);
            });
            yield modal.present();
        });
    }
    countLength(value) {
        const tempArray = value.toString();
        const length = tempArray.length;
        return length;
    }
    validation() {
        if (this.account_SID && this.auth_Token && this.twilio_Number) {
        }
    }
    update() {
        console.log(this.account_SID);
        console.log(this.auth_Token);
        console.log(this.twilio_Number);
        if (this.account_SID && this.auth_Token && this.twilio_Number) {
            if (this.countLength(this.twilio_Number) < 10) {
                alert("plase add min or max 10 digits Twilio number");
            }
            else {
                this.updatetwilio();
            }
        }
        else {
            alert("Please fill all fields");
        }
    }
    updatetwilio() {
        this.loaderService.showLoader();
        let formData = new FormData();
        formData.append("aid", localStorage.getItem('user_id'));
        formData.append("accountsid", this.account_SID);
        formData.append("authtoken", this.auth_Token);
        // formData.append("fromnumber", "+1" + this.twilio_Number);
        formData.append("fromnumber", this.twilio_Number);
        this.chatService.updatetwilio(formData).then((data) => {
            console.log("data : ", data);
            if (data.status) {
                this.loaderService.hideLoader();
            }
            else {
                this.loaderService.hideLoader();
            }
        }, err => {
            this.loaderService.hideLoader();
        });
    }
    onKeyUp(event) {
        let newValue = event.target.value;
        // console.log(event.target.value);
        let regExp = new RegExp('^([0-9_\\-]+)$');
        if (!regExp.test(newValue)) {
            event.target.value = newValue.slice(11, -1);
        }
    }
    openImage() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // const modal = await this.modalController.create({
            //   component: ImageModelPage,
            //   componentProps: {
            //     imageArray: this.imageArray,
            //   }
            // })
            // modal.onDidDismiss().then((data) => {
            //   console.log(data);
            // })
            // await modal.present();
            let navigationExtras = {
                state: {
                    imageArray: this.imageArray
                }
            };
            this.router.navigate(['image-model'], navigationExtras);
        });
    }
};
Tab4Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _services_chat_service__WEBPACK_IMPORTED_MODULE_4__["ChatService"] },
    { type: _services_loader_service__WEBPACK_IMPORTED_MODULE_9__["LoaderService"] },
    { type: _services_events_service__WEBPACK_IMPORTED_MODULE_10__["EventsService"] }
];
Tab4Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_7__["Component"])({
        selector: 'app-tab4',
        template: _raw_loader_tab4_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_tab4_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], Tab4Page);



/***/ }),

/***/ "dkQB":
/*!*********************************************!*\
  !*** ./src/app/tab4/tab4-routing.module.ts ***!
  \*********************************************/
/*! exports provided: Tab4PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab4PageRoutingModule", function() { return Tab4PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _tab4_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab4.page */ "Wxre");




const routes = [
    {
        path: '',
        component: _tab4_page__WEBPACK_IMPORTED_MODULE_3__["Tab4Page"]
    }
];
let Tab4PageRoutingModule = class Tab4PageRoutingModule {
};
Tab4PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], Tab4PageRoutingModule);



/***/ }),

/***/ "kfDu":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab4/tab4.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header title=\"Chat list\" [checkPage]='true'></header>\n\n<ion-content>\n  <div *ngIf=\"isCheck == true\">\n    <ion-row class=\"ion_row\">\n      <ion-col size=\"9\">\n        <ion-searchbar type=\"text\" debounce=\"500\" [(ngModel)]=\"search_value\" (ionChange)=\"setFilteredItems($event,1)\"\n          (ionClear)=\"claerSearcher()\"></ion-searchbar>\n      </ion-col>\n      <ion-col size=\"3\">\n        <p class=\"p_style\">Sort by</p>\n        <ion-select class=\"ion_select\" interface=\"popover\" [(ngModel)]=\"sortBy\" (ionChange)=\"sortFunction(sortBy)\">\n          <ion-select-option value=\"n\">Newest</ion-select-option>\n          <ion-select-option value=\"o\">Oldest</ion-select-option>\n        </ion-select>\n      </ion-col>\n    </ion-row>\n    <div *ngFor=\"let chat of chatList\">\n      <ion-item class=\"item-text-wrap\" lines=\"none\" (click)=\"goToChat(chat.contactnumber,chat.firstname,chat.lastname)\">\n        <ion-avatar slot=\"\" item-start>\n          <img src=\"{{chat.avimage || avatar_url}}\">\n        </ion-avatar>\n        <ion-label class=\"sc-ion-label-ios-h sc-ion-label-ios-s ios hydrated\">\n          <ion-row>\n            <ion-col size=\"8\">\n              <h2>\n                {{chat.firstname}} {{chat.lastname}}\n              </h2>\n              <!-- <div class=\"overFlow_hidden\">\n              <span *ngFor=\"let userData of user; let j=index\">{{userData?.user?.first_name}}<span\n                  *ngIf=\"user.length != (j+1)\">, </span></span>\n            </div> -->\n\n              <p>{{chat.lastmessagetext}}</p>\n            </ion-col>\n            <ion-col size=\"4\" class=\"col_bagde_style\">\n              <span>{{chat.lastmessagetime}}</span>\n              <!-- <ion-bagde>1</ion-bagde> -->\n            </ion-col>\n          </ion-row>\n        </ion-label>\n      </ion-item>\n      <hr>\n    </div>\n  </div>\n  <!-- ///////////////////////////////////////   TWILIO  //////////////////////////// -->\n  <div *ngIf=\"isCheck == false\" class=\"twilio\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <ion-label>\n            Set up account at twilio.com and follow instruction document\n          </ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-card>\n        <ion-row>\n          <ion-col>\n            <h5>\n              Need help <ion-icon name=\"help-circle-outline\"></ion-icon>\n            </h5>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-button class=\"pdf\" expand=\"block\" (click)=\"openImage()\">\n              Text Instructions\n            </ion-button>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-button class=\"video\" expand=\"block\" (click)=\"playVideo()\">\n              Video Tutorial\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-grid>\n    <ion-item>\n      <ion-label position=\"floating\">Account SID</ion-label>\n      <ion-input [(ngModel)]=\"account_SID\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"floating\">Auth Token</ion-label>\n      <ion-input [(ngModel)]=\"auth_Token\"></ion-input>\n    </ion-item>\n    <ion-item class=\"padding_top\">\n      <ion-label>+1</ion-label>\n      <ion-input [(ngModel)]=\"twilio_Number\" placeholder=\"10 digits Twilio number e.g 4124388691\"\n        (keyup)=\"onKeyUp($event)\" type=\"tel\" minlength=\"10\" maxlength=\"10\"></ion-input>\n    </ion-item>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <ion-button expand=\"block\" (click)=\"update()\">\n            Update\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <!-- ​REFRESHER -->\n  <ion-refresher *ngIf=\"isCheck == true\" slot=\"fixed\" (ionRefresh)=\"refreshDataList($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>");

/***/ }),

/***/ "vacZ":
/*!*************************************!*\
  !*** ./src/app/tab4/tab4.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".item ion-avatar {\n  border-radius: 50%;\n  padding: 0px;\n  margin: 10px 10px 10px 0px;\n}\n\n.claim {\n  margin: 0px !important;\n  padding: 0px;\n}\n\n.ion_row {\n  border-bottom: 1px solid #dcdcdc;\n}\n\n.p_style {\n  margin: 0px;\n  padding: 5px 0px 0px 5px;\n}\n\n.ion_select {\n  color: #7d7d7d !important;\n  font-size: 12px !important;\n  padding: 5px;\n}\n\nh2 {\n  margin: 0px;\n  font-size: 1.2rem;\n  text-overflow: ellipsis;\n  overflow: auto;\n  line-height: normal;\n}\n\np {\n  text-overflow: ellipsis;\n  overflow: hidden;\n}\n\nspan {\n  font-size: 12px;\n}\n\nion-bagde {\n  background: #25223b;\n  color: #ffffff;\n  font-size: 12px;\n  height: 22px;\n  width: 22px;\n  display: flex !important;\n  justify-content: center !important;\n  align-items: center !important;\n  border-radius: 50%;\n}\n\n.col_bagde_style {\n  justify-items: end;\n  display: grid;\n}\n\n.overFlow_hidden {\n  text-overflow: ellipsis;\n  overflow: auto;\n}\n\nhr {\n  height: 1px;\n  width: 90%;\n  background: #c1c1c1;\n  margin: 0px 16px;\n}\n\n.twilio ion-label {\n  font-size: 18px;\n}\n\n.twilio ion-button {\n  color: #fff;\n  font-size: 18px;\n  height: 45px;\n}\n\n.twilio .pdf {\n  --background: #337ab7 !important;\n}\n\n.twilio .video {\n  --background: #5bc0de !important;\n}\n\n.twilio .label-floating.sc-ion-label-ios-h {\n  color: #5f5f5f;\n}\n\n.twilio h5 {\n  color: #000 !important;\n}\n\n.twilio ion-item {\n  padding-right: 20px;\n}\n\n.twilio ion-select {\n  width: 24%;\n}\n\n.twilio .padding_top {\n  padding-top: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3RhYjQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsa0JBQUE7RUFDQSxZQUFBO0VBR0EsMEJBQUE7QUFGRjs7QUFLQTtFQUNFLHNCQUFBO0VBQ0EsWUFBQTtBQUZGOztBQUlBO0VBQ0UsZ0NBQUE7QUFERjs7QUFHQTtFQUNFLFdBQUE7RUFDQSx3QkFBQTtBQUFGOztBQUdBO0VBQ0UseUJBQUE7RUFDQSwwQkFBQTtFQUNBLFlBQUE7QUFBRjs7QUFFQTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0FBQ0Y7O0FBRUE7RUFDRSx1QkFBQTtFQUNBLGdCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxlQUFBO0FBQ0Y7O0FBRUE7RUFDRSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSx3QkFBQTtFQUNBLGtDQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtBQUNGOztBQUVBO0VBRUUsa0JBQUE7RUFDQSxhQUFBO0FBQUY7O0FBR0E7RUFDRSx1QkFBQTtFQUNBLGNBQUE7QUFBRjs7QUFHQTtFQUNFLFdBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQUFGOztBQUlFO0VBQ0UsZUFBQTtBQURKOztBQUlFO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFFQSxZQUFBO0FBSEo7O0FBTUU7RUFDRSxnQ0FBQTtBQUpKOztBQU9FO0VBQ0UsZ0NBQUE7QUFMSjs7QUFRRTtFQUNFLGNBQUE7QUFOSjs7QUFTRTtFQUNFLHNCQUFBO0FBUEo7O0FBVUU7RUFDRSxtQkFBQTtBQVJKOztBQVdFO0VBQ0UsVUFBQTtBQVRKOztBQVlFO0VBQ0UsaUJBQUE7QUFWSiIsImZpbGUiOiJ0YWI0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pdGVtIGlvbi1hdmF0YXIge1xuICAvLyBib3JkZXI6IHNvbGlkO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIHBhZGRpbmc6IDBweDtcbiAgLy8gYm9yZGVyLWNvbG9yOiAjMjUyMjNiO1xuICAvLyBib3JkZXItd2lkdGg6IDJweDtcbiAgbWFyZ2luOiAxMHB4IDEwcHggMTBweCAwcHg7XG59XG5cbi5jbGFpbSB7XG4gIG1hcmdpbjogMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDBweDtcbn1cbi5pb25fcm93IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkY2RjZGM7XG59XG4ucF9zdHlsZSB7XG4gIG1hcmdpbjogMHB4O1xuICBwYWRkaW5nOiA1cHggMHB4IDBweCA1cHg7XG59XG5cbi5pb25fc2VsZWN0IHtcbiAgY29sb3I6ICM3ZDdkN2QgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDVweDtcbn1cbmgyIHtcbiAgbWFyZ2luOiAwcHg7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgb3ZlcmZsb3c6IGF1dG87XG4gIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG59XG5cbnAge1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuc3BhbiB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuaW9uLWJhZ2RlIHtcbiAgYmFja2dyb3VuZDogIzI1MjIzYjtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgaGVpZ2h0OiAyMnB4O1xuICB3aWR0aDogMjJweDtcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xuICBhbGlnbi1pdGVtczogY2VudGVyICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cblxuLmNvbF9iYWdkZV9zdHlsZSB7XG4gIC8vIGp1c3RpZnktaXRlbXM6IGZsZXgtZW5kO1xuICBqdXN0aWZ5LWl0ZW1zOiBlbmQ7XG4gIGRpc3BsYXk6IGdyaWQ7XG59XG5cbi5vdmVyRmxvd19oaWRkZW4ge1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgb3ZlcmZsb3c6IGF1dG87XG59XG5cbmhyIHtcbiAgaGVpZ2h0OiAxcHg7XG4gIHdpZHRoOiA5MCU7XG4gIGJhY2tncm91bmQ6ICNjMWMxYzE7XG4gIG1hcmdpbjogMHB4IDE2cHg7XG59XG5cbi50d2lsaW8ge1xuICBpb24tbGFiZWwge1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgfVxuXG4gIGlvbi1idXR0b24ge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAvLyAtLWJhY2tncm91bmQ6ICMwNzkxOTk7XG4gICAgaGVpZ2h0OiA0NXB4O1xuICB9XG5cbiAgLnBkZiB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMzM3YWI3ICFpbXBvcnRhbnQ7XG4gIH1cblxuICAudmlkZW8ge1xuICAgIC0tYmFja2dyb3VuZDogIzViYzBkZSAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmxhYmVsLWZsb2F0aW5nLnNjLWlvbi1sYWJlbC1pb3MtaCB7XG4gICAgY29sb3I6ICM1ZjVmNWY7XG4gIH1cblxuICBoNSB7XG4gICAgY29sb3I6ICMwMDAgIWltcG9ydGFudDtcbiAgfVxuXG4gIGlvbi1pdGVtIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICB9XG5cbiAgaW9uLXNlbGVjdCB7XG4gICAgd2lkdGg6IDI0JTtcbiAgfVxuXG4gIC5wYWRkaW5nX3RvcCB7XG4gICAgcGFkZGluZy10b3A6IDI1cHg7XG4gIH1cbn0iXX0= */");

/***/ })

}]);
//# sourceMappingURL=tab4-tab4-module-es2015.js.map