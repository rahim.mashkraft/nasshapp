(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "5dVO":
/*!********************************************!*\
  !*** ./src/app/services/loader.service.ts ***!
  \********************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let LoaderService = class LoaderService {
    constructor(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
    }
    showLoader() {
        this.isBusy = true;
        // this.loaderToShow = this.loadingCtrl.create({
        //   message: 'Please Wait..'
        // }).then((res) => {
        //   res.present();
        //   // res.onDidDismiss().then((dis) => {
        //   //    console.log('Loading dismissed!',dis);
        //   // });
        // });
        // // this.hideLoader();
    }
    hideLoader() {
        // setTimeout(()=>{
        //   this.loadingCtrl.dismiss();
        // },100)
        this.isBusy = false;
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ "8MT7":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar class=\"header_clas\">\n    <img slot=\"start\" src=\"assets/logo/logo.png\" />\n    <ion-buttons class=\"notification-button \" slot=\"end\" (click)=\"notification()\">\n      <ion-badge color=\"danger\" class=\"notifications-badge\">3</ion-badge>\n      <ion-icon slot=\"icon-only\" name=\"notifications\"></ion-icon>\n    </ion-buttons>\n    <ion-icon slot=\"end\" size=\"large\" name=\"home\" routerLink=\"/tabs/tab1\"></ion-icon>\n  </ion-toolbar>\n</ion-header> -->\n<header title=\"Home\" [checkPage]='true'></header>\n<ion-content>\n  <div class=\"bg-white\">\n\n    <div *ngFor=\"let item of data\" class=\"quiz-categories\" (click)=goToSubCat(item.id,item.title)>\n      <img [src]=\"environment.imageUrl + item.image\" class=\"img-container\">\n    </div>\n  </div>\n<!-- {{postdata.length}}\n  <ion-virtual-scroll [items]=\"postdata\">\n    <ion-item *virtualItem=\"let item\">\n      <ion-col size=\"4\">\n        <ion-label>\n          <p  class=\"col_style\">{{item.firstname}} {{item.lastname}}</p>\n        </ion-label>\n      </ion-col>\n      <ion-col size=\"4\">\n        <ion-label>\n          <p *ngIf=\"item.phone\" class=\"d-block\">{{item.phone}}</p>\n          <p *ngIf=\"item.email != null || item.email != ''\" class=\"d-block\">{{item.email}}</p>\n        </ion-label>\n      </ion-col>\n    </ion-item>\n  </ion-virtual-scroll> -->\n\n  <!-- ​REFRESHER -->\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"refreshData($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n\n  <!-- INFINITE SCROLL -->\n  <ion-infinite-scroll *ngIf=\"!loading && currentPage<lastPage\" threshold=\"100px\"\n    (ionInfinite)=\"loadMoreData(1, $event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more items...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n\n\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>\n\n<!-- <ion-fab horizontal=\"end\" vertical=\"bottom\" slot=\"fixed\" class=\"ios fab-horizontal-end fab-vertical-bottom hydrated\">\n  <ion-fab-button color=\"dark\" class=\"ion-color ion-color-dark ios ion-activatable ion-focusable hydrated\">\n    <ion-icon md=\"caret-up\" ios=\"chevron-up-circle-outline\" role=\"img\" class=\"ios hydrated\"\n      aria-label=\"chevron up circle outline\"></ion-icon>\n  </ion-fab-button>\n  <ion-fab-list side=\"top\" class=\"ios fab-list-side-top hydrated\">\n\n    <div class=\"btn-c\">\n      <ion-label (click)=\"cooling(fab)\" class=\"btn-lable\">{{'coolingProcedure'}} </ion-label>\n      <ion-fab-button color=\"light\" class=\"fab_btn ion-color ion-color-light ios fab-button-in-list ion-activatable ion-focusable hydrated\">\n        <ion-icon name=\"logo-twitter\" role=\"img\" class=\"ios hydrated\" aria-label=\"logo twitter\"></ion-icon>\n      </ion-fab-button>\n    </div>\n\n    <div class=\"btn-c\">\n      <ion-label (click)=\"presentActionSheet(fab)\" class=\"btn-lable\">{{'deviation'}}</ion-label>\n      <ion-fab-button color=\"light\" class=\"fab_btn ion-color ion-color-light ios fab-button-in-list ion-activatable ion-focusable hydrated\">\n        <ion-icon name=\"logo-twitter\" role=\"img\" class=\"ios hydrated\" aria-label=\"logo twitter\"></ion-icon>\n      </ion-fab-button>\n    </div>\n\n    <div class=\"btn-c\">\n      <ion-label (click)=\"presentActionSheet(fab)\" class=\"btn-lable\">{{'deviation'}}</ion-label>\n      <ion-fab-button color=\"light\" class=\"fab_btn ion-color ion-color-light ios fab-button-in-list ion-activatable ion-focusable hydrated\">\n        <ion-icon name=\"logo-twitter\" role=\"img\" class=\"ios hydrated\" aria-label=\"logo twitter\"></ion-icon>\n      </ion-fab-button>\n    </div>\n  </ion-fab-list>\n</ion-fab> -->");

/***/ }),

/***/ "Mzl2":
/*!***********************************!*\
  !*** ./src/app/tab1/tab1.page.ts ***!
  \***********************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_tab1_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./tab1.page.html */ "8MT7");
/* harmony import */ var _tab1_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tab1.page.scss */ "rWyk");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/environments/environment */ "AytR");
/* harmony import */ var _services_categories_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/categories.service */ "ycII");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/loader.service */ "5dVO");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/storage */ "e8h1");
/* harmony import */ var _ionic_native_diagnostic_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/diagnostic/ngx */ "mtRb");











let Tab1Page = class Tab1Page {
    constructor(router, categoriesService, loaderService, plt, storage, diagnostic, alertController) {
        // this.eventsService.subscribe('startContact', (data) => {
        //   console.log("eventsService.subscribe startContact : ")
        //   // let options = {
        //   //   filter: '',
        //   //   multiple: true,
        //   //   hasPhoneNumber: true
        //   // }
        //   // this.contacts.find(['*'], options).then((contacts) => {
        //   //   // this.contacts.find(['displayName', 'name', 'phoneNumbers', 'emails'], options).then((contacts) => {
        //   //   console.log("Contacts : ", contacts)
        //   //   // this.contactList = contacts
        //   //   // console.log("contactList : ", this.contactList)
        //   //   this.storage.set("contacts", contacts)
        //   //   // localStorage.setItem('contacts', this.contactList);
        //   // })
        // });
        // this.eventsService.publish("startContact");
        this.router = router;
        this.categoriesService = categoriesService;
        this.loaderService = loaderService;
        this.plt = plt;
        this.storage = storage;
        this.diagnostic = diagnostic;
        this.alertController = alertController;
        this.data = [];
        this.loading = false;
        this.currentPage = 1;
        this.lastPage = 1;
        this.notificationsData = [];
        this.taskListCustom = [];
        this.array_index = 0;
        this.environment = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"];
        // setTimeout(() => {
        //   this.submit()
        // }, 500);
        // setTimeout(() => {
        // this.playAudio()
        // }, 1500);
        this.checkLocation();
        this.dailygoalscheck();
    }
    dailygoalscheck() {
        let formData = new FormData();
        formData.append("userid", '113');
        const obj = {
            userid: localStorage.getItem('user_id')
        };
        this.categoriesService.dailygoalscheck(formData)
            .then((res) => {
            console.log("res : ", res);
        });
    }
    checkLocation() {
        if (this.plt.is('cordova')) {
            this.diagnostic.getLocationAuthorizationStatus().then((status) => {
                setTimeout(() => {
                    console.log("status.NOT_REQUESTED : ", this.diagnostic.permissionStatus.NOT_REQUESTED);
                    console.log("status.DENIED_ALWAYS : ", this.diagnostic.permissionStatus.DENIED_ALWAYS);
                    console.log("status.GRANTED : ", this.diagnostic.permissionStatus.GRANTED);
                    console.log("status.GRANTED_WHEN_IN_USE : ", this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE);
                }, 100);
                // alert(status) ;
                if (status == this.diagnostic.permissionStatus.GRANTED || status == this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE) {
                    this.diagnostic.isLocationEnabled().then((res) => {
                        console.log("this.diagnostic.isLocationEnabled() : ", res);
                        if (res == true) {
                        }
                    });
                }
                else if (status == this.diagnostic.permissionStatus.NOT_REQUESTED) {
                    console.log("status == this.diagnostic.permissionStatus.NOT_REQUESTED");
                }
                else {
                    // this.further();
                }
                console.log("status : ", status);
            });
        }
    }
    further() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Turn on Location Services',
                message: 'Please turn on location services',
                buttons: [
                    {
                        text: 'Not now',
                        handler: () => {
                            console.log('Cancel No');
                        }
                    },
                    {
                        text: 'Settings',
                        handler: () => {
                            //console.log('Cancel Yes');
                            this.switchToSettings();
                        }
                    }
                ]
            });
            // this.currentAlert = alert;
            yield alert.present();
        });
    }
    switchToSettings() {
        this.diagnostic.switchToSettings()
            .then(() => {
            console.log("Successfully switched to Settings app");
        }).catch(e => console.error(e));
    }
    submit() {
        // let jj = 0;
        // let formData = new FormData();
        // // console.log("this.radio_check : ", this.radio_check)
        // for (let data of this.postdata) {
        //   this.responseData = data;
        //   console.log(this.responseData)
        //   // var firstname = this.responseData.firstname;
        //   // var lastname = this.responseData.lastname;
        //   // var email = this.responseData.email;
        //   // var phone = this.responseData.phone;
        //   formData.append("postdata[" + jj + "][firstname]", this.responseData.firstname);
        //   formData.append("postdata[" + jj + "][lastname]", this.responseData.lastname);
        //   formData.append("postdata[" + jj + "][email]", this.responseData.email);
        //   formData.append("postdata[" + jj + "][phone]", this.responseData.phone);
        //   jj++;
        // }
        // formData.append("aid", localStorage.getItem('user_id'));
        // // formData.append("group_id", this.radio_check);
        // this.contactGroupService.contactsImport(formData).then(data => {
        // })
    }
    ionViewWillEnter() {
        this.currentPage = 1;
        this.fetchCategories(this.currentPage);
    }
    loadData(event) {
        setTimeout(() => {
            console.log('Done');
            event.target.complete();
            // App logic to determine if all data is loaded
            // and disable the infinite scroll
            if (this.taskListCustom.length == 5) {
                event.target.disabled = true;
            }
        }, 500);
    }
    toggleInfiniteScroll() {
        this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    }
    fetchCategories(currentPage, event = null) {
        if (this.loading) {
            return;
        }
        if (this.categoriesSubscription) {
            this.categoriesSubscription.unsubscribe();
        }
        this.loading = true;
        this.loaderService.showLoader();
        let formData = new FormData();
        formData.append("PageNumber", currentPage);
        this.categoriesService.getQuizCategories(formData).then(data => {
            console.log("data : ", data);
            this.dataObj = data;
            this.loading = false;
            this.currentPage = this.dataObj.CurrentPage;
            this.lastPage = this.dataObj.total;
            if (this.currentPage == 1) {
                this.data = this.dataObj.categories;
                setTimeout(() => {
                    this.loaderService.hideLoader();
                }, 500);
                // setTimeout(() => {
                //   this.eventsService.publish("startContact");
                // },1000)
            }
            else {
                this.data = [...this.data, ...this.dataObj.categories];
                setTimeout(() => {
                    this.loaderService.hideLoader();
                }, 500);
            }
            if (event) {
                event.target.complete();
            }
        }).catch(err => {
            this.loaderService.hideLoader();
            this.loading = false;
            if (event) {
                event.target.complete();
            }
        });
        // this.categoriesSubscription = this.categoriesService.getQuizCategories({PageNumber: page})
        // .subscribe((res: any) => {
        //   this.loading = false;
        //   this.currentPage = res.CurrentPage;
        //   this.lastPage = res.lastPage;
        //   if (this.currentPage === 1) {
        //     this.data = res.categories;
        //   } else {
        //     this.data = [...this.data, ...res.categories];
        //   }
        //   if (event) {
        //     event.target.complete();
        //   }
        // },
        // err => {
        //   this.loading = false;
        //   if (event) {
        //     event.target.complete();
        //   }
        // });
    }
    goToSubCat(id, name) {
        let navigationExtras = {
            state: {
                name: name,
                id: id,
            }
        };
        // this.router.navigate(['post-add-second', navigationExtras]);
        this.router.navigate(['tabs/tab1/sub-categories'], navigationExtras);
        // this.navCtrl.navigateForward(['tabs/tab1/sub-categories'],navigationExtras);
    }
    ConvertToInt(currentPage) {
        return parseInt(currentPage);
    }
    loadMoreData(value, event) {
        this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value);
        this.fetchCategories(this.currentPage, event);
    }
    refreshData(event) {
        this.currentPage = 1;
        // this.data = [];
        this.fetchCategories(1, event);
    }
    notification() {
        this.router.navigate(['tabs/notification']);
    }
};
Tab1Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _services_categories_service__WEBPACK_IMPORTED_MODULE_7__["CategoriesService"] },
    { type: _services_loader_service__WEBPACK_IMPORTED_MODULE_8__["LoaderService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["Storage"] },
    { type: _ionic_native_diagnostic_ngx__WEBPACK_IMPORTED_MODULE_10__["Diagnostic"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] }
];
Tab1Page.propDecorators = {
    infiniteScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonInfiniteScroll"],] }],
    search: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['search',] }]
};
Tab1Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-tab1',
        template: _raw_loader_tab1_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_tab1_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], Tab1Page);



/***/ }),

/***/ "XOzS":
/*!*********************************************!*\
  !*** ./src/app/tab1/tab1-routing.module.ts ***!
  \*********************************************/
/*! exports provided: Tab1PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageRoutingModule", function() { return Tab1PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab1.page */ "Mzl2");




const routes = [
    {
        path: '',
        component: _tab1_page__WEBPACK_IMPORTED_MODULE_3__["Tab1Page"],
    },
    {
        path: 'sub-categories',
        loadChildren: () => __webpack_require__.e(/*! import() | sub-categories-sub-categories-module */ "sub-categories-sub-categories-module").then(__webpack_require__.bind(null, /*! ./sub-categories/sub-categories.module */ "0i2D")).then(m => m.SubCategoriesPageModule)
    }
];
let Tab1PageRoutingModule = class Tab1PageRoutingModule {
};
Tab1PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Tab1PageRoutingModule);



/***/ }),

/***/ "rWyk":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".quiz-categories {\n  position: relative;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  margin-bottom: 2px;\n  margin-top: 2px;\n}\n\n.quiz-categories h3 {\n  position: absolute;\n  text-align: center;\n  width: 100%;\n  color: white;\n  font-size: 50px;\n  overflow-wrap: break-word;\n  margin: 0px;\n  line-height: 45px;\n  padding: 5px;\n  font-family: Ubuntu;\n}\n\n.img-container {\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  height: 220px;\n}\n\n.notification-button {\n  padding-right: 10px;\n}\n\n.notification-button ion-icon {\n  z-index: -1;\n}\n\n.notifications-badge {\n  background-color: red;\n  position: absolute;\n  top: -3px;\n  right: 0px;\n  border-radius: 100%;\n}\n\n.btn-c {\n  position: relative;\n}\n\n.btn-lable {\n  position: absolute;\n  top: 10px;\n  right: 60px;\n  top: 6px;\n  right: 50px;\n  color: rgba(0, 0, 0, 0.7);\n  background-color: #f4f4f4;\n  line-height: 24px;\n  padding: 4px 8px;\n  border-radius: 4px;\n  border: 1px solid;\n}\n\n.fab_btn {\n  margin: 3px;\n  width: 40px;\n  height: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3RhYjEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBQ0E7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBRUEseUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFDSjs7QUFDQTtFQUNJLFdBQUE7RUFDQSxvQkFBQTtLQUFBLGlCQUFBO0VBQ0EsYUFBQTtBQUVKOztBQUFBO0VBQ0ksbUJBQUE7QUFHSjs7QUFGSTtFQUNJLFdBQUE7QUFJUjs7QUFDQTtFQUNJLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0FBRUo7O0FBR0E7RUFDSSxrQkFBQTtBQUFKOztBQUVFO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBR0U7RUFDRSxXQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFBSiIsImZpbGUiOiJ0YWIxLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5xdWl6LWNhdGVnb3JpZXMge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG4gICAgbWFyZ2luLXRvcDogMnB4O1xufVxuLnF1aXotY2F0ZWdvcmllcyBoM3tcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDUwcHg7XG4gIC8vICBiYWNrZ3JvdW5kOiAjNDY0NjQ2NGQ7XG4gICAgb3ZlcmZsb3ctd3JhcDogYnJlYWstd29yZDtcbiAgICBtYXJnaW46IDBweDtcbiAgICBsaW5lLWhlaWdodDogNDVweDtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgZm9udC1mYW1pbHk6IFVidW50dTtcbn1cbi5pbWctY29udGFpbmVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBvYmplY3QtZml0OiBjb3ZlcjtcbiAgICBoZWlnaHQ6IDIyMHB4O1xufVxuLm5vdGlmaWNhdGlvbi1idXR0b24geyAgICAgICAgICAgIFxuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gICAgaW9uLWljb24ge1xuICAgICAgICB6LWluZGV4OiAtMTtcbiAgICB9XG59XG5cblxuLm5vdGlmaWNhdGlvbnMtYmFkZ2Uge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAtM3B4O1xuICAgIHJpZ2h0OiAwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbn1cblxuLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyBpb24tRmFiXG5cbi5idG4tY3tcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIH1cbiAgLmJ0bi1sYWJsZXtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAxMHB4O1xuICAgIHJpZ2h0OiA2MHB4O1xuICAgIHRvcDogNnB4O1xuICAgIHJpZ2h0OiA1MHB4O1xuICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNyk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y0ZjRmNDtcbiAgICBsaW5lLWhlaWdodDogMjRweDtcbiAgICBwYWRkaW5nOiA0cHggOHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcbiAgfVxuXG5cbiAgLmZhYl9idG4ge1xuICAgIG1hcmdpbjogM3B4O1xuICAgIHdpZHRoOiA0MHB4O1xuICAgIGhlaWdodDogNDBweDtcblxuICB9XG5cblxuXG4iXX0= */");

/***/ }),

/***/ "tmrb":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.module.ts ***!
  \*************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tab1.page */ "Mzl2");
/* harmony import */ var _tab1_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab1-routing.module */ "XOzS");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../modules/shared/shared.module */ "FpXt");








let Tab1PageModule = class Tab1PageModule {
};
Tab1PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _tab1_routing_module__WEBPACK_IMPORTED_MODULE_6__["Tab1PageRoutingModule"],
            _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_5__["Tab1Page"]]
    })
], Tab1PageModule);



/***/ }),

/***/ "ycII":
/*!************************************************!*\
  !*** ./src/app/services/categories.service.ts ***!
  \************************************************/
/*! exports provided: CategoriesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesService", function() { return CategoriesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _network_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./network.service */ "dwY0");






let CategoriesService = class CategoriesService {
    constructor(alertController, http, networkService) {
        this.alertController = alertController;
        this.http = http;
        this.networkService = networkService;
        this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
    }
    getQuizCategories(formData) {
        return new Promise((resolve, reject) => {
            this.networkService.onNetworkChange().subscribe((status) => {
                if (status == _network_service__WEBPACK_IMPORTED_MODULE_5__["ConnectionStatus"].Online) {
                    console.log(this.apiUrl + 'home.php');
                    formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);
                    this.http.post(this.apiUrl + 'home.php', formData)
                        .subscribe(res => {
                        resolve(res);
                    }, (err) => {
                        reject(err);
                        console.log('something went wrong please try again');
                    });
                }
            });
        });
    }
    dailygoalscheck(formData) {
        return new Promise((resolve, reject) => {
            // const httpOptions = {
            //     headers: new HttpHeaders({
            //        'Accept': 'application/json ',
            //        'Content-Type': 'application/json',
            //     })
            //   }
            console.log(this.apiUrl + 'dailygoalscheck.php');
            this.http.post(this.apiUrl + 'dailygoalscheck.php', formData)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
                console.log('something went wrong please try again');
            });
        });
    }
};
CategoriesService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _network_service__WEBPACK_IMPORTED_MODULE_5__["NetworkService"] }
];
CategoriesService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], CategoriesService);



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module-es2015.js.map