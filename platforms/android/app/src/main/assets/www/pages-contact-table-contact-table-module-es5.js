(function () {
  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-contact-table-contact-table-module"], {
    /***/
    "5dVO":
    /*!********************************************!*\
      !*** ./src/app/services/loader.service.ts ***!
      \********************************************/

    /*! exports provided: LoaderService */

    /***/
    function dVO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoaderService", function () {
        return LoaderService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var LoaderService = /*#__PURE__*/function () {
        function LoaderService(loadingCtrl) {
          _classCallCheck(this, LoaderService);

          this.loadingCtrl = loadingCtrl;
        }

        _createClass(LoaderService, [{
          key: "showLoader",
          value: function showLoader() {
            this.isBusy = true; // this.loaderToShow = this.loadingCtrl.create({
            //   message: 'Please Wait..'
            // }).then((res) => {
            //   res.present();
            //   // res.onDidDismiss().then((dis) => {
            //   //    console.log('Loading dismissed!',dis);
            //   // });
            // });
            // // this.hideLoader();
          }
        }, {
          key: "hideLoader",
          value: function hideLoader() {
            // setTimeout(()=>{
            //   this.loadingCtrl.dismiss();
            // },100)
            this.isBusy = false;
          }
        }]);

        return LoaderService;
      }();

      LoaderService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoaderService);
      /***/
    },

    /***/
    "9cAt":
    /*!*************************************************************!*\
      !*** ./src/app/pages/contact-table/contact-table.module.ts ***!
      \*************************************************************/

    /*! exports provided: ContactTablePageModule */

    /***/
    function cAt(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactTablePageModule", function () {
        return ContactTablePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _contact_table_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./contact-table-routing.module */
      "nqI4");
      /* harmony import */


      var _contact_table_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./contact-table.page */
      "WgAM");
      /* harmony import */


      var src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/modules/shared/shared.module */
      "FpXt");

      var ContactTablePageModule = function ContactTablePageModule() {
        _classCallCheck(this, ContactTablePageModule);
      };

      ContactTablePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _contact_table_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactTablePageRoutingModule"], src_app_modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_contact_table_page__WEBPACK_IMPORTED_MODULE_6__["ContactTablePage"]]
      })], ContactTablePageModule);
      /***/
    },

    /***/
    "WgAM":
    /*!***********************************************************!*\
      !*** ./src/app/pages/contact-table/contact-table.page.ts ***!
      \***********************************************************/

    /*! exports provided: ContactTablePage */

    /***/
    function WgAM(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactTablePage", function () {
        return ContactTablePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_contact_table_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./contact-table.page.html */
      "bd5s");
      /* harmony import */


      var _contact_table_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./contact-table.page.scss */
      "qO6M");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/alert.service */
      "3LUQ");
      /* harmony import */


      var src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/contact-groups.service */
      "rtEV");
      /* harmony import */


      var src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/services/loader.service */
      "5dVO");

      var ContactTablePage = /*#__PURE__*/function () {
        function ContactTablePage(navCtrl, contactGroupsService, loaderService, alertService, router, cdr) {
          _classCallCheck(this, ContactTablePage);

          this.navCtrl = navCtrl;
          this.contactGroupsService = contactGroupsService;
          this.loaderService = loaderService;
          this.alertService = alertService;
          this.router = router;
          this.cdr = cdr; // @ViewChild('select_value', { static: false }) select_value: IonSelect;

          this.customAlertOptions = {// header: 'Pizza Toppings',
            // subHeader: 'Select your toppings',
            // message: '$1.00 per topping',
            // translucent: true
          };
          this.loading = false;
          this.currentPage = 1;
          this.lastPage = 1;
          this.isCheck = false;
          this.sortBy = 'n';
        }

        _createClass(ContactTablePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewDidLoad",
          value: function ionViewDidLoad() {}
        }, {
          key: "selectValue",
          value: function selectValue(value, i, item_obj) {
            var _this = this;

            console.log(value.detail.value);
            console.log(i);
            console.log(item_obj);

            if (value.detail.value == 'email') {
              if (item_obj.email == 'null') {
                // this.select_value = null
                setTimeout(function () {
                  _this.cdr.detectChanges();
                }, 500);
                console.log("this.select_value : ", this.select_value);
                this.alertService.generalAlert('No Email Found.');
              } else {
                var navigationExtras = {
                  state: {
                    item_obj: item_obj,
                    pageName: 'contact'
                  }
                }; // this.router.navigate(['post-add-second', navigationExtras]);

                this.router.navigate(['send-email-contact'], navigationExtras);
              }
            }

            if (value.detail.value == 'sms') {
              if (item_obj.phone == 'null') {
                this.alertService.generalAlert('No Phone Found.');
              } else {
                var _navigationExtras = {
                  state: {
                    item_obj: item_obj,
                    pageName: 'contact'
                  }
                }; // this.router.navigate(['post-add-second', navigationExtras]);

                this.router.navigate(['send-sms-contact'], _navigationExtras);
              }
            }

            if (value.detail.value == 'contact') {
              if (item_obj.phone == 'null') {
                this.alertService.generalAlert('This contact has no phone number.');
              } else {// let navigationExtras: NavigationExtras = {
                //   state: {
                //     item_obj: item_obj,
                // pageName: 'contact'
                //   }
                // };
                // // this.router.navigate(['post-add-second', navigationExtras]);
                // this.router.navigate(['send-sms-contact'], navigationExtras);
              }
            }

            if (value.detail.value == 'edit') {
              if (item_obj.phone == 'null') {
                this.alertService.generalAlert('This contact has no phone number.');
              } else {// let navigationExtras: NavigationExtras = {
                //   state: {
                //     item_obj: item_obj,
                // pageName: 'contact'
                //   }
                // };
                // // this.router.navigate(['post-add-second', navigationExtras]);
                // this.router.navigate(['send-sms-contact'], navigationExtras);
              }
            }
          }
        }, {
          key: "sortFunction",
          value: function sortFunction(sortBy) {
            console.log(sortBy);

            if (sortBy == 'n') {
              this.getContacts(this.currentPage, this.search_value, '');
            } else {
              this.getContacts(this.currentPage, this.search_value, this.sortBy);
            }
          }
        }, {
          key: "claerSearcher",
          value: function claerSearcher() {
            console.log("claerSearcher");
            this.contactsData = [];
            this.currentPage = 1;
            this.getContacts(this.currentPage, '', this.sortBy);
          }
        }, {
          key: "setFilteredItems",
          value: function setFilteredItems(ev, currentPage) {
            console.log(currentPage);
            var val = ev.target.value;
            console.log(val);
            var str = new String(val);
            var len = str.length; // if the value is an empty string don't filter the items

            if (len >= 3 && val && val.trim() !== '') {
              if (this.sortBy == 'n') {
                this.getContacts(this.currentPage, this.search_value, '');
              } else {
                this.getContacts(this.currentPage, this.search_value, this.sortBy);
              } // this.isItemAvailable = true;
              // this.currentPage = 1;
              // let formData = new FormData();
              // formData.append("PageNumber", currentPage);
              // formData.append("searchstring", val);
              // formData.append("aid", localStorage.getItem('user_id'));
              // this.searchService.searchContacts(formData).then((data: any) => {
              //   console.log("data : ", data)
              //   this.dataObj = data
              //   // this.currentPage = this.dataObj.CurrentPage;
              //   // this.lastPage = this.dataObj.total;
              //   // if (this.currentPage == 1) {
              //     this.contactsData = this.dataObj.contacts;
              //     if(this.contactsData.length == 0){
              //       this.isCheck = true;
              //     }
              //   // } else {
              //   //   this.contactsData = [...this.contactsData, ...this.dataObj.contacts];
              //   // }
              //   // this.loaderService.hideLoader();
              // },err => {
              //   this.isCheck = true;
              //     // this.loaderService.hideLoader();
              //   });

            } else {// this.isItemAvailable = false;
              } // this.items = this.filterItems(this.searchTerm);

          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.contactsData = [];
            this.currentPage = 1;
            this.getContacts(this.currentPage, '', '');
          }
        }, {
          key: "getContacts",
          value: function getContacts(currentPage, search_value, sort_value) {
            var _this2 = this;

            var event = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
            this.loaderService.showLoader();
            var formData = new FormData();
            formData.append("PageNumber", currentPage);

            if (search_value) {
              formData.append("searchstring", search_value);
            }

            if (sort_value) {
              formData.append("sort", sort_value);
            }

            formData.append("aid", localStorage.getItem('user_id'));
            this.contactGroupsService.getContacts(formData).then(function (data) {
              console.log("data : ", data);
              _this2.dataObj = data;
              _this2.currentPage = _this2.dataObj.CurrentPage;
              _this2.lastPage = _this2.dataObj.total;

              if (_this2.currentPage == 1) {
                _this2.contactsData = _this2.dataObj.contacts;

                if (_this2.contactsData.length == 0) {
                  _this2.isCheck = true;
                }
              } else {
                _this2.contactsData = [].concat(_toConsumableArray(_this2.contactsData), _toConsumableArray(_this2.dataObj.contacts));
              }

              _this2.loaderService.hideLoader();

              if (event) {
                event.target.complete();
              }
            }, function (err) {
              _this2.loaderService.hideLoader();

              _this2.isCheck = true;

              if (event) {
                event.target.complete();
              }
            });
          }
        }, {
          key: "refreshData",
          value: function refreshData(event) {
            this.currentPage = 1;
            this.getContacts(1, '', '', event);
          }
        }, {
          key: "ConvertToInt",
          value: function ConvertToInt(currentPage) {
            return parseInt(currentPage);
          }
        }, {
          key: "loadMoreData",
          value: function loadMoreData(value, event) {
            this.currentPage = this.ConvertToInt(this.currentPage) + this.ConvertToInt(value);
            this.getContacts(this.currentPage, this.search_value, this.sortBy, event);
          }
        }, {
          key: "back",
          value: function back() {
            this.navCtrl.back();
          }
        }]);

        return ContactTablePage;
      }();

      ContactTablePage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: src_app_services_contact_groups_service__WEBPACK_IMPORTED_MODULE_7__["ContactGroupsService"]
        }, {
          type: src_app_services_loader_service__WEBPACK_IMPORTED_MODULE_8__["LoaderService"]
        }, {
          type: src_app_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ChangeDetectorRef"]
        }];
      };

      ContactTablePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-contact-table',
        template: _raw_loader_contact_table_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_contact_table_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], ContactTablePage);
      /***/
    },

    /***/
    "bd5s":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contact-table/contact-table.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function bd5s(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar class=\"color-black\">\n    <ion-icon style=\"zoom:1.5\" color=\"secondary\" name=\"arrow-back\" (click)=\"back()\" slot=\"start\"></ion-icon>\n    <ion-title color=\"secondary\">Contacts</ion-title>\n    <ion-icon slot=\"end\" size=\"large\" color=\"secondary\" name=\"home\" routerLink=\"/tabs/tab1\"></ion-icon>\n  </ion-toolbar>\n</ion-header> -->\n<header title=\"Contacts\"></header>\n<ion-content>\n\n<ion-row>\n  <ion-col size=\"8\">  <ion-searchbar\n    type=\"text\" debounce=\"500\"\n    [(ngModel)]=\"search_value\"\n      (ionChange)=\"setFilteredItems($event,1)\"\n      (ionClear)=\"claerSearcher()\"\n    ></ion-searchbar></ion-col>\n  <ion-col size=\"4\">\n    <p class=\"p_style\">Sort by</p>\n    <ion-select class=\"ion_select\" interface=\"popover\" [(ngModel)]=\"sortBy\" (ionChange)=\"sortFunction(sortBy)\">\n      <ion-select-option value=\"n\">Newest</ion-select-option>\n      <ion-select-option value=\"o\">Oldest</ion-select-option>\n      <ion-select-option value=\"f\">First Name</ion-select-option>\n      <ion-select-option value=\"l\">Last Name</ion-select-option>\n      <ion-select-option value=\"p\">Phone</ion-select-option>\n      <ion-select-option value=\"e\">Email</ion-select-option>\n    </ion-select>\n  </ion-col>\n</ion-row>\n  <!-- <div *ngIf=\"searching\" class=\"spinner-container\">\n    <ion-spinner></ion-spinner>\n  </div> -->\n\n  <!-- <ion-list>\n    <ion-item *ngFor=\"let item of items\">\n      {{ item.firstname }}\n    </ion-item>\n  </ion-list> -->\n\n  \n  <!-- Custom Table -->\n  <div class=\"bg-white\">\n    <!-- Header -->\n    <ion-grid>\n      <ion-row class=\"table-wrapper \">\n        <!-- <ion-item-divider> -->\n        <ion-col size=\"4\">\n          <ion-label>\n            <h5>Name</h5>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-label>\n            <h5>Phone / Email</h5>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\" class=\"ion-text-end\">\n          <ion-label>\n            <!-- <h5>Setting</h5> -->\n            <ion-icon name=\"settings\"></ion-icon>\n          </ion-label>\n        </ion-col>\n      <!-- </ion-item-divider> -->\n\n      </ion-row>\n      <ion-row *ngFor=\"let item of contactsData; let i=index\">\n        <ion-col size=\"4\">\n          <ion-label>\n            <p *ngIf=\"item.firstname || item.lastname\" class=\"col_style\">{{item.firstname}} {{item.lastname}}</p>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-label>\n            <p *ngIf=\"item.phone\" class=\"d-block\">{{item.phone}}</p>\n            <p *ngIf=\"item.email != null || item.email != ''\" class=\"d-block\">{{item.email}}</p>\n          </ion-label>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-select value=\"\" [interfaceOptions]=\"customAlertOptions\" (ionChange)=\"selectValue($event, i,item)\" okText=\"Okay\" cancelText=\"Dismiss\">\n            <ion-select-option value=\"sms\">Send SMS</ion-select-option>\n            <ion-select-option value=\"email\">Send Email</ion-select-option>\n            <ion-select-option value=\"contact\">Change Contact Group</ion-select-option>\n            <ion-select-option value=\"edit\">Edit Contact</ion-select-option>\n          </ion-select>\n        </ion-col>\n        <hr>\n      </ion-row>\n\n    </ion-grid>\n    <!-- No Data -->\n    <h4 class=\"clr-medium ion-margin-vertical ion-text-center\" *ngIf=\"isCheck == true\">\n      No Record Found\n    </h4>\n  </div>\n  <!-- ​REFRESHER -->\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"refreshData($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n    <!-- INFINITE SCROLL -->\n    <ion-infinite-scroll *ngIf=\"currentPage<lastPage\"  threshold=\"10px\" (ionInfinite)=\"loadMoreData(1, $event)\">\n      <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more items...\">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n</ion-content>\n<div class=\"loading-container\" [ngClass]=\"{'busy': loaderService?.isBusy}\">\n  <div class=\"loading-wrapper-c\">\n    <div class=\"backdrop\"></div>\n    <div class=\"image\">\n      </div>\n    <img src=\"assets/img/icon.png\" class=\"loader_img\">\n    <!-- <img src=\"assets/img/icon_loader.png\" class=\"loader_img\"> -->\n    <!-- <img src=\"assets/img/Spinner.gif\" class=\"loader_img\"> -->\n  </div>\n</div>";
      /***/
    },

    /***/
    "nqI4":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/contact-table/contact-table-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: ContactTablePageRoutingModule */

    /***/
    function nqI4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactTablePageRoutingModule", function () {
        return ContactTablePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _contact_table_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./contact-table.page */
      "WgAM");

      var routes = [{
        path: '',
        component: _contact_table_page__WEBPACK_IMPORTED_MODULE_3__["ContactTablePage"]
      }];

      var ContactTablePageRoutingModule = function ContactTablePageRoutingModule() {
        _classCallCheck(this, ContactTablePageRoutingModule);
      };

      ContactTablePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ContactTablePageRoutingModule);
      /***/
    },

    /***/
    "qO6M":
    /*!*************************************************************!*\
      !*** ./src/app/pages/contact-table/contact-table.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function qO6M(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".table-wrapper ion-label {\n  white-space: nowrap !important;\n}\n.table-wrapper ion-label p {\n  font-size: 12px;\n}\n.table-wrapper .mb-0 {\n  margin-bottom: 0px !important;\n}\n.col_style {\n  color: #7d7d7d !important;\n  font-size: 12px !important;\n}\nion-row {\n  border-bottom: 1px solid #dcdcdc;\n}\n.p_style {\n  margin: 0px;\n  padding: 5px 0px 0px 5px;\n}\n.ion_select {\n  color: #7d7d7d !important;\n  font-size: 12px !important;\n  padding: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2NvbnRhY3QtdGFibGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsOEJBQUE7QUFBSjtBQUVJO0VBQ0UsZUFBQTtBQUFOO0FBSUU7RUFDRSw2QkFBQTtBQUZKO0FBTUE7RUFDRSx5QkFBQTtFQUNBLDBCQUFBO0FBSEY7QUFNQTtFQUNFLGdDQUFBO0FBSEY7QUFNQTtFQUNFLFdBQUE7RUFDQSx3QkFBQTtBQUhGO0FBTUE7RUFDRSx5QkFBQTtFQUNBLDBCQUFBO0VBQ0EsWUFBQTtBQUhGIiwiZmlsZSI6ImNvbnRhY3QtdGFibGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRhYmxlLXdyYXBwZXIge1xuICBpb24tbGFiZWwge1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXAgIWltcG9ydGFudDtcblxuICAgIHAge1xuICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgIH1cbiAgfVxuXG4gIC5tYi0wIHtcbiAgICBtYXJnaW4tYm90dG9tOiAwcHggIWltcG9ydGFudDtcbiAgfVxufVxuXG4uY29sX3N0eWxlIHtcbiAgY29sb3I6ICM3ZDdkN2QgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1yb3cge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RjZGNkYztcbn1cblxuLnBfc3R5bGUge1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogNXB4IDBweCAwcHggNXB4O1xufVxuXG4uaW9uX3NlbGVjdCB7XG4gIGNvbG9yOiAjN2Q3ZDdkICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTJweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiA1cHg7XG59Il19 */";
      /***/
    },

    /***/
    "rtEV":
    /*!****************************************************!*\
      !*** ./src/app/services/contact-groups.service.ts ***!
      \****************************************************/

    /*! exports provided: ContactGroupsService */

    /***/
    function rtEV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactGroupsService", function () {
        return ContactGroupsService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/environments/environment */
      "AytR");

      var ContactGroupsService = /*#__PURE__*/function () {
        function ContactGroupsService(http) {
          _classCallCheck(this, ContactGroupsService);

          this.http = http;
          this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
        }

        _createClass(ContactGroupsService, [{
          key: "getContactGroups",
          value: function getContactGroups() {
            var _this3 = this;

            return new Promise(function (resolve, reject) {
              // if (status == ConnectionStatus.Online) {
              console.log(_this3.apiUrl + 'contactgroups.php');

              _this3.http.get(_this3.apiUrl + 'contactgroups.php').subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              }); // }

            });
          }
        }, {
          key: "getContactGroupsList",
          value: function getContactGroupsList(formData) {
            var _this4 = this;

            return new Promise(function (resolve, reject) {
              // if (status == ConnectionStatus.Online) {
              console.log(_this4.apiUrl + 'contactgroupslist.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this4.http.post(_this4.apiUrl + 'contactgroupslist.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              }); // }

            });
          }
        }, {
          key: "contactsImport",
          value: function contactsImport(formData) {
            var _this5 = this;

            return new Promise(function (resolve, reject) {
              // if (status == ConnectionStatus.Online) {
              console.log(_this5.apiUrl + 'contactsimport.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this5.http.post(_this5.apiUrl + 'contactsimport.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              }); // }

            });
          }
        }, {
          key: "getContacts",
          value: function getContacts(formData) {
            var _this6 = this;

            return new Promise(function (resolve, reject) {
              // if (status == ConnectionStatus.Online) {
              console.log(_this6.apiUrl + 'contacts.php');
              formData.append("auth_token", src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].auth_token);

              _this6.http.post(_this6.apiUrl + 'contacts.php', formData).subscribe(function (res) {
                resolve(res);
              }, function (err) {
                reject(err);
                console.log('something went wrong please try again');
              }); // }

            });
          }
        }]);

        return ContactGroupsService;
      }();

      ContactGroupsService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      };

      ContactGroupsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], ContactGroupsService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-contact-table-contact-table-module-es5.js.map